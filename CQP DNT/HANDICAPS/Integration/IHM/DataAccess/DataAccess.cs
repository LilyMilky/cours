﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Metier.Entities;

namespace Dataaccess
{
    public class DataAccess
    {
        DbProviderFactory dbpf;
        DbConnection conn;
        DbTransaction transaction;

        #region Loic
        public DataAccess()
        {

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

        }

        public EntityConnexion Connexion(String ID, String Mdp)
        {
            EntityConnexion EntityConnexion = new EntityConnexion();
            DbCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT CONN_PROFIL, PSN_ID FROM CONNEXION WHERE CONN_LOGIN = @id AND CONN_PWD = @mdp";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            DbParameter ParamMdp = cmd.CreateParameter();
            ParamMdp.DbType = DbType.String;
            ParamMdp.Direction = ParameterDirection.Input;
            ParamMdp.Value = Mdp;
            ParamMdp.ParameterName = "mdp";

            cmd.Parameters.Add(ParamMdp);

            // Execute the command
            conn.Open();
            DbDataReader DataReader = cmd.ExecuteReader();

            if (DataReader.HasRows)
            {
                DataReader.Read();

                EntityConnexion.PSN_ID = DataReader["PSN_ID"].ToString();
                EntityConnexion.CONN_PROFIL = DataReader["CONN_PROFIL"].ToString();
            }
            conn.Close();
            return EntityConnexion;
        }

        #region Client

        public List<EntityClient> GetClients()
        {
            List<EntityClient> lstClients = new List<EntityClient>();
            EntityClient Client;

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CLT_TELDOMICILE, C.CLT_EMAIL, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE 
                                FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID";
            cmd.Connection = conn;

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        lstClients.Add(Client);
                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstClients;
        }

        public EntityClient GetClientById(string ID)
        {
            EntityClient Client = new EntityClient();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, 
                                P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_ID = @id;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();



                    Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                    Client.ClientID = DataReader["PSN_ID"].ToString();
                    Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                    Client.Email = DataReader["CLT_EMAIL"].ToString();
                    Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                    Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                    Client.Nom = DataReader["PSN_NOM"].ToString();
                    Client.Pays = DataReader["PSN_PAYS"].ToString();
                    Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                    Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                    Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                    Client.Ville = DataReader["PSN_VILLE"].ToString();

                }


            }
            finally
            {
                conn.Close();
            }

            return Client;
        }

        public bool UpdateClient(EntityClient Client)
        {
            // Première requête de la transaction
            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE PERSONNE 
                                SET PSN_NOM = @nom, PSN_PRENOM = @prenom, PSN_ADRESSE = @adresse, PSN_VILLE = @ville, PSN_CODEPOSTAL = @cp, PSN_PAYS = @pays, PSN_MOBILE = @telmob
                                WHERE PSN_ID = @id;";
            cmd.Connection = conn;


            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = Client.ClientID;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            DbParameter ParamNom = cmd.CreateParameter();
            ParamNom.DbType = DbType.String;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Client.Nom;
            ParamNom.ParameterName = "nom";
            cmd.Parameters.Add(ParamNom);

            DbParameter ParamPrenom = cmd.CreateParameter();
            ParamPrenom.DbType = DbType.String;
            ParamPrenom.Direction = ParameterDirection.Input;
            ParamPrenom.Value = Client.Prenom;
            ParamPrenom.ParameterName = "prenom";
            cmd.Parameters.Add(ParamPrenom);

            DbParameter ParamAdresse = cmd.CreateParameter();
            ParamAdresse.DbType = DbType.String;
            ParamAdresse.Direction = ParameterDirection.Input;
            ParamAdresse.Value = Client.Adresse;
            ParamAdresse.ParameterName = "adresse";
            cmd.Parameters.Add(ParamAdresse);

            DbParameter ParamVille = cmd.CreateParameter();
            ParamVille.DbType = DbType.String;
            ParamVille.Direction = ParameterDirection.Input;
            ParamVille.Value = Client.Ville;
            ParamVille.ParameterName = "ville";
            cmd.Parameters.Add(ParamVille);

            DbParameter ParamCP = cmd.CreateParameter();
            ParamCP.DbType = DbType.String;
            ParamCP.Direction = ParameterDirection.Input;
            ParamCP.Value = Client.CodePostal;
            ParamCP.ParameterName = "cp";
            cmd.Parameters.Add(ParamCP);

            DbParameter ParamPays = cmd.CreateParameter();
            ParamPays.DbType = DbType.String;
            ParamPays.Direction = ParameterDirection.Input;
            ParamPays.Value = Client.Pays;
            ParamPays.ParameterName = "pays";
            cmd.Parameters.Add(ParamPays);

            DbParameter ParamTelMob = cmd.CreateParameter();
            ParamTelMob.DbType = DbType.String;
            ParamTelMob.Direction = ParameterDirection.Input;
            ParamTelMob.Value = Client.TelMobile;
            ParamTelMob.ParameterName = "telmob";
            cmd.Parameters.Add(ParamTelMob);



            // Deuxième requête de la transaction
            DbCommand cmd2 = dbpf.CreateCommand();
            cmd2.CommandType = CommandType.Text;
            cmd2.CommandText = @"UPDATE CLIENT 
                                 SET CLT_TELDOMICILE = @telDom, CLT_EMAIL = @mail, CLT_DISTANCE = @distance, CLT_DUREE_DEPLACEMENT = @duree 
                                 WHERE PSN_ID = @id;";
            cmd2.Connection = conn;

            DbParameter ParamID2 = cmd2.CreateParameter();
            ParamID2.DbType = DbType.String;
            ParamID2.Direction = ParameterDirection.Input;
            ParamID2.Value = Client.ClientID;
            ParamID2.ParameterName = "id";
            cmd2.Parameters.Add(ParamID2);

            DbParameter ParamTelDom = cmd2.CreateParameter();
            ParamTelDom.DbType = DbType.String;
            ParamTelDom.Direction = ParameterDirection.Input;
            ParamTelDom.Value = Client.TelDomicile;
            ParamTelDom.ParameterName = "telDom";
            cmd2.Parameters.Add(ParamTelDom);

            DbParameter ParamMail = cmd2.CreateParameter();
            ParamMail.DbType = DbType.String;
            ParamMail.Direction = ParameterDirection.Input;
            ParamMail.Value = Client.Email;
            ParamMail.ParameterName = "mail";
            cmd2.Parameters.Add(ParamMail);

            DbParameter ParamDistance = cmd2.CreateParameter();
            ParamDistance.DbType = DbType.String;
            ParamDistance.Direction = ParameterDirection.Input;
            ParamDistance.Value = Client.DistanceAntenne;
            ParamDistance.ParameterName = "distance";
            cmd2.Parameters.Add(ParamDistance);

            DbParameter ParamDuree = cmd2.CreateParameter();
            ParamDuree.DbType = DbType.String;
            ParamDuree.Direction = ParameterDirection.Input;
            ParamDuree.Value = Client.DureeDeplacement;
            ParamDuree.ParameterName = "duree";
            cmd2.Parameters.Add(ParamDuree);


            try
            {
                
                conn.Open();

                // Ouverture de la transaction
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd2.Transaction = transaction;

                // execution de la transaction
                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();

                // Validation de la transaction et fermeture de la transaction
                transaction.Commit();

            }
            catch (Exception ex)
            {
                try
                {
                    // Annule la transaction et restaure les données précédent la transaction
                    transaction.Rollback();
                    return false;
                }
                catch (Exception ex2)
                {
                    return false;
                }
            }
            finally
            {
                conn.Close();
            }

            return true;
        }

        public List<EntityClient> GetClientsById(string ID)
        {
            List<EntityClient> lstClients = new List<EntityClient>();
            EntityClient Client;

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE 
                                FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_ID = @id;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        lstClients.Add(Client);
                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstClients;
        }


        public List<EntityClient> GetClientsByContrat(string Num_Contrat)
        {
            List<EntityClient> lstClients = new List<EntityClient>();
            EntityClient Client;

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, 
                                P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID INNER JOIN CONTRAT AS CTR ON C.PSN_ID = CTR.CLI_PSN_ID WHERE CTR.CT_NUM_CONTRAT Like @Num_Contrat;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = Num_Contrat + "%";
            ParamID.ParameterName = "Num_Contrat";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        lstClients.Add(Client);
                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstClients;
        }

        public List<EntityClient> GetClientsByName(string Nom)
        {
            List<EntityClient> lstClients = new List<EntityClient>();
            EntityClient Client;

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CLT_TELDOMICILE, C.CLT_EMAIL, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, 
                                P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_NOM like @Nom;";

            DbParameter ParamNom = cmd.CreateParameter();
            ParamNom.DbType = DbType.String;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Nom + "%";
            ParamNom.ParameterName = "Nom";

            cmd.Parameters.Add(ParamNom);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        lstClients.Add(Client);
                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstClients;
        }

        public string[] InsertClient(EntityClient Client)
        {
            Client.ClientID = GetIDLastClient();

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO PERSONNE (PSN_ID, PSN_NOM, PSN_PRENOM, PSN_ADRESSE, PSN_VILLE, PSN_CODEPOSTAL, PSN_PAYS, PSN_MOBILE) 
                                VALUES (@id, @nom, @prenom, @adresse, @ville, @cp, @pays, @telmob);";

            cmd.Connection = conn;

            //TODO LIST
            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = Client.ClientID;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            DbParameter ParamNom = cmd.CreateParameter();
            ParamNom.DbType = DbType.String;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Client.Nom;
            ParamNom.ParameterName = "nom";
            cmd.Parameters.Add(ParamNom);

            DbParameter ParamPrenom = cmd.CreateParameter();
            ParamPrenom.DbType = DbType.String;
            ParamPrenom.Direction = ParameterDirection.Input;
            ParamPrenom.Value = Client.Prenom;
            ParamPrenom.ParameterName = "prenom";
            cmd.Parameters.Add(ParamPrenom);

            DbParameter ParamAdresse = cmd.CreateParameter();
            ParamAdresse.DbType = DbType.String;
            ParamAdresse.Direction = ParameterDirection.Input;
            ParamAdresse.Value = Client.Adresse;
            ParamAdresse.ParameterName = "adresse";
            cmd.Parameters.Add(ParamAdresse);

            DbParameter ParamVille = cmd.CreateParameter();
            ParamVille.DbType = DbType.String;
            ParamVille.Direction = ParameterDirection.Input;
            ParamVille.Value = Client.Ville;
            ParamVille.ParameterName = "ville";
            cmd.Parameters.Add(ParamVille);

            DbParameter ParamCP = cmd.CreateParameter();
            ParamCP.DbType = DbType.String;
            ParamCP.Direction = ParameterDirection.Input;
            ParamCP.Value = Client.CodePostal;
            ParamCP.ParameterName = "cp";
            cmd.Parameters.Add(ParamCP);

            DbParameter ParamPays = cmd.CreateParameter();
            ParamPays.DbType = DbType.String;
            ParamPays.Direction = ParameterDirection.Input;
            ParamPays.Value = Client.Pays;
            ParamPays.ParameterName = "pays";
            cmd.Parameters.Add(ParamPays);

            DbParameter ParamTelMob = cmd.CreateParameter();
            ParamTelMob.DbType = DbType.String;
            ParamTelMob.Direction = ParameterDirection.Input;
            ParamTelMob.Value = Client.TelMobile;
            ParamTelMob.ParameterName = "telmob";
            cmd.Parameters.Add(ParamTelMob);


            DbCommand cmd2 = dbpf.CreateCommand();
            cmd2.CommandType = CommandType.Text;
            cmd2.CommandText = @"INSERT INTO CLIENT (PSN_ID, CLT_TELDOMICILE, CLT_EMAIL, CLT_DISTANCE, CLT_DUREE_DEPLACEMENT) 
                                VALUES (@id, @telDom, @mail, @distance, @duree);";

            cmd2.Connection = conn;

            DbParameter ParamID2 = cmd2.CreateParameter();
            ParamID2.DbType = DbType.String;
            ParamID2.Direction = ParameterDirection.Input;
            ParamID2.Value = Client.ClientID;
            ParamID2.ParameterName = "id";
            cmd2.Parameters.Add(ParamID2);

            DbParameter ParamTelDom = cmd2.CreateParameter();
            ParamTelDom.DbType = DbType.String;
            ParamTelDom.Direction = ParameterDirection.Input;
            ParamTelDom.Value = Client.TelDomicile;
            ParamTelDom.ParameterName = "telDom";
            cmd2.Parameters.Add(ParamTelDom);

            DbParameter ParamMail = cmd2.CreateParameter();
            ParamMail.DbType = DbType.String;
            ParamMail.Direction = ParameterDirection.Input;
            ParamMail.Value = Client.Email;
            ParamMail.ParameterName = "mail";
            cmd2.Parameters.Add(ParamMail);

            DbParameter ParamDistance = cmd2.CreateParameter();
            ParamDistance.DbType = DbType.String;
            ParamDistance.Direction = ParameterDirection.Input;
            ParamDistance.Value = Client.DistanceAntenne;
            ParamDistance.ParameterName = "distance";
            cmd2.Parameters.Add(ParamDistance);

            DbParameter ParamDuree = cmd2.CreateParameter();
            ParamDuree.DbType = DbType.String;
            ParamDuree.Direction = ParameterDirection.Input;
            ParamDuree.Value = Client.DureeDeplacement;
            ParamDuree.ParameterName = "duree";
            cmd2.Parameters.Add(ParamDuree);

            DbCommand cmd3 = dbpf.CreateCommand();
            cmd3.CommandType = CommandType.Text;
            cmd3.CommandText = @"INSERT INTO CONNEXION (CONN_LOGIN, PSN_ID, CONN_PWD, CONN_PROFIL) 
                                VALUES (@login, @id, @pwd, @profil);";
            cmd3.Connection = conn;

            DbParameter ParamLogin = cmd3.CreateParameter();
            ParamLogin.DbType = DbType.String;
            ParamLogin.Direction = ParameterDirection.Input;
            ParamLogin.Value = Client.Email;
            ParamLogin.ParameterName = "login";
            cmd3.Parameters.Add(ParamLogin);

            DbParameter ParamID3 = cmd3.CreateParameter();
            ParamID3.DbType = DbType.String;
            ParamID3.Direction = ParameterDirection.Input;
            ParamID3.Value = Client.ClientID;
            ParamID3.ParameterName = "id";
            cmd3.Parameters.Add(ParamID3);

            string pwd = GeneratePassword (10);

            DbParameter ParamPwd = cmd3.CreateParameter();
            ParamPwd.DbType = DbType.String;
            ParamPwd.Direction = ParameterDirection.Input;
            ParamPwd.Value = pwd;
            ParamPwd.ParameterName = "pwd";
            cmd3.Parameters.Add(ParamPwd);

            DbParameter ParamProfil = cmd3.CreateParameter();
            ParamProfil.DbType = DbType.String;
            ParamProfil.Direction = ParameterDirection.Input;
            ParamProfil.Value = "CLIENT";
            ParamProfil.ParameterName = "profil";
            cmd3.Parameters.Add(ParamProfil);


            try
            {

                conn.Open();

                // Ouverture de la transaction
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd2.Transaction = transaction;
                cmd3.Transaction = transaction;

                // execution de la transaction
                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();
                cmd3.ExecuteNonQuery();

                // Validation de la transaction et fermeture de la transaction
                transaction.Commit();

            }
            catch (Exception ex)
            {
                try
                {
                    // Annule la transaction et restaure les données précédent la transaction
                    transaction.Rollback();
                    return new string[] { "Erreur", ex.Message };
                }
                catch (Exception ex2)
                {
                    return new string[] { "Erreur", ex2.Message };
                }
            }
            finally
            {
                conn.Close();
            }

            return new string[] { Client.ClientID, pwd, Client.Email};

        }

        private string GetIDLastClient()
        {
            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT PSN_ID, CLT_DISTANCE FROM CLIENT;";
            cmd.Connection = conn;

            string ID = "";
            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        ID = DataReader["PSN_ID"].ToString();
                    }
                }

            }
            finally
            {
                conn.Close();
            }

            int iID = Int32.Parse(ID);
            iID++;

            return iID.ToString();
        } 
        #endregion

        #region Contrat

        public bool InsertContrat(EntityContrat contrat)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO CONTRAT (CT_NUM_CONTRAT, TP_CTR_CODE, CCIAL_PSN_ID, CLI_PSN_ID, CT_DATE_ECHEANCE ,CT_DATE_SIGNATURE_CONTRAT, CT_PRIX) 
                                VALUES (@num, @code, @ccial, @cli, @echeance, @signature, @prix);";

            DbParameter ParamNum = cmd.CreateParameter();
            ParamNum.DbType = DbType.String;
            ParamNum.Direction = ParameterDirection.Input;
            ParamNum.Value = CalculNumContrat();
            ParamNum.ParameterName = "num";
            cmd.Parameters.Add(ParamNum);

            DbParameter ParamCode = cmd.CreateParameter();
            ParamCode.DbType = DbType.String;
            ParamCode.Direction = ParameterDirection.Input;
            ParamCode.Value = contrat.TypeContrat;
            ParamCode.ParameterName = "code";
            cmd.Parameters.Add(ParamCode);

            DbParameter ParamCcial = cmd.CreateParameter();
            ParamCcial.DbType = DbType.String;
            ParamCcial.Direction = ParameterDirection.Input;
            ParamCcial.Value = contrat.CommercialID;
            ParamCcial.ParameterName = "ccial";
            cmd.Parameters.Add(ParamCcial);

            DbParameter ParamCli = cmd.CreateParameter();
            ParamCli.DbType = DbType.String;
            ParamCli.Direction = ParameterDirection.Input;
            ParamCli.Value = contrat.ClientID;
            ParamCli.ParameterName = "cli";
            cmd.Parameters.Add(ParamCli);

            DateTime Echeance = new DateTime();
            Echeance = contrat.DateSignatureContrat;
            Echeance.AddYears(1);

            DbParameter ParamEcheance = cmd.CreateParameter();
            ParamEcheance.DbType = DbType.DateTime;
            ParamEcheance.Direction = ParameterDirection.Input;
            ParamEcheance.Value = Echeance;
            ParamEcheance.ParameterName = "echeance";
            cmd.Parameters.Add(ParamEcheance);

            DbParameter ParamSignature = cmd.CreateParameter();
            ParamSignature.DbType = DbType.DateTime;
            ParamSignature.Direction = ParameterDirection.Input;
            ParamSignature.Value = contrat.DateSignatureContrat;
            ParamSignature.ParameterName = "signature";
            cmd.Parameters.Add(ParamSignature);

            DbParameter ParamPrix = cmd.CreateParameter();
            ParamPrix.DbType = DbType.String;
            ParamPrix.Direction = ParameterDirection.Input;
            ParamPrix.Value = "0";
            ParamPrix.ParameterName = "prix";
            cmd.Parameters.Add(ParamPrix);




            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (DbException ex)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

            return true;
        }

        public string CalculNumContrat()
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT TOP 1 CT_NUM_CONTRAT
                                FROM CONTRAT 
                                ORDER BY CT_NUM_CONTRAT DESC;";
            string Num = "";
            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    
                    Num = DataReader["CT_NUM_CONTRAT"].ToString();

                    string NumTemp = Num.Split('-')[1];
                    NumTemp = (Int32.Parse(NumTemp) + 1).ToString();
                    Num = Num.Split('-')[0] + "-" + NumTemp;
                }
            }
            finally
            {
                conn.Close();
            }

            
            return Num;
        }

        public List<string> GetContratsByClientID(string ID)
        {
            List<string> lstContrats = new List<string>();

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT CT_NUM_CONTRAT
                                FROM CONTRAT WHERE CLI_PSN_ID = @id ";
            cmd.Connection = conn;

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {

                        lstContrats.Add(DataReader["CT_NUM_CONTRAT"].ToString());

                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstContrats;
        }

        public List<string> GetContratsByClientIDToday(string ID)
        {
            List<string> lstContrats = new List<string>();

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT CT_NUM_CONTRAT
                                FROM CONTRAT 
                                WHERE CLI_PSN_ID = @id AND CT_DATE_SIGNATURE_CONTRAT >= @date";
            cmd.Connection = conn;

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            DbParameter ParamDate = cmd.CreateParameter();
            ParamDate.DbType = DbType.DateTime;
            ParamDate.Direction = ParameterDirection.Input;
            ParamDate.Value = DateTime.Today;
            ParamDate.ParameterName = "Date";

            cmd.Parameters.Add(ParamDate);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {

                        lstContrats.Add(DataReader["CT_NUM_CONTRAT"].ToString());

                    }
                }


            }
            finally
            {
                conn.Close();
            }

            return lstContrats;
        }

        public EntityContrat GetContratByNumContrat(string ID)
        {
            EntityContrat Contrat = new EntityContrat();

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX, CLI_PSN_ID, CCIAL_PSN_ID
                                FROM CONTRAT WHERE CT_NUM_CONTRAT = @id ";
            cmd.Connection = conn;

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                    Contrat.TypeContrat = Int32.Parse(DataReader["TP_CTR_CODE"].ToString());
                    Contrat.DateEcheance = DateTime.Parse(DataReader["CT_DATE_ECHEANCE"].ToString());
                    if (!String.IsNullOrEmpty(DataReader["CT_DATE_RENOUVELLEMENT"].ToString()))
                        Contrat.DateRenouvellement = DateTime.Parse(DataReader["CT_DATE_RENOUVELLEMENT"].ToString());
                    else
                        Contrat.DateRenouvellement = null;
                    Contrat.DateSignatureContrat = DateTime.Parse(DataReader["CT_DATE_SIGNATURE_CONTRAT"].ToString());
                    Contrat.Prix = Decimal.Parse(DataReader["CT_PRIX"].ToString());
                    Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                    Contrat.CommercialID = DataReader["CCIAL_PSN_ID"].ToString();
                }
                conn.Close();
                Contrat.Taux = GetTauxContratByTypeCode(Contrat.TypeContrat);
            }
            finally
            {
                if(conn.State == ConnectionState.Open)
                    conn.Close();
            }


            return Contrat;

        }

        public int GetTauxContratByTypeCode(int TypeCode)
        {
            EntityContrat Contrat = new EntityContrat();

            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT TAUX
                                FROM TYPE_CONTRAT WHERE TP_CTR_CODE = @TypeCode ";
            cmd.Connection = conn;

            DbParameter ParamTypeCode = cmd.CreateParameter();
            ParamTypeCode.DbType = DbType.String;
            ParamTypeCode.Direction = ParameterDirection.Input;
            ParamTypeCode.Value = TypeCode;
            ParamTypeCode.ParameterName = "TypeCode";

            cmd.Parameters.Add(ParamTypeCode);

            int taux = 0;

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    taux = Int32.Parse(DataReader["TAUX"].ToString());


                }

            }
            finally
            {
                conn.Close();
            }

            return taux;

        }

        public List<int> GetContratCodes()
        {
            List<int> type = new List<int>();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT TP_CTR_CODE 
                                FROM TYPE_CONTRAT";

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        type.Add(Int32.Parse(DataReader["TP_CTR_CODE"].ToString()));
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return type;
        }

        public string GetLastNumContrat()
        {

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT TOP 1 CT_NUM_CONTRAT 
                                FROM CONTRAT
                                ORDER BY CT_NUM_CONTRAT DESC";

            string num = "";

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    num = DataReader["CT_NUM_CONTRAT"].ToString();
                    
                }
            }
            finally
            {
                conn.Close();
            }

            return num;
        }

        public bool UpdateContrat(EntityContrat Contrat)
        {
            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE CONTRAT 
                                SET CT_DATE_ECHEANCE = @echeance, CT_DATE_RENOUVELLEMENT = @renouvellement
                                WHERE CT_NUM_CONTRAT = @num";
            cmd.Connection = conn;

            DbParameter ParamNom = cmd.CreateParameter();
            ParamNom.DbType = DbType.DateTime;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Contrat.DateRenouvellement;
            ParamNom.ParameterName = "renouvellement";
            cmd.Parameters.Add(ParamNom);

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.DateTime;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = Contrat.DateEcheance;
            ParamID.ParameterName = "echeance";
            cmd.Parameters.Add(ParamID);

            

            DbParameter ParamNum = cmd.CreateParameter();
            ParamNum.DbType = DbType.String;
            ParamNum.Direction = ParameterDirection.Input;
            ParamNum.Value = Contrat.NumContrat;
            ParamNum.ParameterName = "num";
            cmd.Parameters.Add(ParamNum);

            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                return false;
                //############ LOG
                //Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                //Console.WriteLine("  Message: {0}", ex.Message);
               
            }
            finally
            {
                conn.Close();
            }

            return true;
        }

        #endregion


        #region Commercial

        public EntityCommercial GetCommercialById(string ID)
        {
            EntityCommercial Commercial = new EntityCommercial();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT C.PSN_ID, C.CCIAL_DATE_EMBAUCHE, C.AN_VILLE, C.CCIAL_TELDOMICILE, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, P.PSN_VILLE, P.PSN_CODEPOSTAL, 
                                P.PSN_PAYS, P.PSN_MOBILE FROM COMMERCIAL AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_ID = @id;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";

            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();



                    Commercial.Adresse = DataReader["PSN_ADRESSE"].ToString();
                    Commercial.ID = DataReader["PSN_ID"].ToString();
                    Commercial.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                    Commercial.Antenne = DataReader["AN_VILLE"].ToString();
                    Commercial.DateEmbauche = DataReader["CCIAL_DATE_EMBAUCHE"].ToString();
                    Commercial.Nom = DataReader["PSN_NOM"].ToString();
                    Commercial.Pays = DataReader["PSN_PAYS"].ToString();
                    Commercial.Prenom = DataReader["PSN_PRENOM"].ToString();
                    Commercial.TelDomicile = DataReader["CCIAL_TELDOMICILE"].ToString();
                    Commercial.TelMobile = DataReader["PSN_MOBILE"].ToString();
                    Commercial.Ville = DataReader["PSN_VILLE"].ToString();

                }


            }
            finally
            {
                conn.Close();
            }

            return Commercial;
        }

        #endregion

        #region Materiel

        public List<string> GetMaterielTypes()
        {
            List<string> type = new List<string>();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT DISTINCT MTL_TYPE 
                                FROM MATERIEL";

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        type.Add(DataReader["MTL_TYPE"].ToString());
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return type;
        }

        public string GetMaterielType(int REF)
        {

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_TYPE 
                                FROM MATERIEL
                                WHERE MTL_REF2 = @REF";

            DbParameter ParamREF = cmd.CreateParameter();
            ParamREF.DbType = DbType.Int32;
            ParamREF.Direction = ParameterDirection.Input;
            ParamREF.Value = REF;
            ParamREF.ParameterName = "REF";
            cmd.Parameters.Add(ParamREF);

            string type = "";
            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    
                    type = DataReader["MTL_TYPE"].ToString();
                    
                }
            }
            finally
            {
                conn.Close();
            }

            return type;
        }

        public List<string> GetMaterielModels(string type)
        {
            List<string> Model = new List<string>();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT DISTINCT MTL_MODELE 
                                FROM MATERIEL
                                WHERE MTL_TYPE = @type";

            DbParameter ParamType = cmd.CreateParameter();
            ParamType.DbType = DbType.String;
            ParamType.Direction = ParameterDirection.Input;
            ParamType.Value = type;
            ParamType.ParameterName = "type";

            cmd.Parameters.Add(ParamType);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Model.Add(DataReader["MTL_MODELE"].ToString());
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Model;
        }  

        public string GetMaterielModel(int REF)
        {

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_MODELE 
                                FROM MATERIEL
                                WHERE MTL_REF2 = @REF";

            DbParameter ParamREF = cmd.CreateParameter();
            ParamREF.DbType = DbType.Int32;
            ParamREF.Direction = ParameterDirection.Input;
            ParamREF.Value = REF;
            ParamREF.ParameterName = "REF";
            cmd.Parameters.Add(ParamREF);

            string type = "";
            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    type = DataReader["MTL_MODELE"].ToString();

                }
            }
            finally
            {
                conn.Close();
            }

            return type;
        }

        public Decimal GetMaterielPrix(string type, string model)
        {

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_PRIX 
                                FROM MATERIEL
                                WHERE MTL_TYPE = @type AND MTL_MODELE = @model";

            DbParameter ParamType = cmd.CreateParameter();
            ParamType.DbType = DbType.String;
            ParamType.Direction = ParameterDirection.Input;
            ParamType.Value = type;
            ParamType.ParameterName = "type";

            cmd.Parameters.Add(ParamType);

            DbParameter ParamModel = cmd.CreateParameter();
            ParamModel.DbType = DbType.String;
            ParamModel.Direction = ParameterDirection.Input;
            ParamModel.Value = model;
            ParamModel.ParameterName = "model";

            cmd.Parameters.Add(ParamModel);

            Decimal Prix = 0;

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Prix = Decimal.Parse(DataReader["MTL_PRIX"].ToString());
                    
                }
            }
            finally
            {
                conn.Close();
            }

            return Prix;
        }

        public int GetMaterielRef(string type, string model)
        {

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_REF2 
                                FROM MATERIEL
                                WHERE MTL_TYPE = @type AND MTL_MODELE = @model";

            DbParameter ParamType = cmd.CreateParameter();
            ParamType.DbType = DbType.String;
            ParamType.Direction = ParameterDirection.Input;
            ParamType.Value = type;
            ParamType.ParameterName = "type";

            cmd.Parameters.Add(ParamType);

            DbParameter ParamModel = cmd.CreateParameter();
            ParamModel.DbType = DbType.String;
            ParamModel.Direction = ParameterDirection.Input;
            ParamModel.Value = model;
            ParamModel.ParameterName = "model";

            cmd.Parameters.Add(ParamModel);

            int MLT_REF = 0;

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    MLT_REF = Int32.Parse(DataReader["MTL_REF2"].ToString());

                }
            }
            finally
            {
                conn.Close();
            }

            return MLT_REF;
        }

        public string InsertMateriel(EntityMaterielAchete materiel)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO MATERIEL_ACHETE (MTL_ID, CT_NUM_CONTRAT, MTL_REF2, MTL_PRIX_ACHAT) 
                                VALUES (@id, @num, @ref, @prix);";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = materiel.MaterielId;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            DbParameter ParamNum = cmd.CreateParameter();
            ParamNum.DbType = DbType.String;
            ParamNum.Direction = ParameterDirection.Input;
            ParamNum.Value = CalculNumContrat();
            ParamNum.ParameterName = "num";
            cmd.Parameters.Add(ParamNum);

            DbParameter ParamRef = cmd.CreateParameter();
            ParamRef.DbType = DbType.Int32;
            ParamRef.Direction = ParameterDirection.Input;
            ParamRef.Value = materiel.Reference;
            ParamRef.ParameterName = "ref";
            cmd.Parameters.Add(ParamRef);


            DbParameter ParamPrix = cmd.CreateParameter();
            ParamPrix.DbType = DbType.Decimal;
            ParamPrix.Direction = ParameterDirection.Input;
            ParamPrix.Value = materiel.PrixAchat;
            ParamPrix.ParameterName = "prix";
            cmd.Parameters.Add(ParamPrix);

            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    return "Le numéro de série de l'appareil existe déjà dans la base";
                else
                    return "Une erreur est survenu pendant l'ajout du materiel, veuillez consulter votre administrateur réseau";
            }
            finally
            {
                conn.Close();
            }

            return "OK";
        }

        public EntityMaterielAchete GetMaterielById(string ID)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_ID, CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE
                                WHERE MTL_ID = @id;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            EntityMaterielAchete materiel = new EntityMaterielAchete();

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        materiel.MaterielId = DataReader["MTL_ID"].ToString();
                        materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        materiel.Reference = Int32.Parse(DataReader["MTL_REF2"].ToString());
                        materiel.DateInstallation = DateTime.Parse(DataReader["MTL_DATE_INSTALLATION"].ToString());
                        materiel.PrixAchat = Decimal.Parse(DataReader["MTL_PRIX_ACHAT"].ToString());
                    }
                   
                }
            }
            finally
            {
                conn.Close();
            }

            return materiel;
        }

        public EntityMaterielAchete GetMaterielSerie(string ID)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_ID, CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE
                                WHERE MTL_ID = @id;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ID;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            EntityMaterielAchete materiel = new EntityMaterielAchete();
            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    
                    
                    materiel.MaterielId = DataReader["MTL_ID"].ToString();
                    materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                    materiel.Reference = Int32.Parse(DataReader["MTL_REF2"].ToString());
                    materiel.DateInstallation = DateTime.Parse(DataReader["MTL_DATE_INSTALLATION"].ToString());
                    materiel.PrixAchat = Decimal.Parse(DataReader["MTL_PRIX_ACHAT"].ToString());
                    
                    

                }
            }
            finally
            {
                conn.Close();
            }

            return materiel;
        }

        public List<EntityMaterielAchete> GetMaterielContrat(string num)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT MTL_ID, CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE
                                WHERE CT_NUM_CONTRAT = @num;";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = num;
            ParamID.ParameterName = "num";
            cmd.Parameters.Add(ParamID);

            List<EntityMaterielAchete> lstMateriel = new List<EntityMaterielAchete>();

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        EntityMaterielAchete materiel = new EntityMaterielAchete();
                        materiel.MaterielId = DataReader["MTL_ID"].ToString();
                        materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        materiel.Reference = Int32.Parse(DataReader["MTL_REF2"].ToString());
                        materiel.DateInstallation = DateTime.Parse(DataReader["MTL_DATE_INSTALLATION"].ToString());
                        materiel.PrixAchat = Decimal.Parse(DataReader["MTL_PRIX_ACHAT"].ToString());
                        lstMateriel.Add(materiel);
                    }

                }
            }
            finally
            {
                conn.Close();
            }

            return lstMateriel;
        }

        public List<EntityMaterielAchete> GetMaterielInstallation(DateTime date, int choix, string ClientID)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;

            /* @choix
             * 1 = installation le
             * 2 = installation depuis le
             * 3 = installation jusqu'au
             */
            switch (choix)
            {
                case 1: cmd.CommandText = @"SELECT DISTINCT M.MTL_ID, M.CT_NUM_CONTRAT, M.MTL_REF2, M.MTL_DATE_INSTALLATION, M.MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE as M, CONTRAT as CTR, CLIENT as C
                                WHERE M.MTL_DATE_INSTALLATION = @date AND M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID;";
                    break;

                case 2: cmd.CommandText = @"SELECT DISTINCT M.MTL_ID, M.CT_NUM_CONTRAT, M.MTL_REF2, M.MTL_DATE_INSTALLATION, M.MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE as M, CONTRAT as CTR, CLIENT as C
                                WHERE M.MTL_DATE_INSTALLATION >= @date AND M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID;";
                    break;

                case 3: cmd.CommandText = @"SELECT DISTINCT M.MTL_ID, M.CT_NUM_CONTRAT, M.MTL_REF2, M.MTL_DATE_INSTALLATION, M.MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE as M, CONTRAT as CTR, CLIENT as C
                                WHERE M.MTL_DATE_INSTALLATION <= @date AND M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID;";
                    break;
            }

            DbParameter ParamDate = cmd.CreateParameter();
            ParamDate.DbType = DbType.DateTime;
            ParamDate.Direction = ParameterDirection.Input;
            ParamDate.Value = date;
            ParamDate.ParameterName = "date";
            cmd.Parameters.Add(ParamDate);

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ClientID;
            ParamID.ParameterName = "ClientID";
            cmd.Parameters.Add(ParamID);

            List<EntityMaterielAchete> lstMateriel = new List<EntityMaterielAchete>();

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        EntityMaterielAchete materiel = new EntityMaterielAchete();
                        materiel.MaterielId = DataReader["MTL_ID"].ToString();
                        materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        materiel.Reference = Int32.Parse(DataReader["MTL_REF2"].ToString());
                        materiel.DateInstallation = DateTime.Parse(DataReader["MTL_DATE_INSTALLATION"].ToString());
                        materiel.PrixAchat = Decimal.Parse(DataReader["MTL_PRIX_ACHAT"].ToString());
                        lstMateriel.Add(materiel);
                    }

                }
            }
            finally
            {
                conn.Close();
            }

            return lstMateriel;
        }

        public List<EntityMaterielAchete> GetMaterielInstallation(DateTime dateDeb, DateTime dateFin, string ClientID)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"SELECT DISTINCT M.MTL_ID, M.CT_NUM_CONTRAT, M.MTL_REF2, M.MTL_DATE_INSTALLATION, M.MTL_PRIX_ACHAT
                                FROM MATERIEL_ACHETE as M, CONTRAT as CTR, CLIENT as C
                                WHERE M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID AND M.MTL_DATE_INSTALLATION BETWEEN @dateDeb AND @dateFin  AND M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID;";
                  
            DbParameter ParamDateDeb = cmd.CreateParameter();
            ParamDateDeb.DbType = DbType.DateTime;
            ParamDateDeb.Direction = ParameterDirection.Input;
            ParamDateDeb.Value = dateDeb;
            ParamDateDeb.ParameterName = "dateDeb";
            cmd.Parameters.Add(ParamDateDeb);

            DbParameter ParamDateFin = cmd.CreateParameter();
            ParamDateFin.DbType = DbType.DateTime;
            ParamDateFin.Direction = ParameterDirection.Input;
            ParamDateFin.Value = dateFin;
            ParamDateFin.ParameterName = "dateFin";
            cmd.Parameters.Add(ParamDateFin);

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ClientID;
            ParamID.ParameterName = "ClientID";
            cmd.Parameters.Add(ParamID);

            List<EntityMaterielAchete> lstMateriel = new List<EntityMaterielAchete>();

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        EntityMaterielAchete materiel = new EntityMaterielAchete();
                        materiel.MaterielId = DataReader["MTL_ID"].ToString();
                        materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        materiel.Reference = Int32.Parse(DataReader["MTL_REF2"].ToString());
                        materiel.DateInstallation = DateTime.Parse(DataReader["MTL_DATE_INSTALLATION"].ToString());
                        materiel.PrixAchat = Decimal.Parse(DataReader["MTL_PRIX_ACHAT"].ToString());
                        lstMateriel.Add(materiel);
                    }

                }
            }
            finally
            {
                conn.Close();
            }

            return lstMateriel;
        }

        public List<string> GetMateriels(string ClientID)
        {
            List<string> materiels = new List<string>();

            DbCommand cmd = conn.CreateCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT DISTINCT M.MTL_ID
                                FROM MATERIEL_ACHETE  as M, CONTRAT as CTR, CLIENT as C
                                WHERE  M.CT_NUM_CONTRAT = CTR.CT_NUM_CONTRAT AND CTR.CLI_PSN_ID = @ClientID";

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = ClientID;
            ParamID.ParameterName = "ClientID";
            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                DbDataReader DataReader = cmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        materiels.Add(DataReader["MTL_ID"].ToString());
                    }

                }
            }
            finally
            {
                conn.Close();
            }

            return materiels;
        }

        public bool UpdateMateriel(EntityMaterielAchete materiel)
        {
            DbCommand cmd = dbpf.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE MATERIEL_ACHETE 
                                SET MTL_REF2 = @REF, MTL_DATE_INSTALLATION = @date, MTL_PRIX_ACHAT = @prix
                                WHERE MTL_ID = @id";
            cmd.Connection = conn;

            DbParameter ParamRef = cmd.CreateParameter();
            ParamRef.DbType = DbType.Int32;
            ParamRef.Direction = ParameterDirection.Input;
            ParamRef.Value = materiel.Reference;
            ParamRef.ParameterName = "REF";
            cmd.Parameters.Add(ParamRef);

            DbParameter ParamDate = cmd.CreateParameter();
            ParamDate.DbType = DbType.DateTime;
            ParamDate.Direction = ParameterDirection.Input;
            ParamDate.Value = materiel.DateInstallation;
            ParamDate.ParameterName = "date";
            cmd.Parameters.Add(ParamDate);

            DbParameter ParamPrix = cmd.CreateParameter();
            ParamPrix.DbType = DbType.Decimal;
            ParamPrix.Direction = ParameterDirection.Input;
            ParamPrix.Value = materiel.PrixAchat;
            ParamPrix.ParameterName = "prix";
            cmd.Parameters.Add(ParamPrix);

            DbParameter ParamID = cmd.CreateParameter();
            ParamID.DbType = DbType.String;
            ParamID.Direction = ParameterDirection.Input;
            ParamID.Value = materiel.MaterielId;
            ParamID.ParameterName = "id";
            cmd.Parameters.Add(ParamID);

            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                return false;
                //############ LOG
                //Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                //Console.WriteLine("  Message: {0}", ex.Message);

            }
            finally
            {
                conn.Close();
            }

            return true;
        }


        #endregion

        static string GeneratePassword(int Length)
        {
            return GeneratePassword(Length, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray());
        }

        static string GeneratePassword(int Length, bool CaseSensitive)
        {
            if (CaseSensitive)
                return GeneratePassword(Length);
            else
                return GeneratePassword(Length, "0123456789abcdefghijklmnopqrstuvwxyz".ToCharArray());
        }

        static string GeneratePassword(int Length, char[] Chars)
        {
            string Password = string.Empty;
            System.Random rnd = new System.Random();
            for (int i = 0; i < Length; i++)
                Password += Chars[rnd.Next(Chars.Length)];
            return Password;
        }
        #endregion

        #region Yann

        public List<EntityClient> SelectClient(string Nom)
        {
            List<EntityClient> Clients = new List<EntityClient>();
            EntityClient Client;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_NOM like @Nom;";

            DbParameter ParamNom = accCmd.CreateParameter();
            ParamNom.DbType = DbType.String;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Nom + "%";
            ParamNom.ParameterName = "Nom";

            accCmd.Parameters.Add(ParamNom);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        Clients.Add(Client);
                    }
                }

                return Clients;
            }
            finally
            {
                conn.Close();
            }

        }

        public List<EntityClient> SelectClient()
        {
            List<EntityClient> Clients = new List<EntityClient>();
            EntityClient Client;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DIGICODE, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Digicode = DataReader["CLT_DIGICODE"].ToString();
                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        Clients.Add(Client);
                    }
                }

                return Clients;
            }
            finally
            {
                conn.Close();
            }

        }

        public List<EntityContrat> GetContratById(string ID)
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT WHERE CLI_PSN_ID = @ID;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = ID;
            ParamId.ParameterName = "ID";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityContrat> GetContrat()
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        string DateR = DataReader["CT_DATE_RENOUVELLEMENT"].ToString();
                        if (!string.IsNullOrEmpty(DateR.Trim()))
                            Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        else
                            Contrat.DateRenouvellement = null;
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityContrat> GetContratsByNumContrat(string NumContrat)
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = NumContrat;
            ParamId.ParameterName = "NumContrat";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityMaterielAchete> GetMaterielAcheteByRef(string Reference)
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE WHERE MTL_REF2 = @Reference;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = Reference;
            ParamId.ParameterName = "Reference";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<EntityMaterielAchete> GetMaterielByContrat(string NumContrat)
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = NumContrat;
            ParamId.ParameterName = "NumContrat";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<EntityMaterielAchete> GetMateriel()
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<string> GetIdMaterielByNumContrat(string NumContrat)
        {
            List<string> IdMateriels = new List<string>();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT MTL_REF2 FROM MATERIEL_ACHETE WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        IdMateriels.Add(DataReader["MTL_REF2"].ToString());
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return IdMateriels;
        }

        public string GetTypeContratByNumeroContrat(string NumContrat)
        {
            string TypeContrat;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT TP_CTR_CODE FROM dbo.CONTRAT
                                    WHERE CT_NUM_CONTRAT=@NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);
            try
            {
                conn.Open();
                TypeContrat = accCmd.ExecuteScalar().ToString();
            }
            finally
            {
                conn.Close();
            }
            return TypeContrat;
        }

        public EntityMateriel GetMaterielByMaterielId(string MaterielId)
        {
            EntityMateriel Materiel = new EntityMateriel();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT MTL_REF2, MTL_TYPE, MTL_MODELE, MTL_PRIX FROM dbo.MATERIEL
                                    WHERE MTL_REF2=@MaterielId;";

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Materiel.MaterielId = Convert.ToInt32(DataReader["MTL_REF2"]);
                    Materiel.Modele = DataReader["MTL_MODELE"].ToString();
                    Materiel.Prix = Convert.ToDecimal(DataReader["MTL_PRIX"]);
                    Materiel.Type = DataReader["MTL_TYPE"].ToString();
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiel;
        }

        public string GetIdIntervention()
        {
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID FROM dbo.INTERVENTION";

            string Id = "0";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Id = DataReader["ITV_ID"].ToString();
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Id;
        }

        public List<string> GetTechniciensId()
        {
            List<string> Ids = new List<string>();
            string Id;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT PSN_ID FROM TECHNICIEN;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Id = DataReader["PSN_ID"].ToString();

                        Ids.Add(Id);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Ids;
        }

        public EntityIntervention GetInterventionByDateDebut(DateTime DateDebut)
        {
            EntityIntervention Intervention = new EntityIntervention();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_KM_PARCOURU, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION
                                    WHERE ITV_DATE_DEBUT=@DateDebut;";

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Intervention = new EntityIntervention();

                    Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                    Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                    Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                    Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                    Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                    Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                    Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                    Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                    if (!string.IsNullOrEmpty(DataReader["ITV_KM_PARCOURU"].ToString()))
                        Intervention.KMParcouru = Convert.ToDecimal(DataReader["ITV_KM_PARCOURU"]);
                    Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                    Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();
                }
            }
            finally
            {
                conn.Close();
            }

            return Intervention;
        }

        public List<EntityIntervention> GetAllIntervention(string TechnicienId)
        {
            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION WHERE TEC_PSN_ID=@TechId;";

            DbParameter ParamTechId = accCmd.CreateParameter();
            ParamTechId.DbType = DbType.String;
            ParamTechId.Direction = ParameterDirection.Input;
            ParamTechId.Value = TechnicienId;
            ParamTechId.ParameterName = "TechId";
            accCmd.Parameters.Add(ParamTechId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Intervention = new EntityIntervention();

                        Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                        Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                        Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                        Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                        Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                        Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                        Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                        Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                        Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();

                        Interventions.Add(Intervention);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Interventions;
        }

        public void UpdateDateIntervention(DateTime DateDebut, DateTime DateFin, DateTime DateStart)
        {
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET ITV_DATE_DEBUT=@DateDebut, ITV_DATE_FIN = @DateFin WHERE ITV_DATE_DEBUT = @DateStart;";

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateFin = accCmd.CreateParameter();
            ParamDateFin.DbType = DbType.DateTime;
            ParamDateFin.Direction = ParameterDirection.Input;
            ParamDateFin.Value = DateFin;
            ParamDateFin.ParameterName = "DateFin";
            accCmd.Parameters.Add(ParamDateFin);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.DateTime;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = DateStart;
            ParamNumContrat.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }

        }

        public void InsertIntervention(string AssistantId, string NumContrat, string TechnicienId, DateTime DateDebut, DateTime DateFin, string DescriptionProbleme, string Garantie, string MaterielId)
        {
            string InterventionId = (Convert.ToInt32(GetIdIntervention()) + 1).ToString();    //Recuperation de l'id de la derniere intervention;

            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"INSERT INTO dbo.INTERVENTION VALUES(@IDIntervention, @NumContrat, @AssistantId,@TechnicienId,
                @DateDebut,@DateEnd,@DescriptionProbleme,NULL,NULL, @Garantie, @MaterielId);";

            #region PARAMETRES
            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "IDIntervention";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            DbParameter ParamAssistantId = accCmd.CreateParameter();
            ParamAssistantId.DbType = DbType.String;
            ParamAssistantId.Direction = ParameterDirection.Input;
            ParamAssistantId.Value = AssistantId;
            ParamAssistantId.ParameterName = "AssistantId";
            accCmd.Parameters.Add(ParamAssistantId);

            DbParameter ParamTechnicienId = accCmd.CreateParameter();
            ParamTechnicienId.DbType = DbType.String;
            ParamTechnicienId.Direction = ParameterDirection.Input;
            ParamTechnicienId.Value = TechnicienId;
            ParamTechnicienId.ParameterName = "TechnicienId";
            accCmd.Parameters.Add(ParamTechnicienId);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateFin = accCmd.CreateParameter();
            ParamDateFin.DbType = DbType.DateTime;
            ParamDateFin.Direction = ParameterDirection.Input;
            ParamDateFin.Value = DateFin;
            ParamDateFin.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateFin);

            DbParameter ParamDescriptionProbleme = accCmd.CreateParameter();
            ParamDescriptionProbleme.DbType = DbType.String;
            ParamDescriptionProbleme.Direction = ParameterDirection.Input;
            ParamDescriptionProbleme.Value = DescriptionProbleme;
            ParamDescriptionProbleme.ParameterName = "DescriptionProbleme";
            accCmd.Parameters.Add(ParamDescriptionProbleme);

            DbParameter ParamGarantie = accCmd.CreateParameter();
            ParamGarantie.DbType = DbType.String;
            ParamGarantie.Direction = ParameterDirection.Input;
            ParamGarantie.Value = Garantie;
            ParamGarantie.ParameterName = "Garantie";
            accCmd.Parameters.Add(ParamGarantie);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public EntityClient GetClient(string ClientId)
        {
            EntityClient Client = new EntityClient();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DIGICODE, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_ID = @ClientId";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = ClientId;
            ParamNumContrat.ParameterName = "ClientId";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Client.Digicode = DataReader["CLT_DIGICODE"].ToString();
                    Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                    Client.ClientID = DataReader["PSN_ID"].ToString();
                    Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                    Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                    Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                    Client.Email = DataReader["CLT_EMAIL"].ToString();
                    Client.Nom = DataReader["PSN_NOM"].ToString();
                    Client.Pays = DataReader["PSN_PAYS"].ToString();
                    Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                    Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                    Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                    Client.Ville = DataReader["PSN_VILLE"].ToString();
                }
            }

            finally
            {
                conn.Close();
            }
            return Client;
        }

        public string GetClientID(string NumContrat)
        {
            string Client;
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID FROM dbo.CONTRAT WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                Client = accCmd.ExecuteScalar().ToString();

            }
            finally
            {
                conn.Close();
            }

            return Client;
        }

        public List<string> GetAllNumContrat()
        {
            List<string> NumContrats = new List<string>();
            string NumContrat;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT FROM dbo.CONTRAT;";

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();

                        NumContrats.Add(NumContrat);
                    }
                }

            }
            finally
            {
                conn.Close();
            }

            return NumContrats;
        }

        public void UpdateIntervention(DateTime DateStart, string NumContrat, DateTime DateDebut, DateTime DateFin, string DescriptionProbleme, string Garantie, string MaterielId)
        {
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET CT_NUM_CONTRAT = @NumContrat, ITV_DATE_DEBUT = @DateDebut, ITV_DATE_FIN = @DateEnd, ITV_DESCRIPTION_PROBLEME= @DescriptionProbleme, ITV_GARANTIE = @Garantie, ITV_MATERIELID = @MaterielId  WHERE ITV_DATE_DEBUT = @DateStart;";

            #region PARAMETRES
            DbParameter ParamDateStart = accCmd.CreateParameter();
            ParamDateStart.DbType = DbType.DateTime;
            ParamDateStart.Direction = ParameterDirection.Input;
            ParamDateStart.Value = DateStart;
            ParamDateStart.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamDateStart);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDescriptionProbleme = accCmd.CreateParameter();
            ParamDescriptionProbleme.DbType = DbType.String;
            ParamDescriptionProbleme.Direction = ParameterDirection.Input;
            ParamDescriptionProbleme.Value = DescriptionProbleme;
            ParamDescriptionProbleme.ParameterName = "DescriptionProbleme";
            accCmd.Parameters.Add(ParamDescriptionProbleme);

            DbParameter ParamDateEnd = accCmd.CreateParameter();
            ParamDateEnd.DbType = DbType.DateTime;
            ParamDateEnd.Direction = ParameterDirection.Input;
            ParamDateEnd.Value = DateFin;
            ParamDateEnd.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateEnd);

            DbParameter ParamGarantie = accCmd.CreateParameter();
            ParamGarantie.DbType = DbType.String;
            ParamGarantie.Direction = ParameterDirection.Input;
            ParamGarantie.Value = Garantie;
            ParamGarantie.ParameterName = "Garantie";
            accCmd.Parameters.Add(ParamGarantie);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void UpdateInterventionTechnicien(DateTime DateStart, DateTime DateFin, string CommentaireTechnicien, decimal KMParcouru)
        {
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET ITV_DATE_FIN = @DateEnd, ITV_COMMENTAIRE = @CommentaireTechnicien, ITV_KM_PARCOURU = @KMParcouru WHERE ITV_DATE_DEBUT = @DateStart;";

            #region PARAMETRES
            DbParameter ParamDateStart = accCmd.CreateParameter();
            ParamDateStart.DbType = DbType.DateTime;
            ParamDateStart.Direction = ParameterDirection.Input;
            ParamDateStart.Value = DateStart;
            ParamDateStart.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamDateStart);

            DbParameter ParamCommentaireTechnicien = accCmd.CreateParameter();
            ParamCommentaireTechnicien.DbType = DbType.String;
            ParamCommentaireTechnicien.Direction = ParameterDirection.Input;
            ParamCommentaireTechnicien.Value = CommentaireTechnicien;
            ParamCommentaireTechnicien.ParameterName = "CommentaireTechnicien";
            accCmd.Parameters.Add(ParamCommentaireTechnicien);

            DbParameter ParamDateEnd = accCmd.CreateParameter();
            ParamDateEnd.DbType = DbType.DateTime;
            ParamDateEnd.Direction = ParameterDirection.Input;
            ParamDateEnd.Value = DateFin;
            ParamDateEnd.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateEnd);

            DbParameter ParamKMParcouru = accCmd.CreateParameter();
            ParamKMParcouru.DbType = DbType.Decimal;
            ParamKMParcouru.Direction = ParameterDirection.Input;
            ParamKMParcouru.Value = KMParcouru;
            ParamKMParcouru.ParameterName = "KMParcouru";
            accCmd.Parameters.Add(ParamKMParcouru);

            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public string GetInterventionPieceId()
        {
            string InterventionPieceId;

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT TOP 1 ITVP_ID FROM dbo.INTERVENTION_PIECE ORDER BY ITVP_ID DESC;";

            try
            {
                conn.Open();
                InterventionPieceId = accCmd.ExecuteScalar().ToString();
            }
            finally
            {
                conn.Close();
            }

            return InterventionPieceId;
        }

        public void InsertInterventionPiece(string InterventionId, int Quantite, string Description, decimal PrixUnitaire, decimal MOPC, decimal Montant)
        {
            string InterventionPieceId = (Convert.ToInt32(GetInterventionPieceId()) + 1).ToString();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"INSERT INTO dbo.INTERVENTION_PIECE VALUES(@InterventionPieceId, @InterventionId, 
                                                        @Quantite, @Description, @PrixUnitaire, @MOPC, @Montant);";

            #region PARAMETRES
            DbParameter ParamInterventionPieceId = accCmd.CreateParameter();
            ParamInterventionPieceId.DbType = DbType.String;
            ParamInterventionPieceId.Direction = ParameterDirection.Input;
            ParamInterventionPieceId.Value = InterventionPieceId;
            ParamInterventionPieceId.ParameterName = "InterventionPieceId";
            accCmd.Parameters.Add(ParamInterventionPieceId);

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "InterventionId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamQuantite = accCmd.CreateParameter();
            ParamQuantite.DbType = DbType.Int32;
            ParamQuantite.Direction = ParameterDirection.Input;
            ParamQuantite.Value = Quantite;
            ParamQuantite.ParameterName = "Quantite";
            accCmd.Parameters.Add(ParamQuantite);

            DbParameter ParamDescription = accCmd.CreateParameter();
            ParamDescription.DbType = DbType.String;
            ParamDescription.Direction = ParameterDirection.Input;
            ParamDescription.Value = Description;
            ParamDescription.ParameterName = "Description";
            accCmd.Parameters.Add(ParamDescription);

            DbParameter ParamPrixUnitaire = accCmd.CreateParameter();
            ParamPrixUnitaire.DbType = DbType.Decimal;
            ParamPrixUnitaire.Direction = ParameterDirection.Input;
            ParamPrixUnitaire.Value = PrixUnitaire;
            ParamPrixUnitaire.ParameterName = "PrixUnitaire";
            accCmd.Parameters.Add(ParamPrixUnitaire);

            DbParameter ParamMOPC = accCmd.CreateParameter();
            ParamMOPC.DbType = DbType.Decimal;
            ParamMOPC.Direction = ParameterDirection.Input;
            ParamMOPC.Value = MOPC;
            ParamMOPC.ParameterName = "MOPC";
            accCmd.Parameters.Add(ParamMOPC);

            DbParameter ParamMontant = accCmd.CreateParameter();
            ParamMontant.DbType = DbType.Decimal;
            ParamMontant.Direction = ParameterDirection.Input;
            ParamMontant.Value = Montant;
            ParamMontant.ParameterName = "Montant";
            accCmd.Parameters.Add(ParamMontant);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void UpdateInterventionPiece(int InterventionPieceId, int Quantite, string Description, decimal PrixUnitaire, decimal MOPC, decimal Montant)
        {
            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION_PIECE SET ITVP_QUANTITE = @Quantite, ITVP_DESCRIPTION = @Description, 
                                        ITVP_PRIX_UNITAIRE = @PrixUnitaire, ITVP_MAIN_OEUVRE_PIECE = @MOPC, 
                                        ITVP_MONTANT = @Montant WHERE ITVP_ID = @InterventionPieceId;";

            #region PARAMETRES

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.Int32;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionPieceId;
            ParamInterventionId.ParameterName = "InterventionPieceId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamQuantite = accCmd.CreateParameter();
            ParamQuantite.DbType = DbType.Int32;
            ParamQuantite.Direction = ParameterDirection.Input;
            ParamQuantite.Value = Quantite;
            ParamQuantite.ParameterName = "Quantite";
            accCmd.Parameters.Add(ParamQuantite);

            DbParameter ParamDescription = accCmd.CreateParameter();
            ParamDescription.DbType = DbType.String;
            ParamDescription.Direction = ParameterDirection.Input;
            ParamDescription.Value = Description;
            ParamDescription.ParameterName = "Description";
            accCmd.Parameters.Add(ParamDescription);

            DbParameter ParamPrixUnitaire = accCmd.CreateParameter();
            ParamPrixUnitaire.DbType = DbType.Decimal;
            ParamPrixUnitaire.Direction = ParameterDirection.Input;
            ParamPrixUnitaire.Value = PrixUnitaire;
            ParamPrixUnitaire.ParameterName = "PrixUnitaire";
            accCmd.Parameters.Add(ParamPrixUnitaire);

            DbParameter ParamMOPC = accCmd.CreateParameter();
            ParamMOPC.DbType = DbType.Decimal;
            ParamMOPC.Direction = ParameterDirection.Input;
            ParamMOPC.Value = MOPC;
            ParamMOPC.ParameterName = "MOPC";
            accCmd.Parameters.Add(ParamMOPC);

            DbParameter ParamMontant = accCmd.CreateParameter();
            ParamMontant.DbType = DbType.Decimal;
            ParamMontant.Direction = ParameterDirection.Input;
            ParamMontant.Value = Montant;
            ParamMontant.ParameterName = "Montant";
            accCmd.Parameters.Add(ParamMontant);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public EntityInterventionPiece GetInterventionPiece(string InterventionId, string MaterielId)
        {
            EntityInterventionPiece entityInterventionPiece = new EntityInterventionPiece();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT IP.ITVP_ID, IP.ITV_ID, IP.ITVP_QUANTITE, IP.ITVP_DESCRIPTION, IP.ITVP_PRIX_UNITAIRE,
            IP.ITVP_MAIN_OEUVRE_PIECE, IP.ITVP_MONTANT FROM dbo.INTERVENTION_PIECE AS IP INNER JOIN dbo.INTERVENTION 
            AS I ON IP.ITV_ID = I.ITV_ID WHERE IP.ITV_ID = @InterventionId AND I.ITV_MATERIELID = @MaterielId;";

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "InterventionId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    {
                        entityInterventionPiece.Description = DataReader["ITVP_DESCRIPTION"].ToString();
                        entityInterventionPiece.InterventionId = DataReader["ITV_ID"].ToString();
                        entityInterventionPiece.InterventionPieceId = Convert.ToInt32(DataReader["ITVP_ID"]);
                        entityInterventionPiece.Montant = Convert.ToDecimal(DataReader["ITVP_MONTANT"]);
                        entityInterventionPiece.MOPC = Convert.ToDecimal(DataReader["ITVP_MAIN_OEUVRE_PIECE"]);
                        entityInterventionPiece.PrixUnitaire = Convert.ToDecimal(DataReader["ITVP_PRIX_UNITAIRE"]);
                        entityInterventionPiece.Quantite = Convert.ToInt32(DataReader["ITVP_QUANTITE"]);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return entityInterventionPiece;
        }

        public List<EntityIntervention> GetALLInterventionByDateDebut(DateTime DateDebut, string technicienId)
        {
            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dbpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ProviderName);
            conn = dbpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["IHM.Properties.Settings.masterConnectionString"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_KM_PARCOURU, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION
                                    WHERE ITV_DATE_DEBUT BETWEEN @DateDebut AND @DateDebut2 AND TEC_PSN_ID = @technicienId;";

            string Debut = DateDebut.Year.ToString() + "-" + DateDebut.Month.ToString().PadLeft(2, '0') + "-" + DateDebut.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            string Fin = DateDebut.Year.ToString() + "-" + DateDebut.Month.ToString().PadLeft(2, '0') + "-" + DateDebut.Day.ToString().PadLeft(2, '0') + " 23:59:59";

            #region PARAMETRES
            DbParameter ParamtechnicienId = accCmd.CreateParameter();
            ParamtechnicienId.DbType = DbType.String;
            ParamtechnicienId.Direction = ParameterDirection.Input;
            ParamtechnicienId.Value = technicienId;
            ParamtechnicienId.ParameterName = "technicienId";
            accCmd.Parameters.Add(ParamtechnicienId);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = Convert.ToDateTime(Debut);
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateDebut2 = accCmd.CreateParameter();
            ParamDateDebut2.DbType = DbType.DateTime;
            ParamDateDebut2.Direction = ParameterDirection.Input;
            ParamDateDebut2.Value = Convert.ToDateTime(Fin);
            ParamDateDebut2.ParameterName = "DateDebut2";
            accCmd.Parameters.Add(ParamDateDebut2);
            #endregion

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {

                        Intervention = new EntityIntervention();

                        Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                        Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                        Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                        Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                        Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                        Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                        Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                        if (!string.IsNullOrEmpty(DataReader["ITV_KM_PARCOURU"].ToString()))
                            Intervention.KMParcouru = Convert.ToDecimal(DataReader["ITV_KM_PARCOURU"]);
                        Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                        Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();

                        Interventions.Add(Intervention);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Interventions;
        }

        #endregion
    }

    
}
