﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dataaccess;
using Metier.Entities;
using System.Text.RegularExpressions;

namespace IHM
{
    public partial class IHMCommercial : Form
    {

        DataAccess DA; 
        EntityClient Client;
        EntityCommercial Commercial;
        EntityMaterielAchete materiel;
        List<string> lstContrats;
        

        public IHMCommercial(string ID)
        {
            InitializeComponent();

            // Empêche la modification des champs sans click sur les boutons modifier
            DisableClientConsulter();

            // Affiche uniquement le panel de recherche client
            AllPanelInvisible();

            DA = new DataAccess();
            Client = new EntityClient();
            Commercial = DA.GetCommercialById(ID);
            List<EntityClient> lstClients = new List<EntityClient>();

            lstClients = DA.GetClients();

            foreach (EntityClient EntityClient in lstClients)
            {
                dgvListClients.Rows.Add(EntityClient.ClientID, EntityClient.Nom, EntityClient.Prenom, EntityClient.Adresse, EntityClient.Ville, EntityClient.CodePostal, EntityClient.Pays, EntityClient.TelDomicile, EntityClient.TelMobile, EntityClient.DistanceAntenne, EntityClient.DureeDeplacement, EntityClient.Email);
            }



        }

        void btn_MouseEnter(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Hover));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseLeave(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseDown(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Click));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseUp(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Hover));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        

                
        #region Général
        private void ChangerPanel(object sender, EventArgs e = null)
        {
            AllPanelInvisible();
            Button btn = (Button)sender;
            switch (btn.Tag.ToString())
            {
                case "cla":
                    pnlClientAjouter.Visible = true;
                    break;
                case "clc":
                    pnlClientConsulter.Visible = true;
                    break;
                case "ma":
                    pnlMaterielAjouter.Visible = true;
                    break;
                case "mc":
                    pnlMaterielConsulter.Visible = true;
                    break;
                case "coc":
                    pnlContratConsulter.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void AllPanelInvisible()
        {
            pnlContratConsulterRecherche.Visible = false;
            pnlClientAjouter.Visible = false;
            pnlClientConsulter.Visible = false;
            pnlClientRechercher.Visible = false;
            pnlContratConsulter.Visible = false;
            pnlMaterielAjouter.Visible = false;
            pnlMaterielConsulter.Visible = false;
        }

        private void RechercheKeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            switch (tb.Name)
            {
                case "tbClientRechercheID":
                    if (e.KeyChar == '\r')
                    {
                        Recherche(btnClientRechercheID, (EventArgs)e);
                    }
                    if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar))
                        e.Handled = false;
                    else
                        e.Handled = true;
                    break;

                case "tbClientRechercheContrat":
                    if (e.KeyChar == '\r')
                        Recherche(btnClientRechercheContrat, (EventArgs)e);

                    if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar))
                    {
                        
                        e.Handled = false;
                    }
                    else
                        if (e.KeyChar == '-' && !tb.Text.Contains('-'))
                            e.Handled = false;
                        else
                            e.Handled = true;
                    break;

                case "tbClientRechercheNom":
                    if (e.KeyChar == '\r')
                    {
                    Recherche(btnClientRechercheNom, (EventArgs)e);
                    }

                    break;
            }
            

            
                
        }

        private void Recherche(object sender, EventArgs e)
        {
            dgvListClients.Rows.Clear();
            List<EntityClient> lstClients = new List<EntityClient>(); ;
            Button btn = (Button)sender;
            switch (btn.Name)
            {
                case "btnClientRechercheID":
                    if (tbClientRechercheID.Text != "")
                        lstClients = DA.GetClientsById(tbClientRechercheID.Text);
                    else
                        lstClients = DA.GetClients();

                    if (lstClients.Count != 0)
                        lbClientRechercheID.Visible = false;
                    else
                    {
                        lbClientRechercheID.Visible = true;
                        lbClientRechercheNom.Visible = false;
                        lbClientRechercheContrat.Visible = false;
                    }

                    break;
                case "btnClientRechercheNom":
                    if (tbClientRechercheNom.Text != "")
                        lstClients = DA.GetClientsByName(tbClientRechercheNom.Text);
                    else
                        lstClients = DA.GetClients();

                    if (lstClients.Count != 0)
                        lbClientRechercheNom.Visible = false;
                    else
                    {
                        lbClientRechercheNom.Visible = true;
                        lbClientRechercheID.Visible = false;
                        lbClientRechercheContrat.Visible = false;
                    }

                    break;

                case "btnClientRechercheContrat":
                    if (tbClientRechercheContrat.Text != "")
                        lstClients = DA.GetClientsByContrat(tbClientRechercheContrat.Text);
                    else
                        lstClients = DA.GetClients();

                    if (lstClients.Count != 0)
                        lbClientRechercheContrat.Visible = false;
                    else
                    {
                        lbClientRechercheContrat.Visible = true;
                        lbClientRechercheNom.Visible = false;
                        lbClientRechercheID.Visible = false;
                    }

                    break;
            }

            if (lstClients.Count != 0)
            {
                foreach (EntityClient EntityClient in lstClients)
                {
                    dgvListClients.Rows.Add(EntityClient.ClientID, EntityClient.Nom, EntityClient.Prenom, EntityClient.Adresse, EntityClient.Ville, EntityClient.CodePostal, EntityClient.Pays, EntityClient.TelDomicile, EntityClient.TelMobile, EntityClient.Email);
                }

                AllPanelInvisible();
                pnlClientRechercher.Visible = true;
            }
            else
                pnlClientRechercher.Visible = false;
            
        }

        private void StockageClient(object sender, EventArgs e)
        {
            // Récupération du sender (DatagridView de la recherche)
            DataGridView dgv = (DataGridView)sender;

            // Récupération de l'ID du client
            Client.ClientID = dgv.CurrentRow.Cells[0].Value.ToString();

            // Récupérations des données du client 
            Client = DA.GetClientById(Client.ClientID);

            // Met tout les panel en visible = false
            AllPanelInvisible();

            // Met à jour les panels avec les données du client.
            RefreshPanels();

            // Affiche le panel de consultation des données client
            pnlClientConsulter.Visible = true;
            
            // Active la navigation par le menu
            EnableMenu();
        }

        private void RefreshPanels()
        {
            RefreshClientConsult();
            RefreshContratConsult();
            RefreshMaterielAjout();
            RefreshMaterielConsult();
        }

        private void EnableMenu()
        {
            btnClientConsulter.Enabled = true;
            btnContratConsulter.Enabled = true;
            btnMaterielAjouter.Enabled = true;
            btnMaterielConsulter.Enabled = true;
        }

        private void IHMCommercial_Load(object sender, EventArgs e)
        {
            dgvListClients.Columns[0].Visible = false;

            btnClientConsulter.Enabled = false;
            btnContratConsulter.Enabled = false;
            btnMaterielAjouter.Enabled = false;
            btnMaterielConsulter.Enabled = false;

        }

        private void Email_Validating(object sender, CancelEventArgs e)
        {
            TextBox tb = (TextBox)(sender);
            Regex Reg = new Regex(@"^([a-zA-Z0-9]+(([\.\-_]?[a-zA-Z0-9]+)+)?)\@(([a-zA-Z0-9]+[\.\-_])+[a-zA-Z]{2,4})$");

            if (!Reg.IsMatch(tb.Text)) errorProvider1.SetError(tb, "E-mail incorrect");
        }

        private void Email_Validated(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)(sender);
            Regex Reg = new Regex(@"^([a-zA-Z0-9]+(([\.\-_]?[a-zA-Z0-9]+)+)?)\@(([a-zA-Z0-9]+[\.\-_])+[a-zA-Z]{2,4})$");

            if (Reg.IsMatch(tb.Text)) errorProvider1.SetError(tb, "");
        }
        #endregion





        #region Client
        private void DisableClientConsulter()
        {
            txtClientConsulterAdresse.Enabled = false;
            txtClientConsulterCodePostal.Enabled = false;
            txtClientConsulterDeplacement.Enabled = false;
            txtClientConsulterDistance.Enabled = false;
            txtClientConsulterEmail.Enabled = false;
            txtClientConsulterNom.Enabled = false;
            txtClientConsulterPays.Enabled = false;
            txtClientConsulterPrenom.Enabled = false;
            txtClientConsulterTelDom.Enabled = false;
            txtClientConsulterTelMob.Enabled = false;
            txtClientConsulterVille.Enabled = false;
        }

        private void btnClientAjouterAjout_Click(object sender, EventArgs e)
        {
            Regex RegMail = new Regex(@"^([a-zA-Z0-9]+(([\.\-_]?[a-zA-Z0-9]+)+)?)\@(([a-zA-Z0-9]+[\.\-_])+[a-zA-Z]{2,4})$");
            Regex RegCP = new Regex(@"^[0-9]{5}$");
            bool bErreur = false;

            if (txtClientAjouterNom.Text != "")
                errorProvider1.SetError(txtClientAjouterNom, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterNom, "Nom incorrect");
            }

            if (txtClientAjouterPrenom.Text != "")
                errorProvider1.SetError(txtClientAjouterPrenom, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterPrenom,"Prenom incorrect");
            }

            if (txtClientAjouterTelDomicile.Text != "")
                errorProvider1.SetError(txtClientAjouterTelDomicile, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterTelDomicile, "Numéro incorrect");
            }

            if (txtClientAjouterTelMobile.Text != "")
                errorProvider1.SetError(txtClientAjouterTelMobile, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterTelMobile, "Numéro incorrect");
            }

            if (RegMail.IsMatch(txtClientAjouterEmail.Text))
                errorProvider1.SetError(txtClientAjouterEmail, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterEmail, "E-mail incorrect");
            }

            if (txtClientAjouterAdresse.Text != "")
                errorProvider1.SetError(txtClientAjouterAdresse, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterAdresse, "Adresse incorrect");
            }

            if (txtClientAjouterVille.Text != "")
                errorProvider1.SetError(txtClientAjouterVille, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterVille, "Ville incorrect");
            }

            if(RegCP.IsMatch(txtClientAjouterCodePostal.Text))
                errorProvider1.SetError(txtClientAjouterCodePostal, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterCodePostal, "Code postal incorrect");
            }

            if (txtClientAjouterPays.Text != "")
                errorProvider1.SetError(txtClientAjouterPays, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterPays, "Pays incorrect");
            }

            if (txtClientAjouterDistance.Text != "")
                errorProvider1.SetError(txtClientAjouterDistance, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterDistance, "Distance incorrect");
            }

            if (txtClientAjouterDeplacement.Text != "")
                errorProvider1.SetError(txtClientAjouterDeplacement, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtClientAjouterDeplacement, "Durée incorrect");
            }

            if (!bErreur)
            {
                EntityClient cli = new EntityClient();
                cli.Nom = txtClientAjouterNom.Text;
                cli.Prenom = txtClientAjouterPrenom.Text;
                cli.TelDomicile = txtClientAjouterTelDomicile.Text;
                cli.TelMobile = txtClientAjouterTelMobile.Text;
                cli.Email = txtClientAjouterEmail.Text;
                cli.Adresse = txtClientAjouterAdresse.Text;
                cli.Ville = txtClientAjouterVille.Text;
                cli.CodePostal = txtClientAjouterCodePostal.Text;
                cli.Pays = txtClientAjouterPays.Text;
                cli.DistanceAntenne = txtClientAjouterDistance.Text;
                cli.DureeDeplacement = txtClientAjouterDeplacement.Text;
                string[] Id_pwd_mail = DA.InsertClient(cli);
                if (Id_pwd_mail[0] != "Erreur")
                {
                    MessageBox.Show("Client Ajouté. son adresse login est :" + Id_pwd_mail[2] + "\r\n et son mot de passe est : " + Id_pwd_mail[1]);
                    Client = cli;
                    Client.ClientID = Id_pwd_mail[0];
                }
                else
                    MessageBox.Show("Une erreur est survenu pendant l'ajout du client, veuillez consulter votre administrateur réseau\r\n" + Id_pwd_mail[1]);
            
                RefreshPanels();
            }
                                                
            
            
        }

        private void RefreshClientConsult()
        {
            txtClientConsulterAdresse.Text = Client.Adresse;
            txtClientConsulterCodePostal.Text = Client.CodePostal;
            txtClientConsulterDeplacement.Text = Client.DureeDeplacement;
            txtClientConsulterDistance.Text = Client.DistanceAntenne;
            txtClientConsulterEmail.Text = Client.Email;
            txtClientConsulterNom.Text = Client.Nom;
            txtClientConsulterPays.Text = Client.Pays;
            txtClientConsulterPrenom.Text = Client.Prenom;
            txtClientConsulterTelDom.Text = Client.TelDomicile;
            txtClientConsulterTelMob.Text = Client.TelMobile;
            txtClientConsulterVille.Text = Client.Ville;
        }

        private void Tel_Validating(object sender, CancelEventArgs e)
        {
            MaskedTextBox mtb = (MaskedTextBox)sender;
            if (!mtb.MaskCompleted) errorProvider1.SetError(mtb, "format incorrect");
        }


        private void Tel_Validated(object sender, EventArgs e)
        {
            MaskedTextBox mtb = (MaskedTextBox)sender;
            if (mtb.MaskCompleted) errorProvider1.SetError(mtb, "");
        }

        private void mtbClientAjouterTelDomicile_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnClientConsulterModifier_Click(object sender, EventArgs e)
        {
            txtClientConsulterAdresse.Enabled = true;
            txtClientConsulterCodePostal.Enabled = true;
            txtClientConsulterDeplacement.Enabled = true;
            txtClientConsulterDistance.Enabled = true;
            txtClientConsulterEmail.Enabled = true;
            txtClientConsulterNom.Enabled = true;
            txtClientConsulterPays.Enabled = true;
            txtClientConsulterPrenom.Enabled = true;
            txtClientConsulterTelDom.Enabled = true;
            txtClientConsulterTelMob.Enabled = true;
            txtClientConsulterVille.Enabled = true;

            btnClientConsulterEnregistrer.Visible = true;
            btnClientConsulterModifier.Visible = false;
            btnClientConsulterAnnuler.Visible = true;
        }

        private void btnClientConsulterEnregistrer_Click(object sender, EventArgs e)
        {
            Client.Adresse = txtClientConsulterAdresse.Text;
            Client.CodePostal = txtClientConsulterCodePostal.Text;
            Client.DureeDeplacement = txtClientConsulterDeplacement.Text;
            Client.DistanceAntenne = txtClientConsulterDistance.Text;
            Client.Email = txtClientConsulterEmail.Text;
            Client.Nom = txtClientConsulterNom.Text;
            Client.Pays = txtClientConsulterPays.Text;
            Client.Prenom = txtClientConsulterPrenom.Text;
            Client.TelDomicile = txtClientConsulterTelDom.Text;
            Client.TelMobile = txtClientConsulterTelMob.Text;
            Client.Ville = txtClientConsulterVille.Text;

            if (DA.UpdateClient(Client))
            {
                MessageBox.Show("Données client mises à jour");
                DisableClientConsulter();

                btnClientConsulterAnnuler.Visible = false;
                btnClientConsulterEnregistrer.Visible = false;
                btnClientConsulterModifier.Visible = true;
            }
            else
                MessageBox.Show("Une erreur est survenu pendant la mise à jour, veuillez consulter votre administrateur réseau");
            
        }

        private void btnClientConsulterAnnuler_Click(object sender, EventArgs e)
        {
            RefreshClientConsult();
            DisableClientConsulter();

            btnClientConsulterAnnuler.Visible = false;
            btnClientConsulterEnregistrer.Visible = false;
            btnClientConsulterModifier.Visible = true;

        }
        #endregion



        #region Contrat
        private void cbContratConsulterRecherche_SelectedIndexChanged(object sender, EventArgs e)
        {
            EntityContrat Contrat = DA.GetContratByNumContrat(cbContratConsulterRecherche.SelectedItem.ToString());
            EntityCommercial Commercial = DA.GetCommercialById(Contrat.CommercialID);
            txtContratConsulterClient.Text = Client.Nom + " " + Client.Prenom;
            txtContratConsulterCommercial.Text = Commercial.Nom + " " + Commercial.Prenom;
            txtContratConsulterEcheance.Text = Contrat.DateEcheance.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(Contrat.DateRenouvellement.ToString()))
                txtContratConsulterRenouvellement.Text = DateTime.Parse(Contrat.DateRenouvellement.ToString()).ToString("dd/MM/yyyy");
            else
                txtContratConsulterRenouvellement.Text = "";

            txtContratConsulterSignature.Text = Contrat.DateSignatureContrat.ToString("dd/MM/yyyy");
            txtContratConsulterPrix.Text = Contrat.Prix.ToString();
            txtContratConsulterType.Text = Contrat.TypeContrat.ToString();
         

            pnlContratConsulterRecherche.Visible = true;

        }

        private void RefreshContratConsult()
        {
            RefreshNumContrat();
        }


        private void RefreshNumContrat()
        {
            cbContratConsulterRecherche.Items.Clear();
            cbContratConsulterRecherche.Text = "";

            lstContrats = DA.GetContratsByClientID(Client.ClientID);

            if (lstContrats.Count != 0)
            {
                foreach (string Contrat in lstContrats)
                {
                    cbContratConsulterRecherche.Items.Add(Contrat);
                }
            }
        }

        private void btnContratConsulterModifier_Click(object sender, EventArgs e)
        {
            txtContratConsulterEcheance.Enabled = true;
            txtContratConsulterRenouvellement.Enabled = true;
            txtContratConsulterSignature.Enabled = true;
            txtContratConsulterPrix.Enabled = true;
        }

        private void btnContratConsulterEnregistrer_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Êtes vous sûr de vouloir prolonger le contrat d'un an ?", "WARNING", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                return;

            EntityContrat Contrat = DA.GetContratByNumContrat(cbContratConsulterRecherche.SelectedItem.ToString());

            if (Contrat.DateEcheance <= DateTime.Now)
            {
                Contrat.DateRenouvellement = DateTime.Now;
                Contrat.DateEcheance = DateTime.Parse(Contrat.DateRenouvellement.ToString()).AddYears(1);
            }
            else
            {
                Contrat.DateRenouvellement = Contrat.DateEcheance;
                Contrat.DateEcheance = Contrat.DateEcheance.AddYears(1);
            }

            if (DA.UpdateContrat(Contrat))
            {
                MessageBox.Show("Le contrat à été prolongé d'un an");
                cbContratConsulterRecherche_SelectedIndexChanged(sender, e);
            }
            else
                MessageBox.Show("Une erreur est survenu pendant la mise à jour, veuillez consulter votre administrateur réseau");
            
        } 
        #endregion

        private void RefreshMaterielAjout()
        {
            cbMaterielAjouterType.Items.Clear();
            cbMaterielAjouterType.Text = "";

            List<string> types = DA.GetMaterielTypes();

            if (types.Count != 0)
            {
                foreach (string type in types)
                {
                    cbMaterielAjouterType.Items.Add(type);
                }
            }

            cbMaterielAjouterContrat.Enabled = false;
            cbMaterielAjouterContrat.Items.Clear();
            cbMaterielAjouterModele.Enabled = false;
            cbMaterielAjouterModele.Items.Clear();
            gbMaterielAjouterContratCarac.Visible = false;
        }

        private void RefreshMaterielConsult()
        {
            cbMaterielConsulterRecherche.SelectedIndex = 0;
            cbMaterielConsulterDgvModifModele.Items.Clear();
            cbMaterielConsulterDgvModifType.Items.Clear();
            foreach (string type in DA.GetMaterielTypes())
	        {
                cbMaterielConsulterDgvModifType.Items.Add(type);
	        }
        }

        private void cbMaterielAjouterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbMaterielAjouterType, "");
            cbMaterielAjouterModele.Items.Clear();
            cbMaterielAjouterModele.Text = "";

            List<string> Models = DA.GetMaterielModels(cbMaterielAjouterType.SelectedItem.ToString());

            if (Models.Count != 0)
            {
                foreach (string Model in Models)
                {
                    cbMaterielAjouterModele.Items.Add(Model);
                }

                cbMaterielAjouterModele.Enabled = true;
                cbMaterielAjouterContrat.Enabled = false;
                gbMaterielAjouterContratCarac.Visible = false;
            }


        }

        private void cbMaterielAjouterModele_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbMaterielAjouterModele, "");
            cbMaterielAjouterContrat.Items.Clear();
            cbMaterielAjouterContrat.Text = "";

            List<string> Contrats = DA.GetContratsByClientIDToday(Client.ClientID);

            if (Contrats.Count != 0)
            {
                foreach (string Contrat in Contrats)
                {
                    cbMaterielAjouterContrat.Items.Add(Contrat);
                }
            }
            cbMaterielAjouterContrat.Items.Add("Nouveau contrat");

            cbMaterielAjouterContrat.Enabled = true;
            gbMaterielAjouterContratCarac.Visible = false;
        }

        private void cbMaterielAjouterContrat_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cbMaterielAjouterContrat, "");
            if (cbMaterielAjouterContrat.SelectedItem.ToString() == "Nouveau contrat")
            {
                IHMCommercialAjoutContrat form = new IHMCommercialAjoutContrat(Commercial.ID,Client.ClientID);
                if (form.ShowDialog(this) != DialogResult.OK)
                    return;

                cbMaterielAjouterContrat.Items.Clear();
                cbMaterielAjouterContrat.Text = "";

                List<string> Contrats = DA.GetContratsByClientIDToday(Client.ClientID);

                if (Contrats.Count != 0)
                {
                    foreach (string Contrat in Contrats)
                    {
                        cbMaterielAjouterContrat.Items.Add(Contrat);
                    }

                    cbMaterielAjouterContrat.Items.Add("Nouveau contrat");

                    cbMaterielAjouterContrat.Text = DA.GetLastNumContrat();

                }
            }

            EntityContrat Ctr = DA.GetContratByNumContrat(cbMaterielAjouterContrat.Text);
            txtMaterielAjouterContratType.Text = Ctr.TypeContrat.ToString();
            txtMaterielAjouterContratEcheance.Text = Ctr.DateEcheance.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(Ctr.DateRenouvellement.ToString()))
                txtMaterielAjouterContratRenouvellement.Text = DateTime.Parse(Ctr.DateRenouvellement.ToString()).ToString("dd/MM/yyyy");
            else
                txtMaterielAjouterContratRenouvellement.Text = "";

            txtMaterielAjouterContratSignature.Text = Ctr.DateSignatureContrat.ToString("dd/MM/yyyy");
            txtMaterielAjouterContratPrixOld.Text = Ctr.Prix.ToString();
            Decimal NewPrix = Ctr.Prix + ((DA.GetMaterielPrix(cbMaterielAjouterType.SelectedItem.ToString(), cbMaterielAjouterModele.SelectedItem.ToString()) / 100) * Ctr.Taux);
            txtMaterielAjouterContratPrixNew.Text = NewPrix.ToString();

            gbMaterielAjouterContratCarac.Visible = true;

        }

        private void btnMaterielAjouterAjout_Click(object sender, EventArgs e)
        {
            bool bErreur = false;

            if (txtMaterielAjouterSérie.Text != "")
                errorProvider1.SetError(txtMaterielAjouterSérie, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(txtMaterielAjouterSérie, "Numéro de série incorrect");
            }

            if (cbMaterielAjouterType.SelectedIndex != -1)
                errorProvider1.SetError(cbMaterielAjouterType, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(cbMaterielAjouterType, "Type incorrect");
            }

            if (cbMaterielAjouterModele.SelectedIndex != -1)
                errorProvider1.SetError(cbMaterielAjouterModele, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(cbMaterielAjouterModele, "Modele incorrect");
            }

            if (cbMaterielAjouterContrat.SelectedIndex != -1)
                errorProvider1.SetError(cbMaterielAjouterContrat, "");
            else
            {
                bErreur = true;
                errorProvider1.SetError(cbMaterielAjouterContrat, "Numéro de contrat incorrect");
            }

            if (!bErreur)
            {
                EntityMaterielAchete materiel = new EntityMaterielAchete();
                materiel.MaterielId = txtMaterielAjouterSérie.Text;
                materiel.Reference = DA.GetMaterielRef(cbMaterielAjouterType.SelectedItem.ToString(), cbMaterielAjouterModele.SelectedItem.ToString());
                materiel.Contratlie = cbMaterielAjouterContrat.SelectedItem.ToString();
                materiel.PrixAchat = DA.GetMaterielPrix(cbMaterielAjouterType.SelectedItem.ToString(), cbMaterielAjouterModele.SelectedItem.ToString());
                string result = DA.InsertMateriel(materiel);
                if (result == "OK")
                {
                    MessageBox.Show("Materiel ajouté");
                }
                else
                    MessageBox.Show(result);
            }
        }

        private void cbMaterielConsulterRecherche_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox) sender;
            switch (cb.SelectedItem.ToString())
            {
                case "N° de série":
                    MaterielConsulterSerie();
                    break;
                case "N° de contrat":
                    MaterielConsulterContrat();
                    break;
                case "Date d'installation":
                    MaterielConsulterInstallation();
                    break;
            }

        }

        private void MaterielConsulterInstallation()
        {
            pnlMaterielConsulterSerie.Visible = false;
            pnlMaterielConsulterContrat.Visible = false;
            pnlMaterielConsulterInstallation.Visible = true;

            cbMaterielConsulterInstallation.SelectedIndex = 0;
        }

        private void MaterielConsulterContrat()
        {
            pnlMaterielConsulterSerie.Visible = false;
            pnlMaterielConsulterContrat.Visible = true;
            pnlMaterielConsulterInstallation.Visible = false;

            cbMaterielConsulterContrat.Items.Clear();

            List<string> lstContrat = DA.GetContratsByClientID(Client.ClientID);

            foreach (string contrat in lstContrat)
            {
                cbMaterielConsulterContrat.Items.Add(contrat);
            }
        }

        private void MaterielConsulterSerie()
        {
            pnlMaterielConsulterSerie.Visible = true;
            pnlMaterielConsulterContrat.Visible = false;
            pnlMaterielConsulterInstallation.Visible = false;

            cbMaterielConsulterSerie.Items.Clear();

            List<string> lstMateriel = DA.GetMateriels(Client.ClientID);

            foreach (string materiel in lstMateriel)
            {
                cbMaterielConsulterSerie.Items.Add(materiel); 
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbMaterielConsulterInstallation.SelectedItem.ToString())
            {
                case "Installé le":
                case "Installé depuis le":
                case "Installer jusqu'au":
                    pnlMaterielConsulterInstallationDate.Visible = false;
                    pnlMaterielConsulterInstallationSansDate.Visible = true;
                    break;
                case "Installé du":
                    pnlMaterielConsulterInstallationDate.Visible = true;
                    pnlMaterielConsulterInstallationSansDate.Visible = false;
                    break;
            }
        }

        private void MaterielConsulterRechercheByDate(object sender, EventArgs e)
        {
            List<EntityMaterielAchete> lstMateriel;
            pnlMaterielConsulterDgvModif.Visible = false;
            switch (cbMaterielConsulterInstallation.SelectedItem.ToString())
            {
                case "Installé le": 
                    lstMateriel = DA.GetMaterielInstallation(DateTime.Parse(dtMaterielConsulterDeb.Text), 1, Client.ClientID);
                    RefreshdgvMaterielConsulter(lstMateriel);
                    break;
                case "Installé depuis le":
                    lstMateriel = DA.GetMaterielInstallation(DateTime.Parse(dtMaterielConsulterDeb.Text), 2, Client.ClientID);
                    RefreshdgvMaterielConsulter(lstMateriel);
                    break;
                case "Installer jusqu'au":
                    lstMateriel = DA.GetMaterielInstallation(DateTime.Parse(dtMaterielConsulterDeb.Text), 3, Client.ClientID);
                    RefreshdgvMaterielConsulter(lstMateriel);
                    break;
                case "Installé du":
                    lstMateriel = DA.GetMaterielInstallation(DateTime.Parse(dtMaterielConsulterDeb.Text), DateTime.Parse(dtMaterielConsulterFin.Text), Client.ClientID);
                    RefreshdgvMaterielConsulter(lstMateriel);
                    break;
            }
        }

        private void cbMaterielConsulterContrat_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<EntityMaterielAchete> lstMateriel = DA.GetMaterielContrat(cbMaterielConsulterContrat.SelectedItem.ToString());

            RefreshdgvMaterielConsulter(lstMateriel);
            pnlMaterielConsulterDgvModif.Visible = false;
        }

        private void cbMaterielConsulterSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            EntityMaterielAchete materiel = DA.GetMaterielSerie(cbMaterielConsulterSerie.SelectedItem.ToString());
            dgvMaterielConsulter.Rows.Clear();
            pnlMaterielConsulterDgvModif.Visible = false; ;

            dgvMaterielConsulter.Rows.Add(materiel.MaterielId, materiel.Contratlie, DA.GetMaterielType(materiel.Reference), DA.GetMaterielModel(materiel.Reference), materiel.DateInstallation, materiel.PrixAchat);
            
        }

        private void RefreshdgvMaterielConsulter(List<EntityMaterielAchete> lstMateriel)
        {
            dgvMaterielConsulter.Rows.Clear();

            foreach (EntityMaterielAchete materiel in lstMateriel)
            {
                dgvMaterielConsulter.Rows.Add(materiel.MaterielId, materiel.Contratlie, DA.GetMaterielType(materiel.Reference), DA.GetMaterielModel(materiel.Reference), materiel.DateInstallation, materiel.PrixAchat);
            }
        }

        

        private void dgvMaterielConsulter_DoubleClick(object sender, EventArgs e)
        {
            materiel = new EntityMaterielAchete();
            pnlMaterielConsulterDgvModif.Visible = true;

            materiel.MaterielId = dgvMaterielConsulter.CurrentRow.Cells[0].Value.ToString();
            cbMaterielConsulterDgvModifType.SelectedItem = dgvMaterielConsulter.CurrentRow.Cells[2].Value.ToString();
            cbMaterielConsulterDgvModifModele.Items.Clear();
            foreach (string modele in DA.GetMaterielModels(dgvMaterielConsulter.CurrentRow.Cells[2].Value.ToString()))
	        {
                cbMaterielConsulterDgvModifModele.Items.Add(modele);
	        }
            cbMaterielConsulterDgvModifModele.SelectedItem = dgvMaterielConsulter.CurrentRow.Cells[3].Value.ToString();

            dtMaterielConsulterDgvModifInstallation.Text = dgvMaterielConsulter.CurrentRow.Cells[4].Value.ToString();
        }

        

        private void cbMaterielConsulterDgvModifType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbMaterielConsulterDgvModifModele.Items.Clear();
            foreach (string modele in DA.GetMaterielModels(cbMaterielConsulterDgvModifType.Text))
            {
                cbMaterielConsulterDgvModifModele.Items.Add(modele);
            }

            cbMaterielConsulterDgvModifModele.SelectedIndex = 0;
        }

        private void btnMaterielConsulterDgvModifSave_Click(object sender, EventArgs e)
        {
            
            materiel.Reference = DA.GetMaterielRef(cbMaterielConsulterDgvModifType.Text, cbMaterielConsulterDgvModifModele.Text);
            materiel.DateInstallation = DateTime.Parse(dtMaterielConsulterDgvModifInstallation.Text);
            materiel.PrixAchat = DA.GetMaterielPrix(cbMaterielConsulterDgvModifType.Text, cbMaterielConsulterDgvModifModele.Text);



            if (DA.UpdateMateriel(materiel))
            {
                MessageBox.Show("Données materiel mises à jour");
                pnlMaterielConsulterDgvModif.Visible = false;

                switch (cbMaterielConsulterRecherche.Text)
                {
                    case "N° de série":
                        cbMaterielConsulterSerie_SelectedIndexChanged(sender, e);
                        break;
                    case "N° de contrat":
                        cbMaterielConsulterContrat_SelectedIndexChanged(sender, e);
                        break;
                    case "Date d'installation":
                        MaterielConsulterRechercheByDate(sender, e);
                        break;
                }
            }
            else
                MessageBox.Show("Une erreur est survenu pendant la mise à jour, veuillez consulter votre administrateur réseau");
            

        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtClientAjouterDeplacement_Validating(object sender, CancelEventArgs e)
        {
            TextBox tb = (TextBox) sender;
            if (tb.Text != "")
            {
                errorProvider1.SetError(tb, "");
                if (Int32.Parse(tb.Text) > 120)
                    if (MessageBox.Show(this, "La durée de déplacement dépasse les 2h. Confirmez-vous cette durée ?", "Attention", MessageBoxButtons.YesNo) == DialogResult.No)
                        tb.Text = "";
            }
            else
                errorProvider1.SetError(tb, "Durée (en minutes) incorrect");
        }


        private void TextBox_Validating(object sender, CancelEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text != "") 
                errorProvider1.SetError(tb, "");
            else
            {
                switch(tb.Name)
                {
                    case "txtClientAjouterNom":
                    case "txtClientConsulterNom":
                        errorProvider1.SetError(tb, "Nom incorrect");
                        break;
                        
                    case "txtClientAjouterTelDom":
                    case "txtClientConsulterTelDom":
                        errorProvider1.SetError(tb, "Telephone incorrect");
                        break;

                    case "txtClientAjouterTelMob":
                    case "txtClientConsulterTelMob":
                        errorProvider1.SetError(tb, "Telephone incorrect");
                        break;

                    case "txtClientAjouterPrenom":
                    case "txtClientConsulterPrenom":
                        errorProvider1.SetError(tb, "Prenom incorrect");
                        break;

                    case "txtClientAjouterAdresse":
                    case "txtClientConsulterAdresse":
                        errorProvider1.SetError(tb, "Adresse incorrect");
                        break;

                    case "txtClientAjouterVille":
                    case "txtClientConsulterVille":
                        errorProvider1.SetError(tb, "Ville incorrect");
                        break;

                    case "txtClientAjouterCodePostal":
                    case "txtClientConsulterCodePostal":
                        errorProvider1.SetError(tb, "Code postal incorrect");
                        break;

                    case "txtClientAjouterPays":
                    case "txtClientConsulterPays":
                        errorProvider1.SetError(tb, "Pays incorrect");
                        break;

                    case "txtMaterielAjouterSérie":
                        errorProvider1.SetError(tb, "Numéro de série incorrect");
                        break;
                }
            }
        }

        private void Numeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (e.KeyChar == ',' && !tb.Text.Contains(','))
                    e.Handled = false;
                else
                    e.Handled = true;
        }

        private void OnlyNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void Serie_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar))
            {

                e.Handled = false;
            }
            else
                if (e.KeyChar == '-' && !tb.Text.Contains('-'))
                    e.Handled = false;
                else
                    e.Handled = true;
        }

       

    }

}

        
