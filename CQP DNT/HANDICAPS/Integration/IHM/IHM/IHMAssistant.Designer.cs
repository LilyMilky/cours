﻿namespace IHM
{
    partial class IHMAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IHMAssistant));
            this.pnlAssistantConsulterClient = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.cLIENTDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ville = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodePostal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelDomicile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelMobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DistanceAntenne = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DureeDeplacement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRechercheClient = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAssistantClientNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlAssistantConsulterContrat = new System.Windows.Forms.Panel();
            this.txtNumContra = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnRechercheIDClient = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txtContratIDClient = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnRechercheContrat = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumContrat = new System.Windows.Forms.DataGridView();
            this.CLI_PSN_LOGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumContrat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeContrat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateEcheance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateRenouvellement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateSignatureContrat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlAssistantConsulterMateriel = new System.Windows.Forms.Panel();
            this.txtAssistantConsulterMaterielContratlie = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnAssistantConsulterMaterielValidationRef = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mATERIEL_ACHETEDataGridView = new System.Windows.Forms.DataGridView();
            this.Reference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContratLie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateInstallation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prix_Achat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAssistantConsulterMaterielValidationCONTRAT = new System.Windows.Forms.Button();
            this.txtAssistantConsulterMaterielReference = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlAssistantTechnicien = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.cbAssistantChoixTechnicien = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClient = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnContrat = new System.Windows.Forms.Button();
            this.weekScheduleControl1 = new Syd.ScheduleControls.WeekScheduleControl();
            this.pnlAssistantConsulterClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLIENTDataGridView)).BeginInit();
            this.pnlAssistantConsulterContrat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumContrat)).BeginInit();
            this.pnlAssistantConsulterMateriel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mATERIEL_ACHETEDataGridView)).BeginInit();
            this.pnlAssistantTechnicien.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlAssistantConsulterClient
            // 
            this.pnlAssistantConsulterClient.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAssistantConsulterClient.BackgroundImage")));
            this.pnlAssistantConsulterClient.Controls.Add(this.label12);
            this.pnlAssistantConsulterClient.Controls.Add(this.btnRechercheClient);
            this.pnlAssistantConsulterClient.Controls.Add(this.label7);
            this.pnlAssistantConsulterClient.Controls.Add(this.txtAssistantClientNom);
            this.pnlAssistantConsulterClient.Controls.Add(this.label1);
            this.pnlAssistantConsulterClient.Controls.Add(this.cLIENTDataGridView);
            this.pnlAssistantConsulterClient.Location = new System.Drawing.Point(105, 27);
            this.pnlAssistantConsulterClient.Name = "pnlAssistantConsulterClient";
            this.pnlAssistantConsulterClient.Size = new System.Drawing.Size(982, 552);
            this.pnlAssistantConsulterClient.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "CONSULTER CLIENT";
            // 
            // cLIENTDataGridView
            // 
            this.cLIENTDataGridView.AllowUserToAddRows = false;
            this.cLIENTDataGridView.AllowUserToDeleteRows = false;
            this.cLIENTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cLIENTDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Nom,
            this.Prenom,
            this.Adresse,
            this.Ville,
            this.CodePostal,
            this.Pays,
            this.TelDomicile,
            this.TelMobile,
            this.DistanceAntenne,
            this.DureeDeplacement,
            this.Email});
            this.cLIENTDataGridView.Location = new System.Drawing.Point(10, 111);
            this.cLIENTDataGridView.Name = "cLIENTDataGridView";
            this.cLIENTDataGridView.ReadOnly = true;
            this.cLIENTDataGridView.Size = new System.Drawing.Size(954, 428);
            this.cLIENTDataGridView.TabIndex = 14;
            this.cLIENTDataGridView.Visible = false;
            this.cLIENTDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.cLIENTDataGridView_CellMouseDoubleClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "PSN_ID";
            this.Column1.HeaderText = "ID Client";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Nom";
            this.Nom.Name = "Nom";
            this.Nom.ReadOnly = true;
            // 
            // Prenom
            // 
            this.Prenom.HeaderText = "Prenom";
            this.Prenom.Name = "Prenom";
            this.Prenom.ReadOnly = true;
            // 
            // Adresse
            // 
            this.Adresse.HeaderText = "Adresse";
            this.Adresse.Name = "Adresse";
            this.Adresse.ReadOnly = true;
            // 
            // Ville
            // 
            this.Ville.HeaderText = "Ville";
            this.Ville.Name = "Ville";
            this.Ville.ReadOnly = true;
            // 
            // CodePostal
            // 
            this.CodePostal.HeaderText = "CodePostal";
            this.CodePostal.Name = "CodePostal";
            this.CodePostal.ReadOnly = true;
            // 
            // Pays
            // 
            this.Pays.HeaderText = "Pays";
            this.Pays.Name = "Pays";
            this.Pays.ReadOnly = true;
            // 
            // TelDomicile
            // 
            this.TelDomicile.HeaderText = "TelDomicile";
            this.TelDomicile.Name = "TelDomicile";
            this.TelDomicile.ReadOnly = true;
            // 
            // TelMobile
            // 
            this.TelMobile.HeaderText = "TelMobile";
            this.TelMobile.Name = "TelMobile";
            this.TelMobile.ReadOnly = true;
            // 
            // DistanceAntenne
            // 
            this.DistanceAntenne.HeaderText = "DistanceAntenne";
            this.DistanceAntenne.Name = "DistanceAntenne";
            this.DistanceAntenne.ReadOnly = true;
            // 
            // DureeDeplacement
            // 
            this.DureeDeplacement.HeaderText = "DureeDeplacement";
            this.DureeDeplacement.Name = "DureeDeplacement";
            this.DureeDeplacement.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // btnRechercheClient
            // 
            this.btnRechercheClient.BackColor = System.Drawing.Color.Transparent;
            this.btnRechercheClient.FlatAppearance.BorderSize = 0;
            this.btnRechercheClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRechercheClient.Image = ((System.Drawing.Image)(resources.GetObject("btnRechercheClient.Image")));
            this.btnRechercheClient.Location = new System.Drawing.Point(533, 55);
            this.btnRechercheClient.Name = "btnRechercheClient";
            this.btnRechercheClient.Size = new System.Drawing.Size(31, 34);
            this.btnRechercheClient.TabIndex = 14;
            this.btnRechercheClient.UseVisualStyleBackColor = false;
            this.btnRechercheClient.Click += new System.EventHandler(this.btnRechercheClient_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(579, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "*Aucun nom ne commence par cette valeur";
            this.label7.Visible = false;
            // 
            // txtAssistantClientNom
            // 
            this.txtAssistantClientNom.Location = new System.Drawing.Point(300, 61);
            this.txtAssistantClientNom.Name = "txtAssistantClientNom";
            this.txtAssistantClientNom.Size = new System.Drawing.Size(227, 20);
            this.txtAssistantClientNom.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(259, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nom :";
            // 
            // pnlAssistantConsulterContrat
            // 
            this.pnlAssistantConsulterContrat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAssistantConsulterContrat.BackgroundImage")));
            this.pnlAssistantConsulterContrat.Controls.Add(this.txtNumContra);
            this.pnlAssistantConsulterContrat.Controls.Add(this.label14);
            this.pnlAssistantConsulterContrat.Controls.Add(this.btnRechercheIDClient);
            this.pnlAssistantConsulterContrat.Controls.Add(this.label11);
            this.pnlAssistantConsulterContrat.Controls.Add(this.txtContratIDClient);
            this.pnlAssistantConsulterContrat.Controls.Add(this.label10);
            this.pnlAssistantConsulterContrat.Controls.Add(this.label6);
            this.pnlAssistantConsulterContrat.Controls.Add(this.btnRechercheContrat);
            this.pnlAssistantConsulterContrat.Controls.Add(this.label2);
            this.pnlAssistantConsulterContrat.Controls.Add(this.txtNumContrat);
            this.pnlAssistantConsulterContrat.Location = new System.Drawing.Point(108, 27);
            this.pnlAssistantConsulterContrat.Name = "pnlAssistantConsulterContrat";
            this.pnlAssistantConsulterContrat.Size = new System.Drawing.Size(970, 549);
            this.pnlAssistantConsulterContrat.TabIndex = 10;
            // 
            // txtNumContra
            // 
            this.txtNumContra.Location = new System.Drawing.Point(560, 41);
            this.txtNumContra.Mask = "0000-000000000";
            this.txtNumContra.Name = "txtNumContra";
            this.txtNumContra.Size = new System.Drawing.Size(170, 20);
            this.txtNumContra.TabIndex = 10;
            this.txtNumContra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssistantConsulterMaterielContratlie_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(10, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "CONSULTER CONTRAT";
            // 
            // btnRechercheIDClient
            // 
            this.btnRechercheIDClient.BackColor = System.Drawing.Color.Transparent;
            this.btnRechercheIDClient.FlatAppearance.BorderSize = 0;
            this.btnRechercheIDClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRechercheIDClient.Image = ((System.Drawing.Image)(resources.GetObject("btnRechercheIDClient.Image")));
            this.btnRechercheIDClient.Location = new System.Drawing.Point(439, 40);
            this.btnRechercheIDClient.Name = "btnRechercheIDClient";
            this.btnRechercheIDClient.Size = new System.Drawing.Size(35, 30);
            this.btnRechercheIDClient.TabIndex = 8;
            this.btnRechercheIDClient.UseVisualStyleBackColor = false;
            this.btnRechercheIDClient.Click += new System.EventHandler(this.btnRechercheIDClient_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(294, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "*ID Client Inconnu";
            this.label11.Visible = false;
            // 
            // txtContratIDClient
            // 
            this.txtContratIDClient.Location = new System.Drawing.Point(268, 43);
            this.txtContratIDClient.Name = "txtContratIDClient";
            this.txtContratIDClient.Size = new System.Drawing.Size(165, 20);
            this.txtContratIDClient.TabIndex = 6;
            this.txtContratIDClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssistantConsulterMaterielContratlie_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(199, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "ID Client :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(583, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "*Numero de contrat inconnu";
            this.label6.Visible = false;
            // 
            // btnRechercheContrat
            // 
            this.btnRechercheContrat.BackColor = System.Drawing.Color.Transparent;
            this.btnRechercheContrat.FlatAppearance.BorderSize = 0;
            this.btnRechercheContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRechercheContrat.Image = ((System.Drawing.Image)(resources.GetObject("btnRechercheContrat.Image")));
            this.btnRechercheContrat.Location = new System.Drawing.Point(748, 37);
            this.btnRechercheContrat.Name = "btnRechercheContrat";
            this.btnRechercheContrat.Size = new System.Drawing.Size(32, 30);
            this.btnRechercheContrat.TabIndex = 3;
            this.btnRechercheContrat.UseVisualStyleBackColor = false;
            this.btnRechercheContrat.Click += new System.EventHandler(this.btnRechercheContrat_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(482, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "N° Contrat :";
            // 
            // txtNumContrat
            // 
            this.txtNumContrat.AllowUserToAddRows = false;
            this.txtNumContrat.AllowUserToDeleteRows = false;
            this.txtNumContrat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.txtNumContrat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CLI_PSN_LOGIN,
            this.NumContrat,
            this.TypeContrat,
            this.DateEcheance,
            this.DateRenouvellement,
            this.DateSignatureContrat,
            this.Prix});
            this.txtNumContrat.Location = new System.Drawing.Point(3, 91);
            this.txtNumContrat.Name = "txtNumContrat";
            this.txtNumContrat.ReadOnly = true;
            this.txtNumContrat.Size = new System.Drawing.Size(967, 448);
            this.txtNumContrat.TabIndex = 0;
            this.txtNumContrat.Visible = false;
            this.txtNumContrat.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.cONTRATDataGridView_CellMouseDoubleClick);
            // 
            // CLI_PSN_LOGIN
            // 
            this.CLI_PSN_LOGIN.DataPropertyName = "CLI_PSN_ID";
            this.CLI_PSN_LOGIN.HeaderText = "ID Client";
            this.CLI_PSN_LOGIN.Name = "CLI_PSN_LOGIN";
            this.CLI_PSN_LOGIN.ReadOnly = true;
            // 
            // NumContrat
            // 
            this.NumContrat.HeaderText = "NumContrat";
            this.NumContrat.Name = "NumContrat";
            this.NumContrat.ReadOnly = true;
            // 
            // TypeContrat
            // 
            this.TypeContrat.HeaderText = "TypeContrat";
            this.TypeContrat.Name = "TypeContrat";
            this.TypeContrat.ReadOnly = true;
            // 
            // DateEcheance
            // 
            this.DateEcheance.HeaderText = "DateEcheance";
            this.DateEcheance.Name = "DateEcheance";
            this.DateEcheance.ReadOnly = true;
            // 
            // DateRenouvellement
            // 
            this.DateRenouvellement.HeaderText = "DateRenouvellement";
            this.DateRenouvellement.Name = "DateRenouvellement";
            this.DateRenouvellement.ReadOnly = true;
            // 
            // DateSignatureContrat
            // 
            this.DateSignatureContrat.HeaderText = "DateSignatureContrat";
            this.DateSignatureContrat.Name = "DateSignatureContrat";
            this.DateSignatureContrat.ReadOnly = true;
            // 
            // Prix
            // 
            this.Prix.HeaderText = "Prix";
            this.Prix.Name = "Prix";
            this.Prix.ReadOnly = true;
            // 
            // pnlAssistantConsulterMateriel
            // 
            this.pnlAssistantConsulterMateriel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAssistantConsulterMateriel.BackgroundImage")));
            this.pnlAssistantConsulterMateriel.Controls.Add(this.txtAssistantConsulterMaterielContratlie);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.label13);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.btnAssistantConsulterMaterielValidationRef);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.label9);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.label8);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.mATERIEL_ACHETEDataGridView);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.btnAssistantConsulterMaterielValidationCONTRAT);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.txtAssistantConsulterMaterielReference);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.label4);
            this.pnlAssistantConsulterMateriel.Controls.Add(this.label3);
            this.pnlAssistantConsulterMateriel.Location = new System.Drawing.Point(105, 27);
            this.pnlAssistantConsulterMateriel.Name = "pnlAssistantConsulterMateriel";
            this.pnlAssistantConsulterMateriel.Size = new System.Drawing.Size(982, 552);
            this.pnlAssistantConsulterMateriel.TabIndex = 4;
            // 
            // txtAssistantConsulterMaterielContratlie
            // 
            this.txtAssistantConsulterMaterielContratlie.Location = new System.Drawing.Point(564, 41);
            this.txtAssistantConsulterMaterielContratlie.Mask = "0000-000000000";
            this.txtAssistantConsulterMaterielContratlie.Name = "txtAssistantConsulterMaterielContratlie";
            this.txtAssistantConsulterMaterielContratlie.Size = new System.Drawing.Size(254, 20);
            this.txtAssistantConsulterMaterielContratlie.TabIndex = 12;
            this.txtAssistantConsulterMaterielContratlie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssistantConsulterMaterielContratlie_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(13, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(148, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "CONSULTER MATERIEL";
            // 
            // btnAssistantConsulterMaterielValidationRef
            // 
            this.btnAssistantConsulterMaterielValidationRef.BackColor = System.Drawing.Color.Transparent;
            this.btnAssistantConsulterMaterielValidationRef.FlatAppearance.BorderSize = 0;
            this.btnAssistantConsulterMaterielValidationRef.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssistantConsulterMaterielValidationRef.Image = ((System.Drawing.Image)(resources.GetObject("btnAssistantConsulterMaterielValidationRef.Image")));
            this.btnAssistantConsulterMaterielValidationRef.Location = new System.Drawing.Point(398, 40);
            this.btnAssistantConsulterMaterielValidationRef.Name = "btnAssistantConsulterMaterielValidationRef";
            this.btnAssistantConsulterMaterielValidationRef.Size = new System.Drawing.Size(30, 30);
            this.btnAssistantConsulterMaterielValidationRef.TabIndex = 10;
            this.btnAssistantConsulterMaterielValidationRef.UseVisualStyleBackColor = false;
            this.btnAssistantConsulterMaterielValidationRef.Click += new System.EventHandler(this.btnAssistantConsulterMaterielValidationRef_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(183, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(213, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "*Aucun matériel trouvé pour cette reference";
            this.label9.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(559, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(245, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "*Aucun matériel trouvé pour ce numéro de contrat.";
            this.label8.Visible = false;
            // 
            // mATERIEL_ACHETEDataGridView
            // 
            this.mATERIEL_ACHETEDataGridView.AllowUserToAddRows = false;
            this.mATERIEL_ACHETEDataGridView.AllowUserToDeleteRows = false;
            this.mATERIEL_ACHETEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mATERIEL_ACHETEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reference,
            this.ContratLie,
            this.DateInstallation,
            this.Prix_Achat});
            this.mATERIEL_ACHETEDataGridView.Location = new System.Drawing.Point(16, 95);
            this.mATERIEL_ACHETEDataGridView.Name = "mATERIEL_ACHETEDataGridView";
            this.mATERIEL_ACHETEDataGridView.ReadOnly = true;
            this.mATERIEL_ACHETEDataGridView.Size = new System.Drawing.Size(946, 463);
            this.mATERIEL_ACHETEDataGridView.TabIndex = 7;
            this.mATERIEL_ACHETEDataGridView.Visible = false;
            // 
            // Reference
            // 
            this.Reference.HeaderText = "Reference";
            this.Reference.Name = "Reference";
            this.Reference.ReadOnly = true;
            // 
            // ContratLie
            // 
            this.ContratLie.HeaderText = "ContratLie";
            this.ContratLie.Name = "ContratLie";
            this.ContratLie.ReadOnly = true;
            // 
            // DateInstallation
            // 
            this.DateInstallation.HeaderText = "DateInstallation";
            this.DateInstallation.Name = "DateInstallation";
            this.DateInstallation.ReadOnly = true;
            // 
            // Prix_Achat
            // 
            this.Prix_Achat.HeaderText = "Prix_Achat";
            this.Prix_Achat.Name = "Prix_Achat";
            this.Prix_Achat.ReadOnly = true;
            // 
            // btnAssistantConsulterMaterielValidationCONTRAT
            // 
            this.btnAssistantConsulterMaterielValidationCONTRAT.BackColor = System.Drawing.Color.Transparent;
            this.btnAssistantConsulterMaterielValidationCONTRAT.FlatAppearance.BorderSize = 0;
            this.btnAssistantConsulterMaterielValidationCONTRAT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssistantConsulterMaterielValidationCONTRAT.Image = ((System.Drawing.Image)(resources.GetObject("btnAssistantConsulterMaterielValidationCONTRAT.Image")));
            this.btnAssistantConsulterMaterielValidationCONTRAT.Location = new System.Drawing.Point(824, 40);
            this.btnAssistantConsulterMaterielValidationCONTRAT.Name = "btnAssistantConsulterMaterielValidationCONTRAT";
            this.btnAssistantConsulterMaterielValidationCONTRAT.Size = new System.Drawing.Size(31, 30);
            this.btnAssistantConsulterMaterielValidationCONTRAT.TabIndex = 7;
            this.btnAssistantConsulterMaterielValidationCONTRAT.UseVisualStyleBackColor = false;
            this.btnAssistantConsulterMaterielValidationCONTRAT.Click += new System.EventHandler(this.btnAssistantConsulterMaterielValidationCONTRAT_Click);
            // 
            // txtAssistantConsulterMaterielReference
            // 
            this.txtAssistantConsulterMaterielReference.Location = new System.Drawing.Point(176, 43);
            this.txtAssistantConsulterMaterielReference.Name = "txtAssistantConsulterMaterielReference";
            this.txtAssistantConsulterMaterielReference.Size = new System.Drawing.Size(220, 20);
            this.txtAssistantConsulterMaterielReference.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(483, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "N° Contrat lié :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(107, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Reference :";
            // 
            // pnlAssistantTechnicien
            // 
            this.pnlAssistantTechnicien.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAssistantTechnicien.BackgroundImage")));
            this.pnlAssistantTechnicien.Controls.Add(this.label15);
            this.pnlAssistantTechnicien.Controls.Add(this.monthCalendar1);
            this.pnlAssistantTechnicien.Controls.Add(this.weekScheduleControl1);
            this.pnlAssistantTechnicien.Controls.Add(this.cbAssistantChoixTechnicien);
            this.pnlAssistantTechnicien.Controls.Add(this.label5);
            this.pnlAssistantTechnicien.Location = new System.Drawing.Point(111, 27);
            this.pnlAssistantTechnicien.Name = "pnlAssistantTechnicien";
            this.pnlAssistantTechnicien.Size = new System.Drawing.Size(970, 552);
            this.pnlAssistantTechnicien.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(40, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(260, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "CONSULTER INTERVENTION/TECHNICIEN";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(659, 9);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 3;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // cbAssistantChoixTechnicien
            // 
            this.cbAssistantChoixTechnicien.FormattingEnabled = true;
            this.cbAssistantChoixTechnicien.Location = new System.Drawing.Point(410, 76);
            this.cbAssistantChoixTechnicien.Name = "cbAssistantChoixTechnicien";
            this.cbAssistantChoixTechnicien.Size = new System.Drawing.Size(121, 21);
            this.cbAssistantChoixTechnicien.TabIndex = 1;
            this.cbAssistantChoixTechnicien.SelectedIndexChanged += new System.EventHandler(this.cbAssistantChoixTechnicien_TextChanged);
            this.cbAssistantChoixTechnicien.TextChanged += new System.EventHandler(this.cbAssistantChoixTechnicien_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(338, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Technicien :";
            // 
            // btnClient
            // 
            this.btnClient.BackColor = System.Drawing.Color.Transparent;
            this.btnClient.FlatAppearance.BorderSize = 0;
            this.btnClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClient.ForeColor = System.Drawing.Color.White;
            this.btnClient.Image = ((System.Drawing.Image)(resources.GetObject("btnClient.Image")));
            this.btnClient.Location = new System.Drawing.Point(-1, 64);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(100, 23);
            this.btnClient.TabIndex = 13;
            this.btnClient.Tag = "Cli";
            this.btnClient.Text = "Client";
            this.btnClient.UseVisualStyleBackColor = false;
            this.btnClient.Click += new System.EventHandler(this.btn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(-1, 298);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 14;
            this.button1.Tag = "Tec";
            this.button1.Text = "Technicien";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btn_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(-1, 218);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 15;
            this.button2.Tag = "Mat";
            this.button2.Text = "Materiel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnContrat
            // 
            this.btnContrat.BackColor = System.Drawing.Color.Transparent;
            this.btnContrat.FlatAppearance.BorderSize = 0;
            this.btnContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContrat.ForeColor = System.Drawing.Color.White;
            this.btnContrat.Image = ((System.Drawing.Image)(resources.GetObject("btnContrat.Image")));
            this.btnContrat.Location = new System.Drawing.Point(-1, 137);
            this.btnContrat.Name = "btnContrat";
            this.btnContrat.Size = new System.Drawing.Size(100, 23);
            this.btnContrat.TabIndex = 16;
            this.btnContrat.Tag = "Con";
            this.btnContrat.Text = "Contrat";
            this.btnContrat.UseVisualStyleBackColor = false;
            this.btnContrat.Click += new System.EventHandler(this.btn_Click);
            // 
            // weekScheduleControl1
            // 
            this.weekScheduleControl1.Date = new System.DateTime(((long)(0)));
            this.weekScheduleControl1.Location = new System.Drawing.Point(24, 173);
            this.weekScheduleControl1.Name = "weekScheduleControl1";
            this.weekScheduleControl1.Size = new System.Drawing.Size(935, 348);
            this.weekScheduleControl1.TabIndex = 2;
            this.weekScheduleControl1.Text = "weekScheduleControl1";
            this.weekScheduleControl1.AppointmentCreate += new System.EventHandler<Syd.ScheduleControls.Events.AppointmentCreateEventArgs>(this.calendar_AppointmentAdd);
            this.weekScheduleControl1.AppointmentMove += new System.EventHandler<Syd.ScheduleControls.Events.AppointmentMoveEventArgs>(this.calendar_AppointmentMove);
            this.weekScheduleControl1.AppointmentEdit += new System.EventHandler<Syd.ScheduleControls.Events.AppointmentEditEventArgs>(this.calendar_AppointmentEdit);
            // 
            // IHMAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1081, 575);
            this.Controls.Add(this.pnlAssistantConsulterClient);
            this.Controls.Add(this.btnContrat);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pnlAssistantConsulterMateriel);
            this.Controls.Add(this.pnlAssistantConsulterContrat);
            this.Controls.Add(this.btnClient);
            this.Controls.Add(this.pnlAssistantTechnicien);
            this.Name = "IHMAssistant";
            this.Text = "IHMAssistantConsulterClient";
            this.Load += new System.EventHandler(this.IHMAssistantConsulterClient_Load);
            this.pnlAssistantConsulterClient.ResumeLayout(false);
            this.pnlAssistantConsulterClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLIENTDataGridView)).EndInit();
            this.pnlAssistantConsulterContrat.ResumeLayout(false);
            this.pnlAssistantConsulterContrat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumContrat)).EndInit();
            this.pnlAssistantConsulterMateriel.ResumeLayout(false);
            this.pnlAssistantConsulterMateriel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mATERIEL_ACHETEDataGridView)).EndInit();
            this.pnlAssistantTechnicien.ResumeLayout(false);
            this.pnlAssistantTechnicien.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlAssistantConsulterClient;
        private System.Windows.Forms.Panel pnlAssistantConsulterContrat;
        private System.Windows.Forms.TextBox txtAssistantClientNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView txtNumContrat;
        private System.Windows.Forms.Button btnRechercheContrat;
        private System.Windows.Forms.Panel pnlAssistantConsulterMateriel;
        private System.Windows.Forms.Button btnAssistantConsulterMaterielValidationCONTRAT;
        private System.Windows.Forms.TextBox txtAssistantConsulterMaterielReference;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlAssistantTechnicien;
        private System.Windows.Forms.DataGridView mATERIEL_ACHETEDataGridView;
        private System.Windows.Forms.ComboBox cbAssistantChoixTechnicien;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnRechercheClient;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnAssistantConsulterMaterielValidationRef;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView cLIENTDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button btnRechercheIDClient;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtContratIDClient;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLI_PSN_LOGIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prenom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ville;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodePostal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pays;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelDomicile;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelMobile;
        private System.Windows.Forms.DataGridViewTextBoxColumn DistanceAntenne;
        private System.Windows.Forms.DataGridViewTextBoxColumn DureeDeplacement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reference;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContratLie;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateInstallation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prix_Achat;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumContrat;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeContrat;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateEcheance;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateRenouvellement;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateSignatureContrat;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prix;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private Syd.ScheduleControls.WeekScheduleControl weekScheduleControl1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox txtAssistantConsulterMaterielContratlie;
        private System.Windows.Forms.MaskedTextBox txtNumContra;
        private System.Windows.Forms.Button btnClient;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnContrat;
    }
}