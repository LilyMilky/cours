﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Syd.ScheduleControls.Events;
using Metier.Entities;
using Dataaccess;
using Syd.ScheduleControls.Test.Dialog;
using Syd.ScheduleControls.Test;

namespace IHM
{
    public partial class IHMTechnicien : Form
    {
        private string technicienId;

        public IHMTechnicien()
        {
            DataAccess A = new DataAccess();
            technicienId = "T1";
            InitializeComponent();
            weekScheduleControl1.Date = DateTime.Now ;

            List<EntityIntervention> Interventions = A.GetAllIntervention(technicienId);

            foreach (EntityIntervention Intervention in Interventions)
            {
                if(Intervention.KMParcouru == 0 && string.IsNullOrEmpty(Intervention.Commentaire))
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
                else
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention Effectué", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
            }

            weekScheduleControl1.RefreshAppointments();
            weekScheduleControl1.Invalidate();
        }

        public IHMTechnicien(string id)
        {
            DataAccess A = new DataAccess();
            technicienId = id;
            InitializeComponent();
            weekScheduleControl1.Date = DateTime.Now;

            List<EntityIntervention> Interventions = A.GetAllIntervention(technicienId);

            foreach (EntityIntervention Intervention in Interventions)
            {
                if (Intervention.KMParcouru == 0 && string.IsNullOrEmpty(Intervention.Commentaire))
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
                else
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention Effectué", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
            }

            weekScheduleControl1.RefreshAppointments();
            weekScheduleControl1.Invalidate();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            weekScheduleControl1.Date = e.Start;
            weekScheduleControl1.RefreshAppointments();
            weekScheduleControl1.Invalidate();
        }

        private void calendar_AppointmentEdit(object sender, AppointmentEditEventArgs e)
        {
            bool Edition = false;
            DateTime AncienneDate = e.Appointment.DateStart;
            DataAccess A = new DataAccess();
            EntityIntervention Intervention = A.GetInterventionByDateDebut(e.Appointment.DateStart);
            EntityInterventionPiece InterventionPiece = A.GetInterventionPiece(Intervention.InterventionId, Intervention.MaterielId);

            //show a dialog to edit the appointment
            using (FicheIntervention dialog = new FicheIntervention())
            {
                dialog.txtDateAjout = Intervention.DateDebut.ToString();
                dialog.DateFin = Intervention.DateEnd.ToString();
                dialog.NumeroContrat = Intervention.NumContrat;
                dialog.DescriptionProbleme = Intervention.DescriptionProbleme;
                dialog.Commentaire = Intervention.Commentaire;
                dialog.Garantie = Intervention.Garantie;

                if(!string.IsNullOrEmpty(Intervention.KMParcouru.ToString()) && Intervention.KMParcouru != 0)
                    dialog.KMParcouru = Intervention.KMParcouru.ToString(); ;

                if (InterventionPiece.InterventionPieceId != 0)
                {
                    Edition = true;
                    dialog.Quantite = InterventionPiece.Quantite.ToString();
                    dialog.Description = InterventionPiece.Description;
                    dialog.MOPC = InterventionPiece.MOPC.ToString();
                    dialog.Montant = InterventionPiece.Montant.ToString();
                    dialog.PrixUnitaire = InterventionPiece.PrixUnitaire.ToString();
                }

                #region ENABLED
                dialog.GarantieRHGEnabled = false;
                dialog.GarantieRSGEnabled = false;
                dialog.GarantieVDCEnabled = false;
                dialog.NumMaterielEnabled = false;
                dialog.txtDateAjoutEnabled = false;
                dialog.DateFinEnabled = true;
                dialog.NumeroContratEnabled = false;
                dialog.DescriptionProblemeEnabled = false;
                dialog.CommentaireEnabled = true;
                dialog.QuantiteEnabled = true;
                dialog.KMParcouruEnabled = true;
                dialog.MOPCEnabled = true;
                dialog.PrixUnitaireEnabled = true;
                dialog.DescriptionEnabled = true;
                #endregion

                dialog.cbNumeroContrat_SelectedIndexChanged(sender, e);
                dialog.SetNumMateriel(Intervention.MaterielId);

                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {                  
                    //if the user clicked 'save', update the appointment dates and title
                    e.Appointment.DateStart = Convert.ToDateTime(dialog.txtDateAjout);
                    e.Appointment.DateEnd = Convert.ToDateTime(dialog.DateFin);
                    if(!string.IsNullOrEmpty(dialog.Commentaire) && !string.IsNullOrEmpty(dialog.KMParcouru))
                        e.Appointment.Subject = "Intervention Effectué";
                    else
                        e.Appointment.Subject = "Intervention";

                    A.UpdateInterventionTechnicien(AncienneDate, e.Appointment.DateEnd, dialog.Commentaire, Convert.ToDecimal(dialog.KMParcouru));

                    if (!string.IsNullOrEmpty(dialog.Quantite) && !string.IsNullOrEmpty(dialog.PrixUnitaire) && !string.IsNullOrEmpty(dialog.MOPC))
                    {
                        if (Edition == false)
                            A.InsertInterventionPiece(Intervention.InterventionId.ToString(), Convert.ToInt32(dialog.Quantite), dialog.Description, Convert.ToDecimal(dialog.PrixUnitaire), Convert.ToDecimal(dialog.MOPC), Convert.ToDecimal(dialog.Montant));
                        else
                            A.UpdateInterventionPiece(InterventionPiece.InterventionPieceId, Convert.ToInt32(dialog.Quantite), dialog.Description, Convert.ToDecimal(dialog.PrixUnitaire), Convert.ToDecimal(dialog.MOPC), Convert.ToDecimal(dialog.Montant));
                    }
                    //have to tell the controls to refresh appointment display
                    weekScheduleControl1.RefreshAppointments();

                    //get the controls to repaint 
                    weekScheduleControl1.Invalidate();
                }
            }
        }
    }
}
