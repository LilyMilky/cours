﻿namespace IHM
{
    partial class IHMCommercial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label25;
            System.Windows.Forms.Label mTL_TYPELabel;
            System.Windows.Forms.Label mTL_MODELELabel;
            System.Windows.Forms.Label cLT_TELDOMICILELabel;
            System.Windows.Forms.Label cLT_EMAILLabel;
            System.Windows.Forms.Label pSN_PRENOMLabel;
            System.Windows.Forms.Label pSN_MOBILELabel;
            System.Windows.Forms.Label pSN_NOMLabel;
            System.Windows.Forms.Label pSN_VILLELabel;
            System.Windows.Forms.Label pSN_CODEPOSTALLabel;
            System.Windows.Forms.Label cLT_DISTANCELabel;
            System.Windows.Forms.Label pSN_ADRESSELabel;
            System.Windows.Forms.Label pSN_PAYSLabel;
            System.Windows.Forms.Label cLT_DUREE_DEPLACEMENTLabel;
            System.Windows.Forms.Label label35;
            System.Windows.Forms.Label label34;
            System.Windows.Forms.Label label42;
            System.Windows.Forms.Label label43;
            System.Windows.Forms.Label label46;
            this.pnlClientAjouter = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtClientAjouterDeplacement = new System.Windows.Forms.TextBox();
            this.txtClientAjouterPays = new System.Windows.Forms.TextBox();
            this.txtClientAjouterDistance = new System.Windows.Forms.TextBox();
            this.txtClientAjouterCodePostal = new System.Windows.Forms.TextBox();
            this.txtClientAjouterAdresse = new System.Windows.Forms.TextBox();
            this.txtClientAjouterVille = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtClientAjouterTelMobile = new System.Windows.Forms.TextBox();
            this.txtClientAjouterTelDomicile = new System.Windows.Forms.TextBox();
            this.txtClientAjouterPrenom = new System.Windows.Forms.TextBox();
            this.txtClientAjouterNom = new System.Windows.Forms.TextBox();
            this.txtClientAjouterEmail = new System.Windows.Forms.TextBox();
            this.btnClientAjouterAjout = new System.Windows.Forms.Button();
            this.pnlClientConsulter = new System.Windows.Forms.Panel();
            this.btnClientConsulterAnnuler = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtClientConsulterDeplacement = new System.Windows.Forms.TextBox();
            this.txtClientConsulterPays = new System.Windows.Forms.TextBox();
            this.txtClientConsulterDistance = new System.Windows.Forms.TextBox();
            this.txtClientConsulterCodePostal = new System.Windows.Forms.TextBox();
            this.txtClientConsulterAdresse = new System.Windows.Forms.TextBox();
            this.txtClientConsulterVille = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtClientConsulterTelMob = new System.Windows.Forms.TextBox();
            this.txtClientConsulterTelDom = new System.Windows.Forms.TextBox();
            this.txtClientConsulterPrenom = new System.Windows.Forms.TextBox();
            this.txtClientConsulterNom = new System.Windows.Forms.TextBox();
            this.txtClientConsulterEmail = new System.Windows.Forms.TextBox();
            this.btnClientConsulterEnregistrer = new System.Windows.Forms.Button();
            this.btnClientConsulterModifier = new System.Windows.Forms.Button();
            this.pnlClientRechercher = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.dgvListClients = new System.Windows.Forms.DataGridView();
            this.PSN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_NOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_PRENOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_ADRESSE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_VILLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_CODEPOSTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_PAYS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PSN_MOBILE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLT_TELDOMICILE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLT_EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMaterielAjouter = new System.Windows.Forms.Panel();
            this.txtMaterielAjouterSérie = new System.Windows.Forms.TextBox();
            this.gbMaterielAjouterContratCarac = new System.Windows.Forms.GroupBox();
            this.txtMaterielAjouterContratPrixNew = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtMaterielAjouterContratType = new System.Windows.Forms.TextBox();
            this.txtMaterielAjouterContratSignature = new System.Windows.Forms.TextBox();
            this.txtMaterielAjouterContratRenouvellement = new System.Windows.Forms.TextBox();
            this.txtMaterielAjouterContratEcheance = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtMaterielAjouterContratPrixOld = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.cbMaterielAjouterContrat = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cbMaterielAjouterModele = new System.Windows.Forms.ComboBox();
            this.cbMaterielAjouterType = new System.Windows.Forms.ComboBox();
            this.btnMaterielAjouterAjout = new System.Windows.Forms.Button();
            this.tbClientRechercheID = new System.Windows.Forms.TextBox();
            this.tbClientRechercheContrat = new System.Windows.Forms.TextBox();
            this.tbClientRechercheNom = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlMaterielConsulter = new System.Windows.Forms.Panel();
            this.pnlMaterielConsulterDgvModif = new System.Windows.Forms.Panel();
            this.btnMaterielConsulterDgvModifSave = new System.Windows.Forms.Button();
            this.dtMaterielConsulterDgvModifInstallation = new System.Windows.Forms.DateTimePicker();
            this.cbMaterielConsulterDgvModifModele = new System.Windows.Forms.ComboBox();
            this.cbMaterielConsulterDgvModifType = new System.Windows.Forms.ComboBox();
            this.pnlMaterielConsulterInstallation = new System.Windows.Forms.Panel();
            this.pnlMaterielConsulterInstallationDate = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.dtMaterielConsulterFin = new System.Windows.Forms.DateTimePicker();
            this.pnlMaterielConsulterInstallationSansDate = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dtMaterielConsulterDeb = new System.Windows.Forms.DateTimePicker();
            this.cbMaterielConsulterInstallation = new System.Windows.Forms.ComboBox();
            this.pnlMaterielConsulterSerie = new System.Windows.Forms.Panel();
            this.cbMaterielConsulterSerie = new System.Windows.Forms.ComboBox();
            this.dgvMaterielConsulter = new System.Windows.Forms.DataGridView();
            this.MTL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CT_NUM_CONTRAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modele = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MTL_DATE_INSTALLATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MTL_PRIX_ACHAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMaterielConsulterContrat = new System.Windows.Forms.Panel();
            this.cbMaterielConsulterContrat = new System.Windows.Forms.ComboBox();
            this.cbMaterielConsulterRecherche = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.pnlContratConsulter = new System.Windows.Forms.Panel();
            this.pnlContratConsulterRecherche = new System.Windows.Forms.Panel();
            this.btnContratConsulterEnregistrer = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtContratConsulterType = new System.Windows.Forms.TextBox();
            this.txtContratConsulterSignature = new System.Windows.Forms.TextBox();
            this.txtContratConsulterRenouvellement = new System.Windows.Forms.TextBox();
            this.txtContratConsulterEcheance = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtContratConsulterPrix = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtContratConsulterClient = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtContratConsulterCommercial = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbContratConsulterRecherche = new System.Windows.Forms.ComboBox();
            this.btnClientRechercheNom = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnClientRechercheContrat = new System.Windows.Forms.Button();
            this.btnClientRechercheID = new System.Windows.Forms.Button();
            this.lbClientRechercheID = new System.Windows.Forms.Label();
            this.lbClientRechercheContrat = new System.Windows.Forms.Label();
            this.lbClientRechercheNom = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnClientAjouter = new System.Windows.Forms.Button();
            this.btnClientConsulter = new System.Windows.Forms.Button();
            this.btnMaterielConsulter = new System.Windows.Forms.Button();
            this.btnMaterielAjouter = new System.Windows.Forms.Button();
            this.btnContratConsulter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            mTL_TYPELabel = new System.Windows.Forms.Label();
            mTL_MODELELabel = new System.Windows.Forms.Label();
            cLT_TELDOMICILELabel = new System.Windows.Forms.Label();
            cLT_EMAILLabel = new System.Windows.Forms.Label();
            pSN_PRENOMLabel = new System.Windows.Forms.Label();
            pSN_MOBILELabel = new System.Windows.Forms.Label();
            pSN_NOMLabel = new System.Windows.Forms.Label();
            pSN_VILLELabel = new System.Windows.Forms.Label();
            pSN_CODEPOSTALLabel = new System.Windows.Forms.Label();
            cLT_DISTANCELabel = new System.Windows.Forms.Label();
            pSN_ADRESSELabel = new System.Windows.Forms.Label();
            pSN_PAYSLabel = new System.Windows.Forms.Label();
            cLT_DUREE_DEPLACEMENTLabel = new System.Windows.Forms.Label();
            label35 = new System.Windows.Forms.Label();
            label34 = new System.Windows.Forms.Label();
            label42 = new System.Windows.Forms.Label();
            label43 = new System.Windows.Forms.Label();
            label46 = new System.Windows.Forms.Label();
            this.pnlClientAjouter.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnlClientConsulter.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlClientRechercher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListClients)).BeginInit();
            this.pnlMaterielAjouter.SuspendLayout();
            this.gbMaterielAjouterContratCarac.SuspendLayout();
            this.pnlMaterielConsulter.SuspendLayout();
            this.pnlMaterielConsulterDgvModif.SuspendLayout();
            this.pnlMaterielConsulterInstallation.SuspendLayout();
            this.pnlMaterielConsulterInstallationDate.SuspendLayout();
            this.pnlMaterielConsulterInstallationSansDate.SuspendLayout();
            this.pnlMaterielConsulterSerie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterielConsulter)).BeginInit();
            this.pnlMaterielConsulterContrat.SuspendLayout();
            this.pnlContratConsulter.SuspendLayout();
            this.pnlContratConsulterRecherche.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(397, 155);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(173, 13);
            label8.TabIndex = 74;
            label8.Text = "Durée de déplacement technicien :";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(12, 116);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(36, 13);
            label9.TabIndex = 68;
            label9.Text = "Pays :";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(12, 46);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(51, 13);
            label10.TabIndex = 62;
            label10.Text = "Adresse :";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(12, 155);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(135, 13);
            label11.TabIndex = 72;
            label11.Text = "Distance Antenne / Client :";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(397, 80);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(70, 13);
            label12.TabIndex = 66;
            label12.Text = "Code Postal :";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(12, 80);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(32, 13);
            label14.TabIndex = 64;
            label14.Text = "Ville :";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new System.Drawing.Point(29, 34);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(35, 13);
            label15.TabIndex = 58;
            label15.Text = "Nom :";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(397, 71);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(68, 13);
            label16.TabIndex = 70;
            label16.Text = "Tel (Mobile) :";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(397, 34);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(49, 13);
            label17.TabIndex = 60;
            label17.Text = "Prenom :";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(29, 109);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(41, 13);
            label18.TabIndex = 78;
            label18.Text = "E-mail :";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(29, 71);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(77, 13);
            label25.TabIndex = 76;
            label25.Text = "Tel (Domicile) :";
            // 
            // mTL_TYPELabel
            // 
            mTL_TYPELabel.AutoSize = true;
            mTL_TYPELabel.Location = new System.Drawing.Point(403, 144);
            mTL_TYPELabel.Name = "mTL_TYPELabel";
            mTL_TYPELabel.Size = new System.Drawing.Size(31, 13);
            mTL_TYPELabel.TabIndex = 16;
            mTL_TYPELabel.Text = "Type";
            // 
            // mTL_MODELELabel
            // 
            mTL_MODELELabel.AutoSize = true;
            mTL_MODELELabel.Location = new System.Drawing.Point(392, 194);
            mTL_MODELELabel.Name = "mTL_MODELELabel";
            mTL_MODELELabel.Size = new System.Drawing.Size(42, 13);
            mTL_MODELELabel.TabIndex = 18;
            mTL_MODELELabel.Text = "Modele";
            // 
            // cLT_TELDOMICILELabel
            // 
            cLT_TELDOMICILELabel.AutoSize = true;
            cLT_TELDOMICILELabel.Location = new System.Drawing.Point(29, 71);
            cLT_TELDOMICILELabel.Name = "cLT_TELDOMICILELabel";
            cLT_TELDOMICILELabel.Size = new System.Drawing.Size(74, 13);
            cLT_TELDOMICILELabel.TabIndex = 76;
            cLT_TELDOMICILELabel.Text = "Tel (Domicile):";
            // 
            // cLT_EMAILLabel
            // 
            cLT_EMAILLabel.AutoSize = true;
            cLT_EMAILLabel.Location = new System.Drawing.Point(29, 109);
            cLT_EMAILLabel.Name = "cLT_EMAILLabel";
            cLT_EMAILLabel.Size = new System.Drawing.Size(38, 13);
            cLT_EMAILLabel.TabIndex = 78;
            cLT_EMAILLabel.Text = "E-mail:";
            // 
            // pSN_PRENOMLabel
            // 
            pSN_PRENOMLabel.AutoSize = true;
            pSN_PRENOMLabel.Location = new System.Drawing.Point(397, 34);
            pSN_PRENOMLabel.Name = "pSN_PRENOMLabel";
            pSN_PRENOMLabel.Size = new System.Drawing.Size(46, 13);
            pSN_PRENOMLabel.TabIndex = 60;
            pSN_PRENOMLabel.Text = "Prenom:";
            // 
            // pSN_MOBILELabel
            // 
            pSN_MOBILELabel.AutoSize = true;
            pSN_MOBILELabel.Location = new System.Drawing.Point(397, 71);
            pSN_MOBILELabel.Name = "pSN_MOBILELabel";
            pSN_MOBILELabel.Size = new System.Drawing.Size(65, 13);
            pSN_MOBILELabel.TabIndex = 70;
            pSN_MOBILELabel.Text = "Tel (Mobile):";
            // 
            // pSN_NOMLabel
            // 
            pSN_NOMLabel.AutoSize = true;
            pSN_NOMLabel.Location = new System.Drawing.Point(29, 34);
            pSN_NOMLabel.Name = "pSN_NOMLabel";
            pSN_NOMLabel.Size = new System.Drawing.Size(32, 13);
            pSN_NOMLabel.TabIndex = 58;
            pSN_NOMLabel.Text = "Nom:";
            // 
            // pSN_VILLELabel
            // 
            pSN_VILLELabel.AutoSize = true;
            pSN_VILLELabel.Location = new System.Drawing.Point(12, 80);
            pSN_VILLELabel.Name = "pSN_VILLELabel";
            pSN_VILLELabel.Size = new System.Drawing.Size(29, 13);
            pSN_VILLELabel.TabIndex = 64;
            pSN_VILLELabel.Text = "Ville:";
            // 
            // pSN_CODEPOSTALLabel
            // 
            pSN_CODEPOSTALLabel.AutoSize = true;
            pSN_CODEPOSTALLabel.Location = new System.Drawing.Point(397, 80);
            pSN_CODEPOSTALLabel.Name = "pSN_CODEPOSTALLabel";
            pSN_CODEPOSTALLabel.Size = new System.Drawing.Size(67, 13);
            pSN_CODEPOSTALLabel.TabIndex = 66;
            pSN_CODEPOSTALLabel.Text = "Code Postal:";
            // 
            // cLT_DISTANCELabel
            // 
            cLT_DISTANCELabel.AutoSize = true;
            cLT_DISTANCELabel.Location = new System.Drawing.Point(12, 155);
            cLT_DISTANCELabel.Name = "cLT_DISTANCELabel";
            cLT_DISTANCELabel.Size = new System.Drawing.Size(132, 13);
            cLT_DISTANCELabel.TabIndex = 72;
            cLT_DISTANCELabel.Text = "Distance Antenne / Client:";
            // 
            // pSN_ADRESSELabel
            // 
            pSN_ADRESSELabel.AutoSize = true;
            pSN_ADRESSELabel.Location = new System.Drawing.Point(12, 46);
            pSN_ADRESSELabel.Name = "pSN_ADRESSELabel";
            pSN_ADRESSELabel.Size = new System.Drawing.Size(48, 13);
            pSN_ADRESSELabel.TabIndex = 62;
            pSN_ADRESSELabel.Text = "Adresse:";
            // 
            // pSN_PAYSLabel
            // 
            pSN_PAYSLabel.AutoSize = true;
            pSN_PAYSLabel.Location = new System.Drawing.Point(12, 116);
            pSN_PAYSLabel.Name = "pSN_PAYSLabel";
            pSN_PAYSLabel.Size = new System.Drawing.Size(33, 13);
            pSN_PAYSLabel.TabIndex = 68;
            pSN_PAYSLabel.Text = "Pays:";
            // 
            // cLT_DUREE_DEPLACEMENTLabel
            // 
            cLT_DUREE_DEPLACEMENTLabel.AutoSize = true;
            cLT_DUREE_DEPLACEMENTLabel.Location = new System.Drawing.Point(397, 155);
            cLT_DUREE_DEPLACEMENTLabel.Name = "cLT_DUREE_DEPLACEMENTLabel";
            cLT_DUREE_DEPLACEMENTLabel.Size = new System.Drawing.Size(170, 13);
            cLT_DUREE_DEPLACEMENTLabel.TabIndex = 74;
            cLT_DUREE_DEPLACEMENTLabel.Text = "Durée de déplacement technicien:";
            // 
            // label35
            // 
            label35.AutoSize = true;
            label35.Location = new System.Drawing.Point(380, 244);
            label35.Name = "label35";
            label35.Size = new System.Drawing.Size(54, 13);
            label35.TabIndex = 22;
            label35.Text = "Contrat lié";
            // 
            // label34
            // 
            label34.AutoSize = true;
            label34.Location = new System.Drawing.Point(6, 12);
            label34.Name = "label34";
            label34.Size = new System.Drawing.Size(31, 13);
            label34.TabIndex = 21;
            label34.Text = "Type";
            // 
            // label42
            // 
            label42.AutoSize = true;
            label42.Location = new System.Drawing.Point(6, 97);
            label42.Name = "label42";
            label42.Size = new System.Drawing.Size(42, 13);
            label42.TabIndex = 22;
            label42.Text = "Modele";
            // 
            // label43
            // 
            label43.AutoSize = true;
            label43.Location = new System.Drawing.Point(6, 176);
            label43.Name = "label43";
            label43.Size = new System.Drawing.Size(90, 13);
            label43.TabIndex = 25;
            label43.Text = "Date d\'installation";
            // 
            // label46
            // 
            label46.AutoSize = true;
            label46.Location = new System.Drawing.Point(375, 102);
            label46.Name = "label46";
            label46.Size = new System.Drawing.Size(59, 13);
            label46.TabIndex = 25;
            label46.Text = "N° de série";
            // 
            // pnlClientAjouter
            // 
            this.pnlClientAjouter.BackColor = System.Drawing.Color.Transparent;
            this.pnlClientAjouter.Controls.Add(this.label20);
            this.pnlClientAjouter.Controls.Add(this.groupBox3);
            this.pnlClientAjouter.Controls.Add(this.groupBox4);
            this.pnlClientAjouter.Controls.Add(this.btnClientAjouterAjout);
            this.pnlClientAjouter.Location = new System.Drawing.Point(144, 80);
            this.pnlClientAjouter.Name = "pnlClientAjouter";
            this.pnlClientAjouter.Size = new System.Drawing.Size(980, 594);
            this.pnlClientAjouter.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(440, 34);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 84;
            this.label20.Text = "Ajouter un client";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtClientAjouterDeplacement);
            this.groupBox3.Controls.Add(this.txtClientAjouterPays);
            this.groupBox3.Controls.Add(label8);
            this.groupBox3.Controls.Add(label9);
            this.groupBox3.Controls.Add(this.txtClientAjouterDistance);
            this.groupBox3.Controls.Add(label10);
            this.groupBox3.Controls.Add(label11);
            this.groupBox3.Controls.Add(this.txtClientAjouterCodePostal);
            this.groupBox3.Controls.Add(this.txtClientAjouterAdresse);
            this.groupBox3.Controls.Add(label12);
            this.groupBox3.Controls.Add(label14);
            this.groupBox3.Controls.Add(this.txtClientAjouterVille);
            this.groupBox3.Location = new System.Drawing.Point(106, 267);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(770, 219);
            this.groupBox3.TabIndex = 83;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Coordonnées";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(708, 155);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(43, 13);
            this.label44.TabIndex = 77;
            this.label44.Text = "minutes";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(331, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "Km";
            // 
            // txtClientAjouterDeplacement
            // 
            this.txtClientAjouterDeplacement.Location = new System.Drawing.Point(576, 152);
            this.txtClientAjouterDeplacement.MaxLength = 20;
            this.txtClientAjouterDeplacement.Name = "txtClientAjouterDeplacement";
            this.txtClientAjouterDeplacement.Size = new System.Drawing.Size(114, 20);
            this.txtClientAjouterDeplacement.TabIndex = 11;
            this.txtClientAjouterDeplacement.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientAjouterDeplacement.Validating += new System.ComponentModel.CancelEventHandler(this.txtClientAjouterDeplacement_Validating);
            // 
            // txtClientAjouterPays
            // 
            this.txtClientAjouterPays.Location = new System.Drawing.Point(172, 113);
            this.txtClientAjouterPays.MaxLength = 50;
            this.txtClientAjouterPays.Name = "txtClientAjouterPays";
            this.txtClientAjouterPays.Size = new System.Drawing.Size(139, 20);
            this.txtClientAjouterPays.TabIndex = 9;
            this.txtClientAjouterPays.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterDistance
            // 
            this.txtClientAjouterDistance.Location = new System.Drawing.Point(172, 152);
            this.txtClientAjouterDistance.MaxLength = 20;
            this.txtClientAjouterDistance.Name = "txtClientAjouterDistance";
            this.txtClientAjouterDistance.Size = new System.Drawing.Size(139, 20);
            this.txtClientAjouterDistance.TabIndex = 10;
            this.txtClientAjouterDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Numeric_KeyPress);
            this.txtClientAjouterDistance.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterCodePostal
            // 
            this.txtClientAjouterCodePostal.Location = new System.Drawing.Point(576, 77);
            this.txtClientAjouterCodePostal.MaxLength = 5;
            this.txtClientAjouterCodePostal.Name = "txtClientAjouterCodePostal";
            this.txtClientAjouterCodePostal.Size = new System.Drawing.Size(114, 20);
            this.txtClientAjouterCodePostal.TabIndex = 8;
            this.txtClientAjouterCodePostal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientAjouterCodePostal.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterAdresse
            // 
            this.txtClientAjouterAdresse.Location = new System.Drawing.Point(172, 43);
            this.txtClientAjouterAdresse.MaxLength = 250;
            this.txtClientAjouterAdresse.Name = "txtClientAjouterAdresse";
            this.txtClientAjouterAdresse.Size = new System.Drawing.Size(518, 20);
            this.txtClientAjouterAdresse.TabIndex = 6;
            this.txtClientAjouterAdresse.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterVille
            // 
            this.txtClientAjouterVille.Location = new System.Drawing.Point(172, 77);
            this.txtClientAjouterVille.MaxLength = 50;
            this.txtClientAjouterVille.Name = "txtClientAjouterVille";
            this.txtClientAjouterVille.Size = new System.Drawing.Size(139, 20);
            this.txtClientAjouterVille.TabIndex = 7;
            this.txtClientAjouterVille.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtClientAjouterTelMobile);
            this.groupBox4.Controls.Add(this.txtClientAjouterTelDomicile);
            this.groupBox4.Controls.Add(this.txtClientAjouterPrenom);
            this.groupBox4.Controls.Add(this.txtClientAjouterNom);
            this.groupBox4.Controls.Add(label15);
            this.groupBox4.Controls.Add(label16);
            this.groupBox4.Controls.Add(label17);
            this.groupBox4.Controls.Add(this.txtClientAjouterEmail);
            this.groupBox4.Controls.Add(label18);
            this.groupBox4.Controls.Add(label25);
            this.groupBox4.Location = new System.Drawing.Point(105, 99);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(770, 156);
            this.groupBox4.TabIndex = 82;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Identité";
            // 
            // txtClientAjouterTelMobile
            // 
            this.txtClientAjouterTelMobile.Location = new System.Drawing.Point(576, 67);
            this.txtClientAjouterTelMobile.MaxLength = 10;
            this.txtClientAjouterTelMobile.Name = "txtClientAjouterTelMobile";
            this.txtClientAjouterTelMobile.Size = new System.Drawing.Size(115, 20);
            this.txtClientAjouterTelMobile.TabIndex = 80;
            this.txtClientAjouterTelMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientAjouterTelMobile.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterTelDomicile
            // 
            this.txtClientAjouterTelDomicile.Location = new System.Drawing.Point(173, 68);
            this.txtClientAjouterTelDomicile.MaxLength = 10;
            this.txtClientAjouterTelDomicile.Name = "txtClientAjouterTelDomicile";
            this.txtClientAjouterTelDomicile.Size = new System.Drawing.Size(138, 20);
            this.txtClientAjouterTelDomicile.TabIndex = 79;
            this.txtClientAjouterTelDomicile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientAjouterTelDomicile.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterPrenom
            // 
            this.txtClientAjouterPrenom.Location = new System.Drawing.Point(576, 31);
            this.txtClientAjouterPrenom.Name = "txtClientAjouterPrenom";
            this.txtClientAjouterPrenom.Size = new System.Drawing.Size(115, 20);
            this.txtClientAjouterPrenom.TabIndex = 2;
            this.txtClientAjouterPrenom.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterNom
            // 
            this.txtClientAjouterNom.Location = new System.Drawing.Point(173, 31);
            this.txtClientAjouterNom.MaxLength = 50;
            this.txtClientAjouterNom.Name = "txtClientAjouterNom";
            this.txtClientAjouterNom.Size = new System.Drawing.Size(138, 20);
            this.txtClientAjouterNom.TabIndex = 1;
            this.txtClientAjouterNom.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientAjouterEmail
            // 
            this.txtClientAjouterEmail.Location = new System.Drawing.Point(172, 109);
            this.txtClientAjouterEmail.MaxLength = 50;
            this.txtClientAjouterEmail.Name = "txtClientAjouterEmail";
            this.txtClientAjouterEmail.Size = new System.Drawing.Size(139, 20);
            this.txtClientAjouterEmail.TabIndex = 5;
            this.txtClientAjouterEmail.Validating += new System.ComponentModel.CancelEventHandler(this.Email_Validating);
            this.txtClientAjouterEmail.Validated += new System.EventHandler(this.Email_Validated);
            // 
            // btnClientAjouterAjout
            // 
            this.btnClientAjouterAjout.Location = new System.Drawing.Point(435, 511);
            this.btnClientAjouterAjout.Name = "btnClientAjouterAjout";
            this.btnClientAjouterAjout.Size = new System.Drawing.Size(100, 38);
            this.btnClientAjouterAjout.TabIndex = 12;
            this.btnClientAjouterAjout.Text = "Ajouter";
            this.btnClientAjouterAjout.UseVisualStyleBackColor = true;
            this.btnClientAjouterAjout.Click += new System.EventHandler(this.btnClientAjouterAjout_Click);
            // 
            // pnlClientConsulter
            // 
            this.pnlClientConsulter.BackColor = System.Drawing.Color.Transparent;
            this.pnlClientConsulter.Controls.Add(this.btnClientConsulterAnnuler);
            this.pnlClientConsulter.Controls.Add(this.label21);
            this.pnlClientConsulter.Controls.Add(this.groupBox2);
            this.pnlClientConsulter.Controls.Add(this.groupBox1);
            this.pnlClientConsulter.Controls.Add(this.btnClientConsulterEnregistrer);
            this.pnlClientConsulter.Controls.Add(this.btnClientConsulterModifier);
            this.pnlClientConsulter.Location = new System.Drawing.Point(144, 80);
            this.pnlClientConsulter.Name = "pnlClientConsulter";
            this.pnlClientConsulter.Size = new System.Drawing.Size(980, 594);
            this.pnlClientConsulter.TabIndex = 1;
            // 
            // btnClientConsulterAnnuler
            // 
            this.btnClientConsulterAnnuler.Location = new System.Drawing.Point(309, 503);
            this.btnClientConsulterAnnuler.Name = "btnClientConsulterAnnuler";
            this.btnClientConsulterAnnuler.Size = new System.Drawing.Size(100, 31);
            this.btnClientConsulterAnnuler.TabIndex = 83;
            this.btnClientConsulterAnnuler.Text = "Annuler";
            this.btnClientConsulterAnnuler.UseVisualStyleBackColor = true;
            this.btnClientConsulterAnnuler.Visible = false;
            this.btnClientConsulterAnnuler.Click += new System.EventHandler(this.btnClientConsulterAnnuler_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(411, 49);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(132, 13);
            this.label21.TabIndex = 82;
            this.label21.Text = "Consulter / Editer un client";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtClientConsulterDeplacement);
            this.groupBox2.Controls.Add(this.txtClientConsulterPays);
            this.groupBox2.Controls.Add(cLT_DUREE_DEPLACEMENTLabel);
            this.groupBox2.Controls.Add(pSN_PAYSLabel);
            this.groupBox2.Controls.Add(this.txtClientConsulterDistance);
            this.groupBox2.Controls.Add(pSN_ADRESSELabel);
            this.groupBox2.Controls.Add(cLT_DISTANCELabel);
            this.groupBox2.Controls.Add(this.txtClientConsulterCodePostal);
            this.groupBox2.Controls.Add(this.txtClientConsulterAdresse);
            this.groupBox2.Controls.Add(pSN_CODEPOSTALLabel);
            this.groupBox2.Controls.Add(pSN_VILLELabel);
            this.groupBox2.Controls.Add(this.txtClientConsulterVille);
            this.groupBox2.Location = new System.Drawing.Point(89, 267);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(770, 219);
            this.groupBox2.TabIndex = 81;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Coordonnées";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(714, 154);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(43, 13);
            this.label45.TabIndex = 77;
            this.label45.Text = "minutes";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(334, 155);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 13);
            this.label13.TabIndex = 76;
            this.label13.Text = "Km";
            // 
            // txtClientConsulterDeplacement
            // 
            this.txtClientConsulterDeplacement.Location = new System.Drawing.Point(576, 152);
            this.txtClientConsulterDeplacement.MaxLength = 3;
            this.txtClientConsulterDeplacement.Name = "txtClientConsulterDeplacement";
            this.txtClientConsulterDeplacement.Size = new System.Drawing.Size(114, 20);
            this.txtClientConsulterDeplacement.TabIndex = 75;
            this.txtClientConsulterDeplacement.Validating += new System.ComponentModel.CancelEventHandler(this.txtClientAjouterDeplacement_Validating);
            // 
            // txtClientConsulterPays
            // 
            this.txtClientConsulterPays.Location = new System.Drawing.Point(172, 113);
            this.txtClientConsulterPays.Name = "txtClientConsulterPays";
            this.txtClientConsulterPays.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterPays.TabIndex = 69;
            this.txtClientConsulterPays.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterDistance
            // 
            this.txtClientConsulterDistance.Location = new System.Drawing.Point(172, 152);
            this.txtClientConsulterDistance.MaxLength = 20;
            this.txtClientConsulterDistance.Name = "txtClientConsulterDistance";
            this.txtClientConsulterDistance.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterDistance.TabIndex = 73;
            this.txtClientConsulterDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Numeric_KeyPress);
            this.txtClientConsulterDistance.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterCodePostal
            // 
            this.txtClientConsulterCodePostal.Location = new System.Drawing.Point(576, 77);
            this.txtClientConsulterCodePostal.MaxLength = 5;
            this.txtClientConsulterCodePostal.Name = "txtClientConsulterCodePostal";
            this.txtClientConsulterCodePostal.Size = new System.Drawing.Size(114, 20);
            this.txtClientConsulterCodePostal.TabIndex = 67;
            this.txtClientConsulterCodePostal.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterAdresse
            // 
            this.txtClientConsulterAdresse.Location = new System.Drawing.Point(172, 43);
            this.txtClientConsulterAdresse.Name = "txtClientConsulterAdresse";
            this.txtClientConsulterAdresse.Size = new System.Drawing.Size(543, 20);
            this.txtClientConsulterAdresse.TabIndex = 63;
            this.txtClientConsulterAdresse.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterVille
            // 
            this.txtClientConsulterVille.Location = new System.Drawing.Point(172, 77);
            this.txtClientConsulterVille.Name = "txtClientConsulterVille";
            this.txtClientConsulterVille.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterVille.TabIndex = 65;
            this.txtClientConsulterVille.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtClientConsulterTelMob);
            this.groupBox1.Controls.Add(this.txtClientConsulterTelDom);
            this.groupBox1.Controls.Add(this.txtClientConsulterPrenom);
            this.groupBox1.Controls.Add(this.txtClientConsulterNom);
            this.groupBox1.Controls.Add(pSN_NOMLabel);
            this.groupBox1.Controls.Add(pSN_MOBILELabel);
            this.groupBox1.Controls.Add(pSN_PRENOMLabel);
            this.groupBox1.Controls.Add(this.txtClientConsulterEmail);
            this.groupBox1.Controls.Add(cLT_EMAILLabel);
            this.groupBox1.Controls.Add(cLT_TELDOMICILELabel);
            this.groupBox1.Location = new System.Drawing.Point(88, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 156);
            this.groupBox1.TabIndex = 80;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Identité";
            // 
            // txtClientConsulterTelMob
            // 
            this.txtClientConsulterTelMob.Location = new System.Drawing.Point(576, 67);
            this.txtClientConsulterTelMob.MaxLength = 10;
            this.txtClientConsulterTelMob.Name = "txtClientConsulterTelMob";
            this.txtClientConsulterTelMob.Size = new System.Drawing.Size(140, 20);
            this.txtClientConsulterTelMob.TabIndex = 83;
            this.txtClientConsulterTelMob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientConsulterTelMob.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterTelDom
            // 
            this.txtClientConsulterTelDom.Location = new System.Drawing.Point(172, 68);
            this.txtClientConsulterTelDom.MaxLength = 10;
            this.txtClientConsulterTelDom.Name = "txtClientConsulterTelDom";
            this.txtClientConsulterTelDom.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterTelDom.TabIndex = 82;
            this.txtClientConsulterTelDom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumber_KeyPress);
            this.txtClientConsulterTelDom.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterPrenom
            // 
            this.txtClientConsulterPrenom.Location = new System.Drawing.Point(576, 31);
            this.txtClientConsulterPrenom.MaxLength = 50;
            this.txtClientConsulterPrenom.Name = "txtClientConsulterPrenom";
            this.txtClientConsulterPrenom.Size = new System.Drawing.Size(140, 20);
            this.txtClientConsulterPrenom.TabIndex = 81;
            this.txtClientConsulterPrenom.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterNom
            // 
            this.txtClientConsulterNom.Location = new System.Drawing.Point(172, 31);
            this.txtClientConsulterNom.MaxLength = 50;
            this.txtClientConsulterNom.Name = "txtClientConsulterNom";
            this.txtClientConsulterNom.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterNom.TabIndex = 80;
            this.txtClientConsulterNom.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // txtClientConsulterEmail
            // 
            this.txtClientConsulterEmail.Location = new System.Drawing.Point(172, 109);
            this.txtClientConsulterEmail.Name = "txtClientConsulterEmail";
            this.txtClientConsulterEmail.Size = new System.Drawing.Size(139, 20);
            this.txtClientConsulterEmail.TabIndex = 79;
            this.txtClientConsulterEmail.Validating += new System.ComponentModel.CancelEventHandler(this.Email_Validating);
            this.txtClientConsulterEmail.Validated += new System.EventHandler(this.Email_Validated);
            // 
            // btnClientConsulterEnregistrer
            // 
            this.btnClientConsulterEnregistrer.Location = new System.Drawing.Point(571, 502);
            this.btnClientConsulterEnregistrer.Name = "btnClientConsulterEnregistrer";
            this.btnClientConsulterEnregistrer.Size = new System.Drawing.Size(100, 31);
            this.btnClientConsulterEnregistrer.TabIndex = 56;
            this.btnClientConsulterEnregistrer.Text = "Enregistrer";
            this.btnClientConsulterEnregistrer.UseVisualStyleBackColor = true;
            this.btnClientConsulterEnregistrer.Visible = false;
            this.btnClientConsulterEnregistrer.Click += new System.EventHandler(this.btnClientConsulterEnregistrer_Click);
            // 
            // btnClientConsulterModifier
            // 
            this.btnClientConsulterModifier.Location = new System.Drawing.Point(440, 502);
            this.btnClientConsulterModifier.Name = "btnClientConsulterModifier";
            this.btnClientConsulterModifier.Size = new System.Drawing.Size(100, 31);
            this.btnClientConsulterModifier.TabIndex = 55;
            this.btnClientConsulterModifier.Text = "Modifier";
            this.btnClientConsulterModifier.UseVisualStyleBackColor = true;
            this.btnClientConsulterModifier.Click += new System.EventHandler(this.btnClientConsulterModifier_Click);
            // 
            // pnlClientRechercher
            // 
            this.pnlClientRechercher.BackColor = System.Drawing.Color.Transparent;
            this.pnlClientRechercher.Controls.Add(this.label22);
            this.pnlClientRechercher.Controls.Add(this.dgvListClients);
            this.pnlClientRechercher.Location = new System.Drawing.Point(144, 80);
            this.pnlClientRechercher.Name = "pnlClientRechercher";
            this.pnlClientRechercher.Size = new System.Drawing.Size(980, 594);
            this.pnlClientRechercher.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(365, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(106, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Rechercher un client";
            // 
            // dgvListClients
            // 
            this.dgvListClients.AllowUserToAddRows = false;
            this.dgvListClients.AllowUserToDeleteRows = false;
            this.dgvListClients.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgvListClients.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListClients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PSN_ID,
            this.PSN_NOM,
            this.PSN_PRENOM,
            this.PSN_ADRESSE,
            this.PSN_VILLE,
            this.PSN_CODEPOSTAL,
            this.PSN_PAYS,
            this.PSN_MOBILE,
            this.CLT_TELDOMICILE,
            this.CLT_EMAIL});
            this.dgvListClients.Location = new System.Drawing.Point(18, 116);
            this.dgvListClients.Name = "dgvListClients";
            this.dgvListClients.ReadOnly = true;
            this.dgvListClients.Size = new System.Drawing.Size(944, 453);
            this.dgvListClients.TabIndex = 0;
            this.dgvListClients.DoubleClick += new System.EventHandler(this.StockageClient);
            // 
            // PSN_ID
            // 
            this.PSN_ID.HeaderText = "ID";
            this.PSN_ID.Name = "PSN_ID";
            this.PSN_ID.ReadOnly = true;
            this.PSN_ID.Visible = false;
            // 
            // PSN_NOM
            // 
            this.PSN_NOM.HeaderText = "Nom";
            this.PSN_NOM.Name = "PSN_NOM";
            this.PSN_NOM.ReadOnly = true;
            // 
            // PSN_PRENOM
            // 
            this.PSN_PRENOM.HeaderText = "Prenom";
            this.PSN_PRENOM.Name = "PSN_PRENOM";
            this.PSN_PRENOM.ReadOnly = true;
            // 
            // PSN_ADRESSE
            // 
            this.PSN_ADRESSE.HeaderText = "Adresse";
            this.PSN_ADRESSE.Name = "PSN_ADRESSE";
            this.PSN_ADRESSE.ReadOnly = true;
            // 
            // PSN_VILLE
            // 
            this.PSN_VILLE.HeaderText = "Ville";
            this.PSN_VILLE.Name = "PSN_VILLE";
            this.PSN_VILLE.ReadOnly = true;
            // 
            // PSN_CODEPOSTAL
            // 
            this.PSN_CODEPOSTAL.HeaderText = "Code postal";
            this.PSN_CODEPOSTAL.Name = "PSN_CODEPOSTAL";
            this.PSN_CODEPOSTAL.ReadOnly = true;
            // 
            // PSN_PAYS
            // 
            this.PSN_PAYS.HeaderText = "Pays";
            this.PSN_PAYS.Name = "PSN_PAYS";
            this.PSN_PAYS.ReadOnly = true;
            // 
            // PSN_MOBILE
            // 
            this.PSN_MOBILE.HeaderText = "Tel (mobile)";
            this.PSN_MOBILE.Name = "PSN_MOBILE";
            this.PSN_MOBILE.ReadOnly = true;
            // 
            // CLT_TELDOMICILE
            // 
            this.CLT_TELDOMICILE.HeaderText = "Tel (domicile)";
            this.CLT_TELDOMICILE.Name = "CLT_TELDOMICILE";
            this.CLT_TELDOMICILE.ReadOnly = true;
            // 
            // CLT_EMAIL
            // 
            this.CLT_EMAIL.HeaderText = "Mail";
            this.CLT_EMAIL.Name = "CLT_EMAIL";
            this.CLT_EMAIL.ReadOnly = true;
            // 
            // pnlMaterielAjouter
            // 
            this.pnlMaterielAjouter.BackColor = System.Drawing.Color.Transparent;
            this.pnlMaterielAjouter.Controls.Add(this.txtMaterielAjouterSérie);
            this.pnlMaterielAjouter.Controls.Add(label46);
            this.pnlMaterielAjouter.Controls.Add(this.gbMaterielAjouterContratCarac);
            this.pnlMaterielAjouter.Controls.Add(this.cbMaterielAjouterContrat);
            this.pnlMaterielAjouter.Controls.Add(label35);
            this.pnlMaterielAjouter.Controls.Add(this.label31);
            this.pnlMaterielAjouter.Controls.Add(this.cbMaterielAjouterModele);
            this.pnlMaterielAjouter.Controls.Add(this.cbMaterielAjouterType);
            this.pnlMaterielAjouter.Controls.Add(mTL_TYPELabel);
            this.pnlMaterielAjouter.Controls.Add(mTL_MODELELabel);
            this.pnlMaterielAjouter.Controls.Add(this.btnMaterielAjouterAjout);
            this.pnlMaterielAjouter.Location = new System.Drawing.Point(144, 80);
            this.pnlMaterielAjouter.Name = "pnlMaterielAjouter";
            this.pnlMaterielAjouter.Size = new System.Drawing.Size(980, 594);
            this.pnlMaterielAjouter.TabIndex = 17;
            // 
            // txtMaterielAjouterSérie
            // 
            this.txtMaterielAjouterSérie.Location = new System.Drawing.Point(480, 99);
            this.txtMaterielAjouterSérie.Name = "txtMaterielAjouterSérie";
            this.txtMaterielAjouterSérie.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterSérie.TabIndex = 26;
            this.txtMaterielAjouterSérie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Serie_KeyPress);
            this.txtMaterielAjouterSérie.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);
            // 
            // gbMaterielAjouterContratCarac
            // 
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratPrixNew);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label41);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratType);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratSignature);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratRenouvellement);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratEcheance);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label36);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label37);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label38);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.txtMaterielAjouterContratPrixOld);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label39);
            this.gbMaterielAjouterContratCarac.Controls.Add(this.label40);
            this.gbMaterielAjouterContratCarac.Location = new System.Drawing.Point(205, 301);
            this.gbMaterielAjouterContratCarac.Name = "gbMaterielAjouterContratCarac";
            this.gbMaterielAjouterContratCarac.Size = new System.Drawing.Size(570, 231);
            this.gbMaterielAjouterContratCarac.TabIndex = 24;
            this.gbMaterielAjouterContratCarac.TabStop = false;
            this.gbMaterielAjouterContratCarac.Text = "Caractéristiques";
            this.gbMaterielAjouterContratCarac.Visible = false;
            // 
            // txtMaterielAjouterContratPrixNew
            // 
            this.txtMaterielAjouterContratPrixNew.Enabled = false;
            this.txtMaterielAjouterContratPrixNew.Location = new System.Drawing.Point(427, 193);
            this.txtMaterielAjouterContratPrixNew.Name = "txtMaterielAjouterContratPrixNew";
            this.txtMaterielAjouterContratPrixNew.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratPrixNew.TabIndex = 23;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(405, 196);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(16, 13);
            this.label41.TabIndex = 22;
            this.label41.Text = "->";
            // 
            // txtMaterielAjouterContratType
            // 
            this.txtMaterielAjouterContratType.Enabled = false;
            this.txtMaterielAjouterContratType.Location = new System.Drawing.Point(275, 35);
            this.txtMaterielAjouterContratType.Name = "txtMaterielAjouterContratType";
            this.txtMaterielAjouterContratType.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratType.TabIndex = 21;
            // 
            // txtMaterielAjouterContratSignature
            // 
            this.txtMaterielAjouterContratSignature.Enabled = false;
            this.txtMaterielAjouterContratSignature.Location = new System.Drawing.Point(275, 155);
            this.txtMaterielAjouterContratSignature.Name = "txtMaterielAjouterContratSignature";
            this.txtMaterielAjouterContratSignature.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratSignature.TabIndex = 20;
            // 
            // txtMaterielAjouterContratRenouvellement
            // 
            this.txtMaterielAjouterContratRenouvellement.Enabled = false;
            this.txtMaterielAjouterContratRenouvellement.Location = new System.Drawing.Point(275, 114);
            this.txtMaterielAjouterContratRenouvellement.Name = "txtMaterielAjouterContratRenouvellement";
            this.txtMaterielAjouterContratRenouvellement.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratRenouvellement.TabIndex = 19;
            // 
            // txtMaterielAjouterContratEcheance
            // 
            this.txtMaterielAjouterContratEcheance.Enabled = false;
            this.txtMaterielAjouterContratEcheance.Location = new System.Drawing.Point(275, 73);
            this.txtMaterielAjouterContratEcheance.Name = "txtMaterielAjouterContratEcheance";
            this.txtMaterielAjouterContratEcheance.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratEcheance.TabIndex = 18;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(205, 192);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(24, 13);
            this.label36.TabIndex = 13;
            this.label36.Text = "Prix";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(117, 154);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(112, 13);
            this.label37.TabIndex = 12;
            this.label37.Text = "Date signature contrat";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(124, 113);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(105, 13);
            this.label38.TabIndex = 11;
            this.label38.Text = "Date renouvellement";
            // 
            // txtMaterielAjouterContratPrixOld
            // 
            this.txtMaterielAjouterContratPrixOld.Enabled = false;
            this.txtMaterielAjouterContratPrixOld.Location = new System.Drawing.Point(275, 193);
            this.txtMaterielAjouterContratPrixOld.Name = "txtMaterielAjouterContratPrixOld";
            this.txtMaterielAjouterContratPrixOld.Size = new System.Drawing.Size(121, 20);
            this.txtMaterielAjouterContratPrixOld.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(173, 72);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Echéance";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(198, 39);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 13);
            this.label40.TabIndex = 9;
            this.label40.Text = "Type";
            // 
            // cbMaterielAjouterContrat
            // 
            this.cbMaterielAjouterContrat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielAjouterContrat.Enabled = false;
            this.cbMaterielAjouterContrat.FormattingEnabled = true;
            this.cbMaterielAjouterContrat.Location = new System.Drawing.Point(480, 240);
            this.cbMaterielAjouterContrat.Name = "cbMaterielAjouterContrat";
            this.cbMaterielAjouterContrat.Size = new System.Drawing.Size(121, 21);
            this.cbMaterielAjouterContrat.TabIndex = 23;
            this.cbMaterielAjouterContrat.SelectedIndexChanged += new System.EventHandler(this.cbMaterielAjouterContrat_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(443, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(94, 13);
            this.label31.TabIndex = 21;
            this.label31.Text = "Ajouter un materiel";
            // 
            // cbMaterielAjouterModele
            // 
            this.cbMaterielAjouterModele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielAjouterModele.Enabled = false;
            this.cbMaterielAjouterModele.FormattingEnabled = true;
            this.cbMaterielAjouterModele.Location = new System.Drawing.Point(480, 192);
            this.cbMaterielAjouterModele.Name = "cbMaterielAjouterModele";
            this.cbMaterielAjouterModele.Size = new System.Drawing.Size(121, 21);
            this.cbMaterielAjouterModele.TabIndex = 20;
            this.cbMaterielAjouterModele.SelectedIndexChanged += new System.EventHandler(this.cbMaterielAjouterModele_SelectedIndexChanged);
            // 
            // cbMaterielAjouterType
            // 
            this.cbMaterielAjouterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielAjouterType.FormattingEnabled = true;
            this.cbMaterielAjouterType.Location = new System.Drawing.Point(480, 144);
            this.cbMaterielAjouterType.Name = "cbMaterielAjouterType";
            this.cbMaterielAjouterType.Size = new System.Drawing.Size(121, 21);
            this.cbMaterielAjouterType.TabIndex = 19;
            this.cbMaterielAjouterType.SelectedIndexChanged += new System.EventHandler(this.cbMaterielAjouterType_SelectedIndexChanged);
            // 
            // btnMaterielAjouterAjout
            // 
            this.btnMaterielAjouterAjout.Location = new System.Drawing.Point(480, 546);
            this.btnMaterielAjouterAjout.Name = "btnMaterielAjouterAjout";
            this.btnMaterielAjouterAjout.Size = new System.Drawing.Size(121, 23);
            this.btnMaterielAjouterAjout.TabIndex = 12;
            this.btnMaterielAjouterAjout.Text = "Ajouter";
            this.btnMaterielAjouterAjout.UseVisualStyleBackColor = true;
            this.btnMaterielAjouterAjout.Click += new System.EventHandler(this.btnMaterielAjouterAjout_Click);
            // 
            // tbClientRechercheID
            // 
            this.tbClientRechercheID.Location = new System.Drawing.Point(287, 26);
            this.tbClientRechercheID.Name = "tbClientRechercheID";
            this.tbClientRechercheID.Size = new System.Drawing.Size(100, 20);
            this.tbClientRechercheID.TabIndex = 4;
            this.tbClientRechercheID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RechercheKeyPress);
            // 
            // tbClientRechercheContrat
            // 
            this.tbClientRechercheContrat.Location = new System.Drawing.Point(602, 26);
            this.tbClientRechercheContrat.Name = "tbClientRechercheContrat";
            this.tbClientRechercheContrat.Size = new System.Drawing.Size(100, 20);
            this.tbClientRechercheContrat.TabIndex = 5;
            this.tbClientRechercheContrat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RechercheKeyPress);
            // 
            // tbClientRechercheNom
            // 
            this.tbClientRechercheNom.Location = new System.Drawing.Point(894, 27);
            this.tbClientRechercheNom.Name = "tbClientRechercheNom";
            this.tbClientRechercheNom.Size = new System.Drawing.Size(100, 20);
            this.tbClientRechercheNom.TabIndex = 6;
            this.tbClientRechercheNom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RechercheKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(228, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "ID Client :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(534, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "N° Contrat :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(824, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nom Client :";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Felix Titling", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Image = global::IHM.Properties.Resources.onglet;
            this.label5.Location = new System.Drawing.Point(0, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 52);
            this.label5.TabIndex = 11;
            this.label5.Text = "Materiel";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlMaterielConsulter
            // 
            this.pnlMaterielConsulter.BackColor = System.Drawing.Color.Transparent;
            this.pnlMaterielConsulter.Controls.Add(this.pnlMaterielConsulterDgvModif);
            this.pnlMaterielConsulter.Controls.Add(this.pnlMaterielConsulterInstallation);
            this.pnlMaterielConsulter.Controls.Add(this.pnlMaterielConsulterSerie);
            this.pnlMaterielConsulter.Controls.Add(this.dgvMaterielConsulter);
            this.pnlMaterielConsulter.Controls.Add(this.pnlMaterielConsulterContrat);
            this.pnlMaterielConsulter.Controls.Add(this.cbMaterielConsulterRecherche);
            this.pnlMaterielConsulter.Controls.Add(this.label32);
            this.pnlMaterielConsulter.Location = new System.Drawing.Point(144, 80);
            this.pnlMaterielConsulter.Name = "pnlMaterielConsulter";
            this.pnlMaterielConsulter.Size = new System.Drawing.Size(980, 594);
            this.pnlMaterielConsulter.TabIndex = 1;
            // 
            // pnlMaterielConsulterDgvModif
            // 
            this.pnlMaterielConsulterDgvModif.Controls.Add(this.btnMaterielConsulterDgvModifSave);
            this.pnlMaterielConsulterDgvModif.Controls.Add(this.dtMaterielConsulterDgvModifInstallation);
            this.pnlMaterielConsulterDgvModif.Controls.Add(label43);
            this.pnlMaterielConsulterDgvModif.Controls.Add(this.cbMaterielConsulterDgvModifModele);
            this.pnlMaterielConsulterDgvModif.Controls.Add(this.cbMaterielConsulterDgvModifType);
            this.pnlMaterielConsulterDgvModif.Controls.Add(label34);
            this.pnlMaterielConsulterDgvModif.Controls.Add(label42);
            this.pnlMaterielConsulterDgvModif.Location = new System.Drawing.Point(759, 184);
            this.pnlMaterielConsulterDgvModif.Name = "pnlMaterielConsulterDgvModif";
            this.pnlMaterielConsulterDgvModif.Size = new System.Drawing.Size(200, 383);
            this.pnlMaterielConsulterDgvModif.TabIndex = 33;
            this.pnlMaterielConsulterDgvModif.Visible = false;
            // 
            // btnMaterielConsulterDgvModifSave
            // 
            this.btnMaterielConsulterDgvModifSave.Location = new System.Drawing.Point(51, 291);
            this.btnMaterielConsulterDgvModifSave.Name = "btnMaterielConsulterDgvModifSave";
            this.btnMaterielConsulterDgvModifSave.Size = new System.Drawing.Size(99, 23);
            this.btnMaterielConsulterDgvModifSave.TabIndex = 27;
            this.btnMaterielConsulterDgvModifSave.Text = "Enregistrer";
            this.btnMaterielConsulterDgvModifSave.UseVisualStyleBackColor = true;
            this.btnMaterielConsulterDgvModifSave.Click += new System.EventHandler(this.btnMaterielConsulterDgvModifSave_Click);
            // 
            // dtMaterielConsulterDgvModifInstallation
            // 
            this.dtMaterielConsulterDgvModifInstallation.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtMaterielConsulterDgvModifInstallation.Location = new System.Drawing.Point(97, 170);
            this.dtMaterielConsulterDgvModifInstallation.Name = "dtMaterielConsulterDgvModifInstallation";
            this.dtMaterielConsulterDgvModifInstallation.Size = new System.Drawing.Size(95, 20);
            this.dtMaterielConsulterDgvModifInstallation.TabIndex = 26;
            // 
            // cbMaterielConsulterDgvModifModele
            // 
            this.cbMaterielConsulterDgvModifModele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterDgvModifModele.FormattingEnabled = true;
            this.cbMaterielConsulterDgvModifModele.Location = new System.Drawing.Point(67, 94);
            this.cbMaterielConsulterDgvModifModele.Name = "cbMaterielConsulterDgvModifModele";
            this.cbMaterielConsulterDgvModifModele.Size = new System.Drawing.Size(125, 21);
            this.cbMaterielConsulterDgvModifModele.TabIndex = 24;
            // 
            // cbMaterielConsulterDgvModifType
            // 
            this.cbMaterielConsulterDgvModifType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterDgvModifType.FormattingEnabled = true;
            this.cbMaterielConsulterDgvModifType.Location = new System.Drawing.Point(67, 9);
            this.cbMaterielConsulterDgvModifType.Name = "cbMaterielConsulterDgvModifType";
            this.cbMaterielConsulterDgvModifType.Size = new System.Drawing.Size(125, 21);
            this.cbMaterielConsulterDgvModifType.TabIndex = 23;
            this.cbMaterielConsulterDgvModifType.SelectedIndexChanged += new System.EventHandler(this.cbMaterielConsulterDgvModifType_SelectedIndexChanged);
            // 
            // pnlMaterielConsulterInstallation
            // 
            this.pnlMaterielConsulterInstallation.Controls.Add(this.pnlMaterielConsulterInstallationDate);
            this.pnlMaterielConsulterInstallation.Controls.Add(this.pnlMaterielConsulterInstallationSansDate);
            this.pnlMaterielConsulterInstallation.Controls.Add(this.dtMaterielConsulterDeb);
            this.pnlMaterielConsulterInstallation.Controls.Add(this.cbMaterielConsulterInstallation);
            this.pnlMaterielConsulterInstallation.Location = new System.Drawing.Point(251, 79);
            this.pnlMaterielConsulterInstallation.Name = "pnlMaterielConsulterInstallation";
            this.pnlMaterielConsulterInstallation.Size = new System.Drawing.Size(622, 55);
            this.pnlMaterielConsulterInstallation.TabIndex = 31;
            this.pnlMaterielConsulterInstallation.Visible = false;
            // 
            // pnlMaterielConsulterInstallationDate
            // 
            this.pnlMaterielConsulterInstallationDate.Controls.Add(this.button2);
            this.pnlMaterielConsulterInstallationDate.Controls.Add(this.label33);
            this.pnlMaterielConsulterInstallationDate.Controls.Add(this.dtMaterielConsulterFin);
            this.pnlMaterielConsulterInstallationDate.Location = new System.Drawing.Point(246, 3);
            this.pnlMaterielConsulterInstallationDate.Name = "pnlMaterielConsulterInstallationDate";
            this.pnlMaterielConsulterInstallationDate.Size = new System.Drawing.Size(217, 42);
            this.pnlMaterielConsulterInstallationDate.TabIndex = 2;
            this.pnlMaterielConsulterInstallationDate.Visible = false;
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::IHM.Properties.Resources.rechercher;
            this.button2.Location = new System.Drawing.Point(127, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 33);
            this.button2.TabIndex = 25;
            this.button2.Tag = "clr";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.MaterielConsulterRechercheByDate);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 17);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(19, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "au";
            // 
            // dtMaterielConsulterFin
            // 
            this.dtMaterielConsulterFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtMaterielConsulterFin.Location = new System.Drawing.Point(28, 13);
            this.dtMaterielConsulterFin.Name = "dtMaterielConsulterFin";
            this.dtMaterielConsulterFin.Size = new System.Drawing.Size(100, 20);
            this.dtMaterielConsulterFin.TabIndex = 3;
            // 
            // pnlMaterielConsulterInstallationSansDate
            // 
            this.pnlMaterielConsulterInstallationSansDate.Controls.Add(this.button1);
            this.pnlMaterielConsulterInstallationSansDate.Location = new System.Drawing.Point(246, 4);
            this.pnlMaterielConsulterInstallationSansDate.Name = "pnlMaterielConsulterInstallationSansDate";
            this.pnlMaterielConsulterInstallationSansDate.Size = new System.Drawing.Size(217, 41);
            this.pnlMaterielConsulterInstallationSansDate.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::IHM.Properties.Resources.rechercher;
            this.button1.Location = new System.Drawing.Point(1, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 36);
            this.button1.TabIndex = 24;
            this.button1.Tag = "clr";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.MaterielConsulterRechercheByDate);
            // 
            // dtMaterielConsulterDeb
            // 
            this.dtMaterielConsulterDeb.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtMaterielConsulterDeb.Location = new System.Drawing.Point(140, 16);
            this.dtMaterielConsulterDeb.Name = "dtMaterielConsulterDeb";
            this.dtMaterielConsulterDeb.Size = new System.Drawing.Size(100, 20);
            this.dtMaterielConsulterDeb.TabIndex = 1;
            // 
            // cbMaterielConsulterInstallation
            // 
            this.cbMaterielConsulterInstallation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterInstallation.FormattingEnabled = true;
            this.cbMaterielConsulterInstallation.Items.AddRange(new object[] {
            "Installé le",
            "Installé depuis le",
            "Installer jusqu\'au",
            "Installé du"});
            this.cbMaterielConsulterInstallation.Location = new System.Drawing.Point(17, 16);
            this.cbMaterielConsulterInstallation.Name = "cbMaterielConsulterInstallation";
            this.cbMaterielConsulterInstallation.Size = new System.Drawing.Size(99, 21);
            this.cbMaterielConsulterInstallation.TabIndex = 0;
            this.cbMaterielConsulterInstallation.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // pnlMaterielConsulterSerie
            // 
            this.pnlMaterielConsulterSerie.Controls.Add(this.cbMaterielConsulterSerie);
            this.pnlMaterielConsulterSerie.Location = new System.Drawing.Point(251, 79);
            this.pnlMaterielConsulterSerie.Name = "pnlMaterielConsulterSerie";
            this.pnlMaterielConsulterSerie.Size = new System.Drawing.Size(622, 55);
            this.pnlMaterielConsulterSerie.TabIndex = 29;
            this.pnlMaterielConsulterSerie.Visible = false;
            // 
            // cbMaterielConsulterSerie
            // 
            this.cbMaterielConsulterSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterSerie.FormattingEnabled = true;
            this.cbMaterielConsulterSerie.Location = new System.Drawing.Point(17, 17);
            this.cbMaterielConsulterSerie.Name = "cbMaterielConsulterSerie";
            this.cbMaterielConsulterSerie.Size = new System.Drawing.Size(269, 21);
            this.cbMaterielConsulterSerie.TabIndex = 0;
            this.cbMaterielConsulterSerie.SelectedIndexChanged += new System.EventHandler(this.cbMaterielConsulterSerie_SelectedIndexChanged);
            // 
            // dgvMaterielConsulter
            // 
            this.dgvMaterielConsulter.AllowUserToAddRows = false;
            this.dgvMaterielConsulter.AllowUserToDeleteRows = false;
            this.dgvMaterielConsulter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterielConsulter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MTL_ID,
            this.CT_NUM_CONTRAT,
            this.Type,
            this.Modele,
            this.MTL_DATE_INSTALLATION,
            this.MTL_PRIX_ACHAT});
            this.dgvMaterielConsulter.Location = new System.Drawing.Point(107, 184);
            this.dgvMaterielConsulter.Name = "dgvMaterielConsulter";
            this.dgvMaterielConsulter.Size = new System.Drawing.Size(646, 383);
            this.dgvMaterielConsulter.TabIndex = 32;
            this.dgvMaterielConsulter.DoubleClick += new System.EventHandler(this.dgvMaterielConsulter_DoubleClick);
            // 
            // MTL_ID
            // 
            this.MTL_ID.HeaderText = "N° de série";
            this.MTL_ID.Name = "MTL_ID";
            this.MTL_ID.ReadOnly = true;
            // 
            // CT_NUM_CONTRAT
            // 
            this.CT_NUM_CONTRAT.HeaderText = "N° de contrat";
            this.CT_NUM_CONTRAT.Name = "CT_NUM_CONTRAT";
            this.CT_NUM_CONTRAT.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Modele
            // 
            this.Modele.HeaderText = "Modele";
            this.Modele.Name = "Modele";
            this.Modele.ReadOnly = true;
            this.Modele.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // MTL_DATE_INSTALLATION
            // 
            this.MTL_DATE_INSTALLATION.HeaderText = "Date d\'installation";
            this.MTL_DATE_INSTALLATION.Name = "MTL_DATE_INSTALLATION";
            this.MTL_DATE_INSTALLATION.ReadOnly = true;
            // 
            // MTL_PRIX_ACHAT
            // 
            this.MTL_PRIX_ACHAT.HeaderText = "Prix";
            this.MTL_PRIX_ACHAT.Name = "MTL_PRIX_ACHAT";
            this.MTL_PRIX_ACHAT.ReadOnly = true;
            // 
            // pnlMaterielConsulterContrat
            // 
            this.pnlMaterielConsulterContrat.Controls.Add(this.cbMaterielConsulterContrat);
            this.pnlMaterielConsulterContrat.Location = new System.Drawing.Point(251, 79);
            this.pnlMaterielConsulterContrat.Name = "pnlMaterielConsulterContrat";
            this.pnlMaterielConsulterContrat.Size = new System.Drawing.Size(622, 55);
            this.pnlMaterielConsulterContrat.TabIndex = 30;
            this.pnlMaterielConsulterContrat.Visible = false;
            // 
            // cbMaterielConsulterContrat
            // 
            this.cbMaterielConsulterContrat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterContrat.FormattingEnabled = true;
            this.cbMaterielConsulterContrat.Location = new System.Drawing.Point(17, 17);
            this.cbMaterielConsulterContrat.Name = "cbMaterielConsulterContrat";
            this.cbMaterielConsulterContrat.Size = new System.Drawing.Size(269, 21);
            this.cbMaterielConsulterContrat.TabIndex = 1;
            this.cbMaterielConsulterContrat.SelectedIndexChanged += new System.EventHandler(this.cbMaterielConsulterContrat_SelectedIndexChanged);
            // 
            // cbMaterielConsulterRecherche
            // 
            this.cbMaterielConsulterRecherche.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterielConsulterRecherche.FormattingEnabled = true;
            this.cbMaterielConsulterRecherche.Items.AddRange(new object[] {
            "N° de série",
            "N° de contrat",
            "Date d\'installation"});
            this.cbMaterielConsulterRecherche.Location = new System.Drawing.Point(107, 96);
            this.cbMaterielConsulterRecherche.Name = "cbMaterielConsulterRecherche";
            this.cbMaterielConsulterRecherche.Size = new System.Drawing.Size(121, 21);
            this.cbMaterielConsulterRecherche.TabIndex = 28;
            this.cbMaterielConsulterRecherche.SelectedIndexChanged += new System.EventHandler(this.cbMaterielConsulterRecherche_SelectedIndexChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(394, 46);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(143, 13);
            this.label32.TabIndex = 27;
            this.label32.Text = "Consulter / Editer un materiel";
            // 
            // pnlContratConsulter
            // 
            this.pnlContratConsulter.BackColor = System.Drawing.Color.Transparent;
            this.pnlContratConsulter.Controls.Add(this.pnlContratConsulterRecherche);
            this.pnlContratConsulter.Controls.Add(this.label19);
            this.pnlContratConsulter.Controls.Add(this.cbContratConsulterRecherche);
            this.pnlContratConsulter.Location = new System.Drawing.Point(144, 80);
            this.pnlContratConsulter.Name = "pnlContratConsulter";
            this.pnlContratConsulter.Size = new System.Drawing.Size(980, 594);
            this.pnlContratConsulter.TabIndex = 1;
            // 
            // pnlContratConsulterRecherche
            // 
            this.pnlContratConsulterRecherche.Controls.Add(this.btnContratConsulterEnregistrer);
            this.pnlContratConsulterRecherche.Controls.Add(this.groupBox6);
            this.pnlContratConsulterRecherche.Controls.Add(this.groupBox5);
            this.pnlContratConsulterRecherche.Location = new System.Drawing.Point(18, 75);
            this.pnlContratConsulterRecherche.Name = "pnlContratConsulterRecherche";
            this.pnlContratConsulterRecherche.Size = new System.Drawing.Size(950, 516);
            this.pnlContratConsulterRecherche.TabIndex = 3;
            // 
            // btnContratConsulterEnregistrer
            // 
            this.btnContratConsulterEnregistrer.Location = new System.Drawing.Point(452, 463);
            this.btnContratConsulterEnregistrer.Name = "btnContratConsulterEnregistrer";
            this.btnContratConsulterEnregistrer.Size = new System.Drawing.Size(100, 31);
            this.btnContratConsulterEnregistrer.TabIndex = 58;
            this.btnContratConsulterEnregistrer.Text = "Prolonger d\'un an";
            this.btnContratConsulterEnregistrer.UseVisualStyleBackColor = true;
            this.btnContratConsulterEnregistrer.Click += new System.EventHandler(this.btnContratConsulterEnregistrer_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtContratConsulterType);
            this.groupBox6.Controls.Add(this.txtContratConsulterSignature);
            this.groupBox6.Controls.Add(this.txtContratConsulterRenouvellement);
            this.groupBox6.Controls.Add(this.txtContratConsulterEcheance);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txtContratConsulterPrix);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Location = new System.Drawing.Point(183, 157);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(603, 287);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Caractéristiques";
            // 
            // txtContratConsulterType
            // 
            this.txtContratConsulterType.Enabled = false;
            this.txtContratConsulterType.Location = new System.Drawing.Point(269, 30);
            this.txtContratConsulterType.Name = "txtContratConsulterType";
            this.txtContratConsulterType.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterType.TabIndex = 21;
            // 
            // txtContratConsulterSignature
            // 
            this.txtContratConsulterSignature.Enabled = false;
            this.txtContratConsulterSignature.Location = new System.Drawing.Point(269, 198);
            this.txtContratConsulterSignature.Name = "txtContratConsulterSignature";
            this.txtContratConsulterSignature.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterSignature.TabIndex = 20;
            // 
            // txtContratConsulterRenouvellement
            // 
            this.txtContratConsulterRenouvellement.Enabled = false;
            this.txtContratConsulterRenouvellement.Location = new System.Drawing.Point(269, 141);
            this.txtContratConsulterRenouvellement.Name = "txtContratConsulterRenouvellement";
            this.txtContratConsulterRenouvellement.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterRenouvellement.TabIndex = 19;
            // 
            // txtContratConsulterEcheance
            // 
            this.txtContratConsulterEcheance.Enabled = false;
            this.txtContratConsulterEcheance.Location = new System.Drawing.Point(269, 84);
            this.txtContratConsulterEcheance.Name = "txtContratConsulterEcheance";
            this.txtContratConsulterEcheance.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterEcheance.TabIndex = 18;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(199, 255);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(24, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "Prix";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(111, 201);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(112, 13);
            this.label27.TabIndex = 12;
            this.label27.Text = "Date signature contrat";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(118, 144);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 13);
            this.label26.TabIndex = 11;
            this.label26.Text = "Date renouvellement";
            // 
            // txtContratConsulterPrix
            // 
            this.txtContratConsulterPrix.Enabled = false;
            this.txtContratConsulterPrix.Location = new System.Drawing.Point(269, 252);
            this.txtContratConsulterPrix.Name = "txtContratConsulterPrix";
            this.txtContratConsulterPrix.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterPrix.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(167, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Echéance";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(192, 33);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 9;
            this.label23.Text = "Type";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtContratConsulterClient);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.txtContratConsulterCommercial);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(182, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(603, 139);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Intervenants";
            // 
            // txtContratConsulterClient
            // 
            this.txtContratConsulterClient.Enabled = false;
            this.txtContratConsulterClient.Location = new System.Drawing.Point(270, 87);
            this.txtContratConsulterClient.Name = "txtContratConsulterClient";
            this.txtContratConsulterClient.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterClient.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(191, 90);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 15;
            this.label30.Text = "Client";
            // 
            // txtContratConsulterCommercial
            // 
            this.txtContratConsulterCommercial.Enabled = false;
            this.txtContratConsulterCommercial.Location = new System.Drawing.Point(270, 33);
            this.txtContratConsulterCommercial.Name = "txtContratConsulterCommercial";
            this.txtContratConsulterCommercial.Size = new System.Drawing.Size(100, 20);
            this.txtContratConsulterCommercial.TabIndex = 3;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(163, 36);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 13);
            this.label29.TabIndex = 14;
            this.label29.Text = "Commercial";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(390, 36);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "N° Contrat";
            // 
            // cbContratConsulterRecherche
            // 
            this.cbContratConsulterRecherche.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbContratConsulterRecherche.FormattingEnabled = true;
            this.cbContratConsulterRecherche.Location = new System.Drawing.Point(463, 33);
            this.cbContratConsulterRecherche.Name = "cbContratConsulterRecherche";
            this.cbContratConsulterRecherche.Size = new System.Drawing.Size(121, 21);
            this.cbContratConsulterRecherche.TabIndex = 0;
            this.cbContratConsulterRecherche.SelectedIndexChanged += new System.EventHandler(this.cbContratConsulterRecherche_SelectedIndexChanged);
            // 
            // btnClientRechercheNom
            // 
            this.btnClientRechercheNom.BackColor = System.Drawing.Color.Transparent;
            this.btnClientRechercheNom.FlatAppearance.BorderSize = 0;
            this.btnClientRechercheNom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientRechercheNom.Image = global::IHM.Properties.Resources.rechercher;
            this.btnClientRechercheNom.Location = new System.Drawing.Point(995, 21);
            this.btnClientRechercheNom.Name = "btnClientRechercheNom";
            this.btnClientRechercheNom.Size = new System.Drawing.Size(35, 31);
            this.btnClientRechercheNom.TabIndex = 17;
            this.btnClientRechercheNom.Tag = "clr";
            this.btnClientRechercheNom.UseVisualStyleBackColor = false;
            this.btnClientRechercheNom.Click += new System.EventHandler(this.Recherche);
            // 
            // btnQuitter
            // 
            this.btnQuitter.BackColor = System.Drawing.Color.Transparent;
            this.btnQuitter.FlatAppearance.BorderSize = 0;
            this.btnQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuitter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnQuitter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnQuitter.Location = new System.Drawing.Point(21, 611);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(103, 45);
            this.btnQuitter.TabIndex = 18;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = false;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnClientRechercheContrat
            // 
            this.btnClientRechercheContrat.BackColor = System.Drawing.Color.Transparent;
            this.btnClientRechercheContrat.FlatAppearance.BorderSize = 0;
            this.btnClientRechercheContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientRechercheContrat.Image = global::IHM.Properties.Resources.rechercher;
            this.btnClientRechercheContrat.Location = new System.Drawing.Point(703, 17);
            this.btnClientRechercheContrat.Name = "btnClientRechercheContrat";
            this.btnClientRechercheContrat.Size = new System.Drawing.Size(32, 36);
            this.btnClientRechercheContrat.TabIndex = 19;
            this.btnClientRechercheContrat.Tag = "clr";
            this.btnClientRechercheContrat.UseVisualStyleBackColor = false;
            this.btnClientRechercheContrat.Click += new System.EventHandler(this.Recherche);
            // 
            // btnClientRechercheID
            // 
            this.btnClientRechercheID.BackColor = System.Drawing.Color.Transparent;
            this.btnClientRechercheID.FlatAppearance.BorderSize = 0;
            this.btnClientRechercheID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientRechercheID.Image = global::IHM.Properties.Resources.rechercher;
            this.btnClientRechercheID.Location = new System.Drawing.Point(390, 19);
            this.btnClientRechercheID.Name = "btnClientRechercheID";
            this.btnClientRechercheID.Size = new System.Drawing.Size(33, 33);
            this.btnClientRechercheID.TabIndex = 20;
            this.btnClientRechercheID.Tag = "clr";
            this.btnClientRechercheID.UseVisualStyleBackColor = false;
            this.btnClientRechercheID.Click += new System.EventHandler(this.Recherche);
            // 
            // lbClientRechercheID
            // 
            this.lbClientRechercheID.AutoSize = true;
            this.lbClientRechercheID.BackColor = System.Drawing.Color.Transparent;
            this.lbClientRechercheID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClientRechercheID.ForeColor = System.Drawing.Color.OrangeRed;
            this.lbClientRechercheID.Location = new System.Drawing.Point(223, 6);
            this.lbClientRechercheID.Name = "lbClientRechercheID";
            this.lbClientRechercheID.Size = new System.Drawing.Size(213, 13);
            this.lbClientRechercheID.TabIndex = 21;
            this.lbClientRechercheID.Text = "Aucun client ne correspond à cet ID";
            this.lbClientRechercheID.Visible = false;
            // 
            // lbClientRechercheContrat
            // 
            this.lbClientRechercheContrat.AutoSize = true;
            this.lbClientRechercheContrat.BackColor = System.Drawing.Color.Transparent;
            this.lbClientRechercheContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClientRechercheContrat.ForeColor = System.Drawing.Color.OrangeRed;
            this.lbClientRechercheContrat.Location = new System.Drawing.Point(500, 6);
            this.lbClientRechercheContrat.Name = "lbClientRechercheContrat";
            this.lbClientRechercheContrat.Size = new System.Drawing.Size(270, 13);
            this.lbClientRechercheContrat.TabIndex = 22;
            this.lbClientRechercheContrat.Text = "Aucun client ne correspond à ce n° de contrat";
            this.lbClientRechercheContrat.Visible = false;
            // 
            // lbClientRechercheNom
            // 
            this.lbClientRechercheNom.AutoSize = true;
            this.lbClientRechercheNom.BackColor = System.Drawing.Color.Transparent;
            this.lbClientRechercheNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClientRechercheNom.ForeColor = System.Drawing.Color.OrangeRed;
            this.lbClientRechercheNom.Location = new System.Drawing.Point(818, 6);
            this.lbClientRechercheNom.Name = "lbClientRechercheNom";
            this.lbClientRechercheNom.Size = new System.Drawing.Size(221, 13);
            this.lbClientRechercheNom.TabIndex = 23;
            this.lbClientRechercheNom.Text = "Aucun client ne correspond à ce Nom";
            this.lbClientRechercheNom.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnClientAjouter
            // 
            this.btnClientAjouter.BackColor = System.Drawing.Color.Transparent;
            this.btnClientAjouter.FlatAppearance.BorderSize = 0;
            this.btnClientAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientAjouter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClientAjouter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnClientAjouter.Location = new System.Drawing.Point(21, 144);
            this.btnClientAjouter.Name = "btnClientAjouter";
            this.btnClientAjouter.Size = new System.Drawing.Size(103, 39);
            this.btnClientAjouter.TabIndex = 25;
            this.btnClientAjouter.Tag = "cla";
            this.btnClientAjouter.Text = "Ajouter";
            this.btnClientAjouter.UseVisualStyleBackColor = false;
            this.btnClientAjouter.Click += new System.EventHandler(this.ChangerPanel);
            // 
            // btnClientConsulter
            // 
            this.btnClientConsulter.BackColor = System.Drawing.Color.Transparent;
            this.btnClientConsulter.FlatAppearance.BorderSize = 0;
            this.btnClientConsulter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientConsulter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClientConsulter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnClientConsulter.Location = new System.Drawing.Point(21, 184);
            this.btnClientConsulter.Name = "btnClientConsulter";
            this.btnClientConsulter.Size = new System.Drawing.Size(103, 39);
            this.btnClientConsulter.TabIndex = 26;
            this.btnClientConsulter.Tag = "clc";
            this.btnClientConsulter.Text = "Consulter";
            this.btnClientConsulter.UseVisualStyleBackColor = false;
            this.btnClientConsulter.Click += new System.EventHandler(this.ChangerPanel);
            // 
            // btnMaterielConsulter
            // 
            this.btnMaterielConsulter.BackColor = System.Drawing.Color.Transparent;
            this.btnMaterielConsulter.FlatAppearance.BorderSize = 0;
            this.btnMaterielConsulter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterielConsulter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMaterielConsulter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnMaterielConsulter.Location = new System.Drawing.Point(21, 310);
            this.btnMaterielConsulter.Name = "btnMaterielConsulter";
            this.btnMaterielConsulter.Size = new System.Drawing.Size(103, 39);
            this.btnMaterielConsulter.TabIndex = 28;
            this.btnMaterielConsulter.Tag = "mc";
            this.btnMaterielConsulter.Text = "Consulter";
            this.btnMaterielConsulter.UseVisualStyleBackColor = false;
            this.btnMaterielConsulter.Click += new System.EventHandler(this.ChangerPanel);
            // 
            // btnMaterielAjouter
            // 
            this.btnMaterielAjouter.BackColor = System.Drawing.Color.Transparent;
            this.btnMaterielAjouter.FlatAppearance.BorderSize = 0;
            this.btnMaterielAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterielAjouter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMaterielAjouter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnMaterielAjouter.Location = new System.Drawing.Point(21, 270);
            this.btnMaterielAjouter.Name = "btnMaterielAjouter";
            this.btnMaterielAjouter.Size = new System.Drawing.Size(103, 39);
            this.btnMaterielAjouter.TabIndex = 27;
            this.btnMaterielAjouter.Tag = "ma";
            this.btnMaterielAjouter.Text = "Ajouter";
            this.btnMaterielAjouter.UseVisualStyleBackColor = false;
            this.btnMaterielAjouter.Click += new System.EventHandler(this.ChangerPanel);
            // 
            // btnContratConsulter
            // 
            this.btnContratConsulter.BackColor = System.Drawing.Color.Transparent;
            this.btnContratConsulter.FlatAppearance.BorderSize = 0;
            this.btnContratConsulter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContratConsulter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnContratConsulter.Image = global::IHM.Properties.Resources.Bouton;
            this.btnContratConsulter.Location = new System.Drawing.Point(21, 398);
            this.btnContratConsulter.Name = "btnContratConsulter";
            this.btnContratConsulter.Size = new System.Drawing.Size(103, 39);
            this.btnContratConsulter.TabIndex = 29;
            this.btnContratConsulter.Tag = "coc";
            this.btnContratConsulter.Text = "Consulter";
            this.btnContratConsulter.UseVisualStyleBackColor = false;
            this.btnContratConsulter.Click += new System.EventHandler(this.ChangerPanel);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Felix Titling", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Image = global::IHM.Properties.Resources.onglet;
            this.label1.Location = new System.Drawing.Point(0, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 52);
            this.label1.TabIndex = 30;
            this.label1.Text = "Client";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Felix Titling", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Image = global::IHM.Properties.Resources.onglet;
            this.label6.Location = new System.Drawing.Point(0, 349);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 52);
            this.label6.TabIndex = 31;
            this.label6.Text = "Contrat";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IHMCommercial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::IHM.Properties.Resources.Background1;
            this.ClientSize = new System.Drawing.Size(1114, 682);
            this.Controls.Add(this.pnlMaterielConsulter);
            this.Controls.Add(this.pnlClientAjouter);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlMaterielAjouter);
            this.Controls.Add(this.pnlContratConsulter);
            this.Controls.Add(this.pnlClientRechercher);
            this.Controls.Add(this.pnlClientConsulter);
            this.Controls.Add(this.btnContratConsulter);
            this.Controls.Add(this.btnMaterielConsulter);
            this.Controls.Add(this.btnMaterielAjouter);
            this.Controls.Add(this.btnClientConsulter);
            this.Controls.Add(this.btnClientAjouter);
            this.Controls.Add(this.lbClientRechercheNom);
            this.Controls.Add(this.lbClientRechercheContrat);
            this.Controls.Add(this.lbClientRechercheID);
            this.Controls.Add(this.btnClientRechercheID);
            this.Controls.Add(this.btnClientRechercheContrat);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnClientRechercheNom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbClientRechercheNom);
            this.Controls.Add(this.tbClientRechercheContrat);
            this.Controls.Add(this.tbClientRechercheID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "IHMCommercial";
            this.Text = "IHMCommercial";
            this.Load += new System.EventHandler(this.IHMCommercial_Load);
            this.pnlClientAjouter.ResumeLayout(false);
            this.pnlClientAjouter.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.pnlClientConsulter.ResumeLayout(false);
            this.pnlClientConsulter.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlClientRechercher.ResumeLayout(false);
            this.pnlClientRechercher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListClients)).EndInit();
            this.pnlMaterielAjouter.ResumeLayout(false);
            this.pnlMaterielAjouter.PerformLayout();
            this.gbMaterielAjouterContratCarac.ResumeLayout(false);
            this.gbMaterielAjouterContratCarac.PerformLayout();
            this.pnlMaterielConsulter.ResumeLayout(false);
            this.pnlMaterielConsulter.PerformLayout();
            this.pnlMaterielConsulterDgvModif.ResumeLayout(false);
            this.pnlMaterielConsulterDgvModif.PerformLayout();
            this.pnlMaterielConsulterInstallation.ResumeLayout(false);
            this.pnlMaterielConsulterInstallationDate.ResumeLayout(false);
            this.pnlMaterielConsulterInstallationDate.PerformLayout();
            this.pnlMaterielConsulterInstallationSansDate.ResumeLayout(false);
            this.pnlMaterielConsulterSerie.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterielConsulter)).EndInit();
            this.pnlMaterielConsulterContrat.ResumeLayout(false);
            this.pnlContratConsulter.ResumeLayout(false);
            this.pnlContratConsulter.PerformLayout();
            this.pnlContratConsulterRecherche.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlClientAjouter;
        private System.Windows.Forms.TextBox tbClientRechercheID;
        private System.Windows.Forms.TextBox tbClientRechercheContrat;
        private System.Windows.Forms.TextBox tbClientRechercheNom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlClientRechercher;
        private System.Windows.Forms.Panel pnlClientConsulter;
        private System.Windows.Forms.Button btnClientAjouterAjout;
        private System.Windows.Forms.Panel pnlMaterielAjouter;
        private System.Windows.Forms.Panel pnlMaterielConsulter;
        private System.Windows.Forms.Panel pnlContratConsulter;
        private System.Windows.Forms.Button btnMaterielAjouterAjout;
        private System.Windows.Forms.Button btnClientRechercheNom;
        private System.Windows.Forms.Button btnClientConsulterEnregistrer;
        private System.Windows.Forms.Button btnClientConsulterModifier;
        private System.Windows.Forms.DataGridView dgvListClients;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtClientAjouterDeplacement;
        private System.Windows.Forms.TextBox txtClientAjouterPays;
        private System.Windows.Forms.TextBox txtClientAjouterDistance;
        private System.Windows.Forms.TextBox txtClientAjouterCodePostal;
        private System.Windows.Forms.TextBox txtClientAjouterAdresse;
        private System.Windows.Forms.TextBox txtClientAjouterVille;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtClientAjouterEmail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cbMaterielAjouterModele;
        private System.Windows.Forms.ComboBox cbMaterielAjouterType;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btnClientRechercheContrat;
        private System.Windows.Forms.Button btnClientRechercheID;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtClientConsulterDeplacement;
        private System.Windows.Forms.TextBox txtClientConsulterPays;
        private System.Windows.Forms.TextBox txtClientConsulterDistance;
        private System.Windows.Forms.TextBox txtClientConsulterCodePostal;
        private System.Windows.Forms.TextBox txtClientConsulterAdresse;
        private System.Windows.Forms.TextBox txtClientConsulterVille;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtClientConsulterEmail;
        private System.Windows.Forms.Label lbClientRechercheID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_NOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_PRENOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_ADRESSE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_VILLE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_CODEPOSTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_PAYS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PSN_MOBILE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLT_TELDOMICILE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLT_EMAIL;
        private System.Windows.Forms.Label lbClientRechercheContrat;
        private System.Windows.Forms.Label lbClientRechercheNom;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbContratConsulterRecherche;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel pnlContratConsulterRecherche;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtContratConsulterPrix;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtContratConsulterClient;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtContratConsulterCommercial;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btnContratConsulterEnregistrer;
        private System.Windows.Forms.TextBox txtContratConsulterSignature;
        private System.Windows.Forms.TextBox txtContratConsulterRenouvellement;
        private System.Windows.Forms.TextBox txtContratConsulterEcheance;
        private System.Windows.Forms.TextBox txtContratConsulterType;
        private System.Windows.Forms.Button btnClientConsulterAnnuler;
        private System.Windows.Forms.ComboBox cbMaterielAjouterContrat;
        private System.Windows.Forms.GroupBox gbMaterielAjouterContratCarac;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratType;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratSignature;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratRenouvellement;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratEcheance;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratPrixOld;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtMaterielAjouterContratPrixNew;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel pnlMaterielConsulterSerie;
        private System.Windows.Forms.ComboBox cbMaterielConsulterRecherche;
        private System.Windows.Forms.Panel pnlMaterielConsulterInstallation;
        private System.Windows.Forms.Panel pnlMaterielConsulterContrat;
        private System.Windows.Forms.ComboBox cbMaterielConsulterSerie;
        private System.Windows.Forms.ComboBox cbMaterielConsulterContrat;
        private System.Windows.Forms.ComboBox cbMaterielConsulterInstallation;
        private System.Windows.Forms.Panel pnlMaterielConsulterInstallationSansDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlMaterielConsulterInstallationDate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.DateTimePicker dtMaterielConsulterFin;
        private System.Windows.Forms.DateTimePicker dtMaterielConsulterDeb;
        private System.Windows.Forms.DataGridView dgvMaterielConsulter;
        private System.Windows.Forms.Panel pnlMaterielConsulterDgvModif;
        private System.Windows.Forms.DateTimePicker dtMaterielConsulterDgvModifInstallation;
        private System.Windows.Forms.ComboBox cbMaterielConsulterDgvModifModele;
        private System.Windows.Forms.ComboBox cbMaterielConsulterDgvModifType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MTL_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CT_NUM_CONTRAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modele;
        private System.Windows.Forms.DataGridViewTextBoxColumn MTL_DATE_INSTALLATION;
        private System.Windows.Forms.DataGridViewTextBoxColumn MTL_PRIX_ACHAT;
        private System.Windows.Forms.Button btnMaterielConsulterDgvModifSave;
        private System.Windows.Forms.TextBox txtClientConsulterPrenom;
        private System.Windows.Forms.TextBox txtClientConsulterNom;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtClientAjouterPrenom;
        private System.Windows.Forms.TextBox txtClientAjouterNom;
        private System.Windows.Forms.TextBox txtClientConsulterTelMob;
        private System.Windows.Forms.TextBox txtClientConsulterTelDom;
        private System.Windows.Forms.TextBox txtClientAjouterTelMobile;
        private System.Windows.Forms.TextBox txtClientAjouterTelDomicile;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtMaterielAjouterSérie;
        private System.Windows.Forms.Button btnClientConsulter;
        private System.Windows.Forms.Button btnClientAjouter;
        private System.Windows.Forms.Button btnMaterielConsulter;
        private System.Windows.Forms.Button btnMaterielAjouter;
        private System.Windows.Forms.Button btnContratConsulter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;

    }
}