﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Common;
using System.Configuration;
using Dataaccess;
using Metier.Entities;

namespace IHM
{
    public partial class IHMAuthentification : Form
    {
        DataAccess DA;

        public IHMAuthentification()
        {
            InitializeComponent();
            DA = new DataAccess();

            btnConnexion.MouseEnter += new EventHandler(btn_MouseEnter);
            btnConnexion.MouseLeave += new EventHandler(btn_MouseLeave);
            btnConnexion.MouseDown  += new MouseEventHandler(btn_MouseDown);
            btnConnexion.MouseUp    += new MouseEventHandler(btn_MouseUp);
        }

        void btn_MouseEnter(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Hover));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseLeave(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseDown(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Click));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        void btn_MouseUp(object sender, EventArgs e)
        {
            Button bouton = (Button)sender;
            bouton.Image = ((System.Drawing.Image)(Properties.Resources.Bouton_Hover));
            bouton.FlatStyle = FlatStyle.Flat;
            bouton.BackColor = Color.Transparent;
        }

        private void btnConnexion_Click(object sender, EventArgs e)
        {
            EntityConnexion EntityConnexion = DA.Connexion(txtID.Text, txtMdp.Text);
            if (!string.IsNullOrEmpty(EntityConnexion.CONN_PROFIL))
            {
                switch (EntityConnexion.CONN_PROFIL)
                {
                    case "COMMERCIAL":
                        IHMCommercial ihmCom = new IHMCommercial(EntityConnexion.PSN_ID);
                        ihmCom.ShowDialog();
                        break;

                    case "ASSISTANT":
                        IHM.IHMAssistant ihmAss = new IHM.IHMAssistant(EntityConnexion.PSN_ID);
                        ihmAss.ShowDialog();
                        break;

                    case "TECHNICIEN":
                        IHM.IHMTechnicien ihmtech = new IHM.IHMTechnicien(EntityConnexion.PSN_ID);
                        ihmtech.ShowDialog();
                        break;
                }
            }
            else
            {
                lbError.Text = "Identifiant / Mot de passe incorrect.";
                lbError.Visible = true;
            }

            
        }

        
    }
}
