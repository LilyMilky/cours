﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Metier.Entities;
using Dataaccess;

namespace IHM
{
    public partial class FicheIntervention : Form
    {
        public FicheIntervention()
        {
            InitializeComponent();
        }

        private string garantie = "";

        #region PROPRIETE
        public string txtDateAjout { get { return this.txtDateDebut.Text; } set { this.txtDateDebut.Text = value; } }

        public string NumeroContrat { get { return this.cbNumeroContrat.Text; } set { this.cbNumeroContrat.Text = value; } }

        public string DateFin { get {return this.txtDateFin.Text ;} set { this.txtDateFin.Text = value;} }

        public string Commentaire { get { return this.txtCommentaireTechnicien.Text; } set { this.txtCommentaireTechnicien.Text = value; } }

        public string DescriptionProbleme { get { return this.txtDescriptionProbleme.Text; } set { this.txtDescriptionProbleme.Text = value; } }

        public string NumClient { get { return this.txtNumClient.Text; } set { this.txtNumClient.Text = value; } }

        public string Nom { get { return this.txtNom.Text; } set { this.txtNom.Text = value; } }

        public string Adresse { get { return this.txtAdresse.Text; } set { this.txtAdresse.Text = value; } }

        public string Adresse2 { get { return this.txtAdresse2.Text; } set { this.txtAdresse2.Text = value; } }

        public string Telephone { get { return this.txtTelephone.Text; } set { this.txtTelephone.Text = value; } }

        public string Digicode { get { return this.txtDigicode.Text; } set { this.txtDigicode.Text = value; } }

        public string Garantie { get { return garantie; } set { garantie = value; } }

        public string NumMateriel { get { return this.txtNumMateriel.Text; } set { this.txtNumMateriel.Text = value; } }

        public string TypeMateriel { get { return this.txtTypeMateriel.Text; } set { this.txtTypeMateriel.Text = value; } }

        public string Modele { get { return this.txtModele.Text; } set { this.txtModele.Text = value; } }

        public string TypeContrat { get { return this.txtTypeContrat.Text; } set { this.txtTypeContrat.Text = value; } }

        public string KMParcouru { get { return this.txtKmParcouru.Text; } set { this.txtKmParcouru.Text = value; } }

        public string Quantite { get { return this.txtQuantite.Text; } set { this.txtQuantite.Text = value; } }

        public string Description { get { return this.txtDescription.Text; } set { this.txtDescription.Text = value; } }

        public string PrixUnitaire { get { return this.txtPrixUnitaire.Text; } set { this.txtPrixUnitaire.Text = value; } }

        public string MOPC { get { return this.txtMOPC.Text; } set { this.txtMOPC.Text = value; } }

        public string Montant { get { return this.txtMontant.Text; } set { this.txtMontant.Text = value; } }
        #endregion

        #region PROPRIETE ENABLED
        public bool txtDateAjoutEnabled { get { return this.txtDateDebut.Enabled; } set { this.txtDateDebut.Enabled = value; } }

        public bool NumeroContratEnabled { get { return this.cbNumeroContrat.Enabled; } set { this.cbNumeroContrat.Enabled = value; } }

        public bool DateFinEnabled { get { return this.txtDateFin.Enabled; } set { this.txtDateFin.Enabled = value; } }

        public bool CommentaireEnabled { get { return this.txtCommentaireTechnicien.Enabled; } set { this.txtCommentaireTechnicien.Enabled = value; } }

        public bool DescriptionProblemeEnabled { get { return this.txtDescriptionProbleme.Enabled; } set { this.txtDescriptionProbleme.Enabled = value; } }

        public bool NumMaterielEnabled { get { return this.txtNumMateriel.Enabled; } set { this.txtNumMateriel.Enabled = value; } }

        public bool GarantieRHGEnabled { get { return cbReparationHorsGarantie.Enabled; } set { cbReparationHorsGarantie.Enabled = value; } }

        public bool GarantieRSGEnabled { get { return cbReparationSousGarantie.Enabled; } set { cbReparationSousGarantie.Enabled = value; } }

        public bool GarantieVDCEnabled { get { return cbVisiteDeControle.Enabled; } set { cbVisiteDeControle.Enabled = value; } }

        public bool KMParcouruEnabled { get { return this.txtKmParcouru.Enabled; } set { this.txtKmParcouru.Enabled = value; } }

        public bool QuantiteEnabled { get { return this.txtQuantite.Enabled; } set { this.txtQuantite.Enabled = value; } }

        public bool DescriptionEnabled { get { return this.txtDescription.Enabled; } set { this.txtDescription.Enabled = value; } }

        public bool PrixUnitaireEnabled { get { return this.txtPrixUnitaire.Enabled; } set { this.txtPrixUnitaire.Enabled = value; } }

        public bool MOPCEnabled { get { return this.txtMOPC.Enabled; } set { this.txtMOPC.Enabled = value; } }

        #endregion

        public void AjoutCombo(List<string> NumContrats)
        {
            cbNumeroContrat.DataSource = NumContrats;
        }

        private void txtDateDebut_Leave(object sender, EventArgs e)
        {
            DateTime retour;
            if (!txtDateDebut.MaskCompleted || !DateTime.TryParse(txtDateDebut.Text, out retour))
            {
                label8.Visible = true;
                btnEnregistrer.Enabled = false;
            }
            else
            {
                label8.Visible = false;
                Activation(sender, e);
            }
        }

        private void Activation(object sender, EventArgs e)
        {
            if (txtDateDebut.MaskCompleted && !string.IsNullOrEmpty(txtDescriptionProbleme.Text) && (cbReparationHorsGarantie.Checked == true || cbReparationSousGarantie.Checked == true || cbVisiteDeControle.Checked == true) && (!string.IsNullOrEmpty(txtNumMateriel.Text)))
                btnEnregistrer.Enabled = true;
            else
                btnEnregistrer.Enabled = false;
        }

        private void Activation2(object sender, EventArgs e)
        {
            decimal result;

            if ((!string.IsNullOrEmpty(txtQuantite.Text) && !string.IsNullOrEmpty(txtPrixUnitaire.Text) && !string.IsNullOrEmpty(txtMOPC.Text)) || string.IsNullOrEmpty(txtQuantite.Text) && string.IsNullOrEmpty(txtPrixUnitaire.Text) && string.IsNullOrEmpty(txtMOPC.Text))
            {
                if (!string.IsNullOrEmpty(txtCommentaireTechnicien.Text) && txtDateFin.MaskCompleted && !string.IsNullOrEmpty(txtKmParcouru.Text) && Decimal.TryParse(txtKmParcouru.Text, out result))
                    btnEnregistrer.Enabled = true;
                else
                    btnEnregistrer.Enabled = false;
            }
            else
                btnEnregistrer.Enabled = false;
        }

        private void txtDateFin_Leave(object sender, EventArgs e)
        {
            DateTime retour;

            if (!txtDateFin.MaskCompleted || !DateTime.TryParse(txtDateFin.Text, out retour))
            {
                label10.Visible = true;
                btnEnregistrer.Enabled = false;
            }
            else
            {
                label10.Visible = false;
                Activation2(sender, e);
            }
        }

        public void cbNumeroContrat_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataAccess A = new DataAccess();

            txtTypeMateriel.Text = null;
            txtModele.Text = null;

            string NumContrat = cbNumeroContrat.Text;
            string ClientId = A.GetClientID(NumContrat);                 
            EntityClient Client = A.GetClient(ClientId);
            List<string> MaterielIds = A.GetIdMaterielByNumContrat(NumContrat);
            txtNumMateriel.DataSource = MaterielIds;
            txtNumMateriel.Text = null;

            txtNumClient.Text = Client.ClientID;
            txtNom.Text = Client.Nom;
            txtAdresse.Text = Client.Adresse;
            txtAdresse2.Text = Client.Ville + " " + Client.CodePostal;
            txtDigicode.Text = Client.Digicode;
            if (string.IsNullOrEmpty(Client.TelMobile))
                txtTelephone.Text = Client.TelMobile;
            else
                txtTelephone.Text = Client.TelDomicile;

            txtTypeContrat.Text = A.GetTypeContratByNumeroContrat(NumContrat);

            Activation(sender, e);
        }

        private void cbReparationSousGarantie_CheckedChanged(object sender, EventArgs e)
        {
            if (garantie != "RSG")
                garantie = "RSG";
            else
                garantie = "";

            Activation(sender, e);
        }

        private void cbReparationHorsGarantie_CheckedChanged(object sender, EventArgs e)
        {
            if (garantie != "RHG")
                garantie = "RHG";
            else
                garantie = "";

            Activation(sender, e);
        }

        private void cbVisiteDeControle_CheckedChanged(object sender, EventArgs e)
        {
            if (garantie != "VDC")
                garantie = "VDC";
            else
                garantie = "";

            Activation(sender, e);
        }

        public void txtNumMateriel_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataAccess A = new DataAccess();
            EntityMateriel Materiel = A.GetMaterielByMaterielId(txtNumMateriel.Text);
            txtTypeMateriel.Text = Materiel.Type;
            txtModele.Text = Materiel.Modele;

            Activation(sender,e);
        }

        public void SetNumMateriel(string MaterielId)
        {
            DataAccess A = new DataAccess();
            EntityMateriel Materiel = A.GetMaterielByMaterielId(MaterielId);
            txtNumMateriel.Text = MaterielId;
            txtTypeMateriel.Text = Materiel.Type;
            txtModele.Text = Materiel.Modele;

        }

        private void FicheIntervention_Load(object sender, EventArgs e)
        {
            switch(garantie)
            {
                case"RSG" : cbReparationSousGarantie.Checked = true;
                    break;
                case "RHG": cbReparationHorsGarantie.Checked = true;
                    break;
                case "VDC": cbVisiteDeControle.Checked = true;
                    break;
                default:
                    cbVisiteDeControle.Checked = false;
                    cbReparationSousGarantie.Checked =false;
                    cbReparationHorsGarantie.Checked = false;
                    break;
            }

            btnEnregistrer.Enabled = false;
            CalculMontant(sender, e);
        }

        private void CalculMontant(object sender, EventArgs e)
        {
            decimal result;

            if (!string.IsNullOrEmpty(txtPrixUnitaire.Text) && !string.IsNullOrEmpty(txtMOPC.Text) && !string.IsNullOrEmpty(txtQuantite.Text))
            {
                if (Decimal.TryParse(txtPrixUnitaire.Text, out result))
                {
                    if (Decimal.TryParse(txtMOPC.Text, out result))
                    {
                        if (Decimal.TryParse(txtQuantite.Text, out result))
                        {
                            label26.Visible = false;
                            label28.Visible = false;
                            label30.Visible = false;
                            txtMontant.Text = (((Convert.ToDecimal(txtPrixUnitaire.Text) + Convert.ToDecimal(txtMOPC.Text)) * Convert.ToDecimal(Quantite))).ToString();
                        }
                        else
                            label26.Visible = true;
                    }
                    else
                        label30.Visible = true;
                }
                else
                    label28.Visible = true;
            }
            else
                txtMontant.Text = "";

            Activation2(sender, e);
        }

        private bool BloquerChar(object sender, KeyPressEventArgs e)
        {
            TextBox TB = (TextBox)sender;
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                if (e.KeyChar == ',' && !TB.Text.Contains(','))
                    e.Handled = false;
                else
                    e.Handled = true;

            return e.Handled;
        }

        private bool BloquerChar2(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;

            return e.Handled;
        }

        private void txtQuantite_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = BloquerChar2(sender, e);

            if (e.Handled == true)
                label26.Visible = true;
            else
                label26.Visible = false;
        }

        private void txtKmParcouru_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = BloquerChar(sender, e);

            if (e.Handled == true)
                label27.Visible = true;
            else
                label27.Visible = false;
        }

        private void txtPrixUnitaire_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = BloquerChar(sender, e);

            if (e.Handled == true)
                label28.Visible = true;
            else
                label28.Visible = false;
        }

        private void txtMOPC_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = BloquerChar(sender, e);

            if (e.Handled == true)
                label30.Visible = true;
            else
                label30.Visible = false;
        }

        private void txtKmParcouru_Leave(object sender, EventArgs e)
        {
            decimal result;

            if (!Decimal.TryParse(txtKmParcouru.Text, out result))
                label27.Visible = true;
            else
                label27.Visible = false;
        }
    }
}
