﻿namespace IHM
{
    partial class IHMCommercialAjoutContrat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbAjoutContratType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dtAjoutContratSignature = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Créer un contrat";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbAjoutContratType
            // 
            this.cbAjoutContratType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAjoutContratType.FormattingEnabled = true;
            this.cbAjoutContratType.Location = new System.Drawing.Point(139, 66);
            this.cbAjoutContratType.Name = "cbAjoutContratType";
            this.cbAjoutContratType.Size = new System.Drawing.Size(99, 21);
            this.cbAjoutContratType.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Type";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(139, 173);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Créer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtAjoutContratSignature
            // 
            this.dtAjoutContratSignature.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAjoutContratSignature.Location = new System.Drawing.Point(139, 118);
            this.dtAjoutContratSignature.Name = "dtAjoutContratSignature";
            this.dtAjoutContratSignature.Size = new System.Drawing.Size(99, 20);
            this.dtAjoutContratSignature.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Date de signature";
            // 
            // IHMCommercialAjoutContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 233);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtAjoutContratSignature);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbAjoutContratType);
            this.Controls.Add(this.label1);
            this.Name = "IHMCommercialAjoutContrat";
            this.Text = "Ajouter un contrat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAjoutContratType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dtAjoutContratSignature;
        private System.Windows.Forms.Label label3;
    }
}