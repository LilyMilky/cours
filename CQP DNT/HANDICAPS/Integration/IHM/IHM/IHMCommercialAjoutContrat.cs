﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dataaccess;
using Metier.Entities;

namespace IHM
{
    public partial class IHMCommercialAjoutContrat : Form
    {
        string CommercialID;
        string ClientID;
        DataAccess DA;

        public IHMCommercialAjoutContrat(string commercialID, string clientID)
        {
            CommercialID = commercialID;
            ClientID = clientID;
            InitializeComponent();
            DA = new DataAccess();
            List<int> ContratCodes = DA.GetContratCodes();

            foreach (int code in ContratCodes)
            {
                cbAjoutContratType.Items.Add(code);
            }

            cbAjoutContratType.SelectedIndex = 0;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbAjoutContratType.SelectedItem.ToString() == "")
                MessageBox.Show("Veuillez renseigner le type du contrat");
            
            EntityContrat contrat = new EntityContrat();
            contrat.NumContrat = "";
            contrat.TypeContrat = Int32.Parse(cbAjoutContratType.SelectedItem.ToString());
            contrat.DateSignatureContrat = DateTime.Parse(dtAjoutContratSignature.Text);
            contrat.CommercialID = CommercialID;
            contrat.ClientID = ClientID;

            if (!DA.InsertContrat(contrat))
                MessageBox.Show("Une erreur est survenu pendant la création du contrat, veuillez consulter votre administrateur réseau");
          
                        
                
        }

    }
}
