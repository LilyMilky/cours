﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityContrat
    {
        public string NumContrat { get; set; }
        public string ClientID { get; set; }
        public string CommercialID { get; set; }
        public int TypeContrat { get; set; }
        public DateTime DateEcheance { get; set; }
        public DateTime? DateRenouvellement { get; set; }
        public DateTime DateSignatureContrat { get; set; }
        public decimal Prix { get; set; }
        public int Taux { get; set; }
    }
}
