﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityMateriel
    {
        public int MaterielId { get; set; }
        public string Type { get; set; }
        public string Modele { get; set; }
        public decimal Prix { get; set; }
    }
}
