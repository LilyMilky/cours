﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityIntervention
    {
        public string InterventionId { get; set; }
        public string NumContrat { get; set; }
        public string AssistantId { get; set; }
        public string TechnicienId { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateEnd { get; set; }
        public string DescriptionProbleme { get; set; }
        public string Commentaire { get; set; }
        public string Garantie { get; set; }
        public string MaterielId { get; set; }
        public decimal KMParcouru { get; set; }

        //Intervention_PIECE
        public int InterventionPieceId { get; set; }
        public int Quantite { get; set; }
        public string DescriptionPiece { get; set; }
        public decimal PrixUnitaire { get; set; }
        public decimal MainOeuvrePiece { get; set; }
        public decimal Montant { get; set; }
    }
}
