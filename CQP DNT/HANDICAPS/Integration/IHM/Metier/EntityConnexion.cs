﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityConnexion
    {
        public string CONN_LOGIN { get; set; }
        public string PSN_ID { get; set; }
        public string CONN_PWD { get; set; }
        public string CONN_PROFIL { get; set; }
    }
}
