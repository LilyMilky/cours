﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityInterventionPiece
    {
        public int InterventionPieceId { get; set; }
        public string InterventionId { get; set; }
        public int Quantite { get; set; }
        public string Description { get; set; }
        public decimal PrixUnitaire { get; set; }
        public decimal MOPC { get; set; }
        public decimal Montant { get; set; }
    }
}
