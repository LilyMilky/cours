﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityCommercial
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string Ville { get; set; }
        public string Antenne { get; set; }
        public string CodePostal { get; set; }
        public string Pays { get; set; }
        public string TelMobile { get; set; }
        public string TelDomicile { get; set; }
        public string DateEmbauche { get; set; }
        public string Email { get; set; }
        public string ID { get; set; }
    }
}
