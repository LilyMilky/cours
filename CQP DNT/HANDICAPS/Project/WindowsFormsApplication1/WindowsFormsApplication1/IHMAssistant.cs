﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DataAccess;
using Metier.Entities;
using Syd.ScheduleControls.Data;
using Syd.ScheduleControls.Events;
using Syd.ScheduleControls.Region;
using Syd.ScheduleControls.Test;
using Syd.ScheduleControls.Test.Dialog;

namespace WindowsFormsApplication1
{
    public partial class IHMAssistant : Form
    {
        private string assistantId;
        private string technicienId;

        public IHMAssistant(string AssistantId)
        {
            Access A = new Access();
            assistantId = AssistantId;
            technicienId = "T1";
            InitializeComponent();
            cbAssistantChoixTechnicien.DataSource = A.GetTechniciensId();
            MiseInvisiblePanel();
        }

        public IHMAssistant()
        {
            Access A = new Access();
            assistantId = "A1";
            technicienId = "T1";
            InitializeComponent();
            cbAssistantChoixTechnicien.DataSource = A.GetTechniciensId();
            MiseInvisiblePanel();
        }

        private void IHMAssistantConsulterClient_Load(object sender, EventArgs e)
        {
            MiseInvisiblePanel();

            weekScheduleControl1.Date = DateTime.Now;

        }

        private void MiseInvisiblePanel()
        {
            pnlAssistantConsulterClient.Visible = false;
            pnlAssistantConsulterContrat.Visible = false;
            pnlAssistantConsulterMateriel.Visible = false;
            pnlAssistantTechnicien.Visible = false;
        }

        #region CLIENT
        private void btnRechercheClient_Click(object sender, EventArgs e)
        {
            cLIENTDataGridView.Rows.Clear();
            Access A = new Access();
            label7.Visible = false;
            List<EntityClient> Clients = new List<EntityClient>();

            if (string.IsNullOrEmpty(txtAssistantClientNom.Text))
            {
                //Methode recherche tous les clients
                List<EntityClient> EntityClients = A.SelectClient();

                foreach (EntityClient EntityClient in EntityClients)
                {
                    cLIENTDataGridView.Rows.Add(EntityClient.ClientID, EntityClient.Nom, EntityClient.Prenom, EntityClient.Adresse, EntityClient.Ville, EntityClient.CodePostal, EntityClient.Pays, EntityClient.TelDomicile, EntityClient.TelMobile, EntityClient.DistanceAntenne, EntityClient.DureeDeplacement, EntityClient.Email);
                }
                cLIENTDataGridView.Visible = true;
            }
            else
            {
                DataAccess.Access Access = new DataAccess.Access();
                Clients = Access.SelectClient(txtAssistantClientNom.Text);

                //Si recuperation de données : cLIENTDataGridView.Visible = true; + Insertion données
                if (Clients.Count != 0)
                {                    
                    //Insertion Donnée
                    foreach (EntityClient EntityClient in Clients)
                    {
                        cLIENTDataGridView.Rows.Add(EntityClient.ClientID, EntityClient.Nom, EntityClient.Prenom, EntityClient.Adresse, EntityClient.Ville, EntityClient.CodePostal, EntityClient.Pays, EntityClient.TelDomicile, EntityClient.TelMobile, EntityClient.DistanceAntenne, EntityClient.DureeDeplacement, EntityClient.Email);
                    }
                    cLIENTDataGridView.Visible = true;
                } 
                else
                    label7.Visible = true;
            }
        }
        #endregion

        #region CONTRAT
        private void cLIENTDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            txtContratIDClient.Text = dg.CurrentRow.Cells[0].Value.ToString();
            MiseInvisiblePanel();
            pnlAssistantConsulterContrat.Visible = true;
        }

        private void btnRechercheContrat_Click(object sender, EventArgs e)
        {
            txtNumContrat.Rows.Clear();
            Access A = new Access();
            label6.Visible = false;
            List<EntityContrat> Contrats;

            if (string.IsNullOrEmpty(txtNumContra.Text) || txtNumContra.Text == "    -")
            {
                //Methode recherche tous les contrats
                Contrats = A.GetContrat();
                foreach (EntityContrat EntityContrat in Contrats)
                {
                    txtNumContrat.Rows.Add(EntityContrat.ClientID, EntityContrat.NumContrat, EntityContrat.TypeContrat, EntityContrat.DateEcheance, EntityContrat.DateRenouvellement, EntityContrat.DateSignatureContrat, EntityContrat.Prix);
                }
                txtNumContrat.Visible = true;
            }
            else
            {
                //Methode recherche client
                Contrats = A.GetContratByNumContrat(txtNumContra.Text);
                if (Contrats.Count != 0)
                {
                    foreach (EntityContrat EntityContrat in Contrats)
                    {
                        txtNumContrat.Rows.Add(EntityContrat.ClientID, EntityContrat.NumContrat, EntityContrat.TypeContrat, EntityContrat.DateEcheance, EntityContrat.DateRenouvellement, EntityContrat.DateSignatureContrat, EntityContrat.Prix);                       
                    }
                    txtNumContrat.Visible = true;
                }
                else
                    label6.Visible = true;
            }
        }

        private void btnRechercheIDClient_Click(object sender, EventArgs e)
        {
            txtNumContrat.Rows.Clear();
            label11.Visible = false;
            Access A = new Access();
            List<EntityContrat> Contrats;

            if (string.IsNullOrEmpty(txtContratIDClient.Text))
            {
                //Methode recherche tous les materiels
                Contrats = A.GetContrat();
                foreach (EntityContrat EntityContrat in Contrats)
                {
                    txtNumContrat.Rows.Add(EntityContrat.ClientID, EntityContrat.NumContrat, EntityContrat.TypeContrat, EntityContrat.DateEcheance, EntityContrat.DateRenouvellement, EntityContrat.DateSignatureContrat, EntityContrat.Prix);
                }
                txtNumContrat.Visible = true;
            }
            else
            {
                //Methode recherche client 
                Contrats = A.GetContratById(txtContratIDClient.Text);
                if (Contrats.Count == 0)
                    label11.Visible = true;
                else
                {
                    //Sinon : cONTRATDataGridView.Visible = true; + Insertion données
                    foreach (EntityContrat EntityContrat in Contrats)
                    {
                        txtNumContrat.Rows.Add(EntityContrat.ClientID, EntityContrat.NumContrat, EntityContrat.TypeContrat, EntityContrat.DateEcheance, EntityContrat.DateRenouvellement, EntityContrat.DateSignatureContrat, EntityContrat.Prix);
                    }
                    txtNumContrat.Visible = true;
                }
            }
        }
        #endregion

        #region MATERIEL
        private void cONTRATDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            txtAssistantConsulterMaterielContratlie.Text = dg.CurrentRow.Cells[1].Value.ToString();
            MiseInvisiblePanel();
            pnlAssistantConsulterMateriel.Visible = true;
        }
 
        private void btnAssistantConsulterMaterielValidationCONTRAT_Click(object sender, EventArgs e)
        {
            mATERIEL_ACHETEDataGridView.Rows.Clear();
            label8.Visible = false;
            Access A = new Access();
            List<EntityMaterielAchete> Materiels;

            if (string.IsNullOrEmpty(txtAssistantConsulterMaterielContratlie.Text) || txtAssistantConsulterMaterielContratlie.Text == "    -")
            {
                //Methode recherche tous les materiels
                Materiels = A.GetMateriel();

                foreach (EntityMaterielAchete Materiel in Materiels)
                {
                    mATERIEL_ACHETEDataGridView.Rows.Add(Materiel.Reference, Materiel.Contratlie, Materiel.DateInstallation, Materiel.PrixAchat);
                }
                mATERIEL_ACHETEDataGridView.Visible = true;
            }
            else
            {
                Materiels = A.GetMaterielByContrat(txtAssistantConsulterMaterielContratlie.Text);
                if(Materiels.Count != 0)
                {
                    foreach (EntityMaterielAchete Materiel in Materiels)
                    {
                        mATERIEL_ACHETEDataGridView.Rows.Add(Materiel.Reference, Materiel.Contratlie, Materiel.DateInstallation, Materiel.PrixAchat);
                    }
                    mATERIEL_ACHETEDataGridView.Visible = true;
                }
                else
                label8.Visible = true;
            }
        }

        private void btnAssistantConsulterMaterielValidationRef_Click(object sender, EventArgs e)
        {
            mATERIEL_ACHETEDataGridView.Rows.Clear();
            label9.Visible = false;
            Access A = new Access();
            List<EntityMaterielAchete> Materiels;

            if (string.IsNullOrEmpty(txtAssistantConsulterMaterielReference.Text))
            {
                //Methode recherche tous les materiels
                Materiels = A.GetMateriel();

                foreach (EntityMaterielAchete Materiel in Materiels)
                {
                    mATERIEL_ACHETEDataGridView.Rows.Add(Materiel.Reference, Materiel.Contratlie, Materiel.DateInstallation, Materiel.PrixAchat);
                }
                mATERIEL_ACHETEDataGridView.Visible = true;
            }
            else
            {
                Materiels = A.GetMaterielAcheteByRef(txtAssistantConsulterMaterielReference.Text);
                if (Materiels.Count != 0)
                {
                    foreach (EntityMaterielAchete Materiel in Materiels)
                    {
                        mATERIEL_ACHETEDataGridView.Rows.Add(Materiel.Reference, Materiel.Contratlie, Materiel.DateInstallation, Materiel.PrixAchat);
                    }
                    mATERIEL_ACHETEDataGridView.Visible = true;
                }
                else
                    label9.Visible = true;
            }
        }
        #endregion

        #region TECHNICIEN

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            weekScheduleControl1.Date = e.Start;
            weekScheduleControl1.RefreshAppointments();
            weekScheduleControl1.Invalidate();
        }  

        private void calendar_AppointmentAdd(object sender, AppointmentCreateEventArgs e)
        {
            bool Enable = false;
            Access A = new Access();
            List<EntityIntervention> Interventions= A.GetALLInterventionByDateDebut(Convert.ToDateTime(e.Date), technicienId);
            
            //show a dialog to add an appointment
            using (FicheIntervention dialog = new FicheIntervention())
            {
                if (e.Date != null)
                    dialog.txtDateAjout = e.Date.ToString();

                dialog.AjoutCombo(A.GetAllNumContrat());
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    foreach (EntityIntervention Intervention in Interventions)
                    {
                        //Test si la date de debut n'est pas comprise dans une autre intervention du technicien
                        if (Intervention.DateDebut <= Convert.ToDateTime(dialog.txtDateAjout) && Intervention.DateEnd >= Convert.ToDateTime(dialog.txtDateAjout))
                            Enable = true;
                            //Test si la date de fin n'est pas comprise dans une autre intervention du technicien
                        else if (Convert.ToDateTime(dialog.txtDateAjout).AddHours(1) >= Intervention.DateDebut && Convert.ToDateTime(dialog.txtDateAjout).AddHours(1)<= Intervention.DateEnd)
                            Enable = true;
                    }

                    string title = "Intervention";
                    DateTime dateStart = Convert.ToDateTime(dialog.txtDateAjout);
                    DateTime dateEnd = Convert.ToDateTime(dialog.txtDateAjout).AddHours(1);
                    if (((dateStart.Hour > 8 && dateEnd.Hour < 12) || (dateStart.Hour > 14 && dateEnd.Hour < 18)) && Enable == false)
                    {
                        e.Control.Appointments.Add(new ExtendedAppointment() { Subject = title, DateStart = dateStart, DateEnd = dateEnd });
                        A.InsertIntervention(assistantId, dialog.NumeroContrat, cbAssistantChoixTechnicien.Text, Convert.ToDateTime(dialog.txtDateAjout), dateEnd, dialog.DescriptionProbleme, dialog.Garantie, dialog.NumMateriel);

                        weekScheduleControl1.RefreshAppointments();
                        weekScheduleControl1.Invalidate();
                    }
                    else
                        MessageBox.Show("L'horaire n'est pas valide");
                }
            }
        }

        private void calendar_AppointmentMove(object sender, AppointmentMoveEventArgs e)
        {
            Access A = new Access();

            //show a dialog to move the appointment date
            using (MoveAppointment dialog = new MoveAppointment())
            {
                dialog.AppointmentOldDateStart = e.Appointment.DateStart;
                dialog.AppointmentOldDateEnd = e.Appointment.DateEnd;
                dialog.AppointmentTitle = e.Appointment.Subject;
                if (e.NewDate != null)
                {
                    dialog.AppointmentDateStart = e.NewDate;
                    dialog.AppointmentDateEnd = new DateTime(e.NewDate.Ticks + (dialog.AppointmentOldDateEnd.Ticks - dialog.AppointmentOldDateStart.Ticks));
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //if the user clicked 'save', update the appointment dates
                    e.Appointment.DateStart = dialog.AppointmentDateStart;
                    e.Appointment.DateEnd = dialog.AppointmentDateEnd;

                    A.UpdateDateIntervention(dialog.AppointmentDateStart, dialog.AppointmentDateEnd, dialog.AppointmentOldDateStart);

                    //have to tell the controls to refresh appointment display
                    weekScheduleControl1.RefreshAppointments();

                    //get the controls to repaint 
                    weekScheduleControl1.Invalidate();
                }
            }
        }

        private void calendar_AppointmentEdit(object sender, AppointmentEditEventArgs e)
        {
            DateTime AncienneDate = e.Appointment.DateStart;
            Access A = new Access();
            EntityIntervention Intervention = A.GetInterventionByDateDebut(e.Appointment.DateStart);
            EntityInterventionPiece InterventionPiece = A.GetInterventionPiece(Intervention.InterventionId, Intervention.MaterielId);

            //show a dialog to edit the appointment
            using (FicheIntervention dialog = new FicheIntervention())
            {
                dialog.AjoutCombo(A.GetAllNumContrat());
                dialog.txtDateAjout = Intervention.DateDebut.ToString();
                dialog.DateFin = Intervention.DateEnd.ToString();
                dialog.NumeroContrat = Intervention.NumContrat;
                dialog.DescriptionProbleme = Intervention.DescriptionProbleme;
                dialog.Commentaire = Intervention.Commentaire;
                dialog.NumMateriel = Intervention.MaterielId;
                dialog.Garantie = Intervention.Garantie;

                if(Intervention.KMParcouru != 0)
                    dialog.KMParcouru = Intervention.KMParcouru.ToString();

                if (InterventionPiece != null)
                {
                    dialog.Quantite = InterventionPiece.Quantite.ToString();
                    dialog.PrixUnitaire = InterventionPiece.PrixUnitaire.ToString();
                    dialog.MOPC = InterventionPiece.MOPC.ToString();
                    dialog.Description = InterventionPiece.Description;
                    dialog.Montant = InterventionPiece.Montant.ToString();
                }

                #region Enabled
                dialog.txtDateAjoutEnabled = true;
                dialog.DateFinEnabled = false;
                dialog.NumeroContratEnabled = true;
                dialog.DescriptionProblemeEnabled = true;
                dialog.CommentaireEnabled = false;
                dialog.NumMaterielEnabled = true;
                #endregion

                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //if the user clicked 'save', update the appointment dates and title
                    if ((Convert.ToDateTime(dialog.txtDateAjout).Hour > 8 && Convert.ToDateTime(dialog.txtDateAjout).AddHours(1).Hour < 12) || (Convert.ToDateTime(dialog.txtDateAjout).Hour > 14 && Convert.ToDateTime(dialog.txtDateAjout).AddHours(1).Hour < 18))
                    {
                        e.Appointment.DateStart = Convert.ToDateTime(dialog.txtDateAjout);
                        e.Appointment.DateEnd = Convert.ToDateTime(dialog.txtDateAjout).AddHours(1);
                        e.Appointment.Subject = "Intervention";

                        A.UpdateIntervention(AncienneDate, dialog.NumeroContrat, e.Appointment.DateStart, e.Appointment.DateEnd, dialog.DescriptionProbleme, dialog.Garantie, dialog.NumMateriel);
                    }
                    else
                        MessageBox.Show("L'horaire n'est pas valide");

                    //have to tell the controls to refresh appointment display
                    weekScheduleControl1.RefreshAppointments();

                    //get the controls to repaint 
                    weekScheduleControl1.Invalidate();
                }
            }
        }

        private void cbAssistantChoixTechnicien_TextChanged(object sender, EventArgs e)
        {
            Access A = new Access();
            technicienId = cbAssistantChoixTechnicien.Text;

            weekScheduleControl1.Appointments.Clear();

            List<EntityIntervention> Interventions = A.GetAllIntervention(technicienId);

            foreach (EntityIntervention Intervention in Interventions)
            {
                if (Intervention.KMParcouru == 0 && string.IsNullOrEmpty(Intervention.Commentaire))
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
                else
                    weekScheduleControl1.Appointments.Add(new ExtendedAppointment() { Subject = "Intervention Effectué", DateStart = Intervention.DateDebut, DateEnd = Intervention.DateEnd });
            }

            weekScheduleControl1.RefreshAppointments();
            weekScheduleControl1.Invalidate();
        }

        #endregion

        private void txtAssistantConsulterMaterielContratlie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            MiseInvisiblePanel();
            Button Bouton = (Button)sender;

            switch (Bouton.Tag.ToString())
            {
                case "Cli":
                    pnlAssistantConsulterClient.Visible = true;
                    break;
                case "Con":
                    pnlAssistantConsulterContrat.Visible = true;
                    break;
                case "Mat":
                    pnlAssistantConsulterMateriel.Visible = true;
                    break;
                case "Tec":
                    pnlAssistantTechnicien.Visible = true;
                    break;
                default:
                    break;
            }        
        }
    }
}
