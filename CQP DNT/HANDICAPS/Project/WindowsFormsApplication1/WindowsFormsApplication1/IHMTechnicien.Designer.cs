﻿namespace WindowsFormsApplication1
{
    partial class IHMTechnicien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IHMTechnicien));
            this.label15 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.weekScheduleControl1 = new Syd.ScheduleControls.WeekScheduleControl();
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(337, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(179, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "CONSULTER INTERVENTION";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(637, 34);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 11;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // weekScheduleControl1
            // 
            this.weekScheduleControl1.Date = new System.DateTime(((long)(0)));
            this.weekScheduleControl1.Location = new System.Drawing.Point(24, 208);
            this.weekScheduleControl1.Name = "weekScheduleControl1";
            this.weekScheduleControl1.Size = new System.Drawing.Size(851, 385);
            this.weekScheduleControl1.TabIndex = 10;
            this.weekScheduleControl1.Text = "weekScheduleControl1";
            this.weekScheduleControl1.AppointmentEdit += new System.EventHandler<Syd.ScheduleControls.Events.AppointmentEditEventArgs>(this.calendar_AppointmentEdit);
            // 
            // IHMTechnicien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(875, 591);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.weekScheduleControl1);
            this.Name = "IHMTechnicien";
            this.Text = "IHMTechnicien";
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private Syd.ScheduleControls.WeekScheduleControl weekScheduleControl1;
    }
}