﻿namespace WindowsFormsApplication1
{
    partial class FicheIntervention
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FicheIntervention));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescriptionProbleme = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCommentaireTechnicien = new System.Windows.Forms.TextBox();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDateDebut = new System.Windows.Forms.MaskedTextBox();
            this.txtDateFin = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbNumeroContrat = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNumClient = new System.Windows.Forms.TextBox();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtAdresse2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtDigicode = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtKmParcouru = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtModele = new System.Windows.Forms.TextBox();
            this.txtTypeContrat = new System.Windows.Forms.TextBox();
            this.txtTypeMateriel = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtPrixUnitaire = new System.Windows.Forms.TextBox();
            this.txtMOPC = new System.Windows.Forms.TextBox();
            this.txtMontant = new System.Windows.Forms.TextBox();
            this.cbReparationSousGarantie = new System.Windows.Forms.RadioButton();
            this.cbVisiteDeControle = new System.Windows.Forms.RadioButton();
            this.cbReparationHorsGarantie = new System.Windows.Forms.RadioButton();
            this.txtNumMateriel = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(19, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de contrat * : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(255, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "FICHE INTERVENTION";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(77, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date Debut * :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(318, 463);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Date Fin * :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(19, 375);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Decription du probleme * :";
            // 
            // txtDescriptionProbleme
            // 
            this.txtDescriptionProbleme.Location = new System.Drawing.Point(157, 332);
            this.txtDescriptionProbleme.Multiline = true;
            this.txtDescriptionProbleme.Name = "txtDescriptionProbleme";
            this.txtDescriptionProbleme.Size = new System.Drawing.Size(442, 99);
            this.txtDescriptionProbleme.TabIndex = 8;
            this.txtDescriptionProbleme.Enter += new System.EventHandler(this.Activation);
            this.txtDescriptionProbleme.Leave += new System.EventHandler(this.Activation);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(19, 694);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Commentaire du technicien * :";
            // 
            // txtCommentaireTechnicien
            // 
            this.txtCommentaireTechnicien.Enabled = false;
            this.txtCommentaireTechnicien.Location = new System.Drawing.Point(167, 657);
            this.txtCommentaireTechnicien.Multiline = true;
            this.txtCommentaireTechnicien.Name = "txtCommentaireTechnicien";
            this.txtCommentaireTechnicien.Size = new System.Drawing.Size(432, 86);
            this.txtCommentaireTechnicien.TabIndex = 10;
            this.txtCommentaireTechnicien.Leave += new System.EventHandler(this.Activation2);
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.BackColor = System.Drawing.Color.Transparent;
            this.btnEnregistrer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEnregistrer.BackgroundImage")));
            this.btnEnregistrer.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEnregistrer.Enabled = false;
            this.btnEnregistrer.FlatAppearance.BorderSize = 0;
            this.btnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistrer.ForeColor = System.Drawing.Color.White;
            this.btnEnregistrer.Location = new System.Drawing.Point(123, 749);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(99, 33);
            this.btnEnregistrer.TabIndex = 15;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = false;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.BackColor = System.Drawing.Color.Transparent;
            this.btnAnnuler.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnnuler.BackgroundImage")));
            this.btnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuler.FlatAppearance.BorderSize = 0;
            this.btnAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnuler.ForeColor = System.Drawing.Color.White;
            this.btnAnnuler.Location = new System.Drawing.Point(429, 749);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(99, 33);
            this.btnAnnuler.TabIndex = 16;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(164, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "* Mauvais format de date";
            this.label8.Visible = false;
            // 
            // txtDateDebut
            // 
            this.txtDateDebut.Location = new System.Drawing.Point(167, 292);
            this.txtDateDebut.Mask = "00-00-0000 00:00";
            this.txtDateDebut.Name = "txtDateDebut";
            this.txtDateDebut.Size = new System.Drawing.Size(122, 20);
            this.txtDateDebut.TabIndex = 18;
            this.txtDateDebut.Leave += new System.EventHandler(this.txtDateDebut_Leave);
            // 
            // txtDateFin
            // 
            this.txtDateFin.Enabled = false;
            this.txtDateFin.Location = new System.Drawing.Point(404, 460);
            this.txtDateFin.Mask = "00-00-0000 00:00";
            this.txtDateFin.Name = "txtDateFin";
            this.txtDateFin.Size = new System.Drawing.Size(191, 20);
            this.txtDateFin.TabIndex = 19;
            this.txtDateFin.Leave += new System.EventHandler(this.txtDateFin_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(413, 444);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "* Mauvais format de date";
            this.label10.Visible = false;
            // 
            // cbNumeroContrat
            // 
            this.cbNumeroContrat.FormattingEnabled = true;
            this.cbNumeroContrat.Location = new System.Drawing.Point(129, 53);
            this.cbNumeroContrat.Name = "cbNumeroContrat";
            this.cbNumeroContrat.Size = new System.Drawing.Size(121, 21);
            this.cbNumeroContrat.TabIndex = 22;
            this.cbNumeroContrat.SelectedIndexChanged += new System.EventHandler(this.cbNumeroContrat_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(365, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "NumClient :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(266, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Adresse :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(19, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Nom :";
            // 
            // txtNumClient
            // 
            this.txtNumClient.Enabled = false;
            this.txtNumClient.Location = new System.Drawing.Point(433, 53);
            this.txtNumClient.Name = "txtNumClient";
            this.txtNumClient.Size = new System.Drawing.Size(191, 20);
            this.txtNumClient.TabIndex = 26;
            // 
            // txtAdresse
            // 
            this.txtAdresse.Enabled = false;
            this.txtAdresse.Location = new System.Drawing.Point(321, 79);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(303, 20);
            this.txtAdresse.TabIndex = 27;
            // 
            // txtNom
            // 
            this.txtNom.Enabled = false;
            this.txtNom.Location = new System.Drawing.Point(94, 103);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(156, 20);
            this.txtNom.TabIndex = 28;
            // 
            // txtAdresse2
            // 
            this.txtAdresse2.Enabled = false;
            this.txtAdresse2.Location = new System.Drawing.Point(321, 106);
            this.txtAdresse2.Name = "txtAdresse2";
            this.txtAdresse2.Size = new System.Drawing.Size(303, 20);
            this.txtAdresse2.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(266, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Adresse2 :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(19, 152);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Telephone :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(313, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Digicode :";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Enabled = false;
            this.txtTelephone.Location = new System.Drawing.Point(94, 149);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(206, 20);
            this.txtTelephone.TabIndex = 33;
            // 
            // txtDigicode
            // 
            this.txtDigicode.Enabled = false;
            this.txtDigicode.Location = new System.Drawing.Point(368, 149);
            this.txtDigicode.Name = "txtDigicode";
            this.txtDigicode.Size = new System.Drawing.Size(256, 20);
            this.txtDigicode.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(19, 463);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Km Parcouru * :";
            // 
            // txtKmParcouru
            // 
            this.txtKmParcouru.Enabled = false;
            this.txtKmParcouru.Location = new System.Drawing.Point(99, 460);
            this.txtKmParcouru.Name = "txtKmParcouru";
            this.txtKmParcouru.Size = new System.Drawing.Size(151, 20);
            this.txtKmParcouru.TabIndex = 36;
            this.txtKmParcouru.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKmParcouru_KeyPress);
            this.txtKmParcouru.Leave += new System.EventHandler(this.txtKmParcouru_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(336, 296);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "Type Contrat :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(470, 250);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Modele :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(240, 250);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "Type Materiel :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(19, 250);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "N° Materiel * :";
            // 
            // txtModele
            // 
            this.txtModele.Enabled = false;
            this.txtModele.Location = new System.Drawing.Point(524, 246);
            this.txtModele.Name = "txtModele";
            this.txtModele.Size = new System.Drawing.Size(119, 20);
            this.txtModele.TabIndex = 45;
            // 
            // txtTypeContrat
            // 
            this.txtTypeContrat.Enabled = false;
            this.txtTypeContrat.Location = new System.Drawing.Point(416, 292);
            this.txtTypeContrat.Name = "txtTypeContrat";
            this.txtTypeContrat.Size = new System.Drawing.Size(151, 20);
            this.txtTypeContrat.TabIndex = 47;
            // 
            // txtTypeMateriel
            // 
            this.txtTypeMateriel.Enabled = false;
            this.txtTypeMateriel.Location = new System.Drawing.Point(316, 246);
            this.txtTypeMateriel.Name = "txtTypeMateriel";
            this.txtTypeMateriel.Size = new System.Drawing.Size(139, 20);
            this.txtTypeMateriel.TabIndex = 48;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(62, 570);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 50;
            this.label16.Text = "Quantite: ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(341, 619);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 13);
            this.label21.TabIndex = 51;
            this.label21.Text = "Montant:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(68, 619);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 52;
            this.label22.Text = "MO/PC:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(341, 570);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 53;
            this.label23.Text = "Prix Unitaire :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(17, 515);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(101, 13);
            this.label24.TabIndex = 54;
            this.label24.Text = "Description produit :";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Enabled = false;
            this.txtQuantite.Location = new System.Drawing.Point(122, 570);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(194, 20);
            this.txtQuantite.TabIndex = 55;
            this.txtQuantite.Enter += new System.EventHandler(this.CalculMontant);
            this.txtQuantite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantite_KeyPress);
            this.txtQuantite.Leave += new System.EventHandler(this.CalculMontant);
            // 
            // txtDescription
            // 
            this.txtDescription.Enabled = false;
            this.txtDescription.Location = new System.Drawing.Point(149, 512);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(475, 20);
            this.txtDescription.TabIndex = 56;
            // 
            // txtPrixUnitaire
            // 
            this.txtPrixUnitaire.Enabled = false;
            this.txtPrixUnitaire.Location = new System.Drawing.Point(415, 567);
            this.txtPrixUnitaire.Name = "txtPrixUnitaire";
            this.txtPrixUnitaire.Size = new System.Drawing.Size(176, 20);
            this.txtPrixUnitaire.TabIndex = 57;
            this.txtPrixUnitaire.Enter += new System.EventHandler(this.CalculMontant);
            this.txtPrixUnitaire.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrixUnitaire_KeyPress);
            this.txtPrixUnitaire.Leave += new System.EventHandler(this.CalculMontant);
            // 
            // txtMOPC
            // 
            this.txtMOPC.Enabled = false;
            this.txtMOPC.Location = new System.Drawing.Point(120, 616);
            this.txtMOPC.Name = "txtMOPC";
            this.txtMOPC.Size = new System.Drawing.Size(197, 20);
            this.txtMOPC.TabIndex = 58;
            this.txtMOPC.Enter += new System.EventHandler(this.CalculMontant);
            this.txtMOPC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMOPC_KeyPress);
            this.txtMOPC.Leave += new System.EventHandler(this.CalculMontant);
            // 
            // txtMontant
            // 
            this.txtMontant.Enabled = false;
            this.txtMontant.Location = new System.Drawing.Point(396, 616);
            this.txtMontant.Name = "txtMontant";
            this.txtMontant.Size = new System.Drawing.Size(166, 20);
            this.txtMontant.TabIndex = 59;
            this.txtMontant.Enter += new System.EventHandler(this.CalculMontant);
            this.txtMontant.Leave += new System.EventHandler(this.CalculMontant);
            // 
            // cbReparationSousGarantie
            // 
            this.cbReparationSousGarantie.AutoSize = true;
            this.cbReparationSousGarantie.BackColor = System.Drawing.Color.Transparent;
            this.cbReparationSousGarantie.Location = new System.Drawing.Point(58, 198);
            this.cbReparationSousGarantie.Name = "cbReparationSousGarantie";
            this.cbReparationSousGarantie.Size = new System.Drawing.Size(147, 17);
            this.cbReparationSousGarantie.TabIndex = 60;
            this.cbReparationSousGarantie.TabStop = true;
            this.cbReparationSousGarantie.Text = "Reparation Sous Garantie";
            this.cbReparationSousGarantie.UseVisualStyleBackColor = false;
            this.cbReparationSousGarantie.CheckedChanged += new System.EventHandler(this.cbReparationSousGarantie_CheckedChanged);
            // 
            // cbVisiteDeControle
            // 
            this.cbVisiteDeControle.AutoSize = true;
            this.cbVisiteDeControle.BackColor = System.Drawing.Color.Transparent;
            this.cbVisiteDeControle.Location = new System.Drawing.Point(480, 198);
            this.cbVisiteDeControle.Name = "cbVisiteDeControle";
            this.cbVisiteDeControle.Size = new System.Drawing.Size(107, 17);
            this.cbVisiteDeControle.TabIndex = 61;
            this.cbVisiteDeControle.TabStop = true;
            this.cbVisiteDeControle.Text = "Visite de Controle";
            this.cbVisiteDeControle.UseVisualStyleBackColor = false;
            this.cbVisiteDeControle.CheckedChanged += new System.EventHandler(this.cbVisiteDeControle_CheckedChanged);
            // 
            // cbReparationHorsGarantie
            // 
            this.cbReparationHorsGarantie.AutoSize = true;
            this.cbReparationHorsGarantie.BackColor = System.Drawing.Color.Transparent;
            this.cbReparationHorsGarantie.Location = new System.Drawing.Point(266, 198);
            this.cbReparationHorsGarantie.Name = "cbReparationHorsGarantie";
            this.cbReparationHorsGarantie.Size = new System.Drawing.Size(145, 17);
            this.cbReparationHorsGarantie.TabIndex = 62;
            this.cbReparationHorsGarantie.TabStop = true;
            this.cbReparationHorsGarantie.Text = "Reparation Hors Garantie";
            this.cbReparationHorsGarantie.UseVisualStyleBackColor = false;
            this.cbReparationHorsGarantie.CheckedChanged += new System.EventHandler(this.cbReparationHorsGarantie_CheckedChanged);
            // 
            // txtNumMateriel
            // 
            this.txtNumMateriel.FormattingEnabled = true;
            this.txtNumMateriel.Location = new System.Drawing.Point(90, 247);
            this.txtNumMateriel.Name = "txtNumMateriel";
            this.txtNumMateriel.Size = new System.Drawing.Size(144, 21);
            this.txtNumMateriel.TabIndex = 63;
            this.txtNumMateriel.SelectedIndexChanged += new System.EventHandler(this.txtNumMateriel_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(28, 200);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 20);
            this.label25.TabIndex = 64;
            this.label25.Text = "*";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(146, 554);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(123, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "* Veuillez saisir un chiffre";
            this.label26.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(19, 444);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(233, 13);
            this.label27.TabIndex = 66;
            this.label27.Text = "* Veuillez saisir seulement des chiffres ou virgule";
            this.label27.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(353, 551);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(238, 13);
            this.label28.TabIndex = 67;
            this.label28.Text = "* Veuillez saisir seulement des chiffres ou virgules";
            this.label28.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(71, 600);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(238, 13);
            this.label30.TabIndex = 69;
            this.label30.Text = "* Veuillez saisir seulement des chiffres ou virgules";
            this.label30.Visible = false;
            // 
            // FicheIntervention
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(646, 784);
            this.ControlBox = false;
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtNumMateriel);
            this.Controls.Add(this.cbReparationHorsGarantie);
            this.Controls.Add(this.cbVisiteDeControle);
            this.Controls.Add(this.cbReparationSousGarantie);
            this.Controls.Add(this.txtMontant);
            this.Controls.Add(this.txtMOPC);
            this.Controls.Add(this.txtPrixUnitaire);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtQuantite);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtTypeMateriel);
            this.Controls.Add(this.txtTypeContrat);
            this.Controls.Add(this.txtModele);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtKmParcouru);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtDigicode);
            this.Controls.Add(this.txtTelephone);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtAdresse2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.txtNumClient);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbNumeroContrat);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtDateFin);
            this.Controls.Add(this.txtDateDebut);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.txtCommentaireTechnicien);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDescriptionProbleme);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FicheIntervention";
            this.Text = " ";
            this.Load += new System.EventHandler(this.FicheIntervention_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescriptionProbleme;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCommentaireTechnicien;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtDateDebut;
        private System.Windows.Forms.MaskedTextBox txtDateFin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbNumeroContrat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNumClient;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtAdresse2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.TextBox txtDigicode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtKmParcouru;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtModele;
        private System.Windows.Forms.TextBox txtTypeContrat;
        private System.Windows.Forms.TextBox txtTypeMateriel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtPrixUnitaire;
        private System.Windows.Forms.TextBox txtMOPC;
        private System.Windows.Forms.TextBox txtMontant;
        private System.Windows.Forms.RadioButton cbReparationSousGarantie;
        private System.Windows.Forms.RadioButton cbVisiteDeControle;
        private System.Windows.Forms.RadioButton cbReparationHorsGarantie;
        private System.Windows.Forms.ComboBox txtNumMateriel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
    }
}