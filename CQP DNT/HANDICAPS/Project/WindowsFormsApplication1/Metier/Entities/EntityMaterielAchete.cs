﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityMaterielAchete
    {
        public string MaterielId { get; set; }
        public int Reference { get; set; }
        public string Contratlie { get; set; }
        public DateTime DateInstallation { get; set; }
        public decimal PrixAchat { get; set; }
    }
}
