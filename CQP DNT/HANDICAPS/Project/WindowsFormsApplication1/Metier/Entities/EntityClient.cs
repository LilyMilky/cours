﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metier.Entities
{
    public class EntityClient
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string Ville { get; set; }
        public string CodePostal { get; set; }
        public string Pays { get; set; }
        public string TelMobile { get; set; }
        public string DistanceAntenne { get; set; }
        public string DureeDeplacement { get; set; }
        public string TelDomicile { get; set; }
        public string Email { get; set; }
        public string ClientID { get; set; }
        public string Digicode { get; set; }
    }
}
