﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using Metier.Entities;

namespace DataAccess
{
    public class Access
    {
        DbProviderFactory dpf;
        DbConnection conn;

        public List<EntityClient> SelectClient(string Nom)
        {
            List<EntityClient> Clients = new List<EntityClient>();
            EntityClient Client;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_NOM like @Nom;";

            DbParameter ParamNom = accCmd.CreateParameter();
            ParamNom.DbType = DbType.String;
            ParamNom.Direction = ParameterDirection.Input;
            ParamNom.Value = Nom + "%";
            ParamNom.ParameterName = "Nom";

            accCmd.Parameters.Add(ParamNom);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                
                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        Clients.Add(Client);
                    }
                }

                return Clients;
            }
            finally
            {
                conn.Close();
            }

        }

        public List<EntityClient> SelectClient()
        {
            List<EntityClient> Clients = new List<EntityClient>();
            EntityClient Client;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DIGICODE, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Client = new EntityClient();

                        Client.Digicode = DataReader["CLT_DIGICODE"].ToString();
                        Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                        Client.ClientID = DataReader["PSN_ID"].ToString();
                        Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                        Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                        Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                        Client.Email = DataReader["CLT_EMAIL"].ToString();
                        Client.Nom = DataReader["PSN_NOM"].ToString();
                        Client.Pays = DataReader["PSN_PAYS"].ToString();
                        Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                        Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                        Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                        Client.Ville = DataReader["PSN_VILLE"].ToString();

                        Clients.Add(Client);
                    }
                }

                return Clients;
            }
            finally
            {
                conn.Close();
            }

        }

        public List<EntityContrat> GetContratById(string ID)
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT WHERE CLI_PSN_ID = @ID;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = ID;
            ParamId.ParameterName = "ID";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityContrat> GetContrat()
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        string DateR = DataReader["CT_DATE_RENOUVELLEMENT"].ToString();
                        if (!string.IsNullOrEmpty(DateR.Trim()))
                            Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        else
                            Contrat.DateRenouvellement = null;
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityContrat> GetContratByNumContrat(string NumContrat)
        {
            List<EntityContrat> Contrats = new List<EntityContrat>();
            EntityContrat Contrat;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID, CT_NUM_CONTRAT, TP_CTR_CODE, CT_DATE_ECHEANCE, CT_DATE_RENOUVELLEMENT, CT_DATE_SIGNATURE_CONTRAT, CT_PRIX FROM CONTRAT WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = NumContrat;
            ParamId.ParameterName = "NumContrat";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Contrat = new EntityContrat();

                        Contrat.DateEcheance = Convert.ToDateTime(DataReader["CT_DATE_ECHEANCE"]);
                        Contrat.DateRenouvellement = Convert.ToDateTime(DataReader["CT_DATE_RENOUVELLEMENT"]);
                        Contrat.DateSignatureContrat = Convert.ToDateTime(DataReader["CT_DATE_SIGNATURE_CONTRAT"]);
                        Contrat.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Contrat.Prix = Convert.ToDecimal(DataReader["CT_PRIX"]);
                        Contrat.ClientID = DataReader["CLI_PSN_ID"].ToString();
                        Contrat.TypeContrat = Convert.ToInt32(DataReader["TP_CTR_CODE"]);

                        Contrats.Add(Contrat);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Contrats;
        }

        public List<EntityMaterielAchete> GetMaterielAcheteByRef(string Reference)
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE WHERE MTL_REF2 = @Reference;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = Reference;
            ParamId.ParameterName = "Reference";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<EntityMaterielAchete> GetMaterielByContrat(string NumContrat)
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamId = accCmd.CreateParameter();
            ParamId.DbType = DbType.String;
            ParamId.Direction = ParameterDirection.Input;
            ParamId.Value = NumContrat;
            ParamId.ParameterName = "NumContrat";

            accCmd.Parameters.Add(ParamId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<EntityMaterielAchete> GetMateriel()
        {
            List<EntityMaterielAchete> Materiels = new List<EntityMaterielAchete>();
            EntityMaterielAchete Materiel;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT, MTL_REF2, MTL_DATE_INSTALLATION, MTL_PRIX_ACHAT FROM MATERIEL_ACHETE;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Materiel = new EntityMaterielAchete();

                        Materiel.Contratlie = DataReader["CT_NUM_CONTRAT"].ToString();
                        Materiel.DateInstallation = Convert.ToDateTime(DataReader["MTL_DATE_INSTALLATION"]);
                        Materiel.PrixAchat = Convert.ToDecimal(DataReader["MTL_PRIX_ACHAT"]);
                        Materiel.Reference = Convert.ToInt32(DataReader["MTL_REF2"]);

                        Materiels.Add(Materiel);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiels;
        }

        public List<string> GetIdMaterielByNumContrat(string NumContrat)
        {
            List<string> IdMateriels = new List<string>();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT MTL_REF2 FROM MATERIEL_ACHETE WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();

                 DbDataReader DataReader = accCmd.ExecuteReader();
                 if (DataReader.HasRows)
                 {
                     while (DataReader.Read())
                     {
                         IdMateriels.Add(DataReader["MTL_REF2"].ToString());
                     }
                 }
            }
            finally
            {
                conn.Close();
            }

            return IdMateriels;
        }

        public string GetTypeContratByNumeroContrat(string NumContrat)
        {
            string TypeContrat;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT TP_CTR_CODE FROM dbo.CONTRAT
                                    WHERE CT_NUM_CONTRAT=@NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);
            try
            {
                conn.Open();
                TypeContrat = accCmd.ExecuteScalar().ToString();
            }
            finally
            {
                conn.Close();
            }
            return TypeContrat;
        }

        public EntityMateriel GetMaterielByMaterielId(string MaterielId)
        {
            EntityMateriel Materiel = new EntityMateriel();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT MTL_REF2, MTL_TYPE, MTL_MODELE, MTL_PRIX FROM dbo.MATERIEL
                                    WHERE MTL_REF2=@MaterielId;";

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Materiel.MaterielId = Convert.ToInt32(DataReader["MTL_REF2"]);
                    Materiel.Modele = DataReader["MTL_MODELE"].ToString();
                    Materiel.Prix = Convert.ToDecimal(DataReader["MTL_PRIX"]);
                    Materiel.Type = DataReader["MTL_TYPE"].ToString();
                }
            }
            finally
            {
                conn.Close();
            }

            return Materiel;
        }

        public string GetIdIntervention()
        {
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID FROM dbo.INTERVENTION";

            string Id = "0";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Id = DataReader["ITV_ID"].ToString();
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Id;
        }

        public List<string> GetTechniciensId()
        {
            List<string> Ids = new List<string>();
            string Id;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT PSN_ID FROM TECHNICIEN;";

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Id = DataReader["PSN_ID"].ToString();

                        Ids.Add(Id);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Ids;
        }

        public EntityIntervention GetInterventionByDateDebut(DateTime DateDebut)
        {
            EntityIntervention Intervention = new EntityIntervention();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_KM_PARCOURU, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION
                                    WHERE ITV_DATE_DEBUT=@DateDebut;";

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Intervention = new EntityIntervention();

                    Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                    Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                    Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                    Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                    Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                    Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                    Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                    Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                    if(!string.IsNullOrEmpty(DataReader["ITV_KM_PARCOURU"].ToString()))
                        Intervention.KMParcouru = Convert.ToDecimal(DataReader["ITV_KM_PARCOURU"]);
                    Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                    Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();
                }
            }
            finally
            {
                conn.Close();
            }

            return Intervention;
        }

        public List<EntityIntervention> GetAllIntervention(string TechnicienId)
        {
            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION WHERE TEC_PSN_ID=@TechId;";

            DbParameter ParamTechId = accCmd.CreateParameter();
            ParamTechId.DbType = DbType.String;
            ParamTechId.Direction = ParameterDirection.Input;
            ParamTechId.Value = TechnicienId;
            ParamTechId.ParameterName = "TechId";
            accCmd.Parameters.Add(ParamTechId);

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        Intervention = new EntityIntervention();

                        Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                        Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                        Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                        Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                        Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                        Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                        Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                        Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                        Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();

                        Interventions.Add(Intervention);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Interventions;
        }

        public void UpdateDateIntervention(DateTime DateDebut, DateTime DateFin, DateTime DateStart)
        {
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET ITV_DATE_DEBUT=@DateDebut, ITV_DATE_FIN = @DateFin WHERE ITV_DATE_DEBUT = @DateStart;";

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateFin = accCmd.CreateParameter();
            ParamDateFin.DbType = DbType.DateTime;
            ParamDateFin.Direction = ParameterDirection.Input;
            ParamDateFin.Value = DateFin;
            ParamDateFin.ParameterName = "DateFin";
            accCmd.Parameters.Add(ParamDateFin);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.DateTime;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = DateStart;
            ParamNumContrat.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }

        }

        public void InsertIntervention(string AssistantId, string NumContrat, string TechnicienId, DateTime DateDebut, DateTime DateFin, string DescriptionProbleme, string Garantie, string MaterielId )
        {
            string InterventionId = (Convert.ToInt32(GetIdIntervention()) + 1).ToString();    //Recuperation de l'id de la derniere intervention;

            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"INSERT INTO dbo.INTERVENTION VALUES(@IDIntervention, @NumContrat, @AssistantId,@TechnicienId,
                @DateDebut,@DateEnd,@DescriptionProbleme,NULL,NULL, @Garantie, @MaterielId);";

            #region PARAMETRES
            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "IDIntervention";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            DbParameter ParamAssistantId = accCmd.CreateParameter();
            ParamAssistantId.DbType = DbType.String;
            ParamAssistantId.Direction = ParameterDirection.Input;
            ParamAssistantId.Value = AssistantId;
            ParamAssistantId.ParameterName = "AssistantId";
            accCmd.Parameters.Add(ParamAssistantId);

            DbParameter ParamTechnicienId = accCmd.CreateParameter();
            ParamTechnicienId.DbType = DbType.String;
            ParamTechnicienId.Direction = ParameterDirection.Input;
            ParamTechnicienId.Value = TechnicienId;
            ParamTechnicienId.ParameterName = "TechnicienId";
            accCmd.Parameters.Add(ParamTechnicienId);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateFin = accCmd.CreateParameter();
            ParamDateFin.DbType = DbType.DateTime;
            ParamDateFin.Direction = ParameterDirection.Input;
            ParamDateFin.Value = DateFin;
            ParamDateFin.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateFin);

            DbParameter ParamDescriptionProbleme = accCmd.CreateParameter();
            ParamDescriptionProbleme.DbType = DbType.String;
            ParamDescriptionProbleme.Direction = ParameterDirection.Input;
            ParamDescriptionProbleme.Value = DescriptionProbleme;
            ParamDescriptionProbleme.ParameterName = "DescriptionProbleme";
            accCmd.Parameters.Add(ParamDescriptionProbleme);

            DbParameter ParamGarantie = accCmd.CreateParameter();
            ParamGarantie.DbType = DbType.String;
            ParamGarantie.Direction = ParameterDirection.Input;
            ParamGarantie.Value = Garantie;
            ParamGarantie.ParameterName = "Garantie";
            accCmd.Parameters.Add(ParamGarantie);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public EntityClient GetClient(string ClientId)
        {
            EntityClient Client = new EntityClient();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT C.PSN_ID, C.CLT_DISTANCE, C.CLT_DUREE_DEPLACEMENT, C.CLT_TELDOMICILE, C.CLT_EMAIL, C.CLT_DIGICODE, P.PSN_NOM, P.PSN_PRENOM, P.PSN_ADRESSE, 
P.PSN_VILLE, P.PSN_CODEPOSTAL, P.PSN_PAYS, P.PSN_MOBILE FROM CLIENT AS C INNER JOIN PERSONNE AS P ON P.PSN_ID = C.PSN_ID WHERE P.PSN_ID = @ClientId";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = ClientId;
            ParamNumContrat.ParameterName = "ClientId";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();

                    Client.Digicode = DataReader["CLT_DIGICODE"].ToString();
                    Client.Adresse = DataReader["PSN_ADRESSE"].ToString();
                    Client.ClientID = DataReader["PSN_ID"].ToString();
                    Client.CodePostal = DataReader["PSN_CODEPOSTAL"].ToString();
                    Client.DistanceAntenne = DataReader["CLT_DISTANCE"].ToString();
                    Client.DureeDeplacement = DataReader["CLT_DUREE_DEPLACEMENT"].ToString();
                    Client.Email = DataReader["CLT_EMAIL"].ToString();
                    Client.Nom = DataReader["PSN_NOM"].ToString();
                    Client.Pays = DataReader["PSN_PAYS"].ToString();
                    Client.Prenom = DataReader["PSN_PRENOM"].ToString();
                    Client.TelDomicile = DataReader["CLT_TELDOMICILE"].ToString();
                    Client.TelMobile = DataReader["PSN_MOBILE"].ToString();
                    Client.Ville = DataReader["PSN_VILLE"].ToString();
                }
            }

            finally
            {
                conn.Close();
            }
            return Client;
        }

        public string GetClientID(string NumContrat)
        {
            string Client;
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CLI_PSN_ID FROM dbo.CONTRAT WHERE CT_NUM_CONTRAT = @NumContrat;";

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            try
            {
                conn.Open();
                Client = accCmd.ExecuteScalar().ToString();

            }
            finally
            {
                conn.Close();
            }

            return Client;
        }

        public List<string> GetAllNumContrat()
        {
            List<string> NumContrats = new List<string>();
            string NumContrat;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT CT_NUM_CONTRAT FROM dbo.CONTRAT;";

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {
                        NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();

                        NumContrats.Add(NumContrat);
                    }
                }

            }
            finally
            {
                conn.Close();
            }

            return NumContrats;
        }

        public void UpdateIntervention(DateTime DateStart, string NumContrat, DateTime DateDebut, DateTime DateFin, string DescriptionProbleme, string Garantie, string MaterielId)
        {
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET CT_NUM_CONTRAT = @NumContrat, ITV_DATE_DEBUT = @DateDebut, ITV_DATE_FIN = @DateEnd, ITV_DESCRIPTION_PROBLEME= @DescriptionProbleme, ITV_GARANTIE = @Garantie, ITV_MATERIELID = @MaterielId  WHERE ITV_DATE_DEBUT = @DateStart;";

            #region PARAMETRES
            DbParameter ParamDateStart = accCmd.CreateParameter();
            ParamDateStart.DbType = DbType.DateTime;
            ParamDateStart.Direction = ParameterDirection.Input;
            ParamDateStart.Value = DateStart;
            ParamDateStart.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamDateStart);

            DbParameter ParamNumContrat = accCmd.CreateParameter();
            ParamNumContrat.DbType = DbType.String;
            ParamNumContrat.Direction = ParameterDirection.Input;
            ParamNumContrat.Value = NumContrat;
            ParamNumContrat.ParameterName = "NumContrat";
            accCmd.Parameters.Add(ParamNumContrat);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = DateDebut;
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDescriptionProbleme = accCmd.CreateParameter();
            ParamDescriptionProbleme.DbType = DbType.String;
            ParamDescriptionProbleme.Direction = ParameterDirection.Input;
            ParamDescriptionProbleme.Value = DescriptionProbleme;
            ParamDescriptionProbleme.ParameterName = "DescriptionProbleme";
            accCmd.Parameters.Add(ParamDescriptionProbleme);

            DbParameter ParamDateEnd = accCmd.CreateParameter();
            ParamDateEnd.DbType = DbType.DateTime;
            ParamDateEnd.Direction = ParameterDirection.Input;
            ParamDateEnd.Value = DateFin;
            ParamDateEnd.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateEnd);

            DbParameter ParamGarantie = accCmd.CreateParameter();
            ParamGarantie.DbType = DbType.String;
            ParamGarantie.Direction = ParameterDirection.Input;
            ParamGarantie.Value = Garantie;
            ParamGarantie.ParameterName = "Garantie";
            accCmd.Parameters.Add(ParamGarantie);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void UpdateInterventionTechnicien(DateTime DateStart, DateTime DateFin, string CommentaireTechnicien, decimal KMParcouru)
        {
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION SET ITV_DATE_FIN = @DateEnd, ITV_COMMENTAIRE = @CommentaireTechnicien, ITV_KM_PARCOURU = @KMParcouru WHERE ITV_DATE_DEBUT = @DateStart;";

            #region PARAMETRES
            DbParameter ParamDateStart = accCmd.CreateParameter();
            ParamDateStart.DbType = DbType.DateTime;
            ParamDateStart.Direction = ParameterDirection.Input;
            ParamDateStart.Value = DateStart;
            ParamDateStart.ParameterName = "DateStart";
            accCmd.Parameters.Add(ParamDateStart);

            DbParameter ParamCommentaireTechnicien = accCmd.CreateParameter();
            ParamCommentaireTechnicien.DbType = DbType.String;
            ParamCommentaireTechnicien.Direction = ParameterDirection.Input;
            ParamCommentaireTechnicien.Value = CommentaireTechnicien;
            ParamCommentaireTechnicien.ParameterName = "CommentaireTechnicien";
            accCmd.Parameters.Add(ParamCommentaireTechnicien);

            DbParameter ParamDateEnd = accCmd.CreateParameter();
            ParamDateEnd.DbType = DbType.DateTime;
            ParamDateEnd.Direction = ParameterDirection.Input;
            ParamDateEnd.Value = DateFin;
            ParamDateEnd.ParameterName = "DateEnd";
            accCmd.Parameters.Add(ParamDateEnd);

            DbParameter ParamKMParcouru = accCmd.CreateParameter();
            ParamKMParcouru.DbType = DbType.Decimal;
            ParamKMParcouru.Direction = ParameterDirection.Input;
            ParamKMParcouru.Value = KMParcouru;
            ParamKMParcouru.ParameterName = "KMParcouru";
            accCmd.Parameters.Add(ParamKMParcouru);

            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public string GetInterventionPieceId()
        {
            string InterventionPieceId;

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT TOP 1 ITVP_ID FROM dbo.INTERVENTION_PIECE ORDER BY ITVP_ID DESC;";

            try
            {
                conn.Open();
                InterventionPieceId = accCmd.ExecuteScalar().ToString();
            }
            finally
            {
                conn.Close();
            }

            return InterventionPieceId;
        }

        public void InsertInterventionPiece(string InterventionId, int Quantite, string Description, decimal PrixUnitaire, decimal MOPC, decimal Montant)
        {
            string InterventionPieceId = (Convert.ToInt32(GetInterventionPieceId()) + 1).ToString();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"INSERT INTO dbo.INTERVENTION_PIECE VALUES(@InterventionPieceId, @InterventionId, 
                                                        @Quantite, @Description, @PrixUnitaire, @MOPC, @Montant);";

            #region PARAMETRES
            DbParameter ParamInterventionPieceId = accCmd.CreateParameter();
            ParamInterventionPieceId.DbType = DbType.String;
            ParamInterventionPieceId.Direction = ParameterDirection.Input;
            ParamInterventionPieceId.Value = InterventionPieceId;
            ParamInterventionPieceId.ParameterName = "InterventionPieceId";
            accCmd.Parameters.Add(ParamInterventionPieceId);

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "InterventionId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamQuantite = accCmd.CreateParameter();
            ParamQuantite.DbType = DbType.Int32;
            ParamQuantite.Direction = ParameterDirection.Input;
            ParamQuantite.Value = Quantite;
            ParamQuantite.ParameterName = "Quantite";
            accCmd.Parameters.Add(ParamQuantite);

            DbParameter ParamDescription = accCmd.CreateParameter();
            ParamDescription.DbType = DbType.String;
            ParamDescription.Direction = ParameterDirection.Input;
            ParamDescription.Value = Description;
            ParamDescription.ParameterName = "Description";
            accCmd.Parameters.Add(ParamDescription);

            DbParameter ParamPrixUnitaire = accCmd.CreateParameter();
            ParamPrixUnitaire.DbType = DbType.Decimal;
            ParamPrixUnitaire.Direction = ParameterDirection.Input;
            ParamPrixUnitaire.Value = PrixUnitaire;
            ParamPrixUnitaire.ParameterName = "PrixUnitaire";
            accCmd.Parameters.Add(ParamPrixUnitaire);

            DbParameter ParamMOPC = accCmd.CreateParameter();
            ParamMOPC.DbType = DbType.Decimal;
            ParamMOPC.Direction = ParameterDirection.Input;
            ParamMOPC.Value = MOPC;
            ParamMOPC.ParameterName = "MOPC";
            accCmd.Parameters.Add(ParamMOPC);

            DbParameter ParamMontant = accCmd.CreateParameter();
            ParamMontant.DbType = DbType.Decimal;
            ParamMontant.Direction = ParameterDirection.Input;
            ParamMontant.Value = Montant;
            ParamMontant.ParameterName = "Montant";
            accCmd.Parameters.Add(ParamMontant);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void UpdateInterventionPiece(int InterventionPieceId, int Quantite, string Description, decimal PrixUnitaire, decimal MOPC, decimal Montant)
        {
            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"UPDATE dbo.INTERVENTION_PIECE SET ITVP_QUANTITE = @Quantite, ITVP_DESCRIPTION = @Description, 
                                        ITVP_PRIX_UNITAIRE = @PrixUnitaire, ITVP_MAIN_OEUVRE_PIECE = @MOPC, 
                                        ITVP_MONTANT = @Montant WHERE ITVP_ID = @InterventionPieceId;";

            #region PARAMETRES

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.Int32;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionPieceId;
            ParamInterventionId.ParameterName = "InterventionPieceId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamQuantite = accCmd.CreateParameter();
            ParamQuantite.DbType = DbType.Int32;
            ParamQuantite.Direction = ParameterDirection.Input;
            ParamQuantite.Value = Quantite;
            ParamQuantite.ParameterName = "Quantite";
            accCmd.Parameters.Add(ParamQuantite);

            DbParameter ParamDescription = accCmd.CreateParameter();
            ParamDescription.DbType = DbType.String;
            ParamDescription.Direction = ParameterDirection.Input;
            ParamDescription.Value = Description;
            ParamDescription.ParameterName = "Description";
            accCmd.Parameters.Add(ParamDescription);

            DbParameter ParamPrixUnitaire = accCmd.CreateParameter();
            ParamPrixUnitaire.DbType = DbType.Decimal;
            ParamPrixUnitaire.Direction = ParameterDirection.Input;
            ParamPrixUnitaire.Value = PrixUnitaire;
            ParamPrixUnitaire.ParameterName = "PrixUnitaire";
            accCmd.Parameters.Add(ParamPrixUnitaire);

            DbParameter ParamMOPC = accCmd.CreateParameter();
            ParamMOPC.DbType = DbType.Decimal;
            ParamMOPC.Direction = ParameterDirection.Input;
            ParamMOPC.Value = MOPC;
            ParamMOPC.ParameterName = "MOPC";
            accCmd.Parameters.Add(ParamMOPC);

            DbParameter ParamMontant = accCmd.CreateParameter();
            ParamMontant.DbType = DbType.Decimal;
            ParamMontant.Direction = ParameterDirection.Input;
            ParamMontant.Value = Montant;
            ParamMontant.ParameterName = "Montant";
            accCmd.Parameters.Add(ParamMontant);
            #endregion

            try
            {
                conn.Open();
                accCmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public EntityInterventionPiece GetInterventionPiece(string InterventionId, string MaterielId)
        {
            EntityInterventionPiece entityInterventionPiece = new EntityInterventionPiece();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT IP.ITVP_ID, IP.ITV_ID, IP.ITVP_QUANTITE, IP.ITVP_DESCRIPTION, IP.ITVP_PRIX_UNITAIRE,
            IP.ITVP_MAIN_OEUVRE_PIECE, IP.ITVP_MONTANT FROM dbo.INTERVENTION_PIECE AS IP INNER JOIN dbo.INTERVENTION 
            AS I ON IP.ITV_ID = I.ITV_ID WHERE IP.ITV_ID = @InterventionId AND I.ITV_MATERIELID = @MaterielId;";

            DbParameter ParamInterventionId = accCmd.CreateParameter();
            ParamInterventionId.DbType = DbType.String;
            ParamInterventionId.Direction = ParameterDirection.Input;
            ParamInterventionId.Value = InterventionId;
            ParamInterventionId.ParameterName = "InterventionId";
            accCmd.Parameters.Add(ParamInterventionId);

            DbParameter ParamMaterielId = accCmd.CreateParameter();
            ParamMaterielId.DbType = DbType.String;
            ParamMaterielId.Direction = ParameterDirection.Input;
            ParamMaterielId.Value = MaterielId;
            ParamMaterielId.ParameterName = "MaterielId";
            accCmd.Parameters.Add(ParamMaterielId);

            try
            {
                conn.Open();
                DbDataReader DataReader = accCmd.ExecuteReader();

                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    {
                        entityInterventionPiece.Description = DataReader["ITVP_DESCRIPTION"].ToString();
                        entityInterventionPiece.InterventionId = DataReader["ITV_ID"].ToString();
                        entityInterventionPiece.InterventionPieceId = Convert.ToInt32(DataReader["ITVP_ID"]);
                        entityInterventionPiece.Montant = Convert.ToDecimal(DataReader["ITVP_MONTANT"]);
                        entityInterventionPiece.MOPC = Convert.ToDecimal(DataReader["ITVP_MAIN_OEUVRE_PIECE"]);
                        entityInterventionPiece.PrixUnitaire = Convert.ToDecimal(DataReader["ITVP_PRIX_UNITAIRE"]);
                        entityInterventionPiece.Quantite = Convert.ToInt32(DataReader["ITVP_QUANTITE"]);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return entityInterventionPiece;
        }

        public List<EntityIntervention> GetALLInterventionByDateDebut(DateTime DateDebut, string technicienId)
        {
            List<EntityIntervention> Interventions = new List<EntityIntervention>();
            EntityIntervention Intervention = new EntityIntervention();

            dpf = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["dbHandicaps"].ProviderName);
            conn = dpf.CreateConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["dbHandicaps"].ConnectionString;

            DbCommand accCmd = conn.CreateCommand();
            accCmd.Connection = conn;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = @"SELECT ITV_ID, CT_NUM_CONTRAT, ASST_PSN_ID, TEC_PSN_ID, ITV_DATE_DEBUT, ITV_DATE_FIN, ITV_DESCRIPTION_PROBLEME, ITV_COMMENTAIRE, ITV_KM_PARCOURU, ITV_GARANTIE, ITV_MATERIELID FROM dbo.INTERVENTION
                                    WHERE ITV_DATE_DEBUT BETWEEN @DateDebut AND @DateDebut2 AND TEC_PSN_ID = @technicienId;";

            string Debut = DateDebut.Year.ToString() + "-" + DateDebut.Month.ToString().PadLeft(2, '0') + "-" + DateDebut.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            string Fin = DateDebut.Year.ToString() + "-" + DateDebut.Month.ToString().PadLeft(2, '0') + "-" + DateDebut.Day.ToString().PadLeft(2, '0') + " 23:59:59";

            #region PARAMETRES
            DbParameter ParamtechnicienId = accCmd.CreateParameter();
            ParamtechnicienId.DbType = DbType.String;
            ParamtechnicienId.Direction = ParameterDirection.Input;
            ParamtechnicienId.Value = technicienId;
            ParamtechnicienId.ParameterName = "technicienId";
            accCmd.Parameters.Add(ParamtechnicienId);

            DbParameter ParamDateDebut = accCmd.CreateParameter();
            ParamDateDebut.DbType = DbType.DateTime;
            ParamDateDebut.Direction = ParameterDirection.Input;
            ParamDateDebut.Value = Convert.ToDateTime(Debut);
            ParamDateDebut.ParameterName = "DateDebut";
            accCmd.Parameters.Add(ParamDateDebut);

            DbParameter ParamDateDebut2 = accCmd.CreateParameter();
            ParamDateDebut2.DbType = DbType.DateTime;
            ParamDateDebut2.Direction = ParameterDirection.Input;
            ParamDateDebut2.Value = Convert.ToDateTime(Fin);
            ParamDateDebut2.ParameterName = "DateDebut2";
            accCmd.Parameters.Add(ParamDateDebut2);
            #endregion

            try
            {
                conn.Open();

                DbDataReader DataReader = accCmd.ExecuteReader();
                if (DataReader.HasRows)
                {
                    while (DataReader.Read())
                    {

                        Intervention = new EntityIntervention();

                        Intervention.AssistantId = DataReader["ASST_PSN_ID"].ToString();
                        Intervention.Commentaire = DataReader["ITV_COMMENTAIRE"].ToString();
                        Intervention.DateDebut = Convert.ToDateTime(DataReader["ITV_DATE_DEBUT"]);
                        Intervention.DateEnd = Convert.ToDateTime(DataReader["ITV_DATE_FIN"]);
                        Intervention.DescriptionProbleme = DataReader["ITV_DESCRIPTION_PROBLEME"].ToString();
                        Intervention.InterventionId = DataReader["ITV_ID"].ToString();
                        Intervention.NumContrat = DataReader["CT_NUM_CONTRAT"].ToString();
                        Intervention.TechnicienId = DataReader["TEC_PSN_ID"].ToString();
                        if (!string.IsNullOrEmpty(DataReader["ITV_KM_PARCOURU"].ToString()))
                            Intervention.KMParcouru = Convert.ToDecimal(DataReader["ITV_KM_PARCOURU"]);
                        Intervention.Garantie = DataReader["ITV_GARANTIE"].ToString();
                        Intervention.MaterielId = DataReader["ITV_MATERIELID"].ToString();

                        Interventions.Add(Intervention);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            return Interventions;
        }
    }
}
