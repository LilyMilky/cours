USE [master]
GO
/****** Object:  Table [dbo].[ANTENNES]    Script Date: 11/30/2012 13:31:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ANTENNES]') AND type in (N'U'))
DROP TABLE [dbo].[ANTENNES]
GO
/****** Object:  Table [dbo].[ASSISTANT]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASSISTANT]') AND type in (N'U'))
DROP TABLE [dbo].[ASSISTANT]
GO
/****** Object:  Table [dbo].[CANDIDATURE]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CANDIDATURE]') AND type in (N'U'))
DROP TABLE [dbo].[CANDIDATURE]
GO
/****** Object:  Table [dbo].[CLIENT]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLIENT]') AND type in (N'U'))
DROP TABLE [dbo].[CLIENT]
GO
/****** Object:  Table [dbo].[COMMERCIAL]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[COMMERCIAL]') AND type in (N'U'))
DROP TABLE [dbo].[COMMERCIAL]
GO
/****** Object:  Table [dbo].[CONNEXION]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONNEXION]') AND type in (N'U'))
DROP TABLE [dbo].[CONNEXION]
GO
/****** Object:  Table [dbo].[CONTRAT]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONTRAT]') AND type in (N'U'))
DROP TABLE [dbo].[CONTRAT]
GO
/****** Object:  Table [dbo].[INTERVENTION]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INTERVENTION]') AND type in (N'U'))
DROP TABLE [dbo].[INTERVENTION]
GO
/****** Object:  Table [dbo].[INTERVENTION_PIECE]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INTERVENTION_PIECE]') AND type in (N'U'))
DROP TABLE [dbo].[INTERVENTION_PIECE]
GO
/****** Object:  Table [dbo].[MATERIEL]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MATERIEL]') AND type in (N'U'))
DROP TABLE [dbo].[MATERIEL]
GO
/****** Object:  Table [dbo].[MATERIEL_ACHETE]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MATERIEL_ACHETE]') AND type in (N'U'))
DROP TABLE [dbo].[MATERIEL_ACHETE]
GO
/****** Object:  Table [dbo].[PLANIFIER]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PLANIFIER]') AND type in (N'U'))
DROP TABLE [dbo].[PLANIFIER]
GO
/****** Object:  Table [dbo].[PERSONNE]    Script Date: 11/30/2012 13:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PERSONNE]') AND type in (N'U'))
DROP TABLE [dbo].[PERSONNE] 
GO
/****** Object:  Table [dbo].[TECHNICIEN]    Script Date: 11/30/2012 13:31:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TECHNICIEN]') AND type in (N'U'))
DROP TABLE [dbo].[TECHNICIEN]
GO
/****** Object:  Table [dbo].[TYPE_CONTRAT]    Script Date: 11/30/2012 13:31:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TYPE_CONTRAT]') AND type in (N'U'))
DROP TABLE [dbo].[TYPE_CONTRAT]
GO
