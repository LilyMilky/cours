/*==============================================================*/
/* Nom de SGBD :  Microsoft SQL Server 2008                     */
/* Date de cr�ation :  17/04/2013 10:05:04                      */
/*==============================================================*/


if exists (select 1
          from sysobjects
          where id = object_id('CLR_TRIGGER_CONNEXION')
          and type = 'TR')
   drop trigger CLR_TRIGGER_CONNEXION
go

if exists (select 1
          from sysobjects
          where id = object_id('TI_CONNEXION')
          and type = 'TR')
   drop trigger TI_CONNEXION
go

if exists (select 1
          from sysobjects
          where id = object_id('TU_CONNEXION')
          and type = 'TR')
   drop trigger TU_CONNEXION
go

if exists (select 1
          from sysobjects
          where id = object_id('CLR_TRIGGER_PERSONNE')
          and type = 'TR')
   drop trigger CLR_TRIGGER_PERSONNE
go

if exists (select 1
          from sysobjects
          where id = object_id('TD_PERSONNE')
          and type = 'TR')
   drop trigger TD_PERSONNE
go

if exists (select 1
          from sysobjects
          where id = object_id('TU_PERSONNE')
          and type = 'TR')
   drop trigger TU_PERSONNE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ANTENNES')
            and   type = 'U')
   drop table ANTENNES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ASSISTANT')
            and   type = 'U')
   drop table ASSISTANT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CANDIDATURE')
            and   type = 'U')
   drop table CANDIDATURE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLIENT')
            and   type = 'U')
   drop table CLIENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMMERCIAL')
            and   type = 'U')
   drop table COMMERCIAL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CONNEXION')
            and   type = 'U')
   drop table CONNEXION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CONTRAT')
            and   type = 'U')
   drop table CONTRAT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('INTERVENTION')
            and   type = 'U')
   drop table INTERVENTION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('INTERVENTION_PIECE')
            and   type = 'U')
   drop table INTERVENTION_PIECE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MATERIEL')
            and   type = 'U')
   drop table MATERIEL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MATERIEL_ACHETE')
            and   type = 'U')
   drop table MATERIEL_ACHETE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PERSONNE')
            and   type = 'U')
   drop table PERSONNE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TECHNICIEN')
            and   type = 'U')
   drop table TECHNICIEN
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TYPE_CONTRAT')
            and   type = 'U')
   drop table TYPE_CONTRAT
go

/*==============================================================*/
/* Table : ANTENNES                                             */
/*==============================================================*/
create table ANTENNES (
   AN_VILLE             varchar(50)          not null,
   AN_ADRESSE           varchar(100)         not null,
   AN_CODEPOSTAL        int                  not null,
   AN_TEL               varchar(10)          not null,
   AN_TELECOPIE         varchar(10)          null,
   constraint PK_ANTENNES primary key nonclustered (AN_VILLE)
)
go

/*==============================================================*/
/* Table : ASSISTANT                                            */
/*==============================================================*/
create table ASSISTANT (
   PSN_ID               varchar(50)          not null,
   ASST_DATE_EMBAUCHE   datetime             not null,
   constraint PK_ASSISTANT primary key nonclustered (PSN_ID)
)
go

/*==============================================================*/
/* Table : CANDIDATURE                                          */
/*==============================================================*/
create table CANDIDATURE (
   CAND_ID              int                  not null,
   CAND_TYPE            char(2)              not null,
   CAND_NOM             varchar(50)          not null,
   CAND_PRENOM          varchar(50)          not null,
   CAND_ADRESSE         varchar(100)         not null,
   CAND_VILLE           varchar(50)          not null,
   CAND_CODEPOSTAL      varchar(5)           not null,
   CAND_PAYS            varchar(50)          not null,
   CAND_CV              varchar(100)         not null,
   CAND_LDM             varchar(100)         not null,
   constraint PK_CANDIDATURE primary key nonclustered (CAND_ID)
)
go

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT (
   PSN_ID               varchar(50)          not null,
   CLT_DISTANCE         varchar(20)          not null,
   CLT_DUREE_DEPLACEMENT varchar(20)          not null,
   CLT_TELDOMICILE      varchar(10)          null,
   CLT_EMAIL            varchar(50)          null,
   CLT_DIGICODE         varchar(10)          null,
   constraint PK_CLIENT primary key nonclustered (PSN_ID)
)
go

/*==============================================================*/
/* Table : COMMERCIAL                                           */
/*==============================================================*/
create table COMMERCIAL (
   PSN_ID               varchar(50)          not null,
   AN_VILLE             varchar(50)          not null,
   CCIAL_TELDOMICILE    varchar(10)          null,
   CCIAL_DATE_EMBAUCHE  datetime             not null,
   constraint PK_COMMERCIAL primary key nonclustered (PSN_ID)
)
go

/*==============================================================*/
/* Table : CONNEXION                                            */
/*==============================================================*/
create table CONNEXION (
   CONN_LOGIN           varchar(50)          not null,
   PSN_ID               varchar(50)          not null,
   CONN_PWD             varchar(50)          not null,
   CONN_PROFIL          varchar(50)          not null,
   constraint PK_CONNEXION primary key nonclustered (CONN_LOGIN)
)
go

/*==============================================================*/
/* Table : CONTRAT                                              */
/*==============================================================*/
create table CONTRAT (
   CT_NUM_CONTRAT       varchar(50)          not null,
   TP_CTR_CODE          int                  not null,
   CCIAL_PSN_ID         varchar(50)          not null,
   CLI_PSN_ID           varchar(50)          not null,
   CT_DATE_ECHEANCE     datetime             not null,
   CT_DATE_RENOUVELLEMENT datetime             null,
   CT_DATE_SIGNATURE_CONTRAT datetime             not null,
   CT_PRIX              decimal(10,2)        null,
   constraint PK_CONTRAT primary key nonclustered (CT_NUM_CONTRAT)
)
go

/*==============================================================*/
/* Table : INTERVENTION                                         */
/*==============================================================*/
create table INTERVENTION (
   ITV_ID               varchar(50)          not null,
   CT_NUM_CONTRAT       varchar(50)          not null,
   ASST_PSN_ID          varchar(50)          not null,
   TEC_PSN_ID           varchar(50)          not null,
   ITV_DATE_DEBUT       datetime             not null,
   ITV_DATE_FIN         datetime             not null,
   ITV_DESCRIPTION_PROBLEME varchar(255)         null,
   ITV_COMMENTAIRE      varchar(255)         null,
   ITV_KM_PARCOURU      decimal(5,2)         null,
   ITV_GARANTIE         varchar(3)           not null,
   ITV_MATERIELID       varchar(50)          not null,
   constraint PK_INTERVENTION primary key nonclustered (ITV_ID)
)
go

/*==============================================================*/
/* Table : INTERVENTION_PIECE                                   */
/*==============================================================*/
create table INTERVENTION_PIECE (
   ITVP_ID              int                  not null,
   ITV_ID               varchar(50)          not null,
   ITVP_QUANTITE        int                  null,
   ITVP_DESCRIPTION     varchar(255)         null,
   ITVP_PRIX_UNITAIRE   decimal(6,2)         null,
   ITVP_MAIN_OEUVRE_PIECE decimal(6,2)         null,
   ITVP_MONTANT         decimal(6,2)         null,
   constraint PK_INTERVENTION_PIECE primary key nonclustered (ITVP_ID)
)
go

/*==============================================================*/
/* Table : MATERIEL                                             */
/*==============================================================*/
create table MATERIEL (
   MTL_REF2             int                  not null,
   MTL_TYPE             varchar(50)          not null,
   MTL_MODELE           varchar(50)          not null,
   MTL_PRIX             decimal(10,2)        not null,
   constraint PK_MATERIEL primary key nonclustered (MTL_REF2)
)
go

/*==============================================================*/
/* Table : MATERIEL_ACHETE                                      */
/*==============================================================*/
create table MATERIEL_ACHETE (
   MTL_ID               varchar(50)          not null,
   CT_NUM_CONTRAT       varchar(50)          null,
   MTL_REF2             int                  not null,
   MTL_DATE_INSTALLATION datetime             null,
   MTL_PRIX_ACHAT       decimal(10,2)        not null,
   constraint PK_MATERIEL_ACHETE primary key nonclustered (MTL_ID)
)
go

/*==============================================================*/
/* Table : PERSONNE                                             */
/*==============================================================*/
create table PERSONNE (
   PSN_ID               varchar(50)          not null,
   PSN_NOM              varchar(50)          not null,
   PSN_PRENOM           varchar(50)          not null,
   PSN_ADRESSE          varchar(250)         not null,
   PSN_VILLE            char(50)             not null,
   PSN_CODEPOSTAL       varchar(5)           not null,
   PSN_PAYS             varchar(50)          not null,
   PSN_MOBILE           varchar(10)          not null,
   constraint PK_PERSONNE primary key nonclustered (PSN_ID)
)
go

/*==============================================================*/
/* Table : TECHNICIEN                                           */
/*==============================================================*/
create table TECHNICIEN (
   PSN_ID               varchar(50)          not null,
   AN_VILLE             varchar(50)          not null,
   TCH_DATE_EMBAUCHE    datetime             not null,
   TCH_QUALIFICATION    varchar(50)          null,
   TCH_DATE_QUALIFICATION datetime             null,
   constraint PK_TECHNICIEN primary key nonclustered (PSN_ID)
)
go

/*==============================================================*/
/* Table : TYPE_CONTRAT                                         */
/*==============================================================*/
create table TYPE_CONTRAT (
   TP_CTR_CODE          int                  not null,
   TP_CTR_LIBELLE       varchar(50)          not null,
   TAUX                 int                  not null,
   constraint PK_TYPE_CONTRAT primary key nonclustered (TP_CTR_CODE)
)
go


create trigger TI_CONNEXION on CONNEXION for insert as
begin
    declare
       @maxcard  int,
       @numrows  int,
       @numnull  int,
       @errno    int,
       @errmsg   varchar(255)

    select  @numrows = @@rowcount
    if @numrows = 0
       return

    /*  Le parent "PERSONNE" doit exister � la cr�ation d'un enfant dans "CONNEXION"  */
    if update(PSN_ID)
    begin
       if (select count(*)
           from   PERSONNE t1, inserted t2
           where  t1.PSN_ID = t2.PSN_ID) != @numrows
          begin
             select @errno  = 50002,
                    @errmsg = 'Le parent n''''existe pas dans "PERSONNE". Impossible de cr�er un enfant dans "CONNEXION".'
             goto error
          end
    end
    /*  La cardinalit� du parent "PERSONNE" dans l'enfant "CONNEXION" est limit�e � 1 */
    if update(PSN_ID)
    begin
       select @maxcard = (select count(*)
          from   CONNEXION old
          where ins.PSN_ID = old.PSN_ID)
       from  inserted ins
       where ins.PSN_ID is not null
       group by ins.PSN_ID
       order by 1
       if @maxcard > 1
       begin
          select @errno  = 50007,
                 @errmsg = 'Nombre maximum d''''occurrences d�pass�. Impossible de cr�er un enfant dans "CONNEXION".'
          goto error
       end
    end

    return

/*  Traitement d'erreurs  */
error:
    raiserror @errno @errmsg
    rollback  transaction
end
go


create trigger TU_CONNEXION on CONNEXION for update as
begin
   declare
      @maxcard  int,
      @numrows  int,
      @numnull  int,
      @errno    int,
      @errmsg   varchar(255)

      select  @numrows = @@rowcount
      if @numrows = 0
         return

      /*  Le parent "PERSONNE" doit exister � la mise � jour d'un enfant dans "CONNEXION"  */
      if update(PSN_ID)
      begin
         if (select count(*)
             from   PERSONNE t1, inserted t2
             where  t1.PSN_ID = t2.PSN_ID) != @numrows
            begin
               select @errno  = 50003,
                      @errmsg = 'PERSONNE" n''''existe pas. Impossible de modifier l''''enfant dans "CONNEXION".'
               goto error
            end
      end
      /*  La cardinalit� du parent "PERSONNE" dans l'enfant "CONNEXION" est limit�e � 1 */
      if update(PSN_ID)
      begin
         select @maxcard = (select count(*)
            from   CONNEXION old
            where ins.PSN_ID = old.PSN_ID)
         from  inserted ins
         where ins.PSN_ID is not null
         group by ins.PSN_ID
         order by 1
         if @maxcard > 1
         begin
            select @errno  = 50007,
                   @errmsg = 'Nombre maximum d''''occurrences d�pass�. Impossible de modifier un enfant dans "CONNEXION".'
            goto error
         end
      end

      return

/*  Traitement d'erreurs  */
error:
    raiserror @errno @errmsg
    rollback  transaction
end
go

create trigger TD_PERSONNE on PERSONNE for delete as
begin
    declare
       @numrows  int,
       @errno    int,
       @errmsg   varchar(255)

    select  @numrows = @@rowcount
    if @numrows = 0
       return

    /*  Impossible de supprimer le parent "PERSONNE" avec des enfants dans "CONNEXION"  */
    if exists (select 1
               from   CONNEXION t2, deleted t1
               where  t2.PSN_ID = t1.PSN_ID)
       begin
          select @errno  = 50006,
                 @errmsg = 'Il existe encore des enfants dans "CONNEXION". Impossible de supprimer le parent "PERSONNE".'
          goto error
       end

    /*  Impossible de supprimer le parent "PERSONNE" avec des enfants dans "COMMERCIAL"  */
    if exists (select 1
               from   COMMERCIAL t2, deleted t1
               where  t2.PSN_ID = t1.PSN_ID)
       begin
          select @errno  = 50006,
                 @errmsg = 'Il existe encore des enfants dans "COMMERCIAL". Impossible de supprimer le parent "PERSONNE".'
          goto error
       end

    /*  Impossible de supprimer le parent "PERSONNE" avec des enfants dans "CLIENT"  */
    if exists (select 1
               from   CLIENT t2, deleted t1
               where  t2.PSN_ID = t1.PSN_ID)
       begin
          select @errno  = 50006,
                 @errmsg = 'Il existe encore des enfants dans "CLIENT". Impossible de supprimer le parent "PERSONNE".'
          goto error
       end

    /*  Impossible de supprimer le parent "PERSONNE" avec des enfants dans "ASSISTANT"  */
    if exists (select 1
               from   ASSISTANT t2, deleted t1
               where  t2.PSN_ID = t1.PSN_ID)
       begin
          select @errno  = 50006,
                 @errmsg = 'Il existe encore des enfants dans "ASSISTANT". Impossible de supprimer le parent "PERSONNE".'
          goto error
       end

    /*  Impossible de supprimer le parent "PERSONNE" avec des enfants dans "TECHNICIEN"  */
    if exists (select 1
               from   TECHNICIEN t2, deleted t1
               where  t2.PSN_ID = t1.PSN_ID)
       begin
          select @errno  = 50006,
                 @errmsg = 'Il existe encore des enfants dans "TECHNICIEN". Impossible de supprimer le parent "PERSONNE".'
          goto error
       end


    return

/*  Traitement d'erreurs  */
error:
    raiserror @errno @errmsg
    rollback  transaction
end
go


create trigger TU_PERSONNE on PERSONNE for update as
begin
   declare
      @numrows  int,
      @numnull  int,
      @errno    int,
      @errmsg   varchar(255)

      select  @numrows = @@rowcount
      if @numrows = 0
         return

      /*  Impossible de modifier le code du parent "PERSONNE" avec des enfants dans "CONNEXION"  */
      if update(PSN_ID)
      begin
         if exists (select 1
                    from   CONNEXION t2, inserted i1, deleted d1
                    where  t2.PSN_ID = d1.PSN_ID
                     and  (i1.PSN_ID != d1.PSN_ID))
            begin
               select @errno  = 50005,
                      @errmsg = 'Il existe encore des enfants dans "CONNEXION". Impossible de modifier le code du parent "PERSONNE".'
               goto error
            end
      end

      /*  Impossible de modifier le code du parent "PERSONNE" avec des enfants dans "COMMERCIAL"  */
      if update(PSN_ID)
      begin
         if exists (select 1
                    from   COMMERCIAL t2, inserted i1, deleted d1
                    where  t2.PSN_ID = d1.PSN_ID
                     and  (i1.PSN_ID != d1.PSN_ID))
            begin
               select @errno  = 50005,
                      @errmsg = 'Il existe encore des enfants dans "COMMERCIAL". Impossible de modifier le code du parent "PERSONNE".'
               goto error
            end
      end

      /*  Impossible de modifier le code du parent "PERSONNE" avec des enfants dans "CLIENT"  */
      if update(PSN_ID)
      begin
         if exists (select 1
                    from   CLIENT t2, inserted i1, deleted d1
                    where  t2.PSN_ID = d1.PSN_ID
                     and  (i1.PSN_ID != d1.PSN_ID))
            begin
               select @errno  = 50005,
                      @errmsg = 'Il existe encore des enfants dans "CLIENT". Impossible de modifier le code du parent "PERSONNE".'
               goto error
            end
      end

      /*  Impossible de modifier le code du parent "PERSONNE" avec des enfants dans "ASSISTANT"  */
      if update(PSN_ID)
      begin
         if exists (select 1
                    from   ASSISTANT t2, inserted i1, deleted d1
                    where  t2.PSN_ID = d1.PSN_ID
                     and  (i1.PSN_ID != d1.PSN_ID))
            begin
               select @errno  = 50005,
                      @errmsg = 'Il existe encore des enfants dans "ASSISTANT". Impossible de modifier le code du parent "PERSONNE".'
               goto error
            end
      end

      /*  Impossible de modifier le code du parent "PERSONNE" avec des enfants dans "TECHNICIEN"  */
      if update(PSN_ID)
      begin
         if exists (select 1
                    from   TECHNICIEN t2, inserted i1, deleted d1
                    where  t2.PSN_ID = d1.PSN_ID
                     and  (i1.PSN_ID != d1.PSN_ID))
            begin
               select @errno  = 50005,
                      @errmsg = 'Il existe encore des enfants dans "TECHNICIEN". Impossible de modifier le code du parent "PERSONNE".'
               goto error
            end
      end


      return

/*  Traitement d'erreurs  */
error:
    raiserror @errno @errmsg
    rollback  transaction
end
go

