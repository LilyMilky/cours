<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="pagetest" page-height="29.7cm" page-width="40cm" margin="0,5cm">
                        <fo:region-body margin-bottom="3cm"/>
                        <fo:region-before extent="0.5cm"/>
                        <fo:region-after extent="2cm"/> 
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="pagetest" initial-page-number="1">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="right" border-top="lpt solid red">
                        listlivre, page:<fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:table border="solid">
                        
                        <fo:table-body>
                            <fo:table-row border-bottom="lpt solid black" background-color="red">
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Author</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Editor</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Titre</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="10cm" border="3pt solid blue">
                                    <fo:block width="10cm" text-align="center">Description</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Image</fo:block>
                                </fo:table-cell>    
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Prix</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Isbn</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="3pt solid blue">
                                    <fo:block text-align="center">Date</fo:block>
                                </fo:table-cell>
                            </fo:table-row>                            
                            <xsl:apply-templates select="listlivre/livre"/>
                        </fo:table-body>
                    </fo:table>              
                </fo:flow>
            </fo:page-sequence>
        </fo:root>       
    </xsl:template>
    
    <xsl:template match="listlivre/livre">
        <fo:table-row border-bottom="lpt solid black">
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:apply-templates select="listauthor/author"/>
                </fo:block>
            </fo:table-cell>
            
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:value-of select="editor"/>
                </fo:block>
            </fo:table-cell>
            
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:value-of select="titre"/>
                </fo:block>
            </fo:table-cell>
            
            <fo:table-cell width="10cm" border="3pt solid red">
                <fo:block width="10cm" text-align="center">
                    <xsl:value-of select="description"/>
                </fo:block>
            </fo:table-cell>  
                
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:apply-templates select="listimage/img"/>
                </fo:block>
             </fo:table-cell>
                 
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:value-of select="prix"/>
                </fo:block>
             </fo:table-cell>
                 
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:value-of select="isbn"/>
                </fo:block>
            </fo:table-cell>
                 
            <fo:table-cell border="3pt solid red">
                <fo:block text-align="center">
                    <xsl:call-template name="datefr">
                        <xsl:with-param name="dateen">
                            <xsl:value-of select="date"/>
                        </xsl:with-param>
                    </xsl:call-template>                  
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template name="datefr">
        <xsl:param name="dateen"/>
        <xsl:value-of select="substring($dateen,9,2)"/>
        <xsl:value-of select="substring($dateen,5,4)"/>
        <xsl:value-of select="substring($dateen,1,4)"/>
    </xsl:template>
    
    <xsl:template match="listimage/img">
        <fo:block>
            <fo:external-graphic src="'url(@img)'" content-width="75px" content-height="75px">
                <xsl:attribute name="src">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </fo:external-graphic>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="listauthor/author">
        <fo:block><xsl:value-of select="."/></fo:block>
    </xsl:template>
    
</xsl:stylesheet>