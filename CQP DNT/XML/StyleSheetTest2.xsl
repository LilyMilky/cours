<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:template match="/">
     <html>
         <head>
             <title>             </title>
         </head>
         <body>
             <table>
                 <tr>
                     <th>author</th>
                     <th>editor</th>
                     <th>titre</th>
                     <th>description</th>
                     <th>image</th>
                     <th>prix</th>
                     <th>isdn</th>
                     <th>date</th>
                 </tr>
                 <xsl:apply-templates select="listlivre/livre"/>
             </table>
         </body>
     </html>
     
 </xsl:template>
    <xsl:template match="listlivre/livre">
        <tr>
            <xsl:if test="@id mod 2 =0">
                <xsl:attribute name="bgcolor">
                    #CCCCCC
                </xsl:attribute>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="@lang='en'">
                    <xsl:attribute name="bgcolor">
                        #555555
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="bgcolor">
                        #999999
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="listauthor/author"/>
            <td><xsl:value-of select="editor"/></td>
            <td><xsl:value-of select="titre"/></td>
            <td><xsl:value-of select="description"/></td>
            <xsl:apply-templates select="listimage/img"/>
            <td><xsl:value-of select="prix"/></td>
            <td><xsl:value-of select="isdn"/></td>
            <td>
                <xsl:call-template name="datefr">
                    <xsl:with-param name="dateen">
                        <xsl:value-of select="date"/>
                    </xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="listauthor/author">
        <td><xsl:value-of select="."/></td>
    </xsl:template>
    
    <xsl:template name="datefr">
        <xsl:param name="dateen"/>
        <xsl:value-of select="substring($dateen,9,2)"/>
        <xsl:value-of select="substring($dateen,5,4)"/>
        <xsl:value-of select="substring($dateen,1,4)"/>
    </xsl:template>
    
    <xsl:template match="listimage/img">
        <td><xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:element name="img">
                <xsl:attribute name="src">
                    <xsl:value-of select="."/>
                </xsl:attribute>
                <xsl:attribute name="width">
                    <xsl:value-of select="100"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:element></td>
    </xsl:template>
    
</xsl:stylesheet>