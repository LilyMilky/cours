<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="/">
        <html>
            <head>
                <link type="text/css" rel="stylesheet" href="vehicules.css"/>
            </head>
            <body>
                <table>                                        
                    <tr>   
                        <xsl:attribute name="bgcolor">
                            #222222
                        </xsl:attribute>                         
                        <th style="color:white">Photo</th>
                        <th style="color:white">Annee</th>
                        <th style="color:white">Marque</th>
                        <th style="color:white">Modele</th>
                        <th style="color:white">Km</th>
                        <th style="color:white">Couleur</th>
                        <th style="color:white">Prix</th>
                        <th style="color:white">Service CarOccas</th>
                        <th style="color:white">Selection CarOccas</th>
                    </tr>
                    <xsl:apply-templates select="vehicules/vehicule">
                        <xsl:sort data-type="number" select="kilometrage" order="ascending"/>
                    </xsl:apply-templates>
                </table>
            </body>
        </html>        
    </xsl:template>
    
    <xsl:template match="vehicules/vehicule">          
        <tr>
            <xsl:if test="(kilometrage &lt; 20000)">
                <xsl:if test="caroccas/@garantie ='oui'">
                    <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                </xsl:if>
            </xsl:if>
            <xsl:if test="position() mod 2 =1">
                <xsl:attribute name="bgcolor">
                    #CCCCCC
                </xsl:attribute>
            </xsl:if>
            <td><xsl:apply-templates select="photo"/></td>
            <td><xsl:value-of select="@annee"/></td>
            <td><xsl:value-of select="@marque"/></td>
            <td><xsl:value-of select="@modele"/></td>
            <td><xsl:value-of select="kilometrage"/></td>
            <xsl:call-template name="couleur">
                <xsl:with-param name="couleur">
                    <xsl:value-of select="couleur"/>
                </xsl:with-param>
            </xsl:call-template> 
            <td><xsl:value-of select="prix"/> €</td>
            <td><xsl:apply-templates select="caroccas"/></td> 
            <td><xsl:call-template name="selection"/></td>            
         </tr>             
    </xsl:template>
    
    <xsl:template name="couleur">  
        <xsl:param name="couleur"/>
        <xsl:choose>
            <xsl:when test="couleur='noir'">
                <td bgcolor="#000000"/>
            </xsl:when>
            <xsl:when test="couleur='argent'">
                <td bgcolor="#DADEDD"/>
            </xsl:when>
            <xsl:when test="couleur='gris'">
                <td bgcolor="#9A9B9B"/>
            </xsl:when>
            <xsl:when test="couleur='bleu_clair'">
                <td bgcolor="#00DCFE"/>
            </xsl:when>
            <xsl:when test="couleur='bleu_marine'">
                <td bgcolor="#0019FA"/>
            </xsl:when>
            <xsl:when test="couleur='blanc'">
                <td bgcolor="#FEFEFE"/>
            </xsl:when>
            <xsl:when test="couleur='jaune'">
                <td bgcolor="#E2FB00"/>
            </xsl:when>
            <xsl:when test="couleur='rouge'">
                <td bgcolor="#FB1100"/>
            </xsl:when>
            <xsl:when test="couleur='violet'">
                <td bgcolor="#6D00FB"/>
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template match="photo">       
            <xsl:element name="img">
                <xsl:attribute name="src">
                    <xsl:value-of select="."/>
                </xsl:attribute>
                <xsl:attribute name="width">
                    <xsl:value-of select="50"/>
                </xsl:attribute>
            </xsl:element>        
    </xsl:template>
    
    <xsl:template match="caroccas">
        <xsl:if test="@garantie='oui'">
            <xsl:if test="@reprise='non'">
                Garantie
            </xsl:if>
        </xsl:if>
        
        <xsl:if test="@reprise='oui'">
            <xsl:if test="@garantie='non'">
                Reprise
            </xsl:if> 
        </xsl:if>            

        <xsl:if test="@reprise='oui'">
            <xsl:if test="@garantie='oui'">
                Garantie + Reprise
            </xsl:if> 
        </xsl:if> 
        
        <xsl:if test="@reprise='non'">
            <xsl:if test="@garantie='non'">
                Aucun
            </xsl:if> 
        </xsl:if>     
    </xsl:template>
    
    <xsl:template name="selection">       
        <xsl:if test="caroccas/@reprise='oui'">
            <xsl:if test="caroccas/@garantie='non'">
                <xsl:if test="kilometrage &lt; 20000">
                    <img src="etoile.gif"/>
                </xsl:if>
            </xsl:if>
        </xsl:if>            
        
        <xsl:if test="caroccas/@garantie='oui'">
            <xsl:if test="caroccas/@reprise='non'">
                <xsl:if test="kilometrage &lt; 20000">                   
                    <img src="etoile.gif"/> <img src="etoile.gif"/>
                </xsl:if>
            </xsl:if>
        </xsl:if>            
        
        <xsl:if test="caroccas/@reprise='oui'">
            <xsl:if test="caroccas/@garantie ='oui'">
                <xsl:if test="kilometrage &lt; 20000">
                    <img src="etoile.gif"/> <img src="etoile.gif"/> <img src="etoile.gif"/>
                </xsl:if>
            </xsl:if> 
        </xsl:if>       
    </xsl:template>
</xsl:stylesheet>