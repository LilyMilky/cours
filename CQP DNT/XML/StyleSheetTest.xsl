<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>liste livres</title>
            </head>
            <body>
                <table>
                    <tr>
                        <th>authors</th>
                        <th>editor</th>
                        <th>titre</th>
                        <th>description</th>
                        <th>images</th>
                        <th>prix</th>
                        <th>isbn</th>
                        <th>date</th>
                    </tr>
                    <xsl:for-each select="listlivre/livre">
                        <tr>
                            <xsl:for-each select="listauthor/author">
                                <td><xsl:value-of select="."/></td>
                            </xsl:for-each>
                            <td><xsl:value-of select="editor"/></td>
                            <td><xsl:value-of select="titre"/></td>
                            <td><xsl:value-of select="description"/></td>
                            <xsl:for-each select="listimage/img">
                                <td>
                                    <xsl:element name="a">
                                        <xsl:attribute name="href">
                                                <xsl:value-of select="."/>
                                        </xsl:attribute>
                                    <xsl:element name="img">
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="."/>
                                        </xsl:attribute>
                                    </xsl:element>
                                    </xsl:element>
                                </td>
                            </xsl:for-each>
                            <td><xsl:value-of select="prix"/></td>
                            <td><xsl:value-of select="isbn"/></td>
                            <td><xsl:value-of select="date"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>  
</xsl:stylesheet>