<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/disque">
        <html>
            <head><link type="text/css" rel="stylesheet" href="recursif.css"/></head>
            <body>
                <ul><li class="disque">                         
                    contenu du disque : <xsl:value-of select="count(descendant::dossier)"/>
                    <xsl:choose>
                        <xsl:when test="count(descendant::dossier) > 1">
                            dossiers, 
                        </xsl:when>
                        <xsl:otherwise>
                            dossier, 
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:value-of select="count(descendant::fichier)"/>
                    <xsl:choose>
                        <xsl:when test="count(descendant::fichier) > 1">
                            fichiers 
                        </xsl:when>
                        <xsl:otherwise>
                            fichier 
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
                    <xsl:apply-templates/>                  
                </ul>
            </body>
        </html>     
    </xsl:template> 

    <xsl:template match="dossier">   
      <ul>  <li class="dossier">
          <xsl:value-of select="@name"/> -
        <xsl:value-of select="count(descendant::dossier)"/>
        <xsl:choose>
            <xsl:when test="count(descendant::dossier) > 1">
                dossiers, 
            </xsl:when>
            <xsl:otherwise>
                dossier, 
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="count(descendant::fichier)"/>
        <xsl:choose>
            <xsl:when test="count(descendant::fichier) > 1">
                fichiers 
            </xsl:when>
            <xsl:otherwise>
                fichier 
            </xsl:otherwise>
        </xsl:choose> 
      </li>
        <xsl:apply-templates select="dossier"/>        
        <xsl:apply-templates select="fichier"/>
      </ul>
    </xsl:template>
    
    <xsl:template match="fichier">   
        <ul><li>
            
            <xsl:attribute name="class">
                <xsl:value-of select="substring(@extension,2)"/>
            </xsl:attribute>
            
            <xsl:value-of select="@name"/>
            <xsl:value-of select="@extension"/>
        </li></ul>
    </xsl:template>
</xsl:stylesheet>