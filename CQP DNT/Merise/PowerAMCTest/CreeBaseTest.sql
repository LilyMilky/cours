/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  19/09/2012 16:35:08                      */
/*==============================================================*/


drop table if exists CATEGORIES;

drop table if exists CLIENTS;

drop table if exists COMMANDES;

drop table if exists CONTIENT;

drop table if exists PRODUITS;

/*==============================================================*/
/* Table : CATEGORIES                                           */
/*==============================================================*/
create table CATEGORIES
(
   ID_CAT               text not null,
   CAT_ID_CAT           text,
   LIBELLE              varchar(50) not null,
   primary key (ID_CAT)
);

/*==============================================================*/
/* Table : CLIENTS                                              */
/*==============================================================*/
create table CLIENTS
(
   ID_CLIENT            int not null auto_increment,
   NOM                  varchar(30) not null,
   PRENOM               varchar(30) not null,
   ADDRESS1             varchar(100) not null,
   ADDRESS2             varchar(100),
   CODEPOSTAL           text not null,
   VILLE                varchar(50) not null,
   MAIL                 varchar(100),
   primary key (ID_CLIENT)
);

/*==============================================================*/
/* Table : COMMANDES                                            */
/*==============================================================*/
create table COMMANDES
(
   REF_COM              int not null auto_increment,
   ID_CLIENT            int not null,
   DATE                 datetime not null,
   ETAT                 char(1) not null,
   primary key (REF_COM)
);

/*==============================================================*/
/* Table : CONTIENT                                             */
/*==============================================================*/
create table CONTIENT
(
   REF_COM              int not null,
   REF_PROD             int not null,
   QUANTITE_COMMANDE    int not null,
   PRIX_UNITAIRE_EFFECTIF decimal(7,2) not null,
   primary key (REF_COM, REF_PROD)
);

/*==============================================================*/
/* Table : PRODUITS                                             */
/*==============================================================*/
create table PRODUITS
(
   REF_PROD             int not null auto_increment,
   ID_CAT               text not null,
   DESIGNATION          varchar(30) not null,
   PRIX_UNITAIRE_PREVU  decimal(7,2) not null,
   DESCRIPTION          varchar(255),
   STOCK                smallint not null,
   primary key (REF_PROD)
);

alter table CATEGORIES add constraint FK_EST_PARENT_DE foreign key (CAT_ID_CAT)
      references CATEGORIES (ID_CAT) on delete restrict on update restrict;

alter table COMMANDES add constraint FK_EFFECTUE foreign key (ID_CLIENT)
      references CLIENTS (ID_CLIENT) on delete restrict on update restrict;

alter table CONTIENT add constraint FK_CONTIENT foreign key (REF_PROD)
      references PRODUITS (REF_PROD) on delete restrict on update restrict;

alter table CONTIENT add constraint FK_CONTIENT2 foreign key (REF_COM)
      references COMMANDES (REF_COM) on delete restrict on update restrict;

alter table PRODUITS add constraint FK_APPARTIENT foreign key (ID_CAT)
      references CATEGORIES (ID_CAT) on delete restrict on update restrict;

