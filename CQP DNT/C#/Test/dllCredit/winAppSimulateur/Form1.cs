﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dllCredit;

namespace winAppSimulateur
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcul_Click(object sender, EventArgs e)
        {
            double m, t, mens;
            int d;
            
            m = double.Parse(txtMontant.Text);
            t = double.Parse(txtTaux.Text);
            d = Int32.Parse(txtDuree.Text);

            SimulateurCredit simu = new SimulateurCredit(m, d, t);

            mens = simu.CalculMensualite();

            label4.Text = mens.ToString("## ###.##");
        }
    }
}
