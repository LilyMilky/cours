﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dllCredit;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCalculer_Click(object sender, EventArgs e)
    {
        double m, t, mens;
        int d;

        m = double.Parse(txtMont.Text);
        t = double.Parse(txtTaux.Text);
        d = Int32.Parse(txtDuree.Text);

        SimulateurCredit simu = new SimulateurCredit(m, d, t);

        mens = simu.CalculMensualite();
        Label1.Text = mens.ToString("####.##");

    }
}
