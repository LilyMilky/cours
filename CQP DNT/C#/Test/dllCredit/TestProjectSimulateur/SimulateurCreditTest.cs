﻿using dllCredit;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProjectSimulateur
{
    
    
    /// <summary>
    ///This is a test class for SimulateurCreditTest and is intended
    ///to contain all SimulateurCreditTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SimulateurCreditTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
        [DataSource("System.Data.SqlClient", 
            "Data Source=.\\sqlexpress;Initial Catalog=\"D:\\VISUAL STUDIO 2010\\SLNTESTDNT6\\TESTPROJECTCREDIT\\BIN\\DEBUG\\BDDTESTS.MDF\";Integrated Security=True", 
            "tblDonnees", 
            DataAccessMethod.Sequential), 
            TestMethod()]
        public void CalculMensualiteTest()
        {

            double m = Convert.ToDouble(TestContext.DataRow["capital"]);
            double t =Convert.ToDouble(TestContext.DataRow["taux"]);;
            int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
            double expected =Convert.ToDouble(TestContext.DataRow["mensualite"]); ;

            SimulateurCredit target = new SimulateurCredit(m,d,t); // TODO: Initialize to an appropriate value
             // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculMensualite();
        
            Assert.AreEqual(expected, actual, 0.01,"Erreur Calcul Mensualité");
            
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
        [TestMethod()]
        public void CalculMensualiteTestBDD()
        {
            SimulateurCredit target = new SimulateurCredit(150000, 15, 3.2); // TODO: Initialize to an appropriate value
            double expected = 1050.36; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculMensualite();

            Assert.AreEqual(expected, actual, 0.01, "Erreur Calcul Mensualité");

            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
        [DataSource("System.Data.SqlClient",
            "Data Source=.\\sqlexpress;Initial Catalog=\"D:\\VISUAL STUDIO 2010\\SLNTESTDNT6\\TESTPROJECTCREDIT\\BIN\\DEBUG\\BDDTESTS.MDF\";Integrated Security=True",
            "tblDonnees",
            DataAccessMethod.Sequential),
            TestMethod()]
        public void CalculMensualiteTestTauxZero()
        {
            double m = Convert.ToDouble(TestContext.DataRow["capital"]);
            double t = Convert.ToDouble(TestContext.DataRow["taux"]); ;
            int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
            double expected = Convert.ToDouble(TestContext.DataRow["mensualite"]); ;

            SimulateurCredit target = new SimulateurCredit(m, d, t); // TODO: Initialize to an appropriate value
            // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculMensualite();
            Assert.AreEqual(actual, expected,0.01, "eRREUR tAUX zERO3");
            

            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
        [DataSource("System.Data.SqlClient",
            "Data Source=.\\sqlexpress;Initial Catalog=\"D:\\VISUAL STUDIO 2010\\SLNTESTDNT6\\TESTPROJECTCREDIT\\BIN\\DEBUG\\BDDTESTS.MDF\";Integrated Security=True",
            "tblDonnees",
            DataAccessMethod.Sequential),
            TestMethod()]
        public void CalculMensualiteTestParametresInvalidesBDD()
        {
            double m = Convert.ToDouble(TestContext.DataRow["capital"]);
            double t = Convert.ToDouble(TestContext.DataRow["taux"]); ;
            int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
            double expected = -1;

            SimulateurCredit target = new SimulateurCredit(m, d, t); // TODO: Initialize to an appropriate value
            // TODO: Initialize to an appropriate value

            double actual;
            actual = target.CalculMensualite();
            Assert.AreEqual(expected, actual, "Erreur Parametres Invalides");


            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
        [TestMethod()]
        public void CalculMensualiteTestParametresInvalides()
        {
            SimulateurCredit target = new SimulateurCredit(-150000, 15, 3.2); // TODO: Initialize to an appropriate value
            double expected = -1; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculMensualite();
            Assert.AreEqual(expected, actual, "Erreur Parametres Invalides");


            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        /// <summary>
        ///A test for CalculMensualite
        ///</summary>
         [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
             "|DataDirectory|\\JeuDonnees.xml", 
             "cas",
             DataAccessMethod.Sequential), 
           DeploymentItem("TestProjectSimulateur\\JeuDonnees.xml"), 
           TestMethod()]
        public void CalculMensualiteTestParametresInvalidesXML()
        {
            double m = Convert.ToDouble(TestContext.DataRow["mont"]);
            double t = Convert.ToDouble(TestContext.DataRow["taux"]); ;
            int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
            double expected = -1;

            SimulateurCredit target = new SimulateurCredit(m, d, t); // TODO: Initialize to an appropriate value
            // TODO: Initialize to an appropriate value

            double actual;
            actual = target.CalculMensualite();
            Assert.AreEqual(expected, actual, "Erreur Parametres Invalides");


            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

         /// <summary>
         ///A test for CalculMensualite
         ///</summary>
         [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
             "|DataDirectory|\\JeuDonnees.xml",
             "cas",
             DataAccessMethod.Sequential),
           DeploymentItem("TestProjectSimulateur\\JeuDonnees.xml"),
           TestMethod()]
         public void CalculMensualiteTestException()
         {
             double m = Convert.ToDouble(TestContext.DataRow["mont"]);
             double t = Convert.ToDouble(TestContext.DataRow["taux"]); ;
             int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
             //double expected = -1;

             SimulateurCredit target = new SimulateurCredit(m, d, t); // TODO: Initialize to an appropriate value
             // TODO: Initialize to an appropriate value

             double actual;
             actual = target.CalculMensualite();

            


             //Assert.Inconclusive("Verify the correctness of this test method.");
         }

    }
}
