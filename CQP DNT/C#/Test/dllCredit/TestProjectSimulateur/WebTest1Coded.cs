﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestProjectSimulateur
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.VisualStudio.TestTools.WebTesting;
    using Microsoft.VisualStudio.TestTools.WebTesting.Rules;


    [DataSource("dsDonnees", "System.Data.SqlClient", "Data Source=.\\sqlexpress;Initial Catalog=\"D:\\VISUAL STUDIO 2010\\SLNTESTDNT6\\TESTP" +
        "ROJECTCREDIT\\BIN\\DEBUG\\BDDTESTS.MDF\";Integrated Security=True", Microsoft.VisualStudio.TestTools.WebTesting.DataBindingAccessMethod.Sequential, Microsoft.VisualStudio.TestTools.WebTesting.DataBindingSelectColumns.SelectOnlyBoundColumns, "tblDonnees")]
    [DeploymentItem("testprojectsimulateur\\JeuDonnees.xml", "testprojectsimulateur")]
    [DataSource("dsXML", "Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\testprojectsimulateur\\JeuDonnees.xml", Microsoft.VisualStudio.TestTools.WebTesting.DataBindingAccessMethod.Sequential, Microsoft.VisualStudio.TestTools.WebTesting.DataBindingSelectColumns.SelectOnlyBoundColumns, "cas")]
    [IncludeDeclarativeWebTest("wsSimulateur", "..\\..\\wssimulateur.webtest", "b67eb0ec-9ee3-44c3-aae7-72ea5ee4568d")]
    [IncludeDeclarativeWebTest("wsXML", "..\\..\\wsxml.webtest", "106966b6-08bf-40bb-bdf5-39bc6fb9a326")]
    public class WebTest1Coded : WebTest
    {

        public WebTest1Coded()
        {
            this.PreAuthenticate = true;
        }

        public override IEnumerator<WebTestRequest> GetRequestEnumerator()
        {
            // Initialize validation rules that apply to all requests in the WebTest
            if ((this.Context.ValidationLevel >= Microsoft.VisualStudio.TestTools.WebTesting.ValidationLevel.Low))
            {
                ValidateResponseUrl validationRule1 = new ValidateResponseUrl();
                this.ValidateResponse += new EventHandler<ValidationEventArgs>(validationRule1.Validate);
            }
            if ((this.Context.ValidationLevel >= Microsoft.VisualStudio.TestTools.WebTesting.ValidationLevel.Low))
            {
                ValidationRuleResponseTimeGoal validationRule2 = new ValidationRuleResponseTimeGoal();
                validationRule2.Tolerance = 0D;
                this.ValidateResponseOnPageComplete += new EventHandler<ValidationEventArgs>(validationRule2.Validate);
            }
            if ((this.Context.ValidationLevel >= Microsoft.VisualStudio.TestTools.WebTesting.ValidationLevel.High))
            {
                ValidationRuleFindText validationRule3 = new ValidationRuleFindText();
                validationRule3.FindText = "Welcome to ASP.NET!";
                validationRule3.IgnoreCase = false;
                validationRule3.UseRegularExpression = false;
                validationRule3.PassIfTextFound = true;
                this.ValidateResponse += new EventHandler<ValidationEventArgs>(validationRule3.Validate);
            }

            foreach (WebTestRequest r in IncludeWebTest("wsSimulateur", true)) { yield return r; };

            foreach (WebTestRequest r in IncludeWebTest("wsXML", true)) { yield return r; };

            WebTestRequest request1 = new WebTestRequest("http://localhost/");
            request1.Method = "POST";
            StringHttpBody request1Body = new StringHttpBody();
            request1Body.ContentType = "";
            request1Body.InsertByteOrderMark = false;
            request1Body.BodyString = "";
            request1.Body = request1Body;
            yield return request1;
            request1 = null;
        }
    }
}
