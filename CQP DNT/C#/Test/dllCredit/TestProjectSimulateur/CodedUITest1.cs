﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace TestProjectSimulateur
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest1
    {
        public CodedUITest1()
        {
        }

        [DataSource("System.Data.SqlClient", "Data Source=.\\sqlexpress;Initial Catalog=\"D:\\VISUAL STUDIO 2010\\SLNTESTDNT6\\TESTPROJECTCREDIT\\BIN\\DEBUG\\BDDTESTS.MDF\";Integrated Security=True", "tblDonnees", DataAccessMethod.Sequential), TestMethod]
        public void CodedUITestMethod1()
        {
            double m = Convert.ToDouble(TestContext.DataRow["capital"]);
            double t = Convert.ToDouble(TestContext.DataRow["taux"]); ;
            int d = Convert.ToInt32(TestContext.DataRow["duree"]); ;
            double mens = Convert.ToDouble(TestContext.DataRow["mensualite"]); ;


            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            this.UIMap.RecordedMethodSimulateur(m,d,t);
            this.UIMap.AssertMethodValeurExpectedValues.UIItem105036TextDisplayText = mens.ToString();
            this.UIMap.AssertMethodValeur(mens);
            
            this.UIMap.Fermeture();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
