﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dllCredit
{
    public class SimulateurCredit
    {
        private double montant;

        public double Montant
        {
            get { return montant; }
            set { montant = value; }
        }

        private int duree;
            
        public int Duree
        {
            get { return duree; }
            set { duree = value; }
        }

        private double taux;

        public double Taux
        {
            get { return taux; }
            set { taux = value; }
        }

        private double mensualite;

        public double Mensualite
        {
            get { return mensualite; }
            set { mensualite = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="d"></param>
        /// <param name="t"></param>
        public SimulateurCredit(double m, int d, double t)
        {
            this.Montant = m;
            this.Duree = d * 12;
            this.Taux = t / 100;
        }

        public SimulateurCredit():this(0.0,0,0.0)
        {  }

        public double CalculMensualite()
        {
            if ((montant < 3000) || (duree < 1) || (taux < 0))
                return -1;

            if (Taux == 0)
            {
                mensualite = montant / duree;
                return mensualite;
            }

           
            double tt = Taux / 12;
            double temp = 1 - (Math.Pow(1 + tt, -Duree));

            Mensualite = (Montant * tt) / temp;

            return Mensualite;
        }

        public double CalculMensualite(double m, int d, double t)
        {
            this.Montant = m;
            this.Duree = d * 12;
            this.Taux = t / 100;

            return CalculMensualite();
        }
    }
}
