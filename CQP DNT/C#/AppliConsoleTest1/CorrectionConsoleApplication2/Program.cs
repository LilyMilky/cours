﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Program
    {
        static void Main(string[] args)
        {
             ConsoleKeyInfo touche;
             do
             {
                 Console.Clear();
                 Console.WriteLine("------- MENU -------");
                 Console.WriteLine(" 1 - Calcul Moyenne");
                 Console.WriteLine(" 2 - Gestion Tableau");
                 Console.WriteLine(" Q : Fin");
                 touche = Console.ReadKey();

                 switch (touche.Key)
                 {
                     case ConsoleKey.NumPad1:
                         CalculMoyenne();
                         break;

                     case ConsoleKey.NumPad2:
                         GestionTableau();
                         Console.ReadLine();
                         break;
                     case ConsoleKey.Q:
                         break;

                     default:
                         Console.WriteLine("Touche Invalide !");
                         Console.Read();   
                         break;
                 }

             } while (touche.Key != ConsoleKey.Q);
           Console.Read();
        }

        private static void GestionTableau()
        {
            int[]tab = new int[3];
            tab[0] = 102;
            tab[1] = 104;
            tab[2] = 106;

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }

            int[] tab2 = new int[3];

            tab.CopyTo(tab2, 0);
            Console.WriteLine("---------------------------");

            foreach (int item in tab2)
            {
                Console.WriteLine(item);
            }

        }

        private static void CalculMoyenne()
        {
            Console.WriteLine("Nombre de Notes ?");
            byte nb = 0;


            string sNb = Console.ReadLine();
            if (Byte.TryParse(sNb, out nb) == false)
            {
                Console.WriteLine("Nombre Invalide");

            }

            int cpt = 1;
            double som = 0;
            float min, max, moy;

            max = moy = 0;
            min = 20;

            while (cpt <= nb)
            {
                Console.WriteLine("Saisir une Note (0-20)");
                float note = Single.Parse(Console.ReadLine());

                if (!((note >= 0) && (note <= 20)))
                {
                    Console.WriteLine("Note Invalide");
                    continue;
                }
                string msg = Resultat(note);
                Console.WriteLine(msg);
                som += note;
                cpt++;

                if (note > max)
                    max = note;
                if (note < min)
                    min = note;

            }
            moy = (float)(som / nb);
            String aff = String.Format("Moyenne {0:#00.000} Max : {1} Min : {2}",
                moy, max, min);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(aff);
            Console.Read();
        }

        private static string Resultat(float note)
        {
            if (note < 8)
                return "Non Admis";
            else
                if (note < 10)
                    return "Rattrapage";
                else
                    return "Admis";
        }
    }
}
