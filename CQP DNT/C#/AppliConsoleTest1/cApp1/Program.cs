﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Program
    {
        static void Main(string[] args)
        {
             ConsoleKeyInfo touche;
             do
             {
                 Console.Clear();
                 Console.WriteLine("------- MENU -------");
                 Console.WriteLine(" 1 - Calcul Moyenne");
                 Console.WriteLine(" 2 - Gestion Tablo ");
                 Console.WriteLine(" 3 - Permutation   ");
                 Console.WriteLine(" 4 - Classe        ");
                 Console.WriteLine(" 5 - Classe Personne Exercice");
                 Console.WriteLine(" Q : Fin           ");
                 touche = Console.ReadKey();

                 switch (touche.Key)
                 {
                     case ConsoleKey.NumPad1:
                         CalculMoyenne();
                         break;
                     case ConsoleKey.NumPad2:
                        GestionTablo2();
                        Console.Read();
                         break;
                     case ConsoleKey.NumPad3:
                         Fonctions();
                         Console.Read();
                         break;
                     case ConsoleKey.NumPad4:
                         AppelClass();
                         Console.Read();
                         break;
                     case ConsoleKey.NumPad5:
                         AppelClassExercice();
                         Console.Read();
                         break;
                     case ConsoleKey.Q:
                         break;

                     default:
                         Console.WriteLine("Touche Invalide !");
                         Console.Read();   
                         break;
                 }

             } while (touche.Key != ConsoleKey.Q);
           Console.Read(); 
        }

        private static void AppelClassExercice()
        {
            string Societe;

            Console.Write("Veuillez saisir votre nom: ");
            string Name = Console.ReadLine();

            do
            {
                Console.Write("Veuillez saisir la société dans laquelle vous travaillez(32 caractères max), Tapez 0 si vous êtes non salarié: ");
                Societe = Console.ReadLine();
            } while (Societe.Length > 32);

            Person Pers = new Person(Name, Societe);

            Console.WriteLine(Pers.Affiche());

            do
            {
                Console.Write("Veuillez saisir votre nouvelle société dans laquelle vous travaillez(32 caractères max), Tapez 0 si vous êtes non salarié: ");
                Societe = Console.ReadLine();
            } while (Societe.Length > 32);
            Pers.ChangeSociete(Societe);
            Console.WriteLine(Pers.Affiche());
        }

        private static void AppelClass()
        {
            Personne Pers = new Personne("Hakurei", "milky", 22);
            Console.WriteLine(Pers.Affiche());
        }

        #region Fonction
        private static void Fonctions()
        {
            int a = 10, b = 20;
            Permute(ref b, ref a);
            Console.WriteLine(a + " " + b);

            Permute(y: ref b, x: ref a);
            fctArgVariables("toto", "titi", "tata");
            Console.WriteLine("---------------");
            fctArgVariables("toto", 2, "titi", "tata", 10, "coco", "rico");


        }
        private static void fctArgVariables(params object[] chaine)
        {
            foreach (object item in chaine)
            {
                Console.WriteLine(item.GetType().Name);
                if (item is int)
                {
                    Console.WriteLine(item + " est un entier");
                }
            }
        }
        private static void Permute(ref int y, ref int x)
        {
            int tampon = 0;
            tampon = x;
            x = y;
            y = tampon;
            Console.WriteLine("Dans fonction : " + x + " " + y);
        } 
        #endregion

        #region Tableau
        private static void GestionTablo2()
        {
            //int[,] tab2dim = new int[2, 3];
            int[,] tab2dim = { { 10, 20, 30 }, { 50, 80, 110 } };

            for (int i = 0; i < tab2dim.GetLength(0); i++)
            {
                for (int j = 0; j < tab2dim.GetLength(1); j++)
                {
                    Console.WriteLine(tab2dim[i, j]);
                }
                {

                }

                int[][] tabD = new int[2][];
                tabD[0] = new int[] { 20, 10, 30 };
                tabD[1] = new int[] { 5, 7 };

                Console.WriteLine(tabD[0][1]);


            }


        }

        private static void GestionTablo()
        {
            int[] tab = new int[3];
            tab[0] = 102;
            tab[1] = 104;
            tab[2] = 106;

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }



            int[] tab2 = new int[4];
            tab.CopyTo(tab2, 0);
            Console.WriteLine("-------------------");
            foreach (int item in tab2)
            {
                Console.WriteLine(item);
            }

            int[] tab3 = new int[3];


            tab3 = tab;

            foreach (int item in tab3)
            {
                Console.WriteLine(item);
            }

            tab3 = (int[])tab.Clone();
            tab[0] = 200;
            foreach (int item in tab3)
            {
                Console.WriteLine(item);
            }
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }
        } 
        #endregion

        #region "Instructuions de Base"


        private static void CalculMoyenne()
        {
            Console.WriteLine("Nombre de Notes ?");
            byte nb = 0;


            string sNb = Console.ReadLine();
            if (Byte.TryParse(sNb, out nb) == false)
            {
                Console.WriteLine("Nombre Invalide");

            }

            int cpt = 1;
            double som = 0;
            float min, max, moy;

            max = moy = 0;
            min = 20;

            while (cpt <= nb)
            {
                Console.WriteLine("Saisir une Note (0-20)");
                float note = Single.Parse(Console.ReadLine());

                if (!((note >= 0) && (note <= 20)))
                {
                    Console.WriteLine("Note Invalide");
                    continue;
                }
                string msg = Resultat(note);
                Console.WriteLine(msg);
                som += note;
                cpt++;

                if (note > max)
                    max = note;
                if (note < min)
                    min = note;

            }
            moy = (float)(som / nb);
            String aff = String.Format("Moyenne {0:#00.000} Max : {1} Min : {2}",
                moy, max, min);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(aff);
            Console.Read();
        }

        private static string Resultat(float note)
        {
            if (note < 8)
                return "Non Admis";
            else
                if (note < 10)
                    return "Rattrapage";
                else
                    return "Admis";
        } 
        
        #endregion


    }
}
