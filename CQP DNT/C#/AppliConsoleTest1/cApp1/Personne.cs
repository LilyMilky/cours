﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Personne
    {
        private string _name = String.Empty;

        public string name 
        {
            get { return _name; }
            set { _name = value; }
        }

        
        private int _age = 0;

        public int age
        {
            get { return _age; }
            set { _age = value; }
        }


        private string _prenom = String.Empty;

        public string prenom
        {
            get { return _prenom.Substring(0,1).ToUpper() + _prenom.Substring(1).ToLower(); }
            set { _prenom = value; }
        }

        private static int _ageMaj = 18;

        public static int Agemaj
        {
            get { return Personne._ageMaj; }
            set { Personne._ageMaj = value; }
        }

        public Personne(string vNom = "", string vPrenom = "", int vAge = 0)
        {
            name = vNom;
            prenom = vPrenom;
            age = vAge;
        }

        internal string Affiche()
        {
            string ch = String.Format("\nNom: {0} - Prenom : {1} - Age : {2}", this._name, this.prenom, this._age);

            ch += (this.age > Personne.Agemaj) ? " est majeur" : " est mineur";

            return ch;
        }
    }
}
