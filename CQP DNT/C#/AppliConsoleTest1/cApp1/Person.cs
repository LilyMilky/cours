﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Person
    {
        private string _name;

        public string Name
        {
            get { return _name.ToUpper(); }
        }

        private string _societe;

        public string Societe
        {
            get { return _societe.ToUpper(); }
            set { _societe = value; }
        }

        internal string Affiche()
        {
            string ch = String.Format("\nNom: {0} - Société : {1}", Name, Societe);

            return ch;
        }

        public Person(string nom, string socie)
        {
            _name = nom;
            if (socie != "0")
                Societe = socie;
            else
                Societe = "Sans Emploi";
        }

        ~Person()
        {
            Console.WriteLine("Objet detruit");
        }

        internal void ChangeSociete(string socie)
        {
            if (socie != "0")
                Societe = socie;
            else
                Societe = "Sans Emploi";
        }

    }
}
