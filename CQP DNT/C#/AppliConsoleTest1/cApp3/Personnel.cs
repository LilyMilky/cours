﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Personnel
    {
        private Person[] tab = null;
        private int index = 0;

        public Personnel(int nb=5)
        {
            tab = new Person[nb];
        }

        public int Nombre { get { return index; } }

        public bool AjouterPers(Person p)
        {
            if (index < tab.Length)
            {
                tab[index++] = p;
                return true;
            }

            return false;
        }

        public void Lister()
        {
            foreach (Person elem in tab)
            {
                Console.WriteLine(elem.Affiche());
            }
        }

        /*public string this[int index]
        {
            get { return tab[index].nom; }
            set { tab[index].nom = value; }
        }*/

        public Person this[int pos]
        {
            get
            {
                pos--;
                if (pos >= 0 && pos <= this.index)
                    return tab[pos];
                else
                    return null;

            }
        }
    }
}
