﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class CercleCompo
    {
        Point2D centre;
        class Point2D
        {
                 int x, y;

                public Point2D(int x, int y)
                {
                    this.x = x;
                    this.y = y;
                }

                public override string ToString()
                {
                    return "X :" + this.x+ "Y: " + this.y;
                }
        }
        int rayon;

        public CercleCompo(int x, int y, int r)
        {
            centre = new Point2D(x, y);
            rayon = r;
        }

        public override string ToString()
        {
            return "P2D" + centre + " Rayon : " + this.rayon;
        }

    }
}
