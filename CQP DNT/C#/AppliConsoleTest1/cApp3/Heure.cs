﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Heure
    {
        private int heure, minute;

        public Heure(int h, int m)
        {
            heure = h;
            minute = m;
        }

        public static int operator -(Heure hDeb, Heure hFin)
        {
            return (hDeb.heure * 60 + hDeb.minute) - (hFin.heure * 60 + hFin.minute);
        }

        public static Heure operator ++(Heure hDeb)
        {
            return new Heure(hDeb.heure + 1, hDeb.minute);
        }
    }
}
