﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Program
    {
        static void Main(string[] args)
        {
             ConsoleKeyInfo touche;
             do
             {
                 //Console.Clear();
                 Console.WriteLine("------- MENU -------");
                 Console.WriteLine(" 1 - Calcul Moyenne");
                 Console.WriteLine(" 2 - Gestion Tablo ");
                 Console.WriteLine(" 3 - Permutation   ");
                 Console.WriteLine(" 4 - Classes   ");
                 Console.WriteLine(" 5 - Exo Personne   ");
                 Console.WriteLine(" 6 - Surcharge Operateur   ");
                 Console.WriteLine(" 7 - Personnel   ");
                 Console.WriteLine(" Q : Fin           ");
                 touche = Console.ReadKey();

                 switch (touche.Key)
                 {
                     case ConsoleKey.NumPad1:
                         CalculMoyenne();
                         break;
                     case ConsoleKey.NumPad2:
                        GestionTablo2();
                        Console.Read();
                         break;
                     case ConsoleKey.NumPad3:
                         Fonctions();
                         Console.Read();
                         break;

                     case ConsoleKey.NumPad4:
                         AppelClasse();
                         Console.Read();
                         break;
                     case ConsoleKey.NumPad5:
                         ExoPersonne();
                         Console.Read();
                         break;
                     case ConsoleKey.NumPad6:
                         CalculMinutes();
                         Console.Read();
                         break;
                     case ConsoleKey.NumPad7:
                         GestionPersonnel();
                         Console.Read();
                         break;
                     case ConsoleKey.Q:
                         //Environment.Exit(0);
                         break;

                     default:
                         Console.WriteLine("Touche Invalide !");
                         Console.Read();   
                         break;
                 }

             } while (touche.Key != ConsoleKey.Q);
           Console.Read(); 
            
        }

        private static void GestionPersonnel()
        {
            Personnel staff = new Personnel(3);
            
            staff.AjouterPers(new Employe("D","E", 40,15000));
            staff.AjouterPers( new Eleve("F", "g", 12, 11));
            staff.AjouterPers(new ChargeTD("J", "k", 22, 18, 45.5f, 2));
            staff.AjouterPers(new Eleve("B", "t", 12, 15));

            staff.Lister();

            Console.WriteLine(staff[2].Affiche());
        }

        private static void CalculMinutes()
        {
            Heure h1 = new Heure(8, 5);
            Heure h2 = new Heure(10, 45);

            int nbMin = h1 - h2;
            Console.WriteLine("Ecart : " + nbMin);

            h1++;

            Console.WriteLine(h1);

            
        }

        private static void ExoPersonne()
        {
            Personne p = new Personne("Toto", "axa");
            Console.WriteLine(p.Affiche());
            p.societe = "";
            Console.WriteLine(p.Affiche());
            p.societe = "axa";
            p.societe = p.societe.PadLeft(33
                ,'*');
            Console.WriteLine(p.Affiche());
        

            using (Personne pers = new Personne("toto", "Allianz"))
            {
                Console.WriteLine(pers.Affiche());
            }

    p.QuitteSociete();
            Console.WriteLine(p.Affiche());


        }

        private static void AppelClasse()
        {
            /*
             * Person p = new Person(vPren:"fraNK", nom:"duPONt",  vAge:20);
            Console.WriteLine(p.Affiche());
            Person.Majorite = 21; 
            Console.WriteLine(p.Affiche());

            Console.WriteLine(p);

            Employe e = new Employe("Clinton", "Bill", 45, 450000);
            Console.WriteLine( e.Affiche());

            Eleve eleve = new Eleve("Gozlan", "Olivier", 12, 15);
            Console.WriteLine(eleve.Affiche());
            */

            Person[] tPers =
                    {
                        new Eleve("A", "b", 20, 12),
                        new Eleve("B", "t", 12, 15),
                        new Employe("D","E", 40,15000),
                        new Eleve("F", "g", 12, 11),
                        new ChargeTD("J", "k", 22, 18, 45.5f, 2)
                    };

            foreach (Person p in tPers)
            {
                Console.WriteLine(p.Affiche());
                Console.WriteLine("Type Objet Instancé " +p);
                if (p is Eleve)
                {
                    ((Eleve)p).TResultat(20);
                }

                if (p is Employe)
                {
                    Console.WriteLine(((Employe)p).SalaireAnnuel);
                }

                if (p is ICalcul)
                {
                    Console.WriteLine("\n----- Salaire Brut : \n" + ((ICalcul)p).CalculBrut());
                }

                p.Message();
            }
            Console.WriteLine("-------------------");

            Employe e = new Employe("Clinton", "Bill", 45, 450000);
            e.Message();

            Eleve eleve = new Eleve("Gozlan", "Olivier", 12, 15);
            eleve.Message();

            
            Employe e1 = new Employe("Georges", "Bush", 45, 450000);
            Employe e2 = new Employe("Georges", "Bush", 45, 450000);

            Console.WriteLine(e1.Equals(e2));
            Console.WriteLine(e1!=e2);

        }


        #region Fonctions

        private static void Fonctions()
        {
            int a = 10, b = 20;
            Permute(ref b, ref a);
            Console.WriteLine(a + " " + b);

            Permute(y: ref b, x: ref a);
            fctArgVariables("toto", "titi", "tata");
            Console.WriteLine("---------------");
            fctArgVariables("toto", 2, "titi", "tata", 10, "coco", "rico");


        }
        private static void fctArgVariables(params object[] chaine)
        {
            foreach (object item in chaine)
            {
                Console.WriteLine(item.GetType().Name);
                if (item is int)
                {
                    Console.WriteLine(item + " est un entier");
                }
            }
        }
        private static void Permute(ref int y, ref int x)
        {
            int tampon = 0;
            tampon = x;
            x = y;
            y = tampon;
            Console.WriteLine("Dans fonction : " + x + " " + y);
        } 
        #endregion

        #region Tablo
        private static void GestionTablo2()
        {
            //int[,] tab2dim = new int[2, 3];
            int[,] tab2dim = { { 10, 20, 30 }, { 50, 80, 110 } };

            for (int i = 0; i < tab2dim.GetLength(0); i++)
            {
                for (int j = 0; j < tab2dim.GetLength(1); j++)
                {
                    Console.WriteLine(tab2dim[i, j]);
                }
                {

                }

                int[][] tabD = new int[2][];
                tabD[0] = new int[] { 20, 10, 30 };
                tabD[1] = new int[] { 5, 7 };

                Console.WriteLine(tabD[0][1]);


            }


        }

        private static void GestionTablo()
        {
            int[] tab = new int[3];
            tab[0] = 102;
            tab[1] = 104;
            tab[2] = 106;

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }



            int[] tab2 = new int[4];
            tab.CopyTo(tab2, 0);
            Console.WriteLine("-------------------");
            foreach (int item in tab2)
            {
                Console.WriteLine(item);
            }

            int[] tab3 = new int[3];


            tab3 = tab;

            foreach (int item in tab3)
            {
                Console.WriteLine(item);
            }

            tab3 = (int[])tab.Clone();
            tab[0] = 200;
            foreach (int item in tab3)
            {
                Console.WriteLine(item);
            }
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }
        }

        
        #endregion
        
        #region "Instructuions de Base"


        private static void CalculMoyenne()
        {
            Console.WriteLine("Nombre de Notes ?");
            byte nb = 0;


            string sNb = Console.ReadLine();
            if (Byte.TryParse(sNb, out nb) == false)
            {
                Console.WriteLine("Nombre Invalide");

            }

            int cpt = 1;
            double som = 0;
            float min, max, moy;

            max = moy = 0;
            min = 20;

            while (cpt <= nb)
            {
                Console.WriteLine("Saisir une Note (0-20)");
                float note = Single.Parse(Console.ReadLine());

                if (!((note >= 0) && (note <= 20)))
                {
                    Console.WriteLine("Note Invalide");
                    continue;
                }
                string msg = Resultat(note);
                Console.WriteLine(msg);
                som += note;
                cpt++;

                if (note > max)
                    max = note;
                if (note < min)
                    min = note;

            }
            moy = (float)(som / nb);
            String aff = String.Format("Moyenne {0:#00.000} Max : {1} Min : {2}",
                moy, max, min);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(aff);
            Console.Read();
        }

        private static string Resultat(float note)
        {
            if (note < 8)
                return "Non Admis";
            else
                if (note < 10)
                    return "Rattrapage";
                else
                    return "Admis";
        } 
        
        #endregion


    }
}
