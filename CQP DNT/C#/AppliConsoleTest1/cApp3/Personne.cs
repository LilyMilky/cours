﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Personne: IDisposable
    {
        private string _nom;

        public string nom
        {
            get { return _nom; }
            private set  { _nom = value; }
        }

        private string _societe = pasDeSociete.ToString();

        public string societe
        {
            get { return _societe; }
            set { _societe = ValideSociete(value); }
        }

        public static char pasDeSociete = '?';

        public Personne(string nom)
        {
            this._nom = nom;
                       
        }

        public Personne(string nom, string societe):this(nom)
        {
            //this.nom = nom;
            
            this.societe = societe;
        }

        //Destructeur
         ~Personne()
        {
            Console.WriteLine("Obje Detruit");
        }

        private string ValideSociete(string value)
        {
            if (String.IsNullOrEmpty(value) || value.Length > 32)
                return "Erreur !!";
            else
                return value.ToUpper();

        }

        public bool EstSalarie()
        {
            return societe != pasDeSociete.ToString();
        }

        public void QuitteSociete()
        {
            societe = pasDeSociete.ToString();
        }

        public string Affiche()
        {
            string ch = this.nom + Environment.NewLine;

            ch+= (!this.EstSalarie())?"Travailleur Indep ou Inactif":societe;

            return ch;

        }

        public void Dispose()
        {
            Console.WriteLine("Suppresion de l'objet");
        }

    }
}
