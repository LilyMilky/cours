﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            float SommeNote = 0;
            float moyenne = 0;
            float max = 0;
            float min = 20;
            float note;

            Console.Write("Saisir le nombre de note à saisir : ");
            string Nombrenote = Console.ReadLine();
            byte nb = 0;

            if (!Byte.TryParse(Nombrenote, out nb))
            {
                Console.WriteLine("Nombre Invalide");
                Environment.Exit(0);
            }

            for (int i = 0; i < nb; i++)
            {
                Console.Write("Saisir une note comprise entre 0 et 20: ");

                note = Convert.ToSingle(Console.ReadLine());

                while (note < 0 || note > 20)
                {
                    Console.Write("Note invalide, veuillez saisir une note comprise entre 0 et 20: ");
                    note = Convert.ToSingle(Console.ReadLine());
                }

                Console.WriteLine(Recu(note));
                SommeNote = SommeNote + note;

                if (note > max)
                    max = note;

                else if (note < min)
                    min = note;

            }

            moyenne = SommeNote / nb;

            

            Console.WriteLine("La note moyenne est: " + moyenne);
            Console.WriteLine("La note maxi est: " + max);
            Console.WriteLine("La note mini est: " + min);
        }

        static string Recu(float note)
        {
            if (note < 8)
                return "Recale\n";
            else 
                if (note < 10)
                    return "Rattrapage\n";
                else
                    return "Admis\n";
        }
    }
}
