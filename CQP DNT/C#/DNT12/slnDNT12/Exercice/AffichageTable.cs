﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Common;

namespace Exercice
{
    public partial class AffichageTable : Form
    {
        SqlCommand accCmd;
        SqlConnection accCnx;

        public AffichageTable()
        {
            InitializeComponent();
            
        }

        private void seConnecterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Connexion window = new Connexion();
            if (window.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string Datasource = window.GetDataSource();

                string requeteSQL = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE Table_Type='BASE TABLE'";

                accCnx = new SqlConnection(@"Data Source=" + Datasource + ";Initial Catalog=dbContactsManager;Integrated Security=True");

                accCmd = new SqlCommand();
                accCmd.Connection = accCnx;
                accCmd.CommandType = CommandType.Text;
                accCmd.CommandText = requeteSQL;

                try
                {
                    accCnx.Open();

                    DbDataReader rdr = accCmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        TreeViewTable.Nodes.Add(rdr ["TABLE_NAME"].ToString());
                    }
                }
                catch
                {
                    throw new Exception("ErreurConnectionAff");
                }
                finally
                {
                    accCnx.Close();
                }
            }
        }

        private void TreeViewTable_AfterSelect(object sender, TreeViewEventArgs e)
        {
            accCmd.CommandText = "Select * FROM " + TreeViewTable.SelectedNode.ToString().Substring(10);

            try
            {
                accCnx.Open();

                DbDataReader rdr = accCmd.ExecuteReader();

                DataTable DT = new DataTable();
                DT.Load(rdr);

                DataView DV = DT.AsDataView();
                dataGridView1.DataSource = DV;

                    //dataGridView1.Rows.Add(rdr["Id_Contact"].ToString(), rdr["Nom_Contact"].ToString(), rdr["Prenom_Contact"].ToString(), rdr["Nele_Contact"].ToString(), rdr["Mail_Contact"].ToString(), rdr["Tel_Contact"].ToString());
                
            }
            catch
            {
                throw new Exception("Erreur Connection Node");
            }
            finally
            {
                accCnx.Close();
            }
        }
    }
}
