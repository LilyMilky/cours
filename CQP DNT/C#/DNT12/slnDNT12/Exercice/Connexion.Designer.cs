﻿namespace Exercice
{
    partial class Connexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxConnexion = new System.Windows.Forms.ComboBox();
            this.ButtonRaffraichir = new System.Windows.Forms.Button();
            this.Base = new System.Windows.Forms.Label();
            this.ComboBoxBase = new System.Windows.Forms.ComboBox();
            this.BoutonAnnuler = new System.Windows.Forms.Button();
            this.BoutonOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sql Server";
            // 
            // ComboBoxConnexion
            // 
            this.ComboBoxConnexion.FormattingEnabled = true;
            this.ComboBoxConnexion.Location = new System.Drawing.Point(15, 35);
            this.ComboBoxConnexion.Name = "ComboBoxConnexion";
            this.ComboBoxConnexion.Size = new System.Drawing.Size(269, 21);
            this.ComboBoxConnexion.TabIndex = 1;
            this.ComboBoxConnexion.SelectedIndexChanged += new System.EventHandler(this.ComboBoxConnexion_SelectedIndexChanged);
            // 
            // ButtonRaffraichir
            // 
            this.ButtonRaffraichir.Location = new System.Drawing.Point(302, 33);
            this.ButtonRaffraichir.Name = "ButtonRaffraichir";
            this.ButtonRaffraichir.Size = new System.Drawing.Size(75, 23);
            this.ButtonRaffraichir.TabIndex = 2;
            this.ButtonRaffraichir.Text = "Rafraichir";
            this.ButtonRaffraichir.UseVisualStyleBackColor = true;
            this.ButtonRaffraichir.Click += new System.EventHandler(this.ButtonRaffraichir_Click);
            // 
            // Base
            // 
            this.Base.AutoSize = true;
            this.Base.Location = new System.Drawing.Point(12, 133);
            this.Base.Name = "Base";
            this.Base.Size = new System.Drawing.Size(90, 13);
            this.Base.TabIndex = 3;
            this.Base.Text = "Base de données";
            // 
            // ComboBoxBase
            // 
            this.ComboBoxBase.FormattingEnabled = true;
            this.ComboBoxBase.Location = new System.Drawing.Point(15, 162);
            this.ComboBoxBase.Name = "ComboBoxBase";
            this.ComboBoxBase.Size = new System.Drawing.Size(269, 21);
            this.ComboBoxBase.TabIndex = 4;
            // 
            // BoutonAnnuler
            // 
            this.BoutonAnnuler.Location = new System.Drawing.Point(38, 218);
            this.BoutonAnnuler.Name = "BoutonAnnuler";
            this.BoutonAnnuler.Size = new System.Drawing.Size(75, 21);
            this.BoutonAnnuler.TabIndex = 5;
            this.BoutonAnnuler.Text = "Annuler";
            this.BoutonAnnuler.UseVisualStyleBackColor = true;
            this.BoutonAnnuler.Click += new System.EventHandler(this.BoutonAnnuler_Click);
            // 
            // BoutonOk
            // 
            this.BoutonOk.Location = new System.Drawing.Point(257, 218);
            this.BoutonOk.Name = "BoutonOk";
            this.BoutonOk.Size = new System.Drawing.Size(75, 23);
            this.BoutonOk.TabIndex = 6;
            this.BoutonOk.Text = "OK";
            this.BoutonOk.UseVisualStyleBackColor = true;
            this.BoutonOk.Click += new System.EventHandler(this.BoutonOk_Click);
            // 
            // Connexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 268);
            this.Controls.Add(this.BoutonOk);
            this.Controls.Add(this.BoutonAnnuler);
            this.Controls.Add(this.ComboBoxBase);
            this.Controls.Add(this.Base);
            this.Controls.Add(this.ButtonRaffraichir);
            this.Controls.Add(this.ComboBoxConnexion);
            this.Controls.Add(this.label1);
            this.Name = "Connexion";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxConnexion;
        private System.Windows.Forms.Button ButtonRaffraichir;
        private System.Windows.Forms.Label Base;
        private System.Windows.Forms.ComboBox ComboBoxBase;
        private System.Windows.Forms.Button BoutonAnnuler;
        private System.Windows.Forms.Button BoutonOk;
    }
}

