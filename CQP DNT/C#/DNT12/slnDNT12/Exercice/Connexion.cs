﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Common;

namespace Exercice
{
    public partial class Connexion : Form
    {
        private string DataBase;

        public string Database
        {
            get { return DataBase; }
        }

        private string DataSource;

        public string Datasource
        {
            get { return DataSource; }
        }

        public string GetDatabase()
        {
            return Database;
        }

        public string GetDataSource()
        {
            return Datasource;
        }

        public Connexion()
        {
            InitializeComponent();
        }

        private void ButtonRaffraichir_Click(object sender, EventArgs e)
        {
            ComboBoxConnexion.Items.Clear();

            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
            System.Data.DataTable table = instance.GetDataSources();

            foreach (DataRow row in table.Rows)
            {
                ComboBoxConnexion.Items.Add(row[0].ToString() + "\\" + row[1].ToString());
            }
        }

        private void ComboBoxConnexion_SelectedIndexChanged(object sender, EventArgs e)
        {
            string requeteSQL = "SELECT * FROM sys.databases";

            SqlConnection accCnx = new SqlConnection(@"Data Source=" + ComboBoxConnexion.SelectedItem + ";Initial Catalog=dbContactsManager;Integrated Security=True");

            SqlCommand accCmd = new SqlCommand();
            accCmd.Connection = accCnx;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = requeteSQL;

            try
            {
                accCnx.Open();

                DbDataReader rdr = accCmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        this.ComboBoxBase.Items.Add(rdr["name"].ToString());
                    }
                }
            }
            catch
            {
                throw new Exception("Erreur Connection");
            }
            finally
            {
                accCnx.Close();
            }
        }

        private void BoutonAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BoutonOk_Click(object sender, EventArgs e)
        {
            DataBase = this.ComboBoxBase.SelectedItem.ToString() ;
            DataSource = ComboBoxConnexion.SelectedItem.ToString();

            DialogResult = DialogResult.OK;
        }
    }
}
