﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

 /*
   <Contact Surname="Alexandre" Name="ASSOUS">
    <BirthDate>13/12/1980</BirthDate>
    <Email>steeve@f2i.fr</Email>
    <Telephone>02 02 02 02 02</Telephone>
  </Contact>
 */


namespace dllContacts
{
    [Serializable]
    public class Contact : IComparable
    {


        #region fields

        string nom, prenom;             //Champs (variables privées dans une classe)
        string email, telephone;

        #endregion
        [XmlElement(Order = 2)]
        public string Telephone
        {
            get { return telephone; }
            set {
                Regex reg = new Regex(@"(\d{2}-){4}\d{2}");
                if (reg.IsMatch(value))
                {
                    telephone = value;
                }
                else
                {
                    throw new ContactException(TypeErreur.TelErreur);              
                }
            
                }
        }
        [XmlElement(Order = 1)]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        [XmlAttribute("Surname")]
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        [XmlAttribute("Name")]
        public string Nom
        {
            get
            { 
               
                    return this.nom; 
            }
            set
            {
                if (value.Length > 20) 
                    throw new ContactException(TypeErreur.LongueurErreur);
                this.nom = value; }
        }

        //[NonSerialized]
        private DateTime datenaissance;
       [XmlIgnore]
        public DateTime Datenaissance
        {
            get { return datenaissance; }
            set { datenaissance = value; }
        }

        [XmlElement(ElementName="BirthDate", Order=0)]
       public string DateNaissanceCourte
       {
           get { return datenaissance.ToString("dd/MM/yyyy"); }
           set {  datenaissance = DateTime.ParseExact(value, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); }
       }

        public Contact()
        { }

        public Contact(string nom, string prenom, DateTime datenaissance, string telephone = "01-23-45-67-89", string email = "toto@mail.com")
        {
            this.Nom = nom;
            this.prenom = prenom;
            this.datenaissance = datenaissance;
            this.Telephone = telephone;                 //ici, on passe par le seter
            this.email = email;
        }

        #region Utilisén lors de l'appel à la methode Sort
        public int CompareTo(object obj)
        {
            if (obj is Contact)
            {
                return this.nom.CompareTo(((Contact)obj).nom);     //Ordre croissant
            }
            else
            {
                throw new ArgumentException("Les objets ne sont pas comparables");
            }
        }
        #endregion

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Nom : ");
            sb.AppendLine(this.Nom);
            sb.AppendFormat("Prenom : {0}", this.Prenom);
            sb.AppendLine();
            sb.AppendFormat("Né le : {0} Age : {1} {2}", 
                this.Datenaissance.ToShortDateString(), this.Age, Environment.NewLine);
            sb.AppendLine("Tel : " + this.Telephone);
            sb.AppendLine("Mail : " + this.Email);
            return sb.ToString();
        }

        
        public int Age { 
            get { return DateTime.Now.Year - this.datenaissance.Year; } 
        }

        public string NomComplet
        { get { return this.Nom + " " + this.Prenom; } }
    }
}
