﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjWinEF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            dbContactsManagerEntities entities;
            using (entities =
                new dbContactsManagerEntities())
            {
                Contact c = new Contact();
                c.Nom_Contact = "SARKOZY";
                c.Prenom_Contact = "Carla";
                c.Mail_Contact = "defghjk@v.fr";
                c.Tel_Contact = "02-02-02-02-02";
                c.Nele_Contact = new DateTime(1950, 2, 5);

                entities.AddToContacts(c);
                entities.SaveChanges();
                foreach (var item in entities.Contacts)
                {
                    listBox1.Items.Add(item.Id_Contact + " - " + item.Nom_Contact);
                }

                this.listBox1.Items.Add("-------------");
                var qContacts = from cont in entities.Contacts
                                where cont.Nom_Contact == "SARKOZY"
                                select cont;

                foreach (var item in qContacts)
                {
                    listBox1.Items.Add(item.Id_Contact + " - " + item.Nom_Contact);
                }


                var qEnt = from ent in entities.Entreprises
                           select ent;



                foreach (var item in qEnt)
                {
                    treeView1.Nodes.Add(item.Nom_Ent).Nodes
                        .Add(item.Adresse_Ent.Adr + " " + item.Adresse_Ent.Cp + " " + item.Adresse_Ent.Ville);


                }


                var qAbandon = from cont in entities.Stagiaires

                                select cont;

                foreach (var item in qAbandon)
                {
                    if (item is StagiaireEnAbandon)
                    listBox2.Items.Add(item.Nom_Stagiaire + " - " +((StagiaireEnAbandon)item).Raison_Dep_Stagiaire);
                }




            }

        }
    }
}
