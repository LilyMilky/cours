﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dllContacts;
using System.Data;

namespace prjExprLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            ContactCollection cc = new ContactCollection();
            var listContacts = cc.InnerListContacts;

            listContacts.Add(new Contact("BENZEMA", "Karim", new DateTime(1982, 5,5), "02-04-09-07-08", "kbenzema@real.fr"));
            //Faire une Expression LINQ qui recupere la liste des contacts dont l'age est > 25 
            var newList = listContacts.Where(cont => cont.Age >=25);
            
             newList = from c in listContacts
                          where c.Age > 25
                      select c;


            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("-- Liste des contacts > 25 ans avec la syntaxe requête (query syntax) --");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var cont in newList)
                {
                    Console.WriteLine(cont);
                } 
                newList.Wait();
	        #endregion


            //Faire une Expression LINQ qui recupere la liste des contacts dont l'age est > 25
            //Type Anonyme
            var inferedType = listContacts
                                  .Where(cont => cont.Age >= 25)
                                  .Select(cont => new { NomAge = cont.Nom.ToUpper() + " a "+ cont.Age +" ans"  });

            inferedType = from cont in listContacts
                      where cont.Age > 25
                      select new { NomAge = cont.Nom.ToUpper() + " a "+ cont.Age +" ans"  };


            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("-- Liste des contacts > 25 ans avec la syntaxe requête Inference de Type (query syntax) --");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var cont in inferedType)
                {
                    Console.WriteLine(cont.NomAge);
                } 
                newList.Wait();
	        #endregion

            //Afficher la liste des clients groupée par Age  décroissant
            //var groupedList = listContacts.OrderByDescending(c => c.Age).GroupBy(c => c.Age);
            var groupedList = from c in listContacts
                           orderby c.Age descending
                           group c by c.Age;
            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("-- la liste des clients âgé  groupée par Age  décroissant --");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var group in groupedList)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Age : " + group.Key);
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    foreach (var cont in group)
                    {
                        Console.WriteLine(cont);
                    }

                } 
                newList.Wait();
	        #endregion
            
            //Afficher la liste des clients âgé de plus de 30 ans groupée par Age
            var groupedList30 = listContacts.Where(c=> c.Age > 30).GroupBy(c => c.Age);
            groupedList30 = from c in listContacts
                            where c.Age > 30
                            orderby c.Age ascending
                            group c by c.Age;
            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
               Console.WriteLine("-- la liste des clients âgé de plus de 30 ans groupée par Age --");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var group in groupedList30)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Age : " + group.Key + " Nb :" + group.Count());
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    foreach (var cont in group)
                    {
                        Console.WriteLine(cont);
                    }

                } 
                newList.Wait();
	        #endregion


            //Afficher la liste des la liste des noms de contacts sans doublons
            //var listDistinct = listContacts.Select(c => c.Nom).Distinct();
                var listDistinct = (from c in listContacts
                                    select c).TakeWhile(c => c.Nom.ToUpper() != "ASSOUS");


            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
               Console.WriteLine("-- la liste des noms de contacts sans doublons --");
                Console.ForegroundColor = ConsoleColor.White;

                foreach (var cont in listDistinct)
                {
                    Console.WriteLine(cont);
                }


                newList.Wait();
	        #endregion

            //Afficher le nombre de contacts  groupée par Age
            var groupedList2 = listContacts.GroupBy(c => c.Age)
                              .Select(gr => new {AGE = gr.Key, NB = gr.Count()});


            #region Affichage du resultat
		       Console.ForegroundColor = ConsoleColor.Green;
               Console.WriteLine("-- la liste des clients âgé de plus de 30 ans groupée par Age --");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var group in groupedList2)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Age : " + group.AGE + " Nb :" + group.NB);                
                } 
                newList.Wait();
	            #endregion

            ////LINQ TO DATASET
            dsManagers ds = new dsManagers();

            dsManagers.tblEmployesDataTable dtEmp = null;
            dsManagers.tblManagersDataTable dtMgr = null;

            dsManagersTableAdapters.tblEmployesTableAdapter te = new dsManagersTableAdapters.tblEmployesTableAdapter();
            te.FillEmployes(ds.tblEmployes);

            dsManagersTableAdapters.tblManagersTableAdapter tm = new dsManagersTableAdapters.tblManagersTableAdapter();
            tm.FillManagers(ds.tblManagers);

            dtEmp = ds.tblEmployes;
            dtMgr = ds.tblManagers;

            var joined = dtMgr.AsEnumerable().Join(dtEmp.AsEnumerable(),
                            mgr => mgr.Field<int?>("Id_Emp"), emp => emp.Field<int?>("Mgr_Emp"),
                            (mgr, emp) =>
                                new
                                {
                                    Manager = mgr.Nom_Emp,
                                    Employe = emp.Nom_Emp
                                });

            //var joined = from mgr in dtMgr
            //             join emp in dtEmp on mgr.Id_Emp equals emp.Mgr_Emp
            //             select new {MGR = mgr, EMP = emp};

            #region Affichage du resultat
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- la liste des clients âgé de plus de 30 ans groupée par Age --");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var item in joined)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                //Console.WriteLine("Manager : " + item.MGR);
                //Console.WriteLine("\tEmploye : " + item.EMP);
                Console.WriteLine("Manager : " + item.Manager);
                Console.WriteLine("\tEmploye : " + item.Employe);
            }
            joined.Wait();
            #endregion


            DataTable dtEmpNT = null;
            DataTable dtMgrNT = null;

            dtEmpNT = ds.tblEmployes;
            dtMgrNT = ds.tblManagers;

            var joined2 = dtMgrNT.AsEnumerable().Join(dtEmpNT.AsEnumerable(),
                            mgr => mgr.Field<int?>("Id_Emp"), emp => emp.Field<int?>("Mgr_Emp"),
                            (mgr, emp) =>
                                new
                                {
                                    Manager = mgr.Field<string>("Nom_Emp") + " " + mgr.Field<string>("Prenom_Emp"),
                                    Employe = emp.Field<string>("Nom_Emp") + " " + emp.Field<string>("Prenom_Emp")
                                });

            #region Affichage du resultat
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- la liste des clients âgé de plus de 30 ans groupée par Age --");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var item in joined2)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Manager : " + item.Manager);
                Console.WriteLine("\tEmploye : " + item.Employe);
            }
            newList.Wait();
            #endregion


            //Caster une liste de Contacts en Tableau de Personne
            var lstPers = listContacts
                            .Cast<Contact>()
                            .Select(c => new Personne { NomPers= c.Nom, PrenPers = c.Prenom  })
                            .ToArray<Personne>(); 

            
            #region Affichage du résultat
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("--- Caster une liste de Contacts en Tableau de Personne ---");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < lstPers.Length; i++)
            {
                Console.WriteLine(lstPers[i].NomPers + " " + lstPers[i].PrenPers);
            }
            lstPers.Wait();
            #endregion


            dsManagers.tblContactsDataTable dtContacts = null;
            dsManagersTableAdapters.tblContactsTableAdapter tC = new dsManagersTableAdapters.tblContactsTableAdapter();
            tC.Fill(ds.tblContacts);

            dtContacts = ds.tblContacts;


            var listCommune =
                dtContacts.AsEnumerable().Intersect(ds.tblEmployes.AsEnumerable(), new RowComparer());

            #region Affichage du résultat
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("--- Intersect entre 2 DataTables ---");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var item in listCommune)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Id : " + item[0]);
                Console.WriteLine("\nNom : " + item[2]);
            }
            listCommune.Wait();
            #endregion

        }


    }

    public static class ClasseExtension
    {
        public static void Wait(this Object o)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Appuyez sur une touche pour continuer");
            Console.ReadKey();
        }

    }

    public class RowComparer : IEqualityComparer<DataRow>
    {

        public bool Equals(DataRow x, DataRow y)
        {
            int cleX = x.Field<int>(0);
            int cleY = y.Field<int>(0);

            return cleX == cleY;
        }

        public int GetHashCode(DataRow obj)
        {
            return 1;
        }
    }


}
