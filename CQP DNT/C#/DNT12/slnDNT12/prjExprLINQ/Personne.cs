﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prjExprLinq
{
    class Personne
    {
        private string _nomPers;

        public string NomPers
        {
            get { return _nomPers; }
            set { _nomPers = value; }
        }


        private string _prenPers;

        public string PrenPers
        {
            get { return _prenPers; }
            set { _prenPers = value; }
        }
        
    }
}
