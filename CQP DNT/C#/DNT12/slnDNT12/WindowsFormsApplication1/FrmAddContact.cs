﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class FrmAddContact : Form
    {
        public FrmAddContact()
        {
            InitializeComponent();
        }

        private dllContacts.Contact newContact;

        public dllContacts.Contact NewContact
        {
            get { return newContact; }
        }
        

        private void btnSave_Click(object sender, EventArgs e)
        {
            newContact = new dllContacts.Contact
            {
                Nom = txtNom.Text,
                Prenom = txtPren.Text,
                Datenaissance = dtpDate.Value,
                Telephone = mskTel.Text,
                Email = txtMail.Text
            };

            DialogResult = DialogResult.OK;

        }

        private void FrmAddContact_Click(object sender, EventArgs e)
        {
            this.Text = "Ajout Contact";
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            pnlAjout.Visible = !checkBox1.Checked;
        }

        private void ControlValidating(object sender, CancelEventArgs e)
        {
            ValidatingControl((Control)sender);
        }
        static int error = 0;
        private void ValidatingControl(Control ctl)
        {
            
            string[] options = ctl.Tag.ToString().Split(new char[]{';', '#'});
            if (ctl is TextBox)
            {
                TextBox tb = ctl as TextBox;
                Regex reg = new Regex(options[0]);
                if (!reg.IsMatch(tb.Text))
                {
                    errorProvider1.SetError(tb, options[1]);
                }
                else
                {
                    errorProvider1.SetError(tb, "");
                    error++;
                }
            }
            
            if (ctl is MaskedTextBox)
            {
                MaskedTextBox msk = ctl as MaskedTextBox;
                Regex reg = new Regex(options[0]);
                if (!reg.IsMatch(msk.Text))
                {
                    errorProvider1.SetError(msk, options[1]);
                }
                else
                {
                    errorProvider1.SetError(msk, "");
                    error++;
                }
            }
            if (error == 4)
                this.btnSave.Enabled = true;
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {

        }

        

    }
}
