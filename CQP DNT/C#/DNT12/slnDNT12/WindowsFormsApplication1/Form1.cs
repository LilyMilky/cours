﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dllContacts;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lstContact = new ContactCollection();
            BarreEtat.Text = "Prêt";
            this.Text = Properties.Resources.Slogan;


            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
        }






        private void triToolStripMenuItem_Click(object sender, EventArgs e)
        {
             //Tri
            dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dataGridView1.Sort(dataGridView1.Columns[1], ListSortDirection.Ascending);
        }

        ContactCollection lstContact;

        private void chargerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ContactCollection lstContacts = new ContactCollection();

            //var liste = (from c in lstContacts
            //             orderby c.Age ascending
            //             select new { NomComplet = c.Nom + " " + c.Prenom, Age = c.Age }).ToList();

            ////Définir les Colonnes à ajouter
            //dataGridView1.Columns.Add("colInfos", "Contact");
            //dataGridView1.Columns.Add("colAge", "âge");

            ////Lier les données issues de LINQ
            //dataGridView1.Columns["colInfos"].DataPropertyName = "NomComplet";
            //dataGridView1.Columns["colAge"].DataPropertyName = "Age";




            //dataGridView1.AutoGenerateColumns = false;

            //dataGridView1.DataSource = liste;

            DialogResult res = OFDELoadContact.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fileName = OFDELoadContact.FileName;
                lstContact.LoadContact(fileName);
                RefreshListBox();
                toolStripStatusLabel1.Text = "Liste Chargée";
            } 

        }

        private void RefreshListBox()
        {
            lbContacts.DataSource = null;
            lbContacts.DataSource = lstContact;
            lbContacts.DisplayMember = "NomComplet";
        }

        private void sauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SFDSauvegarderContact.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            DialogResult res = SFDSauvegarderContact.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fileName = SFDSauvegarderContact.FileName;
                lstContact.SaveContact(fileName);
                RefreshListBox();
                BarreEtat.Text = "Liste Sauvegardée";
            }
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SFDSauvegarderContact_FileOk(object sender, CancelEventArgs e)
        {
           
        }

        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAddContact window = new FrmAddContact();
            lstContact.Added += new EventHandler(lstContact_Added);
            if (window.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lstContact.Add(window.NewContact);
                RefreshListBox();
            }

        }

        void lstContact_Added(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Elément ajouté";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DateTime.Now.ToLongTimeString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DateTime.Now.ToLongTimeString();
        }

        private void lbContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            Contact c = lbContacts.SelectedItem as Contact;
            if (c != null)
            LoadInfo(c);

        }

        private void LoadInfo(Contact c)
        {
            label1.Text = c.Nom;
            label2.Text = c.Prenom;
            label3.Text = c.Datenaissance.ToShortDateString();
            label4.Text = c.Telephone;
            linkLabel1.Text = c.Email;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Contact c = lbContacts.SelectedItem as Contact;
            c.Nom = c.Nom + "aaaaaaa";
            RefreshListBox();
            sauvegarderToolStripMenuItem_Click(this, e);
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            if (pnlConsult.Visible==true)
            {
                pnlConsult.Visible = false;
                pnlModif.BringToFront();
                pnlModif.Visible = true;
                btnModifier.Text = "Valider";
                LoadModif(lbContacts.SelectedItem as Contact);
            }
            else
            {   pnlConsult.Visible = true;
                pnlModif.Visible = false;
                btnModifier.Text = "Modifier";
                
                SaveModif(lbContacts.SelectedItem as Contact);     
                LoadInfo(lbContacts.SelectedItem as Contact);
                RefreshListBox();
                   
            }

        }

        private void LoadModif(Contact c)
        {
            textBox1.Text = c.Nom;
            textBox2.Text = c.Prenom;
            dateTimePicker1.Value = c.Datenaissance;
            maskedTextBox1.Text = c.Telephone;
            textBox3.Text = c.Email;
        }

        private void SaveModif(Contact contact)
        {
            contact.Nom = textBox1.Text;
            contact.Prenom = textBox2.Text;
            contact.Datenaissance = dateTimePicker1.Value;
            contact.Telephone = maskedTextBox1.Text;
            contact.Email = textBox3.Text;
        }
    }
}
