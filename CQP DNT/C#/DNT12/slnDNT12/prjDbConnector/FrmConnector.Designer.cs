﻿namespace prjDbConnector
{
    partial class FrmConnector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboInstances = new System.Windows.Forms.ComboBox();
            this.cboBases = new System.Windows.Forms.ComboBox();
            this.btnRefreshInstances = new System.Windows.Forms.Button();
            this.btnRefreshBases = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboInstances
            // 
            this.cboInstances.FormattingEnabled = true;
            this.cboInstances.Location = new System.Drawing.Point(40, 46);
            this.cboInstances.Name = "cboInstances";
            this.cboInstances.Size = new System.Drawing.Size(258, 21);
            this.cboInstances.TabIndex = 0;
            // 
            // cboBases
            // 
            this.cboBases.FormattingEnabled = true;
            this.cboBases.Location = new System.Drawing.Point(40, 281);
            this.cboBases.Name = "cboBases";
            this.cboBases.Size = new System.Drawing.Size(258, 21);
            this.cboBases.TabIndex = 1;
            this.cboBases.DropDown += new System.EventHandler(this.cboBases_DropDown);
            // 
            // btnRefreshInstances
            // 
            this.btnRefreshInstances.Location = new System.Drawing.Point(319, 46);
            this.btnRefreshInstances.Name = "btnRefreshInstances";
            this.btnRefreshInstances.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshInstances.TabIndex = 2;
            this.btnRefreshInstances.Text = "button1";
            this.btnRefreshInstances.UseVisualStyleBackColor = true;
            this.btnRefreshInstances.Click += new System.EventHandler(this.btnRefreshInstances_Click);
            // 
            // btnRefreshBases
            // 
            this.btnRefreshBases.Location = new System.Drawing.Point(319, 281);
            this.btnRefreshBases.Name = "btnRefreshBases";
            this.btnRefreshBases.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshBases.TabIndex = 3;
            this.btnRefreshBases.Text = "button2";
            this.btnRefreshBases.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(429, 426);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "button3";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(40, 425);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuler.TabIndex = 5;
            this.btnAnnuler.Text = "button4";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // FrmConnector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 472);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnRefreshBases);
            this.Controls.Add(this.btnRefreshInstances);
            this.Controls.Add(this.cboBases);
            this.Controls.Add(this.cboInstances);
            this.Name = "FrmConnector";
            this.Text = "FrmConnector";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboInstances;
        private System.Windows.Forms.ComboBox cboBases;
        private System.Windows.Forms.Button btnRefreshInstances;
        private System.Windows.Forms.Button btnRefreshBases;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnAnnuler;
    }
}