﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace prjDbConnector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string connectionstring;

        private void connexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
           FrmConnector cnxDlg =  new FrmConnector();

           if (cnxDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
           {
               connectionstring = cnxDlg.ConnectionString;

               SqlConnection cnx = new SqlConnection(connectionstring);
               cnx.Open();
               SqlCommand cmd = new SqlCommand("select name from sys.tables", cnx);
               SqlDataReader  rdr = cmd.ExecuteReader();
               while (rdr.Read())
               {
                   treeView1.Nodes["nRacine"].Nodes.Add(new TreeNode(rdr.GetString(0)));
               }

               cnx.Close();
           }

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            SqlConnection cnx = new SqlConnection(connectionstring);
            cnx.Open();
            SqlCommand cmd = new SqlCommand("select * from " +  e.Node.Text, cnx);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            //Création de la TabPage(Onglet)

            TabPage onglet = new TabPage("Résultat de : "+ e.Node.Text);
            DataGridView dgv = new DataGridView();
            dgv.DataSource = dt;

            dgv.Dock = DockStyle.Fill;

            onglet.Controls.Add(dgv);

            tabControl1.TabPages.Add(onglet);




            cnx.Close();


        }


    }
}
