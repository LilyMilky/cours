﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace prjDbConnector
{
    public partial class FrmConnector : Form
    {
        public FrmConnector()
        {
            InitializeComponent();
        }

        private void btnRefreshInstances_Click(object sender, EventArgs e)
        {
            DataTable datasources = SqlDataSourceEnumerator.Instance.GetDataSources();
            cboInstances.Items.Clear();
            foreach (DataRow item in datasources.Rows)
            {
                cboInstances.Items.Add(item["servername"].ToString() + ((string.IsNullOrEmpty(item["instanceName"].ToString())) ? "\\SQLEXPRESS" : "\\" + item["instanceName"].ToString()));
            }



        }

        private void cboBases_DropDown(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = cboInstances.SelectedItem.ToString();
            csb.IntegratedSecurity = true;

            SqlConnection cnx = new SqlConnection(csb.ConnectionString);

            try
            {
                cnx.Open();

                SqlCommand cmd = new SqlCommand("select name from sys.databases", cnx);
                SqlDataReader rdr = cmd.ExecuteReader();
                cboBases.Items.Clear();
                while (rdr.Read())
                {
                    cboBases.Items.Add(rdr[0].ToString());
                }


            }
            catch (Exception)
            {

                throw;
            }

            finally {
                cnx.Close();
            }



        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = cboInstances.SelectedItem.ToString();
            csb.InitialCatalog = cboBases.SelectedItem.ToString();
            csb.IntegratedSecurity = true;

            this.connection = csb.ConnectionString;


            DialogResult = DialogResult.OK;
        }


        private string connection;

        public string ConnectionString
        {
            get { return connection; }
         
        }
        


    }
}
