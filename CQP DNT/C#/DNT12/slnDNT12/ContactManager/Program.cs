﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using dllContacts;

//Excel
using Microsoft.Office.Interop.Excel;
using System.Security.Cryptography;


namespace ContactManager
{
    class Program
    {

        static ContactCollection lstContact = new ContactCollection();

        static void Main(string[] args)
        {
            

            ConsoleKeyInfo touche;
            do
            {
                //Console.Clear();
                Console.WriteLine("------- MENU -------");
                Console.WriteLine(" 0 - Tests");
                Console.WriteLine(" 1 - Ajouter un Contact");
                Console.WriteLine(" 2 - Afficher les Contacts");
                Console.WriteLine(" 3 - Trier les Contacts");
                Console.WriteLine(" 4 - Recherche par Nom");
                Console.WriteLine(" 5 - Recherche par Age");
                Console.WriteLine(" S - Sauvegarder les Contacts");
                Console.WriteLine(" L - Charger les Contacts");
                Console.WriteLine(" Q : Fin           ");
                touche = Console.ReadKey();

                switch (touche.Key)
                {
                    case ConsoleKey.NumPad0:
                        Tests();
                        break;

                    case ConsoleKey.NumPad1:
                        addContact();

                        break;

                    case ConsoleKey.NumPad2:
                        showContact();
                        break;

                    case ConsoleKey.NumPad3:
                        TriContact();
                        break;
                    case ConsoleKey.NumPad4:
                        RecherheParNom();
                        break;
                    case ConsoleKey.NumPad5:
                        RecherheParAge();
                        break;
                    case ConsoleKey.E:
                        ExportVersExcel();
                        break;
                    case ConsoleKey.S:
                        SaveContact();
                        break;
                    case ConsoleKey.L:
                        LoadContact();
                        break;
                    case ConsoleKey.R:
                        lstContact.Reseted += new ResetEventHandler(lstContact_Reseted);
                        lstContact.Reseted += delegate { Console.WriteLine("Liste VIDEEEEEEE " + touche.KeyChar); };
                        lstContact.Reseted -= new ResetEventHandler(lstContact_Reseted);
                        lstContact.Reset();
                        break;

                    case ConsoleKey.Q:
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Touche Invalide !");
                        Console.Read();
                        break;
                }

            } while (touche.Key != ConsoleKey.Q);
            Console.Read();

        }

        private static void LoadContact()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("Chargement en cours");
            lstContact.LoadContact(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\save.dat");
            Console.WriteLine("Chargement terminé");

            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void SaveContact()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Sauvegarde en cours");
            lstContact.SaveContact(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\save.dat");
            Console.WriteLine("Sauvegarde terminée");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Sauvegarde XML en cours");
            lstContact.SaveContactXML(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\save.xml");
            Console.WriteLine("Sauvegarde XML terminée");
            Console.ForegroundColor = ConsoleColor.White;



        }

        private static void RecherheParNom()
        {

            
            showContact();
            Console.WriteLine("Saisir la ou les premieres lettres");
            string chSaisie = Console.ReadLine();

            //Syntax Requete
            var contactsParNom = from c in lstContact
                                 where c.Nom.StartsWith(chSaisie.ToUpper())
                                 orderby c.Nom ascending, c.Prenom descending
                                 select new
                                 {
                                     NomComplet = c.Nom + " " + c.Prenom,
                                     c.Age
                                 };

            //Syntax Methode
            //var contactsParNom = lstContact.Where(c => c.Nom.StartsWith("A"))
            //            .Select(c => new { NomComplet = c.Nom + " " + c.Prenom, 
            //                                    c.Age });
            try
            {
                FileStream fs = new FileStream(Environment.CurrentDirectory + @"\datas.txt", FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);

                foreach (var item in contactsParNom)
                {
                    string ch = item.NomComplet + " a " + item.Age + " ans";
                    sw.WriteLine(ch);
                    Console.WriteLine(ch);
                }
                sw.Close();
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception)
            {
                
                throw;
            }

        }
        private static void RecherheParAge()
        {
            showContact();
            //Syntax Requete
            //var contactsParNom = from c in lstContact
            //                     where c.Nom.StartsWith("A")
            //                     select new { NomComplet = c.Nom + " " + c.Prenom, 
            //                                    c.Age };

            Console.WriteLine("A partir de quel age souhaitez vous afficher les contacts ?");
            string saisie = Console.ReadLine();

            int ageSaisi = Int32.Parse(saisie);


            //Syntax Methode
            var contactsParAge = lstContact.Where(c => c.Age > ageSaisi)
                        .OrderBy(c => c.Nom) .ThenByDescending(c => c.Prenom)
                        .Select(c => new
                        {
                            NomComplet = c.Nom + " " + c.Prenom,
                            c.Age
                        });
            foreach (var item in contactsParAge)
            {
                Console.WriteLine(item.NomComplet + " a " + item.Age + " ans");
            }
            
            var moy = contactsParAge.Average(c => c.Age);

            moy = lstContact.Where(c => c.Age > ageSaisi).Average(c => c.Age);
            Console.WriteLine("Moyenne : " + moy);

            //Syntax Methode
            var maListe = lstContact.Where(c => c.Age > ageSaisi)
                        .OrderBy(c => c.Nom).ThenByDescending(c => c.Prenom)
                        .Select(c => new
                        {
                            NomComplet = c.Nom + " " + c.Prenom,
                            c.Age
                        }).ToList();


            var c3 = maListe[3] ;
            Console.WriteLine(c3);



        }
        static void lstContact_Reseted(object sender, ContactResetedEventArgs e)
        {
            Console.WriteLine("Liste Effacée ! "+ e.Count + " élément(s) supprimé(s) !");
        }

        private static void ExportVersExcel()
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            var classeur = app.Workbooks.Add();
            dynamic feuille = classeur.ActiveSheet;
            feuille.Cells[1, 1] = "Coucou";

            classeur.SaveAs(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\liste.xlsx");
            classeur.Close();
            app.Quit();

        }


        private static void TriContact()
        {
            Console.Clear();
            Console.WriteLine(" 1 - Tri par nom");
            Console.WriteLine(" 2 - Tri par Date");
            Console.WriteLine(" 3 - Tri par EMail");
            Console.WriteLine(" Q : Fin           ");
            ConsoleKeyInfo touche;

                //Console.Clear();
                touche = Console.ReadKey();

                switch (touche.Key)
                {
                    case ConsoleKey.NumPad1:
                        lstContact.sort(SortMode.Nom);
                        break;
                    case ConsoleKey.NumPad2:
                        lstContact.sort(SortMode.DateDeNaissance);
                        break;
                    case ConsoleKey.NumPad3:
                        lstContact.sort(SortMode.Email);
                        break;
                }
                showContact();
        }

        private static void showContact()
        {
            //while (lstContact.MoveNext())
            //{
            //    Console.WriteLine((((Contact)lstContact.Current)).ToString());
            //}


            Func<Contact, string> delInitiales = (c) => { return c.Nom.Substring(0, 1).ToUpper() + "." + c.Prenom.Substring(0, 1).ToUpper(); };
            foreach (var item in lstContact)
            {
                
                Contact c = (Contact)item;

                Func<string> delInitiales2 = () => { return c.Nom.Substring(0, 1).ToUpper() + "." + c.Prenom.Substring(0, 1).ToUpper(); };

                string initiales = delInitiales(c);
                Console.WriteLine(initiales);

                string initiales2 = delInitiales2();
                Console.WriteLine(initiales2);
                Console.WriteLine(item);
            }

        }


        delegate void T();
        delegate int TPara(short a);
        static void f()
        {
            Console.WriteLine("Je suis f()");
        }
        static void g()
        {
            Console.WriteLine("Je suis g()");
        }
        static int Carre(short a)
        {
            return a * a;
        }
        static int Double(short a)
        {
            return a * 2;
        }

        private static void Tests()
        {
            #region MyRegion
            
            
            //Nullable<int> valInt = null;
            //int val2;
            //if (valInt.HasValue)
            //    val2 = valInt.Value;

            //bool? trouver = null;
            //Nullable<bool> ok = null;
            //trouver = ok;

            //dynamic x;

            //x = 10;
            //Console.WriteLine(x);
           /* x = "Coucou";
            f(x);
            x = 12.0f;
            f(x);
            */ 
            //int ch = x;
            //Console.WriteLine(x);
            //x = new Contact();
            //Console.WriteLine(x.Age);
            //if (x is String)
            //{
            //    Console.WriteLine(x.ToUpper());
            //}

            /*
             Console.WriteLine(DateTime.Now.Ticks);
             DateTime dt = DateTime.Now.AddDays(3);
             Console.WriteLine(dt.Ticks);

             */
            #endregion
            
           
            #region Tests Délégués 
            //Delegué
            T monDel = new T(f);
            T monDelLambda = () => { Console.WriteLine("Je suis une fonction"); };

            monDelLambda();


            //monDel += g;

            // monDel();
            monDelLambda();

            TPara monDelPara = new TPara(Carre);
            TPara monDelPara2 = delegate(short a) { return a  * 2; };
            TPara monDelParaLambda = (n) => { return n * n; };

            int res = monDelParaLambda(20);
            Console.WriteLine(res);

            monDelPara = Double;
            res = monDelPara(20);
            Console.WriteLine(res);

            Action<int> delAction = (n) => { Console.WriteLine(n * 2); };
            delAction(2000);


            Func<long> delFunc = () => { return 100 * 120 * res; };
            long resultat = delFunc();
            Console.WriteLine(resultat); 
            #endregion

            int [] tab= {10,20,5,15,8};


            var resTab = from ent in tab where ent >= 10 orderby ent ascending select ent;

            foreach (var item in resTab)
            {
                Console.WriteLine(item);
            }



            //Initialiseur d'objet
            var c = new Contact
            {
                Nom = "toto",
                Prenom = "retyui",
                Telephone = "02-02-02-02-02",
                Email = "a@a.fr",
                Datenaissance = DateTime.Now.AddYears(-26)
            };



            Console.WriteLine(c);
        }
        static void f(int i) { Console.WriteLine("je suis int" + i); }
        static void f(float i) { Console.WriteLine("je suis float" + i); }
        static void f(string i) { Console.WriteLine("je suis string" + i); }

        private static void addContact()
        {


            try
            {
                Console.Clear();
                Console.WriteLine("Votre nom : ");
                string tmpNom = Console.ReadLine();

                Console.WriteLine("Votre prénom : ");
                string tmpPrenom = Console.ReadLine();

                string tmpDate = String.Empty;
                DateTime dateNaissance=DateTime.Now;

                do
                {
                    Console.WriteLine("Votre Date de Naissance : ");
                    tmpDate = Console.ReadLine();

                } while (!DateTime.TryParse(tmpDate, out dateNaissance) || (dateNaissance.PosterieureDateJour()   ));

                Contact c = new Contact(tmpNom, tmpPrenom, dateNaissance);

                Console.WriteLine("Votre Téléphone : ");
                string tmpTel = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(tmpTel))
                {
                    c.Telephone = tmpTel;
                }

                Console.WriteLine("Votre Mail : ");
                string tmpMail = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(tmpMail))
                {
                    c.Email = tmpMail;
                }

                lstContact.Added += new EventHandler(lstContact_Added);
                lstContact.Add(c);
                //var objAnonyme = new { NomComplet = c.Nom + " " + c.Prenom, AGE = DateTime.Now.Year - c.Datenaissance.Year };
            }
            catch (ContactException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception)
            {

                throw;
            }

            Console.WriteLine();
        }

        static void lstContact_Added(object sender, EventArgs e)
        {
            Console.WriteLine("Elément AJouté");
        }

        private static void GenereDivision(int val)
        {
            int i = 1 / val;
        }

    }
}

