﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dllContacts
{
    public class ContactException:Exception
    {
        TypeErreur err;
        public ContactException(TypeErreur type)
        {
            err = type;
        }

        public override string Message
        {
            get
            {
                string msg = "";
                switch (err)
                {
                    case TypeErreur.TelErreur:
                        msg = "Tel Invalide !!";
                        break;
                    case TypeErreur.MailErreur:
                        msg = "Mail Invalide !!";
                        break;
                    case TypeErreur.LongueurErreur:
                        msg = "Longueur Invalide !!";
                        break;
                    case TypeErreur.FormatErreur:
                        msg = "Format Invalide !!";
                        break;
                    default:
                        msg = base.Message;
                        break;
                }
                return msg;
                
            }
        }


    }
}
