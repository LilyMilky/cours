﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dllContacts
{
    public static class DateTimeExtension
    {
        public static bool AnterieureDateJour(this DateTime dt)
        {
            return dt < DateTime.Now;
        }

        public static bool PosterieureDateJour(this DateTime dt)
        {
            return dt > DateTime.Now;
        }

        public static bool Entre(this DateTime dt, DateTime dtDeb, DateTime dtFin)
        {
                return (dt > dtDeb) && (dt < dtFin);
        }
        
    }
}
