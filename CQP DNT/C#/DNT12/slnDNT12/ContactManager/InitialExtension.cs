﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dllContacts;

namespace ContactManager
{
    static class InitialExtension
    {
        public static string Initial(this string name, string prenom)
        {
            return name.Substring(0, 1) + " " + prenom.Substring(0, 1);
        }

        public static string Initial(this Contact cont)
        {
            return cont.Nom.Substring(0, 1).ToUpper() + " " + cont.Prenom.Substring(0, 1).ToUpper();
        }
    }
}
