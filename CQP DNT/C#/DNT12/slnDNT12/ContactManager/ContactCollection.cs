﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace dllContacts
{
    public class ContactResetedEventArgs : EventArgs
    {
        int count;

        public int Count { get { return count; } }

        public ContactResetedEventArgs(int count)
        {
            this.count = count;
        }
    }

    public delegate void ResetEventHandler (object sender, ContactResetedEventArgs e);

    public class ContactCollection:IList, IEnumerator, IEnumerable<Contact>, IEnumerator<Contact>
    {
        private List<Contact> InnerList;

        public ContactCollection()
        {
            InnerList = new List<Contact>();
            /*InnerList.Add(new Contact("ASSOUS", "Alexandre", DateTime.Now.AddYears(-32), "02-02-02-02-02", "steeve@f2i.fr"));
            InnerList.Add(new Contact("BOUBA", "Kar", DateTime.Now.AddYears(-25), "02-02-02-02-02", "bouba@toto.fr"));
            InnerList.Add(new Contact("GUILLON", "Alexandre", DateTime.Now.AddYears(-28), "02-02-02-02-02", "aklex@reveil.fr"));
            InnerList.Add(new Contact("AZZAR", "Eden", DateTime.Now.AddYears(-32), "02-02-02-02-02", "steeve@f2i.fr"));
            InnerList.Add(new Contact("CHEVALIER", "Clément", DateTime.Now.AddYears(-20), "02-02-02-02-02", "cchevclaier@f2i.fr"));
            InnerList.Add(new Contact("HIDEUX", "Sébastien", DateTime.Now.AddYears(-23), "02-02-02-02-02", "shideux@gmail.fr"));
            InnerList.Add(new Contact("ASSOUS", "Benoit", DateTime.Now.AddYears(-32), "02-02-02-02-02", "steeve@f2i.fr"));*/
        }

        #region Elements IList

        public event EventHandler Added;
        public void OnAdded()
        {
            if (Added != null)
            {
                Added(this, new EventArgs());
            }

            
        }


        public int Add(object value)
        {
            EnsureValidType(value);
            InnerList.Add((Contact)value);
            OnAdded();
            return InnerList.Count - 1;
        }

        private void EnsureValidType(object value)
        {
            if (!(value is Contact))
                throw new ContactException(TypeErreur.FormatErreur);
        }


        public void Clear()
        {
            InnerList.Clear();

        }

        public bool Contains(object value)
        {
            EnsureValidType(value);
            return InnerList.Contains((Contact)value);
        }

        public int IndexOf(object value)
        {
            EnsureValidType(value);
            return InnerList.IndexOf((Contact)value);
        }

        public void Insert(int index, object value)
        {
            EnsureValidType(value);
            InnerList.Insert(index, (Contact)value);
        }

        public bool IsFixedSize
        {
            get { return false; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public void Remove(object value)
        {
            EnsureValidType(value);
            InnerList.Remove((Contact)value);
        }

        public void RemoveAt(int index)
        {
            InnerList.RemoveAt(index);
        }

        public object this[int index]
        {
            get
            {
                return InnerList[index];
            }
            set
            {
                EnsureValidType(value);
                InnerList[index] = (Contact)value;
            }
        }

        public void CopyTo(Array array, int index)
        {
            for (int i = 0; i < InnerList.Count; i++)
            {
                array.SetValue(InnerList[i], i + index);
            }
        }

        public int Count
        {
            get { return InnerList.Count; }
        }

        public bool IsSynchronized
        {
            get { return true; }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator GetEnumerator()
        {
            return this;
        } 
        #endregion

        #region Elements IEnumerator
        private int idx = -1;

        public object Current
        {
            get {
                if (idx >= 0)
                    return InnerList[idx];

                else
                    return null;
                }
        }

        public bool MoveNext()
        {
            idx++;
            if (idx <= InnerList.Count - 1)
            {
                return true;
            }
            idx = -1;
            return false;

        }

        public event ResetEventHandler Reseted;

        private void OnReseted(int count)
        {
            if (Reseted != null)
                Reseted(this, new ContactResetedEventArgs(count));
        }


        public void Reset()
        {
            OnReseted(this.Count);
            idx = -1;
        } 
        #endregion

        public void sort(SortMode mode)
        {
            switch (mode)
            {
                case SortMode.Nom:
                    InnerList.Sort();
                    break;
                case SortMode.DateDeNaissance:
                    InnerList.Sort(new Comparison<Contact>(CompareByDateNaissance));
                    break;
                case SortMode.Email:
                    InnerList.Sort(new CompareByEmail());
                    break;
                default:
                    break;
            }
        }


        public static int CompareByDateNaissance(Contact a, Contact b)
        {
            return a.Datenaissance.CompareTo(b.Datenaissance);
        }

        IEnumerator<Contact> IEnumerable<Contact>.GetEnumerator()
        {
            return this;
        }

        Contact IEnumerator<Contact>.Current
        {
            get { return InnerList[idx]; }
        }

        public void Dispose()
        {
            
        }

        public void SaveContact(string nomFichier)
        {
            FileStream fs = new FileStream(nomFichier, FileMode.Create, FileAccess.Write, FileShare.None);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, InnerList);
            fs.Close();
        }

        public void LoadContact(string nomFichier)
        {
            if (File.Exists(nomFichier))
            {
                FileStream fs = new FileStream(nomFichier, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                InnerList = (List<Contact>)bf.Deserialize(fs);
                fs.Close();
            }
            else
                Console.WriteLine("Fichier Introuvable !!");
        }
    }


    public class CompareByEmail : IComparer<Contact>
    {
        public int Compare(Contact x, Contact y)
        {
            return x.Email.CompareTo(y.Email);
        }
    }


}
