﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Configuration;

namespace prjContactManagerADO
{
    class ModeDeconnecte
    {

        private static DbDataAdapter dbDA;

        public static DbDataAdapter DbDAdapter
        {
            get { return ModeDeconnecte.dbDA; }
            set { ModeDeconnecte.dbDA = value; }
        }


        public static DataSet GenerationManuelleDataTable()
        {

            DbProviderFactory dbpf = DbProviderFactories.GetFactory(
                ConfigurationManager.ConnectionStrings["dbContactsAccess"].ProviderName);

            DbConnection cnx = dbpf.CreateConnection();
            cnx.ConnectionString = ConfigurationManager.ConnectionStrings["dbContactsAccess"].ConnectionString;

             dbDA = dbpf.CreateDataAdapter();

            dbDA.SelectCommand = cnx.CreateCommand();
            //dbDA.SelectCommand.Connection = cnx;
            dbDA.SelectCommand.CommandText = "SELECT tblContacts.Id_Contact, tblContacts.Nom_Contact, tblContacts.Prenom_Contact, tblContacts.Mail_Contact, tblContacts.Nele_Contact, tblContacts.Tel_Contact FROM tblContacts";

            DataSet ds = new DataSet();
            dbDA.Fill(ds, "tableContact");
            DataColumn colClePrimaire = ds.Tables[0].Columns["Id_Contact"];
            colClePrimaire.AutoIncrement = true;
            colClePrimaire.AutoIncrementStep = 1;


            ds.Tables["tableContact"].PrimaryKey = new DataColumn[] { colClePrimaire };
            

            DbCommandBuilder dbcb = dbpf.CreateCommandBuilder();
            dbcb.DataAdapter = dbDA; 
            
            dbDA.DeleteCommand = dbcb.GetDeleteCommand();
            dbDA.InsertCommand = dbcb.GetInsertCommand();
            dbDA.UpdateCommand = dbcb.GetUpdateCommand();

            ds.Tables[0].Rows[0].Delete();

            //DataRow dr = ds.Tables[0].NewRow();
            //dr[1] = "zIDANE";
            //dr[2] = "Zinedine";
            //dr[3] = "zizou@france.fr";
            //dr[4] = "10/10/1958";
            //dr[5] = "02-02-03-01-05";
           

            //ds.Tables[0].Rows.Add(dr);

            return ds;
        }


        public static DataSet GenerationDsMutliTables()
        {

            DbProviderFactory dbpf = DbProviderFactories.GetFactory(
                ConfigurationManager.ConnectionStrings["dbContactsAccess"].ProviderName);

            DbConnection cnx = dbpf.CreateConnection();
            cnx.ConnectionString = ConfigurationManager.ConnectionStrings["dbContactsAccess"].ConnectionString;

            dbDA = dbpf.CreateDataAdapter();

            DataSet ds = new DataSet();

            dbDA.SelectCommand = cnx.CreateCommand();
            //dbDA.SelectCommand.Connection = cnx;
            dbDA.SelectCommand.CommandText = "SELECT tblEmployes.Id_Emp, tblEmployes.Nom_Emp, tblEmployes.Prenom_Emp, tblEmployes.Nele_Emp, tblEmployes.Mail_Emp, tblEmployes.Tel_Emp FROM tblEmployes";

            dbDA.Fill(ds, "tblEmp");


            dbDA.SelectCommand = cnx.CreateCommand();
            dbDA.SelectCommand.CommandText = "SELECT tblEmployes.Id_Emp, tblEmployes.Nom_Emp, tblEmployes.Prenom_Emp, tblEmployes.Nele_Emp, tblEmployes.Mail_Emp, tblEmployes.Tel_Emp, tblEmployes.Mgr_Emp FROM tblEmployes";
            dbDA.Fill(ds, "tblManager");


            //Definition des CLES PRIMAIRES de chacune des tables
            DataColumn colClePrimaire = ds.Tables[0].Columns["Id_Emp"];
            ds.Tables["tblEmp"].PrimaryKey = new DataColumn[] { colClePrimaire };

            ds.Tables["tblManager"].PrimaryKey = new DataColumn[] { ds.Tables[1].Columns["Id_Emp"] };

            DataColumn colCleEtrangere = ds.Tables[1].Columns["Mgr_Emp"];

            ForeignKeyConstraint cst = new ForeignKeyConstraint("Emp_Mgr", colClePrimaire, colCleEtrangere);
            cst.DeleteRule = Rule.SetDefault;
            cst.UpdateRule = Rule.SetDefault;

            ds.Tables[1].Constraints.Add(cst);

            DataRelation rel = new DataRelation("rel", colClePrimaire, colCleEtrangere);

            ds.Relations.Add(rel);

            return ds;
        }


    }
}
