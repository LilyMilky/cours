﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjContactManagerADO
{
    public partial class FrmBinding : Form
    {
        public FrmBinding()
        {
            InitializeComponent();
        }

        private void tblContactsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.tblContactsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dsManager);

        }

        private void FrmBinding_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dsManager.tblContacts' table. You can move, or remove it, as needed.
            this.tblContactsTableAdapter.FillContacts(this.dsManager.tblContacts);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tblContactsBindingSource.Position == 0)
                tblContactsBindingSource.MoveLast();
            else
                tblContactsBindingSource.MovePrevious();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //this.tblContactsTableAdapter.FillByPremLettre(this.dsManager.tblContacts, this.toolStripTextBox1.Text);
            //dataGridView1.DataSource = dsManager.tblContacts;   
   
            tblContactsBindingSource.Filter = "Nom_Contact LIKE '"+ toolStripTextBox1.Text +"%'";

        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void toolStripTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            //this.tblContactsTableAdapter.FillByPremLettre(this.dsManager.tblContacts, this.toolStripTextBox1.Text);
            //dataGridView1.DataSource = dsManager.tblContacts;   

            tblContactsBindingSource.Filter = "Nom_Contact LIKE '" + toolStripTextBox1.Text + "%'";

        }
    }
}
