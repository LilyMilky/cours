﻿namespace prjContactManagerADO
{
    partial class FrmAccess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bgwChargement = new System.ComponentModel.BackgroundWorker();
            this.pnlListe = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pnlDeconnecteMono = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mettreAJourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pnlListe.SuspendLayout();
            this.pnlDeconnecteMono.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // bgwChargement
            // 
            this.bgwChargement.WorkerReportsProgress = true;
            this.bgwChargement.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwChargement_DoWork);
            this.bgwChargement.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwChargement_ProgressChanged);
            this.bgwChargement.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwChargement_RunWorkerCompleted);
            // 
            // pnlListe
            // 
            this.pnlListe.Controls.Add(this.button1);
            this.pnlListe.Controls.Add(this.textBox1);
            this.pnlListe.Controls.Add(this.comboBox1);
            this.pnlListe.Controls.Add(this.dataGridView2);
            this.pnlListe.Controls.Add(this.listBox1);
            this.pnlListe.Location = new System.Drawing.Point(16, 38);
            this.pnlListe.Name = "pnlListe";
            this.pnlListe.Size = new System.Drawing.Size(448, 234);
            this.pnlListe.TabIndex = 5;
            this.pnlListe.Visible = false;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(33, 14);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(155, 134);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // pnlDeconnecteMono
            // 
            this.pnlDeconnecteMono.Controls.Add(this.dataGridView1);
            this.pnlDeconnecteMono.Location = new System.Drawing.Point(296, 38);
            this.pnlDeconnecteMono.Name = "pnlDeconnecteMono";
            this.pnlDeconnecteMono.Size = new System.Drawing.Size(271, 169);
            this.pnlDeconnecteMono.TabIndex = 6;
            this.pnlDeconnecteMono.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(271, 169);
            this.dataGridView1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mettreAJourToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(138, 26);
            // 
            // mettreAJourToolStripMenuItem
            // 
            this.mettreAJourToolStripMenuItem.Name = "mettreAJourToolStripMenuItem";
            this.mettreAJourToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.mettreAJourToolStripMenuItem.Text = "Mettre a jour";
            this.mettreAJourToolStripMenuItem.Click += new System.EventHandler(this.mettreAJourToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listBox2);
            this.panel1.Location = new System.Drawing.Point(16, 214);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(551, 169);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(20, 14);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(512, 147);
            this.listBox2.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(206, 14);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(239, 134);
            this.dataGridView2.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(33, 155);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(161, 155);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(199, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmAccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(618, 460);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlDeconnecteMono);
            this.Controls.Add(this.pnlListe);
            this.Name = "FrmAccess";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAccess_FormClosing);
            this.Load += new System.EventHandler(this.FrmAccess_Load);
            this.Controls.SetChildIndex(this.pnlListe, 0);
            this.Controls.SetChildIndex(this.pnlDeconnecteMono, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlListe.ResumeLayout(false);
            this.pnlListe.PerformLayout();
            this.pnlDeconnecteMono.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bgwChargement;
        private System.Windows.Forms.Panel pnlListe;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel pnlDeconnecteMono;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mettreAJourToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
    }
}
