﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using dllContacts;
using System.Data.OleDb;
using System.Configuration;
using System.Data.Common;


namespace prjContactManagerADO
{
    public partial class FrmAccess : prjContactManagerADO.FrmBase
    {
        DataSet ds;
        ToolStripMenuItem importToolStripMenuItem = new ToolStripMenuItem();
        ToolStripMenuItem listeToolStripMenuItem = new ToolStripMenuItem();
        ToolStripMenuItem modeDeconnecteToolStripMenuItem = new ToolStripMenuItem();
        ToolStripMenuItem modeDeconnecteMultiToolStripMenuItem = new ToolStripMenuItem();
        ToolStripProgressBar pgChargement = new ToolStripProgressBar();


        public FrmAccess()
        {
            InitializeComponent();

            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.importToolStripMenuItem.Text = "&Importer (Xml vers Access)";
            this.importToolStripMenuItem.Click += new EventHandler(importToolStripMenuItem_Click);

            

            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.listeToolStripMenuItem.Text = "&Lister";
            this.listeToolStripMenuItem.Click += new EventHandler(listeToolStripMenuItem_Click);

            this.modeDeconnecteToolStripMenuItem.Name = "modeDeconnecteToolStripMenuItem";
            this.modeDeconnecteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.modeDeconnecteToolStripMenuItem.Text = "&Lister en Deconnecte (MonoTable)";
            this.modeDeconnecteToolStripMenuItem.Click += new EventHandler(modeDeconnecteToolStripMenuItem_Click);

            this.modeDeconnecteMultiToolStripMenuItem.Name = "modeDeconnecteToolStripMenuItem";
            this.modeDeconnecteMultiToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.modeDeconnecteMultiToolStripMenuItem.Text = "&Managers Employes";
            this.modeDeconnecteMultiToolStripMenuItem.Click += new EventHandler(modeDeconnecteMultiToolStripMenuItem_Click);

            this.fichierToolStripMenuItem.DropDownItems.Insert(this.fichierToolStripMenuItem.DropDownItems.Count - 1, importToolStripMenuItem);
            this.fichierToolStripMenuItem.DropDownItems.Insert(this.fichierToolStripMenuItem.DropDownItems.Count - 1, listeToolStripMenuItem);
            this.fichierToolStripMenuItem.DropDownItems.Insert(this.fichierToolStripMenuItem.DropDownItems.Count - 1, modeDeconnecteToolStripMenuItem);
            this.fichierToolStripMenuItem.DropDownItems.Insert(this.fichierToolStripMenuItem.DropDownItems.Count - 1, modeDeconnecteMultiToolStripMenuItem);


            this.BarreEtat.Items.Insert(0, pgChargement);


        }

        void modeDeconnecteMultiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel1.Dock = DockStyle.Fill;

            DataSet ds = ModeDeconnecte.GenerationDsMutliTables();

            foreach (DataRow recMgr in ds.Tables["tblEmp"].Rows)
            {

                    listBox2.Items.Add("Manager : " + recMgr["Nom_Emp"] + " " + recMgr["Prenom_Emp"]);
                //else
                //{
                //     listBox2.Items.Add("Manager : " + recMgr["Nom_Emp"] + " " + recMgr["Prenom_Emp"]);
                //}

                foreach (DataRow recEmp in recMgr.GetChildRows("rel"))
                {
                        listBox2.Items.Add("\tEmploye : " + recEmp["Nom_Emp"] + " " + recEmp["Prenom_Emp"]);
                }
            }


        }

        void modeDeconnecteToolStripMenuItem_Click(object sender, EventArgs e)
        {
             ds = ModeDeconnecte.GenerationManuelleDataTable();
            dataGridView1.DataSource = ds.Tables[0];
            pnlDeconnecteMono.Visible = true;
            pnlDeconnecteMono.Dock = DockStyle.Fill;



        }
        DataTable dtContacts;
        void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlListe.Visible = true;
            DbProviderFactory dbpf = DbProviderFactories.GetFactory(
                ConfigurationManager.ConnectionStrings["dbContactsAccess"].ProviderName) ;

            DbConnection accCnx = dbpf.CreateConnection();
            accCnx.ConnectionString = ConfigurationManager.ConnectionStrings["dbContactsAccess"].ConnectionString;

            accCnx.StateChange += new StateChangeEventHandler(accCnx_StateChange);

            DbCommand accCmd = dbpf.CreateCommand();
            accCmd.Connection = accCnx;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = "select id_Contact, nom_Contact from tblContacts";

            accCnx.Open();

            DbDataReader rdr = accCmd.ExecuteReader();



            
            
            
            DataTable dtStructure = rdr.GetSchemaTable();
            dataGridView2.DataSource = dtStructure;
             dtContacts = new DataTable();
            dtContacts.Load(rdr);

            listBox1.DataSource = dtContacts;
            listBox1.DisplayMember = dtContacts.Columns["nom_Contact"].ColumnName;
            listBox1.ValueMember = dtContacts.Columns["Id_Contact"].ColumnName;
            comboBox1.DataSource = dtStructure;

            comboBox1.DisplayMember = dtStructure.Columns[0].ToString();
            comboBox1.ValueMember = dtStructure.Columns[0].ToString();
            /*
            while (rdr.Read())
            {
                //listBox1.Items.Add(rdr.GetString(1));

                //string nom = rdr[1].ToString();
                int? id = rdr[0] as Nullable<int>;
                int cle = 0;
                if (id.HasValue) cle = id.Value;
               
                string nom = rdr["nom_Contact"].ToString() + " " + cle;
                listBox1.Items.Add(nom);

            }
             * */


        }

        void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bgwChargement.RunWorkerAsync();
        }

        void accCnx_StateChange(object sender, StateChangeEventArgs e)
        {
            toolStripStatusLabelEtat.Text = "Avant " + e.OriginalState + " APres " + e.CurrentState;
            /*
            switch (e.CurrentState)
            {
                case ConnectionState.Broken:
                    break;
                case ConnectionState.Closed:
                    break;
                case ConnectionState.Connecting:
                    break;
                case ConnectionState.Executing:
                    break;
                case ConnectionState.Fetching:
                    break;
                case ConnectionState.Open:
                    break;
                default:
                    break;
            }*/
        }
        private void importAccess()
        {
            ContactCollection lstContact = new ContactCollection();
            lstContact.LoadContactXML(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\save.xml");

            OleDbConnection accCnx = new OleDbConnection();
            accCnx.ConnectionString = ConfigurationManager.ConnectionStrings["dbContactsAccess"].ConnectionString;

            accCnx.StateChange += new StateChangeEventHandler(accCnx_StateChange);

            OleDbCommand accCmd = new OleDbCommand();
            accCmd.Connection = accCnx;
            accCmd.CommandType = CommandType.Text;
            accCmd.CommandText = "insert into tblContacts(Nom_Contact, Prenom_Contact, Nele_Contact, Mail_Contact, Tel_Contact) VALUES(@pNom, @pPren, @pDate, @pMail, @pTel)";


            try
            {
                accCnx.Open();
                int percent = 0, i=0;

                bgwChargement.ReportProgress(percent, "Début Chargement");

                while (lstContact.MoveNext())
                {
                    var contact = (Contact)lstContact.Current;

                    accCmd.Parameters.AddWithValue("pNom", contact.Nom);
                    accCmd.Parameters.AddWithValue("pPren", contact.Prenom);
                    accCmd.Parameters.AddWithValue("pDate", contact.DateNaissanceCourte);
                    accCmd.Parameters.AddWithValue("pMail", contact.Email);
                    accCmd.Parameters.AddWithValue("pTel", contact.Telephone);

                    percent = (++i * 100) / lstContact.Count;
                    System.Threading.Thread.Sleep(300);
                    bgwChargement.ReportProgress(percent, "Chargement en Cours");
                    int nb = accCmd.ExecuteNonQuery();
                    accCmd.Parameters.Clear();

                }

                bgwChargement.ReportProgress(100, "Fin Chargement");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Erreur Connexion" + ex.InnerException);
            }
            finally
            {
                accCnx.Close();
            }

        }



        private void FrmAccess_Load(object sender, EventArgs e)
        {

        }

        public override void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Quitter ?","Fin", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            base.quitterToolStripMenuItem_Click(sender, e);
        }

        private void FrmAccess_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Quitter ?", "Fin", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                Environment.Exit(0);
            else
                e.Cancel = true;
        }

        private void bgwChargement_DoWork(object sender, DoWorkEventArgs e)
        {
            importAccess();
            if (bgwChargement.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void bgwChargement_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgChargement.Value = e.ProgressPercentage;
            toolStripStatusLabelEtat.Text = e.UserState.ToString();
        }

        private void bgwChargement_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Traitement Annulé");
            }
        }

        private void mettreAJourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DbDataAdapter da = ModeDeconnecte.DbDAdapter;

            da.Update(ds.Tables[0]);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           // MessageBox.Show(listBox1.SelectedValue.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataView dvContacts = dtContacts.AsDataView();

            dvContacts.RowFilter = this.comboBox1.Text + " = '"+ this.textBox1.Text +"'";
            listBox1.DataSource = dvContacts;
        }


        
    }
}
