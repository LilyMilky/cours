﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;

namespace prjContactManagerADO
{
    class DBConnection
    {
        public static DbConnection getConnection()
        {
            DbProviderFactory dbpf = DbProviderFactories.GetFactory(
ConfigurationManager.ConnectionStrings["prjContactManagerADO.Properties.Settings.dbSQLCnx"].ProviderName);

            DbConnection cnx = dbpf.CreateConnection();
            cnx.ConnectionString = ConfigurationManager.ConnectionStrings["prjContactManagerADO.Properties.Settings.dbSQLCnx"].ConnectionString;
            return cnx;

        }


    }
}
