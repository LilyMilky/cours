﻿namespace prjContactManagerADO
{
    partial class FrmStoredProcedure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label nom_ContactLabel;
            System.Windows.Forms.Label prenom_ContactLabel;
            System.Windows.Forms.Label mail_ContactLabel;
            System.Windows.Forms.Label nele_ContactLabel;
            System.Windows.Forms.Label tel_ContactLabel;
            this.nom_ContactTextBox = new System.Windows.Forms.TextBox();
            this.prenom_ContactTextBox = new System.Windows.Forms.TextBox();
            this.mail_ContactTextBox = new System.Windows.Forms.TextBox();
            this.nele_ContactDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tel_ContactTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            nom_ContactLabel = new System.Windows.Forms.Label();
            prenom_ContactLabel = new System.Windows.Forms.Label();
            mail_ContactLabel = new System.Windows.Forms.Label();
            nele_ContactLabel = new System.Windows.Forms.Label();
            tel_ContactLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nom_ContactLabel
            // 
            nom_ContactLabel.AutoSize = true;
            nom_ContactLabel.Location = new System.Drawing.Point(65, 43);
            nom_ContactLabel.Name = "nom_ContactLabel";
            nom_ContactLabel.Size = new System.Drawing.Size(72, 13);
            nom_ContactLabel.TabIndex = 1;
            nom_ContactLabel.Text = "Nom Contact:";
            // 
            // nom_ContactTextBox
            // 
            this.nom_ContactTextBox.Location = new System.Drawing.Point(143, 40);
            this.nom_ContactTextBox.Name = "nom_ContactTextBox";
            this.nom_ContactTextBox.Size = new System.Drawing.Size(100, 20);
            this.nom_ContactTextBox.TabIndex = 2;
            // 
            // prenom_ContactLabel
            // 
            prenom_ContactLabel.AutoSize = true;
            prenom_ContactLabel.Location = new System.Drawing.Point(51, 73);
            prenom_ContactLabel.Name = "prenom_ContactLabel";
            prenom_ContactLabel.Size = new System.Drawing.Size(86, 13);
            prenom_ContactLabel.TabIndex = 3;
            prenom_ContactLabel.Text = "Prenom Contact:";
            // 
            // prenom_ContactTextBox
            // 
            this.prenom_ContactTextBox.Location = new System.Drawing.Point(143, 66);
            this.prenom_ContactTextBox.Name = "prenom_ContactTextBox";
            this.prenom_ContactTextBox.Size = new System.Drawing.Size(100, 20);
            this.prenom_ContactTextBox.TabIndex = 4;
            // 
            // mail_ContactLabel
            // 
            mail_ContactLabel.AutoSize = true;
            mail_ContactLabel.Location = new System.Drawing.Point(68, 95);
            mail_ContactLabel.Name = "mail_ContactLabel";
            mail_ContactLabel.Size = new System.Drawing.Size(69, 13);
            mail_ContactLabel.TabIndex = 5;
            mail_ContactLabel.Text = "Mail Contact:";
            // 
            // mail_ContactTextBox
            // 
            this.mail_ContactTextBox.Location = new System.Drawing.Point(143, 92);
            this.mail_ContactTextBox.Name = "mail_ContactTextBox";
            this.mail_ContactTextBox.Size = new System.Drawing.Size(100, 20);
            this.mail_ContactTextBox.TabIndex = 6;
            // 
            // nele_ContactLabel
            // 
            nele_ContactLabel.AutoSize = true;
            nele_ContactLabel.Location = new System.Drawing.Point(65, 122);
            nele_ContactLabel.Name = "nele_ContactLabel";
            nele_ContactLabel.Size = new System.Drawing.Size(72, 13);
            nele_ContactLabel.TabIndex = 7;
            nele_ContactLabel.Text = "Nele Contact:";
            // 
            // nele_ContactDateTimePicker
            // 
            this.nele_ContactDateTimePicker.Location = new System.Drawing.Point(143, 118);
            this.nele_ContactDateTimePicker.Name = "nele_ContactDateTimePicker";
            this.nele_ContactDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.nele_ContactDateTimePicker.TabIndex = 8;
            // 
            // tel_ContactLabel
            // 
            tel_ContactLabel.AutoSize = true;
            tel_ContactLabel.Location = new System.Drawing.Point(72, 147);
            tel_ContactLabel.Name = "tel_ContactLabel";
            tel_ContactLabel.Size = new System.Drawing.Size(65, 13);
            tel_ContactLabel.TabIndex = 9;
            tel_ContactLabel.Text = "Tel Contact:";
            // 
            // tel_ContactTextBox
            // 
            this.tel_ContactTextBox.Location = new System.Drawing.Point(143, 144);
            this.tel_ContactTextBox.Name = "tel_ContactTextBox";
            this.tel_ContactTextBox.Size = new System.Drawing.Size(100, 20);
            this.tel_ContactTextBox.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(143, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Ajouter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmStoredProcedure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 290);
            this.Controls.Add(this.button1);
            this.Controls.Add(tel_ContactLabel);
            this.Controls.Add(this.tel_ContactTextBox);
            this.Controls.Add(nele_ContactLabel);
            this.Controls.Add(this.nele_ContactDateTimePicker);
            this.Controls.Add(mail_ContactLabel);
            this.Controls.Add(this.mail_ContactTextBox);
            this.Controls.Add(prenom_ContactLabel);
            this.Controls.Add(this.prenom_ContactTextBox);
            this.Controls.Add(nom_ContactLabel);
            this.Controls.Add(this.nom_ContactTextBox);
            this.Name = "FrmStoredProcedure";
            this.Text = "FrmStoredProcedure";
            this.Load += new System.EventHandler(this.FrmStoredProcedure_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nom_ContactTextBox;
        private System.Windows.Forms.TextBox prenom_ContactTextBox;
        private System.Windows.Forms.TextBox mail_ContactTextBox;
        private System.Windows.Forms.DateTimePicker nele_ContactDateTimePicker;
        private System.Windows.Forms.TextBox tel_ContactTextBox;
        private System.Windows.Forms.Button button1;

    }
}