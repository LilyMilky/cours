﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjContactManagerADO
{
    public partial class FrmBase : Form
    {
        public FrmBase()
        {
            InitializeComponent();
            timer1_Tick(this, null);
        }

        public ToolStripItemCollection Items { get; set; }



        public virtual void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
              Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DateTime.Now.ToLongTimeString();
        }

        protected void fichierToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FrmBase_Load(object sender, EventArgs e)
        {

        }

        private void FrmBase_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            
        }

        private void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
