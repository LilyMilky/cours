﻿namespace prjContactManagerADO
{
    partial class FrmBinding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label id_ContactLabel;
            System.Windows.Forms.Label nom_ContactLabel;
            System.Windows.Forms.Label prenom_ContactLabel;
            System.Windows.Forms.Label mail_ContactLabel;
            System.Windows.Forms.Label nele_ContactLabel;
            System.Windows.Forms.Label tel_ContactLabel;
            System.Windows.Forms.Label nomCompletLabel;
            System.Windows.Forms.Label hasEmailLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBinding));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dsManager = new prjContactManagerADO.dsManager();
            this.tblContactsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblContactsTableAdapter = new prjContactManagerADO.dsManagerTableAdapters.tblContactsTableAdapter();
            this.tableAdapterManager = new prjContactManagerADO.dsManagerTableAdapters.TableAdapterManager();
            this.tblContactsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tblContactsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.id_ContactTextBox = new System.Windows.Forms.TextBox();
            this.nom_ContactTextBox = new System.Windows.Forms.TextBox();
            this.prenom_ContactTextBox = new System.Windows.Forms.TextBox();
            this.mail_ContactTextBox = new System.Windows.Forms.TextBox();
            this.nele_ContactDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tel_ContactTextBox = new System.Windows.Forms.TextBox();
            this.nomCompletTextBox = new System.Windows.Forms.TextBox();
            this.hasEmailCheckBox = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nomContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.neleContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            id_ContactLabel = new System.Windows.Forms.Label();
            nom_ContactLabel = new System.Windows.Forms.Label();
            prenom_ContactLabel = new System.Windows.Forms.Label();
            mail_ContactLabel = new System.Windows.Forms.Label();
            nele_ContactLabel = new System.Windows.Forms.Label();
            tel_ContactLabel = new System.Windows.Forms.Label();
            nomCompletLabel = new System.Windows.Forms.Label();
            hasEmailLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactsBindingNavigator)).BeginInit();
            this.tblContactsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // id_ContactLabel
            // 
            id_ContactLabel.AutoSize = true;
            id_ContactLabel.Location = new System.Drawing.Point(23, 55);
            id_ContactLabel.Name = "id_ContactLabel";
            id_ContactLabel.Size = new System.Drawing.Size(59, 13);
            id_ContactLabel.TabIndex = 1;
            id_ContactLabel.Text = "Id Contact:";
            // 
            // nom_ContactLabel
            // 
            nom_ContactLabel.AutoSize = true;
            nom_ContactLabel.Location = new System.Drawing.Point(23, 81);
            nom_ContactLabel.Name = "nom_ContactLabel";
            nom_ContactLabel.Size = new System.Drawing.Size(72, 13);
            nom_ContactLabel.TabIndex = 3;
            nom_ContactLabel.Text = "Nom Contact:";
            // 
            // prenom_ContactLabel
            // 
            prenom_ContactLabel.AutoSize = true;
            prenom_ContactLabel.Location = new System.Drawing.Point(23, 107);
            prenom_ContactLabel.Name = "prenom_ContactLabel";
            prenom_ContactLabel.Size = new System.Drawing.Size(86, 13);
            prenom_ContactLabel.TabIndex = 5;
            prenom_ContactLabel.Text = "Prenom Contact:";
            // 
            // mail_ContactLabel
            // 
            mail_ContactLabel.AutoSize = true;
            mail_ContactLabel.Location = new System.Drawing.Point(23, 133);
            mail_ContactLabel.Name = "mail_ContactLabel";
            mail_ContactLabel.Size = new System.Drawing.Size(69, 13);
            mail_ContactLabel.TabIndex = 7;
            mail_ContactLabel.Text = "Mail Contact:";
            // 
            // nele_ContactLabel
            // 
            nele_ContactLabel.AutoSize = true;
            nele_ContactLabel.Location = new System.Drawing.Point(23, 160);
            nele_ContactLabel.Name = "nele_ContactLabel";
            nele_ContactLabel.Size = new System.Drawing.Size(72, 13);
            nele_ContactLabel.TabIndex = 9;
            nele_ContactLabel.Text = "Nele Contact:";
            // 
            // tel_ContactLabel
            // 
            tel_ContactLabel.AutoSize = true;
            tel_ContactLabel.Location = new System.Drawing.Point(23, 185);
            tel_ContactLabel.Name = "tel_ContactLabel";
            tel_ContactLabel.Size = new System.Drawing.Size(65, 13);
            tel_ContactLabel.TabIndex = 11;
            tel_ContactLabel.Text = "Tel Contact:";
            // 
            // nomCompletLabel
            // 
            nomCompletLabel.AutoSize = true;
            nomCompletLabel.Location = new System.Drawing.Point(23, 211);
            nomCompletLabel.Name = "nomCompletLabel";
            nomCompletLabel.Size = new System.Drawing.Size(73, 13);
            nomCompletLabel.TabIndex = 13;
            nomCompletLabel.Text = "Nom Complet:";
            // 
            // hasEmailLabel
            // 
            hasEmailLabel.AutoSize = true;
            hasEmailLabel.Location = new System.Drawing.Point(23, 239);
            hasEmailLabel.Name = "hasEmailLabel";
            hasEmailLabel.Size = new System.Drawing.Size(57, 13);
            hasEmailLabel.TabIndex = 15;
            hasEmailLabel.Text = "Has Email:";
            // 
            // dsManager
            // 
            this.dsManager.DataSetName = "dsManager";
            this.dsManager.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblContactsBindingSource
            // 
            this.tblContactsBindingSource.DataMember = "tblContacts";
            this.tblContactsBindingSource.DataSource = this.dsManager;
            // 
            // tblContactsTableAdapter
            // 
            this.tblContactsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.tblContactsTableAdapter = this.tblContactsTableAdapter;
            this.tableAdapterManager.UpdateOrder = prjContactManagerADO.dsManagerTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tblContactsBindingNavigator
            // 
            this.tblContactsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tblContactsBindingNavigator.BindingSource = this.tblContactsBindingSource;
            this.tblContactsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tblContactsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.tblContactsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tblContactsBindingNavigatorSaveItem,
            this.toolStripTextBox1,
            this.toolStripButton1});
            this.tblContactsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.tblContactsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tblContactsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tblContactsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tblContactsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tblContactsBindingNavigator.Name = "tblContactsBindingNavigator";
            this.tblContactsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tblContactsBindingNavigator.Size = new System.Drawing.Size(902, 25);
            this.tblContactsBindingNavigator.TabIndex = 0;
            this.tblContactsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tblContactsBindingNavigatorSaveItem
            // 
            this.tblContactsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tblContactsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tblContactsBindingNavigatorSaveItem.Image")));
            this.tblContactsBindingNavigatorSaveItem.Name = "tblContactsBindingNavigatorSaveItem";
            this.tblContactsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.tblContactsBindingNavigatorSaveItem.Text = "Save Data";
            this.tblContactsBindingNavigatorSaveItem.Click += new System.EventHandler(this.tblContactsBindingNavigatorSaveItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBox1_KeyUp);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(161, 22);
            this.toolStripButton1.Text = "Rechercher par Prmier Lettre";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // id_ContactTextBox
            // 
            this.id_ContactTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "Id_Contact", true));
            this.id_ContactTextBox.Location = new System.Drawing.Point(115, 52);
            this.id_ContactTextBox.Name = "id_ContactTextBox";
            this.id_ContactTextBox.Size = new System.Drawing.Size(200, 20);
            this.id_ContactTextBox.TabIndex = 2;
            // 
            // nom_ContactTextBox
            // 
            this.nom_ContactTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "Nom_Contact", true));
            this.nom_ContactTextBox.Location = new System.Drawing.Point(115, 78);
            this.nom_ContactTextBox.Name = "nom_ContactTextBox";
            this.nom_ContactTextBox.Size = new System.Drawing.Size(200, 20);
            this.nom_ContactTextBox.TabIndex = 4;
            // 
            // prenom_ContactTextBox
            // 
            this.prenom_ContactTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "Prenom_Contact", true));
            this.prenom_ContactTextBox.Location = new System.Drawing.Point(115, 104);
            this.prenom_ContactTextBox.Name = "prenom_ContactTextBox";
            this.prenom_ContactTextBox.Size = new System.Drawing.Size(200, 20);
            this.prenom_ContactTextBox.TabIndex = 6;
            // 
            // mail_ContactTextBox
            // 
            this.mail_ContactTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "Mail_Contact", true));
            this.mail_ContactTextBox.Location = new System.Drawing.Point(115, 130);
            this.mail_ContactTextBox.Name = "mail_ContactTextBox";
            this.mail_ContactTextBox.Size = new System.Drawing.Size(200, 20);
            this.mail_ContactTextBox.TabIndex = 8;
            // 
            // nele_ContactDateTimePicker
            // 
            this.nele_ContactDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblContactsBindingSource, "Nele_Contact", true));
            this.nele_ContactDateTimePicker.Location = new System.Drawing.Point(115, 156);
            this.nele_ContactDateTimePicker.Name = "nele_ContactDateTimePicker";
            this.nele_ContactDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.nele_ContactDateTimePicker.TabIndex = 10;
            // 
            // tel_ContactTextBox
            // 
            this.tel_ContactTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "Tel_Contact", true));
            this.tel_ContactTextBox.Location = new System.Drawing.Point(115, 182);
            this.tel_ContactTextBox.Name = "tel_ContactTextBox";
            this.tel_ContactTextBox.Size = new System.Drawing.Size(200, 20);
            this.tel_ContactTextBox.TabIndex = 12;
            // 
            // nomCompletTextBox
            // 
            this.nomCompletTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblContactsBindingSource, "NomComplet", true));
            this.nomCompletTextBox.Location = new System.Drawing.Point(115, 208);
            this.nomCompletTextBox.Name = "nomCompletTextBox";
            this.nomCompletTextBox.Size = new System.Drawing.Size(200, 20);
            this.nomCompletTextBox.TabIndex = 14;
            // 
            // hasEmailCheckBox
            // 
            this.hasEmailCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.tblContactsBindingSource, "HasEmail", true));
            this.hasEmailCheckBox.Location = new System.Drawing.Point(115, 234);
            this.hasEmailCheckBox.Name = "hasEmailCheckBox";
            this.hasEmailCheckBox.Size = new System.Drawing.Size(200, 24);
            this.hasEmailCheckBox.TabIndex = 16;
            this.hasEmailCheckBox.Text = "checkBox1";
            this.hasEmailCheckBox.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Red;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomContactDataGridViewTextBoxColumn,
            this.prenomContactDataGridViewTextBoxColumn,
            this.neleContactDataGridViewTextBoxColumn,
            this.telContactDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tblContactsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(346, 52);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(503, 206);
            this.dataGridView1.TabIndex = 17;
            // 
            // nomContactDataGridViewTextBoxColumn
            // 
            this.nomContactDataGridViewTextBoxColumn.DataPropertyName = "Nom_Contact";
            this.nomContactDataGridViewTextBoxColumn.HeaderText = "Nom_Contact";
            this.nomContactDataGridViewTextBoxColumn.Name = "nomContactDataGridViewTextBoxColumn";
            this.nomContactDataGridViewTextBoxColumn.Width = 97;
            // 
            // prenomContactDataGridViewTextBoxColumn
            // 
            this.prenomContactDataGridViewTextBoxColumn.DataPropertyName = "Prenom_Contact";
            this.prenomContactDataGridViewTextBoxColumn.HeaderText = "Prenom_Contact";
            this.prenomContactDataGridViewTextBoxColumn.Name = "prenomContactDataGridViewTextBoxColumn";
            this.prenomContactDataGridViewTextBoxColumn.Width = 111;
            // 
            // neleContactDataGridViewTextBoxColumn
            // 
            this.neleContactDataGridViewTextBoxColumn.DataPropertyName = "Nele_Contact";
            this.neleContactDataGridViewTextBoxColumn.HeaderText = "Nele_Contact";
            this.neleContactDataGridViewTextBoxColumn.Name = "neleContactDataGridViewTextBoxColumn";
            this.neleContactDataGridViewTextBoxColumn.Width = 97;
            // 
            // telContactDataGridViewTextBoxColumn
            // 
            this.telContactDataGridViewTextBoxColumn.DataPropertyName = "Tel_Contact";
            this.telContactDataGridViewTextBoxColumn.HeaderText = "Tel_Contact";
            this.telContactDataGridViewTextBoxColumn.Name = "telContactDataGridViewTextBoxColumn";
            this.telContactDataGridViewTextBoxColumn.Width = 90;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(421, 288);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmBinding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 369);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(id_ContactLabel);
            this.Controls.Add(this.id_ContactTextBox);
            this.Controls.Add(nom_ContactLabel);
            this.Controls.Add(this.nom_ContactTextBox);
            this.Controls.Add(prenom_ContactLabel);
            this.Controls.Add(this.prenom_ContactTextBox);
            this.Controls.Add(mail_ContactLabel);
            this.Controls.Add(this.mail_ContactTextBox);
            this.Controls.Add(nele_ContactLabel);
            this.Controls.Add(this.nele_ContactDateTimePicker);
            this.Controls.Add(tel_ContactLabel);
            this.Controls.Add(this.tel_ContactTextBox);
            this.Controls.Add(nomCompletLabel);
            this.Controls.Add(this.nomCompletTextBox);
            this.Controls.Add(hasEmailLabel);
            this.Controls.Add(this.hasEmailCheckBox);
            this.Controls.Add(this.tblContactsBindingNavigator);
            this.Name = "FrmBinding";
            this.Text = "FrmBinding";
            this.Load += new System.EventHandler(this.FrmBinding_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactsBindingNavigator)).EndInit();
            this.tblContactsBindingNavigator.ResumeLayout(false);
            this.tblContactsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private dsManager dsManager;
        private System.Windows.Forms.BindingSource tblContactsBindingSource;
        private dsManagerTableAdapters.tblContactsTableAdapter tblContactsTableAdapter;
        private dsManagerTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator tblContactsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tblContactsBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox id_ContactTextBox;
        private System.Windows.Forms.TextBox nom_ContactTextBox;
        private System.Windows.Forms.TextBox prenom_ContactTextBox;
        private System.Windows.Forms.TextBox mail_ContactTextBox;
        private System.Windows.Forms.DateTimePicker nele_ContactDateTimePicker;
        private System.Windows.Forms.TextBox tel_ContactTextBox;
        private System.Windows.Forms.TextBox nomCompletTextBox;
        private System.Windows.Forms.CheckBox hasEmailCheckBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn neleContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}