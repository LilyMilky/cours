﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Transactions;

namespace prjContactManagerADO
{
    public partial class FrmStoredProcedure : Form
    {
        public FrmStoredProcedure()
        {
            InitializeComponent();
        }

        private void tblContactsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

        }

        private void FrmStoredProcedure_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dsManager.tblContacts' table. You can move, or remove it, as needed.
           

        }

        private void button1_Click(object sender, EventArgs e)
        {


            SqlConnection cnx = (SqlConnection)DBConnection.getConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "spAjoutContact";
            cmd.Connection = cnx;

            cmd.Parameters.AddWithValue("@nom", this.nom_ContactTextBox.Text);

            SqlParameter pPren = new SqlParameter();
            pPren.ParameterName = "@prenom";
            pPren.SqlDbType = SqlDbType.VarChar;
            pPren.Size = 50;
            pPren.Value = this.prenom_ContactTextBox.Text;

            cmd.Parameters.Add(pPren);
            cmd.Parameters.AddWithValue("@mail", this.mail_ContactTextBox.Text);

            cmd.Parameters.Add("@nele", SqlDbType.DateTime);
            cmd.Parameters["@nele"].Value = nele_ContactDateTimePicker.Value;

            cmd.Parameters.AddWithValue("@tel", this.tel_ContactTextBox.Text);


            SqlParameter pNum = new SqlParameter("@num", SqlDbType.Int);
            pNum.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pNum);

            try
            {
                cnx.Open();
                int nb = cmd.ExecuteNonQuery();


                int? numero = (int?)cmd.Parameters["@num"].Value;
                MessageBox.Show("Création de : " + numero.ToString());



            }
            catch (Exception)
            {
                throw;
            }
            finally { cnx.Close(); }

        }
    }
}
