﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Transactions;

namespace prjContactManagerADO
{
    public partial class FrmDataSet : Form
    {
        public FrmDataSet()
        {
            InitializeComponent();
        }
        dsManager dsM;
        dsManagerTableAdapters.tblContactsTableAdapter taContacts;
        private void copierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Copie ?", "Bulk Copie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                DataSet ds = ModeDeconnecte.GenerationManuelleDataTable();
                DataTable dtContacts = ds.Tables[0];

                SqlBulkCopy bulkCopie = new SqlBulkCopy(Properties.Settings.Default.dbSQLCnx);
                bulkCopie.DestinationTableName = "tblContacts";
                bulkCopie.NotifyAfter = 3;
                bulkCopie.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopie_SqlRowsCopied);
                bulkCopie.WriteToServer(dtContacts);
            }

            dsM = new dsManager();
            taContacts = new dsManagerTableAdapters.tblContactsTableAdapter();
            taContacts.FillContacts(dsM.tblContacts);

            

            dataGridView1.DataSource = dsM.tblContacts;
            pnlBulkCopy.Visible = true;

        }

        void bulkCopie_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {

            MessageBox.Show("Lignes copiés : " + e.RowsCopied);
        }

        private void FrmDataSet_Load(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            taContacts.Update(dsM.tblContacts);
            MessageBox.Show("Mise a jour effectuée");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataRow ligne in dsM.tblContacts.Rows)
            {
                Debug.Print(ligne[2, DataRowVersion.Original] + " Current " + ligne[2, DataRowVersion.Current] + ligne.RowState.ToString());
            }

            dsM.tblContacts.AcceptChanges();
            foreach (DataRow ligne in dsM.tblContacts.Rows)
            {
                Debug.Print(ligne[2, DataRowVersion.Original] + " Current " + ligne[2, DataRowVersion.Current] + ligne.RowState.ToString());
            }
        }

        private void procedureStockeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmStoredProcedure().Show();
        }

        private void transactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                SqlConnection cnx = (SqlConnection)DBConnection.getConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "spAjoutContact";
                cmd.Connection = cnx;

                cmd.Parameters.AddWithValue("@nom", "DUPONT");

                SqlParameter pPren = new SqlParameter();
                pPren.ParameterName = "@prenom";
                pPren.SqlDbType = SqlDbType.VarChar;
                pPren.Size = 50;
                pPren.Value = "Fred";

                cmd.Parameters.Add(pPren);
                cmd.Parameters.AddWithValue("@mail", "fdupont@toto.fr");

                cmd.Parameters.Add("@nele", SqlDbType.DateTime);
                cmd.Parameters["@nele"].Value = DateTime.Now.ToString();

                cmd.Parameters.AddWithValue("@tel", "01-47-58-02-69");


                SqlParameter pNum = new SqlParameter("@num", SqlDbType.Int);
                pNum.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pNum);

                try
                {
                    cnx.Open();
                    int nb = cmd.ExecuteNonQuery();


                    int? numero = (int?)cmd.Parameters["@num"].Value;
                    //MessageBox.Show("Création de : " + numero.ToString());

                    SqlCommand cmdEnt = new SqlCommand();
                    cmdEnt.Connection = cnx;
                    cmdEnt.CommandText = "insert into tblEntreprises (Nom_Ent, Adr_Ent, Cp_Ent, Ville_Ent, Id_Contact) VALUES ('AXA', '10 rue de chez moi TROLOLO', 94300 ,'Vincenne', " + numero + ")";

                    int nbl = cmdEnt.ExecuteNonQuery();
                    trans.Complete();
                }
                catch (Exception)
                {
                    throw;
                }
                finally { cnx.Close(); }
            }
        }
    }
}
