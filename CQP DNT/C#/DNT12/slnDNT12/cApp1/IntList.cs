﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace cApp1
{
    class IntList :IEnumerable
    {
        private ArrayList al = new ArrayList();

        public void Add(int val)
        {
            al.Add(val);
        }
        public int this[int index]
        {
            get { return (int)al[index]; }
            set { /* set the specified index to value here */ }
        }
        public IEnumerator  GetEnumerator()
        {
            return al.GetEnumerator();
        }
        }
}
