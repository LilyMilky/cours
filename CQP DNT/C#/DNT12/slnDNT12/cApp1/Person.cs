﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    abstract class Person :IComparable
    {
        
        private string _nom = String.Empty;

        public string nom
        {
            get { return _nom.ToUpper(); }
            set { _nom = value; }
        }
        
        private int _age = 0;
        
        public int age
        {
            get { return _age; }
            set { _age = value; }
        }

        private string _pren= String.Empty;

        public string pren
        {
            get {
                _pren = FirstMaj(_pren);
                    return _pren; }
            set { _pren = value; }
        }

        private string FirstMaj(string _pren)
        {
            return _pren[0].ToString().ToUpper() + _pren.Substring(1).ToLower();
        }


        private static int _majorite = 18;

        public static int Majorite
        {
            get { return Person._majorite; }
            set { Person._majorite = value; }
        }

        //Constructeur
        public Person(string nom="", string vPren="", int vAge=0)
        {
            this.nom = nom;
            pren = vPren;
            age = vAge;
        }



        //Methodes 

        public virtual string AffichePers(string msg="")
        {
            string ch = msg + Environment.NewLine + String.Format("\nNom : {0} Pren : {1}  Pren : {2}\n", 
                                    this.nom, this.pren, this.age);

            ch+= (this._age > Person.Majorite) ? " est Majeur":" est Mineur";

            return ch;
        }

        //public override string ToString()
        //{
        //    return this.Affiche();
        //}
        public abstract string Affiche(string msg="Salut");

        public virtual void Message()
        {
            Console.WriteLine("Message Person");
        }

        public override bool Equals(object obj)
        {
            if (obj is Person)
            {
                Person objP = (Person)obj;
                return this.nom == objP.nom && this.pren.Equals(objP.pren) && this.age == objP.age;
            }
            else
                return false;

        }

        public override string ToString()
        {
            return AffichePers();
        }


        public override int GetHashCode()
        {
            return String.Concat(this.nom + " " + this.pren).GetHashCode();
        }


        public int CompareTo(object obj)
        {
            if (obj is Person)
            {
                Person objPers = obj as Person;
                return this.nom.CompareTo(objPers.nom);

            }
            else
                throw new ArgumentException("Pas le type Attendu");
        }
    }
}
