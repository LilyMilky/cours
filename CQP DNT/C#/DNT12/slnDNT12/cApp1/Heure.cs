﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Heure
    {
        private int h, m;
        public Heure(int h, int m)
        {
            this.h = h; this.m = m;

        }


        public override string ToString()

        {
            return String.Format("{0:00}:{1:00}",this.h, this.m);
        }
        public static int operator -(Heure hDeb, Heure hFin)
        {
            return (hDeb.h * 60 + hDeb.m) - (hFin.h * 60 + hFin.m);
        }

        public static Heure operator ++(Heure hDeb)
        {
            return new Heure(hDeb.h + 1,hDeb.m);
        }
    }
}
