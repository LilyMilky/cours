﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    partial class Personnel
    {

        public string this[string nom]
        {
            get
            {
                if (!String.IsNullOrEmpty(nom))
                {
                    for (int i = 0; i < tab.Length; i++)
                    {
                        if (tab[i].nom == nom)
                            return tab[i].pren;
                    }
                }
                return null;
            }

        }


         public void Lister()
        {
            foreach (Person elem in tab)
            {
                Console.WriteLine(elem.Affiche());
            }
        }


    }
}
