﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace cApp1
{
    class Cercle
    {
        Point centre;
        int rayon;

        public Cercle(int x, int y, int r)
        {
            centre = new Point(x, y);
            rayon = r;
        }

        public override string ToString()
        {
            return centre + " Rayon : " + this.rayon;
        }
    }
}
