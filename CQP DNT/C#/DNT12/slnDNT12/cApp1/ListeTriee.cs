﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class ListeTriee<T> : List<T> where T:Person, IComparable
    {
        public void Ajouter(T element) 
        {
            base.Add(element);
        }

        public T recupElement(int index)
        {
            return base[index];
        }

        public void trier()
        {
            base.Sort();
        }
    }
}
