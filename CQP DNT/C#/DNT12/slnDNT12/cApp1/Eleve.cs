﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class Eleve : Person
    {
        private float note;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        /// <param name="a"></param>
        /// <param name="no"></param>
        public Eleve(string n, string p, int a, float no):base(n,p,a)
        {
            this.note = no;
        }


        /// <summary>
        /// 
        /// </summary>
        public float Note
        {
            get { return note; }
            set { note = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public override string Affiche(string msg="Coucou")
        {
            return base.AffichePers() + " Note : " + this.Note;
        }

        public void TResultat(int NoteMax)
        {
            int moy = NoteMax / 2;
            if (note >= moy)
                Console.WriteLine("Admis"); 
            else
                Console.WriteLine("Non Admis");
        }

        public new void Message()
        {
            Console.WriteLine("Message Eleve");
        }

    }
}
