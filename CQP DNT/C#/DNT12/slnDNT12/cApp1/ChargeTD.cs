﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    class ChargeTD : Eleve, ICalcul
    {
        private float tauxHoraire;

        public float TauxHoraire
        {
            get { return tauxHoraire; }
            set { tauxHoraire = value; }
        }
        private int nbHeures;

        public int NbHeures
        {
            get { return nbHeures; }
            set { nbHeures = value; }
        }

        public ChargeTD(string n, string p, int a, float no, float t, int nb):base(n,p,a,no)
        {
            this.tauxHoraire = t;
            this.NbHeures = nb;
        }

        public override string Affiche(string Message="Fac : ")
        {
            return base.Affiche() + " Tx : " + this.TauxHoraire + " Nb " + this.NbHeures;
        }

        public double CalculBrut()
        {
            return (this.nbHeures * this.tauxHoraire) * 1.2;
        }
    }
}
