﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace cApp1
{
     class Employe:Person, ICalcul
    {
        private float _salaire;

        public float salaire
        {
            get { return _salaire; }
            set { _salaire = value; }
        }

        public Employe(string nom, string pren, int age, float salaire):base(nom, pren, age)
        {
            this.salaire = salaire;
       }


        public override string Affiche(string msg = "")
        {
            return base.AffichePers(msg) + "Salaire : " + this.salaire;
        }

        public double SalaireAnnuel { get { return this.salaire * 12; } }

        public override void Message()
        {
            Console.WriteLine("Message Employe");
        }

        ~Employe()
        {
            //Trace.WriteLine("Employe detruit.");
            Console.WriteLine("Obj detruit");
        }


        public double CalculBrut()
        {
            return this.salaire * 1.2;
        }


        //Comparaison
        public override bool Equals(object obj)
        {

            if ((obj != null ) && (this.GetType() == obj.GetType()))
            {
                Employe objE = (Employe)obj;
                return base.Equals(objE) && this.salaire == objE.salaire;
            }
            else
                return false;

        }


        public static bool operator ==(Employe e1, Employe e2)
        {
            return e1.Equals(e2);
        }
        public static bool operator !=(Employe e1, Employe e2)
        {
            return !e1.Equals(e2);
        }
    }
}
