﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cApp1
{
    partial class Personnel:IEnumerable<Person>
    {
        private Person[] tab = null;
        private int index = 0;

        public Personnel(int nb=5)
        {
            tab = new Person[nb];
        }

        public int Nombre { get { return index; } }

        public bool AjouterPers(Person p)
        {
            if (index < tab.Length)
            {
                tab[index++] = p;
                return true;
            }

            return false;
        }


        //public string this[int position]
        //{
        //    get { return tab[index].nom; }
        //    set { tab[index].nom = value; }
        //}

        //
        public Person this[int position]
        {
            get {
                position--;
                if (position >= 0 && position <= this.index)
                    return tab[position];
                else
                    return null;
                }
        }









        public IEnumerator<Person> GetEnumerator()
        {
            foreach (Person p in tab)
            {
                yield return p;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
