using System;
using System.Collections.Generic;
using System.Text;

namespace prjDelegues
{
    class AccidentEventArgs : EventArgs
    {
        public string adresse;
        public AccidentEventArgs(string adresse)
        {
            this.adresse = adresse;
        }
    }

    class Surveillant
    {
        public delegate void AccidentHandler(object sender, AccidentEventArgs acc);
        public event  AccidentHandler Accident;

        public void signaler(string adr)
        {
            AccidentEventArgs e = new AccidentEventArgs(adr);
            if (Accident != null)
                Accident(this, e);
        }


    }
    class Samu
    {
        public void onAccident(object sender, AccidentEventArgs e)
        {
            System.Console.WriteLine("Appele recu pour "+ e.adresse);
        }
    }
}
