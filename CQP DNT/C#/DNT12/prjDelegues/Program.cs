using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace prjDelegues
{
    class Program
    {
        //Sans argument
        static void f1() { System.Console.WriteLine("fonction f1" ); }
        static void f2() { System.Console.WriteLine("fonction f2" ); }

        //Avec Arguments
        static void f3(int val) { System.Console.WriteLine("fonction f3 avec val : " + val.ToString()); }
        static void f4(int val) { System.Console.WriteLine("fonction f4 avec val : " + val.ToString()); }

        //On definit un delegue qui pointe sur des fonctions ss arguments        
        delegate void T();

        //Lancer plusieurs fonctions les unes a la suite des autres
        delegate void delMulticast();

        //delegue utilise pour des fonctions comportant un argument
        delegate void delFctArg(int n);


        static void Main(string[] args)
        {


            /*
            //Delegue sur une fonction sans argument
            System.Console.WriteLine("Delegue sur une fonction sans argument. Undelegue par fonction");
            T de = new T(f1);
            T de1 = f1;
            de();
            
            de = new T(f2);
            de();
            Console.Read();
            




            //Lancer plusieurs fonctions a partir d'un delegue
            System.Console.WriteLine("Lancer plusieurs fonctions a partir d'un delegue");
            delMulticast delUn = new delMulticast(f1);
            delUn += new delMulticast(f2);
            delUn();
            Console.Read();



            /*delFctArg delDeux = new delFctArg(f3);
            delDeux(100);
            delDeux += new delFctArg(f4);
            delDeux(150);
            delFctArg delPara;
            delPara = f4;
            delPara.Invoke(10);
            Console.Read();
*/
            //Gestion des Events via des delegu�s
            Surveillant sur = new Surveillant();
            Samu sam = new Samu();
            
            //On le notifie sur le gestionnaire d'event
            sur.Accident+=new Surveillant.AccidentHandler(sam.onAccident);
            sur.Accident += new Surveillant.AccidentHandler(sur_Accident);
            sur.signaler("Hopital BEGIN");

        }

        static void sur_Accident(object sender, AccidentEventArgs acc)
        {
            Console.WriteLine("Appel Recu Pompier ! " + acc.adresse);
        }
    }
}
