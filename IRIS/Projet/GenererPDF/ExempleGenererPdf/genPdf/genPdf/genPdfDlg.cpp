// genPdfDlg.cpp : fichier d'impl�mentation
//

#include "stdafx.h"
#include "genPdf.h"
#include "genPdfDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// bo�te de dialogue CgenPdfDlg




CgenPdfDlg::CgenPdfDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CgenPdfDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CgenPdfDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CgenPdfDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CgenPdfDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// gestionnaires de messages pour CgenPdfDlg

BOOL CgenPdfDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// D�finir l'ic�ne de cette bo�te de dialogue. L'infrastructure effectue cela automatiquement
	//  lorsque la fen�tre principale de l'application n'est pas une bo�te de dialogue
	SetIcon(m_hIcon, TRUE);			// D�finir une grande ic�ne
	SetIcon(m_hIcon, FALSE);		// D�finir une petite ic�ne

	// TODO : ajoutez ici une initialisation suppl�mentaire


	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

// Si vous ajoutez un bouton R�duire � votre bo�te de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'ic�ne. Pour les applications MFC utilisant le mod�le Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CgenPdfDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de p�riph�rique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'ic�ne dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'ic�ne
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Le syst�me appelle cette fonction pour obtenir le curseur � afficher lorsque l'utilisateur fait glisser
//  la fen�tre r�duite.
HCURSOR CgenPdfDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CgenPdfDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CString tempFdf = "tempFDF.fdf";		// Creation d'un attribut tempFdf de type CString initialis� � "tempFDF.fdf"
	CString inputPdf = "PDFfacture.pdf";	// Creation d'un attribut inputPdf de type CString initialis� � "PDFfacture.pdf"
	CString outputPdf = "Facture.pdf";		// Creation d'un attribut outputPdf de type CString initialis� � "Facture.pdf"
	GetFileAttributes(outputPdf);			// Permet de regarder les attributs du fichier outputPdf
	if (GetLastError() != ERROR_FILE_NOT_FOUND)
		DeleteFile(outputPdf);			// Si outputPdf est non trouv�e, on supprime le fichier
	generateFdfFile(inputPdf, tempFdf);		// Permet de g�n�rer le fichier Fdf
	if (generatePDF(inputPdf, tempFdf, outputPdf))
		ShellExecute(NULL,"open",outputPdf,"","",SW_NORMAL);	// Permet d'ouvrir un fichier, dans notre cas outputPdf
	else
		AfxMessageBox("Erreur lors de la g�n�ration automatique du PDF");
	DeleteFile(tempFdf);		// Suppresion du fichier tempFdf

}

void CgenPdfDlg::generateFdfFile(CString pdfInput, CString fdfOutput)
{
	CStdioFile fdfFile(fdfOutput, CFile::modeCreate | CFile::modeWrite | CFile::typeText ); 
	CString name, surName, Ville, Ville2, Adresse, Adresse2, Tag, Tag2, DateFacture, DateReglement, NumFacture, CodePostal, CodePostal2, CodePostal3, ReferenceAbonnement, PeriodeFactureDebut, PeriodeFactureFin, PrixTotal;

	GetDlgItem(IDC_Nom)->GetWindowText(name);			// Permet de recup�rer ce que l'on a �crit dans notre champs (Ici le champ IDC_Nom)
	GetDlgItem(IDC_Prenom)->GetWindowText(surName);
	GetDlgItem(IDC_Ville)->GetWindowText(Ville);
	GetDlgItem(IDC_Ville)->GetWindowText(Ville2);
	GetDlgItem(IDC_Adresse)->GetWindowText(Adresse);
	GetDlgItem(IDC_Adresse)->GetWindowText(Adresse2);
	GetDlgItem(IDC_CodePostal)->GetWindowText(CodePostal);
	GetDlgItem(IDC_CodePostal)->GetWindowText(CodePostal2);
	GetDlgItem(IDC_CodePostal)->GetWindowText(CodePostal3);
	GetDlgItem(IDC_Tag)->GetWindowText(Tag);
	GetDlgItem(IDC_DateFacture)->GetWindowText(DateFacture);
	GetDlgItem(IDC_DateReglement)->GetWindowText(DateReglement);
	GetDlgItem(IDC_NumFacture)->GetWindowText(NumFacture);
	GetDlgItem(IDC_RefAbonnement)->GetWindowText(Tag2);
	GetDlgItem(IDC_FactureBegin)->GetWindowText(PeriodeFactureDebut);
	GetDlgItem(IDC_FactureEnd)->GetWindowText(PeriodeFactureFin);
	GetDlgItem(IDC_PrixTotal)->GetWindowText(PrixTotal);


	CString fdfString;
	fdfString = "%FDF-1.2\n%����\n1 0 obj\n<< /FDF";
	fdfString += "<< /F(" + pdfInput + ") /Fields 2 0 R>>>>\n";
	fdfString += "endobj\n2 0 obj\n[\n";


	fdfString += "<< /T(Ville) /V(" + Ville +" " " )>>\n";
	fdfString += "<< /T(Ville2) /V(" + Ville2 +" " " )>>\n";
	fdfString += "<< /T(NomPrenomHabitant) /V(" + name + " " + surName + ")>>\n";	
	fdfString += "<< /T(NumeroFacture) /V(" + NumFacture +" )>>\n";
	fdfString += "<< /T(DateFacture) /V(" + DateFacture +"  )>>\n";
	fdfString += "<< /T(DateReglement) /V(" + DateReglement +"  )>>\n";
	fdfString += "<< /T(AdresseHabitant) /V(" + Adresse+"  )>>\n";
	fdfString += "<< /T(AdresseHabitant2) /V(" + Adresse2+"  )>>\n";
	fdfString += "<< /T(NumTag) /V(" + Tag +" )>>\n";
	fdfString += "<< /T(NumTag2) /V(" + Tag2 +" )>>\n";
	fdfString += "<< /T(CodePostal) /V(" + CodePostal +" )>>\n";
	fdfString += "<< /T(CodePostal2) /V(" + CodePostal2 +" )>>\n";
	fdfString += "<< /T(CodePostal3) /V(" + CodePostal3 +" )>>\n";
	fdfString += "<< /T(PeriodeFactureDebut) /V(" + PeriodeFactureDebut +"  )>>\n";
	fdfString += "<< /T(PeriodeFactureFin) /V(" + PeriodeFactureFin +"  )>>\n";
    fdfString += "<< /T(NetAPayer) /V(" + PrixTotal +"  )>>\n";
	fdfString += "]\nendobj\ntrailer\n<</Root 1 0 R>>\n%%EOF\n";

	fdfFile.WriteString(fdfString); 
	fdfFile.Close();
}

BOOL CgenPdfDlg::generatePDF(CString pdfInput, CString fdfInput, CString pdfOutput)
{
	//                  pdftk form.pdf fill_form data.fdf output out.pdf
	CString commandLine = "pdftk.exe " + pdfInput + " fill_form " + fdfInput + " output " + pdfOutput;
	CString outputString;
    SECURITY_DESCRIPTOR sd;
    SECURITY_ATTRIBUTES sa;
    InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
    SetSecurityDescriptorDacl(&sd, true, NULL, false);
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = true;
    sa.lpSecurityDescriptor = &sd;
    
    HANDLE hReadPipe;
    HANDLE hWritePipe;
    if (! CreatePipe(&hReadPipe, &hWritePipe, &sa, NULL)) 
        return FALSE;
    
    STARTUPINFO si;
    memset(&si, 0, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESHOWWINDOW |STARTF_USESTDHANDLES;
    si.wShowWindow = SW_HIDE;
    si.hStdOutput = hWritePipe;
    si.hStdError = hWritePipe;
    PROCESS_INFORMATION pi;
    
    if ( ! CreateProcess(NULL, commandLine.GetBuffer(commandLine.GetLength()), NULL, NULL, TRUE, 0, 0, 0, &si, &pi) )
        return FALSE;
    WaitForSingleObject(pi.hProcess, INFINITE);
    
    DWORD BytesRead ;
    char dest[2048];
    memset(dest, 0, 2048);
	DWORD byteAvail = 0;
	PeekNamedPipe(hReadPipe, NULL, 0, NULL, &byteAvail, NULL);
	if (!byteAvail) // la g�n�ration n'a rien produit, tout est ok
		return TRUE;
    while (ReadFile(hReadPipe, dest, 2048, &BytesRead, NULL))
    {
        if (!BytesRead)
            break;
		outputString += dest;
		PeekNamedPipe(hReadPipe, NULL, 0, NULL, &byteAvail, NULL);
        if (BytesRead<2048 || !byteAvail)
            break;
    }
	AfxMessageBox(outputString); // affiche l'erreur
    CloseHandle(hReadPipe);
    CloseHandle(hWritePipe);
	return FALSE;
}