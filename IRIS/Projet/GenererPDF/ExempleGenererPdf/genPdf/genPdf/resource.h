//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by genPdf.rc
//
#define IDD_GENPDF_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_Nom                         1000
#define IDC_EDIT2                       1001
#define IDC_Prenom                      1001
#define IDC_EDIT3                       1002
#define IDC_CodePostal                  1002
#define IDC_Adresse                     1003
#define IDC_Ville                       1004
#define IDC_Tag                         1005
#define IDC_DateFacture                 1006
#define IDC_DateReglement               1007
#define IDC_NumFacture                  1008
#define IDC_RefAbonnement               1009
#define IDC_FactureBegin                1010
#define IDC_EDIT6                       1011
#define IDC_FactureEnd                  1011
#define IDC_EDIT4                       1012
#define IDC_PrixTotal                   1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
