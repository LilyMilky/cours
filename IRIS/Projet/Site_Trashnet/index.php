<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
   <head>
       <title> Site officiel du TrashNet </title>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	   <link rel="stylesheet" tylie="text/css" media="screen" href="css/Style.css" />
	   <link rel="icon" type="image/jpg" href="img/logo.jpg" />
	</head>
	<body>
		<div id = "entete">
			<?php
				include("src/header.html");
			?>
		</div>
		
		<div id="menu">
			<?php
				include("src/menu.html");
			?>
		</div>
		
		<div id="corps">
			<?php
				include("src/verifcorps.php");
			?>
		</div>
		
		<div id="footer">
			<?php
				include("src/footer.html");
			?>
		</div>
	</body>
	
</html>