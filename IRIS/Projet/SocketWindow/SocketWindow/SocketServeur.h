#pragma once
#include "SocketService.h"

// Cible de la commande CSocketServeur

class CSocketServeur : public CAsyncSocket
{
public:
	CSocketServeur();
	virtual ~CSocketServeur();
	void OnAccept(int CodeErreur); 
	
private:
	CSocketService m_SocService;
};


