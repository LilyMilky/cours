// SocketServeur.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "SocketWindow.h"
#include "SocketServeur.h"


// CSocketServeur

CSocketServeur::CSocketServeur()
{
}

CSocketServeur::~CSocketServeur()
{
}

void CSocketServeur::OnAccept(int CodeErreur)
{
	if (m_hSocket == INVALID_SOCKET)
	{
		VERIFY(SOCKET_ERROR != closesocket(m_hSocket));
		CAsyncSocket::KillSocket(m_hSocket, this);
		m_hSocket = INVALID_SOCKET;
	}

	// le serveur accepte les connexions
    Accept(m_SocService);
    CAsyncSocket::OnAccept(CodeErreur);
        
}

// Fonctions membres CSocketServeur
