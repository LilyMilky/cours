
// SocketWindow.h�: fichier d'en-t�te principal pour l'application PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "incluez 'stdafx.h' avant d'inclure ce fichier pour PCH"
#endif

#include "resource.h"		// symboles principaux


// CSocketWindowApp�:
// Consultez SocketWindow.cpp pour l'impl�mentation de cette classe
//

class CSocketWindowApp : public CWinApp
{
public:
	CSocketWindowApp();

// Substitutions
public:
	virtual BOOL InitInstance();

// Impl�mentation

	DECLARE_MESSAGE_MAP()
};

extern CSocketWindowApp theApp;