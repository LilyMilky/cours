// SocketService.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "SocketWindow.h"
#include "SocketService.h"


// CSocketService

CSocketService::CSocketService()
{
}

CSocketService::~CSocketService()
{
}

void CSocketService::OnReceive(int CodeErreur)
{ 
	// Exemple de code pour la r�ception des donn�es 
	TCHAR buffer[5120];
	CString m_strReception;
	int nbRead;
	nbRead = Receive(buffer, 5120);
	switch (nbRead)
	{
	case 0:
		Close();
		break;
	case SOCKET_ERROR:
		if (GetLastError() != WSAEWOULDBLOCK)
		{
			AfxMessageBox(_T("Une erreur !!"));
			Close();
		}
		break;
	default:
		buffer[nbRead] = 0; // pour terminer la chaine
		CString szTmp(buffer);
		m_strReception += szTmp; // m_strReception est un objet CString
		AfxMessageBox(m_strReception);
		// in CMyAsyncSocket
		if (szTmp.CompareNoCase(_T("ciao")) == 0 ) ShutDown();
	}
	// appel de la version classe de base
	CAsyncSocket::OnReceive(CodeErreur);

}