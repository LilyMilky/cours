#include <ctype.h>

#ifdef TIXML_USE_STL
#include <sstream>
#include <iostream>
#endif

#include "tinyxml.h"
#include <string>
using namespace std;

void main( )
{
	int TAGRFID[10] = {1001,5001,1500,1501,5120,1547,1523,2658,6548,4512} ,Masse[10] = {25,50,75,100,125,150,175,200,225,250};
	char DateDebutTournee[20] = "29/10/2011 10h30" , DateFinTournee[20] = "29/10/2011 19h10";

	TiXmlDocument doc;
	TiXmlElement * Date;
	TiXmlElement * Habitant;

	TiXmlElement * root = new TiXmlElement( "TrashNet" );			//Ajout Balise TrashNet
	doc.LinkEndChild( root );

/*******************************************************************************************************************
														Block Date
*******************************************************************************************************************/

	Date = new TiXmlElement( "Date" );								//Ajout Balise Date
	root->LinkEndChild( Date );
		
	Date->SetAttribute("DateDebutTournee", DateDebutTournee);		//Ajout Date debut tournee = DateDebutTournee
																	//dans la balise Date

/*******************************************************************************************************************
														Block Habitant
*******************************************************************************************************************/
	for(int i = 0; i < 10 ; i++)
	{			
		Habitant = new TiXmlElement( "Habitant" );					//Ajout Balide Habitant
		root->LinkEndChild( Habitant );	
		
		Habitant->SetAttribute("TAG", TAGRFID[i]);					//Ajout TAG = TAGRIFD[i] dans la balise Habitant
		Habitant->SetAttribute("Masse", Masse[i]);					//Ajout Masse = Masse[i] dans la balise Habitant
	}
		
	Date->SetAttribute("DateFinTournee", DateFinTournee);			//Ajout Date debut tournee = DateDebutTournee
																	//dans la balise Date

	doc.SaveFile( "Masse.xml" );									//Sauvegarde dans le fichier Masse.xml
}
