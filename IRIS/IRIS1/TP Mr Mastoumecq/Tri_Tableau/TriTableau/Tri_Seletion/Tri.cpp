/******************************************************************************************************
											Fonction Tri_Selection
******************************************************************************************************/

#include "Permutation.h"
#include <iostream>
using namespace std;

void Trie(int Tri[],const int NbCase)
{
	int i, Pos = 1, Min, Case,Tour,j, X = 0;
	for (Tour = 0; Tour < NbCase - 1; Tour++)
	{
		Min = Tri[Tour];
		for (i = Tour ; i < NbCase - 1; i++)
		{
			if (Tri[i] < Min)
			{
				Min = Tri[i];
				Case = i;
			}
		}
		Permutation (Tri, Case, Pos);

			cout << "\nTour "<< X << " : ";
			X = X + 1;
			for (j = 0; j < NbCase; j++)
			{
				cout << Tri[j] << " ";
			}
		Pos = Pos + 1;
	}
}