/********************************************************************************************************
Yann Lacquehay
Programme Tri_Tableau
05/12/09
********************************************************************************************************/

#include "Tri.h"
#include <iostream>
using namespace std;

void main (void)
{
	const int NbCase = 10;
	int i,Tri[NbCase];

	for (i = 0; i < NbCase; i++)
	{
		cout << "Veuillez saisir la valeur de la case " << i << " : ";
		cin  >> Tri[i];
	}
	cout << "			****Tableau non trie****" << endl;
	
	for (i = 0; i < NbCase; i++)
	{
		cout << Tri[i] << " ";
	}

	Trie( Tri, NbCase );

	cout << "\n			****Tableau trie****" << endl;
	for (i = 0; i < NbCase; i++)
	{
		cout << Tri[i] << " ";
	}
}