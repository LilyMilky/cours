/******************************************************************************************************
											Fonction Tri_Propagation
******************************************************************************************************/
#include "Permutation.h"
#include <iostream>
using namespace std;

void Trie(int Tri[], const int NbCase)
{
	int i,j, X = 0;
	bool Fin = true;

	do
	{
		for (i = 0; i < NbCase - 1; i++)
		{
			if (Tri[i] > Tri[i+1])
			{
				Permutation ( Tri, i, i+1);
				Fin = false;
			}

			cout << "\nTour "<< X << " : ";
			X = X + 1;
			for (j = 0; j < NbCase; j++)
			{
				cout << Tri[j] << " ";
			}

		}
	}while (!(Fin = true));
}