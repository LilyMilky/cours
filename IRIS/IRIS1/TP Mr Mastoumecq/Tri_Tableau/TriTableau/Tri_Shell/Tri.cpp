/******************************************************************************************************
											Fonction Tri_Shell
******************************************************************************************************/

#include "Permutation.h"

void Trie(int Tri[],const int NbCase)
{
	int Delta = 0, i, j;

	do
	{
		Delta = 3 * Delta + 1;
	}while (!(Delta > NbCase));

	do
	{
		Delta = Delta / 3;
		for (i = Delta; i < NbCase - 1; i++)
		{
			for (j = i; j < Delta; j = -Delta)
			{
				if (Tri[j] < Tri[j - Delta])
				{
					Permutation (Tri, j, j-Delta);
				}
			}
		}
	}while(!(Delta = 1));
}