//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Prem_IHM2.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PREM_IHM2_DIALOG            103
#define IDR_MAINFRAME                   128
#define IDC_Fumeur                      1000
#define IDC_Sportif                     1001
#define IDC_Age1                        1002
#define IDC_Age2                        1003
#define IDC_Age3                        1004
#define IDC_Taille1                     1005
#define IDC_Taille2                     1006
#define IDC_Taille3                     1007
#define IDC_Taille4                     1008
#define IDC_AfficheAge                  1009
#define IDC_AfficheTaille               1010
#define IDC_AfficheSportif              1011
#define IDC_AfficheFumeur               1012
#define IDC_AfficheCurseur              1013
#define IDC_Curseur                     1014
#define IDC_BoutonLireFichier           1016
#define IDC_BoutonSupprimer             1017
#define IDC_BoutonInserer               1018
#define IDC_AfficheNumeroZone           1019
#define IDC_PROGRESS1                   1021
#define IDC_ComboBox                    1022
#define IDC_ChoixAjout                  1023
#define IDC_BoutonAjouter               1024
#define IDC_ChoixRealise                1025
#define IDC_BoutonOuvrir                1026
#define IDC_BoutonAjout                 1027
#define IDC_EDIT6                       1028
#define IDC_LIST1                       1029
#define IDC_ListBox                     1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
