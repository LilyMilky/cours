// Prem_IHM2Dlg.h : fichier d'en-t�te
//

#pragma once
#include "afxwin.h"


// bo�te de dialogue CPrem_IHM2Dlg
class CPrem_IHM2Dlg : public CDialog
{
// Construction
public:
	CPrem_IHM2Dlg(CWnd* pParent = NULL);	// constructeur standard

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PREM_IHM2_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Impl�mentation
protected:
	HICON m_hIcon;

	// Fonctions g�n�r�es de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedFumeur();
	afx_msg void OnBnClickedAge1();
	afx_msg void OnBnClickedAge2();
	afx_msg void OnBnClickedAge3();
	afx_msg void OnBnClickedTaille1();
	afx_msg void OnBnClickedTaille2();
	afx_msg void OnBnClickedTaille3();
	afx_msg void OnBnClickedTaille4();
	afx_msg void OnBnClickedBoutonlirefichier();
	afx_msg void OnBnClickedBoutonsupprimer();
	afx_msg void OnBnClickedBoutoninserer();
private:
	BOOL m_Fumeur;
	CString m_AfficheFumeur;
	afx_msg void OnBnClickedSportif();
	CString m_AfficheAge;
	CString m_AfficheTaille;
	BOOL m_Sportif;
	CString m_AfficheSportif;
	CEdit NumZone;
	CListBox m_ZoneAffichage;
	CButton m_Suppr;
	CButton m_Inserer;
};
