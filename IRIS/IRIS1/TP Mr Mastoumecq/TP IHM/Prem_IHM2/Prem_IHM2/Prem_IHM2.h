// Prem_IHM2.h�: fichier d'en-t�te principal pour l'application PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "incluez 'stdafx.h' avant d'inclure ce fichier pour PCH"
#endif

#include "resource.h"		// symboles principaux


// CPrem_IHM2App:
// Consultez Prem_IHM2.cpp pour l'impl�mentation de cette classe
//

class CPrem_IHM2App : public CWinApp
{
public:
	CPrem_IHM2App();

// Substitutions
	public:
	virtual BOOL InitInstance();

// Impl�mentation

	DECLARE_MESSAGE_MAP()
};

extern CPrem_IHM2App theApp;