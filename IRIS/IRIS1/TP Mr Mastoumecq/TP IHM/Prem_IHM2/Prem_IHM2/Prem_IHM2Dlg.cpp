// Prem_IHM2Dlg.cpp : fichier d'impl�mentation
//

#include "stdafx.h"
#include "Prem_IHM2.h"
#include "Prem_IHM2Dlg.h"
#include <fstream>
#include <iostream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// bo�te de dialogue CAboutDlg utilis�e pour la bo�te de dialogue '� propos de' pour votre application

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

// Impl�mentation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// bo�te de dialogue CPrem_IHM2Dlg




CPrem_IHM2Dlg::CPrem_IHM2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrem_IHM2Dlg::IDD, pParent)
	, m_Fumeur(FALSE)
	, m_AfficheFumeur(_T("Non"))
	, m_Sportif(FALSE)
	, m_AfficheSportif(_T("Non"))
	, m_AfficheAge(_T("???"))
	, m_AfficheTaille(_T("???"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPrem_IHM2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_Fumeur, m_Fumeur);
	DDX_Text(pDX, IDC_AfficheFumeur, m_AfficheFumeur);
	DDX_Check(pDX, IDC_Sportif, m_Sportif);
	DDX_Text(pDX, IDC_AfficheSportif, m_AfficheSportif);
	DDX_Text(pDX, IDC_AfficheAge, m_AfficheAge);
	DDX_Text(pDX, IDC_AfficheTaille, m_AfficheTaille);
	DDX_Control(pDX, IDC_AfficheNumeroZone, NumZone);
	DDX_Control(pDX, IDC_ListBox, m_ZoneAffichage);
	DDX_Control(pDX, IDC_BoutonSupprimer, m_Suppr);
	DDX_Control(pDX, IDC_BoutonInserer, m_Inserer);
}

BEGIN_MESSAGE_MAP(CPrem_IHM2Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_Fumeur, &CPrem_IHM2Dlg::OnBnClickedFumeur)
	ON_BN_CLICKED(IDC_Sportif, &CPrem_IHM2Dlg::OnBnClickedSportif)
	ON_BN_CLICKED(IDC_Age1, &CPrem_IHM2Dlg::OnBnClickedAge1)
	ON_BN_CLICKED(IDC_Age2, &CPrem_IHM2Dlg::OnBnClickedAge2)
	ON_BN_CLICKED(IDC_Age3, &CPrem_IHM2Dlg::OnBnClickedAge3)
	ON_BN_CLICKED(IDC_Taille1, &CPrem_IHM2Dlg::OnBnClickedTaille1)
	ON_BN_CLICKED(IDC_Taille2, &CPrem_IHM2Dlg::OnBnClickedTaille2)
	ON_BN_CLICKED(IDC_Taille3, &CPrem_IHM2Dlg::OnBnClickedTaille3)
	ON_BN_CLICKED(IDC_Taille4, &CPrem_IHM2Dlg::OnBnClickedTaille4)
	ON_BN_CLICKED(IDC_BoutonLireFichier, &CPrem_IHM2Dlg::OnBnClickedBoutonlirefichier)
	ON_BN_CLICKED(IDC_BoutonSupprimer, &CPrem_IHM2Dlg::OnBnClickedBoutonsupprimer)
	ON_BN_CLICKED(IDC_BoutonInserer, &CPrem_IHM2Dlg::OnBnClickedBoutoninserer)
END_MESSAGE_MAP()


// gestionnaires de messages pour CPrem_IHM2Dlg

BOOL CPrem_IHM2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Ajouter l'�l�ment de menu "� propos de..." au menu Syst�me.

	// IDM_ABOUTBOX doit se trouver dans la plage des commandes syst�me.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// D�finir l'ic�ne de cette bo�te de dialogue. L'infrastructure effectue cela automatiquement
	//  lorsque la fen�tre principale de l'application n'est pas une bo�te de dialogue
	SetIcon(m_hIcon, TRUE);			// D�finir une grande ic�ne
	SetIcon(m_hIcon, FALSE);		// D�finir une petite ic�ne

	// TODO : ajoutez ici une initialisation suppl�mentaire

	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

void CPrem_IHM2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Si vous ajoutez un bouton R�duire � votre bo�te de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'ic�ne. Pour les applications MFC utilisant le mod�le Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CPrem_IHM2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de p�riph�rique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'ic�ne dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'ic�ne
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Le syst�me appelle cette fonction pour obtenir le curseur � afficher lorsque l'utilisateur fait glisser
//  la fen�tre r�duite.
HCURSOR CPrem_IHM2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++		Ajout    +++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

						/**************************************
						*******  Gestion Bouton Fumeur  *******
						***************************************/

void CPrem_IHM2Dlg::OnBnClickedFumeur()
{
	UpdateData(1);					//Recuperation Info

	if (m_Fumeur == TRUE)			//Si case coch�
	{
		m_AfficheFumeur = "Oui";
	}
	else							//D�coch�
	{
		m_AfficheFumeur = "Non";
	}

	UpdateData(0);					//Renvoi info
}
						/*************************************
						******  Gestion Bouton Sportif  ******
						*************************************/

void CPrem_IHM2Dlg::OnBnClickedSportif()
{
	UpdateData(1);

	if (m_Sportif == TRUE)
	{
		m_AfficheSportif = "Oui";
	}
	else
	{
		m_AfficheSportif = "Non";
	}

	UpdateData(0);
}

						/************************************
						********  Gestion Bouton Age ********
						************************************/

void CPrem_IHM2Dlg::OnBnClickedAge1()
{
	m_AfficheAge = "- de 18ans";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedAge2()
{
	m_AfficheAge = "entre 18 et 50ans";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedAge3()
{
	m_AfficheAge = "+ de 50ans";
	UpdateData(0);
}

						/***********************************
						******  Gestion Bouton Taille ******
						***********************************/

void CPrem_IHM2Dlg::OnBnClickedTaille1()
{
	m_AfficheTaille = "- de 1.50m";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedTaille2()
{
	m_AfficheTaille = "1.50m � 1.70m";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedTaille3()
{
	m_AfficheTaille = "1.71m � 1.90m";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedTaille4()
{
	m_AfficheTaille = "+ de 1.90m";
	UpdateData(0);
}

void CPrem_IHM2Dlg::OnBnClickedBoutonlirefichier()
{
	ifstream infile;
	char Tab[50];

	m_ZoneAffichage.ResetContent();

	infile.open("Temperature.txt", ios::in);
	if(!infile)
	{
		AfxMessageBox("Probleme ouverte flux");
	}
	else
	{
		while (infile.getline(Tab, sizeof(Tab)))
		{
			m_ZoneAffichage.AddString(Tab);
		}
	}

	if (infile.eof())
	{
		infile.clear();
	}
	infile.close();

	NumZone.ShowWindow(true);
	m_Suppr.EnableWindow(true);
	m_Inserer.EnableWindow(true);
}

void CPrem_IHM2Dlg::OnBnClickedBoutonsupprimer()
{
	int i,NbreLigne, ibis ;
	CString Supprimer, Ligne;
	bool Found = false;
	
	NumZone.GetWindowTextA(Supprimer);

	NbreLigne = m_ZoneAffichage.GetCount();
	for (i = 0; i < NbreLigne; i++)
	{ 
		m_ZoneAffichage.GetText(i,Ligne);
		if (Supprimer[0] == Ligne[4])
		{
			ibis = i;
			Found = true;
		}
	}
	if (Found == true)
	{
		m_ZoneAffichage.DeleteString(ibis);
	}
	else
	{
		AfxMessageBox ("Cette zone n'existe pas!");
	}
}

void CPrem_IHM2Dlg::OnBnClickedBoutoninserer()
{
	CString Inserer;
	NumZone.GetWindowTextA(Inserer);
	m_ZoneAffichage.AddString(Inserer);
}
