/***********************************---Tp Algo---*************************************
Programme Multiplicateur
Nom: Lacquehay Yann
Date: 14/09/09 
**************************************************************************************/

#include <iostream>						
using namespace std;

/****************************************************************************************************
										---Programme Principal---
****************************************************************************************************/

void main(void)		
{
	float Nombre, Resultat;

	cout << "Veuillez taper le nombre a multiplier: ";
	cin  >> Nombre;									// Recuperation valeur

	if(Nombre <= 10)								//Si Nombre < ou = � 10
	{
		Resultat = Nombre * 2;						
		cout << "Le resultat est: ";			// Affichage resultat Nombre*2
		cout << Resultat <<endl;
	}
	else
	{
		Resultat = Nombre * 20;						
		cout << "Le resultat est: ";
		cout << Resultat <<endl;				// Affichage resultat Nombre*20
	}
}