/***************************************---Tp Algo---**********************************************
Programme Prix_Reduction
Nom: Lacquehay Yann
Date: 01/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	float Prix, Prix_Reduc,Reduction;
	int i;

	i = 0;

	cout << "\t\t\t******Calcul reduction prix******\n\n";
	while (i != 1)
	{
		cout << "Veuillez taper le Prix payer: ";
		cin  >> Prix;
		Prix_Reduc=Prix;
		if (Prix >= 350.0 && Prix <= 600.0)
		{					
			Prix_Reduc = Prix * 0.97;					//Reduction 3%
			Reduction = Prix * 0.03;
			cout << "Reduction de 3%: ";
			cout << Reduction <<endl;
		}
		if ( Prix > 600.0)
		{					
			Prix_Reduc = Prix * 0.95;					//Reduction 5%
			Reduction = Prix * 0.05;
			cout << "Reduction de 5%: ";
			cout << Reduction <<endl;
	}
	cout << "Montant a Payer: ";
	cout << Prix_Reduc <<endl;
	cout << "\nSi vous voulez quitter le programme, tapez 1 sinon tapez un autre chiffre entier: ";
	cin  >> i;
	cout << "\n";
	}
}