/***************************************---Tp Algo---**********************************************
Programme Calcul_PPCM
Nom: Lacquehay Yann
Date: 01/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	int Nombre1, Nombre2, Temp1, Temp2,i;
	i = 0;

	cout << "\t  ******Calcul du plus petit multiple commun****** \n\n";

	while (i != 1)
	{
	cout << "Veuillez saisir une valeur enti\212re: ";
	cin  >> Nombre1;
	cout << "Veuillez saisir une autre valeur enti\212re plus grande du meme signe: ";
	cin  >> Nombre2;

	Temp1 = Nombre1;								
	Temp2 = Nombre2;

	while (Temp1 != Temp2)							//Repetition jusqu'a avoir le PPCM
	{												//On additionne Temp1 avec Nombre1
		Temp1 = Temp1 + Nombre1;					//Jusqu'� Temp1 >= � Temp2
		if (Temp1 > Temp2)							//Si Temp1 est plus grand que Temp2
		{											//On additionne Temp2 avec Nombre2
			while (Temp1 > Temp2)
			{
				Temp2 = Temp2 + Nombre2;
			}
		}
	}
	cout << "Le plus petit multiple commun est: ";
	cout << Temp1 <<endl;
	cout << "\nTapez 1 si vous voulez quitter le programme sinon entr\202 un autre chiffre entier:";
	cin  >> i ;
	cout << "\n";
	}
}