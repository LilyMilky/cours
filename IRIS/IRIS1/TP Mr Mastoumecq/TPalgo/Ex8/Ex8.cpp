/*************************---Tp Algo---******************************
Programme Somme 4 nombre boucle for
Nom: Lacquehay Yann
Date: 24/09/09 
********************************************************************/
#include <iostream>	
using namespace std;

/****************************************************************************************************
										---Programme Principal---
****************************************************************************************************/		
void main(void)							
{
	int Nbre , Somme ;

	Somme=0;
	
	for (int i = 1 ; i <= 4; i++)
	{
		cout << "Entre une valeur num\202rique enti\212re: ";
		cin  >> Nbre;

		Somme = Somme+Nbre;
	}

	cout << "La somme vaut: ";
	cout << Somme <<endl;
	
}