/***************************************---Tp Algo---**********************************************
Programme Multiplication_Decalage
Nom: Lacquehay Yann
Date: 05/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	int Nbre1, Nbre2, Resultat;
	Resultat = 0;

	cout << "Veuillez taper une valeur entiere positive: ";
	cin  >> Nbre1;
	cout << "Veuillez taper une autre valeur entiere positive: ";
	cin  >> Nbre2;
	while (Nbre1 != 0)
	{
		if ((Nbre1 % 2) == 0)
		{
			Nbre1 = Nbre1 >> 1;
			Nbre2 = Nbre2 << 1;
		}
		else
		{
			Nbre1 = Nbre1 - 1;
			Resultat = Resultat + Nbre2;
		}
	}
	cout << "Le resultat de la multiplication est: " << Resultat << endl;
}