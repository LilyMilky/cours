class RS232
{
private:
		HANDLE HandPort;
		DCB StructConfigPort;
		
		int NumPort;
		int Vitesse;
		int NbBitDonnee;
		int Parite;
		int NbBitStop;

		char buffer[100];
		int	bufferSize;

public:
		RS232(void);
		~RS232(void);
		
		void SetNumPort(int);
		void SetVitesse(int);
		void SetNbBitDonnee(int);
		void SetParite(int);
		void SetNbBitStop(int);

		int GetNumPort(void);
		int GetVitesse(void);
		int GetNbBitDonnee(void);
		int GetParite(void);
		int GetNbBitStop(void);
		
		char* Getbuffer(void);	
		
		void ConfigPort(int Num, int Vit, int Data , int Pair, int Stop);
		bool ViderBuffer(void);

		bool OuvrirPort(void);
		bool FermerPort(void);

		bool Ecrire(char* buf);
		const char* lire(int);
		
		string TraiterErreur(void);
};
