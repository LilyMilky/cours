/***************************************************************************************************
											---Tp Algo---
Programme Calcul Aire
Nom: Lacquehay Yann
Date: 10/09/09
****************************************************************************************************/
#include <iostream>						//inclure bibliotheque
using namespace std;
float longueur , resultat , largeur;		//Variable R�el

/****************************************************************************************************
										---Programme Principal---
****************************************************************************************************/
void main(void)							//Affichage Texte
{
	cout << "Veuillez saisir la valeur de la longueur:  ";
	cin  >>  longueur;
	cout << "\nVeuillez saisir la valeur de la largeur:  ";
	cin  >>  largeur;
	resultat = longueur * largeur;		//Calcul
	cout << "\nL'aire du rectangle est:  ";
	cout <<   resultat<<endl<<endl;
}