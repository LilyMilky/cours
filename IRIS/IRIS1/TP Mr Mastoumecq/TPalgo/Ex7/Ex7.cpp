/***************************************---Tp Algo---**********************************************
Programme Acquisition
Nom: Lacquehay Yann
Date: 10/09/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

#include "Code_Barre.h"

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	float identifiant;						
	unsigned short int code;

	cout << "Bienvenue dans la section BTS IRIS\n";		//Exercice1
	cout << "Pour acc\202der aux locaux il faudra:\n";
	cout << "\t-Passer votre badge ou saisir identifiant(10chiffres)\n";
	cout << "\t-Saisir votre code d'acces (4chiffres)\n";

	identifiant = LireBadge();							//Passage Badge
	cout << "\nTapez votre Code D'acces: ";
	cin  >> code;										

	if (identifiant == 1234512345 && code == 2008)
	{
		cout << "Acces Autoris\202\n";
	}
	else
	{
		cout << "Acces Refus\202\n";
	}

}



