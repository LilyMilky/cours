/***************************************---Tp Algo---**********************************************
Programme Ajout_Gardien
Nom: Lacquehay Yann
Date: 08/10/09 
**************************************************************************************************/
#include <iostream>	
#include<math.h>
using namespace std;

#include "Code_Barre.h"

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	float identifiant;						
	unsigned short int code;

	const float ID1 = 1234512345, ID2 = 1122334455, ID3 = 1166778899, IDGardien = 1111122222;
	const unsigned short int Code1 = 2008 ,Code2 = 1234 , Code3 = 5678, CodeGardien = 1122;

	cout << "Bienvenue dans la section BTS IRIS\n";			//Reprise Exercice1
	cout << "Pour acc\202der aux locaux il faudra:\n";
	cout << "\t-Passer votre badge ou saisir identifiant(10chiffres)\n";
	cout << "\t-Saisir votre code d'acces (4chiffres)\n\n";
	
	do
	{
		identifiant = LireBadge();							//Passage Badge
		if ( identifiant == ID1 || identifiant == ID2 || identifiant == ID3 || identifiant == IDGardien)
		{
			cout << "Veuillez saisir votre code: ";
			cin  >> code;									//Verification que l'identifiant et le code correspondent
			if ((code == Code1 && identifiant == ID1) || (code == Code2 && identifiant == ID2) || (code == Code3 && identifiant == ID3) )
			{
				cout << "Acces autoris\202\n\n";
			}
			else if ( identifiant == IDGardien && code == CodeGardien)
			{
				cout << "Identifiant et code gardien, arret du programme"<<endl;
			}
			else
			{
				cout << "Code Incorrecte, Acces refus\202\n\n";
			}
		}
		else
		{
			cout << "Identifiant Incorrecte, Acces refus\202\n\n";
		}
	}while ( ! (identifiant == IDGardien && code == CodeGardien));
}

