int parfait(int valeur)
{
   int facteur;          // Valeur testee pour la factorisation
   int somme = 1;        // Somme courante pour les facteurs trouves
   int max = valeur/2;   // Valeur maximale pour un facteur

   for (facteur = 2 ; facteur <= max ; facteur ++)
   {
      if (valeur%facteur == 0)
      {
         somme += (facteur + valeur/facteur); // on compatibilise la somme
         max = valeur/facteur-1;
      }
   }
   return (somme == valeur);
}