/***********************************---Tp Algo---*************************************
Programme Tri par permutation
Nom: Lacquehay Yann
Date: 14/09/09 
**************************************************************************************/

#include <iostream>						
using namespace std;

/****************************************************************************************************
										---Programme Principal---
****************************************************************************************************/

void main(void)		
{
	float Nbre1 , Nbre2 , Nbre3 , Var_Temp;
	
	cout << "Nombre1 ? :";
	cin  >> Nbre1;
	cout << "Nombre2 ? :";
	cin  >> Nbre2;
	cout << "Nombre3 ? :";
	cin  >> Nbre3;

	if(Nbre1 > Nbre2)					// Si Nombre1 > Nombre 2
	{									// Alors on echange les valeurs de
		Var_Temp = Nbre1;				// Nombre 1 et Nombre2
		Nbre1 = Nbre2;
		Nbre2 = Var_Temp;
	}
	if(Nbre2 > Nbre3)					// Si Nombre2 > Nombre3
	{									// Alors on echange les valeurs de
		Var_Temp = Nbre2;				// Nombre2 et Nombre3
		Nbre2 = Nbre3;
		Nbre3 = Var_Temp;
	}
//**********************************---Correction---**********************************
	if(Nbre1 > Nbre2)					// Ici on refait la comparaison entre
	{									// Nombre 1 et Nombre2 car sinon
		Var_Temp = Nbre1;				// Si Nombre1>Nombre2>Nombre3
		Nbre1 = Nbre2;					// On a Nombre1 > Nombre2
		Nbre2 = Var_Temp;
	}
//*********************************---Fin Correction**********************************
	cout << "Apres le tri, cela donne : ";
	cout << Nbre1;
	cout << " ";
	cout << Nbre2;
	cout << " ";
	cout << Nbre3 <<endl;
}