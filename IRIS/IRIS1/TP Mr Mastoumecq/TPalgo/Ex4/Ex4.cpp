/***********************************---Tp Algo---*************************************
Programme Comparateur 2 nombres
Nom: Lacquehay Yann
Date: 14/09/09 
**************************************************************************************/

#include <iostream>						
using namespace std;

/****************************************************************************************************
										---Programme Principal---
****************************************************************************************************/

void main(void)		
{
	float Nombre1 , Nombre2;

	cout << "Veuillez taper un nombre: ";					//Recuperation valeur
	cin  >> Nombre1;
	cout << "Veuillez taper un autre nombre: ";
	cin  >> Nombre2;

	if(Nombre1 > Nombre2)									// Si Nombre1 > Nombre2
	{
		cout << "Le nombre le plus grand est: ";			//Affiche Nombre 1
		cout << Nombre1 <<endl;
	}
	else													// Nombre1 < Nombre2
	{
		cout << "Le nombre le plus grand est: ";			//Affiche Nombre 2
		cout << Nombre2 <<endl;
	}
}