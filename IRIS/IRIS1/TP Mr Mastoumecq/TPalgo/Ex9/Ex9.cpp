/*****************************************---Tp Algo---*******************************************
Programme Affichage_n_ligne
Nom: Lacquehay Yann
Date: 24/09/09 
*************************************************************************************************/
#include <iostream>	
using namespace std;
/************************************************************************************************
									Programme Principal
*************************************************************************************************/			
void main(void)							
{
	int i, Nbre;

	cout << "Veuillez saisir le nombre de ligne que vous voulez afficher: ";
	cin  >> Nbre;

	i = 0;
	while (i != Nbre)
	{
		cout << i+1;
		cout << ") **********\n";

		i = i + 1;
	}
	
}