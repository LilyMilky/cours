/***************************************---Tp Algo---**********************************************
Programme Nombre_Parfait
Nom: Lacquehay Yann
Date: 08/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main (void)
{
	int Nombre, Diviseur, TotalDiviseur;
	const int NbreMax = 100;

	for (Nombre = 3; Nombre < NbreMax; Nombre++)
	{
		TotalDiviseur = 1;
		for (Diviseur = 2; Diviseur < Nombre; Diviseur ++)
		{
			if (Nombre % Diviseur == 0)
			{
				TotalDiviseur = TotalDiviseur + Diviseur;
			}
		}
		if (TotalDiviseur == Nombre)
		{
			cout << Nombre << " est un nombre parfait\n";
		}
 	}
}