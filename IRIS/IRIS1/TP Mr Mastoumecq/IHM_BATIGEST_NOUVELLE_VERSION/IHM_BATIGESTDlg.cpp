// IHM_BATIGESTDlg.cpp : fichier d'impl�mentation
//

#include "stdafx.h"
#include "IHM_BATIGEST.h"
#include "IHM_BATIGESTDlg.h"


/*******************************************************************************************************
* Ajout 
*******************************************************************************************************/
#include "Lecture.h"				//Pour pouvoir ouvrir la fen�tre de Lecture
#include "Ecriture.h"				//Pour pouvoir ouvrir la fen�tre de Ecriture
/******************************************************************************************************/

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// bo�te de dialogue CAboutDlg utilis�e pour la bo�te de dialogue '� propos de' pour votre application

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

// Impl�mentation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// bo�te de dialogue CIHM_BATIGESTDlg




CIHM_BATIGESTDlg::CIHM_BATIGESTDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIHM_BATIGESTDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIHM_BATIGESTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CIHM_BATIGESTDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_LECTURE, &CIHM_BATIGESTDlg::OnBnClickedLecture)
	ON_BN_CLICKED(IDC_ECRITURE, &CIHM_BATIGESTDlg::OnBnClickedEcriture)
END_MESSAGE_MAP()

// gestionnaires de messages pour CIHM_BATIGESTDlg

BOOL CIHM_BATIGESTDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Ajouter l'�l�ment de menu "� propos de..." au menu Syst�me.

	// IDM_ABOUTBOX doit se trouver dans la plage des commandes syst�me.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// D�finir l'ic�ne de cette bo�te de dialogue. L'infrastructure effectue cela automatiquement
	// lorsque la fen�tre principale de l'application n'est pas une bo�te de dialogue
	SetIcon(m_hIcon, TRUE);			// D�finir une grande ic�ne
	SetIcon(m_hIcon, FALSE);		// D�finir une petite ic�ne

	// TODO : ajoutez ici une initialisation suppl�mentaire

	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

void CIHM_BATIGESTDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Si vous ajoutez un bouton R�duire � votre bo�te de dialogue, vous devez utiliser le code ci-dessous
// pour dessiner l'ic�ne. Pour les applications MFC utilisant le mod�le Document/Vue,
// cela est fait automatiquement par l'infrastructure.

void CIHM_BATIGESTDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de p�riph�rique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'ic�ne dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'ic�ne
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Le syst�me appelle cette fonction pour obtenir le curseur � afficher lorsque l'utilisateur fait glisser
// la fen�tre r�duite.
HCURSOR CIHM_BATIGESTDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++		DEBUT DU CODAGE DES EVENEMENTS		+++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
* Lors d'un clic sur le bouton Lecture
* il faut ouvrir la fen�tre permettant d'effectuer
* les diff�rentes op�rations de Lecture sur un fichier.
******************************************************************************/
void CIHM_BATIGESTDlg::OnBnClickedLecture()
{
	// TODO: Add your control notification handler code here
	
	CLecture FenetreLecture;			//Cr�ation de la fen�tre mais non affich�e

	FenetreLecture.DoModal();			//Permet d'afficher la fen�tre
}

/******************************************************************************
* Lors d'un clic sur le bouton Ecriture
* il faut ouvrir la fen�tre permettant d'effectuer
* les diff�rentes op�rations d'Ecriture sur un fichier.
******************************************************************************/
void CIHM_BATIGESTDlg::OnBnClickedEcriture()
{
	// TODO: Add your control notification handler code here

	CEcriture FenetreEcriture;			//Cr�ation de la fen�tre mais non affich�e

	FenetreEcriture.DoModal();			//Permet d'afficher la fen�tre
}
