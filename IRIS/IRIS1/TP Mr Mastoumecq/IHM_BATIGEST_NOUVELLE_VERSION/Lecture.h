#pragma once
//#include "afxwin.h"


// CLecture dialog

class CLecture : public CDialog
{
	DECLARE_DYNAMIC(CLecture)

public:
	CLecture(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLecture();

// Dialog Data
	enum { IDD = IDD_LECTURE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	/*************************************************************************************************************
	* Pour acceder au diff�rents champs de saisie utilisateur
	**************************************************************************************************************
	* R�cup�ration de la valeur saisie par l'utilisateur dans l'attribut : UpdateData(1);
	* Envoie de la valeur de l'attribut dans le champ de saisie : UpdateData(0);
	*************************************************************************************************************/
	CString m_NomFichier;   // Nom du fichier tap� par l'utilisateur sur lequel il faut travailler
	CString m_TempMoy;		// Valeur de la temp�rature moyenne calcul�e pour une zone

	/*************************************************************************************************************
	* Pour Pouvoir r�cup�rer ce que tape l'utilisateur avec un attribut de type CEdit il faut :
	* D�clarer une autre variable de type CString =>	CString NumZone;
	* R�cup�ration dans NumZone de la valeur saisie par l'utilisateur : m_NumZone.GetWindowText(NumZone);
	* Envoie de la valeur contenu dans NumZone dans le champ de saisie : m_NumZone.SetWindowText(NumZone);
	*************************************************************************************************************/
	CEdit m_NumZone;		// Num�ro de la zone utilis�e choisi par l'utilisateur

	/*************************************************************************************************************
	* Pour travailler dans cette zone il faut utiliser
	* des m�thodes sp�cifiques de la classe CListBox
	*************************************************************************************************************/
	CListBox m_ZoneAffichage;

public:
	/*************************************************************************************************************
	* Liste des m�thodes ("fonctions membres") appel�es lors des clics sur les botuons
	**************************************************************************************************************/
	afx_msg void OnBnClickedLirefichier();
	afx_msg void OnBnClickedMesureszone();
	afx_msg void OnBnClickedTemperaturemoyenne();
};
