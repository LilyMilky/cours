#pragma once
#include "afxwin.h"


// CEcriture dialog

class CEcriture : public CDialog
{
	DECLARE_DYNAMIC(CEcriture)

public:
	CEcriture(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEcriture();

// Dialog Data
	enum { IDD = IDD_ECRITURE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedAjoutcomment();
	afx_msg void OnBnClickedFichierhtml();
	afx_msg void OnBnClickedSupzone();
	afx_msg void OnBnClickedInsererzone();
	afx_msg void OnBnClickedModifzone();

private:
	/*************************************************************************************************************
	* Pour acceder au différents champs de saisie utilisateur
	**************************************************************************************************************
	* Récupération de la valeur saisie par l'utilisateur dans l'attribut : UpdateData(1);
	* Envoie de la valeur de l'attribut dans le champ de saisie : UpdateData(0);
	*************************************************************************************************************/
	CString m_NomFichier;
	CString m_Commentaire;
	CString m_NumZone;
	CString m_Temperatures;

	CListBox m_ZoneAffichage;
	afx_msg void OnLbnDblclkAffichfichier();
};
