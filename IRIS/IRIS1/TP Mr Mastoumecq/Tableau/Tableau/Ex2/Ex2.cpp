/**************************************************************************************************
Programme AfficheVal
Nom: Lacquehay Yann
Date: 19/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	int Valeur[10] = {1 , 2 , 1 , 3 , 2 , 4 , 1 , 2 , 3 , 1};
	int Val, i , Nbre = 0;

	cout << "Veuillez saisir un nombre entre 1 et 5: ";
	cin  >> Val;
	for (i = 0; i < 10; i++)
	{
		if (Valeur[i] == Val)
		{
			Nbre = Nbre + 1;
		}
	}
	cout << "\nIl y a " << Nbre << " fois " << Val << " dans le tableau \n";
}