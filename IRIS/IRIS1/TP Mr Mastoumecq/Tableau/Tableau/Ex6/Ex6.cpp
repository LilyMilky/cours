/**************************************************************************************************
Programme Tri_Tableau
Nom: Lacquehay Yann
Date: 15/11/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/	

void main (void)
{
	int Tri[10];
	int i;
	void Tri_Tableau ( int Tab[10]);

	for (i = 0; i <10 ; i++)
	{
		cout << "Veuillez sasir la valeur de la case " << i+1 << " : ";
		cin  >> Tri[i];
	}
	cout << "\n			**** Tableau avant le tri ****\n";
	for (i = 0; i < 10; i++)
	{
		cout << Tri[i] << " ,";
	}
	Tri_Tableau (Tri);
	cout << "\n			**** Tableau apres le tri ****\n";
	for (i = 0; i < 10; i++)
	{
		cout << Tri[i] << " ,";
	}	
}

/*************************************************************************************************
										Fonction Tri_Tableau
*************************************************************************************************/	

void Tri_Tableau (int Tab[10])
{
	int i, x=0, Mem;

	for ( i = 0; i < 10; i++ )
	{
		if ( Tab[i] < 0)
		{
			Mem = Tab[x];
			Tab[x] = Tab[i];
			Tab[i] = Mem;
			x = x+1;
		}
	}
}