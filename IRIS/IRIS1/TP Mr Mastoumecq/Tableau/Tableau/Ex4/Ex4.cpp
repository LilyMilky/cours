/**************************************************************************************************
Programme Affiche_Intervalle
Nom: Lacquehay Yann
Date: 19/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	float Valeur[10] = {8 , 12 , 15 , 22 , 6 , 2 , 32 , 1 , 9 , 25};
	int i,Nbre = 0;
	float Val1, Val2;

	cout << "Veuillez saisir la 1ere valeur: ";
	cin  >> Val1;
	cout << "Veuillez saisir une 2nde valeur superieur a la 1ere: ";
	cin  >> Val2;
	for ( i = 0; i < 10 ; i++)
	{
		if ( Valeur [i] >= Val1 && Valeur [i] <= Val2)
		{
			Nbre = Nbre+1;
		}
	}
	cout << "Il y a: "<< Nbre << " valeur entre: " << Val1 << " et " << Val2 << endl;
}