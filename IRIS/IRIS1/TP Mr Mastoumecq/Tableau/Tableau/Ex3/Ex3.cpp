/**************************************************************************************************
Programme Val_Min_Max
Nom: Lacquehay Yann
Date: 19/10/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/			
void main(void)							
{
	int Valeur[10] = {7 , 11 , 3 , 20 , 6 , 2 , 1 , 5 , 15 , 8};
	int i , Nbremax = Valeur[0] , Nbremin = Valeur[0];
	
	for (i = 1; i < 10; i++)
	{
		if (Valeur[i] > Nbremax)
		{
			Nbremax = Valeur[i];
		}
		else if (Valeur[i] < Nbremin)
		{
			Nbremin = Valeur[i];
		}
	}
	cout << "La valeur maximale du tableau est: " << Nbremax << " et la valeur minimale est: " << Nbremin << endl;
}