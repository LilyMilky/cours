/**************************************************************************************************
Programme Nbre_Jour
Nom: Lacquehay Yann
Date: 15/11/09 
**************************************************************************************************/
#include <iostream>	
using namespace std;

/*************************************************************************************************
										Programme Principal
*************************************************************************************************/	

void main (void)
{
	int JourD, JourF, MoisD, MoisF, JourTT;
	int Nbre_Jour (int Jour1, int Mois1, int Jour2, int Mois2);

	cout << "Veuillez saisir le jour de debut: ";
	cin  >> JourD;
	cout << "Veuillez saisir le mois de debut en chiffre: ";
	cin  >> MoisD;
	cout << "Veuillez saisir le jour de fin: ";
	cin  >> JourF;
	cout << "Veuillez saisir le mois de fin en chiffre: ";
	cin  >> MoisF;
	JourTT = Nbre_Jour (JourD, MoisD, JourF, MoisF);
	cout << "Il y a: " << JourTT << " jours entre le " << JourD << "/" << MoisD << " et le " << JourF << "/" << MoisF << endl;
}

/************************************************************************************************
										Fonction Nbre_Jour
************************************************************************************************/

int Nbre_Jour (int Jour1, int Mois1, int Jour2, int Mois2)
{
	int i, Jours;
		int JourParMois[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	if (Mois1 == Mois2)
	{
		Jours = Jour2 - Jour1;
	}
	else
	{
		Jours = JourParMois [ Mois1 - 1] -Jour1;

		for ( i = Mois1; i < Mois2 -2; i++)
		{
			Jours = Jours + JourParMois[i];
		}
		Jours = Jours + Jour2;
	}
	return (Jours);
}