/******************************************************************************************************************
Lacquehay Yann
Programme Vote_Informatise
20/01/10
*******************************************************************************************************************/
#include "Utilitaires.h"
#include "LancerVote.h"
#include "VoirResultat.h"

#include <iostream>
using namespace std;

void main(void)
{
	//Variables
	int Choix, Num_Candidat, Nbre_Candidat = 3;
	char Nom[30], Prenom[30], DateNaissance[9];
	bool FichierOK;

		//Titre
		cout << "*******************************************************************************" <<endl;
		cout << "***************************** VOTE INFORMATISE ********************************" <<endl;
		cout << "*******************************************************************************" <<endl <<endl;
	do
	{
		//Menu
		cout << "\n1. Saisie des personnes eligibles" <<endl;
		cout << "2. Verifier la liste des personnes eligibles" <<endl;
		cout << "3. Lancer le vote" <<endl;
		cout << "4. Voir les resultats" <<endl;
		cout << "5. Quitter" <<endl <<endl;
		cout << "Votre Choix: ";
		cin  >> Choix;		//Choix Menu

		switch(Choix)
		{
				case 1 :
						cout << "\n**************************Saisie des personnes eligibles************************" <<endl <<endl;
						for (Num_Candidat = 1; Num_Candidat <= Nbre_Candidat; Num_Candidat ++)
						{
							cout << "Veuillez saisir le Nom du Candidat " << Num_Candidat  << ": ";  
							//Saisis du Nom
							cin  >> Nom;
							cout << "Veuillez saisir le Prenom du Candidat " << Num_Candidat << ": ";
							// Saisis du Prenom
							cin  >> Prenom;
							cout << "Veuillez saisir la Date de Naissance du Candidat " << Num_Candidat << "(JJ/MM/AA): "; 
							//Saisis de la Date de Naissance
							cin  >> DateNaissance;
							FichierOK = EnregistrerFichier(Num_Candidat, Nom, Prenom, DateNaissance);
							//Enregistre tous les candidats dans un fichier
							if (FichierOK == 0)						//Test de l'enregistrement
							{
								cout << "Erreur lors de l'enregistrement du fichier";
							}
						}
						break;

				case 2 :
						cout << "\n*********************Verifier la liste des personnes eligibles******************" <<endl <<endl;
						VerifierListePersonnes();					//Fonction Affichage des Candidats
						break;

				case 3  :
						cout << "\n********************************Lancer le vote*********************************" <<endl <<endl;
						LancerVote();								//Fonction Vote
						break;

				case 4 :
						cout << "\n********************************Voir les resultats*****************************" <<endl <<endl;
						VoirResultat (Nbre_Candidat);				//Fonction Resultat
						break;

				case 5 : break;

				default : cout << "\nMauvais Choix!" <<endl <<endl;	//Si Choix < 1 ou > 5
		}
	}while (!(Choix == 5));
}