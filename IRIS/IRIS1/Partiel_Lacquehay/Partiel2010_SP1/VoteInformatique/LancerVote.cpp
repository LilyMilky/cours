#include "Utilitaires.h"
#include "VerifierCode.h"
#include <iostream>
using namespace std;

/*******************************************************************************************************************
												Fonction LancerVote
*******************************************************************************************************************/

void LancerVote()
{
	//Variables
	int LectureOK, Lecture = 0, i, Code, ErreurCode, Choix, CodeOK;
	bool EnregistrementOK ;
	Badge Tab_Badge [20], BadgeRecupere;	

	for (i = 0; i < 20; i++)
	{
		ErreurCode = 0;
		LectureOK = LireBadge(&BadgeRecupere);								//Lecture Des Badges
		if (LectureOK == 0)
		{
			cout << "Erreur de Lecture du Badge";							//Test de la lecture du Badge
		}
		Tab_Badge[i] = BadgeRecupere;										//Sauvegarde des Badges
		cout << "Badge Numero " << i+1 << ": ";
		cout << Tab_Badge [i].Nom << " " <<Tab_Badge[i].Prenom<< " " << Tab_Badge[i].DateNaissance;
		cout << " " << Tab_Badge[i].Code <<endl;							//Affichage Des Badges
		do
		{
			cout << "Veuillez saisir votre Code: ";							//Saisis du Code
			cin  >> Code;
			CodeOK = VerifierCode(BadgeRecupere, Code);						//Verification du Code
			if (CodeOK == true)
			{
				cout << "Code Correct, Veuillez choisir un candidat dans la liste suivante" <<endl;
				AfficherListeCandidat();
				cin  >> Choix;
				EnregistrementOK = EnregistrerVote(Choix);					//Enregistrement du vote
				if (EnregistrementOK == 0)
				{
					cout << "Erreur lors de l'Enregistrement du Vote";		//Test de l'enregistrement du vote
				}
			}
			else
			{
				ErreurCode ++;												//Incrementation du nombre d'erreur
				cout << "Code Incorrect, Veuillez le ressaisir" <<endl;
				if (ErreurCode == 3)
				{
					cout << "3 Erreurs a la suite, Vote Nul";
					Choix = -1;												//Vote Nul si 3Erreurs
					EnregistrementOK = EnregistrerVote(Choix);
				}
			}
		}while(!(CodeOK == true || ErreurCode == 3)); 
	}
}