#include "Utilitaires.h"
#include <iostream>
using namespace std;

void VoirResultat (int Nbre_Candidat)
{
	//Variables
	int NB_Vote, TabResultat[20],i, VotesBlancs = 0,VotesNuls = 0, Votes1 = 0, Votes2 = 0, Votes3 = 0;

	NB_Vote = LireResultats(TabResultat);				//Lecture du Resultat
	cout << "Le Resultat: "; 
	for (i = 0; i < NB_Vote; i++)
	{
		cout << TabResultat[i] << "," ;
		if (TabResultat[i] == 0)
		{
			VotesBlancs ++;								//Comptage du nombre de vote blanc (= 0)
		}
		else if (TabResultat[i] < 0 || TabResultat[i] > 3)
		{
			VotesNuls ++;								//Comptage du nombre de vote nul ( < 1 ou > 3)
		}
		else if (TabResultat[i] == 1)
		{
			Votes1 ++;									//Comptage du nombre de vote pour DUPONT
		}
		else if(TabResultat[i] == 2)
		{
			Votes2 ++;									//Comptage du nombre de vote pour HENRY
		}
		else
		{
			Votes3 ++;									//Comptage du nombre de vote pour ROBIN
		}
	}
	cout << "\nIl y a "<< VotesNuls << " de votes nuls, "<< VotesBlancs << " de votes blancs.";
	cout << "Pour les candidats:" <<endl;
	cout << "DUPONT: " << Votes1 <<endl;				//Affichage des Resultats
	cout << "HENRY: " << Votes2 <<endl;
	cout << "ROBIN: " << Votes3 <<endl;
} 