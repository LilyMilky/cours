//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Type structur� nomm� "Badge" dont les champs sont :
// - le nom du porteur du badge
// - le pr�nom du porteur du badge
// - la date de naissance du porteur du badge
// - le code attribu� au porteur du badge
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
struct Badge
{
	char Nom[30];
	char Prenom[30];
	char DateNaissance[10];
	int Code;
};

/****************************************************************************
*	EnregistrerFichier()													*
*																			*
*	Cette fonction permet d'ajouter un candidat								*
*	dans le fichier "ListeCandidat.txt"										*
*																			*		
*	Valeur de retour :														*
*	0 si l'enregistrement ne s'est pas bien pass�							*
*	1 si l'enregistrement s'est bien pass�									*
*																			*
*	Cette fonction prend en param�tres :									*
*	- le num�ro du candidat													*
*	- le nom du candidat													*
*	- le pr�nom du candidat													*
*	- la date de naissance du candidat										*
*																			*
****************************************************************************/
bool EnregistrerFichier(int NumCandidat, char *Nom, char *Prenom, char *DateNaissance);

/****************************************************************************
*	VerifierListePersonnes()												*
*																			*
*	Cette fonction affiche la liste des personnes							*
*	qui se sont pr�sent�es pour cette �lection								*
*   qui viennent d'�tre enregistr�es dans le fichier "ListeCandidat.txt"	*		
*																			*
*	Elle ne prend pas de param�tre et ne renvoie pas de r�sultat			*
*																			*
****************************************************************************/
void VerifierListePersonnes(void);

/****************************************************************************
*	LireBadge()																*
*																			*
*	Cette fonction permet de lire le badge d'une personne					*
*	dans le fichier "ListeVotant.txt"										*
*																			*		
*	Valeur de retour :														*
*	0 si la lecture ne s'est pas bien pass�e								*
*	1 si la lecture s'est bien pass�										*
*	-1 quand tous les badges dans le fichier ont �t� lus					*
*																			*
*	Cette fonction prend en param�tre :										*
*	- l'adresse d'une structure de type Badge								*
*	  ce qui va permettre d'initialiser le badge							*
*	  avec les valeurs lues dans le fichier									*
*																			*
****************************************************************************/
int LireBadge(Badge *BadgeRecupere);

/****************************************************************************
*	AfficherListeCandidat()													*
*																			*
*	Cette fonction affiche la liste des candidats enregistr�s dans le		*
*	fichier "ListeCandidat.txt"	avec une mise en forme						*
*																			*
*	Elle ne prend pas de param�tre et ne renvoie pas de r�sultat			*
*																			*
****************************************************************************/
void AfficherListeCandidat(void);

/****************************************************************************
*	EnregistrerVote()														*
*																			*
*	Cette fonction permet d'enregistrer tous les votes						*
*	dans le fichier "Votes.txt"												*
*																			*		
*	Valeur de retour :														*
*	0 si l'enregistrement ne s'est pas bien pass�e							*
*	1 si l'enregistrement s'est bien pass�									*
*																			*
*	Cette fonction prend en param�tre :										*
*	- le num�ro du candidat pour qui la personne a vot�e					*
*																			*
****************************************************************************/
bool EnregistrerVote(int Choix);

/****************************************************************************
*	LireResultats()															*
*																			*
*	Cette fonction lit les r�sultats enregistr�s dans le fichier	 		*
*	"Votes.txt" et l'enregistre dans un tableau d'entiers					*
*																			*		
*	Elle renvoie en r�sultat le nombre de vote								*
*	Cette fonction prend en param�tre :										*
*	- l'adresse du tableau dans lequel seront stock�s les 					*
*	  r�sultats du vote														*
****************************************************************************/
int LireResultats(int *TabResultat);