/***************************************************************************************************************************
													Fonction MinMax
***************************************************************************************************************************/
#include <iostream>
using namespace std;

void rechercheMinMax(int notes[],int tailleNotes,int max,int min)
{
	int i;

	for ( i = 1; i < tailleNotes ; i++)
	{
		if (notes[i] < min)
		{
			min = notes[i];
		}
		else if (notes[i] > max)
		{
			max = notes[i];
		}
	}
	cout<<"la note maximale est "<<max<<endl;
	cout<<"la note minimale est "<<min<<endl;
}