/**************************************************************************************************************************
													Fonction Afficher Graphique
***************************************************************************************************************************/
#include <iostream>
using namespace std;

void afficherGraphique(int statistique[] ,const int taille, int NbMax)
{
	int i, j;

	for ( j = 1 ; j < 7 ; j++)
	{
		if (statistique[j] > NbMax)
		{
			NbMax = statistique[j];
		}	
	}

	for ( i = NbMax ; i > 0 ; i--)
	{
		cout << i << " >";
	}
	cout << "\n +-------+-------+-------+-------+-------+-------+-------+\n";
	cout << "    0a9    10a19   20a29  30a39    40a49   50a59    60\n";
}
