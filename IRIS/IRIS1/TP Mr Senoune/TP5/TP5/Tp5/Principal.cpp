/***************************************************************************************************************************
Lacquehay Yann
Graphique_Note
01/12/09
***************************************************************************************************************************/
#include <iostream>
using namespace std;
#include "stat.h"
#include "RechercheMinMax.h"
#include "afficherGraphique.h"

const int N=100;

void initNotes(int notes[],int taille);

void main()
{
	int notes[N];				/* tableau des notes */
	int tailleNotes;
	int statistique[7];			/* tableau des statistiques */
	
								/* Saisie des donn�es */
	cout <<" Entrez le nombre d eleves (max.100) : ";
	cin >> tailleNotes;
	if(tailleNotes >0 && tailleNotes <=100)
	{
		initNotes(notes,tailleNotes);
		int min = notes[0],max = notes[0];
	}
	else
	{
		cout <<"Erreur dans le choix du nombre d eleves";
		exit(0);
	}
	int min = notes[0],max = notes[0];
								/* Calcul et affichage du maximum et du minimum des points */
	rechercheMinMax(notes,tailleNotes,max,min);

								/* Statistique sur les notes �l�ves*/
	stat(notes,tailleNotes,statistique);
	int NbMax = statistique[0];
								/*affichage du graphique des statistiques*/
	afficherGraphique(statistique , 7, NbMax);
}

/*************************************************************************************************************************
													Fonction initNotes
**************************************************************************************************************************/

void initNotes(int notes[],int taille)
{
	int i, points;
	cout <<"Entrez les points des eleves:(compris entre 0 et 60)"<<endl;
	for (i=0; i<taille;i++)
	{
		cout<<"Eleve"<<i+1<<" ";
		cin >> points;
		notes[i]=points%61;
	}
	cout << endl;
}