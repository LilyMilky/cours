// classe: Nombres rationnels

#include <sstream>
using namespace std;
#include <stdlib.h>
#include <math.h>
#include "Rationnel.h"

// constructeur
Rationnel::Rationnel(long n, long d) 
{
	if (d == 0L) 
	{
		cerr << "Division par Zero" << endl;
		exit(1);
	}
	if (d < 0L) 
	{ 
		n = -n; d = -d; 
	}
	if (n == 0L) 
	{
		num = 0L; den = 1L;
	} 
	else 
	{
		long g = pgcd(n, d);
		num = n/g; den = d/g;
	}
}

const Rationnel Rationnel::operator+(const Rationnel& rhs)
{
	Rationnel Res ;
	Res = *this ;
	Res += rhs ;
	return Res;
}

const Rationnel Rationnel::operator-(const Rationnel& rhs) 
{
	Rationnel Res(-rhs.numerator(),rhs.denominator()) ;
	Res += *this ;
	return Res;
}

const Rationnel Rationnel::operator*(const Rationnel& rhs)
{
	return *this *= rhs;
}

const Rationnel Rationnel::operator/(const Rationnel& rhs) 
{
	return *this /= rhs;
}

const Rationnel& Rationnel::operator+=(const Rationnel& rhs) 
{
	long g1 = pgcd(den, rhs.den);
	if (g1 == 1L) 
	{					// 61% probability!
		num = num*rhs.den + den*rhs.num;
		den = den*rhs.den;
	} 
	else
	{
		long t = num * (rhs.den/g1) + (den/g1)*rhs.num;
		long g2 = pgcd(t, g1);
		num = t/g2;
		den = (den/g1) * (rhs.den/g2);
	}	
	return *this;
}

const Rationnel& Rationnel::operator+=(long rhs)
{
	num = num + den*rhs;
	return *this;
}

const Rationnel& Rationnel::operator-=(const Rationnel& rhs) 
{
	long g1 = pgcd(den, rhs.den);
	if (g1 == 1L) 
	{					// 61% probabilité!
		num = num*rhs.den - den*rhs.num;
		den = den*rhs.den;
	} 
	else
	{
		long t = num * (rhs.den/g1) - (den/g1)*rhs.num;
		long g2 = pgcd(t, g1);
		num = t/g2;
		den = (den/g1) * (rhs.den/g2);
	}
	return *this;
}

const Rationnel& Rationnel::operator-=(long rhs)
{
	num = num - den*rhs;
	return *this;
}

const Rationnel& Rationnel::operator*=(const Rationnel& rhs)
{
	long g1 = pgcd(num, rhs.den);
	long g2 = pgcd(den, rhs.num);
	num = (num/g1) * (rhs.num/g2);
	den = (den/g2) * (rhs.den/g1);
	return *this;
}

const Rationnel& Rationnel::operator*=(long rhs)
{
	long g = pgcd(den, rhs);
	num *= rhs/g;
	den /= g;
	return *this;
}

const Rationnel& Rationnel::operator/=(const Rationnel& rhs)
{
	if (rhs == 0) 
	{
		cerr << "Division par Zero" << endl;
		exit(1);
	}
	long g1 = pgcd(num, rhs.num);
	long g2 = pgcd(den, rhs.den);
	num = (num/g1) * (rhs.den/g2);
	den = (den/g2) * (rhs.num/g1);
	if (den < 0L) 
	{ 
		num = -num; den = -den; 
	}
	return *this;
}

const Rationnel& Rationnel::operator/=(long rhs)
{
	if (rhs == 0L)
	{
		cerr << "Division par Zero" << endl;
		exit(1);
	}
	long g = pgcd(num, rhs);
	num /= g;
	den *= rhs/g;
	if (den < 0L) 
	{
		num = -num; den = -den;
	}
	return *this;
}

// incrementation/decrementation
const Rationnel& Rationnel::operator++() 
{
	return (*this += 1);
}

const Rationnel Rationnel::operator++(int)
{
	Rationnel oldVal = *this;
	++(*this);
	return oldVal;
}

const Rationnel& Rationnel::operator--()
{
	return (*this -= 1);
}

const Rationnel Rationnel::operator--(int) 
{
	Rationnel oldVal = *this;
	--(*this);
	return oldVal;
}

// ............................
long Rationnel::pgcd(long u, long v)
{
	long a = labs(u);
	long b = labs(v);
	long tmp;
	if (b > a)
	{
		tmp = a; a = b; b = tmp;
	}
	for(;;)
	{
		if (b == 0L)
		{
			return a;
		}
		else if (b == 1L)
		{
			return b;
		}
		else 
		{
			tmp = b; b = a % b; a = tmp;
		}
	}
}

// operateur de comparaison
bool operator==(const Rationnel& lhs, const Rationnel& rhs) 
{
	return (lhs.numerator() == rhs.numerator() &&
	lhs.denominator() == rhs.denominator());
}