#ifndef Rationnel_H
#define Rationnel_H
#include <iostream>
using namespace std;

// classe: Nombres rationnels
class Rationnel {
public:
// ............
Rationnel() : num(0), den(1) {}
// ...................
Rationnel(long n, long d = 1);

// constructeur de copie
Rationnel(const Rationnel& rhs) : num(rhs.num), den(rhs.den) {}
// ..................
~Rationnel(void) {}

// accesseur
long numerator(void) const { return num; }
long denominator(void) const { return den; }

// operateurs d'assignements
Rationnel& operator=(const Rationnel& rhs);
Rationnel& operator=(long rhs);

// operateurs unaires
Rationnel operator+(void) const { return *this; }
Rationnel operator-(void) const { return Rationnel(-num, den); }
Rationnel invert(void) const { return Rationnel(den, num); }

const Rationnel operator+(const Rationnel& rhs);
const Rationnel operator-(const Rationnel& rhs);
const Rationnel operator*(const Rationnel& rhs);
const Rationnel operator/(const Rationnel& rhs);
// .................
const Rationnel& operator+=(const Rationnel& rhs);
const Rationnel& operator-=(const Rationnel& rhs);
const Rationnel& operator*=(const Rationnel& rhs);
const Rationnel& operator/=(const Rationnel& rhs);
const Rationnel& operator+=(long rhs);
const Rationnel& operator-=(long rhs);
const Rationnel& operator*=(long rhs);
const Rationnel& operator/=(long rhs);

// incrementation/decrementation
const Rationnel& operator++();
const Rationnel operator++(int);
const Rationnel& operator--();
const Rationnel operator--(int);

private:
// .................
long num; // numerateur
long den; // denominateur
// .......................
long pgcd(long, long);
};
// ...................
inline Rationnel& Rationnel::operator=(const Rationnel& rhs) {
num = rhs.num;
den = rhs.den;
return *this;
}
inline Rationnel& Rationnel::operator=(long rhs) {
num = rhs;
den = 1;
return *this;
}
// ..............
bool operator==(const Rationnel& lhs, const Rationnel& rhs);
#endif