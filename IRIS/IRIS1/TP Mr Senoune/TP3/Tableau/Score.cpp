
#include <iostream>
#include <stdlib.h>

using namespace std ;

const int maxVisiteurs = 30 ; 
int scores[maxVisiteurs];
int nbVisiteurs ,Choix,NV;

void AfficherScores (int nbVisiteurs);
void saisieScores (int NV) ;
int saisiescorealeatoire (int NV);
void Moyenne (int NV);
void MaxMin (int NV);

void main(void)
{
	cout <<"Saisir le nombre de visiteurs (30 maxi): ";
	cin >> nbVisiteurs;
	if (nbVisiteurs <= maxVisiteurs)
	{
		saisiescorealeatoire (NV);
	}
	cout << "Voulez vous afficher les scores? 1 oui ,autre chiffres sinon: ";
	cin  >> Choix;
	if (Choix == 1)
	{
		AfficherScores (nbVisiteurs);
	}
	cout << "\nVoulez vous afficher la moyenne? 2 oui, autre chiffres sinon: ";
	cin >> Choix;
	if (Choix == 2)
	{
		Moyenne (NV);
	}
	cout << "\nVoulez vous voir le score max et le score min?, 3 oui, autre chiffre sinon: ";
	cin >> Choix;
	if (Choix == 3)
	{
		MaxMin (NV);
	}
}
/*****************************************************************************************************************************
											Fonction Saisie Score
*****************************************************************************************************************************/
void saisieScores (int NV)
{
	int i, numV;
	
	NV = nbVisiteurs;
	numV = 0;

	cout << "\nLe nombre de visiteur est: " << NV;
	for (i = 0; i <= NV - 1  ; i++)
		{
			numV = numV + 1;
			cout << "\n\nVeuillez saisir le score du visiteur numero " << numV << ": ";
			cin  >> scores[i];
		}
}
/*****************************************************************************************************************************
											Fonction Afficher Score
*****************************************************************************************************************************/
void AfficherScores (int nbVisiteurs)
{
	int score, numV;

	for (numV = 0; numV <= nbVisiteurs-1; numV++)
	{
		cout << "Les scores du visiteur n " << numV+1 << " est: ";
		score = scores[numV];
		cout << score << endl ;
	}
}

/*****************************************************************************************************************************
											Fonction Saisie score aleatoire
*****************************************************************************************************************************/
int saisiescorealeatoire (int NV)
{
	int i;
	NV = nbVisiteurs;

	cout << "\nLe nombre de visiteur est: " << NV <<endl;
	for (i = 0; i <= NV - 1  ; i++)
	{
		scores[i] = rand() % 20;
	}
	return scores[maxVisiteurs];
}

/*****************************************************************************************************************************
												Fonction Moyenne
*****************************************************************************************************************************/

void Moyenne (int NV)
{
	int i;
	float Somme,Moyenne;	
	NV = nbVisiteurs;
	Somme = 0;

	for (i = 0; i <= NV - 1;i++)
	{
		Somme = Somme + scores[i];
	}
	Moyenne = Somme / NV;
	cout << "La moyenne des scores est : " << Moyenne << " /20";
}

/*****************************************************************************************************************************
												Fonction Score Mini/Moyen/Max
*****************************************************************************************************************************/

void MaxMin (int NV)
{
	int i, Mini, Max;
	NV = nbVisiteurs;
	Mini = scores[0];
	Max = scores[0];

	for ( i = 1 ; i <= NV - 1 ; i++)
	{
		if (scores[i] < Mini)
		{
			Mini = scores[i];
		}
		else if (scores[i] > Max)
		{
			Max = scores[i];
		}
	}
	cout << "\nLa valeur max est: " << Max << " et la valeur mini est: " << Mini <<endl;
}