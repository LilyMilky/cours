#include "Rationnel.h"
#include <iostream>
using namespace std;

Rationnel::Rationnel(int x , int y)
{
	Numerateur = x;
	Denominateur = y;
}

Rationnel::~Rationnel(void)
{
}

void Rationnel::Affiche(void)
{
	cout << Numerateur << "/" << Denominateur <<endl;
}

void Rationnel::Convertir(void)
{
	float Ratio, Num, Deno;
	Num = Numerateur;
	Deno = Denominateur;
	Ratio = Num / Deno;
	cout << "Le rationnel est: " << Ratio <<endl;
}
