#include "Rationnel.h"
#include <iostream>
using namespace std;

Rationnel::Rationnel(void)
: Numerateur(0)
, Denominateur(0)
{
	cout << "Veuillez saisir le numerateur: ";
	cin  >> Numerateur;
	cout << "Veuillez saisir le denominateur: ";
	cin  >> Denominateur;
}

Rationnel::~Rationnel(void)
{
}

void Rationnel::Affiche(void)
{
	cout << Numerateur << "/" << Denominateur <<endl;
}

void Rationnel::Convertir(void)
{
	float Ratio, Num, Deno;
	Num = Numerateur;
	Deno = Denominateur;
	Ratio = Num / Deno;
	cout << "Le rationnel est: " << Ratio <<endl;
}
