/***********************************************************************************************************************************
Yann Lacquehay
20/10/09
Tension / Pression
************************************************************************************************************************************/
#include "C:\Documents and Settings\Administrateur\Bureau\TP4\TP4\acquisition.h"
#include <iostream>
using namespace std;

	const int NbCase = 100;
	float RechercheMinMax (double mesures[NbCase], int x);
	float CalcMoyenne (double mesures[NbCase]);
	float PressionBar(double Tension_mvolt, double & P);

/*********************************************************************************************************************************
												Programme Principal
**********************************************************************************************************************************/
void main(void)
{

	double mesures[NbCase];
	const int L = 2, taille = 7, Taille = NbCase;
	float Moy,Tmvolt = 30;
	double Pres, Pression_Bar, Min, Max;
	int stat[7] = {0,0,0,0,0,0,0};
	int i, min = 8, x;

	InitAcquisition();
	for (i = 0; i < NbCase ; i++)
	{
		Acquisition (mesures, Taille);
	}

	x = 0;
	Min = RechercheMinMax ( mesures, x );
	x = 1;
	Max = RechercheMinMax ( mesures, x );

	cout << "Le min est: " << Min << " et le max est: " << Max << endl;
	Moy = CalcMoyenne ( mesures );
	cout << "La moyenne est: " << Moy << endl;
	Pression_Bar = PressionBar(Tmvolt, Pres);
	cout << "La pression en bar est: " << Pression_Bar << endl ;
	for (i = 0; i < NbCase ; i++)
	{
		if (mesures[i] >= 8 && (mesures[i] < 10))
		{
			stat[0] = stat[0] +1;
		}
		else if (mesures[i] >= 10 && (mesures[i] < 12))
		{
			stat[1] = stat[1] +1;
		}
		else if (mesures[i] >= 12 && (mesures[i] < 14))
		{
			stat[2] = stat[2] +1;
		}
		else if (mesures[i] >= 14 && (mesures[i] < 16))
		{
			stat[3] = stat[3] +1;
		}
		else if (mesures[i] >= 16 && (mesures[i] < 18))
		{
			stat[4] = stat[4] +1;
		}
		else if (mesures[i] >= 18  && (mesures[i] < 20))
		{
			stat[5] = stat[5] +1;
		}
		else if (mesures[i] >= 20 && (mesures[i] < 22))
		{
			stat[6] = stat[6] +1;
		}
	}
	AfficheResultat(stat , taille, L , min , Moy) ;
}

/**********************************************************************************************************************************
												Fonction Recherche MinMax
***********************************************************************************************************************************/
float RechercheMinMax (double mesure[NbCase],int x)
{
	int i;
	float Min = mesure[0], Max = mesure[0];
	
	if (x == 0)
	{
		for (i = 1; i <= NbCase; i++)
		{
			if (mesure[i] < Min)
			{
				Min = mesure[i];
			}
		}
		return Min;
	}
	else
	{
		for (i = 1; i <= NbCase; i++)
		{
			if (mesure[i] > Max)
			{
				Max = mesure[i];
			}
		}
		return Max;
	}
}

/**********************************************************************************************************************************
											Fonction Calcul Moyenne
***********************************************************************************************************************************/

float CalcMoyenne (double mesures[NbCase])
{
	int i;
	double Somme = 0;
	float Moyenne;

	for (i = 0; i < NbCase ; i++)
	{
		Somme = Somme + mesures[i];
	}
	Moyenne = Somme / NbCase;
	return Moyenne;
}

/************************************************************************************************************************************
											Fonction Pression Bar
************************************************************************************************************************************/

float PressionBar(double Tension_mvolt, double & P)
{
	float Pression_Bar;

	P = TensionToPression(Tension_mvolt);
	Pression_Bar = P / 1000;
	return Pression_Bar;
}

