#if !defined(ACQUISITION_H_INCLUDED_)
#define ACQUISITION_H_INCLUDED_

void Acquisition (double Mesures[100] , int taille );
void Caracteristique(double mbar[60] , double mvolt[60]);
double TensionToPression(double tmvolt);
// stat en mbar , moyenne en bar
void AfficheResultat(int stat[60],int taille,double L,double Pmini,double moyenne);
void InitAcquisition();

#endif 