#include <iostream>
using namespace std;

bool Test_Interval(int & valeur)
{
	bool ret ;
	if ((valeur > 20 ) && ( valeur < 100)) 
	{
		ret = true;
	}
	else 
	{
		ret = false ;
	}
	valeur = 50 ;
	return ret ;
}

void main(void)
{
	bool flg ;
	int x ;
	cout << "Entrer x : " ;
	cin >> x ;
	flg = Test_Interval(x) ;
	cout << "La valeur x saisie est : " << x << endl ;

	if ( flg == true )
	{
		cout << "x est dans l intervalle ]20 , 100[ \n" ;
	}
	else
	{
		cout << "x est en dehors de l intervalle ]20 , 100[ \n" ;
	}
}
