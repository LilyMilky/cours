struct Pilote
{
	char Nom[50];
	char Prenom[50];
	int NumKart;
	int Temps;
};

void saisiePilote(Pilote TableauPilote[10],int maxPilote);
void afficherPilote(Pilote TableauPilote[10], int maxPilote);
void afficherPiloteDetail(Pilote TableauPilote[10], int maxPilote);

void qualif(Pilote TableauPilote[10], int maxPilote);
void afficheClassement(Pilote TableauPilote[10], int maxPilote);
void afficheClassementClasse (Pilote TableauPilote[10], int maxPilote);
