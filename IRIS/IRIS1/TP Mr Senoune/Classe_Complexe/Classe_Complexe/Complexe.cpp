#include "Complexe.h"
#include <math.h>

Complexe::Complexe(void)
: Reel(0)
, Imaginaire(0)
, Module(0)
, Argument(0)
, ReelAdd(0)
, ImaginaireAdd(0)
, ReelMult(0)
, ImaginaireMult(0)
{
}

Complexe::~Complexe(void)
{
}

void Complexe::Add(Complexe Z2)
{
	ReelAdd = Reel + Z2.Reel;
	ImaginaireAdd = Imaginaire + Z2.Imaginaire;
}

void Complexe::Mult(Complexe Z2)
{
	ReelMult = (Reel * Z2.Reel) - (Imaginaire * Z2.Imaginaire);
	ImaginaireMult = (Reel * Z2.Imaginaire) + (Z2.Reel * Imaginaire);
}

void Complexe::Mod(void)
{
	Module = sqrt((Reel*Reel) + (Imaginaire*Imaginaire));	//sqrt = racine carr�
}

void Complexe::Argu(void)
{
	Argument = atan2 (Reel,Imaginaire);						//atan2 = Arctan
}

Complexe::Complexe(float x, float y)
{
	Imaginaire = y;
	Reel = x;
}

void Complexe::ModifZ1(void)
{
	Reel = 10;
	Imaginaire = 50;
}

void Complexe::ModifMultiplicationZ1(void)
{
	Reel = 5;
	Imaginaire = 3;
}

void Complexe::ModifMultiplicationZ2(void)
{
	Reel = 10;
	Imaginaire = 6;
}
