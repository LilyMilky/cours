#include "Complexe.h"
#include <iostream>
using namespace std;

void main(void)
{
	Complexe Z1(10,10);
	cout << "Z1: " <<Z1.Reel << " + " << Z1.Imaginaire<< "i" <<endl;
	Z1.Mod();
	cout << "Module Z1 : " << Z1.Module <<endl;
	Z1.Argu();
	cout << "Argument Z1 : " << Z1.Argument << endl;
	Z1.ModifZ1();
	cout << "Z1 modifie: " <<Z1.Reel << " + " << Z1.Imaginaire << "i" <<endl;
	Complexe Z2(10,10);
	cout << "Z2: " << Z2.Reel << " + " << Z2.Imaginaire<<"i" <<endl;
	Z1.Add(Z2);
	cout << "Z1 + Z2: " <<Z1.ReelAdd << " + " << Z1.ImaginaireAdd << "i" <<endl;
	Z1.ModifMultiplicationZ1();
	cout << "Modification pour multiplication Z1: " << Z1.Reel << " + " << Z1.Imaginaire << "i" << endl;
	Z2.ModifMultiplicationZ2();
	cout << "Modification pour multiplication Z2: " << Z2.Reel << " + " << Z2.Imaginaire << "i" << endl;
	Z1.Mult(Z2);
	cout << "Z1 * Z2: " <<Z1.ReelMult << " + " << Z1.ImaginaireMult << "i" <<endl;
}