/*******************************************************************
Nom de la fonction : Felicitation()
Description : compare la moyenne pass�e en param�tre � 10
Valeur de retour et son type : true si moy>=10 et false sinon
Param�tres de la fonction : la moyenne de type float
********************************************************************/
#include "fctfelicit.h"


bool Felicitation(float moy)
{
bool bien=true;
if(moy>=10.0)
	return(bien);
else
	return(!bien);	
}
