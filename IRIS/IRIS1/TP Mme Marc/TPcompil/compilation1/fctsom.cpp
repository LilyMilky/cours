/*******************************************************************
Nom de la fonction : Somme3()
Description : fait la somme de 3 notes pass�es en param�tres
Valeur de retour et son type : la somme de type float
Param�tres de la fonction : 3 notes de type float
********************************************************************/
#include "fctsom.h"


float Somme3(float note1,float note2,float note3)
{
float somme=0;
somme=note1+note2+note3;
return (somme);
}