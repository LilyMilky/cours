/*******************************************************************
Nom de la fonction :  Moyenne()
Description : calcule la moyenne, � partir d'une somme de notes
et du nbre de notes pass�s en param�tres
Valeur de retour et son type : la moyenne de type float
Param�tres de la fonction : la somme de notes et le nbre de notes
********************************************************************/
#include "fctsom1.h"
#include "fctmoy1.h"

float Moyenne(float note1, float note2, float note3)
{
float moy=0;
float som=0;
som=Somme3(note1,note2,note3);
moy=som/3;
return (moy);
}
