/********************************************************************
* Nom : pgmoy.cpp                                                   *
* Description : programme calculant la moyenne de 3 notes           *
*                                                                   *
* Auteur : V.MARC                                                   *
* S�ance de TP - Septembre 2005 - Fichier C++ � compiler            *
* ------------------------------------------------------------------*
* Compilation => Ligne de commande :                                 *
*                               � d�terminer!!!                     *
********************************************************************/
#include <iostream>
using namespace std;
#include "fctmoy1.h"

int main(void)
{
float average=0;
float mark1,mark2,mark3;

//saisie des 3 notes au clavier
cout<<"Veuillez saisir les 3 notes : "<<endl;

cin>>mark1>>mark2>>mark3;


//calcul de la moyenne des 3 notes
average=Moyenne(mark1,mark2,mark3);


//affichage � l'�cran de la moyenne calcul�e
cout<<"\t La moyenne est : "<<average<<endl;
}