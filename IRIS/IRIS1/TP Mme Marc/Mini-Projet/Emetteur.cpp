/************************************************************************************
Yann Lacquehay
Emetteur
************************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <iostream>
using namespace std;

int main (void)
{
  int fd, val_ret,Taille;
  char Commande[50];

  fd = open ("/dev/ttyS0", O_WRONLY);	//Ouverture port serie
  if (fd == -1)				// Message si erreur ouverture port
  {
    cout << "Echec ouverture du port serie";
  }
  else
  {
    do
    {
      cin.getline(Commande, 50, '\n');   // Saisis en prenant en compte espace + enter
      Taille = strlen(Commande);		// Mesure de Commande
      val_ret = write (fd, Commande, Taille);
    if (val_ret != Taille)
    {
      cout << "Erreur ecriture sur le port serie";
    }
    }while (!(Taille < 1));		// Arret si Taille = 0 (Enter)
  }
    close (fd);
}