/************************************************************************************
Yann Lacquehay
Emetteur/Recepteur
************************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <iostream>
using namespace std;

int main (void)
{
  int fd, val_ret,Taille, i = 0;
  char Commande[51],Saisie[5], Car;

  fd = open ("/dev/ttyS0", O_RDWR);	// Ouverture port serie
  if (fd == -1)				// Message si erreur ouverture port
  {
    cout << "Echec ouverture du port serie";
  }
  else
  {
    do
    {
      cin.getline(Saisie, 5, '\n');     // Saisis en prenant en compte espace + enter
      Taille = strlen(Saisie);		// Mesure de Commande
      val_ret = write (fd, Saisie, 1);

      if (val_ret != 1)
      {
	cout << "Erreur ecriture sur le port serie";
      }

      else if (Saisie[0] == 'P')
      {
	  
	  cin.getline(Saisie, 5, '\n');
	   val_ret = write (fd, Saisie, 1);
      }

	 
	 if (Saisie[0] == 'Q')
	  {
	    do
	   {
	     val_ret = read (fd, &Car, 1);		//Lecture Recepteur
	     cout << Car;
	     Commande[i] = Car;
	     i = i+1;
	   }while (Commande[i] == 0x46);
	    Commande[i+1] = '\0';
	    cout << Commande ;
	  }
      }
     }while (!(Taille < 1));			// Arret si Taille = 0 (Enter)
  }
  close (fd);
}