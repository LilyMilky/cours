/*********************************************************************************************************************
Yann Lacquehay
Communication Shell Shell
*********************************************************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <iostream>
using namespace std;

int main (void)
{
  int fd, val_ret, Taille;
  char Commande[50];
 
  fd = open ("/dev/pts/2", O_WRONLY);
  do
  {
    cin.getline(Commande, 50, '\n');   // Saisis en prenant en compte espace + enter
    Taille = strlen(Commande);		// Mesure de Commande
    cout << Taille << "est la taille \n";
    val_ret = write (fd, Commande, Taille);
  }while (!(Taille < 1));		// Arret si Taille = 0 (Enter)

  close (fd);
}