/************************************************************************************
Yann Lacquehay
Communication Recepteur
************************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <iostream>
using namespace std;


int main (void)
{
  int fd, val_ret;
  char Commande;

  fd = open ("/dev/ttyS0", O_RDWR);
  do
  {
    val_ret = read (fd, &Commande, 1);
    cout << Commande;
  }while (!( Commande == '0'));

  close (fd);
}