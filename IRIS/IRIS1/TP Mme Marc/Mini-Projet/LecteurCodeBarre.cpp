/************************************************************************************
Yann Lacquehay
Recepteur
************************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <iostream>
using namespace std;


int main (void)
{
  int fd, val_ret;
  char Commande;

  fd = open ("/dev/ttyS0", O_RDONLY);
  if (fd == -1)
  {
    cout << "Echec ouverture du port serie";	// Message si erreur ouverture port
  }
  else
  {
    do
    {
      val_ret = read (fd, &Commande, 1);	//Lecture Recepteur
      cout << Commande;
    }while (1);					//Boucle Infini
  }
}