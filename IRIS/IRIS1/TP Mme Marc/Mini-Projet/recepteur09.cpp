/**************************************************************************/
//Auteur : V.MARC
//Nom du fichier : recepteur.cpp
//Description : recoit, en mode NON CANONIQUE, sur le port serie RS232 tous
// 		les caracteres envoyes par l'autre �quipement et les affiche
//Date : 13/01/10
/*************************************************************************/
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <termios.h>
using namespace std;
#define BAUDRATE B9600

int main (void)
{
	int fd ,nbr;
	char car;
	int i;
	struct termios old_term,term;
	cout<<"******** Reception*************"<<endl;
	//ouverture du canal de communication avec le port serie
	fd = open("/dev/ttyS0",O_RDONLY);
	if(fd < 0)
	{
		cout <<"Probleme d'ouverture du port";
		exit(0);
	}

	tcgetattr(fd,&old_term);//sauvegarde des param?tre du port
	//configuration du port s?rie : MODE NON CANONIQUE
	//bzero(&term,sizeof(term));
	term.c_cflag=BAUDRATE|CS8|CLOCAL|CREAD;
	term.c_iflag=0;
	term.c_oflag=0;
	term.c_lflag=0;
	term.c_cc[VTIME]=0;
	term.c_cc[VMIN]=1;
	tcflush(fd,TCIFLUSH); //vide le tampon d'entr?e
	tcsetattr(fd, TCSANOW,&term);//applique la configuration
	//Boucle de lecture et d'affichage des caract?res re?us sur le port
	while(1)
	{
	nbr=read(fd,&car,1);
	cout<<nbr<<" : "<<car<<endl;
	}
	//restuaration des anciens param?tres du port
	tcsetattr(fd, TCSANOW,&old_term);
return(0);

}
