/****************************************************************************************
Nom: Lacquehay Yann                                                                     
Date: 05/05/2010                                                                       
Programme principal                                                                     
Version 1                                                                              
Fichier: maintest.cpp                                                                  
****************************************************************************************/
#include <string.h>
#include <iostream>
using namespace std;
#include "PortSerieLinux.h"

int main()
{
	char Rep1 [80], Chemin[15];
	int Retour,Choix;
	int Terminator;						// Variable indication si Terminateur atteint
	
	cout << "Veuillez saisir le chemin d'acces du port souhaité: ";
	cin  >> Chemin;
	CPortSerieLinux Port1(Chemin);			//instanciation d'un objet Port1 de la classe 									CPortSerieLinux
		
	do
	{
		cout << "Souhaitez vous allumer le ventilateur(Tapez 1), l'eteindre(Tapez 0) ou quitter le programme (Tapez n'importe quel autre chiffre)?: "<<endl;
		cin  >> Choix;
		if (Choix == 1)
		{	
			Retour = Port1.Send("#010001\r",8);	
			Terminator = Port1.Receive(Rep1, 80, '\r');		//Appel de la m�thode Send sur l'objet Port1 qui 								envoie une commande �El'adresse #01
		}
		else if (Choix == 0)
		{
			Retour = Port1.Send("#010000\r",8);
			Terminator = Port1.Receive(Rep1, 80, '\r');
		}
		else if (Choix ==9)
		{
			cout << "Baka Number 9!"<<endl;
		}
			int taille = strlen(Rep1);
			Rep1[taille] = '\0';

			cout << "Reponse Module 1 :" << Rep1 << endl <<endl;
			//Appel de la m�thode Receive sur l'objet Port1 qui r�cup�re une r�ponse qu'elle met dans la variable Rep1 jusqu'au caract�re terminateur '\r'
	}while(Choix == 1 || Choix == 0 || Choix == 9);
		
	
}