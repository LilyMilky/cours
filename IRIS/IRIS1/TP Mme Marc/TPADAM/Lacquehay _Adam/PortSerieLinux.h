//**********************************************************
// Classe C++ CPortSerieLinux
// Auteur : V.MARC
// Date : mars 2006
//**********************************************************

class CPortSerieLinux
{
private :
int fd;

public :
CPortSerieLinux(char *path);
int Send(char *Commande,int NbOct);
int Receive(char *Reponse,int RepMax, char Terminateur);
~CPortSerieLinux();
};

