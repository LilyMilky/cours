//**********************************************************
// Classe C++ CPortSerieLinux - fonctions membres
// Auteur : V.MARC
// Date : mars 2006
//**********************************************************

#include "PortSerieLinux.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

CPortSerieLinux::CPortSerieLinux(char *path)
{

/*ouverture d'un canal de communication avec le port serie*/

if ( (fd = open(path,O_RDWR)) < 0 )
		{ exit(1); }
/*configuration de la ligne serie*/
//A COMPLETER

}

int CPortSerieLinux::Send(char *Commande,int NbOct)
{

ssize_t	n;
/* Ecriture sur le port serie */
if ((n = write (fd, Commande, NbOct)) != NbOct)
	{ return(-1); }

return(0);
}

int CPortSerieLinux::Receive(char *Reponse,int RepMax, char Terminateur)
{
int i=0;

do
	read(fd, &Reponse[i++],1);
	
while((Reponse[i-1]!=Terminateur) && (i<RepMax));



if (Terminateur == '\r')
{
      Terminateur = '\0';
}



if (Reponse[i-1]==Terminateur) return(i);
else return (-1);
}

CPortSerieLinux::~CPortSerieLinux()
{
close(fd);
}