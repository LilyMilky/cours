//*******************************************************************************************/
TD/TP - Codage en C++ d'une association 

FICHIER d'AIDE METHODOLOGIQUE
V.MARC - avril 2010
//*******************************************************************************************/

1) Cr�ez un r�pertoire de travail. (ex : MON_Nom_ADAM)
2) Dans ce r�pertoire de travail, mettez les fichiers PortSerieLinux.o, PortSerieLinux.h, les 2 fichiers Adam4011.cpp et .h et maintest.cpp
3) Mettez � jour les directives du pr�processeur

=============================================================================================
LA CLASSE C++ CPortSerieLinux
=============================================================================================
4) Dans votre main() de test,instanciez la classe CPortserieLinux et appelez les fonctions membres :
- Send()
- Receive()
Testez alors la communication s�rie RS232 avec un �quipement de votre choix.

----------------------------------------------------------------------------
Rappel :
(Commande � envoyer � un module Adam4011 pour avoir la temp�rature : #AA(CR)
	 Ex : 	#01(CR) au module d'adresse 0x01
		#0B(CR) au module d'adresse 0xB
----------------------------------------------------------------------------

---------------------------------
Remarque : 3 instructions � coder
---------------------------------

=============================================================================================
LA CLASSE C++ Adam4011
=============================================================================================

5) Codez le constructeur de la classe Adam4011
6) Dans votre main() de test,instanciez la classe Adam4011
7) Compl�tez la fonction membre sequence() qui envoie une question au module et attend sa r�ponse
8) Dans votre main() de test,
	a) mettez en commentaire les appels aux fonctions membres Send() et Receive() de la 		classe CPortSerieLinux
	b) appelez sequence()
	 (Commande � envoyer � un module Adam4011 pour avoir la temp�rature : #AA(CR)
	 Ex : 	#01(CR) au module d'adresse 0x01
		#0B(CR) au module d'adresse 0xB

9) Compl�tez la fonction membre GetTemperature()
10) Dans votre main() de test, appelez GetTemperature()

---------------------------------
Remarque : 7 instructions � coder
---------------------------------
