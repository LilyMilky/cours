#include <iostream>
using namespace std;


/**************************************************************************************************
										Fonction Millier
**************************************************************************************************/
void millier ( int Millier)
{
	int i;

	for (i = 0; i < Millier; i++)
	{
		cout << "M";
	}
}
/**************************************************************************************************
										Fonction Centaine
**************************************************************************************************/
void centaine ( int Centaine)
{
	int i;

	if (Centaine == 4)
	{
		cout << "CD";
	}
	else if (Centaine > 4 && Centaine < 9)
	{
		cout << "D";
		for (i = 6 ; i <= Centaine ; i++)
		{
			cout << "C";
		}
	}
	else if (Centaine == 9)
	{
		cout << "CM";
	}
	else
	{
		for (i = 0; i < Centaine; i++)
		{
			cout << "C";
		}
	}
}
/**************************************************************************************************
										Fonction Dizaine
**************************************************************************************************/
void dizaine ( int Dizaine)
{
	int i;

	if (Dizaine == 4)
	{
		cout << "XL";
	}
	else if (Dizaine > 4 && Dizaine < 9)
	{
		cout << "L";
		for (i = 6 ; i <= Dizaine ; i++)
		{
			cout << "X";
		}
	}
	else if (Dizaine == 9)
	{
		cout << "XC";
	}
	else
	{
		for (i = 0; i < Dizaine; i++)
		{
			cout << "X";
		}
	}
}
/**************************************************************************************************
										Fonction Unite
**************************************************************************************************/
void unite ( int Unite)
{
	int i;

	if (Unite == 4)
	{
		cout << "IV";
	}
	else if (Unite > 4 && Unite < 9)
	{
		cout << "V";
		for (i = 6 ; i <= Unite ; i++)
		{
			cout << "I";
		}
	}
	else if (Unite == 9)
	{
		cout << "IX";
	}
	else
	{
		for (i = 0; i < Unite; i++)
		{
			cout << "I";
		}
	}
}