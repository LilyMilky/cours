/*************************************************************************************************
Lacquehay Yann
ChiffreRomain
10/12/09
**************************************************************************************************/
#include "Fonction.h"
#include <iostream>
using namespace std;

void main (void)
{
	int Val_saisie, Millier, Centaine, Dizaine, Unite;

	do
	{
		cout << "Veuillez saisir une valeur entre 1 et 4999: ";
		cin  >> Val_saisie;
	}while(!(Val_saisie >= 1 && Val_saisie <= 4999));

	Millier = Val_saisie / 1000;
	Val_saisie = Val_saisie % 1000;
	Centaine = Val_saisie / 100;
	Val_saisie = Val_saisie % 100;
	Dizaine = Val_saisie / 10;
	Val_saisie = Val_saisie % 10;
	Unite = Val_saisie;

	cout << "Le nombre romain est : \n";

	millier (Millier);
	centaine (Centaine);
	dizaine (Dizaine);
	unite (Unite);
	cout << "\n";
}