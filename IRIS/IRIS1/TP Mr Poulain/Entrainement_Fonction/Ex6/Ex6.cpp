/****************************************************************
* Nom : Ex5.cpp													*
* Description : Contr�le d'acc�s d'un batiment					*
*				Avec des Fonctions								*
* Auteur : JB Mastoumecq										*
* Proposition de solution										*
****************************************************************/
#include <iostream>		
using namespace std;	

/****************************************************************
* Definition du Prototype des fonctions							*
****************************************************************/
void Affichage(void);
void Acquerirbadge (void);
bool AutoriserAcces(double, unsigned short int);
/***************************************************************
					Variable Globale
****************************************************************/
double Identifiant;
unsigned short int CdeAcces;
/****************************************************************
* Programme Principal											*
****************************************************************/
void main(void)
{

	bool FinProg = false;
	bool Id_Code_Valide = false;

	do
	{
		Affichage();
		
		do
		{
			Acquerirbadge ();
			Id_Code_Valide = true;

			if (Identifiant < 1000000000 || Identifiant > 9999999999)
			{
				Id_Code_Valide = false;		
				cout << "\n !!!! Identifiant non valide (il faut 10 chiffres) !!!!" << endl;
			}
				
			if (CdeAcces <1000 || CdeAcces > 9999)
			{
				Id_Code_Valide = false;
				cout << "\n !!!! Code d'acces non valide (il faut 4 chiffres) !!!!" << endl;
			}

		}while(!(Id_Code_Valide == true ));

		FinProg = AutoriserAcces(Identifiant,CdeAcces);
		
	}while(!FinProg);

	cout << "\n  !!!! Il n'est plus posible d'acceder au batiment !!!!" << endl;
	cout << "        !!!! horaires d'ouverture : 9h - 19h !!!!" << endl;
}
/****************************************************************
* Sous-Programme : Fonction Affichage							*
****************************************************************/
void Affichage(void)
{
	cout << "Bienvenue en BTS IRIS\n";
	cout << "Pour acceder aux locaux, il faut: \n";
	cout << "\t-Passer votre BADGE ou saisir l identifiant (10chiffres)\n";
	cout << "\t-Saisir votre code d acces (4chiffres)\n";
}
/************************************************************************************************
		Sous-Programme : Fonction Acquerir Badge
************************************************************************************************/
void Acquerirbadge (void)
{
	cout << "Veuillez saisir votre identifiant: ";
	cin  >> Identifiant;
	cout << "Veuillez saisir votre code d acces: ";
	cin  >> CdeAcces;
}
/****************************************************************
* Sous-Programme : Fonction AutoriserAcces						*
****************************************************************/
bool AutoriserAcces(double Id, unsigned short int Cde)
{
	bool Arret_Prog = false;

	double ID1 = 1111111111, ID2 = 2222222222, ID3 = 3333333333, IDgard = 1234567123;
	unsigned short int MDP1 = 1111, MDP2 = 2222, MDP3 = 3333, MDPgard = 1234;

	if( (Id == ID1 && Cde == MDP1) || (Id == ID2 && Cde == MDP2) || (Id == ID3 && Cde == MDP3) )
	{
		cout << "Acces autorise\n";
	}
	else if ( Id == IDgard && Cde == MDPgard)
	{
		cout << "Code gardien, arret du programme\n";
		Arret_Prog = true;
	}
	else
	{
		cout << "Acces refuse\n";
	}
	return Arret_Prog;
}