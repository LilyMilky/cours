/*************************************************************************************************
Lacquehay Yann
15/10/09
Programme : Fonction
*************************************************************************************************/

#include <iostream>
using namespace std;

/************************************************************************************************
										Fonction Principale
************************************************************************************************/
void main (void)
{
	float Nbre1, Nbre2, Resultat;
	void Message();								//Declaration des fonctions
	void AffichResult(float Resultat);
	float Multiplication (float Nbre1 , float Nbre2);

	Message ();									//Affiche Message
	cin  >> Nbre1;
	Message ();
	cin  >> Nbre2;
	Resultat = Multiplication (Nbre1, Nbre2);	//Fait la Multiplication entre Nbre1 et Nbre2
	AffichResult(Resultat);						//Affiche Resultat
}
/*************************************************************************************************
										Fonction Message
*************************************************************************************************/
void Message()
{
	cout << "Tapez un nombre: ";
}
/***********************************************************************************************
										Fonction AffichResult
***********************************************************************************************/
void AffichResult(float Resultat)
{
	cout << "Le resultat est: " << Resultat << endl;
}
/************************************************************************************************
										Fonction Multiplication
************************************************************************************************/
float Multiplication (float Nbre1 , float Nbre2)
{
	return (Nbre1 * Nbre2);
}
