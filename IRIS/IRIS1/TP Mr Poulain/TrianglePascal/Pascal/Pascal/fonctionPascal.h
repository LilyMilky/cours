/****************************************************************
* Nom         : fonctionPascal                                  *
* Description : Fonctions necessaires a la construction         *
*               du triangle de Pascal.                          *
*                                                               *
* Auteur : Daniel POULAIN                                       *
*                                                               *
****************************************************************/
#ifndef _FONCTIONPASCAL_H
#define _FONCTIONPASCAL_H

#include <iostream>

using namespace std;

/****************************************************************
* Definition des prototypes                                     *
****************************************************************/
unsigned short int SaisieOperateur (void);
void AfficherTrianglePascal        (int *LigneTrianglePascal,    int LgLigneTriangle);
void CalculCoefficient             (int *T_source, int Lgsource, int *T_destination);
#endif
