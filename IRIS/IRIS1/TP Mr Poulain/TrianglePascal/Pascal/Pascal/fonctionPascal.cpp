/****************************************************************
* Nom         : fonctionPascal                                   *
* Description : Fonctions necessaires a la construction          *
*               du triangle de Pascal.                           *
*               Corps de programme.                              *
*                                                                *
* Auteur : Daniel POULAIN                                        *
*                                                                *
*****************************************************************/
#include <iostream>
using namespace std;

/****************************************************************
* Definition de la fonction SaisieOperateur                     *
****************************************************************/
unsigned short int SaisieOperateur (void) 
{
 
  // Declaration des variables
  unsigned short int RangTriangle;
   
  // Affichage de la presentation de construction
  // du triangle de Pascal.
  cout << endl;
  cout << "===================================" << endl;
  cout << "Construction du Triangle de Pascal " << endl;
  cout << endl;
  cout << "===================================" << endl;
  cout << "** Nombre representant le rang du triangle de Pascal " << endl;
  
  // Saisie du rang du triangle de Pascal
  do 
  {
     cout << "** Veuillez saisir le rang ? (entre 2 et 99)";
     cin  >> RangTriangle;  
  } while ( RangTriangle < 2 || RangTriangle > 99);
  
  return  RangTriangle;
};

/****************************************************************
* Definition de la fonction CalculCoefficient                   *
****************************************************************/
void CalculCoefficient (int *T_source, int Lgsource, int *T_destination)
{
    for ( int i = 0; i < Lgsource - 1; i++)
	{
        T_destination[i+1] = T_source[i] + T_source[i+1];
    }
    T_destination[Lgsource] = 1;
};

/******************************************************          **********
* Definition de la fonction AfficherTrianglePascal              *
****************************************************************/
void AfficherTrianglePascal (int *LigneTrianglePascal,  int LgLigneTriangle) 
{
   for (int i = 0; i < LgLigneTriangle; i++) 
   {   
      cout.width(4);
      cout << LigneTrianglePascal [i] << " ";
   }
   cout << endl;
};
