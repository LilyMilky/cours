/****************************************************************
* Nom         : TriangleDePascal                                *
* Description : Affichage du triangle de Pascal.                *
*                Programme Principal.                           *
* Contrainte  : La limite du rang du triangle de Pascal         *
*                 est egale a 99.                               *
*                                                               *
* Auteur      : Daniel POULAIN                                  *
*                                                               *
****************************************************************/
#include <iostream>
#include "fonctionPascal.h"

using namespace std;

/****************************************************************
* Programme principal                                           *
****************************************************************/
int main (void) {


  // Declaration des variables
  int T_LigneTrianglePascal_1 [100];
  int T_LigneTrianglePascal_2 [100];
  unsigned short int Rang;
 

  // Initialisation
  T_LigneTrianglePascal_1 [0] = 1;
  T_LigneTrianglePascal_2 [0] = 1;
 
  // Saisie du rang du triangle de Pascal
  Rang = SaisieOperateur();
  
  // Affichage de la presentation du programme
  AfficherTrianglePascal(T_LigneTrianglePascal_1, 1);
  
  /* Pour calculer les coefficients du triangle de Pascal 
     deux lignes sont necessaires :
       - La ligne du rang du triangle en cours de calcul,
       - La ligne du rang precedent la ligne courante. */
  for  (int n = 1; n <= Rang; n++) {
     // Pour faciliter les calculs, le tableau representant le
     // rang de la ligne courante et le tableau representant la ligne
     // precedente sont inverses une fois sur deux.    
     if (n % 2 != 0) {
        CalculCoefficient(T_LigneTrianglePascal_1, n, T_LigneTrianglePascal_2);
        AfficherTrianglePascal (T_LigneTrianglePascal_2, n+1);
     } else {
        CalculCoefficient(T_LigneTrianglePascal_2, n,T_LigneTrianglePascal_1);
        AfficherTrianglePascal (T_LigneTrianglePascal_1, n+1);
     }
  }
} 
