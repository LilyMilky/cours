/***************************************************************************************************
Yann Lacquehay
Fonction
17/12/09
***************************************************************************************************/
#include "Fonction.h"
#include <iostream>
using namespace std;

/***************************************************************************************************
											Programme Principal
***************************************************************************************************/
void main (void)
{
	char OpLeft[9], OpRight[9], Conversion[9] = {'0','0','0','0','0','0','0','0'};

	cout << "Veuillez saisir l'operande de gauche en binaire (un octet) : "; 
	cin  >> OpLeft;
	cout << "Veuillez saisir l'operande de droite en binaire (un octet) : ";
	cin  >> OpRight;
	FonctionET (OpLeft, OpRight, Conversion);
	cout << "Le resultat avec la fonction ET : " << Conversion << endl;
	FonctionOU (OpLeft, OpRight, Conversion);
	cout << "Le resultat de la fonction OU : " << Conversion << endl;
	FonctionOUEX (OpLeft, OpRight, Conversion);
	cout << "Le resultat de la fonction OU Exclusif : " << Conversion << endl;
}