/**************************************************************************************************
										Fonction ET
**************************************************************************************************/
void FonctionET (char OpLeft[], char OpRight[], char Conversion[])
{
	int i;

	for (i = 0; i < 8; i++ )
	{
		if (OpLeft[i] == '1' && OpRight[i] == '1')
		{
			Conversion[i] = '1';
		}
		else 
		{
			Conversion[i] = '0';
		}
	}
} 

/**************************************************************************************************
										Fonction OU
***************************************************************************************************/

void FonctionOU (char OpLeft[], char OpRight[], char Conversion[])
{
	int i;

	for (i = 0; i < 8; i++ )
	{
		if ((OpLeft[i] == '0' && OpRight[i] == '0'))
		{
			Conversion[i] = '0';
		}
		else 
		{
			Conversion[i] = '1';
		}
	}
}
/*************************************************************************************************
										Fonction Ou Exclusif
*************************************************************************************************/
void FonctionOUEX (char OpLeft[], char OpRight[], char Conversion[])
{
	int i;

	for (i = 0; i < 8; i++ )
	{
		if ((OpLeft[i] == '0' && OpRight[i] == '0') || (OpLeft[i] == '1' && OpRight[i] == '1'))
		{
			Conversion[i] = '0';
		}
		else 
		{
			Conversion[i] = '1';
		}
	}
}