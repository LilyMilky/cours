/********************************************************************************************************************
Lacquehay Yann
11/03/10
TP Chaines de caracteres
*********************************************************************************************************************/

#include "Menu.h"
#include "codageTrame.h"
#include "decodageTrame.h"
#include <iostream>
using namespace std;

void main(void)
{
	int Menu();

	int Choix;
	char Nom[30];
	char Prenom[30];
	char Annee[5];
	char Jour[3];
	char Mois[3];
	char Heure[3];
	char Minute[3];

	do
	{
		Choix = Menu();

		switch(Choix)
		{
			case 1: cout << "Veuillez saisir votre nom: ";
					cin  >> Nom;
					break;

			case 2: cout << "Veuillez saisir votre prenom: ";
					cin  >> Prenom;
					break;

			case 3: cout << "Veuillez saisir votre jour de naissance: ";
					cin  >> Jour;
					cout << "Veuillez saisir votre mois de naissance: ";
					cin  >> Mois;
					cout << "Veuillez saisir votre annee de naissance: ";
					cin  >> Annee;
					cout << "Veuillez saisir votre heure de naissance: ";
					cin  >> Heure;
					cout << "et la minute: ";
					cin  >> Minute;
					break;

			case 4: codageTrame(Nom,Prenom,Jour,Mois,Annee,Heure,Minute);
					break;

			case 5: decodageTrame();
					break;
					
			case 6: break;

			default : cout << "Veuillez saisir un nombre entre 1 et 6!";
					  break;
		}
	}while (! (Choix == 6));
}