/*******************************************************************************************************************
Yann Lacquehay
Flux Entr�e/Sortie
21/01/10
*******************************************************************************************************************/

#include <iostream>
using namespace std;

void main (void)
{
	char Chaine_Une[255];
	char Chaine_Deux[255];
	char Chaine_Trois[255];

	cout << "Entrez la premiere chaine (Bonjour a tous): ";
	cin.get(Chaine_Une, 255, '\n');
	// Permet de recuperer caractere par caractere
	cout << "Chaine Une: " << Chaine_Une << endl;

	cout << "\n\nEntrez la deuxieme chaine: ";
	cin.getline(Chaine_Deux, 255, '\n');
	//Recupere la ligne entiere mais avec le cin.get du dessus, la chaine deux ne se rempli pas 
	//Car il recoit le ��entree�� de precedemment.
	cout << "\nChaine Deux: " << Chaine_Deux <<endl;

	cout << "\n\nEncore une fois\n";
	
	cout << "\n\nEntrez a nouveau la premiere chaine (Bonjour a tous): ";
	cin.get(Chaine_Une, 255);
	cout << "Chaine Une: " << Chaine_Une << endl;

	cin.ignore(255, '\n');
	//Permet d'ignorer un caractere , ici il permet d'ignorer entree ce qui permet de faire la chaine deux.

	cout << "\n\nEntrez la deuxieme chaine (vous allez bien?): ";
	cin.getline(Chaine_Deux, 255);
	cout << "Chaine Deux: " << Chaine_Deux << "\n" <<endl;

	cout << "\n\nEntrez la troisieme chaine (ca y est c'est la fin): ";
	cin.getline(Chaine_Trois, 255);
	cout << "Chaine Trois: " << Chaine_Trois << "\n" <<endl;
}