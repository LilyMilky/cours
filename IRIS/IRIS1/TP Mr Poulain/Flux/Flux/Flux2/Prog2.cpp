/*******************************************************************************************************************
Yann Lacquehay
Flux Entr�e/Sortie
21/01/10
*******************************************************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

void main (void)
{
	char c = 'A';
	char Tab[] = "Bonjour";

	int n = 12000;

	cout << "\n\nUtilisation de get, Tapez Bonjour a tous" << "\n" << endl;
	cout << "Pour sortir appuyez sur CTRL Z" << "\n" << endl;

	while ((c = cin.get())!= EOF)
	//Permet de recuperer un caractere a la fois tant qu'on a pas EOF (CTRL Z), 
	//Ce qui permet avec le while d'afficher un caractere a la fois.
	{
		cout << "Lettre?" << c << endl;
	}

	cout << "\nDebut: ";
	cout.width(10);
	cout << "789";

	cout << "\nDebut: ";
	cout.width(10);
	cout << "56789" << " Suivant : 10";

	cout << "\nDebut: ";
	cout.width(3);
	cout << "01234";

	cout << "\nDebut: ";
	cout.width(10);
	//Permet de laisser un espace de 10, si des nombres sont afficher, il les affiche � ��leur place�� 
	//(Espace 7 rempli par le chiffre 7 etc...)
	cout.fill('*');
	//Permet de remplir  chaque espace sans nombre par ce qu'il y a entre ( ) (ici par des *)
	cout << "789" <<endl;

	cout << "\n\nAutre mode d'affichage: " <<endl;

	cout << setw(10) << "789" <<endl;
	//Permet de faire la meme chose que cout.width(10).
	cout.fill('*');
	cout << setw(10) << "56789" << endl;
	cout << "0123456789" <<endl;
}