/*******************************************************************************************************************
Yann Lacquehay
Flux Entr�e/Sortie
21/01/10
*******************************************************************************************************************/

#include <iostream>
using namespace std;

void main (void)
{
	char c = 'A';
	char Tab[] = "Bonjour";

	int n = 12000;

	cout << "Utilisation de getline, Taper un mot (6/7 lettres): ";
	cin.getline(Tab, sizeof(Tab), '\n');
	// Permet de recuperer toute une ligne saisie par l'utilisateur.

	cout << "\nUtilisation de write pour afficher 5 caracteres: ";
	cout.write(Tab,5);
	// Permet de n'afficher qu'un certain  nombre de caractere, 
	// On met entre ( ) la chaine de caractere et le nombre de caractere � afficher.

	cout << "\nUtilisation de write pour afficher 9 caracteres: ";
	cout.write(Tab,9);

	cout << "\n\nUtilisation de get, Tapez un caractere: ";
	cin.get(c);
	// Recupere un caractere.

	cout << "\nUtilisation de put pour afficher un caractere: ";
	cout.put(c);
	// Permet d'afficher un caractere.

	cout << "\n\n\nAction sur la base du nombre (12000 en decimal): \n" <<endl;
	cout << "Par default	: "	<< n <<endl;
	cout << "En Octal	: " << oct	<< n << endl;
	cout << "En decimal	: " << dec	<< n << endl;
	cout << "En hexadecimal : " << hex	<< n << endl;
	cout << "En suite	: "		<< n << endl;
	//Oct permet d'afficher la valeur en octal  
	//Dec permet d'afficher la valeur en decimal (avec ici une valeur decimal a 12000 de base)
	//Hex permet d'afficher la valeur en hexadecimal.
}