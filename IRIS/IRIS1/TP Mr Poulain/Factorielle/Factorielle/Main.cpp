/************************************************************************************************
Lacquehay Yann
Progroamme Factorielle
07/01/10
*************************************************************************************************/

#include "Factorielle.h"
#include <iostream>
#include "Combinaison.h"
using namespace std;

void main (void)
{
	int Nbre1, Nbre2,tour = 1;
	float Resultat1,Resultat3;
	float Combi;

	cout << "Veuillez saisir un nombre entier positif: ";
	cin  >> Nbre1;
	cout << "Veuillez saisir un 2eme nombre entier positif plus petit que le premier: ";
	cin  >> Nbre2;
	
	Resultat1 = Factorielle (Nbre1,Nbre2,tour);
	cout << "\nLe factorielle optimise de Factorielle1/ Factorielle2 est : " << Resultat1 << endl <<endl;
	
	tour = tour + 1;

	Resultat3 = Factorielle(Nbre1,Nbre2,tour);
	cout << "\nLe factorielle de Nombre1 - Nombre2 est : " << Resultat3 << endl <<endl;

	Combi = Combinaison(Resultat1,Resultat3);
	cout << "Factorielle1 / Factorielle(Nombre1 - Nombre2)= ";
	cout << Combi <<endl;
}