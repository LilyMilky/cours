/***************************************************************************
* J-B MASTOUMECQ	
* Vect_badge.cpp  Projet Supervision
* Les fonctions membres de la classe vect_badge
***************************************************************************/
#include <fstream>
#include "Vect_badge.h"

/*******************************************************************
Constructeur par d�faut:r�cupere les badges du fichier
badge.txt dans le vector
*******************************************************************/
vect_badge::vect_badge()
{
	badge b;
	ifstream infile;
	
	infile.open("badge.txt" , ios::out|ios::in);

	//r�cup�ration des donn�es (des badges) dans le conteneur vector
	// A completer

	infile.close();
}
/*******************************************************************
Constructeur:r�cupere les badges du fichier sp�cifi� dans le vector
*******************************************************************/
vect_badge::vect_badge(char *fichier)
{
	badge b;
	ifstream infile;

	//connexion d'un flux au fichier texte pass� en param�tre
	// A completer

	//r�cup�ration des donn�es (des badges) dans le conteneur vector
	// A completer

	infile.close();
}
/*******************************************************************
Destructeur:r�injecte les badges du vector dans le fichier
*******************************************************************/
vect_badge::~vect_badge()
{
	ofstream outfile;
	
	//connexion d'un flux au fichier texte en �crasant l'ancien contenu
	// A completer

	//R�injection des donn�es du vector dans le fichier
	// A completer

	outfile.close();
}

/*******************************************************************
M�thode afficher_badge():affiche les badges du vector
*******************************************************************/
void vect_badge::afficher_badge()
{
	// A completer
}

/*******************************************************************
M�thode trouver_badge():recherche si un badge est dans le vector
*******************************************************************/
bool vect_badge::rechercher_badge(badge b)
{
	// A completer
	return(true);
}

/*******************************************************************
M�thode supprimer_badge():supprime un badge du vector
*******************************************************************/
void vect_badge::supprimer_badge(badge b)
{
	// A completer
}

/*******************************************************************
M�thode inserer_badge():insere un badge dans le vector
*******************************************************************/
void vect_badge::inserer_badge(badge b)
{
	// A completer
}

