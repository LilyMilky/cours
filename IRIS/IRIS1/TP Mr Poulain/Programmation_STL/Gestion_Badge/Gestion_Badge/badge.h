/***************************************************************************
* J-B MASTOUMECQ	
* badge.h  Projet Supervision
* Déclaration de la classe badge 			
/***************************************************************************/
#include <string>
#include <fstream>
using namespace std;

class badge
{
	private :
	string nom, date;
	int identifiant;
	unsigned short int codeAcces;

	public :
		badge();
		badge(int id, unsigned short int code, string name, string d);

		string	Nom()		{return nom;}
		string	Date()		{return date;}
		int	Identifiant()				{return identifiant;}
		unsigned short int CodeAcces()	{return codeAcces;}
		
		void Set_Badge(int id,unsigned short int code );
		void Set_Badge(int id,unsigned short int code ,string name,string d);

		/*******************************************************************
		A COMPLETER ??????????
		*******************************************************************/
		friend istream & operator>> (istream &entree,badge &badg) // Redefinition de l'operateur >>
		{
			entree >> badg.identifiant >> badg.codeAcces >> badg.nom >> badg.date;
			return entree;
		}

		/*******************************************************************
		A COMPLETER ??????????
		*******************************************************************/
		friend ostream & operator<< (ostream &sortie,badge badg)// Redefinition de l'operateur <<
		{
			sortie << badg.identifiant <<" "<< badg.codeAcces <<" "<< badg.nom <<" "<< badg.date;
			return sortie;
		}
		/*******************************************************************
		A COMPLETER ??????????
		*******************************************************************/
		bool operator==(badge badg)// Redefinition de l'operateur ==
		{
			if (badg.identifiant == this->identifiant &&
				badg.codeAcces == this->codeAcces &&
				badg.nom == this->nom &&
				badg.date == this->date)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
};