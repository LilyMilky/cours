/***************************************************************************
* J-B MASTOUMECQ	
* badge.cpp  Projet Supervision
* Les fonctions membres de la classe badge
***************************************************************************/
#include "badge.h"
#include <iostream>
using namespace std;


/*******************************************************************
Constructeur: attribue des valeurs par d�faut aux attributs
*******************************************************************/
badge::badge()
{
	nom = "MASTOUMECQ";
	date = "01/01/09";
	identifiant = 10000000;
	codeAcces = 1000;
}
/*******************************************************************
Constructeur: attribue les valeurs pass�es en param�tres aux attributs
*******************************************************************/
badge::badge(int id, unsigned short int code, string name, string d)
{
	identifiant = id;
	codeAcces = code;
	nom = name;
	date = d;
}
/*******************************************************************
Initialisation d'un badge avec des valeurs pass�es en param�tres
*******************************************************************/
void badge::Set_Badge(int id,unsigned short int code)
{
	identifiant = id;
	codeAcces = code;
}

/*******************************************************************
Initialisation d'un badge avec des valeurs pass�es en param�tres
*******************************************************************/
void badge::Set_Badge(int id,unsigned short int code,string name,string d)
{
	identifiant = id;
	codeAcces = code;
	nom = name;
	date = d;
}
