#include <iostream>
using namespace std;
#include "badge.h"
#include <string>

void main (void)
{
	string nom1, nom2, date1, date2, d , name;
	int identifiant1 , identifiant2, id2, id;
	unsigned short int codeAcces1, codeAcces2, code2, code;

	badge Badge1, Badge2(11111111, 1111, "Patchouli Knowledge", "29/10/90");
	nom1 = Badge1.Nom();
	date1 = Badge1.Date();
	codeAcces1 = Badge1.CodeAcces();
	identifiant1 = Badge1.Identifiant();
	cout << "Badge1 : "<< nom1 << " " << date1 << " " << codeAcces1 << " " << identifiant1 << endl;

	nom2 = Badge2.Nom();
	date2 = Badge2.Date();
	codeAcces2 = Badge2.CodeAcces();
	identifiant2 = Badge2.Identifiant();
	cout << "Badge2 : "<< nom2 << " " << date2 << " " << codeAcces2 << " " << identifiant2 << endl << endl;

	cout << "Veuillez saisir l'identifiant a rentrer dans le badge : ";
	cin >> id;
	cout << "et un code: ";
	cin >> code;
	Badge1.Set_Badge(id, code);
	identifiant1 = Badge1.Identifiant();
	codeAcces1 = Badge1.CodeAcces();
	cout << "Badge1 : "<< nom1 << " " << date1 << " " << codeAcces1 << " " << identifiant1 << endl;

	cout << "Veuillez saisir l'identifiant a rentrer dans le badge : ";
	cin >> id2;
	cout << "et un code: ";
	cin >> code2;
	cout << "Veuillez saisir un nom : ";
	cin >> name;
	cout << "Veuillez saisir une date (JJ/MM/AA): ";
	cin >> d;
	Badge1.Set_Badge(id2, code2, name, d);
	nom1 = Badge1.Nom();
	date1 = Badge1.Date();
	codeAcces1 = Badge1.CodeAcces();
	identifiant1 = Badge1.Identifiant();
	cout << "Badge1 : "<< nom1 << " " << date1 << " " << codeAcces1 << " " << identifiant1 << endl;
}