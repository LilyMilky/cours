/***************************************************************************
* J-B MASTOUMECQ	
* Vect_badge.h  Projet Supervision
* Déclaration de la classe vect_badge 			
/***************************************************************************/
#include "badge.h"
// A COMPLETER



class vect_badge
{
	private:

		// A COMPLETER

	public:	

		vect_badge();
		vect_badge(char *fichier);
		~vect_badge();
		
		void afficher_badge();
		
		bool rechercher_badge(badge b);
		void supprimer_badge(badge b);
		void inserer_badge(badge b);
};

