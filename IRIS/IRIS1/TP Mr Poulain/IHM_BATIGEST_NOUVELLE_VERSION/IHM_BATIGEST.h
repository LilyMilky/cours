// IHM_BATIGEST.h�: fichier d'en-t�te principal pour l'application PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "incluez 'stdafx.h' avant d'inclure ce fichier pour PCH"
#endif

#include "resource.h"		// symboles principaux


// CIHM_BATIGESTApp:
// Consultez IHM_BATIGEST.cpp pour l'impl�mentation de cette classe
//

class CIHM_BATIGESTApp : public CWinApp
{
public:
	CIHM_BATIGESTApp();

// Substitutions
	public:
	virtual BOOL InitInstance();

// Impl�mentation

	DECLARE_MESSAGE_MAP()
};

extern CIHM_BATIGESTApp theApp;