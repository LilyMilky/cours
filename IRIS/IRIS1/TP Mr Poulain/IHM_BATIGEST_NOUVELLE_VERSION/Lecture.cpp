// Lecture.cpp : implementation file
//

#include "stdafx.h"
#include "IHM_BATIGEST.h"
#include "Lecture.h"

/*******************************************************************************************************
* Ajout 
*******************************************************************************************************/
#include <fstream>	   //Pour pouvoir travailler avec des fichiers
using namespace std;
/******************************************************************************************************/

// CLecture dialog

IMPLEMENT_DYNAMIC(CLecture, CDialog)

CLecture::CLecture(CWnd* pParent /*=NULL*/)
	: CDialog(CLecture::IDD, pParent)
	, m_NomFichier(_T(""))
	, m_TempMoy(_T(""))
{

}

CLecture::~CLecture()
{
}

void CLecture::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	DDX_Text(pDX, IDC_Nomfichier, m_NomFichier);
	DDX_Text(pDX, IDC_TempMoy, m_TempMoy);
	
	DDX_Control(pDX, IDC_NumZone, m_NumZone);
	DDX_Control(pDX, IDC_AffichFichier, m_ZoneAffichage);
}


BEGIN_MESSAGE_MAP(CLecture, CDialog)
	ON_BN_CLICKED(IDC_LireFichier, &CLecture::OnBnClickedLirefichier)
	ON_BN_CLICKED(IDC_MesuresZone, &CLecture::OnBnClickedMesureszone)
	ON_BN_CLICKED(IDC_TemperatureMoyenne, &CLecture::OnBnClickedTemperaturemoyenne)
END_MESSAGE_MAP()


// CLecture message handlers

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++		DEBUT DU CODAGE DES EVENEMENTS		+++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
* Lors d'un clic sur le bouton Lire Fichier
* il faut afficher dans la fen�tre de visualisation
* le contenu du fichier indiqu� par l'utilisateur.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
******************************************************************************/
void CLecture::OnBnClickedLirefichier()
{
	// TODO: Add your control notification handler code here

	ifstream Fich_Texte;
	char Ligne[255];
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	if(m_NomFichier != "")
	{
		Fich_Texte.open(m_NomFichier, ios::in);				// Ouverture du fichier en Lecture
		if (!Fich_Texte)
		{
			AfxMessageBox("Ouverture impossible (lecture)");
		}
		else
		{
			m_ZoneAffichage.ResetContent();
			while (Fich_Texte.getline(Ligne,255))
			{
				m_ZoneAffichage.AddString(Ligne);			//Ajout d'une ligne dans la liste
			}

			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}
			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� de nom de fichier");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Mesure pour une zone
* il faut afficher dans la fen�tre de visualisation
* les mesures de la zone indiqu� par l'utilisateur.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
* et un num�ro de zone
******************************************************************************/
void CLecture::OnBnClickedMesureszone()
{
	// TODO: Add your control notification handler code here

	ifstream Fich_Texte;
	char Ligne[255];
	CString ZoneX;
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	m_NumZone.GetWindowTextA(ZoneX);				//Pour recuperer ds une variable le contenu d'une
													//d'une zone de texte de type CEdit.
	
	if( (m_NomFichier != "") && (ZoneX != "") )
	{
		Fich_Texte.open(m_NomFichier, ios::in);					// Ouverture du fichier en Lecture
		if (!Fich_Texte)
		{
			AfxMessageBox("Ouverture impossible (lecture)");
		}
		else
		{
			do
			{
				Fich_Texte.getline(Ligne,255);					// Lecture des Zones
			}while( !(Ligne[4]== ZoneX[0]) );					// Ceci ne fonctionne que si la zone existe

			m_ZoneAffichage.ResetContent();
			m_ZoneAffichage.AddString(Ligne);					//Ajout d'une ligne dans la liste
			
			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}

			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� de nom de fichier\n ou de num�ro de zone");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Temp�rature Moyenne
* il faut afficher dans le cadre Temp�rature moyenne
* la valeur moyyenne des temp�ratures pour la zone indiqu� par l'utilisateur.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
* et un num�ro de zone
******************************************************************************/
void CLecture::OnBnClickedTemperaturemoyenne()
{
	// TODO: Add your control notification handler code here

	ifstream Fich_Texte;
	CString ZoneX;

	char ZoneLigne[6];
	float T1,T2,T3,T4,Moy;
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	m_NumZone.GetWindowTextA(ZoneX);				//Pour recuperer ds une variable le contenu d'une
													//d'une zone de texte de type CEdit.
	
	if( (m_NomFichier != "") && (ZoneX != "") )
	{
		Fich_Texte.open(m_NomFichier, ios::in);		// Ouverture du fichier en Lecture
		if (!Fich_Texte)
		{
			AfxMessageBox("Ouverture impossible (lecture)");
		}
		else
		{
			do
			{
				Fich_Texte >> ZoneLigne >> T1 >> T2 >> T3 >> T4;
			}while( !(ZoneLigne[4]== ZoneX[0]) );	// Ceci ne fonctionne que si la zone existe

			Moy = (T1+T2+T3+T4)/4;					// Calcul de la moyenne des t� pour la Zone
			m_TempMoy.Format("%.3f",Moy);			//Conversion r�el en CString avec 3 chiffre apr�s la virgule

			UpdateData(0);							//Permet d'envoyer dans les zones de saisie de l'IHM
													//le contenu des variables membres
			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}
			
			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� de nom de fichier\n ou de num�ro de zone");
	}
}
