//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by IHM_BATIGEST.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IHM_BATIGEST_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_LECTURE                     129
#define IDD_ECRITURE                    130
#define IDC_ECRITURE                    1000
#define IDC_LECTURE                     1001
#define IDC_LireFichier                 1001
#define IDC_MesuresZone                 1002
#define IDC_TemperatureMoyenne          1003
#define IDC_AffichFichier               1006
#define IDC_NumZone                     1007
#define IDC_TempMoy                     1008
#define IDC_TemperatureZone             1008
#define IDC_Nomfichier                  1009
#define IDC_AjoutComment                1010
#define IDC_FichierHTML                 1011
#define IDC_SupZone                     1012
#define IDC_InsererZone                 1013
#define IDC_ModifZone                   1014
#define IDC_TemperatureZone2            1015
#define IDC_Commentaire                 1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
