// Ecriture.cpp : implementation file
//

#include "stdafx.h"
#include "IHM_BATIGEST.h"
#include "Ecriture.h"

/*******************************************************************************************************
* Ajout 
*******************************************************************************************************/
#include <fstream>					//Pour pouvoir travailler avec des fichiers
using namespace std;
/******************************************************************************************************/

// CEcriture dialog

IMPLEMENT_DYNAMIC(CEcriture, CDialog)

CEcriture::CEcriture(CWnd* pParent /*=NULL*/)
	: CDialog(CEcriture::IDD, pParent)
	, m_NomFichier(_T(""))
	, m_Commentaire(_T(""))
	, m_NumZone(_T(""))
	, m_Temperatures(_T(""))
{

}

CEcriture::~CEcriture()
{
}

void CEcriture::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Nomfichier, m_NomFichier);
	DDX_Control(pDX, IDC_AffichFichier, m_ZoneAffichage);
	DDX_Text(pDX, IDC_Commentaire, m_Commentaire);
	DDX_Text(pDX, IDC_NumZone, m_NumZone);
	DDX_Text(pDX, IDC_TemperatureZone, m_Temperatures);
}


BEGIN_MESSAGE_MAP(CEcriture, CDialog)
	ON_BN_CLICKED(IDC_AjoutComment, &CEcriture::OnBnClickedAjoutcomment)
	ON_BN_CLICKED(IDC_FichierHTML, &CEcriture::OnBnClickedFichierhtml)
	ON_BN_CLICKED(IDC_SupZone, &CEcriture::OnBnClickedSupzone)
	ON_BN_CLICKED(IDC_InsererZone, &CEcriture::OnBnClickedInsererzone)
	ON_BN_CLICKED(IDC_ModifZone, &CEcriture::OnBnClickedModifzone)
	ON_LBN_DBLCLK(IDC_AffichFichier, &CEcriture::OnLbnDblclkAffichfichier)
END_MESSAGE_MAP()


// CEcriture message handlers


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++		DEBUT DU CODAGE DES EVENEMENTS		+++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
* Lors d'un clic sur le bouton Ajouter Commentaire
* Il faut ajouter le commentaire saisie par l'utilisateur en fin de fichier
* puis il faut afficher dans la fen�tre de visualisation.
* Le contenu du fichier indiqu� par l'utilisateur.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
* et un commentaire
******************************************************************************/
void CEcriture::OnBnClickedAjoutcomment()
{
	// TODO: Add your control notification handler code here

	fstream Fich_Texte;
	char Ligne[255];
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	if(m_NomFichier != "" && m_Commentaire != "")
	{
		Fich_Texte.open(m_NomFichier, ios::in|ios::out|ios::app);	//Ouverture avec positionnement 
																	//en fin du fichier
		if (!Fich_Texte)
		{
			AfxMessageBox("Ouverture impossible (ecriture)");
		}
		else
		{
			Fich_Texte << m_Commentaire << endl;			//Ajout du commentaire dans le fichier

			Fich_Texte.seekp(0, ios::beg);					//Positionnement en d�but de fichier
			m_ZoneAffichage.ResetContent();

			while (Fich_Texte.getline(Ligne,255))
			{
				m_ZoneAffichage.AddString(Ligne);			//Ajout d'une ligne dans la zone d'affichage
			}
	
			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}

			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� de nom de fichier\n ou vous n'avez pas tap� de commenatire");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Fichier HTML
* Il faut cr�er un fichier nomm� "jrXXXXXX.htm" puis
* ajouter "<html><body><pre><h1>\n" en d�but de fichier,
* ensuite recopier le contenu du fichier choisi par l'utilisateur jrXXXXXX.txt" puis
* ajouter "</h1></pre></body></html>\n" en fin de fichier.
* Ensuite afficher dans la fen�tre de visualisation.
* Le contenu du fichier indiqu� par l'utilisateur.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
******************************************************************************/
void CEcriture::OnBnClickedFichierhtml()
{
	// TODO: Add your control notification handler code here

	fstream Fich_HTML;
	ifstream Fich_Texte;
	CString NomFich_HTML;
	char Ligne[255];
	char c;

	UpdateData(1);										//Permet de recuperer dans les variables membres
														//ce qui a �t� saisie par l'utilisateur sur l'IHM
	if(m_NomFichier != "")
	{
		NomFich_HTML = m_NomFichier;
		Fich_Texte.open(m_NomFichier, ios::in);			//Ouverture en lecture du fichier texte d'origine

		if (!Fich_Texte)
		{
			AfxMessageBox("Pb Ouverture fichier\n(Fichier Texte en lecture)");
		}
		else
		{
			NomFich_HTML.Replace("txt", "htm");								//remplace jrXXXXXX.txt en jrXXXXXX.htm
			
			Fich_HTML.open(NomFich_HTML, ios::out|ios::in|ios::trunc);		// Suppression du fichier jrXXXXXX.htm et 
																			// Recr�ation avec ouverture en Lecture/Ecriture
			
			if (!Fich_HTML)
			{
				AfxMessageBox("Pb Ouverture fichier\n(Fichier HTML Lecture/Ecriture)");
			}
			else
			{
				Fich_HTML << "<HTML><BODY><PRE><H1>" << endl;

				c = Fich_Texte.get();					// Recopie
				while (c != EOF)						// du fichier texte d'origine
				{	
					Fich_HTML << c;						// dans le fichier HTML
					c = Fich_Texte.get();
				}										// ouvert juste avant
				
				if(Fich_Texte.eof())					// La boucle while pr�c�dente
				{										// fait passer le bit eof � 1
					Fich_Texte.clear();					// donc remise � 0
				}
				Fich_Texte.close();						// Avant la d�connection du flux

				Fich_HTML << "</H1></PRE></BODY></HTML>" << endl;

				Fich_HTML.seekg(0, ios::beg);

				m_ZoneAffichage.ResetContent();
				while (Fich_HTML.getline(Ligne,255))
				{
					m_ZoneAffichage.AddString(Ligne);		//Ajout d'une ligne dans la zone d'affichage
				}
				if(Fich_HTML.eof())
				{	
					Fich_HTML.clear();
				}
				Fich_HTML.close();
			}
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� de nom de fichier");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Supprimer une Zone
* Il faut supprimer dans le fichier choisi par l'utilisateur
* la ligne correspondante au num�ro de zone indiqu� par l'utilisateur
* Pour cela 2 solutions :
* Soit en utilisant un fichier temporaire dans lequel on recopie toutes les lignes
* sauf la ligne correspondante � la zone � supprimer.
* Puis on recopie ce fichier dans le fichier d'origine.
* Soit en supprimant dans la zone d'affichage la ligne correspondante � la zone 
* � supprimer. Puis recopie de la zone d'affichage dans le fichier d'origine
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier
* et un num�ro de zone
******************************************************************************/
void CEcriture::OnBnClickedSupzone()
{
	// TODO: Add your control notification handler code here

	fstream Fich_Texte,Fich_Temp;
	char Ligne[255];
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	if(m_NomFichier != "" && m_NumZone != "")
	{
		Fich_Texte.open(m_NomFichier, ios::in);						//Ouverture en lecture du fichier
		Fich_Temp.open("Temp.txt", ios::out|ios::in|ios::trunc);	//Suppression du fichier Temp.txt et 
																	//recr�ation avec Ouverture en Lecture/Ecriture

		if (!Fich_Texte || !Fich_Temp)
		{
			AfxMessageBox("Ouverture d'un des ficiers impossible");
		}
		else
		{
			while(Fich_Texte.getline(Ligne,50,'\n'))		// Lecture dans le fichier texte d'origine
			{
				if (Ligne[4]!=m_NumZone)					// Si ce n'est pas la zone a supprimer
				{
					Fich_Temp << Ligne << endl;				// Alors recopie dans le fichier Temporaire
				}
			}

			if(Fich_Texte.eof())
			{	
					Fich_Texte.clear();
			}
			Fich_Texte.close();
			Fich_Temp.seekg(0,ios::beg);

			Fich_Texte.open(m_NomFichier, ios::out|ios::in|ios::trunc);	//Suppression du fichier texte d'origine puis
																		//Recr�ation et ouverture en �criture
			if (!Fich_Texte)
			{
				AfxMessageBox("Ouverture impossible");
			}
			else
			{
				while(Fich_Temp.getline(Ligne,50,'\n'))				// Recopie dans le fichier texte d'origine
				{
					Fich_Texte << Ligne << endl;
				}
				if(Fich_Temp.eof())
				{	
					Fich_Temp.clear();
				}
				Fich_Temp.close();
				Fich_Texte.seekg(0,ios::beg);

				m_ZoneAffichage.ResetContent();
				while (Fich_Texte.getline(Ligne,255))
				{
					m_ZoneAffichage.AddString(Ligne);				//Ajout d'une ligne dans la zone d'affichage
				}

				if(Fich_Texte.eof())
				{	
					Fich_Texte.clear();
				}
				Fich_Texte.close();
			}
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� le nom de fichier\n ou le num�ro de zone");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Ins�rer une Zone
* Il faut ajouter dans le fichier choisi par l'utilisateur
* la ligne correspondante au num�ro de zone indiqu� par l'utilisateur
* avec les valeurs de temp�rature qu'il aura saisies.
* Pour cela 2 solutions :
* Soit en utilisant un fichier temporaire dans lequel on recopie toutes les lignes
* et la ligne correspondante � la zone � ins�rer.
* Puis on recopie ce fichier dans le fichier d'origine.
* Soit en ins�rant dans la zone d'affichage la ligne correspondante � la zone 
* � ins�rer. Puis recopie de la zone d'affichage dans le fichier d'origine
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier,
* un num�ro de zone et les 4 temp�ratures
******************************************************************************/
void CEcriture::OnBnClickedInsererzone()
{
	// TODO: Add your control notification handler code here

	fstream Fich_Texte,Fich_Temp;
	char Ligne[255];
	char ZoneAdd[6] = "Zone6";
	int Nb_Ligne=0;
	int i;
	
	UpdateData(1);											//Permet de recuperer dans les variables membres
															//ce qui a �t� saisie par l'utilisateur sur l'IHM
	
	if(m_NomFichier != "" && m_NumZone != "" && m_Temperatures != "")
	{
		Fich_Texte.open(m_NomFichier, ios::in|ios::out);			//Ouverture en Lecture/Ecriture du fichier texte d'origin
		Fich_Temp.open("Temp.txt", ios::out|ios::in|ios::trunc);	//Suppression du fichier Temp.txt et 
																	//recr�ation avec Ouverture en Lecture/Ecriture
		if (!Fich_Texte || !Fich_Temp)
		{
			AfxMessageBox("Ouverture d'un des fichier impossible");
		}
		else
		{

			while(Fich_Texte.getline(Ligne,50,'\n'))			// Recopie du fichier d'origine ds le fichier Temporaire
			{
				Fich_Temp << Ligne << endl;
				if(Ligne[0]=='Z' && Ligne[4] < m_NumZone[0])	// Si C'est une Zone et X (ZoneX) < au n� de zone � ajouter 
				{												// Sans le test de savoir si c'est une zone,si le commentaire est OK, cela ne fonctionne pas 
					Nb_Ligne++;									// Compte le nbr de ligne se trouvant avant la zone � ajouter
				}
			}

			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}

			Fich_Texte.seekp(0, ios::beg);				// Place le pointeur au debut du fichier
			Fich_Temp.seekp(0, ios::beg);				// Place le pointeur au debut du fichier

			for(i=0;i<Nb_Ligne;i++)						// Recopie du fichier Temporaire ds le fichier d'origine
			{
				Fich_Temp.getline(Ligne,50,'\n');		// Le nbre de ligne se trouvant avant la zone
				Fich_Texte << Ligne << endl;			// � ajouter
			}

			ZoneAdd[4]=m_NumZone[0];									// Ajout de la zone suppl�mentaire
			Fich_Texte << ZoneAdd << " " << m_Temperatures << endl;		// "ZoneX" et le temp�ratures

			while(Fich_Temp.getline(Ligne,50,'\n'))		// Recopie des autres Zone s'il en reste
			{
				Fich_Texte << Ligne << endl;			// Et du commentaire
			}
			
			if(Fich_Temp.eof())
			{	
				Fich_Temp.clear();
			}
			
			Fich_Temp.close();

			Fich_Texte.seekg(0,ios::beg);				// Place le pointeur au debut du fichier
		
			m_ZoneAffichage.ResetContent();
			while (Fich_Texte.getline(Ligne,255))
			{
				m_ZoneAffichage.AddString(Ligne);		//Ajout d'une ligne dans la zone d'affichage
			}
			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}
			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� le nom de fichier\n ou le num�ro de zone\n ou les 4 temp�ratures");
	}
}

/******************************************************************************
* Lors d'un clic sur le bouton Modifier une Zone
* Il faut modifier dans le fichier choisi par l'utilisateur
* la ligne correspondante au num�ro de zone indiqu� par l'utilisateur
* avec les valeurs de temp�rature qu'il aura saisies.
* Pour cela 2 solutions :
* Soit en utilisant un fichier temporaire dans lequel on recopie toutes les lignes
* ne correspondant pas � la zone � modifier.
* Si la ligne correspond � la zone � modifier, il faut remplacer les valeurs des 
* temp�ratures par les valeurs saisies par l'utilisateur.
* Pour finir on recopie ce fichier dans le fichier d'origine.
* Soit lors d'un double clic sur la ligne � modifier dans la
* Zonne d'affichage, le num�ro de zone et les 4 temp�ratures
* correspondant � cette ligne apparaissent dans les zones appropri�es
* L'utilisateur modifie les temp�ratures puis clique ensuite sur le bouton
* Modifer Zone pour effectuer les modif.
* ATTENTION : S'assurer que l'utilisateur a bien saisie un nom de fichier,
* un num�ro de zone et les 4 temp�ratures
******************************************************************************/
void CEcriture::OnBnClickedModifzone()
{
	// TODO: Add your control notification handler code here


	fstream Fich_Texte,Fich_Temp;
	char Ligne[255];
	char ZoneX[6]="ZoneX";
	
	UpdateData(1);									//Permet de recuperer dans les variables membres
													//ce qui a �t� saisie par l'utilisateur sur l'IHM
	
	if(m_NomFichier != "" && m_NumZone != "" && m_Temperatures != "")
	{
		Fich_Texte.open(m_NomFichier, ios::in);		//Ouverture en lecture du fichier
		Fich_Temp.open("Temp.txt", ios::out|ios::in|ios::trunc);	//Suppression du fichier Temp.txt et 
																	//recr�ation avec Ouverture en Lecture/Ecriture
		if (!Fich_Texte || !Fich_Temp)
		{
			AfxMessageBox("Ouverture d'un des fichiers impossible");
		}
		else
		{
			while(Fich_Texte.getline(Ligne,50,'\n'))	// Recopie du fichier d'origine ds le fichier Temporaire
			{
				if (Ligne[0]!='Z' || Ligne[4] != m_NumZone)				// Si c'est pas une Zone (commentaire) ou X (ZoneX) != au N� de zone
				{
					Fich_Temp << Ligne << endl;							// Recopie dans le fichier temporaire
				}
				else													// Sinon	
				{
					ZoneX[4]=m_NumZone[0];
					Fich_Temp << ZoneX << " " << m_Temperatures << endl;	// Ecriture dans le fichier Temporaire
				}															// de la zone � modifier
			}

			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}
			Fich_Texte.close();
			
			Fich_Temp.seekg(0,ios::beg);
			Fich_Texte.open(m_NomFichier, ios::out|ios::in|ios::trunc);	//Ouverture en ecriture du fichier

			while(Fich_Temp.getline(Ligne,50,'\n'))					// Recopie du fichier temp dans
			{
				Fich_Texte << Ligne << endl;						// le fichier d'origine
			}

			if(Fich_Temp.eof())
			{	
				Fich_Temp.clear();
			}
			Fich_Temp.close();

			
			Fich_Texte.seekg(0,ios::beg);

			m_ZoneAffichage.ResetContent();
			while (Fich_Texte.getline(Ligne,255))
			{
				m_ZoneAffichage.AddString(Ligne);			//Ajout d'une ligne dans la zone d'affichage
			}
		
			if(Fich_Texte.eof())
			{	
				Fich_Texte.clear();
			}
			Fich_Texte.close();
		}
	}
	else
	{
		AfxMessageBox("Vous n'avez pas indiqu� le nom de fichier\n ou le num�ro de zone\n ou les 4 temp�ratures");
	}
}

/******************************************************************************
* Lors d'un double clic sur la zone d'affichage
* il faut r�cup�rer le n� de ligne sur laquelle a cliqu� l'utilisateur
* pour mettre le N� de Zone dans le champs Num�ro de Zone
* et les 4 temp�ratures dans le champs Temp�ratures de la zone
******************************************************************************/
void CEcriture::OnLbnDblclkAffichfichier()
{
	// TODO: Add your control notification handler code here

	CString Ligne;
	CString Zone;									//Permet de decomposer en 2 chaine une ligne
	CString Temperature;							//de la zone de visualisation
	
	int NumLigne, Taille;

	NumLigne = m_ZoneAffichage.GetCurSel();			//Recup�ration du n� de ligne du double clic

	if (NumLigne != LB_ERR)
	{
		m_ZoneAffichage.GetText(NumLigne,Ligne);

		if (Ligne[0]!='Z')
		{
			AfxMessageBox("Vous n'avez pas cliqu� sur une zone");
		}
		else
		{
			Taille = Ligne.GetLength();

			Zone = Ligne.Left(5);			
			Temperature = Ligne.Right(Taille-6);

			m_NumZone = Zone[4];
			m_Temperatures = Temperature;

			UpdateData(0);						//Permet d'envoyer dans les zones de saisie de l'IHM
												//le contenu des variables membres
		}
	}
}
