<html>
<head>
	<LINK rel="stylesheet" tylie="text/css" media="screen" href="Style.css" />
	<title> Gestion BdD --Ajout Badge-- </title>
	<link rel="icon" type="image/jpg" href="80290bfc4e00bbf3cbb324de84e655a2.JPG" />
</head>

<body>
	<div id="entete">
		<center><h1> AJOUTER UN BADGE </h1></center>
	</div>
	
	<div id="menu">
		<ul>
			<li> <a href= "index.php"> Accueil </a></li>
			<li> <a href= "ajouter.php"> Ajouter </a></li>
			<li> <a href= "recherche.php"> Recherche </a></li>
			<li> <a href= "imprimer.php"> Imprimer </a></li>
		</ul>
	</div>
	 
	<div id="corps">
		<form action="AjoutBadge.php" method="post" target="_blank">
			<h3> Formulaire pour ajouter une badge :</h3>
			<ul>
				<li>
					<label for="Identifiant :">Identifiant :</label><input type="text" name="identifiant" size="4" maxlength="2"/>
				</li>
				<li>
					<label for="Code d'acces :">Code d'acces :</label><input type="text" name="code_acces" size="3" maxlength="1"/>
				</li>
				<li>
					<label for="Nom :">Nom :</label><input type="text" name="nom" size="10" maxlength="8"/>
				</li>
				<li>
					<label for="Prenom :">Prenom :</label><input type="text" name="prenom" size="10" maxlength="8"/>
				</li>
				<li>
					<label for="Zone Autoriser :">Zone Autoriser :</label><input type="text" name="Zone_Autoriser" size="10" maxlength="8"/>
				</li>
				<li>
					<label for="Date de Cr&#233;ation :">Date de Cr&#233;ation :</label><input type="text" name="date_creation" size="10" maxlength="8"/>
				</li>
					<input type="submit" value="Ajouter" name="valider"/> <input type="reset" value="Effacer tout" name="raz"/>
			</ul>
		</form>
		<br><br><br>
		<!--############### Cette partie du code permet d'afficher le contenue de la table badge ###############-->
		<?php
			include "ConnectBaseDonnee.php";
		
			$id = ConnectBase();
			if($id)
			{
				$select = 'SELECT Identifiant,Code,Nom,Prenom,NumZoneAutorisee,DateCreation FROM badge';
				$result = mysql_query($select,$id) or die ('Erreur : '.mysql_error() );
				$total = mysql_num_rows($result);

				if($total) {
					echo '<table border="3px">'."\n";
					echo '<tr bgcolor="grey">';
					echo '<td>Identifiant</td>';
					echo '<td>Code d&#39;acces</td>';
					echo '<td>Nom</td>';
					echo '<td>Prenom</td>';
					echo '<td>Zone Autoriser</td>';
					echo '<td>Date de creation</td>';
					echo '</tr>'."\n";
					
					while($row = mysql_fetch_array($result)) {
						echo '<tr>';
						echo '<td>'.$row["Identifiant"].'</td>';
						echo '<td>'.$row["Code"].'</td>';
						echo '<td>'.$row["Nom"].'</td>';
						echo '<td>'.$row["Prenom"].'</td>';
						echo '<td>'.$row["NumZoneAutorisee"].'</td>';
						echo '<td>'.$row["DateCreation"].'</td>';
						echo '</tr>';
					}
					echo '</table>';
				}
				else echo 'Pas d\'enregistrements dans cette table...';
				mysql_free_result($result);
			}
		?>
	</div>
	
	<div id="pied_page">
		<h4>Copyright Patchy</h4>
	</div>
	
</body>
</html>