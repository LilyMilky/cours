// Affichage_IHMDlg.cpp : fichier d'impl�mentation
//

#include "stdafx.h"
#include "Affichage_BdD.h"
#include "Affichage_IHMDlg.h"
#include "CBdD.h"
#include <string>
#include <vector>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// bo�te de dialogue CAboutDlg utilis�e pour la bo�te de dialogue '� propos de' pour votre application

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

// Impl�mentation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// bo�te de dialogue CAffichage_IHMDlg




CAffichage_IHMDlg::CAffichage_IHMDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAffichage_IHMDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAffichage_IHMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_Affichage_Donnees);
	DDX_Control(pDX, IDC_Badges, m_Badges);
	DDX_Control(pDX, IDC_Connexion, m_Connexion);
	DDX_Control(pDX, IDC_Zone, m_Zone);
	DDX_Control(pDX, IDC_Horaire, m_Horaire);
}

BEGIN_MESSAGE_MAP(CAffichage_IHMDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_Connexion, &CAffichage_IHMDlg::OnBnClickedConnexion)
	ON_BN_CLICKED(IDC_Badges, &CAffichage_IHMDlg::OnBnClickedBadges)
	ON_BN_CLICKED(IDC_Zone, &CAffichage_IHMDlg::OnBnClickedZone)
	ON_BN_CLICKED(IDC_Horaire, &CAffichage_IHMDlg::OnBnClickedHoraire)
END_MESSAGE_MAP()


// gestionnaires de messages pour CAffichage_IHMDlg

BOOL CAffichage_IHMDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Ajouter l'�l�ment de menu "� propos de..." au menu Syst�me.

	// IDM_ABOUTBOX doit se trouver dans la plage des commandes syst�me.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// D�finir l'ic�ne de cette bo�te de dialogue. L'infrastructure effectue cela automatiquement
	//  lorsque la fen�tre principale de l'application n'est pas une bo�te de dialogue
	SetIcon(m_hIcon, TRUE);			// D�finir une grande ic�ne
	SetIcon(m_hIcon, FALSE);		// D�finir une petite ic�ne

	// TODO : ajoutez ici une initialisation suppl�mentaire

	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

void CAffichage_IHMDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Si vous ajoutez un bouton R�duire � votre bo�te de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'ic�ne. Pour les applications MFC utilisant le mod�le Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CAffichage_IHMDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de p�riph�rique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'ic�ne dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'ic�ne
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Le syst�me appelle cette fonction pour obtenir le curseur � afficher lorsque l'utilisateur fait glisser
//  la fen�tre r�duite.
HCURSOR CAffichage_IHMDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CAffichage_IHMDlg::OnBnClickedConnexion()
{
	bool Valret;
	Valret = BasedeDonnees.Connexion();

	if(Valret)
	{
		AfxMessageBox( "Connexion a la base de donnee ok");

		m_Badges.EnableWindow(true); // Affiche le bouton badge si la connexion est faite
		m_Zone.EnableWindow(true);	// Affiche le bouton zone si la connexion est faite
		m_Horaire.EnableWindow(true);	// Affiche le bouton horaire si la connexion est faite
		m_Connexion.EnableWindow(false);	// N'affiche pas le bouton connexion
										// si la connexion est faite
	}
	else
	{
		AfxMessageBox("Erreur de connexion!!");
	}
}

void CAffichage_IHMDlg::OnBnClickedBadges()
{
	vector <string>::iterator i;
	vector <string> ListeEnreg;

	if (BasedeDonnees.RechercheEnregistrementTable("Badges", &ListeEnreg))
	{
		for (i= ListeEnreg.begin(); i!= ListeEnreg.end(); i++)
		{
				
				m_Affichage_Donnees.AddString(i->c_str()); //Copie de la chaine de caract�re
				
		}	
	}
}

void CAffichage_IHMDlg::OnBnClickedZone()
{
	vector <string>::iterator i;
	vector <string> ListeEnreg;

	if (BasedeDonnees.RechercheEnregistrementTable("Zone", &ListeEnreg))
	{
		for (i= ListeEnreg.begin(); i!= ListeEnreg.end(); i++)
		{
				
				m_Affichage_Donnees.AddString(i->c_str()); //Copie de la chaine de caract�re
				
		}	
	}
}

void CAffichage_IHMDlg::OnBnClickedHoraire()
{
	vector <string>::iterator i;
	vector <string> ListeEnreg;

	if (BasedeDonnees.RechercheEnregistrementTable("Horaire", &ListeEnreg))
	{
		for (i= ListeEnreg.begin(); i!= ListeEnreg.end(); i++)
		{
				
				m_Affichage_Donnees.AddString(i->c_str()); //Copie de la chaine de caract�re
				
		}	
	}
}
