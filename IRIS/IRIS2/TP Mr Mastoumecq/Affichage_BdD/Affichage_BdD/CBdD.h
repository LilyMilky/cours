#pragma once

#include <string.h>
#include <iostream>
using namespace std;

#include <vector>
#include <winsock.h>
#include <mysql.h>

class CBdD
{
private: 
	string Requete;
	string NomServeur;
	string NomUtilisateur;
	string MotDePasse;
	string NomBdD;

	unsigned int PortCom;
	int RequeteNonOK;
	bool Ok;

	MYSQL *mysqlconnexion;
	MYSQL_ROW myROW;
	MYSQL_RES * myRES;

public:
	CBdD(void);
	~CBdD(void);

	bool Connexion(void);

	bool RechercheEnregistrementTable(string Table, vector <string> * ListEnregistrements);

};