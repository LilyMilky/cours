// Affichage_IHMDlg.h : fichier d'en-t�te
//

#pragma once
#include "afxwin.h"
#include "CBdD.h"


// bo�te de dialogue CAffichage_IHMDlg
class CAffichage_IHMDlg : public CDialog
{
// Construction
public:
	CAffichage_IHMDlg(CWnd* pParent = NULL);	// constructeur standard

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_AFFICHAGE_BDD_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Impl�mentation
protected:
	HICON m_hIcon;

	// Fonctions g�n�r�es de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedConnexion();
	afx_msg void OnBnClickedBadges();
	afx_msg void OnBnClickedZone();
	afx_msg void OnBnClickedHoraire();

private:
	CBdD BasedeDonnees;
	CListBox m_Affichage_Donnees;
	CButton m_Badges;
	CButton m_Connexion;
	CButton m_Zone;
	CButton m_Horaire;
};