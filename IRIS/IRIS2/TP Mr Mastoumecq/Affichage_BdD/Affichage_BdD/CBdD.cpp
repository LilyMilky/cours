// CBdD.cpp�: d�finit le point d'entr�e pour l'application console.

#include <iostream>

using namespace std;

#include "stdafx.h"
#include "CBdD.h"
#include <string>

CBdD::CBdD(void)
{
	NomServeur = "192.168.2.24";
	NomUtilisateur = "eleve";
	MotDePasse = "eleve";
	NomBdD = "poussard";
	PortCom = 3306;
}

CBdD::~CBdD(void)
{
}

bool CBdD::Connexion(void)
{
	bool Ok = true;
	mysqlconnexion = mysql_init(NULL);

	if(!mysqlconnexion)
	{
		Ok = false;
	}
	if(!mysql_real_connect(mysqlconnexion, NomServeur.c_str(),  NomUtilisateur.c_str(), MotDePasse.c_str(), NomBdD.c_str(), PortCom, NULL, 0))
	{
		Ok = false;
	}

	return(Ok);
}

bool CBdD::RechercheEnregistrementTable(string NomTable, vector <string> * ListeEnregistrement)
{

	unsigned i,j;

	string Enregistrement = "" ;

	Requete = "select * from " + NomTable;
	RequeteNonOK =mysql_query(mysqlconnexion,Requete.c_str());

	if(RequeteNonOK)
	{
		return false;
	}
	else
	{
		myRES = mysql_store_result(mysqlconnexion);

		if(myRES)
		{
			for(i=0; i < (myRES->row_count); i++)
			{
				myROW = mysql_fetch_row(myRES);

				for(j=0; j < mysql_num_fields(myRES); j++)
				{
					Enregistrement += myROW[j];
					Enregistrement += " ";
				}

				ListeEnregistrement->push_back(Enregistrement);

				Enregistrement = "";
			}
			 mysql_free_result(myRES);
		}
	}
	mysql_close(mysqlconnexion);

	return(true);
}