// Affichage_BdD.h�: fichier d'en-t�te principal pour l'application PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "incluez 'stdafx.h' avant d'inclure ce fichier pour PCH"
#endif

#include "resource.h"		// symboles principaux


// CAffichage_BdDApp:
// Consultez Affichage_BdD.cpp pour l'impl�mentation de cette classe
//

class CAffichage_BdDApp : public CWinApp
{
public:
	CAffichage_BdDApp();

// Substitutions
	public:
	virtual BOOL InitInstance();

// Impl�mentation

	DECLARE_MESSAGE_MAP()
};

extern CAffichage_BdDApp theApp;