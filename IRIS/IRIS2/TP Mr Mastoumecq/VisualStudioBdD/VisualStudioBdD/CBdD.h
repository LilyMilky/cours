#pragma once

#include "string.h"
#include <vector>
#include <iostream>
using namespace std;
#include <winsock.h>
#include <mysql.h>

class CBdD
{
	private:
		string NomServeur;
		string NomUtilisateur;
		string MotDePasse;
		string NomBdD;
		unsigned int PortCom;
		MYSQL *mysqlconnexion;
		MYSQL_ROW MyROW;
		MYSQL_RES *MyRES;
		string Requete;
		int RequeteNonOK;
		bool OK;

	public:
		CBdD(void);
		~CBdD(void);
		bool Connexion(void);
		bool RechercheEnregistrementTable(string Table, vector <string> *ListeEnregistrement);
};
