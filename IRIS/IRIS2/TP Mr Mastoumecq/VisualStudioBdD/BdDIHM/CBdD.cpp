#include "stdafx.h"
#include "CBdD.h"

CBdD::CBdD(void)
{
	NomServeur = "192.168.2.5";
	NomUtilisateur = "eleve";
	MotDePasse = "eleve";
	NomBdD = "lacquehay";
	PortCom = 3306;
	mysqlconnexion = mysql_init(NULL);
}

CBdD::~CBdD(void)
{
}

bool CBdD::Connexion(void)
{
	bool OK = true;

	if (!mysqlconnexion)
		OK = false;

	if (!mysql_real_connect(mysqlconnexion,NomServeur.c_str(),NomUtilisateur.c_str(),MotDePasse.c_str(),NomBdD.c_str(),PortCom,NULL,0))
		OK = false;

	return OK;
}

bool CBdD::RechercheEnregistrementTable(string Table, vector <string> *ListeEnregistrement)
{
	unsigned int i,j;
	bool OK = false;
	Requete = "select * from ";
	Requete += Table;
	RequeteNonOK = mysql_query(mysqlconnexion,Requete.c_str());
	string Enregistrement = "";

	if(RequeteNonOK)
		OK = false;
	else
	{	
		MyRES = mysql_store_result(mysqlconnexion);

		if (MyRES)
		{
			for(i = 0 ; i < (MyRES -> row_count) ; i++)
			{
				MyROW = mysql_fetch_row(MyRES);
				for(j = 0 ; j < mysql_num_fields(MyRES) ; j++)
				{
					//cout << MyROW[j]<< " ";	
					Enregistrement += MyROW[j];	
					Enregistrement += " ";
				}
				//cout << endl;
				ListeEnregistrement->push_back(Enregistrement); //Enregistrement BdD dans ListeEnregistrement
				Enregistrement = "";
			}
			mysql_free_result(MyRES);
		}
		mysql_close(mysqlconnexion);
		OK = true;
	}
	return OK;
}

