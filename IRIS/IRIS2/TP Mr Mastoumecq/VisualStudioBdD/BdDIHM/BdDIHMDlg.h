// BdDIHMDlg.h : fichier d'en-t�te
//

#pragma once
#include "afxwin.h"


// bo�te de dialogue CBdDIHMDlg
class CBdDIHMDlg : public CDialog
{
// Construction
public:
	CBdDIHMDlg(CWnd* pParent = NULL);	// constructeur standard

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_BDDIHM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Impl�mentation
protected:
	HICON m_hIcon;

	// Fonctions g�n�r�es de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnEnChangeConnexion();
	afx_msg void OnBnClickedButtonConnection();
private:
	CComboBox m_ChoixTable;
	CListBox m_ListBox;
	CButton m_IDOK;
};
