/*********************************************************
Yann Lacquehay
Allumage Del
SW6 : PortD on
SW9 : Tout off
*********************************************************/

// Initialisation Port

void Init ()
{
 PORTD = 0;                                     //Mise � 0 avant envoi
 TRISD = 0;                                     //Sortie
 TRISA = 0xFF;
 ADCON1= 0x07;
}

// Main

void main ()
{
 Init();
 while(1)
 {
        switch (PORTA)
        {
               case 0 : PortD = 0x00;
                        break;
               case 1 : PortD = 0x01;
                        break;
               case 2 : PortD = 0x02;
                        break;
               case 3 : PortD = 0x04;
                        break;
               case 4 : PortD = 0x08;
                        break;
               case 5 : PortD = 0x10;
                        break;
               case 6 : PortD = 0x20;
                        break;
               case 7 : PortD = 0x40;
                        break;
               case 8 : PortD = 0x80;
                        break;
               default : PortD = 0x00;
         }
 }
}

