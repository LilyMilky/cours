#line 1 "C:/BTS2/Lacquehay/MikroC/Exercice9b.c"
#line 6 "C:/BTS2/Lacquehay/MikroC/Exercice9b.c"
unsigned short int Pression = 0;
const unsigned short int Convert7Seg[10]= {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};


void Init ()
{
 PORTD = 0;
 TRISD = 0;
 ADCON1= 0x07;
 TrisB = 0x01;
 PORTC = 0;
 TRISC = 0;
 TRISA = 0x07;
}

void Init_Interrupt()
{
 INTCON = 0x90;
 OPTION_REG = 0xFF;
}

void interrupt ()
{
 unsigned short int Centaine,Dizaine,Unite;

 if (INTCON.INTF)
 {
 Pression++;
 if (Pression > 255)
 {
 Pression = 0;
 }

 PortC = Pression;

 Centaine = Pression/100;
 PortD = Convert7Seg[Centaine];
 PortA = 0x04;

 Dizaine = (Pression-(100*Centaine))/10;
 PortD = Convert7Seg[Dizaine];
 PortA = 0x02;

 Unite = Pression -(100*Centaine)-(10*Dizaine);
 PortD = Convert7Seg[Unite];
 PortA = 0x01;
 }

 delay_ms(200);
 INTCON = 0x90;

}



void main ()
{
 long int i;
 Init();
 Init_Interrupt();

 while(1)
 {
 for (i = 0; i < 55000 ; i++)
 PortD = 0xFF;

 for (i = 0; i < 55000 ; i++)
 PortD = 0x00;
 }
}
