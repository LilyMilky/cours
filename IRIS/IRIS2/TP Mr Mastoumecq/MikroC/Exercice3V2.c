/*********************************************************
Yann Lacquehay
Allumage Del
SW6 : PortD on
SW9 : Tout off
*********************************************************/

// Initialisation Port

void Init ()
{
 PORTD = 0;                                     //Mise � 0 avant envoi
 TRISD = 0;                                     //Sortie
 TRISA = 0xFF;                                  //Entr�e PORTA 1 � 5 car bouton
 ADCON1= 0x07;
}

// Main

void main ()
{
 Init();
 while(1)
 {
     PORTD = 1 << PORTA -1  ;
 }
}
