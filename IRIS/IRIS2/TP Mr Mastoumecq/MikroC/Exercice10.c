/*********************************************************
Yann Lacquehay
Afficheur 7 segments
*********************************************************/

long int Compteur = 0;
/************************************************************
*                     FONCTION INITIALISATION               *
************************************************************/
void init(void)
{
     PORTB = 0;     // La valeur par defaut du PORTB est 0
     TRISB = 0xFF;  // Le PORTB est en entrée
     
     PORTC = 0;
     TRISC = 0;
     
     ADCON1 = 0x07; // ADCON permet d'utiliser le PORTA en digital, il faut que
  			            // sa valeur soit à 7
     PORTA = 1;
     TRISA = 0;     // Le PORTA est en sortie
     
     PORTD = 0;
     TRISD = 0;


     INTCON = 0xA0;
     OPTION_REG = 0x87;

 }
/************************************************************
*                      FONCTION INTERRUPT                   *
************************************************************/
void interrupt(void)
{
     Compteur ++;
     if (Compteur == 15)
     {
        if (PORTC == 0xFF)
        {
           PORTC = 0x00;
        }
        else
        {
           PORTC = 0xFF;
        }
        Compteur = 0;
     }
     INTCON = 0xA0;

}
/************************************************************
*                     PROGRAMME PRINCIPALE                  *
************************************************************/
void main(void)
{
     init();
     while (1)
     {
           switch(PORTB)
           {
              case 0x00 : PORTD = 0x3F; // Permet d'afficher '0' sur l'afficheur 7 segments
              break;
              case 0x01 : PORTD = 0x06; // Permet d'afficher '1' sur l'afficheur 7 segments
              break;
              case 0x02 : PORTD = 0x5B; // Permet d'afficher '2' sur l'afficheur 7 segments
              break;
              case 0x03 : PORTD = 0x4F; // Permet d'afficher '3' sur l'afficheur 7 segments
              break;
              case 0x04 : PORTD = 0x66; // Permet d'afficher '4' sur l'afficheur 7 segments
              break;
              case 0x05 : PORTD = 0x6D; // Permet d'afficher '5' sur l'afficheur 7 segments
              break;
              case 0x06 : PORTD = 0x7D; // Permet d'afficher '6' sur l'afficheur 7 segments
              break;
              case 0x07 : PORTD = 0x07; // Permet d'afficher '7' sur l'afficheur 7 segments
              break;
              case 0x08 : PORTD = 0x7F; // Permet d'afficher '8' sur l'afficheur 7 segments
              break;
              case 0x09 : PORTD = 0x6F; // Permet d'afficher '9' sur l'afficheur 7 segments
              break;
          }
    }

}