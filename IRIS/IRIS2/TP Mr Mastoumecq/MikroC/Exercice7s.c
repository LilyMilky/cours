/*********************************************************
Yann Lacquehay
Afficheur 7 segments
*********************************************************/
const unsigned short int Convert7Seg[10]= {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};

void Init ()
{
   PortD = 0;
   TRISD = 0;
   PortA = 0x07;
   TRISA = 0;
}
 void main ()
{
  unsigned short int D,Centaine,Dizaine,Unite;
  Init();

  while(1)
  {
    D = PortB;

    Centaine = D/100;
    PortD = Convert7Seg[Centaine];
    PortA = 0x04;

    Dizaine = (D-(100*Centaine))/10;
    PortD = Convert7Seg[Dizaine];
    PortA = 0x02;

    Unite = D-(100*Centaine)-(10*Dizaine);
    PortD = Convert7Seg[Unite];
    PortA = 0x01;
  }
}
