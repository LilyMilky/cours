/*********************************************************
Yann Lacquehay
Afficheur LED
*********************************************************/
#define RS PORTB.F4							//Defini le bit4 du PortB avec le nom RS
#define E PortB.F5							//Defini le bit5 du PortB avec le nom E
#define DataLCD PortB						//Defini le PortB avec le nom DATALCD

void Init()
{
     PORTB = 0;
     TrisB = 0;                             //PortB en sortie
}

 void ValidCommand()
{
     E = 1;
     delay_us(600);
     E = 0;
}

void EnvoiInstructionLCD(unsigned short int Commande)
{
     unsigned short int Quartet = 0;
     RS = 0;

     Quartet = (Commande & 0b11110000);      //Masque sur le 1er Quartet
     Quartet = Quartet >> 4;
     DataLCD = Quartet;
     RS = 0;
     ValidCommand();

     Quartet = (Commande & 0b00001111);      //Masque sur le 2ème Quartet
     DataLCD = Quartet;
     RS = 0;
     ValidCommand();
}

void InitLCD()
{
     RS = 0;

     delay_ms(15);
     DataLCD = 0x3;                          //Initialisation
     ValidCommand();
     delay_ms(5);
     ValidCommand();
     delay_ms(5);
     ValidCommand();
     delay_ms(5);

     DataLCD = 0x2;                           //Mode 4bits
     ValidCommand();
     
     EnvoiInstructionLCD(0x28);               //2 lignes, matrice de 8x5
     EnvoiInstructionLCD(0x0D);               //Active l'ecran + Curseur Nonvisible + Clignotement du caractere
     EnvoiInstructionLCD(0x01);               //Effacement écran
     EnvoiInstructionLCD(0x06);               //Deplace le curseur ・droite apres affichage d'un caractere
}

void EnvoiCaractere (unsigned char Lettre)
{
     unsigned char Quartet = 0  ;


     Quartet = (Lettre & 0b11110000);      //Masque sur le 1er Quartet
     Quartet = Quartet >> 4;
     DataLCD = Quartet;
     RS = 1;                               //Envoi donnée
     ValidCommand();
     
     Quartet = (Lettre & 0b00001111);      //Masque sur le 2ème Quartet
     DataLCD = Quartet;
     RS = 1;
     ValidCommand();
     
     //DataLCD = Lettre;
}

void EnvoiMessage(unsigned short int Ligne, unsigned short int Colonne, unsigned char *Message)
{
     RS = 1;
     
     if (Ligne == 1)			//1ère Ligne
                    EnvoiInstructionLCD(0x80 + Colonne);
           
     else if (Ligne == 2)		//2ème Ligne
                    EnvoiInstructionLCD(0xC0 + Colonne);
           
     else
                    EnvoiCaractere('X') ;		//Erreur
           
     while(*Message != 0)
     {
                    EnvoiCaractere(*Message);
                    *Message ++;
     }

}

void main()
{
     Init();
     InitLCD();
     unsigned char Message1[16] = "Bienvenue en";
     unsigned char Message2[16] = "BTS IRIS";


     EnvoieMessage(0,2,Message1);   //On envoie le message 1  sur la ligne 1 à la colonne 2
     EnvoieMessage(1,3,Message2);  //On envoie le message 2 sur la ligne 2 à la colonne 3
     
    //EnvoiInstructionLCD(0x82);                //1ere ligne 3eme case
    // EnvoiCaractere('H');
    // EnvoiCaractere('P');
    // EnvoiCaractere('=');
    // EnvoiCaractere('0');
    //EnvoiInstructionLCD(0xC2);                //2eme ligne 3eme cases
    // EnvoiMessage(2,1,"Coucou");
     
}