#line 1 "D:/BTS2/LACQUEHAY/MikroC/Exercice13.c"
#line 9 "D:/BTS2/LACQUEHAY/MikroC/Exercice13.c"
char LSB,MSB;


void interrupt()
{
 if(INTCON.PEIE)
 {
 LSB = ADRESL;
 MSB = ADRESH;

 PIR1 = 0b01000000;
 ADCON0 = 0b10000100;
 INTCON = 0b11000000;
 }
}



void Init()
{
 PORTB = 0;
 TrisB = 0;

 INTCON = 0b11000000;
 PIE1 = 0b01000000;

 PortA = 0xff;
 TrisA = 1;
}


void INITCAN ()
{
 ADCON1 = 0b10001110;

 ADCON0 = 0b10000000;
 delay_us(30);
 ADCON0 = 0b10000100;
}


 void ValidCommand()
{
  PortB.F5  = 1;
 delay_us(600);
  PortB.F5  = 0;
}


void EnvoiInstructionLCD(unsigned short int Commande)
{
 unsigned short int Quartet = 0;
  PORTB.F4  = 0;

 Quartet = (Commande & 0b11110000);
 Quartet = Quartet >> 4;
  PortB  = Quartet;
  PORTB.F4  = 0;
 ValidCommand();

 Quartet = (Commande & 0b00001111);
  PortB  = Quartet;
  PORTB.F4  = 0;
 ValidCommand();
}


void InitLCD()
{
  PORTB.F4  = 0;

 delay_ms(15);
  PortB  = 0x3;
 ValidCommand();
 delay_ms(5);
 ValidCommand();
 delay_ms(5);
 ValidCommand();
 delay_ms(5);

  PortB  = 0x2;
 ValidCommand();

 EnvoiInstructionLCD(0x28);
 EnvoiInstructionLCD(0x0D);
 EnvoiInstructionLCD(0x01);
 EnvoiInstructionLCD(0x06);
}


void EnvoiCaractere (unsigned char Lettre)
{
 unsigned char Quartet = 0 ;

 Quartet = (Lettre & 0b11110000);
 Quartet = Quartet >> 4;
  PortB  = Quartet;
  PORTB.F4  = 1;
 ValidCommand();

 Quartet = (Lettre & 0b00001111);
  PortB  = Quartet;
  PORTB.F4  = 1;
 ValidCommand();
}


void EnvoiMessage(unsigned short int Ligne, unsigned short int Colonne, unsigned char *Message)
{
  PORTB.F4  = 1;

 if (Ligne == 0)
 EnvoiInstructionLCD(0x80 + Colonne);

 else if (Ligne == 1)
 EnvoiInstructionLCD(0xC0 + Colonne);

 while(*Message != 0)
 {
 EnvoiCaractere(*Message);
 *Message ++;
 }
}


void main()
{
 char Message1[6] = "MSB:";
 char Message2[6] = "LSB:";
 char Message3[7] = "Volt:";
 int Volt,Bit0,Bit1,Bit2,Bit3,Bit4,Bit5,Bit6,Bit7,L,M;

 Init();
 InitLCD();
 INITCAN();

 do
 {
#line 182 "D:/BTS2/LACQUEHAY/MikroC/Exercice13.c"
 EnvoiMessage(0,0,Message1);
 EnvoiMessage(0,4,MSB);
 EnvoiMessage(0,8,Message2);
 EnvoiMessage(0,12,LSB);
 EnvoiMessage(1,3,Message3);
 EnvoiMessage(1,9,Volt);
 }while(1);
}
