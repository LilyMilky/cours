;  ASM code generated by mikroVirtualMachine for PIC - V. 8.0.0.0
;  Date/Time: 08/10/2010 09:22:57
;  Info: http://www.mikroe.com


; ADDRESS	OPCODE	ASM
; ----------------------------------------------
$0000	$280E			GOTO	_main
$0004	$	_Init:
;Exercice3.c,10 :: 		void Init ()
;Exercice3.c,12 :: 		PORTD = 0;                                     //Mise � 0 avant envoi
$0004	$1303			BCF	STATUS, RP1
$0005	$1283			BCF	STATUS, RP0
$0006	$0188			CLRF	PORTD, 1
;Exercice3.c,13 :: 		TRISD = 0;                                     //Sortie
$0007	$1683			BSF	STATUS, RP0
$0008	$0188			CLRF	TRISD, 1
;Exercice3.c,14 :: 		TRISA = 0xFF;                                  //Entr�e PORTA 1 � 5 car bouton
$0009	$30FF			MOVLW	255
$000A	$0085			MOVWF	TRISA
;Exercice3.c,15 :: 		ADCON1= 0x07;
$000B	$3007			MOVLW	7
$000C	$009F			MOVWF	ADCON1
;Exercice3.c,16 :: 		}
$000D	$0008			RETURN
$000E	$	_main:
;Exercice3.c,20 :: 		void main ()
;Exercice3.c,22 :: 		Init();
$000E	$2004			CALL	_Init
;Exercice3.c,23 :: 		while(1)
$000F	$	L_main_0:
;Exercice3.c,25 :: 		switch (PORTA)
$000F	$1303			BCF	STATUS, RP1
$0010	$1683			BSF	STATUS, RP0
$0011	$282F			GOTO	L_main_2
;Exercice3.c,27 :: 		case 0 : PortD = 0x00;
$0012	$	L_main_4:
$0012	$1283			BCF	STATUS, RP0
$0013	$0188			CLRF	PORTD, 1
;Exercice3.c,28 :: 		break;
$0014	$2856			GOTO	L_main_3
;Exercice3.c,29 :: 		case 1 : PortD = 0x01;
$0015	$	L_main_5:
$0015	$3001			MOVLW	1
$0016	$0088			MOVWF	PORTD
;Exercice3.c,30 :: 		break;
$0017	$2856			GOTO	L_main_3
;Exercice3.c,31 :: 		case 2 : PortD = 0x02;
$0018	$	L_main_6:
$0018	$3002			MOVLW	2
$0019	$0088			MOVWF	PORTD
;Exercice3.c,32 :: 		break;
$001A	$2856			GOTO	L_main_3
;Exercice3.c,33 :: 		case 3 : PortD = 0x04;
$001B	$	L_main_7:
$001B	$3004			MOVLW	4
$001C	$0088			MOVWF	PORTD
;Exercice3.c,34 :: 		break;
$001D	$2856			GOTO	L_main_3
;Exercice3.c,35 :: 		case 4 : PortD = 0x08;
$001E	$	L_main_8:
$001E	$3008			MOVLW	8
$001F	$0088			MOVWF	PORTD
;Exercice3.c,36 :: 		break;
$0020	$2856			GOTO	L_main_3
;Exercice3.c,37 :: 		case 5 : PortD = 0x10;
$0021	$	L_main_9:
$0021	$3010			MOVLW	16
$0022	$0088			MOVWF	PORTD
;Exercice3.c,38 :: 		break;
$0023	$2856			GOTO	L_main_3
;Exercice3.c,39 :: 		case 6 : PortD = 0x20;
$0024	$	L_main_10:
$0024	$3020			MOVLW	32
$0025	$0088			MOVWF	PORTD
;Exercice3.c,40 :: 		break;
$0026	$2856			GOTO	L_main_3
;Exercice3.c,41 :: 		case 7 : PortD = 0x40;
$0027	$	L_main_11:
$0027	$3040			MOVLW	64
$0028	$0088			MOVWF	PORTD
;Exercice3.c,42 :: 		break;
$0029	$2856			GOTO	L_main_3
;Exercice3.c,43 :: 		case 8 : PortD = 0x80;
$002A	$	L_main_12:
$002A	$3080			MOVLW	128
$002B	$0088			MOVWF	PORTD
;Exercice3.c,44 :: 		break;
$002C	$2856			GOTO	L_main_3
;Exercice3.c,45 :: 		default : PortD = 0x00;
$002D	$	L_main_13:
$002D	$0188			CLRF	PORTD, 1
;Exercice3.c,46 :: 		}
$002E	$2856			GOTO	L_main_3
$002F	$	L_main_2:
$002F	$1303			BCF	STATUS, RP1
$0030	$1283			BCF	STATUS, RP0
$0031	$0805			MOVF	PORTA, 0
$0032	$3A00			XORLW	0
$0033	$1903			BTFSC	STATUS, Z
$0034	$2812			GOTO	L_main_4
$0035	$0805			MOVF	PORTA, 0
$0036	$3A01			XORLW	1
$0037	$1903			BTFSC	STATUS, Z
$0038	$2815			GOTO	L_main_5
$0039	$0805			MOVF	PORTA, 0
$003A	$3A02			XORLW	2
$003B	$1903			BTFSC	STATUS, Z
$003C	$2818			GOTO	L_main_6
$003D	$0805			MOVF	PORTA, 0
$003E	$3A03			XORLW	3
$003F	$1903			BTFSC	STATUS, Z
$0040	$281B			GOTO	L_main_7
$0041	$0805			MOVF	PORTA, 0
$0042	$3A04			XORLW	4
$0043	$1903			BTFSC	STATUS, Z
$0044	$281E			GOTO	L_main_8
$0045	$0805			MOVF	PORTA, 0
$0046	$3A05			XORLW	5
$0047	$1903			BTFSC	STATUS, Z
$0048	$2821			GOTO	L_main_9
$0049	$0805			MOVF	PORTA, 0
$004A	$3A06			XORLW	6
$004B	$1903			BTFSC	STATUS, Z
$004C	$2824			GOTO	L_main_10
$004D	$0805			MOVF	PORTA, 0
$004E	$3A07			XORLW	7
$004F	$1903			BTFSC	STATUS, Z
$0050	$2827			GOTO	L_main_11
$0051	$0805			MOVF	PORTA, 0
$0052	$3A08			XORLW	8
$0053	$1903			BTFSC	STATUS, Z
$0054	$282A			GOTO	L_main_12
$0055	$282D			GOTO	L_main_13
$0056	$	L_main_3:
;Exercice3.c,47 :: 		}
$0056	$280F			GOTO	L_main_0
;Exercice3.c,48 :: 		}
$0057	$2857			GOTO	$
