/*********************************************************
Yann Lacquehay
Afficheur 7 segments
*********************************************************/

#include "Convert7Seg.h"

void Init ()
{
   PortD = 0;
   TRISD = 0;
   PortA = 0x07;
   TRISA = 0;
}
 void main ()
{
  unsigned short int D,Centaine,Dizaine,Unite;
  Init();
  
  while(1)
  {
    D = PortB;
    
    Centaine = D/100;
    PortD = Convert7Seg(Centaine);
    PortA = 0x04;

    Dizaine = (D-(100*Centaine))/10;
    PortD = Convert7Seg(Dizaine);
    PortA = 0x02;

    Unite = D-(100*Centaine)-(10*Dizaine);
    PortD = Convert7Seg(Unite);
    PortA = 0x01;

  }
}