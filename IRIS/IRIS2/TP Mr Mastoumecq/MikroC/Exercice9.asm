;  ASM code generated by mikroVirtualMachine for PIC - V. 8.0.0.0
;  Date/Time: 05/11/2010 09:58:13
;  Info: http://www.mikroe.com


; ADDRESS	OPCODE	ASM
; ----------------------------------------------
$0000	$285E			GOTO	_main
$0004	$	_interrupt:
$0004	$00FF			MOVWF	STACK_15
$0005	$0E03			SWAPF	STATUS, 0
$0006	$0183			CLRF	STATUS
$0007	$00A2			MOVWF	?saveSTATUS
$0008	$0804			MOVF	FSR, 0
$0009	$00A1			MOVWF	?saveFSR
$000A	$080A			MOVF	PCLATH, 0
$000B	$00A3			MOVWF	?savePCLATH
$000C	$018A			CLRF	PCLATH
$000D	$0870			MOVF	STACK_0, 0
$000E	$00A8			MOVWF	STSAVED_0
;Exercice9.c,28 :: 		void interrupt ()
;Exercice9.c,30 :: 		if (INTCON.INTF)                         //Test Interruption RB0
$000F	$1C8B			BTFSS	INTCON, 1
$0010	$2819			GOTO	L_interrupt_0
;Exercice9.c,32 :: 		Pression++;
$0011	$0AA0			INCF	_Pression, 1
;Exercice9.c,33 :: 		if (Pression > 255)
$0012	$0820			MOVF	_Pression, 0
$0013	$3CFF			SUBLW	255
$0014	$1803			BTFSC	STATUS, C
$0015	$2817			GOTO	L_interrupt_1
;Exercice9.c,35 :: 		Pression = 0;
$0016	$01A0			CLRF	_Pression, 1
;Exercice9.c,36 :: 		}
$0017	$	L_interrupt_1:
;Exercice9.c,37 :: 		PortC = Pression;
$0017	$0820			MOVF	_Pression, 0
$0018	$0087			MOVWF	PORTC
;Exercice9.c,38 :: 		}
$0019	$	L_interrupt_0:
;Exercice9.c,40 :: 		delay_ms(100);  //Tempo pour eviter le ph�nom�ne de rebond
$0019	$3002			MOVLW	2
$001A	$00FC			MOVWF	STACK_12
$001B	$30FF			MOVLW	255
$001C	$00FB			MOVWF	STACK_11
$001D	$30FF			MOVLW	255
$001E	$00FA			MOVWF	STACK_10
$001F	$0BFC			DECFSZ	STACK_12, F
$0020	$2822			GOTO	$+2
$0021	$2829			GOTO	$+8
$0022	$0BFB			DECFSZ	STACK_11, F
$0023	$2825			GOTO	$+2
$0024	$2828			GOTO	$+4
$0025	$0BFA			DECFSZ	STACK_10, F
$0026	$2825			GOTO	$-1
$0027	$2822			GOTO	$-5
$0028	$281F			GOTO	$-9
$0029	$3006			MOVLW	6
$002A	$00FB			MOVWF	STACK_11
$002B	$30FF			MOVLW	255
$002C	$00FA			MOVWF	STACK_10
$002D	$0BFB			DECFSZ	STACK_11, F
$002E	$2830			GOTO	$+2
$002F	$2833			GOTO	$+4
$0030	$0BFA			DECFSZ	STACK_10, F
$0031	$2830			GOTO	$-1
$0032	$282D			GOTO	$-5
$0033	$300A			MOVLW	10
$0034	$00FA			MOVWF	STACK_10
$0035	$0BFA			DECFSZ	STACK_10, F
$0036	$2835			GOTO	$-1
;Exercice9.c,41 :: 		INTCON = 0x90;   //Remise du flag � 0 afin de pouvoir relancer l'interruption
$0037	$3090			MOVLW	144
$0038	$008B			MOVWF	INTCON
;Exercice9.c,43 :: 		}
$0039	$	L_Interrupt_end:
$0039	$0828			MOVF	STSAVED_0, 0
$003A	$00F0			MOVWF	STACK_0
$003B	$0823			MOVF	?savePCLATH, 0
$003C	$008A			MOVWF	PCLATH
$003D	$0821			MOVF	?saveFSR, 0
$003E	$0084			MOVWF	FSR
$003F	$0E22			SWAPF	?saveSTATUS, 0
$0040	$0083			MOVWF	STATUS
$0041	$0EFF			SWAPF	STACK_15, 1
$0042	$0E7F			SWAPF	STACK_15, 0
$0043	$0009			RETFIE
$0044	$	_Init:
;Exercice9.c,12 :: 		void Init ()
;Exercice9.c,14 :: 		PORTD = 0;                                     //Mise � 0 avant envoi
$0044	$1303			BCF	STATUS, RP1
$0045	$1283			BCF	STATUS, RP0
$0046	$0188			CLRF	PORTD, 1
;Exercice9.c,15 :: 		TRISD = 0;                                     //Sortie
$0047	$1683			BSF	STATUS, RP0
$0048	$0188			CLRF	TRISD, 1
;Exercice9.c,16 :: 		ADCON1= 0x07;
$0049	$3007			MOVLW	7
$004A	$009F			MOVWF	ADCON1
;Exercice9.c,17 :: 		TrisB = 0x01;                                  //PortB en entr�e
$004B	$3001			MOVLW	1
$004C	$0086			MOVWF	TRISB
;Exercice9.c,18 :: 		PORTC = 0;
$004D	$1283			BCF	STATUS, RP0
$004E	$0187			CLRF	PORTC, 1
;Exercice9.c,19 :: 		TRISC = 0;                                     //PortC en sortie
$004F	$1683			BSF	STATUS, RP0
$0050	$0187			CLRF	TRISC, 1
;Exercice9.c,20 :: 		}
$0051	$0008			RETURN
$0052	$	_Init_Interrupt:
;Exercice9.c,22 :: 		void Init_Interrupt()
;Exercice9.c,24 :: 		INTCON = 0x90;
$0052	$3090			MOVLW	144
$0053	$008B			MOVWF	INTCON
;Exercice9.c,25 :: 		OPTION_REG = 0xFF;
$0054	$30FF			MOVLW	255
$0055	$1303			BCF	STATUS, RP1
$0056	$1683			BSF	STATUS, RP0
$0057	$0081			MOVWF	OPTION_REG
;Exercice9.c,26 :: 		}
$0058	$0008			RETURN
$0059	$	GlobalIniExercice9:
$0059	$3000			MOVLW	0
$005A	$1303			BCF	STATUS, RP1
$005B	$1283			BCF	STATUS, RP0
$005C	$00A0			MOVWF	_Pression+0
$005D	$0008			RETURN
$005E	$	_main:
;Exercice9.c,47 :: 		void main ()
;Exercice9.c,50 :: 		Init();
$005E	$2059			CALL	GlobalIniExercice9
$005F	$2044			CALL	_Init
;Exercice9.c,51 :: 		Init_Interrupt();
$0060	$2052			CALL	_Init_Interrupt
;Exercice9.c,53 :: 		while(1)
$0061	$	L_main_2:
;Exercice9.c,55 :: 		for (i = 0; i < 55000 ; i++)
$0061	$1283			BCF	STATUS, RP0
$0062	$01A4			CLRF	main_i_L0
$0063	$01A5			CLRF	main_i_L0+1
$0064	$01A6			CLRF	main_i_L0+2
$0065	$01A7			CLRF	main_i_L0+3
$0066	$	L_main_4:
$0066	$3080			MOVLW	128
$0067	$0627			XORWF	main_i_L0+3, 0
$0068	$00F0			MOVWF	STACK_0
$0069	$3080			MOVLW	128
$006A	$0270			SUBWF	STACK_0, 0
$006B	$1D03			BTFSS	STATUS, Z
$006C	$2877			GOTO	L_main_10
$006D	$3000			MOVLW	0
$006E	$0226			SUBWF	main_i_L0+2, 0
$006F	$1D03			BTFSS	STATUS, Z
$0070	$2877			GOTO	L_main_10
$0071	$30D6			MOVLW	214
$0072	$0225			SUBWF	main_i_L0+1, 0
$0073	$1D03			BTFSS	STATUS, Z
$0074	$2877			GOTO	L_main_10
$0075	$30D8			MOVLW	216
$0076	$0224			SUBWF	main_i_L0, 0
$0077	$	L_main_10:
$0077	$1803			BTFSC	STATUS, C
$0078	$2883			GOTO	L_main_5
;Exercice9.c,56 :: 		PortD = 0xFF;
$0079	$30FF			MOVLW	255
$007A	$0088			MOVWF	PORTD
$007B	$	L_main_6:
;Exercice9.c,55 :: 		for (i = 0; i < 55000 ; i++)
$007B	$0AA4			INCF	main_i_L0, 1
$007C	$1903			BTFSC	STATUS, Z
$007D	$0AA5			INCF	main_i_L0+1, 1
$007E	$1903			BTFSC	STATUS, Z
$007F	$0AA6			INCF	main_i_L0+2, 1
$0080	$1903			BTFSC	STATUS, Z
$0081	$0AA7			INCF	main_i_L0+3, 1
;Exercice9.c,56 :: 		PortD = 0xFF;
$0082	$2866			GOTO	L_main_4
$0083	$	L_main_5:
;Exercice9.c,58 :: 		for (i = 0; i < 55000 ; i++)
$0083	$01A4			CLRF	main_i_L0
$0084	$01A5			CLRF	main_i_L0+1
$0085	$01A6			CLRF	main_i_L0+2
$0086	$01A7			CLRF	main_i_L0+3
$0087	$	L_main_7:
$0087	$3080			MOVLW	128
$0088	$0627			XORWF	main_i_L0+3, 0
$0089	$00F0			MOVWF	STACK_0
$008A	$3080			MOVLW	128
$008B	$0270			SUBWF	STACK_0, 0
$008C	$1D03			BTFSS	STATUS, Z
$008D	$2898			GOTO	L_main_11
$008E	$3000			MOVLW	0
$008F	$0226			SUBWF	main_i_L0+2, 0
$0090	$1D03			BTFSS	STATUS, Z
$0091	$2898			GOTO	L_main_11
$0092	$30D6			MOVLW	214
$0093	$0225			SUBWF	main_i_L0+1, 0
$0094	$1D03			BTFSS	STATUS, Z
$0095	$2898			GOTO	L_main_11
$0096	$30D8			MOVLW	216
$0097	$0224			SUBWF	main_i_L0, 0
$0098	$	L_main_11:
$0098	$1803			BTFSC	STATUS, C
$0099	$28A3			GOTO	L_main_8
;Exercice9.c,59 :: 		PortD = 0x00;
$009A	$0188			CLRF	PORTD, 1
$009B	$	L_main_9:
;Exercice9.c,58 :: 		for (i = 0; i < 55000 ; i++)
$009B	$0AA4			INCF	main_i_L0, 1
$009C	$1903			BTFSC	STATUS, Z
$009D	$0AA5			INCF	main_i_L0+1, 1
$009E	$1903			BTFSC	STATUS, Z
$009F	$0AA6			INCF	main_i_L0+2, 1
$00A0	$1903			BTFSC	STATUS, Z
$00A1	$0AA7			INCF	main_i_L0+3, 1
;Exercice9.c,59 :: 		PortD = 0x00;
$00A2	$2887			GOTO	L_main_7
$00A3	$	L_main_8:
;Exercice9.c,60 :: 		}
$00A3	$2861			GOTO	L_main_2
;Exercice9.c,61 :: 		}
$00A4	$28A4			GOTO	$
