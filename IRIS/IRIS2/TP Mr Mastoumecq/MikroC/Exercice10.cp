#line 1 "C:/BTS2/Lacquehay Yann/MikroC/Exercice10.c"
#line 6 "C:/BTS2/Lacquehay Yann/MikroC/Exercice10.c"
unsigned short int Time = 0;


void interrupt()
{
 if(INTCON.T0IF)
 {
 Time++;
 if(Time == 15)
 {
 Time = 0;
 if(PortC == 0x00)
 PortC = 0xFF;
 else
 PortC = 0x00;
 }
 }
 INTCON = 0xA0;
}

void Init ()
{
 PORTD = 0;
 TRISD = 0;
 ADCON1= 0x07;
 TrisB = 0xFF;
 PORTC = 0xFF;
 TRISC = 0;
 PortA = 0x07;
 TRISA = 0;
}

void Init_Interrupt()
{
 INTCON = 0xA0;
 OPTION_REG = 0x87;
 TMR0 = 0;
}



void main ()
{
 const unsigned short int Convert7Seg[10]= {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};
 unsigned short int Affiche = 0;
 Init();
 Init_Interrupt();

 while(1)
 {
 Affiche = PortB;
 if(PortB > 9)
 Affiche = 0;

 PortD = Convert7Seg[Affiche];
 PortA = 0x01;
 }
}
