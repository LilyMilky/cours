/*********************************************************
Yann Lacquehay
Voltmetre
*********************************************************/
#define RS PORTB.F4
#define E PortB.F5
#define DataLCD PortB

char LSB,MSB;

// Fonction Interruption sur le CAN
void interrupt()
{
  if(INTCON.PEIE)
  {
     LSB = ADRESL;
     MSB = ADRESH;

     PIR1 = 0b01000000;
     ADCON0 = 0b10000100;             //Bit6 : Lancement conversion
     INTCON = 0b11000000;   //Remise du flag � 0 afin de pouvoir relancer l'interruption
  }
}

//Initialisation des ports

void Init()
{
   PORTB = 0;
   TrisB = 0;                             //PortB en sortie
   
   INTCON = 0b11000000;
   PIE1 = 0b01000000;
   
   TrisA = 0xff;
}

//Initialisation du CAN
void INITCAN ()
{
   ADCON1 = 0b10001110;
   
   ADCON0 = 0b10000000;             //Bit poids faible : Mise en service
   delay_us(30);
   ADCON0 = 0b10000100;             //Bit6 : Lancement conversion
}

//Validation de l'envoi de l'instruction envoy� au LCD
 void ValidCommand()
{
     E = 1;
     delay_us(600);
     E = 0;
}

//Envoi d'une instruction au LCD
void EnvoiInstructionLCD(unsigned short int Commande)
{
     unsigned short int Quartet = 0;
     RS = 0;

     Quartet = (Commande & 0b11110000);      //Masque sur le 1er Quartet
     Quartet = Quartet >> 4;
     DataLCD = Quartet;
     RS = 0;
     ValidCommand();

     Quartet = (Commande & 0b00001111);      //Masque sur le 2�me Quartet
     DataLCD = Quartet;
     RS = 0;
     ValidCommand();
}

//Initialisation du LCD
void InitLCD()
{
     RS = 0;

     delay_ms(15);
     DataLCD = 0x3;                          //Initialisation
     ValidCommand();
     delay_ms(5);
     ValidCommand();
     delay_ms(5);
     ValidCommand();
     delay_ms(5);

     DataLCD = 0x2;                           //Mode 4bits
     ValidCommand();

     EnvoiInstructionLCD(0x28);               //2 lignes, matrice de 8x5
     EnvoiInstructionLCD(0x0D);               //Active l'ecran + Curseur Nonvisible + Clignotement du caractere
     EnvoiInstructionLCD(0x01);               //Effacement �cran
     EnvoiInstructionLCD(0x06);               //Deplace le curseur � droite apres affichage d'un caractere
}

//Envoi d'un caractere au LCD
void EnvoiCaractere (unsigned char Lettre)
{
     unsigned char Quartet = 0  ;

     Quartet = (Lettre & 0b11110000);      //Masque sur le 1er Quartet
     Quartet = Quartet >> 4;
     DataLCD = Quartet;
     RS = 1;                               //Envoi donn�e
     ValidCommand();

     Quartet = (Lettre & 0b00001111);      //Masque sur le 2�me Quartet
     DataLCD = Quartet;
     RS = 1;
     ValidCommand();
}

//Envoi d'un message au LCD
void EnvoiMessage(unsigned short int Ligne, unsigned short int Colonne, unsigned char *Message)
{
     RS = 1;

     if (Ligne == 0)
                    EnvoiInstructionLCD(0x80 + Colonne);

     else if (Ligne == 1)
                    EnvoiInstructionLCD(0xC0 + Colonne);

     while(*Message != 0)
     {
                    EnvoiCaractere(*Message);
                    *Message ++;
     }
}

//Fonction main
void main()
{
     char Message1[6] = "MSB:";
     char Message2[6] = "LSB:";
     char Message3[7] = "Volt:";
     int Volt;
     
     Init();
     InitLCD();
     INITCAN();

     do
     {
     
       EnvoiMessage(0,0,Message1);
       EnvoiMessage(0,4,MSB+0x30);
       EnvoiMessage(0,8,Message2);
       EnvoiMessage(0,12,LSB+0x30);
       EnvoiMessage(1,3,Message3);
       EnvoiMessage(1,9,Volt);
     }while(1);
}