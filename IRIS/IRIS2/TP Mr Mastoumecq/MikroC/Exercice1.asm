;  ASM code generated by mikroVirtualMachine for PIC - V. 8.0.0.0
;  Date/Time: 01/10/2010 10:41:49
;  Info: http://www.mikroe.com


; ADDRESS	OPCODE	ASM
; ----------------------------------------------
$0000	$280C			GOTO	_main
$0004	$	_Init:
;Exercice1.c,10 :: 		void Init ()
;Exercice1.c,12 :: 		PORTD = 0;
$0004	$1303			BCF	STATUS, RP1
$0005	$1283			BCF	STATUS, RP0
$0006	$0188			CLRF	PORTD, 1
;Exercice1.c,13 :: 		TRISD = 0;
$0007	$1683			BSF	STATUS, RP0
$0008	$0188			CLRF	TRISD, 1
;Exercice1.c,14 :: 		TRISC = 1;
$0009	$3001			MOVLW	1
$000A	$0087			MOVWF	TRISC
;Exercice1.c,15 :: 		}
$000B	$0008			RETURN
$000C	$	_main:
;Exercice1.c,19 :: 		void main ()
;Exercice1.c,21 :: 		Init();
$000C	$2004			CALL	_Init
;Exercice1.c,22 :: 		while(1)
$000D	$	L_main_0:
;Exercice1.c,24 :: 		PortD = PortC;
$000D	$1283			BCF	STATUS, RP0
$000E	$0807			MOVF	PORTC, 0
$000F	$0088			MOVWF	PORTD
;Exercice1.c,25 :: 		}
$0010	$280D			GOTO	L_main_0
;Exercice1.c,27 :: 		}
$0011	$2811			GOTO	$
