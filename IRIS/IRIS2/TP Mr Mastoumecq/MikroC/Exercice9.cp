#line 1 "C:/BTS2/Lacquehay/MikroC/Exercice9.c"
#line 8 "C:/BTS2/Lacquehay/MikroC/Exercice9.c"
 unsigned short int Pression = 0;



void Init ()
{
 PORTD = 0;
 TRISD = 0;
 ADCON1= 0x07;
 TrisB = 0x01;
 PORTC = 0;
 TRISC = 0;
}

void Init_Interrupt()
{
 INTCON = 0x90;
 OPTION_REG = 0xFF;
}

void interrupt ()
{
 if (INTCON.INTF)
 {
 Pression++;
 if (Pression > 255)
 {
 Pression = 0;
 }
 PortC = Pression;
 }

 delay_ms(100);
 INTCON = 0x90;

}



void main ()
{
 long int i;
 Init();
 Init_Interrupt();

 while(1)
 {
 for (i = 0; i < 55000 ; i++)
 PortD = 0xFF;

 for (i = 0; i < 55000 ; i++)
 PortD = 0x00;
 }
}
