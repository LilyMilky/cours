#line 1 "C:/BTS2/Lacquehay Yann/MikroC/Exercice11.c"
#line 9 "C:/BTS2/Lacquehay Yann/MikroC/Exercice11.c"
void Init()
{
 PORTB = 0;
 TrisB = 0;
}

 void ValidCommand()
{
  PortB.F5  = 1;
 delay_us(600);
  PortB.F5  = 0;
}

void EnvoiInstructionLCD(unsigned short int Commande)
{
 unsigned short int Quartet = 0;
  PORTB.F4  = 0;

 Quartet = (Commande & 0b11110000);
 Quartet = Quartet >> 4;
  PortB  = Quartet;
  PORTB.F4  = 0;
 ValidCommand();

 Quartet = (Commande & 0b00001111);
  PortB  = Quartet;
  PORTB.F4  = 0;
 ValidCommand();
}

void InitLCD()
{
  PORTB.F4  = 0;

 delay_ms(15);
  PortB  = 0x3;
 ValidCommand();
 delay_ms(5);
 ValidCommand();
 delay_ms(5);
 ValidCommand();
 delay_ms(5);

  PortB  = 0x2;
 ValidCommand();

 EnvoiInstructionLCD(0x28);
 EnvoiInstructionLCD(0x0D);
 EnvoiInstructionLCD(0x01);
 EnvoiInstructionLCD(0x06);
}

void EnvoiCaractere (unsigned char Lettre)
{
 unsigned char Quartet = 0 ;


 Quartet = (Lettre & 0b11110000);
 Quartet = Quartet >> 4;
  PortB  = Quartet;
  PORTB.F4  = 1;
 ValidCommand();

 Quartet = (Lettre & 0b00001111);
  PortB  = Quartet;
  PORTB.F4  = 1;
 ValidCommand();


}

void EnvoiMessage(unsigned short int Ligne, unsigned short int Colonne, unsigned char *Message)
{
  PORTB.F4  = 1;

 if (Ligne == 1)
 EnvoiInstructionLCD(0x80 + Colonne);

 else if (Ligne == 2)
 EnvoiInstructionLCD(0xC0 + Colonne);

 else
 EnvoiCaractere('X') ;

 while(*Message != 0)
 {
 EnvoiCaractere(*Message);
 *Message ++;
 }

}

void main()
{
 Init();
 InitLCD();

 EnvoiInstructionLCD(0x82);
 EnvoiCaractere('H');
 EnvoiCaractere('P');
 EnvoiCaractere('=');
 EnvoiCaractere('0');

 EnvoiMessage(2,1,"Coucou");

}
