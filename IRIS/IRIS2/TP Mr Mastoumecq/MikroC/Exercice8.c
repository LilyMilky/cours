/*********************************************************
Yann Lacquehay
Clignotage Del
*********************************************************/

// Initialisation Port

void Init ()
{
 PORTD = 0;                                     //Mise �E0 avant envoi
 TRISD = 0;                                     //Sortie
 TRISA = 0xFF;                                  //PortA en entr�e
 PortC = 0;
 TRISC = 0;                                     //PortC en Sortie
 ADCON1= 0x07;
}

// Main

void main ()
{
 unsigned short int Pression = 0;
 Init();

        while(1)
        {
          long int i;
          for (i = 0; i < 55000 ; i++)
              PortD = 0xFF;
  
          for (i = 0; i < 55000 ; i++)
               PortD = 0x00;
          
          Pression = Pression + PortA;
          PortC = Pression;
          if (Pression > 255)
          {
              Pression = 0;
          }
        }
}

// Le probleme de cette methode est qu'il faut attendre la fin du clignotement
// pour que le Microprosseseur prenne en compte le bouton.