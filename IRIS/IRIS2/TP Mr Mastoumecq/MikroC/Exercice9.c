/*********************************************************
Yann Lacquehay
Clignotage Del
SW6 : PortD on
SW9 : Tout off
*********************************************************/

  unsigned short int Pression = 0;
  
// Initialisation Port

void Init ()
{
 PORTD = 0;                                     //Mise � 0 avant envoi
 TRISD = 0;                                     //Sortie
 ADCON1= 0x07;
 TrisB = 0x01;                                  //PortB en entr�e
 PORTC = 0;
 TRISC = 0;                                     //PortC en sortie
}

void Init_Interrupt()
{
 INTCON = 0x90;
 OPTION_REG = 0xFF;
}

void interrupt ()
{
  if (INTCON.INTF)                         //Test Interruption RB0
  {
     Pression++;
     if (Pression > 255)
     {
         Pression = 0;
     }
     PortC = Pression;
  }
  
  delay_ms(200);  //Tempo pour eviter le ph�nom�ne de rebond
  INTCON = 0x90;   //Remise du flag � 0 afin de pouvoir relancer l'interruption

}

// Main

void main ()
{
 long int i;
 Init();
 Init_Interrupt();

        while(1)
        {
          for (i = 0; i < 55000 ; i++)
              PortD = 0xFF;

          for (i = 0; i < 55000 ; i++)
              PortD = 0x00;
        }
}
