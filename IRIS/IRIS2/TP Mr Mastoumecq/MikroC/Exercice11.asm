;  ASM code generated by mikroVirtualMachine for PIC - V. 8.0.0.0
;  Date/Time: 26/11/2010 11:06:07
;  Info: http://www.mikroe.com


; ADDRESS	OPCODE	ASM
; ----------------------------------------------
$0000	$28CE			GOTO	_main
$0004	$	_ValidCommand:
;Exercice11.c,15 :: 		void ValidCommand()
;Exercice11.c,17 :: 		E = 1;
$0004	$1303			BCF	STATUS, RP1
$0005	$1283			BCF	STATUS, RP0
$0006	$1686			BSF	PORTB, 5
;Exercice11.c,18 :: 		delay_us(600);
$0007	$3002			MOVLW	2
$0008	$00FB			MOVWF	STACK_11
$0009	$30FF			MOVLW	255
$000A	$00FA			MOVWF	STACK_10
$000B	$0BFB			DECFSZ	STACK_11, F
$000C	$280E			GOTO	$+2
$000D	$2811			GOTO	$+4
$000E	$0BFA			DECFSZ	STACK_10, F
$000F	$280E			GOTO	$-1
$0010	$280B			GOTO	$-5
$0011	$308C			MOVLW	140
$0012	$00FA			MOVWF	STACK_10
$0013	$0BFA			DECFSZ	STACK_10, F
$0014	$2813			GOTO	$-1
$0015	$0000			NOP
$0016	$0000			NOP
;Exercice11.c,19 :: 		E = 0;
$0017	$1286			BCF	PORTB, 5
;Exercice11.c,20 :: 		}
$0018	$0008			RETURN
$0019	$	_EnvoiInstructionLCD:
;Exercice11.c,22 :: 		void EnvoiInstructionLCD(unsigned short int Commande)
;Exercice11.c,25 :: 		RS = 0;
$0019	$1303			BCF	STATUS, RP1
$001A	$1283			BCF	STATUS, RP0
$001B	$1206			BCF	PORTB, 4
;Exercice11.c,27 :: 		Quartet = (Commande & 0b11110000);      //Masque sur le 1er Quartet
$001C	$30F0			MOVLW	240
$001D	$052A			ANDWF	FARG_EnvoiInstructionLCD+0, 0
$001E	$00F2			MOVWF	STACK_2
;Exercice11.c,28 :: 		Quartet = Quartet >> 4;
$001F	$0872			MOVF	STACK_2, 0
$0020	$00F0			MOVWF	STACK_0
$0021	$0CF0			RRF	STACK_0, 1
$0022	$13F0			BCF	STACK_0, 7
$0023	$0CF0			RRF	STACK_0, 1
$0024	$13F0			BCF	STACK_0, 7
$0025	$0CF0			RRF	STACK_0, 1
$0026	$13F0			BCF	STACK_0, 7
$0027	$0CF0			RRF	STACK_0, 1
$0028	$13F0			BCF	STACK_0, 7
;Exercice11.c,29 :: 		DataLCD = Quartet;
$0029	$0870			MOVF	STACK_0, 0
$002A	$0086			MOVWF	PORTB
;Exercice11.c,30 :: 		RS = 0;
$002B	$1206			BCF	PORTB, 4
;Exercice11.c,31 :: 		ValidCommand();
$002C	$2004			CALL	_ValidCommand
;Exercice11.c,33 :: 		Quartet = (Commande & 0b00001111);      //Masque sur le 2�me Quartet
$002D	$300F			MOVLW	15
$002E	$052A			ANDWF	FARG_EnvoiInstructionLCD+0, 0
$002F	$0086			MOVWF	PORTB
;Exercice11.c,34 :: 		DataLCD = Quartet;
;Exercice11.c,35 :: 		RS = 0;
$0030	$1206			BCF	PORTB, 4
;Exercice11.c,36 :: 		ValidCommand();
$0031	$2004			CALL	_ValidCommand
;Exercice11.c,37 :: 		}
$0032	$0008			RETURN
$0033	$	_EnvoiCaractere:
;Exercice11.c,61 :: 		void EnvoiCaractere (unsigned char Lettre)
;Exercice11.c,66 :: 		Quartet = (Lettre & 0b11110000);      //Masque sur le 1er Quartet
$0033	$30F0			MOVLW	240
$0034	$1303			BCF	STATUS, RP1
$0035	$1283			BCF	STATUS, RP0
$0036	$052A			ANDWF	FARG_EnvoiCaractere+0, 0
$0037	$00F2			MOVWF	STACK_2
;Exercice11.c,67 :: 		Quartet = Quartet >> 4;
$0038	$0872			MOVF	STACK_2, 0
$0039	$00F0			MOVWF	STACK_0
$003A	$0CF0			RRF	STACK_0, 1
$003B	$13F0			BCF	STACK_0, 7
$003C	$0CF0			RRF	STACK_0, 1
$003D	$13F0			BCF	STACK_0, 7
$003E	$0CF0			RRF	STACK_0, 1
$003F	$13F0			BCF	STACK_0, 7
$0040	$0CF0			RRF	STACK_0, 1
$0041	$13F0			BCF	STACK_0, 7
;Exercice11.c,68 :: 		DataLCD = Quartet;
$0042	$0870			MOVF	STACK_0, 0
$0043	$0086			MOVWF	PORTB
;Exercice11.c,69 :: 		RS = 1;                               //Envoi donn�e
$0044	$1606			BSF	PORTB, 4
;Exercice11.c,70 :: 		ValidCommand();
$0045	$2004			CALL	_ValidCommand
;Exercice11.c,72 :: 		Quartet = (Lettre & 0b00001111);      //Masque sur le 2�me Quartet
$0046	$300F			MOVLW	15
$0047	$052A			ANDWF	FARG_EnvoiCaractere+0, 0
$0048	$0086			MOVWF	PORTB
;Exercice11.c,73 :: 		DataLCD = Quartet;
;Exercice11.c,74 :: 		RS = 1;
$0049	$1606			BSF	PORTB, 4
;Exercice11.c,75 :: 		ValidCommand();
$004A	$2004			CALL	_ValidCommand
;Exercice11.c,78 :: 		}
$004B	$0008			RETURN
$004C	$	_Init:
;Exercice11.c,9 :: 		void Init()
;Exercice11.c,11 :: 		PORTB = 0;
$004C	$1303			BCF	STATUS, RP1
$004D	$1283			BCF	STATUS, RP0
$004E	$0186			CLRF	PORTB, 1
;Exercice11.c,12 :: 		TrisB = 0;                             //PortB en sortie
$004F	$1683			BSF	STATUS, RP0
$0050	$0186			CLRF	TRISB, 1
;Exercice11.c,13 :: 		}
$0051	$0008			RETURN
$0052	$	_InitLCD:
;Exercice11.c,39 :: 		void InitLCD()
;Exercice11.c,41 :: 		RS = 0;
$0052	$1303			BCF	STATUS, RP1
$0053	$1283			BCF	STATUS, RP0
$0054	$1206			BCF	PORTB, 4
;Exercice11.c,43 :: 		delay_ms(15);
$0055	$3027			MOVLW	39
$0056	$00FB			MOVWF	STACK_11
$0057	$30FF			MOVLW	255
$0058	$00FA			MOVWF	STACK_10
$0059	$0BFB			DECFSZ	STACK_11, F
$005A	$285C			GOTO	$+2
$005B	$285F			GOTO	$+4
$005C	$0BFA			DECFSZ	STACK_10, F
$005D	$285C			GOTO	$-1
$005E	$2859			GOTO	$-5
$005F	$30DB			MOVLW	219
$0060	$00FA			MOVWF	STACK_10
$0061	$0BFA			DECFSZ	STACK_10, F
$0062	$2861			GOTO	$-1
$0063	$0000			NOP
;Exercice11.c,44 :: 		DataLCD = 0x3;                          //Initialisation
$0064	$3003			MOVLW	3
$0065	$0086			MOVWF	PORTB
;Exercice11.c,45 :: 		ValidCommand();
$0066	$2004			CALL	_ValidCommand
;Exercice11.c,46 :: 		delay_ms(5);
$0067	$300D			MOVLW	13
$0068	$00FB			MOVWF	STACK_11
$0069	$30FF			MOVLW	255
$006A	$00FA			MOVWF	STACK_10
$006B	$0BFB			DECFSZ	STACK_11, F
$006C	$286E			GOTO	$+2
$006D	$2871			GOTO	$+4
$006E	$0BFA			DECFSZ	STACK_10, F
$006F	$286E			GOTO	$-1
$0070	$286B			GOTO	$-5
$0071	$30F3			MOVLW	243
$0072	$00FA			MOVWF	STACK_10
$0073	$0BFA			DECFSZ	STACK_10, F
$0074	$2873			GOTO	$-1
$0075	$0000			NOP
;Exercice11.c,47 :: 		ValidCommand();
$0076	$2004			CALL	_ValidCommand
;Exercice11.c,48 :: 		delay_ms(5);
$0077	$300D			MOVLW	13
$0078	$00FB			MOVWF	STACK_11
$0079	$30FF			MOVLW	255
$007A	$00FA			MOVWF	STACK_10
$007B	$0BFB			DECFSZ	STACK_11, F
$007C	$287E			GOTO	$+2
$007D	$2881			GOTO	$+4
$007E	$0BFA			DECFSZ	STACK_10, F
$007F	$287E			GOTO	$-1
$0080	$287B			GOTO	$-5
$0081	$30F3			MOVLW	243
$0082	$00FA			MOVWF	STACK_10
$0083	$0BFA			DECFSZ	STACK_10, F
$0084	$2883			GOTO	$-1
$0085	$0000			NOP
;Exercice11.c,49 :: 		ValidCommand();
$0086	$2004			CALL	_ValidCommand
;Exercice11.c,50 :: 		delay_ms(5);
$0087	$300D			MOVLW	13
$0088	$00FB			MOVWF	STACK_11
$0089	$30FF			MOVLW	255
$008A	$00FA			MOVWF	STACK_10
$008B	$0BFB			DECFSZ	STACK_11, F
$008C	$288E			GOTO	$+2
$008D	$2891			GOTO	$+4
$008E	$0BFA			DECFSZ	STACK_10, F
$008F	$288E			GOTO	$-1
$0090	$288B			GOTO	$-5
$0091	$30F3			MOVLW	243
$0092	$00FA			MOVWF	STACK_10
$0093	$0BFA			DECFSZ	STACK_10, F
$0094	$2893			GOTO	$-1
$0095	$0000			NOP
;Exercice11.c,52 :: 		DataLCD = 0x2;                           //Mode 4bits
$0096	$3002			MOVLW	2
$0097	$0086			MOVWF	PORTB
;Exercice11.c,53 :: 		ValidCommand();
$0098	$2004			CALL	_ValidCommand
;Exercice11.c,55 :: 		EnvoiInstructionLCD(0x28);               //2 lignes, matrice de 8x5
$0099	$3028			MOVLW	40
$009A	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$009B	$2019			CALL	_EnvoiInstructionLCD
;Exercice11.c,56 :: 		EnvoiInstructionLCD(0x0D);               //Active l'ecran + Curseur Nonvisible + Clignotement du caractere
$009C	$300D			MOVLW	13
$009D	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$009E	$2019			CALL	_EnvoiInstructionLCD
;Exercice11.c,57 :: 		EnvoiInstructionLCD(0x01);               //Effacement �cran
$009F	$3001			MOVLW	1
$00A0	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$00A1	$2019			CALL	_EnvoiInstructionLCD
;Exercice11.c,58 :: 		EnvoiInstructionLCD(0x06);               //Deplace le curseur � droite apres affichage d'un caractere
$00A2	$3006			MOVLW	6
$00A3	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$00A4	$2019			CALL	_EnvoiInstructionLCD
;Exercice11.c,59 :: 		}
$00A5	$0008			RETURN
$00A6	$	_EnvoiMessage:
;Exercice11.c,80 :: 		void EnvoiMessage(unsigned short int Ligne, unsigned short int Colonne, unsigned char *Message)
;Exercice11.c,82 :: 		RS = 1;
$00A6	$1303			BCF	STATUS, RP1
$00A7	$1283			BCF	STATUS, RP0
$00A8	$1606			BSF	PORTB, 4
;Exercice11.c,84 :: 		if (Ligne == 1)
$00A9	$0827			MOVF	FARG_EnvoiMessage+0, 0
$00AA	$3A01			XORLW	1
$00AB	$1D03			BTFSS	STATUS, Z
$00AC	$28B2			GOTO	L_EnvoiMessage_0
;Exercice11.c,85 :: 		EnvoiInstructionLCD(0x80 + Colonne);
$00AD	$0828			MOVF	FARG_EnvoiMessage+1, 0
$00AE	$3F80			ADDLW	128
$00AF	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$00B0	$2019			CALL	_EnvoiInstructionLCD
$00B1	$28BE			GOTO	L_EnvoiMessage_1
$00B2	$	L_EnvoiMessage_0:
;Exercice11.c,87 :: 		else if (Ligne == 2)
$00B2	$0827			MOVF	FARG_EnvoiMessage+0, 0
$00B3	$3A02			XORLW	2
$00B4	$1D03			BTFSS	STATUS, Z
$00B5	$28BB			GOTO	L_EnvoiMessage_2
;Exercice11.c,88 :: 		EnvoiInstructionLCD(0xC0 + Colonne);
$00B6	$0828			MOVF	FARG_EnvoiMessage+1, 0
$00B7	$3FC0			ADDLW	192
$00B8	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$00B9	$2019			CALL	_EnvoiInstructionLCD
$00BA	$28BE			GOTO	L_EnvoiMessage_3
$00BB	$	L_EnvoiMessage_2:
;Exercice11.c,91 :: 		EnvoiCaractere('X') ;
$00BB	$3058			MOVLW	88
$00BC	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00BD	$2033			CALL	_EnvoiCaractere
$00BE	$	L_EnvoiMessage_3:
$00BE	$	L_EnvoiMessage_1:
;Exercice11.c,93 :: 		while(*Message != 0)
$00BE	$	L_EnvoiMessage_4:
$00BE	$0829			MOVF	FARG_EnvoiMessage+2, 0
$00BF	$0084			MOVWF	FSR
$00C0	$0800			MOVF	INDF, 0
$00C1	$00F1			MOVWF	STACK_1
$00C2	$0871			MOVF	STACK_1, 0
$00C3	$3A00			XORLW	0
$00C4	$1903			BTFSC	STATUS, Z
$00C5	$28CD			GOTO	L_EnvoiMessage_5
;Exercice11.c,95 :: 		EnvoiCaractere(*Message);
$00C6	$0829			MOVF	FARG_EnvoiMessage+2, 0
$00C7	$0084			MOVWF	FSR
$00C8	$0800			MOVF	INDF, 0
$00C9	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00CA	$2033			CALL	_EnvoiCaractere
;Exercice11.c,96 :: 		*Message ++;
$00CB	$0AA9			INCF	FARG_EnvoiMessage+2, 1
;Exercice11.c,97 :: 		}
$00CC	$28BE			GOTO	L_EnvoiMessage_4
$00CD	$	L_EnvoiMessage_5:
;Exercice11.c,99 :: 		}
$00CD	$0008			RETURN
$00CE	$	_main:
$00CE	$3043			MOVLW	67
$00CF	$1303			BCF	STATUS, RP1
$00D0	$1283			BCF	STATUS, RP0
$00D1	$00A0			MOVWF	lstr1_Exercice11+0
$00D2	$306F			MOVLW	111
$00D3	$00A1			MOVWF	lstr1_Exercice11+1
$00D4	$3075			MOVLW	117
$00D5	$00A2			MOVWF	lstr1_Exercice11+2
$00D6	$3063			MOVLW	99
$00D7	$00A3			MOVWF	lstr1_Exercice11+3
$00D8	$306F			MOVLW	111
$00D9	$00A4			MOVWF	lstr1_Exercice11+4
$00DA	$3075			MOVLW	117
$00DB	$00A5			MOVWF	lstr1_Exercice11+5
$00DC	$01A6			CLRF	lstr1_Exercice11+6
;Exercice11.c,101 :: 		void main()
;Exercice11.c,103 :: 		Init();
$00DD	$204C			CALL	_Init
;Exercice11.c,104 :: 		InitLCD();
$00DE	$2052			CALL	_InitLCD
;Exercice11.c,106 :: 		EnvoiInstructionLCD(0x82);                //1ere ligne 3eme case
$00DF	$3082			MOVLW	130
$00E0	$00AA			MOVWF	FARG_EnvoiInstructionLCD+0
$00E1	$2019			CALL	_EnvoiInstructionLCD
;Exercice11.c,107 :: 		EnvoiCaractere('H');
$00E2	$3048			MOVLW	72
$00E3	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00E4	$2033			CALL	_EnvoiCaractere
;Exercice11.c,108 :: 		EnvoiCaractere('P');
$00E5	$3050			MOVLW	80
$00E6	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00E7	$2033			CALL	_EnvoiCaractere
;Exercice11.c,109 :: 		EnvoiCaractere('=');
$00E8	$303D			MOVLW	61
$00E9	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00EA	$2033			CALL	_EnvoiCaractere
;Exercice11.c,110 :: 		EnvoiCaractere('0');
$00EB	$3030			MOVLW	48
$00EC	$00AA			MOVWF	FARG_EnvoiCaractere+0
$00ED	$2033			CALL	_EnvoiCaractere
;Exercice11.c,112 :: 		EnvoiMessage(2,1,"Coucou");
$00EE	$3002			MOVLW	2
$00EF	$00A7			MOVWF	FARG_EnvoiMessage+0
$00F0	$3001			MOVLW	1
$00F1	$00A8			MOVWF	FARG_EnvoiMessage+1
$00F2	$3020			MOVLW	lstr1_Exercice11
$00F3	$00A9			MOVWF	FARG_EnvoiMessage+2
$00F4	$20A6			CALL	_EnvoiMessage
;Exercice11.c,114 :: 		}
$00F5	$28F5			GOTO	$
