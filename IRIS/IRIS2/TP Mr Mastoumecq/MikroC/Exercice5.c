/*********************************************************
Yann Lacquehay
Afficheur 7 segments
*********************************************************/

// Initialisation Port

void Init ()
{
 PortD = 0;
 TRISD = 0;
}

// Main

void main ()
{
  Init();
  while(1)
  {
    switch (PortB)
    {
        case 0 : PortD = 0x3F;
                 break;
        case 1 : PortD = 0x06;
                 break;
        case 2 : PortD = 0x5B;
                 break;
        case 3 : PortD = 0x4F;
                 break;
        case 4 : PortD = 0x66;
                 break;
        case 5 : PortD = 0x6D;
                 break;
        case 6 : PortD = 0x7D;
                 break;
        case 7 : PortD = 0x07;
                 break;
        case 8 : PortD = 0x7F;
                 break;
        case 9 : PortD = 0x6F;
                 break;
        default : PortD = 0x00;
    }
  }
}