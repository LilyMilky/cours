/* servweb.c :
 *
 * un serveur web très simple:
 *
 * 1. Mode Autonome:
 *  $ ./servweb autonome
 *
 *  Dans ce mode le programme fonctionne comme un serveur web autonome
 *  Cependant, le privilège root est nécessaire 
 *
 *  Dans ce mode le programme contacte
 *  "sockserv server" pour demander une socket associée au port 80
 *  Si sockserv autorise la requete, il retourne une socket asoociée au port 80.
 *  Ceci permet au programme de fonctionner sans le privilège root
 */
//#include "common.h"

#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
using namespace std;

static void bail(const char *on_what) 
{
    if ( errno != 0 ) 
    {
        fputs(strerror(errno),stderr);
        fputs(": ",stderr);
    }
    fputs(on_what,stderr);
    fputc('\n',stderr);
    exit(1);
}

int main(int argc,char **argv)
{
    int z;
    int s;             			/* socket Web Server		*/
    int c;                 		/* socket client		*/
    socklen_t alen;             		/* long. adresse		*/
    struct sockaddr_in a_web; 		/* Serveur Web	*/
    struct sockaddr_in a_cln;		/* adresse Client	*/
    int b = 1;      			/* pour SO_REUSEADDR		*/
    FILE *rx;                		/* flux de lecture	*/
    FILE *tx;               		/* flux d'écriture	*/
    time_t td;       			/* date/temps courant		*/


	/*   ETAPE 1 */
	s = socket(PF_INET,SOCK_STREAM,0);
	
	if ( s == -1 )
		bail("socket(2)");
	
	/*serveur sur le port 80:*/
	memset(&a_web,0,sizeof a_web);
	a_web.sin_family 		= AF_INET;
	a_web.sin_port 			= ntohs(128);
	a_web.sin_addr.s_addr 		= ntohl(INADDR_ANY);
	
	/*   ETAPE 2 */
	/*
		* Bind : de l'adresse du serveur web , 
			* il faut être root pour que cela fonctione sur le port 80
	*/
	z = bind(s,(struct sockaddr *)&a_web,sizeof( a_web));
	
	// test
	if ( z == -1 )  bail("impossible d ecouter sur le port 128");
	
	/*
		* positionner le flag SO_REUSEADDR :
	*/
		z = setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&b,sizeof b);
		
	if ( z == -1 )	bail("setsockopt(2)");

	/*   ETAPE 3 */
	/*
	* maintenant on met la socket en écoute:
	*/
		z = listen(s,10);
	
	if ( z == -1 )
		bail("listen(2)");
	
	/*
	* serveur web simple: 
		* on accepte une seule ligne du client, 
		* main on ne l'interprète pas.
		* on renvoit une page html simple.
	*/
	for (;;) 
	{
		/*on attend une connection venant du navigateur:*/
		alen = sizeof a_cln;

		/*   ETAPE 4 */
		c = accept(s, (struct sockaddr *)&a_cln, &alen);
		if ( c == -1 ) 
		{
			perror("accept(2)");
			continue;			
		}
		cout << "Adresse du client: " << inet_ntoa(a_cln.sin_addr) <<endl;	//Affichage AdresseIP dans le shell
	
		{
			/*   ETAPE 5 */

			char buf[8192] ;
			struct sockaddr_in adr ;
			socklen_t x;
			int y ;
			char env[500];
			/*envoyer une page HTML :*/
			strcpy(env, "<HTML>\n<HEAD>\n""page de test ""</HEAD><BODY>""cela semble marcher""</BODY></HTML>");
			x = sizeof(buf) ;
			recvfrom(c,						// Socket
				buf,						// buffer de reception
				sizeof (buf),					// taille max du buf de reception
				0,						// Flags: pas d'options
				(struct sockaddr *)&adr,			// adresse
				&x);
			y = sizeof(buf);
			printf("recu %d octets : <<%s>>\n",x,buf);
			send(c,env,strlen(env),0);
			
			strcpy(env,"<HTML>\n<HEAD>\n""</HEAD><BODY>""Adr Client: ""</BODY></HTML>");	
			send(c,env,strlen(env),0);
			strcpy(env,inet_ntoa(a_cln.sin_addr));	//Affichage AdresseIP dans la page WEB
			send(c,env,strlen(env),0);
			close(c);
				
		}
	
	}
	
	return 0;
}

