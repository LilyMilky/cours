#include <arpa/inet.h>
#include <ctime> 

#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#define MAX_MSG 100
using namespace std;
int main(int argc, char *argv[])
{
	int listenSocket, i;
	unsigned short int listenPort;
	socklen_t clientAddressLength;	
	struct sockaddr_in clientAddress, serverAddress;
	char line[(MAX_MSG+1)];
	int OctetLu = 0;

	cout << "Entrer le numéro de port d'ecoute entre 1500 et 65000: ";
	cin >> listenPort;

	// Création de la socket d'ecoute des demandes de connexions des clients
	listenSocket = socket(AF_INET, SOCK_DGRAM, 0);

	// au cas où quelque chose pose problème
	if (listenSocket < 0) 
	{
		cerr << "ne peut creer la socket";
		exit(1);
	}

	// Bind : associe la socket d'ecoute au port d'ecoute.
	// Positionner différents champs dans la structure serverAddress
	// sur x86 l'octet de poinds faible en premier
	// sur le réseau l'octet de poinds fort en premier
	// htonl()...........................................
	// htons() ...........................................
	serverAddress.sin_family = PF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(listenPort);

	// Ensuie on appelle bind().
	if (bind(listenSocket,(struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) 
	{
		cerr << "cannot bind socket";
		exit(1);
	}

	// Attente des demandes de connexions des clients
	// Attention : ce n'est pas un appel bloquant, cela indique au niveau du
	// système que notre programme attend des connexions sur cette socket, ce fil
	// d exécution continue
	listen(listenSocket, 5);
	cout << "Attente des connexions sur le port" << listenPort << "\n";

	//...........................................................
	while (1) 
	{
		clientAddressLength = sizeof(clientAddress);

		// mettre les octets du buffer à zéro, de cette manière on peut détecter
		// la fin de la chaine de caractères.
		memset(line, 0x0, (MAX_MSG+1));

		//.............................................................
		if (recvfrom(listenSocket, line, MAX_MSG, 0,(struct sockaddr *)
			&clientAddress,&clientAddressLength) < 0)
			{
				cerr << " Problème I/O";
				exit(1);
			}
	
		// .................................
		// inet_ntoa .......................
		
		cout << "\n De la part de : " << inet_ntoa(clientAddress.sin_addr);
				

		// Afficher le numero de port
		cout << " : " << ntohs(clientAddress.sin_port) << "\n";

		//Afficher Heure debut traitement
		
		time_t temps_act; 
		time(&temps_act); 
	
		cout << "\nDebut du traitement à :" << ctime(&temps_act) << endl;
		
		sleep(2);
		
		time(&temps_act); 
		cout << "Fin de traitement à :" << ctime(&temps_act) <<endl <<endl;

		// Afficher la phrase
		cout << " Reçu: " << line << "\n\n";
	}
}
