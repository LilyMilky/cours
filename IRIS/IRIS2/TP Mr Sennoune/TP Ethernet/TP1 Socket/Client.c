								//Code du programme client.c :
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#define MAX_LINE 100
#define LINE_ARRAY_SIZE (MAX_LINE+3)
#define NEWLINE "\n"
using namespace std;

int main()
{
	int socketDescriptor, i;
	int numRead;
	unsigned short int serverPort;
	struct sockaddr_in serverAddress;
	struct hostent *hostInfo;
	struct timeval timeVal;
	fd_set readSet;
	char buf[LINE_ARRAY_SIZE], c;
	int Retour;

	cout << "Entrer le nom du serveur ou son adresse IPV4: ";
	memset(buf, 0x0, LINE_ARRAY_SIZE); // ......................
	cin.get(buf, MAX_LINE, '\n'); // ......................

	// gethostbyname() : prend en argument un nom ou une adresse IP
	// retourne un pointeur sur une structure
	// Vous pouvez jeter un coup d'oeil sur la composition de la structure
	//...................................................................
	hostInfo = gethostbyname(buf);

	if (hostInfo == NULL)
	{
		cout << "probleme d'interpretation du host: " << buf << "\n";
		exit(1);
	}

	cout << "Entrer le numero de port du serveur: ";
	cin >> serverPort;

	// ...........................................
	cin.get(c);

	// Création d'une socket
	socketDescriptor = socket(PF_INET, SOCK_DGRAM, 0);

	// test du descripteur de socket obtenu
	if (socketDescriptor < 0) 
	{
		cerr << "cannot create socket\n";
		exit(1);
	}

	// Positonner quelques champs dans la structure serverAddress
	// ...........................................................
	serverAddress.sin_family = hostInfo->h_addrtype;

	//............................................................
	memcpy((char *) &serverAddress.sin_addr.s_addr,
	hostInfo->h_addr_list[0], hostInfo->h_length);

	//.................................................
	serverAddress.sin_port = htons(serverPort);

	//cout << "\nEntrer une phrase,ensuite return, elle sera affichée sur le serveur\n";
	
	cout << "Phrase: ";

	//......................................................
	memset(buf, 0x0, LINE_ARRAY_SIZE);

	//cin.getline(buf,100);	
	time_t temps_act; 
	
	
	for (i = 0 ; i < 8; i++)	
	{
		buf[0] = i;
		buf[1] = ')';
		buf[2] = ' ';
		time(&temps_act);
		strcpy(&buf[3],ctime(&temps_act));
		if (sendto(socketDescriptor, buf, strlen(buf), 0,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) 
		{
			cerr <<"Impossible d'envoyer les donnees";
			close(socketDescriptor);
			exit(1);
		}
		sleep(1);
	}

	// Envoi de la ligne au serveur
	if (sendto(socketDescriptor, buf, strlen(buf), 0,(struct sockaddr *) &serverAddress,sizeof(serverAddress)) < 0) 
	{
		cerr <<"Impossible d'envoyer les donnees";
		close(socketDescriptor);
		exit(1);
	}

	close(socketDescriptor);
	return 0;
}

