#pragma once

class CPersonne
{
private:
	double taille,poids;				//taille en m, pois en kg
public:
	CPersonne(void);					//construsteur sans argument
	CPersonne(double itaille, double ipoids);	//construsteur avec 2 arguments
	virtual ~CPersonne(void);					//destructeur
	void setPoids (double ipoids);		//modifie l'attribut poids
	void setTaille (double itaille);	//modifie l'attribut taille
	double indiceMasseCorporelle();
	double getTaille();					//renvoie l'attribut taille 
	double getPoids();					//renvoie l'attribut poids
};
