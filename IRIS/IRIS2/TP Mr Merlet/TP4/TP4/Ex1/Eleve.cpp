#include "Eleve.h"
#include <iostream>
using namespace std;

CEleve::CEleve(void)
{
	this->nbHeuresCours=25;
}

CEleve::~CEleve(void)
{
	cout<<"Appel destructeur de CEleve"<<endl;
}

void CEleve::identifie()
{
	cout << "Je suis un eleve" <<endl;
}