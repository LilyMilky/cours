/*******************************************************************************************
Yann Lacquehay
27/09/10
*******************************************************************************************/

#include "Personne.h"
#include "Professeur.h"
#include "Eleve.h"
#include <iostream>
using namespace std;

void main()
{
	CPersonne *Ptr;

	//Impossible d'instancier une classe abstraite. Il faut redefinir la m�thode identifie.

	Ptr = new CProfesseur;
	Ptr->identifie();
	Ptr = new CEleve;
	Ptr->identifie();
}