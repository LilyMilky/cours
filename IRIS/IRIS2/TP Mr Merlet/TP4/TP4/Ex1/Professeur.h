#pragma once
#include "Personne.h"

class CProfesseur :
	public CPersonne
{
private:
	int nbclasses;
public:
	CProfesseur(int inbclasses, double it, double ip);
	CProfesseur(void);
	~CProfesseur(void);
	int getNbclasses();
	void identifie();
};
