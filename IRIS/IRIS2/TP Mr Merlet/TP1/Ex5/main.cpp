/******************************************************************************************
Yann Lacquehay
Exercice 2 
06/09/10
******************************************************************************************/

#include "Professeur.h"
#include <iostream>
using namespace std;

void main()
{
	CProfesseur Prof(2,18,1.75,60); //Aucun constructeur par defaut car le constructeur re�oit un argument

	cout <<"Le professeur a "<< Prof.returnage() << "ans, mesure "<< Prof.returntaille() <<"m et pese "<< Prof.returnpoids() << "Kg\n";
}