#pragma once
#include "Personne.h"

class CProfesseur :
	public CPersonne
{
private:
	int nbclasses;
	int age;
	double taille,poids;	//taille en m, poids en kg
public:
	CProfesseur(int inbclasses, int ia, double it, double ip);
	~CProfesseur(void);
	int getNbclasses();
	int returnage();
	double returntaille();
	double returnpoids();
	double ageParNBclasses(int age, int nbclasses);

};
