#pragma once

class CPersonne
{
private:
	int age;
	double taille,poids;	//taille en m, poids en kg
public:
	CPersonne(int iage, double itaille, double ipoids);
	~CPersonne(void);
	void Modification(int iage, double itaille, double ipoids);//On renomme initialise car le constructeur initialise deja, c'est une modification
	double indiceMasseCorporelle();
	int returnage();
	double returntaille();
	double returnpoids();
	CPersonne();
};
