/******************************************************************************************
Yann Lacquehay
Exercice 2 
06/09/10
******************************************************************************************/

#include "Personne.h" //on inclut la declaration de la classe CPersonne
#include <iostream>
using namespace std;

int main()
{
CPersonne Louis;					//Declaration de l'objet Louis
CPersonne *ptrPers;					//Declaration d'un pointeur sur un objet de type CPersonne
ptrPers = &Louis;					//ptrPers pointe sur l'objet Louis
ptrPers -> poids = 63;				//Initialisation du poids Louis
ptrPers -> taille = 1.75;			//Initialisation de la taille de Louis

cout << "Louis fait " << Louis.poids << "kg et " << Louis.taille << "m\n";
return 0;
}