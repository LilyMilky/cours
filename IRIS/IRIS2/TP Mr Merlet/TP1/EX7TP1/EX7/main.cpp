/******************************************************************************************
Yann Lacquehay
13/09/10
EX7
******************************************************************************************/

#include "CDate.h"
#include <iostream>
using namespace std;

void main()
{
	CDate Date;
	char Tab[11];
	bool Val;

	cout << "Veuillez saisir une date comprise entre 01/01/1990 et 31/12/2010: ";
	do
	{
		cin  >> Tab;
		Val = Date.setDate(Tab);

		if (Val != true)
		{
			cout << "La date que vous avez entree n'est pas comprise entre 01/01/1990 et 31/12/2010, Veuillez resaisir: ";
		}
	}while (! (Val == true));	
	cout << "La date saisie est bien entre 01/01/1990 et 31/12/2010 \n";

	cout << "Vous avez saisie : " << Date.getTchar_date() <<endl;
	cout << "Jour: "<< int(Date.getJour()) << " Mois: " << int(Date.getMois()) << " Annee: " << Date.getAnnee() << endl;
}