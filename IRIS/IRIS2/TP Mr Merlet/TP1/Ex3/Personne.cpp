#include "Personne.h"

//Modifie les variables membres de la classe
void CPersonne::Modification(int iage, double itaille, double ipoids)
{
	age=iage;
	taille=itaille;
	poids=ipoids;
}
//Constructeur
CPersonne::CPersonne(int iage, double itaille, double ipoids)
{
	Modification(iage, itaille, ipoids);
}
//Destructueur
CPersonne::~CPersonne()
{}
//retourne l'indice de masse corporelle de la personne
double CPersonne::indiceMasseCorporelle()
{
	return poids/(taille*taille);
}

//Retourne l'age
int CPersonne::returnage()
{
	return age;
}
//Retourne la taille
double CPersonne::returntaille()
{
	return taille;
}
//Retourne le poids
double CPersonne::returnpoids()
{
	return poids;
}