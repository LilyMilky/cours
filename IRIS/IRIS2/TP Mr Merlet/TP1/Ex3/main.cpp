/******************************************************************************************
Yann Lacquehay
Exercice 1 
06/09/10
******************************************************************************************/

#include "Personne.h"
#include <iostream>
using namespace std;

void main()
{
	int age;
	double taille, poids;

	CPersonne Louis(18,1.75,65); 
	age = Louis.returnage();
	taille = Louis.returntaille();
	poids = Louis.returnpoids();
	cout << "Louis a " << age << "ans, fait " << poids << "Kg et il mesure " << taille << "m\n";
}