#include "Personne.h"

//initialise les variables membres de la classe
void CPersonne::initialise(int iage, double itaille, double ipoids)
{
	age=iage;
	taille=itaille;
	poids=ipoids;
}

//retourne l'indice de masse corporelle de la personne
double CPersonne::indiceMasseCorporelle()
{
	return poids/(taille*taille);
}

