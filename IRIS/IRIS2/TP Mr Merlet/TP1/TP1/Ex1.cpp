/******************************************************************************************
Yann Lacquehay
Exercice 1 
06/09/10
******************************************************************************************/

#include "Personne.h"
#include <iostream>
using namespace std;

void main()
{
	CPersonne Marc;							//On declare l'objet Marc
	double Masse;

	cout << "Marc a " << Marc.age <<endl;	// Erreur car non initialisť
	Marc.initialise(60,1.95,9);				//On initialise l'objet Marc
	Marc.age = 60;
	Marc.taille = 1.95;
	Marc.poids = 9;

	Masse = Marc.indiceMasseCorporelle();
	cout << "Marc fait " << Masse <<endl;

}