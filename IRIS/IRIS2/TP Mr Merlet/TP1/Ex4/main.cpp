/******************************************************************************************
Yann Lacquehay
Exercice 2 
06/09/10
******************************************************************************************/

#include "Professeur.h"
#include <iostream>
using namespace std;

void main()
{
	CProfesseur Prof(2); //Aucun constructeur par defaut car le constructeur re�oit un argument

	//Ordre d'execution: Constructeur CPersonne -> Constructeur CProfesseur -> Destructeur CProfesseur -> Destructeur CPersonne
}