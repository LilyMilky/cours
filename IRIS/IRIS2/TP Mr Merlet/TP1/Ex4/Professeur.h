#pragma once
#include "Personne.h"

class CProfesseur :
	public CPersonne
{
private:
	int nbclasses;
public:
	CProfesseur(int inbclasses);
	~CProfesseur(void);
	int getNbclasses();
};
