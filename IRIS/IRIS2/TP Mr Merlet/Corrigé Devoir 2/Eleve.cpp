#include "Eleve.h"

CEleve::CEleve(string inomeleve, unsigned short int iage, unsigned short int inote)
										//:CPersonne(inomeleve,iage)
{
	this->setAge(iage);
	this->setNom(inomeleve);
	this->noteMoyenneAnnee=inote;
}

CEleve::~CEleve(void)
{
}


unsigned short int CEleve::getNoteMoyenneAnnee()
{
	return this->noteMoyenneAnnee;
}

void CEleve::setNoteMoyenneAnnee(unsigned short int inote)
{
	this->noteMoyenneAnnee=inote;
}

string CEleve::identifie()
{
	return "Eleve";
}