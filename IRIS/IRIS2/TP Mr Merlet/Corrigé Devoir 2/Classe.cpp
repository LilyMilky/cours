#include "Classe.h"

CClasse::CClasse(string inom)
{
	nom=inom;
	vectEffectif.clear();
}

CClasse::~CClasse(void)
{
}

string CClasse::getNom()
{
	return nom;
}

void CClasse::ajoutLien(CPersonne * ptrPersonne)
{
	vectEffectif.push_back(ptrPersonne);
}

	
bool CClasse::supprimerLien(CPersonne * ptrPersonne)
{
	bool res=false;
	vector<CPersonne *>::iterator it;
	for (it=vectEffectif.begin();it!=vectEffectif.end();it++)
	{
		if (*it==ptrPersonne)
		{
			vectEffectif.erase(it);
			res=true;
			break;
		}
	}
	return res;
}

vector<CPersonne *> CClasse::getVectEffectif()
{
	return this->vectEffectif;
}
