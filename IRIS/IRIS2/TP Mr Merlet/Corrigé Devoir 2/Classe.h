#pragma once
#include <vector>
#include "Personne.h"

class CClasse
{
private:
	string nom;
	vector<CPersonne *> vectEffectif;
public:
	CClasse(string inom);
	~CClasse(void);
	string getNom();

	//ajoute un lien avec un objet de la classe CPersonne
	void ajoutLien(CPersonne * ptrPersonne);	

	//	si un lien avec l'objet CPersonne dont l'adresse est pass�e 
	//	en param�tre existe, il est supprim� et supprimerLien() retourne "true"
	//	sinon supprimerLien() retourne "false"
	bool supprimerLien(CPersonne * ptrPersonne);
	vector<CPersonne *> getVectEffectif();
};
