#pragma once

#include <string>
using namespace std;

class CPersonne
{
private:
	string nom;
	unsigned short int age;
public:
	CPersonne(string inom="",unsigned short int iage=0);
	virtual ~CPersonne(void);
	string getNom();
	void setNom(string inom);
	unsigned short int getAge();
	void setAge(unsigned short int iage);
	virtual string identifie()=0;
};
