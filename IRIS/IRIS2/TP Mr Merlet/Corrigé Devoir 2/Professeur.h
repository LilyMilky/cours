#pragma once
#include "personne.h"

class CProfesseur :
	public CPersonne
{
private:
	string discipline;
public:
	CProfesseur(string inomprof="", unsigned short int iage=0, string idiscipline="");
	~CProfesseur(void);
	string getDiscipline();
	void setDiscipline(string idiscipline);
	string identifie();
};
