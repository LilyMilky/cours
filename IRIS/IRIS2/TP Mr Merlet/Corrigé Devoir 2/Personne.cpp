#include "Personne.h"

CPersonne::CPersonne(string inom,unsigned short int iage)
{
	nom=inom;
	age=iage;
}

CPersonne::~CPersonne(void)
{
}

string CPersonne::getNom()
{
	return nom;
}

void CPersonne::setNom(string inom)
{
	nom=inom;
}

unsigned short int CPersonne::getAge()
{
	return age;
}
	
void CPersonne::setAge(unsigned short int iage)
{
	age=iage;
}
