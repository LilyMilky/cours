#include "Personne.h"
#include "Professeur.h"
#include "Eleve.h"
#include "Classe.h"
#include <iostream>
using namespace std;

void main()
{
	// affiche les caract�ristiques de l'objet pass� en param�tre
	// et celles des objets qui lui sont li�s
	void affiche(CClasse & classe1);	
	//construction d'un objet c1 de type CClasse avec un attribut nom="BTS2" 
	CClasse c1("BTS2");
	//construction d'un objet e1 de type CEleve avec des attributs initialis�s 
	//� des valeurs diff�rentes de celles par d�faut
	CEleve e1("Toto",20,15);
	//construction d'un objet p1 de type CProfesseur avec des attributs initialis�s 
	//� des valeurs diff�rentes de celles par d�faut
	CProfesseur p1("Durand",30,"Informatique");
	// pointeur sur le type CPersonne destin� � recevoir 
	// l'adresse d'un CProfesseur ou celle d'un CEleve
	CPersonne * ptr_CPersonne;
	

	bool valretsupp;
	int choix; 
	string nompersonne,disciplineprof;
	unsigned short int note, age;
	
	cout<<"Pour cr\202er un prof, choix 1"<<endl;
	cout<<"Pour cr\202er un eleve, choix 0"<<endl;
	cout<<"Votre choix:";
	cin>>choix;
	cout<<"Nom:";
	cin>>nompersonne;
	cout<<"Age:";
	cin>>age;
	if(choix)
	{
		cout<<"Discipline:";
		cin>>disciplineprof;
		//Allocation dynamique de m�moire et construction d'un objet
		ptr_CPersonne=new CProfesseur(nompersonne,age,disciplineprof);
	}
	else
	{
		cout<<"Note:";
		cin>>note;
		//Allocation dynamique de m�moire et construction d'un objet
		ptr_CPersonne=new CEleve(nompersonne,age,note);
	}

	affiche(c1);

	// Ajout d'un lien entre c1 et l'objet dynamique
	c1.ajoutLien(ptr_CPersonne);
	// Ajout d'un lien entre c1 et p1
	c1.ajoutLien(&p1);
	// Ajout d'un lien entre c1 et e1
	c1.ajoutLien(&e1);
	affiche(c1);

	// Suppression du lien entre c1 et l'objet dynamique
	valretsupp=c1.supprimerLien(ptr_CPersonne);
	cout<<"apres supp d'une personne \202tant li\202e a c1, valretsupp=";
	cout<<valretsupp<<endl;
	affiche(c1);

	// Suppression du lien entre c1 et l'objet dynamique
	valretsupp=c1.supprimerLien(ptr_CPersonne);
	cout<<"apres supp d'une personne n \202tant pas li\202e a c1, valretsupp=";
	cout<<valretsupp<<endl;
	affiche(c1);
	//??????????????????????
	delete ptr_CPersonne;
}


// affiche les caract�ristiques (attribut nom) de l'objet pass� en param�tre
// et certaines caract�ristiques des objets qui lui sont li�s :
//		- valeur de retour de la m�thode identifie()
//		- attributs nom et age
void affiche(CClasse & classe1)
{
	size_t NbPerslies,i;
	string identifiant;
	vector<CPersonne *> VecteurEffectif;
	
	cout<<"****Affichage des caract\202ristiques de la classe*****"<<endl;
	VecteurEffectif=classe1.getVectEffectif();
	// Affectation dans NbPerslies du nombre de personnes li�es � l'objet classe1
	NbPerslies=VecteurEffectif.size();
	cout<< NbPerslies<<" personnes sont li\202es a l'objet ";
	cout<<classe1.getNom()<<":"<<endl;
	for (i=0;i<NbPerslies;i++)
	{
		identifiant=VecteurEffectif[i]->identifie();
		cout<<identifiant<<"->"<<"Nom:"<<VecteurEffectif[i]->getNom();
		cout<<",Age:"<<VecteurEffectif[i]->getAge();
		if(identifiant=="Eleve")
		{
			cout<<", Note="<<((CEleve *)VecteurEffectif[i])->getNoteMoyenneAnnee();
			cout<<endl;
		}
		else
		{
			cout<<", Discipline="<<((CProfesseur *)VecteurEffectif[i])->getDiscipline();
			cout<<endl;
		}
	}
	cout<<"***Fin de l affichage des caract\202ristiques de la classe****"<<endl<<endl;
}
