#include "Professeur.h"

CProfesseur::CProfesseur(string inomprof, unsigned short int iage, string idiscipline)
			:CPersonne(inomprof,iage)
{
	discipline=idiscipline;
}

CProfesseur::~CProfesseur(void)
{
}


string CProfesseur::getDiscipline()
{
	return discipline;
}

void CProfesseur::setDiscipline(string idiscipline)
{
	discipline=idiscipline;
}

string CProfesseur::identifie()
{
	return "Professeur";
}