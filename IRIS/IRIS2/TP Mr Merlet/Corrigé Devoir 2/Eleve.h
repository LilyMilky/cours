#pragma once
#include "personne.h"

class CEleve :
	public CPersonne
{
private:
	unsigned short int noteMoyenneAnnee;	//note moyenne sur l'ann�e
public:
	CEleve(string inomeleve="", unsigned short int iage=0, unsigned short int inote=0);
	~CEleve(void);
	unsigned short int getNoteMoyenneAnnee();
	void setNoteMoyenneAnnee(unsigned short int inote);
	string identifie();
};
