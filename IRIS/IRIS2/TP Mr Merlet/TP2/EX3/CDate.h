#pragma once

static char * tinit = "01/01/2001";
//char const * tinit = "01/01/2001"; ne compile pas
class CDate
{
private:
	unsigned char Jour;
	unsigned char Mois;
	unsigned int Annee;
	char tchar_date [11];

public:
	CDate(void);				// constructeur sans arguments
	~CDate(void);				// destructeur
	bool setDate(char * t);		// modifie la date si elle est valide
	unsigned char getJour();	// retourne le jour
	unsigned char getMois();	// retourne le mois
	unsigned int getAnnee();	// retourne l'annee
	char * getTchar_date();		// retourne la date (chaine de caracteres C)
};

