/******************************************************************************************
Yann Lacquehay
13/09/10
EX3 TP2
******************************************************************************************/

#include "CDate.h"
#include <iostream>
using namespace std;

void main ()
{
	CDate *Dates;
	char Tab[11];
	bool OK;
	int NbDates;
	int i;

	cout << "Veuillez saisir le nombre de date que vous voulez saisir: ";
	cin >> NbDates;
	Dates = new CDate[NbDates];

	for (i = 0; i < NbDates; i++)
	{
		cout << "Veuillez saisir une date comprise entre 01/01/1990 et 31/12/2010: ";
		cin >> Tab;
		OK = Dates[i].setDate(Tab);

		if ( OK == false)
			cout << "La date n'est pas comprise entre 01/01/1990 et 31/12/2010: 01/01/2001 rentre" << endl;
		else
			cout << "La date est comprise entre 01/01/1990 et 31/12/2010" << endl;
	}
	for (i=0; i<NbDates; i++)
		cout << "La date " << i+1 << " est: " << Dates[i].getTchar_date() << "." << endl;
}