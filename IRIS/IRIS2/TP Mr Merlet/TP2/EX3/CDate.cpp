#include "CDate.h"
#include <iostream>
using namespace std;

CDate::CDate(void)
{
	strcpy_s(tchar_date , tinit);
}

CDate::~CDate(void)
{}

bool CDate::setDate(char * t)
{
	char jour[3];
	strncpy_s(jour,t,2);

	char mois[3];
	strncpy_s(mois,&t[3],2);

	char annee[5];
	strncpy_s(annee,&t[6],4);

	Jour = atoi(jour);
	Mois = atoi(mois);
	Annee = atoi(annee);

	if (Jour <= 31 && Jour >= 01)
	{
		if(Mois <= 12 && Mois >=01)
		{
			if(Annee <= 2010 && Annee >= 1990)
			{
				strcpy_s(tchar_date,t);
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;
}

unsigned char CDate::getJour()
{
	return Jour;
}

unsigned char CDate::getMois()
{
	return Mois;
}

unsigned int CDate::getAnnee()
{
	return Annee;
}

char *CDate::getTchar_date()
{
	return tchar_date;
}