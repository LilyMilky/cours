#include "Personne.h"

// contructeur � 2 arguments
CPersonne::CPersonne(double itaille, double ipoids,unsigned int Enfants,unsigned int *Ages)
{
	taille = itaille;
	poids = ipoids;
	nb_enfants = Enfants;
	ages_enfants = new unsigned int[nb_enfants];

	unsigned int j ;
	for (j = 0; j < nb_enfants; j++)
	{
		ages_enfants[j] = Ages[j] ;
	}
}

//constructeur sans arguments
CPersonne::CPersonne(void)
{
	taille = 1.70;
	poids = 75;
	nb_enfants = 0;
	ages_enfants = new unsigned int[nb_enfants];
}

//destructeur
CPersonne::~CPersonne(void)
{
	delete ages_enfants;
}

//retourne l'attribut taille
double CPersonne::getTaille()
{
	return taille;
}

//retourne l'attribut poids
double CPersonne::getPoids()
{
	return poids;
}

//modifie l'attribut taille
void CPersonne::setTaille(double itaille)
{
	taille=itaille;
}

//modifie l'attribut poids
void CPersonne::setPoids(double ipoids)
{
	poids=ipoids;
}

//retourne l'indice de masse corporelle de la personne
double CPersonne::indiceMasseCorporelle()
{
	return poids/(taille*taille);
}

// Retourne le nombre d'enfant
unsigned int CPersonne::getNbEnfants()
{
	return nb_enfants;
}

// Retourne l'age de l'indice choisis

unsigned int CPersonne::getAgeEnfant(unsigned int Indice)
{
	int Retour;

	if (Indice >= nb_enfants)
		Retour = 0;
	else 
		Retour = ages_enfants[Indice];

	return Retour;
}

// Permet de modifier nb_enfant et ages.
void CPersonne::setEnfants(unsigned int Enfants, unsigned int *Ages)
{
	if (!(Enfants == nb_enfants))
	{
		nb_enfants = Enfants ;		
	}

	for (int y=0; y<nb_enfants; y++)
	{
		if (ages_enfants[y] != Ages[y])
		{
			ages_enfants[y] = Ages[y] ;			
		}	
	}
}