#pragma once

class CPersonne
{
private:
	double taille,poids;				//taille en m, poids en kg
	unsigned int nb_enfants;
	unsigned int *ages_enfants;
public:
	CPersonne(void);					//construsteur sans argument
	CPersonne(double itaille, double ipoids, unsigned int nb_enfants, unsigned int *ages_enfants);	//construsteur avec 2 arguments
	~CPersonne(void);					//destructeur
	void setPoids (double ipoids);		//modifie l'attribut poids
	void setTaille (double itaille);	//modifie l'attribut taille
	double indiceMasseCorporelle();
	double getTaille();					//renvoie l'attribut taille 
	double getPoids();					//renvoie l'attribut poids
	unsigned int getNbEnfants();
	unsigned int getAgeEnfant(unsigned int Indice);
	void setEnfants (unsigned int Enfants, unsigned int *Ages);
};
