/******************************************************************************************
Yann Lacquehay
20/09/10
EX4 TP2
******************************************************************************************/

#include "Personne.h"
#include <iostream>
using namespace std;

void main ()
{
	unsigned int Nb_enfants = 0;
	unsigned int *AgesEnfants ;
	unsigned int *AgesEnfantsbis;
	unsigned int Choix = 0;		//Enfant choisis pour afficher age
	unsigned int Age =0;		//Age de l'enfant choisis

	cout << "Veuillez saisir le nombre d'enfant : ";			//Saisie du Nombre d'enfant
	cin  >> Nb_enfants;
	
	AgesEnfants = new unsigned int [Nb_enfants];				//Cr�ation du tableau 

	for (int i = 0; i < Nb_enfants ; i++)
	{
		cout << "Veuillez saisir l'age de l'enfant " << i+1 << " :";	//Saisie de l'age
		cin  >> AgesEnfants[i];
	}

	CPersonne Patchy(1.70,50,Nb_enfants, AgesEnfants);	//Cr�ation Personne Patchy

	cout << "Le taille de Patchy est : " << Patchy.getTaille() << "m" <<endl;
	cout << "Le poids de Patchy est : " << Patchy.getPoids() << "Kg" <<endl;
	cout << "Patchy a : " << Patchy.getNbEnfants() << " enfants" <<endl;

	cout << "Veuillez saisir le numero de enfant dont vous voulez afficher l'age: ";
	cin  >> Choix;

	Age = Patchy.getAgeEnfant(Choix - 1);
	if (Age == 0)
		cout << "Il n'y a pas d'enfant a ce numero" <<endl;
	else
		cout << "L'enfant " << Choix << " de Patchy a " << Age << " ans" <<endl;
//*******************************************************************************************
	cout << "Veuillez modifier le nombre d'enfant: ";
	cin  >> Nb_enfants;
	
	AgesEnfantsbis = new unsigned int [Nb_enfants];

	for (int i = 0; i < Nb_enfants ; i++)
	{
		cout << "Veuillez saisir l'age de l'enfant " << i + 1 << " :";	//Saisie de l'age
		cin  >> AgesEnfantsbis[i];
	}

	Patchy.setEnfants(Nb_enfants, AgesEnfantsbis);

	cout << "Le nouveau nombre d'enfant de Patchy est : "<< Patchy.getNbEnfants() <<endl;

	cout << "L'age du nouveau nombre d'enfant de Patchy est: " <<endl;
	for (int i = 0; i < Nb_enfants ; i++)
	{
		cout << "Enfant "<< i + 1 << "a : " << Patchy.getAgeEnfant(i) << "ans" <<endl;
	}
}