/******************************************************************************************
Yann Lacquehay
13/09/10
EX2 TP2
******************************************************************************************/

#include <iostream>
using namespace std;

void main()
{
	int nbEntier;
	int * Tableau;

	cout << "Veuillez saisir le nombre d'entier que vous voulez rentrer dans le tableau: ";
	cin  >> nbEntier;

	Tableau = new int[nbEntier];

	for (int i=0; i < nbEntier; i++)
	{
		cout << "Veuillez saisir l'entier " << i+1 << ": ";
		cin  >> Tableau[i];
	}

	for (int i=0; i < nbEntier; i++)
	{
		cout << Tableau[i] <<endl;
	}
	
	delete[] Tableau;
}