#include "CTraitement.h"

CAdministrateur::CAdministrateur()
 {
	 LigneJoueurActif = 0;
	 PartieEnCours = false;
 }

CAdministrateur::~CAdministrateur()
{
}
//##ModelId=4CD1A3C302B7
string CAdministrateur::LancerPartie()
{
	string Joueur;
	ifstream fluxE;
	char Lecture[50];
	infojoueur Info;

	fluxE.open(Nom_Joueur, ios::in);
	if(fluxE)
	{		
			fluxE.getline(Lecture,50);
			Joueur = Lecture;
			SendJoueurActifServeur();

			Info.ID = Lecture;
			Info.Scores = 0;
			VectJoueurs.push_back(Info);

			while(fluxE)
			{
				fluxE.getline(Lecture,50);
				Info.ID = Lecture;
				Info.Scores = 0;
				VectJoueurs.push_back(Info);
			}
			fluxE.clear(); //d�sactive tous les bits d�erreur
			fluxE.close(); //Fermeture du flux

			/*for(int i = 0; i < VectJoueurs.size(); i++)						//Affichage TEST
			{		
				cout << VectJoueurs[i].ID;
				cout << VectJoueurs[i].Scores;
			}*/
	}
	else
	{
		fluxE.clear(); //d�sactive tous les bits d�erreur
		Joueur = "Erreur";
	}

	return Joueur;
}

//##ModelId=4CEA645E0167
bool CAdministrateur::Classer()
{
	struct infojoueur Valeur;
	int i;
	bool Ecriture = true;
	
	for ( i=0; i< VectJoueurs.size() ; i++)
	{

		for( i=0; i< VectJoueurs.size() ;i++)
		{

			if (VectJoueurs[i+1].Scores > VectJoueurs[i].Scores)
			{

				Valeur = VectJoueurs[i];
				VectJoueurs[i] = VectJoueurs[i+1];
				VectJoueurs[i+1] = Valeur;
			}
		}
	}
	ofstream flux;

	flux.open(Classement,ios::out);

	if (flux)
	{
		for (i=0; i< VectJoueurs.size() ; i++ )
		{			
			flux << VectJoueurs[i].ID << " " << VectJoueurs[i].Scores << endl;
		}
	}
	else
	{
		Ecriture = false;
	}
	flux.close();

	return Ecriture;
}

//##ModelId=4CEA6EB901E4
string CAdministrateur::SendScores(string scores)
{
	ifstream fluxE;		//Flux Nom_Joueur
	ifstream FLUXE;		//Flux joueur actif
	int char1 /*Caractere de depart*/, somme = 0, i;
	char car[2];		//Caractere lu
	string val_ret = " ";

	if(PartieEnCours == true)
	{
		for(i = 0; i < 3; i++)
		{
			char1 = scores.find(";")-1;

			car[0] = scores[char1];
			car[1] = '\0';
			//cout << car[0] << endl; //Test Lecture Caractere

			scores.erase(char1,2);

			somme = somme + atoi(car);
		}
		cout << "somme : " << somme <<endl;  //Test Somme
	
		VectJoueurs[LigneJoueurActif].Scores = somme;		//Ecriture dans structure

		cout << VectJoueurs[LigneJoueurActif].ID << " : " << VectJoueurs[LigneJoueurActif].Scores <<endl; //Test Ecriture Structure

		LigneJoueurActif++;

		if(LigneJoueurActif == VectJoueurs.size())
			PartieEnCours = false;

		else
		{
			val_ret = VectJoueurs[LigneJoueurActif].ID;
			SendJoueurActifServeur();
		} 
	}
	else
	{}
	return val_ret;
}

//##ModelId=4CF208DE0393
void CAdministrateur::SendJoueurActifServeur()
{
	int i;
	char JoueurTempo[51];
	LigneJoueurActif++;
	//******************************************
	//	LECTURE DANS LE FICHIER Nom_Joueur.txt
	//******************************************
		fstream fluxE;
		fluxE.open(Nom_Joueur,ios::in); //ouverture d�un flux connect� au
											//fichier nom_fichier
		if(fluxE)		// le fichier existe et il est accessible
		{
			for(i = 0 ; i < LigneJoueurActif ; i++)	//On lit fichier jusqu'� ce que l'on lise
			{										//la ligne d�sir�e	
				fluxE.getline(JoueurTempo,51);
			}

			if(!(JoueurTempo[0] == '#'))
			{
				//------------------------------------------------------------------------
				//Pour tester la classe
							cout << "Player dans Nom_Joueur : " <<  JoueurTempo << endl;
				//------------------------------------------------------------------------
			//********************************************
			//	ECRITURE DANS LE FICHIER JoueurActif.txt
			//********************************************
				ofstream FluxEcriture;

				FluxEcriture.open(JoueurActif, ios::out );

				if(FluxEcriture)
				{
					FluxEcriture << JoueurTempo ;					
				}
				FluxEcriture.close();
			//********************************************
				
			}
			cout << "\nFin de Fichier\n";
			fluxE.close(); //Fermeture du flux


		}
		else		// flux dans un �tat d�erreur :le fichier n�existe pas ou il
		{			// est inaccessible
			
		}
}

