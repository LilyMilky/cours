#ifndef CTRAITEMENT_H_HEADER_INCLUDED_B30C310C
#define CTRAITEMENT_H_HEADER_INCLUDED_B30C310C

#include "infojoueur.h"
#include "Fichiers.h"
#include <string>
#include <vector>
#include <fstream>
#include <string.h>
#include <iostream>
using namespace std;

//##ModelId=4CB31D680119
class CAdministrateur
{
  public:

    CAdministrateur();
	~CAdministrateur();
    //##ModelId=4CD1A3C302B7
    string LancerPartie();

    //##ModelId=4CEA645E0167
    bool Classer();

    //##ModelId=4CEA6EB901E4
    string SendScores(string scores);

    //##ModelId=4CF208DE0393
    void SendJoueurActifServeur();

  private:
    //##ModelId=4CF2134E017F
    bool PartieEnCours;
    //##ModelId=4CF213860005
    vector<infojoueur> VectJoueurs;
	int LigneJoueurActif;
};



#endif /* CTRAITEMENT_H_HEADER_INCLUDED_B30C310C */
