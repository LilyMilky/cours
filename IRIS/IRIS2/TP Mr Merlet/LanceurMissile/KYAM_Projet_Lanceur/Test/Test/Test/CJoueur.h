#ifndef CJOUEUR_H_HEADER_INCLUDED_B30C7FB8
#define CJOUEUR_H_HEADER_INCLUDED_B30C7FB8

#include "Fichiers.h"
#include <string>
#include <vector>
#include <fstream>
#include <string.h>
#include <iostream>
using namespace std;

//##ModelId=4CF203F900DE
class CJoueur
{
  public:
    //##ModelId=4CF20468038B
    vector<string> GetClassement();

    //##ModelId=4CF2062E007D
    string GetIDJoueur();

    // Lit le fichier JoueurActif.txt
    // Si (Nomlu=NomJoueur) alors
    //      Modification du fichier
    // FinSi
    //##ModelId=4CF2065002EB
    string GetIDJoueurActifServeur();

    //##ModelId=4CF20FE7036F
    bool EnregistrementID(string IDjoueur);

  private:
    //##ModelId=4CF213EB007A
    string NomJoueur;
};



#endif /* CJOUEUR_H_HEADER_INCLUDED_B30C7FB8 */
