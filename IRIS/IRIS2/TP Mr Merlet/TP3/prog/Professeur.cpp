#include "Professeur.h"
#include <iostream>
using namespace std;

CProfesseur::CProfesseur(int inbclasses, double it, double ip):CPersonne(it,ip)
{
	nbclasses=inbclasses;	//initialisation des membres sp�cifiques de la classe d�riv�e
}

CProfesseur::~CProfesseur(void)
{
}

int CProfesseur::getNbclasses()
{
	return nbclasses;
}
CProfesseur::CProfesseur(void)
{
	nbclasses=2;
}

void CProfesseur::identifie()
{
	cout<<"Je suis un professeur"<<endl;
}