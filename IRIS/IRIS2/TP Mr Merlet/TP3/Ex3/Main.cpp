/*****************************************************
 Lacquehay Yann								 
 27/09/10								 
*****************************************************/
#include <iostream>
#include "Personne.h"
#include "Professeur.h"

using namespace std;

void main_identifie(CPersonne Yuyuko, CPersonne *PtrPers2, CPersonne & Youmu) ;

void main(void)
{
		CPersonne Patchy(1.50,50);
		CProfesseur Prof;
		CPersonne Yuyuko;
		CPersonne *PtrPers2;
		CPersonne Youmu;
		CPersonne *PtrPers;
		PtrPers = &Prof; 

		
		Patchy.identifie(); 
		Prof.identifie(); 
		PtrPers -> identifie();

		PtrPers -> Affiche();
		Yuyuko = Prof;
		PtrPers2 = &Prof;
		Youmu = Prof;
		main_identifie(Yuyuko,PtrPers2,Youmu);
}

void main_identifie(CPersonne Yuyuko, CPersonne *PtrPers2, CPersonne & Youmu)
{
	Yuyuko.identifie();		// appel de identifie() pour Yuyuko
	PtrPers2->identifie();	// appel de identifie() pour PtrPers2
	Youmu.identifie();		// appel de identifie() pour Youmu
}