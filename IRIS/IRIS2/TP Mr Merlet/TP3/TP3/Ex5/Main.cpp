/*****************************************************
 Lacquehay Yann								 
 27/09/10								 
*****************************************************/
#include <iostream>
#include "Personne.h"
#include "Professeur.h"
#include "CEleve.h"

using namespace std;


void main(void)
{
	char Choix;
	CPersonne *Ptr[5];

	for (int i = 0; i < 5; i++)
	{
		cout << "Pointeur "<< i <<" pour? (E = eleve, P= prof) : " << endl;
		cin  >> Choix;
		if (Choix == 'E')
			Ptr[i] = new CEleve;

		else if (Choix == 'P')
			Ptr[i] = new CProfesseur;

		Ptr[i] -> identifie();
	}

	delete [] *Ptr;
}
