#include "Personne.h"
#include <iostream>
using namespace std;

// contructeur �E2 arguments
CPersonne::CPersonne(double itaille, double ipoids)
{
	taille=itaille;
	poids=ipoids;
}

//constructeur sans arguments
CPersonne::CPersonne(void)
{
	taille=1.70;
	poids=75;
}

//destructeur
CPersonne::~CPersonne(void)
{
}

//retourne l'attribut taille
double CPersonne::getTaille()
{
	return taille;
}

//retourne l'attribut poids
double CPersonne::getPoids()
{
	return poids;
}

//modifie l'attribut taille
void CPersonne::setTaille(double itaille)
{
	taille=itaille;
}

//modifie l'attribut poids
void CPersonne::setPoids(double ipoids)
{
	poids=ipoids;
}

//retourne l'indice de masse corporelle de la personne
double CPersonne::indiceMasseCorporelle()
{
	return poids/(taille*taille);
}

void CPersonne::identifie()
{
	cout<<"Je suis une Personne"<< endl;
} 

void CPersonne::Affiche()
{
	cout << "Patchy pese: " << poids << "kg" << endl;
	cout << "Et mesure: " << taille << "m" << endl;
}