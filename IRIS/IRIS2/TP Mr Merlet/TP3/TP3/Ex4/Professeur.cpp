#include "Professeur.h"
#include "Personne.h"
#include <iostream>
using namespace std;

CProfesseur::CProfesseur(int inbclasses, double it, double ip):CPersonne(it,ip)
{
	nbclasses=inbclasses;	//initialisation des membres sp�cifiques de la classe d�riv�e
}

CProfesseur::~CProfesseur(void)
{
	cout << "Appel destructeur CProfesseur" << endl;
}

int CProfesseur::getNbclasses()
{
	return nbclasses;
}
CProfesseur::CProfesseur(void)
{
	nbclasses = 6;
}

void CProfesseur::identifie()
{
	cout<<"Je suis un Professeur" << endl;
}

void CProfesseur::Affiche()
{
	CPersonne::Affiche();
	cout << "Nombre de classe: " << nbclasses << endl;

	
}