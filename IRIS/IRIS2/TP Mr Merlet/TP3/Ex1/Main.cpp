/*****************************************************
 Lacquehay Yann								 
 27/09/10								 
*****************************************************/
#include <iostream>
#include "Personne.h"
#include "Professeur.h"

using namespace std;

void main(void)
{
		CPersonne Patchy(1.50,50);
		CProfesseur Prof;
		CPersonne * PtrPers;
		PtrPers = &Patchy; 
		
		Patchy.identifie(); 
		Prof.identifie(); 
		PtrPers -> identifie();

		PtrPers -> Affiche();
		PtrPers = &Prof;
		PtrPers -> Affiche();

	



}