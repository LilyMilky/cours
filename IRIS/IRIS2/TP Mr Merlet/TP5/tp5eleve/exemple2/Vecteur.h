#pragma once
#include "MonExcept.h"

// La classe CVecteur est une classe qui permet de g�rer des 
// tableaux de double de taille variable
// Ce tableau est allou� dynamiquement

class CVecteur
{
private:
	double * ptrtab;
	int NbElements;
public:
	CVecteur(int nbelements=0);	// constructeur poss�dant un argument par d�faut
	~CVecteur(void);			//destructeur
	double GetElement(int indice);	//retourne l'element correspondant � l'indice
	void SetElement(int indice,double val); //affecte val dans l'element correspondant � l'indice
	int GetNbElements();		// retourne le nombre d'�l�ments du vecteur
};
