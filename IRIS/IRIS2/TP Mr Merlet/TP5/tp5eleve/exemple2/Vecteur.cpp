#include "Vecteur.h"

CVecteur::CVecteur(int nbelements)
{
	int i;
	this->NbElements=nbelements;
	this->ptrtab=new double [NbElements];
	for(i=0;i<NbElements;i++)
		ptrtab[i]=0;
}

CVecteur::~CVecteur(void)
{
	delete ptrtab;
}

double CVecteur::GetElement(int indice)
{
	CMonExcept Me;
	if (indice <0 || indice >=NbElements)
		throw Me;
	return ptrtab[indice]; //pas de controle du debordement
}

void CVecteur::SetElement(int indice,double val)
{
	ptrtab[indice]=val;
}

int CVecteur::GetNbElements()
{
	return this->NbElements;
}
