#include "Vecteur.h"	//on inclut la declaration de la classe CVecteur
#include "MonExcept.h"
#include <iostream>		/*on inclut le fichier <iostream> qui contient les declarations 
						necessaires � l'utilisation des entr�es sorties standard*/
using namespace std;	// on utilise les symboles definis dans l'espace de noms std

void main()
{
	int i;
	CVecteur V1;
	cout<<"Taille de V1="<<V1.GetNbElements()<<endl;
	CVecteur V2(5);
	cout<<"Taille de V2="<<V2.GetNbElements()<<endl;
	cout<<"Affichage des elements du vecteur V2:"<<endl; 
	for (i=0;i<V2.GetNbElements();i++)
		cout<<V2.GetElement(i)<<" ";
	cout<<endl;
	try
	{
		do
		{
			cout<<"Donnez un indice:";
			cin>>i;
			cout<<"Element d'indice "<<i<<":"<<V2.GetElement(i)<<endl;
		}
		while(i);
	}
	catch(CMonExcept e)
	{
		cout<<"Debordement d'indice"<<endl;
		//exit(-1);
	}
	cout<<"sortie du main"<<endl;
}