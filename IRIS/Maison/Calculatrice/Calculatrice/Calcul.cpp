/**********************************************************************************************************************************************
Lacquehay
Yann
Date: 02/10/09
Programme : Calculatrice
**********************************************************************************************************************************************/

#include <iostream>
using namespace std;
/*********************************************************************************************************************************************
													----Programme Principal----
********************************************************************************************************************************************/

 void main (void)
 {

	int Choixmenu, Reset;
	float Nombre1, Nombre2, Resultat;
	char Pseudo[] = "Milky";
	
	Resultat = 0;
	Nombre1 = 1;
	cout << "********************************************************************************";
	cout << "\t  ******Bienvenue sur la calculatrice cree par "<< Pseudo <<"******\n";
	cout << "********************************************************************************\n";

	Choixmenu = 1;

	while (Choixmenu > 0)																	//Menu de Selection
	{
		cout << "\n1) Addition\n";
		cout << "2) Soustraction\n";
		cout << "3) Multiplication\n";
		cout << "4) Division\n";
		cout << "Ou tapez 0 si vous voulez quitter\n\n";
		cout << "Quel calcul desirez vous faire? Selectionner avec le chiffre desire: ";
		cin  >> Choixmenu;
		cout << endl;
	
		if (Choixmenu == 1)
		{
			cout << "********************************************************************************";
			cout << "\t\t\t******Addition******\n";
			cout << "********************************************************************************\n";

			cout << "Veuillez taper un nombre a ajouter a " << Resultat << ": ";			//On ajoute la valeur X au resultat
				cin  >> Nombre1;
				cout << endl;
				cout << Resultat << " + " << Nombre1;

				Resultat = Resultat + Nombre1;

				cout << " = " << Resultat <<endl;
			
		}

		if (Choixmenu == 2)
		{
			cout << "********************************************************************************";
			cout << "\t\t\t******Soustraction******\n";
			cout << "********************************************************************************\n\n";

				if (Resultat != 0)															//Si Resultat n'est pas nul, on soustrait
				{																			//une valeur à Resultat
					cout << "Veuillez taper la valeur a soustraire a " << Resultat << ": ";		
					cin  >> Nombre1;
					cout << Resultat;

					Resultat = Resultat - Nombre1;

					cout << " - " << Nombre1 << " = " << Resultat;
				}

				if (Resultat == 0)															//Si resultat est nul, on demande 2 valeurs
				{																			//à l'utilisateur 
					cout << "Veuillez taper une premiere valeur: ";
					cin  >> Nombre1;
					cout << "Veuillez saisir une deuxieme valeur: ";
					cin  >> Nombre2;
					cout << Nombre1 << " - " << Nombre2 << " = " << Resultat;

					Resultat = Nombre1 - Nombre2;
				}

		}	

		if (Choixmenu == 3)
		{
			cout << "********************************************************************************";
			cout << "\t\t\t******Multiplication******\n";
			cout << "********************************************************************************\n\n";

				if (Resultat != 0)															// Si resultat n'est pas nul, on multiplie
				{																			// une valeur à resultat
					cout << "Veuillez taper la valeur a multiplier a " << Resultat << ": ";
					cin  >> Nombre1;
					cout << Resultat;

					Resultat = Resultat * Nombre1;

					cout << " * " << Nombre1 << " = " << Resultat;
				}
				if (Resultat == 0)															//Si Resultat est nul, on demande 2 valeurs
				{																			//à l'utilisateur
					cout << "Veuillez taper une premiere valeur: ";
					cin  >> Nombre1;
					cout << "Veuillez saisir une deuxieme valeur: ";
					cin  >> Nombre2;

					Resultat = Nombre1 * Nombre2;

					cout << Nombre1 << " * " << Nombre2 << " = " << Resultat;
				}
				
	
		}

		if (Choixmenu == 4)
		{
			cout << "********************************************************************************";
			cout << "\t\t\t******Division******\n";
			cout << "********************************************************************************\n\n";

				if (Resultat != 0)															//Si Resultat n'est pas nul, on divise
				{																			//une valeur à resultat
					cout << "Veuillez taper la valeur a diviser a " << Resultat << ": ";
					cin  >> Nombre1;
					cout << Resultat;

					Resultat = Resultat / Nombre1;

					cout << " / " << Nombre1 << " = " << Resultat;
				}
				if (Resultat == 0)															//Si Resultat est nul, on demande 2 valeurs
				{																			//à l'utilisateur
					cout << "Veuillez taper une premiere valeur: ";
					cin  >> Nombre1;
					cout << "Veuillez saisir une deuxieme valeur: ";
					cin  >> Nombre2;
					cout << endl;

					Resultat = Nombre1 / Nombre2;

					cout << Nombre1 << " / " << Nombre2 << " = " << Resultat;
				}
		}

		if (Choixmenu > 4)																	//Si l'utilisateur ne rentre pas un chiffre en 
		{																					//0 et 4
			cout << "********************************************************************************";
			cout << "\t\t\tVeuillez taper un nombre entre 0 et 4\n";
			cout << "********************************************************************************\n";
		}
		
		if (Choixmenu <= 4)																	//Si l'utilisateur effectué un calcul
		{																					//Demande si conservation du Résultat
			cout << "\n\nVoulez vous garder le resultat obtenu? Tapez 1 pour le conserver sinon tapez un autre chiffre: ";
			cin  >> Reset;

			if (Reset != 1)
			{
				Resultat = 0;
			}
		}
	}
 }