<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${estVide}">
    Panier vide !!!    
</c:if>

<c:if test="${!estVide}">            
    <c:forEach var="i" items="${list}">
        ${i.titreLivre} - ${i.prixHTLivre}&euro; ---- Quantite : ${i.qty} - Prix : ${i.prixTotal}&euro;
        <a href="Controller?section=panier&add=${i.isbnLivre}">+</a>
        <a href="Controller?section=panier&dec=${i.isbnLivre}">-</a>
        <a href="Controller?section=panier&del=${i.isbnLivre}">X</a>
        <br>    
    </c:forEach>              
    Prix Total : ${prixTotal}&euro;   <br>         
    <a href="Controller?section=panier&clear">Vider le panier</a>
</c:if>

<form name="formAdresse" action="Controller">
    <input type="hidden" name="section" value="choixAdresse"/>
    <input type="submit" value="Valider" name="doIt" />
</form>    
