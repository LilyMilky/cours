<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Choix Paiement!</h1>

        <form name="formPaiement" action="Controller">
            <input type="hidden" name="section" value="paiement"/>
            Choix du site de paiement : <select name = "site">
                <c:forEach var="site" items="${listsite}">
                    <option value="${site.nomSitePaiement}"> ${site.nomSitePaiement} </option>
                </c:forEach>
            </select>
            <br>
            Choix du transporteur : <select name = "transporteur">
                <c:forEach var="transporteur" items="${listtransporteur}">
                    <option value="${transporteur.nomTransporteur}"> ${transporteur.nomTransporteur} </option>
                </c:forEach>
            </select>
            <br>
            <input type="submit" value="Valider" name="doIt" />
        </form>

    </body>
</html>
