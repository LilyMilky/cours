package Beans;

import Classes.*;
import BDD.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class beanPanier implements Serializable {
    
    HashMap<String,Livre> map;
    String adresseLivraison;
    String adresseFacturation;
    Transporteur trans;
    SitePaiement site;

    public beanPanier() {
        this.map = new HashMap();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER/SETTER">
  
    public Transporteur getTrans() {
        return trans;
    }
    
    public void setTrans(Transporteur trans) {
        this.trans = trans;
    }
    
    public SitePaiement getSite() {
        return site;
    }
    
    public void setSite(SitePaiement site) {
        this.site = site;
    }
    
    public String getAdresseLivraison() {
        return adresseLivraison;
    }
    
    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }
    
    public String getAdresseFacturation() {
        return adresseFacturation;
    }
    
    public void setAdresseFacturation(String adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }
//</editor-fold>
    
    public void add(String isbnLivre){
        
        LivreBDD livrebdd = new LivreBDD("SQLServer");
        Livre l = livrebdd.sqlGetLivre(isbnLivre);
        l.setQty(1);
        add(l, 1);
        
    }
    
    public void add(Livre l, int qty){
        if(map.containsKey(l.getIsbnLivre()))
        {
            Livre i = map.get(l.getIsbnLivre());
            i.ajoutQty(qty);
        }
        else
        {
            this.map.put(l.getIsbnLivre(), l);
        }
    }
    
    public void add(String isbnLivre, int qty){
        if(map.containsKey(isbnLivre))
        {
            Livre i = map.get(isbnLivre);
            i.ajoutQty(qty);
        }
        else
        {
            LivreBDD livrebdd = new LivreBDD("SQLServer");
            Livre l = livrebdd.sqlGetLivre(isbnLivre);
            
            this.map.put(l.getIsbnLivre(), l);
        }
    }
    
    public void del(String ref){
        this.map.remove(ref);
    }
    
    public void dec(String isbnLivre){
        dec(isbnLivre, -1);
    }
    
    public void dec(String isbnLivre, int qty){
        if(map.containsKey(isbnLivre))
        {
            Livre i = map.get(isbnLivre);
            i.ajoutQty(qty);
            
            if(i.getQty() <1)
                del(isbnLivre);
        }
    }
    
    public void clear(){
        map.clear();
    }
    
    public boolean isEmpty(){
        return map.isEmpty();
    }
    
    public Collection getList(){
        return map.values();
    }

    public int getNbArticle(){
        return map.size();
    }
 
    public float getTotal(){
        float prixTotal = 0;
        
        Iterator entries = map.entrySet().iterator();
            while(entries.hasNext())
            {
                Entry thisEntry = (Entry)entries.next();
                Livre l = (Livre) thisEntry.getValue();
                
                prixTotal += l.getPrixTotal();
            }

           // prixTotal = (float)((int)prixTotal*100)/100;
            
        return prixTotal;
    }       
}
