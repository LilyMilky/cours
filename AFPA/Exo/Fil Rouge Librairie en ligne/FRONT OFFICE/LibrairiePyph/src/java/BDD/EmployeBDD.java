package BDD;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hervé
 */
public class EmployeBDD {

    // La methode qui ajoute un employe dans la base 
    public int AjouterEmploye(String nomEmploye, String prenomEmploye, String emailEmploye, String telephoneEmploye, int hideEmploye) {
        Connexion con = new Connexion();
        int res = 0;
        con.connexion();
        try {
            String requete = "insert into employe (nomEmploye, prenomEmploye,emailemploye,telephoneEmploye,hideEmploye) values('" + nomEmploye + "','" + prenomEmploye + "','" + emailEmploye + "','" + telephoneEmploye + "'," + hideEmploye + ")";

            Statement stmt = con.getConnexion().createStatement();
            res = stmt.executeUpdate(requete);

            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(EmployeBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
        con.close();
        return res;
    }

    // La methode qui fait une  mise  à jour ou une modification 
    public int modifierEmploye(int idEmploye, String nomEmploye, String prenomEmploye, String emailEmploye, String telephoneEmploye) {
        Connexion con = new Connexion();
        int result = 0;
        con.connexion();
        try {

            String requete = "Update employe set nomEmploye = '" + nomEmploye
                    + "',  prenomEmploye = '" + prenomEmploye
                    + "',  emailEmploye= '" + emailEmploye
                    + "', telephoneEmploye= '" + telephoneEmploye
                    + "'  where idEmploye = " + idEmploye;

            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(requete);
            stmt.close();

        } catch (Exception ex) {

            System.out.println("oops" + ex.getMessage());
        }
        con.close();
        return result;

    }

    // Changer la colonne hyde en 0
    public int supprimerEmploye(int hideEmploye, int idEmploye) {
        Connexion con = new Connexion();
        int result = 0;
        con.connexion();
        try {

            String requete = "Update employe set hideEmploye= " + hideEmploye
                    + " where idEmploye = " + idEmploye;

            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(requete);

        } catch (Exception ex) {

            System.out.println("oops" + ex.getMessage());
        }
        con.close();
        return result;
    }

}
