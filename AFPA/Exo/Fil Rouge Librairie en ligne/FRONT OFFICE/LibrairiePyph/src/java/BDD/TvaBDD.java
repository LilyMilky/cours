package BDD;

import Classes.Tva;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class TvaBDD {

    public void enregistrerTva(Connexion con, String tauxTva, int typeTva, String dateApplicable) {
        float tauxTvaFloat = Float.valueOf(tauxTva);
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO tva(tauxTva, typeTva, dateTva) VALUES(" + tauxTvaFloat + ", " + typeTva + ", '" + dateApplicable + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
    //Permet d'afficher le taux de TVA dans les labels correspondants.
    public Vector recupererTva(int typeTva, Connexion con){
        Vector v = new Vector();
        con.connexion();
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT top 1 * FROM tva WHERE dateTva <= GETDATE() AND typeTva = " + typeTva +  " ORDER BY dateTva desc";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new Tva(rs.getFloat("tauxTva"), rs.getInt("typeTva"), rs.getInt("idTva"), rs.getString("dateTva")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            con.close();
            return v;
        }
        con.close();
        return v;
    }
    
    public Float recupererTauxTVA(int idTva, Connexion con)
    {
        Float tauxTVA = 0f;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT tauxTva FROM tva WHERE idTva = "+ idTva;
            ResultSet rs = stmt.executeQuery(query);
            
            rs.next();
            tauxTVA = rs.getFloat("tauxTva");
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return tauxTVA;
        }

        return tauxTVA;
    }
}
