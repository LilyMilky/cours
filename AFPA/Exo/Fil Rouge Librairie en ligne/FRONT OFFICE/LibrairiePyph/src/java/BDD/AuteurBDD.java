package BDD;

import Classes.Auteur;
import Classes.Livre;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class AuteurBDD {

    private Connection connexion = null;

    public AuteurBDD(String sqlProtocol) {
        if (sqlProtocol.equals("SQLServer")) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException ex) {
                System.err.println("Oops: Classe pas trouvé: " + ex.getMessage());
            }

            try {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=Librairie;user=sa;password=sa;";
                connexion = DriverManager.getConnection(connectionUrl);
            } catch (SQLException ex) {
                System.err.println("Oops:SQLConnection:" + ex.getErrorCode() + "/" + ex.getMessage());
            }
        }
    }

    public Auteur sqlGetAuteur(String AuteurToGet) {
        Auteur subResult = new Auteur();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM auteur WHERE nomAuteur=" + "'" + AuteurToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);
            
            
            while (rs.next()) {
                subResult.setIdAuteur(Integer.parseInt(rs.getString("idAuteur")));
                subResult.setNomAuteur(rs.getString("nomAuteur"));
                subResult.setPrenomAuteur(rs.getString("prenomAuteur"));
                subResult.setDateNaissanceAuteur(rs.getString("dateNaissanceAuteur"));
                subResult.setDateDecesAuteur(rs.getString("dateDecesAuteur"));
                subResult.setAvisAuteur(rs.getString("avisAuteur"));
            }
            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return subResult;
    }

    public void sqlAddAuteur(Auteur paramAuteur) {
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "INSERT INTO auteur (nomAuteur,prenomAuteur,dateNaissanceAuteur,dateDecesAuteur,avisAuteur) VALUES ("
                    + "'" + paramAuteur.getNomAuteur() + "',"
                    + "'" + paramAuteur.getPrenomAuteur() + "',"
                    + "'" + paramAuteur.getDateNaissanceAuteur() + "',"
                    + "'" + paramAuteur.getDateDecesAuteur() + "',"
                    + "'" + paramAuteur.getAvisAuteur() + "'"
                    + ");";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateAuteur(Auteur paramAuteur) {
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            System.out.println("idauteur" + paramAuteur.getIdAuteur());

            query = "UPDATE auteur SET "
                    + "nomAuteur='" + paramAuteur.getNomAuteur() + "',"
                    + "prenomAuteur='" + paramAuteur.getPrenomAuteur() + "',"
                    + "dateNaissanceAuteur='" + paramAuteur.getDateNaissanceAuteur() + "',"
                    + "dateDecesAuteur='" + paramAuteur.getDateDecesAuteur() + "',"
                    + "avisAuteur='" + paramAuteur.getAvisAuteur() + "'"
                    + " WHERE idAuteur='" + paramAuteur.getIdAuteur() + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateLivre(Livre tempLivre, int auteurChoix, int rubriqueChoix, ArrayList<Integer> selectedMotCle) {
        for (int z : selectedMotCle) {
            System.out.println(z);
        }
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            String isbnStr = tempLivre.getIsbnLivre();
            query = "UPDATE livre SET "
                    + "numeroSiretEditeur='" + tempLivre.getNumeroSiretEditeur() + "',"
                    + "idFormat='" + tempLivre.getIdFormat() + "',"
                    + "titreLivre='" + tempLivre.getTitreLivre() + "',"
                    + "sousTitreLivre='" + tempLivre.getSousTitreLivre() + "',"
                    + "adresseImageLivre='" + tempLivre.getAdresseImageLivre() + "',"
                    + "resumeLivre='" + tempLivre.getResumeLivre() + "',"
                    + "stockLivre='" + tempLivre.getStockLivre() + "',"
                    + "collectionLivre='" + tempLivre.getCollectionLivre() + "',"
                    + "prixHTLivre='" + tempLivre.getPrixHTLivre() + "',"
                    + "avisLivre='" + tempLivre.getAvisLivre() + "'"
                    + " WHERE isbnLivre='" + isbnStr + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

            Statement stmt2del = connexion.createStatement();
            String query2del = "";

            query2del = "DELETE FROM ecriture WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result2del = stmt2del.executeUpdate(query2del);
            stmt2del.close();

            Statement stmt2 = connexion.createStatement();
            String query2 = "";

            query2 = "INSERT INTO ecriture (isbnLivre,idAuteur) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + auteurChoix + "'"
                    + ");";

            int result2 = stmt2.executeUpdate(query2);
            stmt2.close();

            Statement stmt3del = connexion.createStatement();
            String query3del = "";

            query3del = "DELETE FROM participation WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result3del = stmt3del.executeUpdate(query3del);
            stmt3del.close();

            Statement stmt3 = connexion.createStatement();
            String query3 = "";

            query3 = "INSERT INTO participation (isbnLivre,idRubrique) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + rubriqueChoix + "'"
                    + ");";

            int result3 = stmt3.executeUpdate(query3);
            stmt3.close();

            Statement stmt4del = connexion.createStatement();
            String query4del = "";

            for (int a : selectedMotCle) {
                query4del = "DELETE FROM referencement WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

                int result4del = stmt4del.executeUpdate(query4del);
            }

            stmt4del.close();

            Statement stmt4 = connexion.createStatement();
            String query4 = "";

            for (int a : selectedMotCle) {
                query4 = "INSERT INTO referencement (idMotCle,isbnLivre) VALUES ("
                        + "'" + a + "',"
                        + "'" + tempLivre.getIsbnLivre() + "'"
                        + ");";

                int result4 = stmt4.executeUpdate(query4);
            }

            stmt4.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlClose() {
        try {
            connexion.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:close:" + ex.getMessage());
        }

    }
}
