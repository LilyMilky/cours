/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BDD;

import Classes.MotCle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author Philippe Crombez
 */
public class FormatBDD {

    // Methode pour retourner un vecteur pour l'initialisation de la liste 
    public Vector initVectorFormat(Connexion con) {
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM format ORDER BY nomFormat";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idFormat"), rs.getString("nomFormat")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    // Méthode pour initialliser une liste en fonction du champ 'recherche rapide'
    public Vector initVectorFormat(Connexion con, String saisieRapide) {
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM format WHERE nomFormat LIKE '" + saisieRapide + "%' ORDER BY nomFormat";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idFormat"), rs.getString("nomFormat")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    // Méthode pour modifier un format dans la BDD
    public void modifierFormat(String ancienFormat, String formatModifie, Connexion con) {
        
        long index=0;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM format ORDER BY nomFormat";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if (ancienFormat.equalsIgnoreCase(rs.getString("nomFormat"))) {
                    index = rs.getLong("idFormat");
                }
            }
            System.out.println(index);
            query = "UPDATE format SET nomFormat=? FROM theme WHERE idFormat=?";
            PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
            stmt2.setString(1, formatModifie);
            stmt2.setLong(2, index);
            stmt2.executeUpdate();
            
            stmt2.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
    }

    //Méthode pour vérifier si un format existe ou non
    public boolean isExist(String nomFormat, Vector v) {
        Iterator it = v.iterator();
        
        while(it.hasNext()){
            String test = it.next().toString();
            if(nomFormat.equalsIgnoreCase(test)){
                return true;
            }
        }
        return false;
    }

    // Méthode pour ajouter un format à la BDD
    public void AjouterFormat(String nouveauFormat, Connexion con) {
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO format(nomFormat) VALUES('" + nouveauFormat + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
}
