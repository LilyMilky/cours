package BDD;

import Classes.Commentaire;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Philippe Crombez
 */
public class CommentaireBDD {

    // Methode pour rechercher le nom du client en fonction de son idClient
    public String rechercherNomClient(int idClient){
        String nomClient = null;
        Connexion con = new Connexion();
        String index = String.valueOf(idClient);
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT nomCoordonnees, prenomCoordonnees FROM coordonnees AS co JOIN reglement AS re ON co.idCoordonnees = re.idCoordonnees"
                    + " WHERE idClient = " + index;
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                nomClient = rs.getString("nomCoordonnees") + " " + rs.getString("prenomCoordonnees");
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        con.close();
        return nomClient;
    }
    
    // Méthode pour rechercher le commentaire du client en fonction de l'isbn du livre et du numéro de commande
    public String rechercherCommentaire(Connexion con, String numeroCommande, String isbnLivre) {
        String texteCommentaire = null;
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT texteCommentaire FROM commentaire WHERE isbnLivre = '" + isbnLivre + "' "
                    + "AND numeroCommande = '" + numeroCommande + "'";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                texteCommentaire = rs.getString("texteCommentaire");
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        con.close();
        return texteCommentaire;
    }
    
    // Méthode pour rechercher le commentaire du client en fonction de l'isbn du livre et du numéro de commande
    public String rechercherCommentaire(String numeroCommande, Connexion con, String isbnLivre) {
        String texteModerationCommentaire = null;
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT texteModerationCommentaire FROM commentaire WHERE isbnLivre = '" + isbnLivre + "' "
                    + "AND numeroCommande = '" + numeroCommande + "'";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                texteModerationCommentaire = rs.getString("texteModerationCommentaire");
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        con.close();
        return texteModerationCommentaire;
    }

    // Méthode renvoyant un vecteur pour la recherche par nom du client
    public Vector rechercherCommentaireBDD(Connexion con, String nomClient) {
        Vector v = new Vector();
        
        try {
            
            int idClient = rechercherNomClient(con, nomClient);
            
            String query = "SELECT * FROM commentaire WHERE idClient = '"+ idClient + "' ORDER BY idClient";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Méthode récupérer l'idClient avec le nom lors de la recherche
    public int rechercherNomClient(Connexion con, String nomClient){
        int idClient=0;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT idClient FROM reglement AS re JOIN coordonnees AS co ON co.idCoordonnees = re.idCoordonnees"
                    + " WHERE co.nomCoordonnees = '" + nomClient + "'";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                idClient = rs.getInt("idClient");
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        
        return idClient;
    }

    //Methode pour la recherche en fonction du nom du client et de l'isbn du livre
    public Vector rechercherCommentaireBDD(Connexion con, String nomClient, String isbnLivre) {
        Vector v = new Vector();
        
        try {
            
            int idClient = rechercherNomClient(con, nomClient);
            
            String query = "SELECT * FROM commentaire WHERE idClient = '"+ idClient + "' AND isbnLivre = '" + isbnLivre + "' ORDER BY idClient";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    //Surcharge de la methode pour la recherche en fonction du nom du client et du numéro client
    public Vector rechercherCommentaireBDD(String nomClient, Connexion con, String NumeroCommande) {
        Vector v = new Vector();
        
        try {
            
            int idClient = rechercherNomClient(con, nomClient);
            
            String query = "SELECT * FROM commentaire WHERE idClient = '"+ idClient + "' AND numeroCommande = '" + NumeroCommande + "' ORDER BY idClient";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    //Surcharge de la methode pour la recherche en fonction de l'isbn du livre
    public Vector rechercherCommentaireBDD(String isbnLivre, Connexion con) {
        Vector v = new Vector();
        
        try {
            
            String query = "SELECT * FROM commentaire WHERE isbnLivre = '"+ isbnLivre + "' ORDER BY isbnLivre";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    //Surcharge de la methode pour la recherche en fonction de l'isbn du livre et du numéro de commande
    public Vector rechercherCommentaireBDD(String isbnLivre, String NumeroCommande, Connexion con) {
        Vector v = new Vector();
        
        try {
            
            String query = "SELECT * FROM commentaire WHERE isbnLivre = '"+ isbnLivre + "' AND numeroCommande = '" + NumeroCommande + "' ORDER BY idClient";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    //Methode pour la recherche en fonction du numéro de commande
    public Vector rechercherCommentaireBDD2(String numeroCommande, Connexion con) {
        Vector v = new Vector();
        
        try {
            
            String query = "SELECT * FROM commentaire WHERE numeroCommande = '"+ numeroCommande + "' ORDER BY numeroCommande";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    //Surcharge de la methode pour la recherche en fonction de l'isbn du livre, du nom du client et du numéro de commande
    public Vector rechercherCommentaireBDD(String nomClient, String isbnLivre, String NumeroCommande, Connexion con) {
        Vector v = new Vector();
        
        try {
            
            int idClient = rechercherNomClient(con, nomClient);
            
            String query = "SELECT * FROM commentaire WHERE idClient = '"+ idClient + "' AND isbnLivre = '" + isbnLivre + "' AND numeroCommande = '" + NumeroCommande + "' ORDER BY idClient";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre")
                        , rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    //Methode pour mettre à jour la BDD
    public void enregristrerCommentaire(Connexion con, String moderationCommentaire, String texteCommentaire
            , String texteModerationCommentaire, String isbnLivre, String numeroCommande) {
        int valeurModerationMessage;
        
        if(moderationCommentaire.equalsIgnoreCase("false")){
            valeurModerationMessage = 1;
        }else{
            valeurModerationMessage = 0;
        }
        
        try { 
            String query = "UPDATE commentaire SET moderationCommentaire=?, texteCommentaire=?"
                    + ", texteModerationCommentaire=? FROM commentaire WHERE isbnLivre=? AND numeroCommande=?";
            PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
            stmt2.setInt(1, valeurModerationMessage);
            stmt2.setString(2, texteCommentaire);
            stmt2.setString(3, texteModerationCommentaire);
            stmt2.setString(4, isbnLivre);
            stmt2.setString(5, numeroCommande);
            stmt2.executeUpdate();
            
            stmt2.close();
            
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
    public Vector initialiserModele(Connexion con){
        Vector v = new Vector();
        try {
            String query = "SELECT * FROM commentaire ORDER BY idClient;";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Commentaire p = new Commentaire(rs.getString("numeroCommande"), rs.getInt("idClient"), rs.getString("isbnLivre"), rs.getString("texteCommentaire"), rs.getString("dateCommentaire"), rs.getInt("moderationCommentaire"));
                v.add(p.getVector());
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        con.close();
        return v;
    }
}
