package BDD;

import Classes.Auteur;
import Classes.Editeur;
import Classes.Format;
import Classes.Livre;
import Classes.MotCle;
import Classes.Rubrique;
import Classes.Tva;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JOptionPane;

public class LivreBDD {

    private Connection connexion = null;

    
    public LivreBDD(String sqlProtocol) {
        if (sqlProtocol.equals("SQLServer")) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException ex) {
                System.err.println("Oops: Classe pas trouvé: " + ex.getMessage());
            }

            try {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=Librairie;user=sa;password=sa;";
                connexion = DriverManager.getConnection(connectionUrl);
            } catch (SQLException ex) {
                System.err.println("Oops:SQLConnection:" + ex.getErrorCode() + "/" + ex.getMessage());
            }
        }
    }

    public ArrayList<Auteur> sqlGetAuteur() {
        ArrayList<Auteur> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM auteur;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Auteur subResult = new Auteur();
                subResult.setIdAuteur(Integer.parseInt(rs.getString("idAuteur")));
                subResult.setNomAuteur(rs.getString("nomAuteur"));
                subResult.setPrenomAuteur(rs.getString("prenomAuteur"));
                subResult.setDateNaissanceAuteur(rs.getString("dateNaissanceAuteur"));
                subResult.setDateDecesAuteur(rs.getString("dateDecesAuteur"));
                subResult.setAvisAuteur(rs.getString("avisAuteur"));

                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }
    
    public ArrayList<Format> sqlGetFormat() {
        ArrayList<Format> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM format;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Format subResult = new Format();

                subResult.setIdFormat(rs.getInt("idFormat"));
                subResult.setNomFormat(rs.getString("nomFormat"));
                
                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }

    public ArrayList<Editeur> sqlGetEditeur() {
        ArrayList<Editeur> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM editeur;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Editeur subResult = new Editeur();

                subResult.setNumeroSiretEditeur(rs.getString("numeroSiretEditeur"));
                subResult.setIdCoordonnees(Integer.parseInt(rs.getString("idCoordonnees")));
                subResult.setNomEditeur(rs.getString("nomEditeur"));
                subResult.setAvisEditeur(rs.getString("avisEditeur"));

                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }

    public ArrayList<Rubrique> sqlGetRubrique() {
        ArrayList<Rubrique> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM rubrique;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Rubrique subResult = new Rubrique();

                subResult.setIdRubrique(Integer.parseInt(rs.getString("idRubrique")));
                subResult.setNomRubrique(rs.getString("nomRubrique"));
                subResult.setDateDebutRubrique(rs.getString("dateDebutRubrique"));
                subResult.setDateFinRubrique(rs.getString("dateFinRubrique"));
                subResult.setAvisRubrique(rs.getString("avisRubrique"));
                subResult.setTauxPromotionRubrique(Integer.parseInt(rs.getString("tauxPromotionRubrique")));

                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }

    public ArrayList<Tva> sqlGetTva() {
        ArrayList<Tva> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM tva;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Tva subResult = new Tva();

                subResult.setIdTva(Integer.parseInt(rs.getString("idTva")));
                subResult.setTauxTva(Float.parseFloat(rs.getString("tauxTva")));
                subResult.setTypeTva(Integer.parseInt(rs.getString("typeTva")));
                subResult.setDateTva(rs.getString("dateTva"));

                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }

    public ArrayList<MotCle> sqlGetMotCle() {
        ArrayList<MotCle> totalResult = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM motCle;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                MotCle subResult = new MotCle();

                subResult.setIdMotCle(Integer.parseInt(rs.getString("idMotCle")));
                subResult.setNomMotCle(rs.getString("nomMotCle"));

                totalResult.add(subResult);
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return totalResult;
    }

    public Livre sqlGetLivre(String isbnToGet) {
        Livre subResult = new Livre();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM livre WHERE isbnLivre=" + "'" + isbnToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                subResult.setIsbnLivre(rs.getString("isbnLivre"));
                subResult.setNumeroSiretEditeur(rs.getString("numeroSiretEditeur"));
                subResult.setIdFormat(Integer.parseInt(rs.getString("idFormat")));
                subResult.setTitreLivre(rs.getString("titreLivre"));
                subResult.setSousTitreLivre(rs.getString("sousTitreLivre"));
                subResult.setAdresseImageLivre(rs.getString("adresseImageLivre"));
                subResult.setResumeLivre(rs.getString("resumeLivre"));
                subResult.setStockLivre(Integer.parseInt(rs.getString("stockLivre")));
                subResult.setCollectionLivre(rs.getString("collectionLivre"));
                subResult.setPrixHTLivre(Float.parseFloat(rs.getString("prixHTLivre")));
                subResult.setAvisLivre(rs.getString("avisLivre"));
                subResult.setIdTva(Integer.parseInt(rs.getString("idTva")));
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return subResult;
    }

    public int sqlGetComboBoxAuteur(String isbnToGet) {
        int result = 0;
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM ecriture WHERE isbnLivre=" + "'" + isbnToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                result = Integer.parseInt(rs.getString("idAuteur"));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return result;
    }

    public int sqlGetComboBoxEditeur(String siretToGet) {
        int result = 0;
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM editeur WHERE numeroSiretEditeur=" + "'" + siretToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                result = Integer.parseInt(rs.getString("idAuteur"));
            }

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return result;
    }

    public int sqlGetComboBoxRubrique(String isbnToGet) {
        int result = 0;
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM participation WHERE isbnLivre=" + "'" + isbnToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                result = Integer.parseInt(rs.getString("idRubrique"));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return result;
    }

    public ArrayList<MotCle> sqlGetComboBoxMotCle(String isbnToGet) {
        ArrayList<MotCle> tempMotCleList = new ArrayList();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM referencement WHERE isbnLivre=" + "'" + isbnToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                MotCle tempMotCle = new MotCle();
                tempMotCle.setIdMotCle(Integer.parseInt(rs.getString("idMotCle")));
                tempMotCle.setNomMotCle(rs.getString("isbnLivre"));
                tempMotCleList.add(tempMotCle);
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return tempMotCleList;
    }

    public void sqlAddLivre(Livre tempLivre, int auteurChoix, int rubriqueChoix, ArrayList<Integer> selectedMotCle) {
        System.out.println("go");
        System.out.println(tempLivre.getIsbnLivre());
        System.out.println("end");
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "INSERT INTO livre (isbnLivre,numeroSiretEditeur,idFormat,titreLivre,sousTitreLivre,adresseImageLivre,resumeLivre,stockLivre,collectionLivre,prixHTLivre,avisLivre,idTva) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + tempLivre.getNumeroSiretEditeur() + "',"
                    + "'" + tempLivre.getIdFormat() + "',"
                    + "'" + tempLivre.getTitreLivre() + "',"
                    + "'" + tempLivre.getSousTitreLivre() + "',"
                    + "'" + tempLivre.getAdresseImageLivre() + "',"
                    + "'" + tempLivre.getResumeLivre() + "',"
                    + "'" + tempLivre.getStockLivre() + "',"
                    + "'" + tempLivre.getCollectionLivre() + "',"
                    + "'" + tempLivre.getPrixHTLivre() + "',"
                    + "'" + tempLivre.getAvisLivre() + "',"
                    + tempLivre.getIdTva()
                    + ");";

            int result = stmt.executeUpdate(query);
            stmt.close();

            Statement stmt2 = connexion.createStatement();
            String query2 = "";

            query2 = "INSERT INTO ecriture (isbnLivre,idAuteur) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + auteurChoix + "'"
                    + ");";

            int result2 = stmt2.executeUpdate(query2);
            stmt2.close();

            Statement stmt3 = connexion.createStatement();
            String query3 = "";

            query3 = "INSERT INTO participation (isbnLivre,idRubrique) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + rubriqueChoix + "'"
                    + ");";

            int result3 = stmt3.executeUpdate(query3);
            stmt3.close();

            Statement stmt4 = connexion.createStatement();
            String query4 = "";

            for (int a : selectedMotCle) {
                query4 = "INSERT INTO referencement (idMotCle,isbnLivre) VALUES ("
                        + "'" + a + "',"
                        + "'" + tempLivre.getIsbnLivre() + "'"
                        + ");";

                int result4 = stmt4.executeUpdate(query4);
            }

            stmt4.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateLivre(Livre tempLivre, int auteurChoix, int rubriqueChoix, ArrayList<Integer> selectedMotCle) {
        for (int z : selectedMotCle) {
            System.out.println(z);
        }
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            String isbnStr = tempLivre.getIsbnLivre();
            query = "UPDATE livre SET "
                    + "numeroSiretEditeur='" + tempLivre.getNumeroSiretEditeur() + "',"
                    + "idFormat='" + tempLivre.getIdFormat() + "',"
                    + "titreLivre='" + tempLivre.getTitreLivre() + "',"
                    + "sousTitreLivre='" + tempLivre.getSousTitreLivre() + "',"
                    + "adresseImageLivre='" + tempLivre.getAdresseImageLivre() + "',"
                    + "resumeLivre='" + tempLivre.getResumeLivre() + "',"
                    + "stockLivre='" + tempLivre.getStockLivre() + "',"
                    + "collectionLivre='" + tempLivre.getCollectionLivre() + "',"
                    + "prixHTLivre='" + tempLivre.getPrixHTLivre() + "',"
                    + "avisLivre='" + tempLivre.getAvisLivre() + "',"
                    + "idTva=" + tempLivre.getIdTva()
                    + " WHERE isbnLivre='" + isbnStr + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

            Statement stmt2del = connexion.createStatement();
            String query2del = "";

            query2del = "DELETE FROM ecriture WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result2del = stmt2del.executeUpdate(query2del);
            stmt2del.close();

            Statement stmt2 = connexion.createStatement();
            String query2 = "";

            query2 = "INSERT INTO ecriture (isbnLivre,idAuteur) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + auteurChoix + "'"
                    + ");";

            int result2 = stmt2.executeUpdate(query2);
            stmt2.close();

            Statement stmt3del = connexion.createStatement();
            String query3del = "";

            query3del = "DELETE FROM participation WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result3del = stmt3del.executeUpdate(query3del);
            stmt3del.close();

            Statement stmt3 = connexion.createStatement();
            String query3 = "";

            query3 = "INSERT INTO participation (isbnLivre,idRubrique) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + rubriqueChoix + "'"
                    + ");";

            int result3 = stmt3.executeUpdate(query3);
            stmt3.close();

            Statement stmt4del = connexion.createStatement();
            String query4del = "";

            for (int a : selectedMotCle) {
                query4del = "DELETE FROM referencement WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

                int result4del = stmt4del.executeUpdate(query4del);
            }

            stmt4del.close();

            Statement stmt4 = connexion.createStatement();
            String query4 = "";

            for (int a : selectedMotCle) {
                query4 = "INSERT INTO referencement (idMotCle,isbnLivre) VALUES ("
                        + "'" + a + "',"
                        + "'" + tempLivre.getIsbnLivre() + "'"
                        + ");";

                int result4 = stmt4.executeUpdate(query4);
            }

            stmt4.close();

        } catch (SQLException ex) {
            if(ex.getErrorCode()==547){
                JOptionPane.showMessageDialog(null, "Vous ne pouvez pas modifier l'ISBN","Inane error",JOptionPane.ERROR_MESSAGE);
            }
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public Vector sqlInitLabelTva(int typeTva) {
        Vector v = new Vector();

        try {
            Statement stmt = connexion.createStatement();

            String query = "SELECT top 1 * FROM tva WHERE dateTva <= GETDATE() AND typeTva = " + typeTva + " ORDER BY dateTva desc";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                v.add(new Tva(rs.getFloat("tauxTva"), rs.getInt("typeTva"), rs.getInt("idTva"), rs.getString("dateTva")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    public void sqlClose() {
        try {
            connexion.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:close:" + ex.getMessage());
        }

    }    
}
