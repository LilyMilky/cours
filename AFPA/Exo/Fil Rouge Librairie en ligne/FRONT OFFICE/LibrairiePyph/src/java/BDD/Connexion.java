package BDD;

import Classes.Compte;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Connexion {
    
    Connection connexion = null;
    
    //Permet la connection a la base SQLSERVER
    public void connexion()
    {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Oops: ClassNotFound: " +ex.getMessage());
        }
        
        try {
            String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=Librairie;user=sa;password=sa";
            connexion = DriverManager.getConnection(connectionUrl);
        } catch (SQLException ex) {
            System.err.println("Oops: SQLConnexion: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
    }
    
    //Fermeture de la connection avec SQLSERVER
    public void close()
    {
        try {
            connexion.close();
        } catch (SQLException ex) {
            System.err.println("Echec fermeture connexion");
        }
    }
    
    //Recuperation du compte
    public Compte identification(String identifiant, String mdp)
    {
        Compte compte = new Compte();
        
        
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "select * from dbo.compte where identifiantCompte = '" + identifiant + "' AND mdpCompte = '" + mdp + "'";
            ResultSet rs = stmt.executeQuery(query);
            
            rs.next();
            
            if(rs.getString("identifiantCompte").equals(identifiant))
            {
                compte.setActifCompte(rs.getInt("actifCompte"));
                compte.setDateCreationCompte(rs.getString("dateCreationCompte"));
                compte.setIdAcces(rs.getInt("idAcces"));
                compte.setIdClient(rs.getInt("idClient"));
                compte.setIdEmploye(rs.getInt("idEmploye"));
                compte.setIdentifiantCompte(identifiant);
                compte.setMdpCompte(mdp);
            }

            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
            compte.setActifCompte(10);
        }
        
        return compte;
    }

    public Connection getConnexion() {
        return connexion;
    }
    
}
