package BDD;

import Classes.Transporteur;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class TransporteurBDD {
    
    //Ajout d'un transporteur en base
    public int addTransporteur(String nomTransporteur, String telTransporteur, String avisTransporteur, Connexion con)
    {
        int result = 0;
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "insert into transporteur (nomTransporteur, telephoneTransporteur, avisTransporteur, hideTransporteur) values ('"
                    + nomTransporteur +"','"+ telTransporteur + "','" + avisTransporteur + "'," + 1 + ")";

            result = stmt.executeUpdate(query);
            
            stmt.close();
            
            
        } catch (SQLException ex) {
            //System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
            return result;
        }
         
         return result;
    }
    
    //Recuperation des transporteurs pour la comboBox
    public Vector getAllTransporteur(Connexion con)
    {
        Vector v = new Vector();
        
        String query = "SELECT * FROM Transporteur Order by nomTransporteur;";
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                v.add(new Transporteur(rs.getInt("idTransporteur"), rs.getString("nomTransporteur"), rs.getString("telephoneTransporteur"), rs.getString("avisTransporteur"), rs.getInt("hideTransporteur")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        return v;
    }
    
    //Mise a jour d'un transporteur via l'id
    public int updateTransporteur(Transporteur trans, Connexion con)
    {
        int result = 0;
        
        String query = "Update Transporteur set nomTransporteur = '" + trans.getNomTransporteur() 
                + "', telephoneTransporteur = '" + trans.getTelTransporteur()
                + "', avisTransporteur = '" + trans.getAvisTransporteur()
                + "', hideTransporteur = " + trans.getHideTransporteur()
                + "where idTransporteur = " + trans.getIdTransporteur();
        
        try
        {
            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(query);
            
            stmt.close();
        }
        catch(SQLException ex)
        {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        return result;
    }
    
    public int getIdTransporteur(String nomTransporteur ,Connexion con)
    {
        int id = 0;

        String query = "Select idTransporteur from transporteur where nomTransporteur = '" +nomTransporteur+ "'";
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs = stmt.executeQuery(query);

            rs.next();
            id = rs.getInt("idTransporteur");
            

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        
        return id;
    }
    
}
