package BDD;

import Classes.MotCle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.ListModel;

/**
 *
 * @author Philippe Crombez
 */
public class MotCleBDD {
    
    // Methode pour remplir la liste des mots clés.
    public Vector initVectorMotCle(Connexion con){
        
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM motCle ORDER BY nomMotCle";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idMotCle"), rs.getString("nomMotCle")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Methode pour remplir la liste des mots clés en fonction des lettres saisies
    public Vector initVectorMotCle(Connexion con, String saisieRapide){
        
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM motCle WHERE nomMotCle LIKE '" + saisieRapide + "%' ORDER BY nomMotCle";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idMotCle"), rs.getString("nomMotCle")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Methode pour vérifier si un mot clé n'a pas déjà été sélectionné
    public boolean isExist(String motCleSelectionne, Vector v) {
        Iterator it = v.iterator();
        
        while(it.hasNext()){
            String test = it.next().toString();
            if(motCleSelectionne.equalsIgnoreCase(test)){
                return true;
            } 
        }
        return false;
    }
    
    // Methode pour ajouter un mot clé à la base
    public void ajouterMotCle(String nouveauMotCle, Connexion con){
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO motCle(nomMotCle) VALUES('" + nouveauMotCle + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
    //Méthode pour savoir si un isbn existe ou pas
    public boolean isExist(String isbnLivre, Connexion con) {
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM livre ORDER BY isbnLivre";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if(isbnLivre.equalsIgnoreCase(rs.getString("isbnLivre"))){
                    rs.close();
                    stmt.close();
                    return true;
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        return false;
    }

    //Méthode pour rechercher le titre d'un livre en fonction de l'isbn
    public String RechercherTitreLivre(String isbnLivre, Connexion con) {
        String result = null;
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM livre ORDER BY isbnLivre";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                if(isbnLivre.equalsIgnoreCase(rs.getString("isbnLivre"))){
                    result = rs.getString("titreLivre");
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        return result;
    }
    
    // Methode pour initialiser les mots clés(liste du bas) liés à un livre
    public Vector initVectorMotCle(String isbnLivre, Connexion con){
        
        Vector v = new Vector();
        List<Integer> idMotCle = new ArrayList();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            Statement stmt2 = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM referencement WHERE isbnLivre='" + isbnLivre + "' ORDER BY idMotCle";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                idMotCle.add(rs.getInt("idMotCle"));
            }
            
            query = "SELECT * FROM motCle ORDER BY nomMotCle";
            ResultSet rs2 = stmt2.executeQuery(query);
            while(rs2.next()){
                for(Integer compteur : idMotCle){
                    if(rs2.getLong("idMotCle") == compteur){
                        v.add(new MotCle(rs2.getInt("idMotCle"), rs2.getString("nomMotCle")));
                    }
                }
            }
            
            rs2.close();
            rs.close();
            stmt2.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    // Méthode pour enregistrer les modifications apportées.
    public void enregistrerMotCle(Connexion con, String isbnLivre, ListModel listNouveauMotCle) {
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "DELETE FROM referencement WHERE isbnLivre='" + isbnLivre + "'";
            stmt.executeUpdate(query);
            int taille = listNouveauMotCle.getSize();
            for(int i = 0; i < taille; i++){
                MotCle tmp = (MotCle)listNouveauMotCle.getElementAt(i);
                query = "INSERT INTO referencement(isbnLivre, idMotCle) VALUES(?, ?)";
                PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
                stmt2.setString(1, isbnLivre);
                stmt2.setInt(2, tmp.getIdMotCle());
                stmt2.executeUpdate();
                stmt2.close();
            }
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }

    
}
