package BDD;

import java.util.Date;
import java.sql.SQLException;
import java.sql.Statement;
import Classes.*;
import Classes.Rubrique;
import java.sql.ResultSet;
import java.util.Vector;

public class RubriqueBDD {
    
    //Permet l'ajout d'une rubrique en base
    public int addRubrique(String nomRubrique, Date dateDebutRubrique, Date dateFinRubrique, String avisRubrique, int promotionRubrique, Connexion con)
    {
        int result = 0;
        
        //Convertion de la date dans un format compréhensible pour sqlServer
        String debut = ConvertDate.convertDateSQL(dateDebutRubrique);
        String fin = ConvertDate.convertDateSQL(dateFinRubrique);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "insert into rubrique (nomRubrique, dateDebutRubrique, dateFinRubrique, avisRubrique, tauxPromotionRubrique) values ('"
                    + nomRubrique +"','" + debut + "','" + fin + "','" + avisRubrique + "',"+ promotionRubrique + ")";

            //System.out.println(query);
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
            
            
        } catch (SQLException ex) {
            //System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
            return result;
        }
         
         return result;
    }
    
    //Permet la recherche d'une rubrique par le nom via un like(Mise a jour a chaque pression sur le clavier)
    public Vector rechercheBddLike(String nom, Connexion con)
    {
        Vector v = new Vector();
        
        String query = "SELECT * FROM rubrique where nomRubrique like '" + nom + "%';";
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                v.add(new Rubrique(rs.getInt("idRubrique"), rs.getString("nomRubrique"), ConvertDate.convertDate(rs.getString("dateDebutRubrique")), ConvertDate.convertDate(rs.getString("dateFinRubrique")), rs.getString("avisRubrique"), rs.getInt("tauxPromotionRubrique")));
            }

            //System.out.println(query);
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;      
    }
    
    //Permet la recherche des rubriques qui commence apres une date
    public Vector rechercheBddDate(String date, Connexion con)
    {
        Vector v = new Vector();
        
        String query = "SELECT * FROM rubrique where dateFinRubrique >= '" + date + "';";
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                v.add(new Rubrique(rs.getInt("idRubrique"), rs.getString("nomRubrique"), ConvertDate.convertDate(rs.getString("dateDebutRubrique")), ConvertDate.convertDate(rs.getString("dateFinRubrique")), rs.getString("avisRubrique"), rs.getInt("tauxPromotionRubrique")));
            }

            //System.out.println(query);
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;      
    }
    
    //Permet la mise a jour du nom d'une rubrique
    public int updateNomRubrique(Rubrique rub, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE rubrique set nomRubrique = '" + rub.getNomRubrique() + "' where idRubrique = " + rub.getIdRubrique();
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    //Permet la mise a jour de la date d'une rubrique
    public int updateDateRubrique(Rubrique rub, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE rubrique set dateDebutRubrique = '" + rub.getDateDebutRubrique()+ "' where idRubrique = " + rub.getIdRubrique();
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    //Permet la mise a jour de la date de fin d'une rubrique
    public int updateDateFinRubrique(Rubrique rub, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE rubrique set dateFinRubrique = '" + rub.getDateFinRubrique() + "' where idRubrique = " + rub.getIdRubrique();
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    //Permet la mise a jour de l'avis d'une rubrique
    public int updateAvisRubrique(Rubrique rub, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE rubrique set avisRubrique = '" + rub.getAvisRubrique() + "' where idRubrique = " + rub.getIdRubrique();
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    //Permet la mise a jour du taux de promotion appliqué a la rubrique
    public int updateTauxPromotionRubrique(Rubrique rub, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE rubrique set tauxPromotionRubrique = " + rub.getTauxPromotionRubrique() + " where idRubrique = " + rub.getIdRubrique();
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
}
