package Classes;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexEtSyntaxe {
    public boolean verifMail(String emailClient){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("[a-zA-Z0-9][-_.a-zA-Z0-9]*@[a-zA-Z0-9][-.a-zA-Z0-9]*.[a-zA-Z]{2,6}");
        correspond = comparatif.matcher(emailClient);
        return correspond.find();
    }
    
    public boolean verifTelephoneFixe(String telephone){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("0[^a-zA-Z678][0-9]{8}");
        correspond = comparatif.matcher(telephone);
        return correspond.find();
    }
    
    public boolean verifTelephonePortable(String telephone){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("0[^a-zA-Z1234589][0-9]{8}");
        correspond = comparatif.matcher(telephone);
        return correspond.find();
    }
    
    public boolean verifSiret(String siret){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("[0-9]{14}");
        correspond = comparatif.matcher(siret);
        return correspond.find();
    }
    
    public boolean verifIsbnLivreType17(String isbnLivre){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("97[8-9]-2-[0-9]{3}-[0-9]{5}-[0-9]");
        correspond = comparatif.matcher(isbnLivre);
        return correspond.find();
    }
    
    public boolean verifIsbnLivreType13(String isbnLivre){
        
        Pattern comparatif;
        Matcher correspond;
        
        comparatif = Pattern.compile("97[8-9]2[0-9]{9}");
        correspond = comparatif.matcher(isbnLivre);
        return correspond.find();
    }    
    
    
    
    public String ISBN13Vers17(String isbnLivre){
        String result = "";
        result = isbnLivre.substring(0, 3)+"-"+isbnLivre.substring(3, 4)+"-"+isbnLivre.substring(4,7)+"-"+isbnLivre.substring(7,12)+"-"+isbnLivre.substring(12,13);
        return result;
    }

    public void verifIsbnLivreLongueur(String isbnLivre){
        int chiffreCle=0;
        String ChiffreCleString;
        if(isbnLivre.length()==13){
            isbnLivre = "978-"+isbnLivre.substring(0, 12);
            ChiffreCleString = isbnLivre.replaceAll("-", "");
            for(int i=0; i<12; i=i+2){
                chiffreCle += ((int)Integer.valueOf(ChiffreCleString.substring(i, i+1)))+(3*(int)Integer.valueOf(ChiffreCleString.substring(i+1, i+2)));
            }
            chiffreCle = (10-(chiffreCle%10))%10;
            isbnLivre = ChiffreCleString + String.valueOf(chiffreCle);
            System.out.println(isbnLivre);
        }
    }
    
    public String protectString (String paramString){
        String result="";
        ArrayList<Character> updatedString = new ArrayList(); 

        for(int i=0;i<paramString.length();i++){
            if(paramString.charAt(i)==39){
                updatedString.add(paramString.charAt(i));
                updatedString.add(paramString.charAt(i));
            }
            else if(paramString.charAt(i)!=39){
                updatedString.add(paramString.charAt(i));
            }
        }
        String finalString = "";
        for(int i = 0; i<updatedString.size();i++){
            finalString += updatedString.get(i);
        }
        
        result = finalString;
        return result;
    }  
}
