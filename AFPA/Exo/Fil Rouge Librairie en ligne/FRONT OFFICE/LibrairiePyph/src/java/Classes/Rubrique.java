package Classes;

import Classes.ConvertDate;

/**
 *
 * @author Milky
 */
public class Rubrique {
    
    private int idRubrique;
    private String nomRubrique;
    private String dateDebutRubrique;
    private String dateFinRubrique;
    private String avisRubrique;
    private int tauxPromotionRubrique;

    public Rubrique() {
    }

    public Rubrique(int idRubrique, String nomRubrique, String dateDebutRubrique, String dateFinRubrique, String avisRubrique, int tauxPromotionRubrique) {
        this.idRubrique = idRubrique;
        this.nomRubrique = nomRubrique;
        this.dateDebutRubrique = dateDebutRubrique;
        this.dateFinRubrique = dateFinRubrique;
        this.avisRubrique = avisRubrique;
        this.tauxPromotionRubrique = tauxPromotionRubrique;
    }

    public int getIdRubrique() {
        return idRubrique;
    }

    public void setIdRubrique(int idRubrique) {
        this.idRubrique = idRubrique;
    }

    public String getNomRubrique() {
        return nomRubrique;
    }

    public void setNomRubrique(String nomRubrique) {
        this.nomRubrique = nomRubrique;
    }

    public String getDateDebutRubrique() {
        
         return dateDebutRubrique;
    }

    public void setDateDebutRubrique(String dateDebutRubrique) {
        this.dateDebutRubrique = dateDebutRubrique;
    }

    public String getDateFinRubrique() {

        return dateFinRubrique;
    }

    public void setDateFinRubrique(String dateFinRubrique) {
        this.dateFinRubrique = dateFinRubrique;
    }

    public String getAvisRubrique() {
        return avisRubrique;
    }

    public void setAvisRubrique(String avisRubrique) {
        this.avisRubrique = avisRubrique;
    }

    public int getTauxPromotionRubrique() {
        return tauxPromotionRubrique;
    }

    public void setTauxPromotionRubrique(int tauxPromotionRubrique) {
        this.tauxPromotionRubrique = tauxPromotionRubrique;
    }

    @Override
    public String toString() {
        if(ConvertDate.convertDate(dateDebutRubrique).equals("01-01-1900")){
            return nomRubrique;
        }else{
            return nomRubrique + " du " + ConvertDate.convertDate(dateDebutRubrique) + " au " + ConvertDate.convertDate(dateFinRubrique);
        }
    }
    
    
    
    
}
