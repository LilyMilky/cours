package Classes;

import java.text.DecimalFormat;

public class Tva {
    private float tauxTva;
    private int typeTva;
    private int idTva;
    private String dateTva;
    DecimalFormat df = new DecimalFormat("######.00");

    public Tva() {
    }

    public Tva(float tauxTva, int typeTva, int idTva, String dateTva) {
        this.tauxTva = tauxTva;
        this.typeTva = typeTva;
        this.idTva = idTva;
        this.dateTva = dateTva;
    }

    

    public void setTauxTva(float tauxTva) {
        this.tauxTva = tauxTva;
    }

    public void setTypeTva(int typeTva) {
        this.typeTva = typeTva;
    }

    public void setIdTva(int idTva) {
        this.idTva = idTva;
    }
    
    public void setDateTva(String dateTva) {
        this.dateTva = dateTva;
    }

    public float getTauxTva() {
        return tauxTva;
    }

    public int getTypeTva() {
        return typeTva;
    }

    public int getIdTva() {
        return idTva;
    }
    
    public String getDateTva() {
        return dateTva;
    }

    @Override
    public String toString() {
        return df.format(tauxTva) + "%";
    }
}
