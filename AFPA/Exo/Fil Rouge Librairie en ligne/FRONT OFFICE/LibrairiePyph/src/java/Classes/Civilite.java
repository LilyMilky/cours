package Classes;

/**
 *
 * @author Milky
 */
public class Civilite {
    
    private int idCivilite;
    private int typeCivilite;
    private String nomCivilite;

    public Civilite() {
    }

    public Civilite(int idCivilite, int typeCivilite, String nomCivilite) {
        this.idCivilite = idCivilite;
        this.typeCivilite = typeCivilite;
        this.nomCivilite = nomCivilite;
    }

    public int getIdCivilite() {
        return idCivilite;
    }

    public void setIdCivilite(int idCivilite) {
        this.idCivilite = idCivilite;
    }

    public int getTypeCivilite() {
        return typeCivilite;
    }

    public void setTypeCivilite(int typeCivilite) {
        this.typeCivilite = typeCivilite;
    }

    public String getNomCivilite() {
        return nomCivilite;
    }

    public void setNomCivilite(String nomCivilite) {
        this.nomCivilite = nomCivilite;
    }
    
    
    
}
