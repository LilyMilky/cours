package Classes;

import java.util.Vector;

/**
 *
 * @author Milky
 */
public class Coordonnees {

    private int idCoordonnees;
    private int idCivilite;
    private String nomCoordonnees;
    private String prenomCoordonnees;
    private String voieCoordonnees;
    private String codePostalCoordonnees;
    private String villeCoordonnees;
    private String complementCoordonnees;
    private String telephoneCoordonnees;          //Au moins un des 2 tels obligatoire.
    private String telephonePortableCoordonnees;
    private String emailCoordonnees;
    int typeCoordonnees;

    public Coordonnees() {
    }

    public Coordonnees(int idCoordonnees, int idCivilite, String nomCoordonnees, String prenomCoordonnees, String voieCoordonnees, String codePostalCoordonnees, String villeCoordonnees, String complementCoordonnees, String telCoordonnees, String telMobileCoordonnees, String emailCoordonnees, int typeCoordonnees) {
        this.idCoordonnees = idCoordonnees;
        this.idCivilite = idCivilite;
        this.nomCoordonnees = nomCoordonnees;
        this.prenomCoordonnees = prenomCoordonnees;
        this.voieCoordonnees = voieCoordonnees;
        this.codePostalCoordonnees = codePostalCoordonnees;
        this.villeCoordonnees = villeCoordonnees;
        this.complementCoordonnees = complementCoordonnees;
        this.telephoneCoordonnees = telCoordonnees;
        this.telephonePortableCoordonnees = telMobileCoordonnees;
        this.emailCoordonnees = emailCoordonnees;
        this.typeCoordonnees = typeCoordonnees;
    }

    public int getIdCoordonnees() {
        return idCoordonnees;
    }

    public void setIdCoordonnees(int idCoordonnees) {
        this.idCoordonnees = idCoordonnees;
    }

    public int getIdCivilite() {
        return idCivilite;
    }

    public void setIdCivilite(int idCivilite) {
        this.idCivilite = idCivilite;
    }

    public String getNomCoordonnees() {
        return nomCoordonnees;
    }

    public void setNomCoordonnees(String nomCoordonnees) {
        this.nomCoordonnees = nomCoordonnees;
    }

    public String getPrenomCoordonnees() {
        return prenomCoordonnees;
    }

    public void setPrenomCoordonnees(String prenomCoordonnees) {
        this.prenomCoordonnees = prenomCoordonnees;
    }

    public String getVoieCoordonnees() {
        return voieCoordonnees;
    }

    public void setVoieCoordonnees(String voieCoordonnees) {
        this.voieCoordonnees = voieCoordonnees;
    }

    public String getCodePostalCoordonnees() {
        return codePostalCoordonnees;
    }

    public void setCodePostalCoordonnees(String codePostalCoordonnees) {
        this.codePostalCoordonnees = codePostalCoordonnees;
    }

    public String getVilleCoordonnees() {
        return villeCoordonnees;
    }

    public void setVilleCoordonnees(String villeCoordonnees) {
        this.villeCoordonnees = villeCoordonnees;
    }

    public String getComplementCoordonnees() {
        return complementCoordonnees;
    }

    public void setComplementCoordonnees(String complementCoordonnees) {
        this.complementCoordonnees = complementCoordonnees;
    }

    public String getTelephoneCoordonnees() {
        return telephoneCoordonnees;
    }

    public void setTelephoneCoordonnees(String telephoneCoordonnees) {
        this.telephoneCoordonnees = telephoneCoordonnees;
    }

    public String getTelephonePortableCoordonnees() {
        return telephonePortableCoordonnees;
    }

    public void setTelephonePortableCoordonnees(String telephonePortableCoordonnees) {
        this.telephonePortableCoordonnees = telephonePortableCoordonnees;
    }

    public String getEmailCoordonnees() {
        return emailCoordonnees;
    }

    public void setEmailCoordonnees(String emailCoordonnees) {
        this.emailCoordonnees = emailCoordonnees;
    }

    public int getTypeCoordonnees() {
        return typeCoordonnees;
    }

    public void setTypeCoordonnees(int typeCoordonnees) {
        this.typeCoordonnees = typeCoordonnees;
    }

    @Override
    public String toString() {
        return nomCoordonnees;
    }

    public Vector getVector() {
        Vector vv = new Vector();
        if (this.idCivilite == 1) {
            vv.add("M.");
        } else if (this.idCivilite == 2) {
            vv.add("Mme");
        } else {
            vv.add("Societe");
        }
        vv.add(this);
        vv.add(this.prenomCoordonnees);
        vv.add(this.voieCoordonnees);
        vv.add(this.codePostalCoordonnees);
        vv.add(this.villeCoordonnees);
        vv.add(this.complementCoordonnees);
        vv.add(this.telephoneCoordonnees);
        vv.add(this.telephonePortableCoordonnees);
        vv.add(this.emailCoordonnees);
        vv.add(this.typeCoordonnees);

        return vv;
    }

}
