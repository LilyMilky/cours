package Classes;

/**
 *
 * @author Milky
 */
public class Editeur {
    
    private String numeroSiretEditeur;
    private int idCoordonnees;
    private String nomEditeur;
    private String avisEditeur;

    public Editeur() {
    }

    public Editeur(String numeroSiretEditeur, int idCoordonnees, String nomEditeur, String avisEditeur) {
        this.numeroSiretEditeur = numeroSiretEditeur;
        this.idCoordonnees = idCoordonnees;
        this.nomEditeur = nomEditeur;
        this.avisEditeur = avisEditeur;
    }

    public String getNumeroSiretEditeur() {
        return numeroSiretEditeur;
    }

    public void setNumeroSiretEditeur(String numeroSiretEditeur) {
        this.numeroSiretEditeur = numeroSiretEditeur;
    }

    public int getIdCoordonnees() {
        return idCoordonnees;
    }

    public void setIdCoordonnees(int idCoordonnees) {
        this.idCoordonnees = idCoordonnees;
    }

    public String getNomEditeur() {
        return nomEditeur;
    }

    public void setNomEditeur(String nomEditeur) {
        this.nomEditeur = nomEditeur.toUpperCase();
    }

    public String getAvisEditeur() {
        return avisEditeur;
    }

    public void setAvisEditeur(String avisEditeur) {
        this.avisEditeur = avisEditeur;
    }
    
    @Override
    public String toString() {
        return nomEditeur + ", siret:" + numeroSiretEditeur ;
    }
    
}
