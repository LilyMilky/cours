package Classes;

/**
 *
 * @author Milky
 */
public class Etat {
    
    private int idEtat;
    private int typeEtat;
    private String nomEtat;

    public Etat() {
    }

    public Etat(int idEtat, int typeEtat, String nomEtat) {
        this.idEtat = idEtat;
        this.typeEtat = typeEtat;
        this.nomEtat = nomEtat;
    }

    public int getIdEtat() {
        return idEtat;
    }

    public void setIdEtat(int idEtat) {
        this.idEtat = idEtat;
    }

    public int getTypeEtat() {
        return typeEtat;
    }

    public void setTypeEtat(int typeEtat) {
        this.typeEtat = typeEtat;
    }

    public String getNomEtat() {
        return nomEtat;
    }

    public void setNomEtat(String nomEtat) {
        this.nomEtat = nomEtat;
    }

    @Override
    public String toString() {
        return typeEtat + ": " + nomEtat;
    }
    
    
    
}
