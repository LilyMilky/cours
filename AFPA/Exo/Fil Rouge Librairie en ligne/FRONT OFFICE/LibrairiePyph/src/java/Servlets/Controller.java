package Servlets;

import Beans.beanPanier;
import BDD.*;
import Classes.*;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Controller", urlPatterns = {"/Controller"})
public class Controller extends HttpServlet {

    private Cookie getCookie(Cookie[] cookies, String name) {
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals((name))) {
                    return c;
                }
            }
        }
        return null;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = "/WEB-INF/jspAccueil.jsp";
        HttpSession session = request.getSession();
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD commandebdd = new CommandeBDD();
        SitePaiementBDD sitebdd = new SitePaiementBDD();
        TransporteurBDD transbdd = new TransporteurBDD();

        beanPanier bPanier = (beanPanier) session.getAttribute("panier");

        if ("vuepanier".equals(request.getParameter("section"))) {

            url = "/WEB-INF/jspPanier.jsp";

            if (bPanier == null) {
                bPanier = new beanPanier();
                bPanier.add("978-2-266-26282-8");//TEST
                bPanier.add("978-2-731-63071-8");//TEST
                session.setAttribute("panier", bPanier);
            }
            request.setAttribute("estVide", bPanier.isEmpty());
            request.setAttribute("list", bPanier.getList());
            request.setAttribute("prixTotal", bPanier.getTotal());
        }
        if ("panier".equals(request.getParameter("section"))) {

            if (bPanier == null) {
                bPanier = new beanPanier();
                session.setAttribute("panier", bPanier);
            }

            if (request.getParameter("add") != null) {
                bPanier.add(request.getParameter("add"));
            }

            if (request.getParameter("dec") != null) {
                bPanier.dec(request.getParameter("dec"));
            }

            if (request.getParameter("del") != null) {
                bPanier.del(request.getParameter("del"));
            }

            if (request.getParameter("clear") != null) {
                bPanier.clear();
            }

            request.setAttribute("estVide", bPanier.isEmpty());
            request.setAttribute("prixTotal", bPanier.getTotal());
        }

        if ("choixAdresse".equals(request.getParameter("section"))) {

            url = "/WEB-INF/choixadresse.jsp";

        }

        if ("vuepaiement".equals(request.getParameter("section"))) {
            url = "/WEB-INF/jspPaiement.jsp";

            Vector sitePaiements = commandebdd.getSitePaiement(con);
            Vector transporteurs = commandebdd.getTransporteurs(con);

            request.setAttribute("listsite", sitePaiements);
            request.setAttribute("listtransporteur", transporteurs);
        }

        if ("paiement".equals(request.getParameter("section"))) {
            int result;
            if (request.getParameter("site") != null && request.getParameter("transporteur") != null) {
                //Commande validée, enregistrement en base!

                //Recuperation du dernier numero de commande
                String numeroCommande = commandebdd.getNumLastCommande(con);
                if (!numeroCommande.isEmpty()) {
                    int num = Integer.parseInt(numeroCommande) + 1;
                    numeroCommande = "" + num;

                    //Recuperation de l'id du site de paiement
                    int idSite = sitebdd.getIdSitePaiement(request.getParameter("site"), con);
                    if (idSite != 0) {
                        //Recuperation de l'id du transporteur
                        int idTransporteur = transbdd.getIdTransporteur(request.getParameter("transporteur"), con);
                        if (idTransporteur != 0) {

                            //Ajout de la commande
                            result = commandebdd.addCommande(numeroCommande, idSite,/*((Client)session.getAttribute("idClient")).getIdClient()*/ 1/*TEST ID CLIENT*/, /*((Coordonnees)session.getAttribute("idCoordonneesLivraison")).getIdCoordonnees()*/1/*Test idLivraison*/,
                                    idTransporteur , /*((Coordonnees)session.getAttribute("idCoordonneesFacturation")).getIdCoordonnees()*/6/* Test idFacturation */, Integer.toString(request.getRemotePort())/*ipCommande*/,
                                    numeroCommande/*referencePaiement*/, numeroCommande/*referenceColisCommande*/, con);

                            if (result == 0) {
                                url = "/WEB-INF/jspErreurBDD.jsp";
                            } else {
                                //Ajout du contenu
                                Collection<Livre> livres = bPanier.getList();
                                for (Livre l : livres) {
                                    result = commandebdd.addContenant(l.getIsbnLivre(), numeroCommande, l.getQty(), l.getPrixHTLivre(), l.getTVA(), 0/*Promotion*/, con);
                                }
                                if (result != 0) {
                                    url = "/WEB-INF/jspCommandeValide.jsp";
                                } else {
                                    url = "/WEB-INF/jspErreurBDD.jsp";
                                }
                            }
                        } else {
                            url = "/WEB-INF/jspErreurBDD.jsp";
                        }
                    } else {
                            url = "/WEB-INF/jspErreurBDD.jsp";
                        }
                } else {
                    url = "/WEB-INF/jspErreurBDD.jsp";
                }
            } else {
                //Renvoi sur l'accueil si pas de parametre (lien modifier/enregistre)
                url = "/WEB-INF/jspAccueil.jsp";
            }
        }

        request.getRequestDispatcher(url).include(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
