use Librairie
go


/*==============================================================*/
/* Nom de SGBD :  Microsoft SQL Server 2008                     */
/* Date de cr�ation :  16/02/2016 11:20:24                      */
/*==============================================================*/

/**************DROP KEY***********************/
/******************************************************************FOREIGN KEY*********************************************************************/

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidSitePaiementCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidSitePaiementCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidClientCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidClientCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidEtatCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidEtatCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidLivraisonCoordonneesCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidLivraisonCoordonneesCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidTransporteurCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidTransporteurCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidFacturationCoordonneesCommande')
   AND parent_object_id = OBJECT_ID(N'dbo.commande')
)
alter table commande drop constraint fkidFacturationCoordonneesCommande

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreContenant')
   AND parent_object_id = OBJECT_ID(N'dbo.contenant')
)
alter table contenant drop constraint fkisbnLivreContenant

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fknumeroCommandeContenant')
   AND parent_object_id = OBJECT_ID(N'dbo.contenant')
)
alter table contenant drop constraint fknumeroCommandeContenant


IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidClientLivraison')
   AND parent_object_id = OBJECT_ID(N'dbo.livraison')
)
alter table livraison drop constraint fkidClientLivraison

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidCoordonneesLivraison')
   AND parent_object_id = OBJECT_ID(N'dbo.livraison')
)
alter table livraison drop constraint fkidCoordonneesLivraison

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidClientReglement')
   AND parent_object_id = OBJECT_ID(N'dbo.reglement')
)
alter table reglement drop constraint fkidClientReglement

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidCoordonneesReglement')
   AND parent_object_id = OBJECT_ID(N'dbo.reglement')
)
alter table reglement drop constraint fkidCoordonneesReglement

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fknumeroCommandeCommentaire')
   AND parent_object_id = OBJECT_ID(N'dbo.commentaire')
)
alter table commentaire drop constraint fknumeroCommandeCommentaire

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidClientCommentaire')
   AND parent_object_id = OBJECT_ID(N'dbo.commentaire')
)
alter table commentaire drop constraint fkidClientCommentaire

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreCommentaire')
   AND parent_object_id = OBJECT_ID(N'dbo.commentaire')
)
alter table commentaire drop constraint fkisbnLivreCommentaire

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidClientCompte')
   AND parent_object_id = OBJECT_ID(N'dbo.compte')
)
alter table compte drop constraint fkidClientCompte

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidEmployeCompte')
   AND parent_object_id = OBJECT_ID(N'dbo.compte')
)
alter table compte drop constraint fkidEmployeCompte

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidAccesCompte')
   AND parent_object_id = OBJECT_ID(N'dbo.compte')
)
alter table compte drop constraint fkidAccesCompte

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidCiviliteCoordonnees')
   AND parent_object_id = OBJECT_ID(N'dbo.coordonnees')
)
alter table coordonnees drop constraint fkidCiviliteCoordonnees

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreEcriture')
   AND parent_object_id = OBJECT_ID(N'dbo.ecriture')
)
alter table ecriture drop constraint fkisbnLivreEcriture

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidAuteurEcriture')
   AND parent_object_id = OBJECT_ID(N'dbo.ecriture')
)
alter table ecriture drop constraint fkidAuteurEcriture

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidCoordonneesEditeur')
   AND parent_object_id = OBJECT_ID(N'dbo.editeur')
)
alter table editeur drop constraint fkidCoordonneesEditeur

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fknumeroSiretEditeurLivre')
   AND parent_object_id = OBJECT_ID(N'dbo.livre')
)
alter table livre drop constraint fknumeroSiretEditeurLivre

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidTvaLivre')
   AND parent_object_id = OBJECT_ID(N'dbo.livre')
)
alter table livre drop constraint fkidTvaLivre

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidFormatLivre')
   AND parent_object_id = OBJECT_ID(N'dbo.livre')
)
alter table livre drop constraint fkidFormatLivre

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreParticipation')
   AND parent_object_id = OBJECT_ID(N'dbo.participation')
)
alter table participation drop constraint fkisbnLivreParticipation

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidRubriqueParticipation')
   AND parent_object_id = OBJECT_ID(N'dbo.participation')
)
alter table participation drop constraint fkidRubriqueParticipation

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidMotCleReferencement')
   AND parent_object_id = OBJECT_ID(N'dbo.referencement')
)
alter table referencement drop constraint fkidMotCleReferencement

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreReferencement')
   AND parent_object_id = OBJECT_ID(N'dbo.referencement')
)
alter table referencement drop constraint fkisbnLivreReferencement

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidSousThemeSpecialisation')
   AND parent_object_id = OBJECT_ID(N'dbo.specialisation')
)
alter table specialisation drop constraint fkidSousThemeSpecialisation

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidThemeSpecialisation')
   AND parent_object_id = OBJECT_ID(N'dbo.specialisation')
)
alter table specialisation drop constraint fkidThemeSpecialisation

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkidSousThemeThematisation')
   AND parent_object_id = OBJECT_ID(N'dbo.thematisation')
)
alter table thematisation drop constraint fkidSousThemeThematisation

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.fkisbnLivreThematisation')
   AND parent_object_id = OBJECT_ID(N'dbo.thematisation')
)
alter table thematisation drop constraint fkisbnLivreThematisation


/***********************DROP TABLE***************/
if exists (select 1
            from  sysobjects
           where  id = object_id('ACCES')
            and   type = 'U')
   drop table ACCES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TVA')
            and   type = 'U')
   drop table tva
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AUTEUR')
            and   type = 'U')
   drop table AUTEUR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CIVILITE')
            and   type = 'U')
   drop table CIVILITE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CLIENT')
            and   name  = 'CONNECTER_FK'
            and   indid > 0
            and   indid < 255)
   drop index CLIENT.CONNECTER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLIENT')
            and   type = 'U')
   drop table CLIENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'FACTURER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.FACTURER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'ENVOYER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.ENVOYER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'QUALIFIER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.QUALIFIER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'TRANSPORTER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.TRANSPORTER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'CHOISIR_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.CHOISIR_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMMANDE')
            and   name  = 'PASSER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMMANDE.PASSER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMMANDE')
            and   type = 'U')
   drop table COMMANDE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('commentaire')
            and   name  = 'COMMENTER3_FK'
            and   indid > 0
            and   indid < 255)
   drop index commentaire.COMMENTER3_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('commentaire')
            and   name  = 'COMMENTER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index commentaire.COMMENTER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('commentaire')
            and   name  = 'COMMENTER_FK'
            and   indid > 0
            and   indid < 255)
   drop index commentaire.COMMENTER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('commentaire')
            and   type = 'U')
   drop table commentaire
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPTE')
            and   name  = 'IDENTIFIER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPTE.IDENTIFIER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPTE')
            and   name  = 'DISPOSER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPTE.DISPOSER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPTE')
            and   name  = 'CONNECTER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPTE.CONNECTER2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMPTE')
            and   type = 'U')
   drop table COMPTE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('contenant')
            and   name  = 'CONTENIR2_FK'
            and   indid > 0
            and   indid < 255)
   drop index contenant.CONTENIR2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('contenant')
            and   name  = 'CONTENIR_FK'
            and   indid > 0
            and   indid < 255)
   drop index contenant.CONTENIR_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('contenant')
            and   type = 'U')
   drop table contenant
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COORDONNEES')
            and   name  = 'FORMALISER_FK'
            and   indid > 0
            and   indid < 255)
   drop index COORDONNEES.FORMALISER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COORDONNEES')
            and   type = 'U')
   drop table COORDONNEES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ecriture')
            and   name  = 'ECRIRE2_FK'
            and   indid > 0
            and   indid < 255)
   drop index ecriture.ECRIRE2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ecriture')
            and   name  = 'ECRIRE_FK'
            and   indid > 0
            and   indid < 255)
   drop index ecriture.ECRIRE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ecriture')
            and   type = 'U')
   drop table ecriture
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('EDITEUR')
            and   name  = 'LOCALISER_FK'
            and   indid > 0
            and   indid < 255)
   drop index EDITEUR.LOCALISER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EDITEUR')
            and   type = 'U')
   drop table EDITEUR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('EMPLOYE')
            and   name  = 'DISPOSER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index EMPLOYE.DISPOSER2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EMPLOYE')
            and   type = 'U')
   drop table EMPLOYE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ETAT')
            and   type = 'U')
   drop table ETAT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FORMAT')
            and   type = 'U')
   drop table FORMAT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LIVRE')
            and   name  = 'SPECIFIER_FK'
            and   indid > 0
            and   indid < 255)
   drop index LIVRE.SPECIFIER_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LIVRE')
            and   name  = 'PUBLIER_FK'
            and   indid > 0
            and   indid < 255)
   drop index LIVRE.PUBLIER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LIVRE')
            and   type = 'U')
   drop table LIVRE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('livraison')
            and   name  = 'LIVRER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index livraison.LIVRER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('livraison')
            and   name  = 'LIVRER_FK'
            and   indid > 0
            and   indid < 255)
   drop index livraison.LIVRER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('livraison')
            and   type = 'U')
   drop table livraison
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MOTCLE')
            and   type = 'U')
   drop table MOTCLE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('participation')
            and   name  = 'PARTICIPER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index participation.PARTICIPER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('participation')
            and   name  = 'PARTICIPER_FK'
            and   indid > 0
            and   indid < 255)
   drop index participation.PARTICIPER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('participation')
            and   type = 'U')
   drop table participation
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('referencement')
            and   name  = 'REFERENCER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index referencement.REFERENCER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('referencement')
            and   name  = 'REFERENCER_FK'
            and   indid > 0
            and   indid < 255)
   drop index referencement.REFERENCER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('referencement')
            and   type = 'U')
   drop table referencement
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('reglement')
            and   name  = 'REGLER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index reglement.REGLER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('reglement')
            and   name  = 'REGLER_FK'
            and   indid > 0
            and   indid < 255)
   drop index reglement.REGLER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('reglement')
            and   type = 'U')
   drop table reglement
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RUBRIQUE')
            and   type = 'U')
   drop table RUBRIQUE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SITEPAIEMENT')
            and   type = 'U')
   drop table SITEPAIEMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SOUSTHEME')
            and   type = 'U')
   drop table SOUSTHEME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('specialisation')
            and   name  = 'SPECIALISER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index specialisation.SPECIALISER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('specialisation')
            and   name  = 'SPECIALISER_FK'
            and   indid > 0
            and   indid < 255)
   drop index specialisation.SPECIALISER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('specialisation')
            and   type = 'U')
   drop table specialisation
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('thematisation')
            and   name  = 'THEMATISER2_FK'
            and   indid > 0
            and   indid < 255)
   drop index thematisation.THEMATISER2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('thematisation')
            and   name  = 'THEMATISER_FK'
            and   indid > 0
            and   indid < 255)
   drop index thematisation.THEMATISER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('thematisation')
            and   type = 'U')
   drop table thematisation
go

if exists (select 1
            from  sysobjects
           where  id = object_id('THEME')
            and   type = 'U')
   drop table THEME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TRANSPORTEUR')
            and   type = 'U')
   drop table TRANSPORTEUR
go

/*==============================================================*/
/* Table : ACCES                                                */
/*==============================================================*/
CREATE TABLE acces (
   idAcces              BIGINT               identity(1,1),
   typeAcces            INT                  not null,
   designationAcces     VARCHAR(50)          not null,
   CONSTRAINT PK_ACCES PRIMARY KEY NONCLUSTERED (idAcces)
)
GO

/*==============================================================*/
/* Table : TVA                                                */
/*==============================================================*/
CREATE TABLE tva (
   idTva				BIGINT              identity(1,1),
   tauxTva				float               not null,
   typeTva				int					not null,
   dateTva				DATETIME			not null,
   CONSTRAINT PK_TVA PRIMARY KEY NONCLUSTERED (idTva)
)
GO

/*==============================================================*/
/* Table : AUTEUR                                               */
/*==============================================================*/
CREATE TABLE auteur (
   idAuteur             BIGINT              IDENTITY(1,1),
   nomAuteur            VARCHAR(50)          not null,
   prenomAuteur         VARCHAR(50)          null,
   dateNaissanceAuteur  DATETIME             not null,
   dateDecesAuteur      DATETIME             null,
   avisAuteur           VARCHAR(255)         null,
   CONSTRAINT PK_AUTEUR PRIMARY KEY NONCLUSTERED (IDAUTEUR)
)
GO

/*==============================================================*/
/* Table : CIVILITE                                             */
/*==============================================================*/
CREATE TABLE civilite (
   idCivilite           BIGINT              IDENTITY(1,1),
   typeCivilite         INT                  not null,
   nomCivilite          VARCHAR(50)          not null,
   CONSTRAINT PK_CIVILITE PRIMARY KEY NONCLUSTERED (IDCIVILITE)
)
GO

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
CREATE TABLE client (
   idClient					BIGINT               IDENTITY(1,1),
   siretClient				VARCHAR(14)          null,
   avisClient				VARCHAR(255)         null,
   CONSTRAINT PK_CLIENT PRIMARY KEY NONCLUSTERED (IDCLIENT)
)
GO

/*==============================================================*/
/* Table : COMMANDE                                             */
/*==============================================================*/
CREATE TABLE commande (
   numeroCommande			VARCHAR(20)              not null,
   idSitePaiement			BIGINT              not null,
   idClient					BIGINT				not null,
   idEtat					BIGINT	            not null,
   idLivraisonCoordonnees   BIGINT	            not null,
   idTransporteur			BIGINT	            not null,
   idFacturationCoordonnees BIGINT	            not null,
   dateCommande				DATETIME            not null,
   modePaiementCommande		VARCHAR(50)         not null,
   dateLivraisonCommande	DATETIME            null,
   ipCommande				VARCHAR(20)         not null,
   referencePaiement		VARCHAR(50)         null,
   referenceColisCommande	VARCHAR(50)         null,
   CONSTRAINT PK_COMMANDE PRIMARY KEY NONCLUSTERED (NUMEROCOMMANDE)
)
GO

/*==============================================================*/
/* Index : PASSER_FK                                            */
/*==============================================================*/
create index PASSER_FK on commande (
idClient ASC
)
GO

/*==============================================================*/
/* Index : CHOISIR_FK                                           */
/*==============================================================*/
create index CHOISIR_FK on commande (
idSitePaiement ASC
)
GO

/*==============================================================*/
/* Index : TRANSPORTER_FK                                       */
/*==============================================================*/
create index TRANSPORTER_FK on commande (
idTransporteur ASC
)
GO

/*==============================================================*/
/* Index : QUALIFIER_FK                                         */
/*==============================================================*/
create index QUALIFIER_FK on commande (
idEtat ASC
)
GO

/*==============================================================*/
/* Index : ENVOYER_FK                                           */
/*==============================================================*/
create index ENVOYER_FK on commande (
idLivraisonCoordonnees ASC
)
GO

/*==============================================================*/
/* Index : FACTURER_FK                                          */
/*==============================================================*/
create index FACTURER_FK on commande (
idFacturationCoordonnees ASC
)
GO

/*==============================================================*/
/* Table : COMMENTAIRE                                          */
/*==============================================================*/
create table commentaire (
   numeroCommande			VARCHAR(20)     not null,
   idClient					BIGINT			not null,
   isbnLivre				VARCHAR(17)     not null,
   texteCommentaire			VARCHAR(255)    null,
   dateCommentaire			DATETIME        null,
   moderationCommentaire	INT				not null,
   texteModerationCommentaire	VARCHAR(255)	not null,
   CONSTRAINT PK_COMMENTER PRIMARY KEY (numeroCommande, idClient, isbnLivre)
)
GO

/*==============================================================*/
/* Index : COMMENTER_FK                                         */
/*==============================================================*/
create index COMMENTER_FK on commentaire (
numeroCommande ASC
)
GO

/*==============================================================*/
/* Index : COMMENTER2_FK                                        */
/*==============================================================*/
create index COMMENTER2_FK on commentaire (
idClient ASC
)
GO

/*==============================================================*/
/* Index : COMMENTER3_FK                                        */
/*==============================================================*/
create index COMMENTER3_FK on commentaire (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Table : COMPTE                                               */
/*==============================================================*/
CREATE TABLE compte (
   identifiantCompte    VARCHAR(100)        not null,
   idClient             BIGINT              null,
   idEmploye            BIGINT              null,
   idAcces              BIGINT              not null,
   mdpCompte            VARCHAR(100)        not null,
   dateCreationCompte   DATETIME            not null,
   actifCompte			INT					not null,
   CONSTRAINT PK_COMPTE PRIMARY KEY NONCLUSTERED (identifiantCompte)
)
GO

/*==============================================================*/
/* Index : CONNECTER2_FK                                        */
/*==============================================================*/
create index CONNECTER2_FK on compte (
idClient ASC
)
GO

/*==============================================================*/
/* Index : DISPOSER_FK                                          */
/*==============================================================*/
create index DISPOSER_FK on compte (
idEmploye ASC
)
GO

/*==============================================================*/
/* Index : IDENTIFIER_FK                                        */
/*==============================================================*/
create index IDENTIFIER_FK on compte (
idAcces ASC
)
GO

/*==============================================================*/
/* Table : CONTENANT                                            */
/*==============================================================*/
CREATE TABLE contenant (
   isbnLivre            VARCHAR(17)   not null,
   numeroCommande       VARCHAR(20)   not null,
   nbLivre              INT           not null,
   prixVente            FLOAT         not null,
   TVA					FLOAT		  NOT NULL,
   promotion            INT           null,
   CONSTRAINT PK_CONTENIR PRIMARY KEY (isbnLivre, numeroCommande)
)
GO

/*==============================================================*/
/* Index : CONTENIR_FK                                          */
/*==============================================================*/
create index CONTENIR_FK on contenant (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Index : CONTENIR2_FK                                         */
/*==============================================================*/
create index CONTENIR2_FK on contenant (
numeroCommande ASC
)
GO

/*==============================================================*/
/* Table : COORDONNEES                                          */
/*==============================================================*/
CREATE TABLE coordonnees (
   idCoordonnees        BIGINT              identity,
   idCivilite           BIGINT              not null,
   nomCoordonnees       VARCHAR(50)         not null,
   prenomCoordonnees    VARCHAR(50)         null,
   voieCoordonnees      VARCHAR(100)        not null,
   codePostalCoordonnees VARCHAR(5)         not null,
   villeCoordonnees     VARCHAR(50)         not null,
   complementCoordonnees VARCHAR(100)       null,
   telephoneCoordonnees VARCHAR(12)         null,
   telephonePortableCoordonnees VARCHAR(12) null,
   emailCoordonnees     VARCHAR(100)        null,
   typeCoordonnees      INT                 not null,
   CONSTRAINT PK_COORDONNEES PRIMARY KEY NONCLUSTERED (idCoordonnees)
)
GO

/*==============================================================*/
/* Index : FORMALISER_FK                                        */
/*==============================================================*/
create index FORMALISER_FK on coordonnees (
idCivilite ASC
)
GO

/*==============================================================*/
/* Table : ECRIRE                                               */
/*==============================================================*/
CREATE TABLE ecriture (
   isbnLivre            VARCHAR(17)         not null,
   idAuteur             BIGINT              not null,
   CONSTRAINT PK_ECRIRE PRIMARY KEY (isbnLivre, idAuteur)
)
GO

/*==============================================================*/
/* Index : ECRIRE_FK                                            */
/*==============================================================*/
create index ECRIRE_FK on ecriture (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Index : ECRIRE2_FK                                           */
/*==============================================================*/
create index ECRIRE2_FK on ecriture (
idAuteur ASC
)
GO

/*==============================================================*/
/* Table : EDITEUR                                              */
/*==============================================================*/
CREATE TABLE editeur (
   numeroSiretEditeur   VARCHAR(14)         not null,
   idCoordonnees        BIGINT              not null,
   nomEditeur           VARCHAR(50)         not null,
   avisEditeur          VARCHAR(255)        null,
   CONSTRAINT PK_EDITEUR PRIMARY KEY NONCLUSTERED (numeroSiretEditeur)
)
GO

/*==============================================================*/
/* Index : LOCALISER_FK                                         */
/*==============================================================*/
create index LOCALISER_FK on editeur (
idCoordonnees ASC
)
GO

/*==============================================================*/
/* Table : EMPLOYE                                              */
/*==============================================================*/
CREATE TABLE employe (
   idEmploye            BIGINT              IDENTITY(1,1),
   nomEmploye           VARCHAR(50)         not null,
   prenomEmploye        VARCHAR(50)         not null,
   emailEmploye         VARCHAR(100)        not null,
   telephoneEmploye     VARCHAR(12)         null,
   hideEmploye			INT					NOT NULL,
   CONSTRAINT PK_EMPLOYE PRIMARY KEY NONCLUSTERED (idEmploye)
)
GO

/*==============================================================*/
/* Table : ETAT                                                 */
/*==============================================================*/
CREATE TABLE etat (
   idEtat               BIGINT              IDENTITY(1,1),
   typeEtat             INT                 not null,
   nomEtat              VARCHAR(50)         not null,
   CONSTRAINT PK_ETAT PRIMARY KEY NONCLUSTERED (idEtat)
)
GO

/*==============================================================*/
/* Table : FORMAT                                               */
/*==============================================================*/
CREATE TABLE format (
   idFormat             BIGINT              IDENTITY(1,1),
   nomFormat            VARCHAR(50)         not null,
   CONSTRAINT PK_FORMAT PRIMARY KEY NONCLUSTERED (idFormat)
)
GO

/*==============================================================*/
/* Table : LIVRE                                                */
/*==============================================================*/
CREATE TABLE livre (
   isbnLivre            VARCHAR(17)         not null,
   numeroSiretEditeur   VARCHAR(14)         not null,
   idFormat             BIGINT              not null,
   idTva				BIGINT				not null,
   titreLivre           VARCHAR(255)        not null,
   sousTitreLivre       VARCHAR(255)        null,
   adresseImageLivre    VARCHAR(255)        not null,
   resumeLivre          VARCHAR(255)        not null,
   stockLivre           INT                 not null,
   collectionLivre      VARCHAR(50)         not null,
   prixHTLivre          FLOAT		        not null,
   avisLivre            VARCHAR(255)        null,
   CONSTRAINT PK_LIVRE PRIMARY KEY NONCLUSTERED (isbnLivre)
)
GO

/*==============================================================*/
/* Index : PUBLIER_FK                                           */
/*==============================================================*/
create index PUBLIER_FK on livre (
numeroSiretEditeur ASC
)
GO

/*==============================================================*/
/* Index : SPECIFIER_FK                                         */
/*==============================================================*/
create index SPECIFIER_FK on livre (
idFormat ASC
)
GO

/*==============================================================*/
/* Table : LIVRER                                               */
/*==============================================================*/
CREATE TABLE livraison (
   idClient             BIGINT              not null,
   idCoordonnees        BIGINT              not null,
   CONSTRAINT PK_LIVRER PRIMARY KEY (idClient, idCoordonnees)
)
GO

/*==============================================================*/
/* Index : LIVRER_FK                                            */
/*==============================================================*/
create index LIVRER_FK on livraison (
idClient ASC
)
GO

/*==============================================================*/
/* Index : LIVRER2_FK                                           */
/*==============================================================*/
create index LIVRER2_FK on livraison (
idCoordonnees ASC
)
GO

/*==============================================================*/
/* Table : MOTCLE                                               */
/*==============================================================*/
CREATE TABLE motCle (
   idMotCle             BIGINT              IDENTITY(1,1),
   nomMotCle            VARCHAR(50)         not null,
   CONSTRAINT PK_MOTCLE PRIMARY KEY NONCLUSTERED (idMotCle)
)
GO

/*==============================================================*/
/* Table : PARTICIPER                                           */
/*==============================================================*/
CREATE TABLE participation (
   isbnLivre            VARCHAR(17)         not null,
   idRubrique           BIGINT              not null,
   CONSTRAINT PK_PARTICIPER PRIMARY KEY (isbnLivre, idRubrique)
)
GO

/*==============================================================*/
/* Index : PARTICIPER_FK                                        */
/*==============================================================*/
create index PARTICIPER_FK on participation (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Index : PARTICIPER2_FK                                       */
/*==============================================================*/
create index PARTICIPER2_FK on participation (
idRubrique ASC
)
GO

/*==============================================================*/
/* Table : REFERENCER                                           */
/*==============================================================*/
CREATE TABLE referencement (
   idMotCle             BIGINT              not null,
   isbnLivre            VARCHAR(17)         not null,
   CONSTRAINT PK_REFERENCER PRIMARY KEY (idMotCle, isbnLivre)
)
GO

/*==============================================================*/
/* Index : REFERENCER_FK                                        */
/*==============================================================*/
create index REFERENCER_FK on referencement (
idMotCle ASC
)
GO

/*==============================================================*/
/* Index : REFERENCER2_FK                                       */
/*==============================================================*/
create index REFERENCER2_FK on referencement (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Table : REGLER                                               */
/*==============================================================*/
CREATE TABLE reglement (
   idCoordonnees        BIGINT              not null,
   idClient             BIGINT              not null,
   CONSTRAINT PK_REGLER PRIMARY KEY (idCoordonnees, idClient)
)
GO

/*==============================================================*/
/* Index : REGLER_FK                                            */
/*==============================================================*/
create index REGLER_FK on reglement (
idCoordonnees ASC
)
GO

/*==============================================================*/
/* Index : REGLER2_FK                                           */
/*==============================================================*/
create index REGLER2_FK on reglement (
idClient ASC
)
GO

/*==============================================================*/
/* Table : RUBRIQUE                                             */
/*==============================================================*/
CREATE TABLE rubrique (
   idRubrique           BIGINT              IDENTITY(1,1),
   nomRubrique          VARCHAR(50)         not null,
   dateDebutRubrique    DATETIME            not null,
   dateFinRubrique      DATETIME            not null,
   avisRubrique         VARCHAR(255)        not null,
   tauxPromotionRubrique INT                not null,
   CONSTRAINT PK_RUBRIQUE PRIMARY KEY NONCLUSTERED (idRubrique)
)
GO

/*==============================================================*/
/* Table : SITE_PAIEMENT                                        */
/*==============================================================*/
CREATE TABLE sitePaiement (
   idSitePaiement       BIGINT              IDENTITY(1,1),
   nomSitePaiement      VARCHAR(150)        not null,
   lienSitePaiement     VARCHAR(255)        not null,
   hideSitePaiement		INT					NOT NULL,
   CONSTRAINT PK_SITE_PAIEMENT PRIMARY KEY NONCLUSTERED (idSitePaiement)
)
GO

/*==============================================================*/
/* Table : SOUSTHEME                                            */
/*==============================================================*/
CREATE TABLE soustheme (
   idSousTheme          BIGINT              IDENTITY(1,1),
   nomSousTheme         VARCHAR(50)         not null,
   CONSTRAINT PK_SOUSTHEME PRIMARY KEY NONCLUSTERED (idSousTheme)
)
GO

/*==============================================================*/
/* Table : SPECIALISER                                          */
/*==============================================================*/
CREATE TABLE specialisation (
   idSousTheme          BIGINT              not null,
   idTheme              BIGINT              not null,
   CONSTRAINT PK_SPECIALISER PRIMARY KEY (idSousTheme, idTheme)
)
GO

/*==============================================================*/
/* Index : SPECIALISER_FK                                       */
/*==============================================================*/
create index SPECIALISER_FK on specialisation (
idSousTheme ASC
)
GO

/*==============================================================*/
/* Index : SPECIALISER2_FK                                      */
/*==============================================================*/
create index SPECIALISER2_FK on specialisation (
idTheme ASC
)
GO

/*==============================================================*/
/* Table : THEMATISER                                           */
/*==============================================================*/
CREATE TABLE thematisation (
   idSousTheme          BIGINT              not null,
   isbnLivre            VARCHAR(17)         not null,
   CONSTRAINT PK_THEMATISER PRIMARY KEY (idSousTheme, isbnLivre)
)
GO

/*==============================================================*/
/* Index : THEMATISER_FK                                        */
/*==============================================================*/
create index THEMATISER_FK on thematisation (
idSousTheme ASC
)
GO

/*==============================================================*/
/* Index : THEMATISER2_FK                                       */
/*==============================================================*/
create index THEMATISER2_FK on thematisation (
isbnLivre ASC
)
GO

/*==============================================================*/
/* Table : THEME                                                */
/*==============================================================*/
CREATE TABLE theme (
   idTheme              BIGINT              IDENTITY(1,1),
   nomTheme             VARCHAR(255)        not null,
   CONSTRAINT PK_THEME PRIMARY KEY NONCLUSTERED (idTheme)
)
GO

/*==============================================================*/
/* Table : TRANSPORTEUR                                         */
/*==============================================================*/
CREATE TABLE transporteur (
   idTransporteur			BIGINT              IDENTITY(1,1),
   nomTransporteur			VARCHAR(50)         not null,
   telephoneTransporteur	VARCHAR(12)         not null,
   avisTransporteur			VARCHAR(255)        null,
   hideTransporteur			INT					NOT NULL,
   CONSTRAINT PK_TRANSPORTEUR PRIMARY KEY NONCLUSTERED (idTransporteur)
)
GO




alter table commande add constraint fkidSitePaiementCommande foreign key(idSitePaiement) references sitePaiement(idSitePaiement)
alter table commande add constraint fkidClientCommande foreign key(idClient) references client(idClient)
alter table commande add constraint fkidEtatCommande foreign key(idEtat) references etat(idEtat)
alter table commande add constraint fkidLivraisonCoordonneesCommande foreign key(idLivraisonCoordonnees) references coordonnees(idCoordonnees)
alter table commande add constraint fkidTransporteurCommande foreign key(idTransporteur) references transporteur(idTransporteur)
alter table commande add constraint fkidFacturationCoordonneesCommande foreign key(idFacturationCoordonnees) references coordonnees(idCoordonnees)


alter table contenant add constraint fkisbnLivreContenant foreign key(isbnLivre) references livre(isbnLivre)
alter table contenant add constraint fknumeroCommandeContenant foreign key(numeroCommande) references commande(numeroCommande)


alter table livraison add constraint fkidClientLivraison foreign key(idClient) references client(idClient)
alter table livraison add constraint fkidCoordonneesLivraison foreign key(idCoordonnees) references coordonnees(idCoordonnees)


alter table reglement add constraint fkidClientReglement foreign key(idClient) references client(idClient)
alter table reglement add constraint fkidCoordonneesReglement foreign key(idCoordonnees) references coordonnees(idCoordonnees)


alter table commentaire add constraint fknumeroCommandeCommentaire foreign key(numeroCommande) references commande(numeroCommande)
alter table commentaire add constraint fkidClientCommentaire foreign key(idClient) references client(idClient)
alter table commentaire add constraint fkisbnLivreCommentaire foreign key(isbnLivre) references livre(isbnLivre)


alter table compte add constraint fkidClientCompte foreign key(idClient) references client(idClient)
alter table compte add constraint fkidEmployeCompte foreign key(idEmploye) references employe(idEmploye)
alter table compte add constraint fkidAccesCompte foreign key(idAcces) references acces(idAcces)


alter table coordonnees add constraint fkidCiviliteCoordonnees foreign key(idCivilite) references civilite(idCivilite)


alter table ecriture add constraint fkisbnLivreEcriture foreign key(isbnLivre) references livre(isbnLivre)
alter table ecriture add constraint fkidAuteurEcriture foreign key(idAuteur) references auteur(idAuteur)


alter table editeur add constraint fkidCoordonneesEditeur foreign key(idCoordonnees) references coordonnees(idCoordonnees)


alter table livre add constraint fknumeroSiretEditeurLivre foreign key(numeroSiretEditeur) references editeur(numeroSiretEditeur)
alter table livre add constraint fkidFormatLivre foreign key(idFormat) references format(idFormat)
alter table livre add constraint fkidTvaLivre foreign key(idTva) references tva(idTva)

alter table participation add constraint fkisbnLivreParticipation foreign key(isbnLivre) references livre(isbnLivre)
alter table participation add constraint fkidRubriqueParticipation foreign key(idRubrique) references rubrique(idRubrique)


alter table referencement add constraint fkidMotCleReferencement foreign key(idMotCle) references motCle(idMotCle)
alter table referencement add constraint fkisbnLivreReferencement foreign key(isbnLivre) references livre(isbnLivre)


alter table specialisation add constraint fkidSousThemeSpecialisation foreign key(idSousTheme) references soustheme(idSousTheme)
alter table specialisation add constraint fkidThemeSpecialisation foreign key(idTheme) references theme(idTheme)


alter table thematisation add constraint fkidSousThemeThematisation foreign key(idSousTheme) references soustheme(idSousTheme)
alter table thematisation add constraint fkisbnLivreThematisation foreign key(isbnLivre) references livre(isbnLivre)



