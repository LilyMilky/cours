﻿USE Librairie
GO

/****************************************************************************************/
/*																						*/
/*										Jeu de test BDD Livrairie						*/
/*																						*/
/****************************************************************************************/



/**********************************************SITEPAIEMENT******************************/
-- Yann

insert into sitePaiement(nomSitePaiement, lienSitePaiement, hideSitePaiement) values
('Paypal','https://www.paypal.com', 1),
('Payzen','https://payzen.eu/', 1),
('Payline','www.payline.com/', 0)

/**********************************************SITEPAIEMENT******************************/
-- Philippe

INSERT INTO tva(tauxTva, typeTva, dateTva) VALUES
(5.5, 0, '01-01-2014'),
(20, 1, '01-01-2014')


/*********************************************TRANSPORTEUR********************************/
-- Yann

insert into transporteur(nomTransporteur, telephoneTransporteur, avisTransporteur, hideTransporteur) values
('Chronopost','0825801801','CHER, PAS PRO!!', 1),
('GLS','0102030405','pas cher', 1),
('La Poste','0506080709','Classique', 1),
('GEFCO','0146964578','RAS', 0)

/*************************************************ETAT*************************************/
-- Yann

insert into etat(typeEtat, nomEtat) values
(1,'En cours'),
(2,'En preparation'),
(3,'Livre'),
(4,'Colis Volé'),
(5,'Colis Perdu'),
(6,'En point Relais')

/******************************************CIVILITE***************************************/
-- Herve

 insert into civilite(typeCivilite,nomCivilite)  values (1,'homme'),
														(2,'femme'),
														(3,'neutre'),
														(4,'société')

/*******************************************ACCES*****************************************/
--Herve

insert into acces(typeAcces,designationAcces)  values(1,'admin'),
                                                     (2,'moderateur'),
  												     (3,'client'),
													 (4, 'superadmin')

/***************************************COORDONNEES****************************************/
--Herve

insert into coordonnees(idCivilite,nomCoordonnees,prenomCoordonnees,voieCoordonnees,codePostalCoordonnees,
		    villeCoordonnees,complementCoordonnees,telephoneCoordonnees,telephonePortableCoordonnees,
		    emailCoordonnees,typeCoordonnees)	
			values    
( 1,'artois','stephane','21 rue du marechal foch', 78110,'Le vesinet','bat A porte 130','0144754857','0625845874','artoistephane@yahoo.fr',1),
( 1,'Gordo','Eddy','10 rue du temple', 72000,'Le mans ','','0244757557','0775842314','eddygordo@hotmail.com',1),
( 2,'Ling','xiaou','110 rue de stalingrad ', 95140,'Garges-les-gonesses','','0158472396','','lingXiao@gmail.com',1),
( 2,'williams','anna','45 rue jules vernes ',13000,'Marseille','','0484396895','','annawilliams@facebook.com',2),	
( 3,'pelerin','sylvestre','14 bd de la republique',49100,'Angers','','0243396895','','pelerinsylvestre@hotmail.com',2),
( 3,'gun','jack','76 ave des filiformes',76000,'Vernon','','0232396895','','gunJack@facebook.com',2),		
( 1,'Fury','Bryan','5 ave des ouvriers',76000,'Vernon','','0232396895','','bryanfury@hotmail.fr',1),
( 2,'jun','kitana','8 ruel à riou ',69000,'Lyon','','0472103030','','jun@sfr.fr',2),	
( 1,'Law','Forest','17 place de la mairie',84000,'Avignon','','0474587496','','law@sfr.fr',1),	
( 1,'wulong','Lei','1 ave de montaigne ',75012,'Paris','','0186357421','0658742135','lei@info.net',1),

( 4,'Gallimard','gaston','5 rue gaston gallimard ',75328,'Paris','','0149544200','','edgal@gallimard.fr',3),
( 4,'Pocket','polly','68 rue de rivoli ',75004,'Paris','','0149324586','','pocket@pocket.net',3),
( 4,'Dorey','Philippe','17 rue jacob ',75006,'Paris','',' 0149154563','','dphil@jc-lattes.net',3),
( 4,'Cache','sebastien','31 rue de fleurus ',75278,'Paris6','','0149543700','','csebastien@orange.fr',3),
( 4,'Ankama ','vivian','75 BOULEVARD D ARMENTIERES ',59100,'Roubaix','',' 0320363000','','ankvivian@ankama.net',3),
( 4,'Duval','charles','24, avenue Philippe Auguste',75011,'paris','','0149298888  ','','dcharles@gmail.net',3),
( 4,'Delcourt','jacques','24 RUE DE MARCQ EN BAROEUL ',59270,'WASQUEHAL','','','','sarl.delcourt@wanadoo.fr',3),
( 4,'Soufflet','julien','10, rue de Nesle ',75006,'paris','',' 0143290050 ','','s.julien@bibliothequedelimage.com',3),
( 4,'Hachette','basile','58 rue Jean Bleuzen',92170,'VANNES','','0143923030','','bhachette@hachette.fr',3),
( 4,'Faugeras','Françoise','RAUD',38710,'mens','','0476348080','','ffaugeras@terrevivante.com',3)

/***************************************CLIENT************************************************/
--Herve

insert into client(siretClient,avisClient)values
	('', ''),
	('', 'Client de longue date'),
	('',  ''),
	('', ''),
	('12335498631574', 'rien à dire'),
	('', 'génial!!!!'),
	('', ''),
	('43895123548628', 'satisfait !!'),
	('', ''),
	('', '')

/***************************************EMPLOYE*********************************************/
--Herve

insert into employe(nomEmploye,prenomEmploye,emailEmploye, hideEmploye) 
values('solal','alain','alain.dupont@pyph.com', 1),
	  ('hegel','Thomas','thomashegel@pyph.com', 1),
	  ('poisson','valentine','vpoisson@pyph.com', 0),
	  ('speaker','catherine','catSpeaker2@pyph.com', 1),
	  ('barreau','thibaut','thibautbarreau85@pyph.com', 1),
	  ('soler ','christine','solerchristine@pyph.com', 0),
	  ('tran','michel','micheltran@pyph.com', 1),
	  ('vasseur','eugenie','eugenievasseur@pyph.com', 1),
	  ('lefebvre','annie','annielefebvre@pyph.com', 1),
	  ('pasteur','Jean-Marc','marcpasteur@pyph.com', 1),
	  ('cardinal','jerome','jcardinal@pyph.com', 1),
	  ('milky', 'milky', 'milky@pyph.com', 1),
	  ('graven', 'graven', 'graven@pyph.com', 1),
	  ('herve', 'herve', 'herve@pyph.com', 1),
	  ('pascal', 'pascal', 'pascal@pyph.com', 1)

/***************************************COMPTE*********************************************/
--Herve

insert into compte(identifiantCompte ,idClient,idEmploye,idAcces,mdpCompte,dateCreationCompte, actifCompte)
values('artoistephane@yahoo.fr',1,null,3,'stephartois','22-12-2015 03:51', 1),
      ('eddygordo@hotmail.com',2,null,3,'edgordo','27-01-2014 01:20', 1),
	  ('lingXiao@gmail.com',3,null,3,'lingxiao','14-04-2014 10:30', 1),
	  ('pelerinsylvestre@hotmail.com',5,null,3,'perlsylvestre','21-11-2014 06:11', 1),
	  ('annawilliams@facebook.com',4,null,3,'annawil','28-01-2015 16:51', 1),
	  ('gunJack@facebook.com',6,null,3,'gunjack ','25-01-2015 08:41', 1),
	  ('jun@sfr.fr',8,null,3,'jun','25-01-2015 08:45', 1),
	  ('law@sfr.fr',9,null,3,'laws','28-01-2016 08:41', 1),
	  ('bryanfury@hotmail.fr',7,null,3,'bryfury','16-02-2016 14:48', 1),
	  ('lei@info.net',10,null,3,'lei','25-12-2015 10:41', 1),

	  ('alain.dupont@pyph.com',null,1,1,'alain','25-12-2010 10:41', 1),
	  ('thomashegel@pyph.com',null,2,1,'thomas','25-02-2011 10:41', 1),
	  ('vpoisson@pyph.com',null,3,2,'valentine','25-03-2012 10:41', 1),
	  ('catSpeaker2@pyph.com',null,4,1,'catherine','25-04-2013 10:41', 1),
	  ('thibautbarreau85@pyph.com',null,5,1,'thibaut','25-05-2014 10:41', 1),
	  ('solerchristine@pyph.com',null,6,1,'christine','25-06-2015 10:41', 1),
	  ('micheltran@pyph.com',null,7,1,'michel','01-01-2000 10:41', 1),
	  ('eugenievasseur@pyph.com',null,8,2,'eugenie','25-07-2001 10:41', 1),
	  ('annielefebvre@pyph.com',null,9,1,'annie','25-08-2002 10:41', 1),
	  ('marcpasteur@pyph.com',null,10,2,'jeanmarc','25-09-2003 10:41', 1),
	  ('jcardinal@pyph.com',null,11,1,'jerome','25-10-2004 10:41', 1),
	  ('milky@pyph.com', null,12, 4, 'milky', '01-01-2016', 1),
	  ('graven@pyph.com', null,12, 4, 'graven', '01-01-2016', 1),
	  ('herve@pyph.com', null,12, 4, 'herve', '01-01-2016', 1),
	  ('pascal@pyph.com', null,12, 4, 'pascal', '01-01-2016', 1)

/************************************************FORMAT************************************/
-- Philippe

INSERT INTO format(nomFormat)
VALUES
('POCHE'),
('BANDE DESSINEE'),
('MANGA'),
('BROCHE'),
('RELIE'),
('ROMAN'),
('JEUNESSE'),
('DICTIONNAIRE'),
('MAGAZINE'),
('ENCYCLOPEDIE')

/************************************************RUBRIQUE**********************************/
-- Philippe

INSERT INTO rubrique(nomRubrique, dateDebutRubrique, dateFinRubrique, avisRubrique, tauxPromotionRubrique)
VALUES
('Aucun', '01-01-1900 00:00', '', '', 0),
('St Valentin', '08-02-2016 00:00', '15-02-2016 00:00', '', 0),
('Noël', '15-11-2015 00:00', '30-12-2015 00:00', '', 0),
('Chandeleur', '15-01-2015 00:00', '03-02-2015 00:00', '', 0),
('Summer time', '15-06-2015 00:00', '15-09-2015 00:00', '', 0),
('Promotion', '01-03-2015 00:00', '15-03-2015 00:00', '', 5),
('Coup de coeur', '01-02-2016 00:00', '29-02-2016 00:00', '', 0),
('Meilleures ventes', '01-03-2016 00:00', '31-03-2016 00:00', '', 0),
('Meilleures ventes', '01-04-2016 00:00', '30-04-2016 00:00', '', 0),
('Meilleures ventes', '01-02-2016 00:00', '29-02-2016 00:00', '', 0),
('Nouveautés', '01-04-2016 00:00', '30-04-2016 00:00', '', 0),
('Top nouveautés', '01-02-2016 00:00', '28-02-2016 00:00', '', 0),
('A paraître', '01-11-2016 00:00', '30-11-2016 00:00', '', 0),
('Prix de la littérature', '01-10-2016 00:00', '31-10-2016 00:00', '', 0)

/************************************************THEME************************************/
-- Pascal

INSERT INTO theme (nomTheme)
VALUES 
('Science-Fiction'),
('Roman'),
('Polar'),
('Aventure'),
('Jeu-vidéo'),
('Art'),
('Cuisine'),
('Jardin')

/************************************************SOUS-THEME********************************/
-- Pascal

INSERT INTO soustheme (nomSousTheme)
VALUES 
('Thriller'),
('Nouvelle'),
('Sentimentale'),
('Policier et suspense'),
('Heroic Fantasy'),
('Espace'),
('Exploration'),
('Romantisme'),
('Cuisine expérimentale'),
('Jardinage')

/************************************************MOT CLE**********************************/
-- Pascal

INSERT INTO motCle (nomMotCle)
VALUES
('Dinosaure'),
('Tyranosaurus Rex'),
('Diplodocus'),
('Velociraptor'),
('JPark'),
('Grant'),

('Staline'),
('Big Brother'),
('Ignorance'),
('Force'),
('Surveillance'),
('Dictature'),

('Nuance'),
('Gris'),
('SM'),
('Supplice'),
('Douleur'),
('Masochiste'),
('Erotisme'),

('Hotel'),
('Overlook'),
('Montagne'),
('Colorado'),
('Demon'),
('Force du mal'),


('Tofu'),
('Magie'),
('Confrérie'),
('Dessin animé'),
('Epopée'),
('Dofus'),

('Dystopique'),
('Pyramide'),
('Extraterrestre'),
('Techno-Techno'),
('Imperoratriz'),
('Gardienne'),

('Apocalypse'),
('Boston'),
('Survivant'),
('Radioactif'),
('Pip-Boy'),
('Bombe atomique'),

('Maroc'),
('Aquarelle'),
('Croquis'),
('Carnet de voyage'),

('Aquilonie'),
('Sauron'),
('Piparkakku'),
('Brochette'),
('Barbras'),
('Ragout'),

('Serre'),
('Terreau'),
('Fumier'),
('Semences'),
('Récolte'),
('Fruit'),
('Légume')

/************************************************AUTEUR**********************************/
-- Pascal

INSERT INTO auteur (nomAuteur, prenomAuteur,dateNaissanceAuteur,dateDecesAuteur,avisAuteur)
VALUES 
('Crichton','Michael','19550322',null,'Cool'),
('Orwell','George','19030620','19500121','Super auteur'),
('Mitchell','Erika','19630307',null,'Genial'),
('King','Stephen','19470821',null,'Top'),
('Sassine','Said','19780110',null,'Trop bien'),
('Giraud','Jean','19380208','20120310','Culte'),
('Softwork','Bethesda','20010201',null,'Wow'),
('Delacroix','Eugene','17980427','18630813','Grand artiste'),
('Villanova','Thibaud','19720710',null,'Cuisinier original'),
('Thorez','Jean-Paul','19670307',null,'Sérieux et déterminé')

/************************************************EDITEUR***************************************/
-- Philippe

INSERT INTO editeur(numeroSiretEditeur, idCoordonnees, nomEditeur, avisEditeur)
VALUES
('57220675300012', 11,'Edition GALLIMARD', 'En attente de réappro'),
('44447186600020', 12, 'Edition POCKET', null),
('68202865900048', 13, 'Edition JC LATTES', null),
('54208674900117', 14, 'Le livre de poche', 'A changé d''adresse'),
('49236073000021', 15, 'Ankama', null),
('47882795900030', 16, 'Les humanoïdes associés', null),
('41519788800068', 17, 'DELCOURT', null),
('32227218800010', 18, 'Bibliothèque de l''image', null),
('32105727500034', 20, 'Terre Vivante Editions', null),
('60206014700033', 19, 'Hachette pratique', null)

/************************************************LIVRE**********************************/
-- Pascal

INSERT INTO livre (isbnLivre,numeroSiretEditeur,idFormat,titreLivre,sousTitreLivre,adresseImageLivre,resumeLivre,
stockLivre,collectionLivre,prixHTLivre,avisLivre, idTva)
VALUES 
('978-2-266-26282-8','57220675300012',4,'Jurassic Park','','./images/Jurassic.jpg', '
Isla Nublar. L''armée doit venir " faire le ménage ". Le programme dont cette île est le théâtre avait pourtant tout du paradis scientifique...
',200,'',8,'Le mythe de Frankenstein revisité', 1),
('978-2-070-36822-8','44447186600020',7,'1984','','./images/1984.jpg', '
De tous les carrefours importants, le visage à la moustache noire vous fixait du regard. BIG BROTHER VOUS REGARDE, répétait la légende, tandis que le regard des yeux noirs pénétrait les yeux de Winston... 
',150,'',8.70,'Pas si science fiction que ça!', 1),
('978-2-709-64252-1','68202865900048',6,'Cinquante nuances plus grises','','./images/50nuance.jpg', '
Ana et Christian ont tout pour être heureux : l''amour, la fortune et un avenir plein de promesses... 
',50,'Cinquante nuances',17,'Très bon ouvrage', 2),
('978-2-253-15162-3','54208674900117',1,'The Shining','','./images/Shining.jpg', '
Situé dans les montagnes Rocheuses, l’Overlook Palace passe pour être l’un des plus beaux lieux du monde. Confort, luxe, volupté… L’hiver, l’hôtel est fermé. Coupé du monde par le froid et la neige.
',150,'',7.90,'Magnifique et horrible à la fois!', 2),
('978-2-359-10274-1','49236073000021',3,'Wakfu','','./images/Wakfu01.jpg', '
L’action prend place directement à la fin de la deuxième saison du dessin animé, dont l’épilogue a été diffusé début mars 2012 sur France 3.
',300,'Wakfu',6.95,'Aventures sympathiques!', 2),
('978-2-731-63071-8','47882795900030',2,'L''incal volume 1','','./images/Incal.jpg', '
Dans un futur lointain, une autre galaxie ou un autre espace-temps, l’Incal et l’immense pouvoir qu’il confère exacerbent toutes les convoitises. John Difool, minable détective de classe R adepte d’homéoputes.
',70,'Incal',7.9,'Ma BD favorite, cultissime', 1),
('978-1-616-55980-9','41519788800068',5,'L''art de fallout 4','','./images/Fallout.jpg', '
Bethesda Game Studios, the award-winning creators of Fallout® 3 and The Elder Scrolls V: Skyrim®, welcome you to the world of Fallout® 4 - their most ambitious game ever, and the next generation of open-world gaming.
',20,'Fallout',39.9,'Une piece de collection', 1),
('978-2-909-80872-7','32227218800010',4,'Delacroix, voyage au Maroc','Aquarelle','./images/DelacroixMaroc.jpg', '
Ouvrage correspondant au descriptif et utilisable avec des élèves dans le cadre d''une étude sur le peintre en raison de son coût peu élevé. les reproductions sont d''un format également intéressant. 
',130,'Delacroix',10,'Pour le amateurs de carnet de voyage', 2),
('978-2-012-31832-8','60206014700033',8,'Gastronogeek: 42 recettes inspirées des cultures de l''imaginaire','Bouffe','./images/Gastronogeek.jpg', '
42 recettes créatives et gourmandes autour de 15 thèmes incontournables de la culture geek. De Harry Potter à Star Wars, en passant par Dragon Ball, un hommage gastronomique à des références cultes et une série d''énigmes.
',250,'Hachette Pratique',14,'Super original', 2),
('978-2-360-98057-4','32105727500034',4,'Je démarre mon potager','Introduction au bio','./images/PotagerBio.jpg', '
Pour démarrer son potager sans risque de déconvenue, le jardinier en herbe a besoin d''être bien guidé. Avant tout, il doit préparer sa terre, apprendre à la désherber ou à l''enrichir naturellement sans la polluer. 
',150,'Facile & Bio',12,'Parfait pour le débutant', 1)

/************************************************ECRITURE**********************************/
-- Pascal

INSERT INTO ecriture (isbnLivre,idAuteur)
VALUES 
('978-2-266-26282-8',1),
('978-2-070-36822-8',2),
('978-2-709-64252-1',3),
('978-2-253-15162-3',4),
('978-2-359-10274-1',5),
('978-2-731-63071-8',6),
('978-1-616-55980-9',7),
('978-2-909-80872-7',8),
('978-2-012-31832-8',9),
('978-2-360-98057-4',10)

/************************************************REFERENCEMENT**********************************/
-- Pascal

INSERT INTO referencement (idMotCle,isbnLivre)
VALUES
(1,'978-2-266-26282-8'),
(2,'978-2-266-26282-8'),
(3,'978-2-266-26282-8'),
(4,'978-2-266-26282-8'),
(5,'978-2-266-26282-8'),
(6,'978-2-266-26282-8'),

(7,'978-2-070-36822-8'),
(8,'978-2-070-36822-8'),
(9,'978-2-070-36822-8'),
(10,'978-2-070-36822-8'),
(11,'978-2-070-36822-8'),
(12,'978-2-070-36822-8'),

(13,'978-2-709-64252-1'),
(14,'978-2-709-64252-1'),
(15,'978-2-709-64252-1'),
(16,'978-2-709-64252-1'),
(17,'978-2-709-64252-1'),
(18,'978-2-709-64252-1'),
(19,'978-2-709-64252-1'),

(20,'978-2-253-15162-3'),
(21,'978-2-253-15162-3'),
(22,'978-2-253-15162-3'),
(23,'978-2-253-15162-3'),
(24,'978-2-253-15162-3'),
(25,'978-2-253-15162-3'),

(26,'978-2-359-10274-1'),
(27,'978-2-359-10274-1'),
(28,'978-2-359-10274-1'),
(29,'978-2-359-10274-1'),
(30,'978-2-359-10274-1'),
(31,'978-2-359-10274-1'),

(32,'978-2-731-63071-8'),
(33,'978-2-731-63071-8'),
(34,'978-2-731-63071-8'),
(35,'978-2-731-63071-8'),
(36,'978-2-731-63071-8'),
(37,'978-2-731-63071-8'),

(38,'978-1-616-55980-9'),
(39,'978-1-616-55980-9'),
(40,'978-1-616-55980-9'),
(41,'978-1-616-55980-9'),
(42,'978-1-616-55980-9'),
(43,'978-1-616-55980-9'),

(44,'978-2-909-80872-7'),
(45,'978-2-909-80872-7'),
(46,'978-2-909-80872-7'),
(47,'978-2-909-80872-7'),

(48,'978-2-012-31832-8'),
(49,'978-2-012-31832-8'),
(50,'978-2-012-31832-8'),
(51,'978-2-012-31832-8'),
(52,'978-2-012-31832-8'),
(53,'978-2-012-31832-8'),

(54,'978-2-360-98057-4'),
(55,'978-2-360-98057-4'),
(56,'978-2-360-98057-4'),
(57,'978-2-360-98057-4'),
(58,'978-2-360-98057-4'),
(59,'978-2-360-98057-4'),
(60,'978-2-360-98057-4')

/************************************************SPECIALISATION**********************************/
-- Pascal

INSERT INTO specialisation (idSousTheme,idTheme)
VALUES
(1,1),
(2,1),
(3,2),
(4,3),
(5,4),
(6,4),
(7,5),
(8,6),
(9,7),
(10,8)

/************************************************THEMATISATION**********************************/
-- Pascal

INSERT INTO thematisation (idSousTheme,isbnLivre)
VALUES
(1,'978-2-266-26282-8'),
(2,'978-2-070-36822-8'),
(3,'978-2-709-64252-1'),
(4,'978-2-253-15162-3'),
(5,'978-2-359-10274-1'),
(6,'978-2-731-63071-8'),
(7,'978-1-616-55980-9'),
(8,'978-2-909-80872-7'),
(9,'978-2-012-31832-8'),
(10,'978-2-360-98057-4')

/*************************************************COMMANDE**************************************/
-- Yann

insert into commande (numeroCommande,idSitePaiement,idClient,idEtat,idLivraisonCoordonnees,idTransporteur,idFacturationCoordonnees,dateCommande,modePaiementCommande
,dateLivraisonCommande,ipCommande, referencePaiement, referenceColisCommande) values

('00000000000000000001', 1, 1, 1, 1, 1, 6,'20-10-2015 00:00','Carte Bancaire', null,'10.75.240.60','00000000000000000001','00000000000000000001'),
('00000000000000000002', 2, 2, 6, 2, 2, 3,'20-12-2015 00:00','Carte Bancaire', null,'70.72.200.25','00000000000000000002','00000000000000000002'),
('00000000000000000003', 3, 3, 3, 9, 3, 7,'08-10-2014 00:00','Carte Bancaire','11-10-2014 00:00','11.33.145.36','00000000000000000003','00000000000000000003'),
('00000000000000000004', 1, 4, 3, 4, 4, 6,'01-01-2015 00:00','Carte Bancaire', '15-01-2015 00:00','15.25.120.64','00000000000000000004','00000000000000000004'),
('00000000000000000005', 2, 5, 6, 11, 1, 10,'11-02-2016 00:00','Carte Bancaire', null,'100.28.167.17','00000000000000000005','00000000000000000005')


/***********************************************CONTENANT**************************************/
-- Yann

insert into contenant (isbnLivre, numeroCommande, nbLivre, prixVente, TVA, promotion) values
('978-2-266-26282-8','00000000000000000001','3', 5.5,10.20,0),
('978-2-909-80872-7','00000000000000000001','1', 5.5,8.50,0),
('978-2-360-98057-4','00000000000000000001','1', 5.5,20.00,0),
('978-1-616-55980-9','00000000000000000002','7', 5.5,10.00,0),
('978-2-359-10274-1','00000000000000000003','2', 5.5,15.50,5),
('978-2-253-15162-3','00000000000000000003','1', 5.5,4.99,0),
('978-2-070-36822-8','00000000000000000004','1', 5.5,10.20,0),
('978-2-360-98057-4','00000000000000000004','3', 5.5,10.20,5),
('978-2-253-15162-3','00000000000000000004','4', 5.5,10.20,0),
('978-2-709-64252-1','00000000000000000004','3', 5.5,10.20,0),
('978-2-266-26282-8','00000000000000000005','15', 5.5,1.20,0)


/***********************************************LIVRAISON****************************************/
-- Yann

insert into livraison(idClient, idCoordonnees) values
(1,6),
(2,2),
(3,7),
(4,4),
(5,9)

/************************************************REGLEMENT***************************************/
-- Yann

insert into reglement(idCoordonnees, idClient) values
(1,1),
(2,2),
(3,3),
(4,4),
(5,5)


/************************************************PARTICIPATION**********************************/
-- Philippe

INSERT INTO participation(isbnLivre, idRubrique)
VALUES
('978-2-709-64252-1', 1),
('978-2-360-98057-4', 5),
('978-2-266-26282-8', 7),
('978-2-012-31832-8', 1),
('978-2-070-36822-8', 5),
('978-2-709-64252-1', 6)


/*********************************************COMMENTAIRE**********************************/
-- Philippe

INSERT INTO commentaire(numeroCommande, idClient, isbnLivre, texteCommentaire, dateCommentaire, moderationCommentaire, texteModerationCommentaire)
VALUES 
('00000000000000000003', 3, '978-2-359-10274-1', 'L''histoire laisse un sentiment de pas fini', '01-12-2014 00:00', 1, ''),
('00000000000000000004', 4, '978-2-253-15162-3', 'Pas mal', '01-02-2015 00:00', 0, 'Merci de rester courtoie')

