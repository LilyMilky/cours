use Librairie;

alter table livre add constraint ckStockNonNega
	Check(stockLivre >= 0)

alter table livre add constraint ckPrixNonNega
	Check(prixHTLivre >= 0)

alter table rubrique add constraint ckTauxPromotionRubriqueMin
	Check(tauxPromotionRubrique >= 0)

alter table rubrique add constraint ckTauxPromotionRubriqueMax
	Check (tauxPromotionRubrique <= 5)

/*****Valide?*****/
alter table coordonnees add constraint ckCodePostalChiffreOnly
	Check(codePostalCoordonnees >= 0)


