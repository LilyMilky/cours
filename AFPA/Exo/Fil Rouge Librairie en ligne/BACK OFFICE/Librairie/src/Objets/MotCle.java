package Objets;

/**
 *
 * @author Milky
 */
public class MotCle {
    
    private int idMotCle;
    private String nomMotCle;
    
    public MotCle() {
    }

    public MotCle(int idMotCle, String nomMotCle) {
        this.idMotCle = idMotCle;
        this.nomMotCle = nomMotCle;
    }
    
    
    public int getIdMotCle() {
        return idMotCle;
    }

    public void setIdMotCle(int idMotCle) {
        this.idMotCle = idMotCle;
    }

    public String getNomMotCle() {
        return nomMotCle;
    }

    public void setNomMotCle(String nomMotCle) {
        this.nomMotCle = nomMotCle;
    }
}
