package Objets;

/**
 *
 * @author Milky
 */
public class Client {
    
    private int idClient;
    private String siretClient;
    private String avisClient;

    public Client() {
    }

    public Client(int idClient, String siretClient, String avisClient) {
        this.idClient = idClient;
        this.siretClient = siretClient;
        this.avisClient = avisClient;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getSiretClient() {
        return siretClient;
    }

    public void setSiretClient(String siretClient) {
        this.siretClient = siretClient;
    }

    public String getAvisClient() {
        return avisClient;
    }

    public void setAvisClient(String avisClient) {
        this.avisClient = avisClient;
    }
    
    
    
}
