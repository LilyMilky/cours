package Objets;

/**
 *
 * @author Milky
 */
public class Livre {
    
    String isbnLivre;
    String numeroSiretEditeur;
    int idFormat;
    String titreLivre;
    String sousTitreLivre;
    String adresseImageLivre;
    String resumeLivre;
    int stockLivre; //   > 0
    String collectionLivre;
    float prixHTLivre;  //  > 0
    String avisLivre;

    public Livre() {
    }

    public Livre(String isbnLivre, String numeroSiretEditeur, int idFormat, String titreLivre, String sousTitreLivre, String adresseImageLivre, String resumeLivre, int stockLivre, String collectionLivre, float prixHTLivre, String avisLivre) {
        this.isbnLivre = isbnLivre;
        this.numeroSiretEditeur = numeroSiretEditeur;
        this.idFormat = idFormat;
        this.titreLivre = titreLivre;
        this.sousTitreLivre = sousTitreLivre;
        this.adresseImageLivre = adresseImageLivre;
        this.resumeLivre = resumeLivre;
        this.stockLivre = stockLivre;
        this.collectionLivre = collectionLivre;
        this.prixHTLivre = prixHTLivre;
        this.avisLivre = avisLivre;
    }

    public String getIsbnLivre() {
        return isbnLivre;
    }

    public void setIsbnLivre(String isbnLivre) {
        this.isbnLivre = isbnLivre;
    }

    public String getNumeroSiretEditeur() {
        return numeroSiretEditeur;
    }

    public void setNumeroSiretEditeur(String numeroSiretEditeur) {
        this.numeroSiretEditeur = numeroSiretEditeur;
    }

    public int getIdFormat() {
        return idFormat;
    }

    public void setIdFormat(int idFormat) {
        this.idFormat = idFormat;
    }

    public String getTitreLivre() {
        return titreLivre;
    }

    public void setTitreLivre(String titreLivre) {
        this.titreLivre = titreLivre;
    }

    public String getSousTitreLivre() {
        return sousTitreLivre;
    }

    public void setSousTitreLivre(String sousTitreLivre) {
        this.sousTitreLivre = sousTitreLivre;
    }

    public String getAdresseImageLivre() {
        return adresseImageLivre;
    }

    public void setAdresseImageLivre(String adresseImageLivre) {
        this.adresseImageLivre = adresseImageLivre;
    }

    public String getResumeLivre() {
        return resumeLivre;
    }

    public void setResumeLivre(String resumeLivre) {
        this.resumeLivre = resumeLivre;
    }

    public int getStockLivre() {
        return stockLivre;
    }

    public void setStockLivre(int stockLivre) {
        this.stockLivre = stockLivre;
    }

    public String getCollectionLivre() {
        return collectionLivre;
    }

    public void setCollectionLivre(String collectionLivre) {
        this.collectionLivre = collectionLivre;
    }

    public float getPrixHTLivre() {
        return prixHTLivre;
    }

    public void setPrixHTLivre(float prixHTLivre) {
        this.prixHTLivre = prixHTLivre;
    }

    public String getAvisLivre() {
        return avisLivre;
    }

    public void setAvisLivre(String avisLivre) {
        this.avisLivre = avisLivre;
    }
    
    
    
}
