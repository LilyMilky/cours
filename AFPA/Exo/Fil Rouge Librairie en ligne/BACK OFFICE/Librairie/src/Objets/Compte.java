package Objets;

public class Compte {
    private String identifiantCompte;
    private int idClient;
    private int idEmploye;
    private int idAcces;
    private String mdpCompte;
    private String dateCreationCompte;
    private int actifCompte;

    public Compte() {
    }

    public Compte(String identifiantCompte, int idClient, int idEmploye, int idAcces, String mdpCompte, String dateCreationCompte, int actifCompte) {
        this.identifiantCompte = identifiantCompte;
        this.idClient = idClient;
        this.idEmploye = idEmploye;
        this.idAcces = idAcces;
        this.mdpCompte = mdpCompte;
        this.dateCreationCompte = dateCreationCompte;
        this.actifCompte = actifCompte;
    }

    public String getIdentifiantCompte() {
        return identifiantCompte;
    }

    public void setIdentifiantCompte(String identifiantCompte) {
        this.identifiantCompte = identifiantCompte;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public int getIdAcces() {
        return idAcces;
    }

    public void setIdAcces(int idAcces) {
        this.idAcces = idAcces;
    }

    public String getMdpCompte() {
        return mdpCompte;
    }

    public void setMdpCompte(String mdpCompte) {
        this.mdpCompte = mdpCompte;
    }

    public String getDateCreationCompte() {
        return dateCreationCompte;
    }

    public void setDateCreationCompte(String dateCreationCompte) {
        this.dateCreationCompte = dateCreationCompte;
    }

    public int getActifCompte() {
        return actifCompte;
    }

    public void setActifCompte(int actifCompte) {
        this.actifCompte = actifCompte;
    }

    @Override
    public String toString() {
        return "Compte{" + "identifiantCompte=" + identifiantCompte + ", idClient=" + idClient + ", idEmploye=" + idEmploye + ", idAcces=" + idAcces + ", mdpCompte=" + mdpCompte + ", dateCreationCompte=" + dateCreationCompte + ", actifCompte=" + actifCompte + '}';
    }
    
    
}
