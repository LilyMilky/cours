package Objets;

/**
 *
 * @author Milky
 */
public class Commentaire {
    
    private String numeroCommande;
    private int idClient;
    private String isbnLivre;
    private String texteCommentaire;
    private String dateCommentaire;
    private int moderationCommentaire; //0 Inactif - 1 Actif

    public Commentaire() {
    }

    public Commentaire(String numeroCommande, int idClient, String isbnLivre, String texteCommentaire, String dateCommentaire, int moderationCommentaire) {
        this.numeroCommande = numeroCommande;
        this.idClient = idClient;
        this.isbnLivre = isbnLivre;
        this.texteCommentaire = texteCommentaire;
        this.dateCommentaire = dateCommentaire;
        this.moderationCommentaire = moderationCommentaire;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getIsbnLivre() {
        return isbnLivre;
    }

    public void setIsbnLivre(String isbnLivre) {
        this.isbnLivre = isbnLivre;
    }

    public String getTexteCommentaire() {
        return texteCommentaire;
    }

    public void setTexteCommentaire(String texteCommentaire) {
        this.texteCommentaire = texteCommentaire;
    }

    public String getDateCommentaire() {
        return dateCommentaire;
    }

    public void setDateCommentaire(String dateCommentaire) {
        this.dateCommentaire = dateCommentaire;
    }

    public int getModerationCommentaire() {
        return moderationCommentaire;
    }

    public void setModerationCommentaire(int moderationCommentaire) {
        this.moderationCommentaire = moderationCommentaire;
    }
    
    
    
}
