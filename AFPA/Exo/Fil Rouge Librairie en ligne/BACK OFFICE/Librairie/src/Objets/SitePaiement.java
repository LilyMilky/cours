package Objets;

/**
 *
 * @author Milky
 */
public class SitePaiement {
    
    private int idSitePaiement;
    private String nomSitePaiement;
    private String lienSitePaiement;

    public SitePaiement() {
    }

    public SitePaiement(int idSitePaiement, String nomSitePaiement, String lienSitePaiement) {
        this.idSitePaiement = idSitePaiement;
        this.nomSitePaiement = nomSitePaiement;
        this.lienSitePaiement = lienSitePaiement;
    }

    public int getIdSitePaiement() {
        return idSitePaiement;
    }

    public void setIdSitePaiement(int idSitePaiement) {
        this.idSitePaiement = idSitePaiement;
    }

    public String getNomSitePaiement() {
        return nomSitePaiement;
    }

    public void setNomSitePaiement(String nomSitePaiement) {
        this.nomSitePaiement = nomSitePaiement;
    }

    public String getLienSitePaiement() {
        return lienSitePaiement;
    }

    public void setLienSitePaiement(String lienSitePaiement) {
        this.lienSitePaiement = lienSitePaiement;
    }

    @Override
    public String toString() {
        return nomSitePaiement;
    }
    
    
    
}
