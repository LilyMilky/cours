package Objets;

/**
 *
 * @author Milky
 */
public class Employe {
    
    private int idEmploye;
    private String nomEmploye;
    private String prenomEmploye;
    private String emailEmploye;
    private String telEmploye;

    public Employe() {
    }

    public Employe(int idEmploye, String nomEmploye, String prenomEmploye, String emailEmploye, String telEmploye) {
        this.idEmploye = idEmploye;
        this.nomEmploye = nomEmploye;
        this.prenomEmploye = prenomEmploye;
        this.emailEmploye = emailEmploye;
        this.telEmploye = telEmploye;
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public String getPrenomEmploye() {
        return prenomEmploye;
    }

    public void setPrenomEmploye(String prenomEmploye) {
        this.prenomEmploye = prenomEmploye;
    }

    public String getEmailEmploye() {
        return emailEmploye;
    }

    public void setEmailEmploye(String emailEmploye) {
        this.emailEmploye = emailEmploye;
    }

    public String getTelEmploye() {
        return telEmploye;
    }

    public void setTelEmploye(String telEmploye) {
        this.telEmploye = telEmploye;
    }
    
    
    
}
