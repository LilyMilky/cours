package Interface;

public class jFrameMenu extends javax.swing.JFrame {

    static public int typeAcces;
    
    public jFrameMenu() {
        initComponents();
        
        //Si compte non Super Admin, les acces ne sont pas accesible
        if(typeAcces != 4)
            jMenuAcces.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuProduit = new javax.swing.JMenu();
        jMenuLivre = new javax.swing.JMenu();
        jMenuItemAjouterLivre = new javax.swing.JMenuItem();
        jMenuItemRechercherLivre = new javax.swing.JMenuItem();
        jMenuAuteur = new javax.swing.JMenu();
        jMenuItemAjouterAuteur = new javax.swing.JMenuItem();
        jMenuItemRechercherAuteur = new javax.swing.JMenuItem();
        jMenuEditeur = new javax.swing.JMenu();
        jMenuItemAjouterEditeur = new javax.swing.JMenuItem();
        jMenuItemRechercherEditeur = new javax.swing.JMenuItem();
        jMenuFormat = new javax.swing.JMenu();
        jMenuItemAjouterFormat = new javax.swing.JMenuItem();
        jMenuItemRechercherFormat = new javax.swing.JMenuItem();
        jMenuMotCle = new javax.swing.JMenu();
        jMenuItemAjouterMotCle = new javax.swing.JMenuItem();
        jMenuItemRechercherMotCle = new javax.swing.JMenuItem();
        jMenuTheme = new javax.swing.JMenu();
        jMenuItemAjouterTheme = new javax.swing.JMenuItem();
        jMenuItemRechercherTheme = new javax.swing.JMenuItem();
        jMenuVente = new javax.swing.JMenu();
        jMenuItemClient = new javax.swing.JMenuItem();
        jMenuItemRechercherCommande = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItemAjouterEtat = new javax.swing.JMenuItem();
        jMenuItemRechercherEtat = new javax.swing.JMenuItem();
        jMenuItemCommentaire = new javax.swing.JMenuItem();
        jMenuGestion = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItemAjouterRubrique = new javax.swing.JMenuItem();
        jMenuItemRechercherRubrique = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenuItemAjouterSitePaiement = new javax.swing.JMenuItem();
        jMenuItemRechercherSitePaiement = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItemAjouterTransporteur = new javax.swing.JMenuItem();
        jMenuItemRechercherTransporteur = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemAjouterEmploye = new javax.swing.JMenuItem();
        jMenuItemRechercherEmploye = new javax.swing.JMenuItem();
        jMenuAcces = new javax.swing.JMenu();
        jMenuItemAjouterAcces = new javax.swing.JMenuItem();
        jMenuItemRechercherAcces = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestionnaire de la librairie");

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1008, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 721, Short.MAX_VALUE)
        );

        jMenuProduit.setText("Produits");

        jMenuLivre.setText("Livre");

        jMenuItemAjouterLivre.setText("Ajouter");
        jMenuLivre.add(jMenuItemAjouterLivre);

        jMenuItemRechercherLivre.setText("Rechercher");
        jMenuLivre.add(jMenuItemRechercherLivre);

        jMenuProduit.add(jMenuLivre);

        jMenuAuteur.setText("Auteur");

        jMenuItemAjouterAuteur.setText("Ajouter");
        jMenuAuteur.add(jMenuItemAjouterAuteur);

        jMenuItemRechercherAuteur.setText("Rechercher");
        jMenuAuteur.add(jMenuItemRechercherAuteur);

        jMenuProduit.add(jMenuAuteur);

        jMenuEditeur.setText("Editeur");

        jMenuItemAjouterEditeur.setText("Ajouter");
        jMenuEditeur.add(jMenuItemAjouterEditeur);

        jMenuItemRechercherEditeur.setText("Rechercher");
        jMenuEditeur.add(jMenuItemRechercherEditeur);

        jMenuProduit.add(jMenuEditeur);

        jMenuFormat.setText("Format");

        jMenuItemAjouterFormat.setText("Ajouter");
        jMenuFormat.add(jMenuItemAjouterFormat);

        jMenuItemRechercherFormat.setText("Rechercher");
        jMenuFormat.add(jMenuItemRechercherFormat);

        jMenuProduit.add(jMenuFormat);

        jMenuMotCle.setText("Mot clé");

        jMenuItemAjouterMotCle.setText("Ajouter");
        jMenuMotCle.add(jMenuItemAjouterMotCle);

        jMenuItemRechercherMotCle.setText("Rechercher");
        jMenuMotCle.add(jMenuItemRechercherMotCle);

        jMenuProduit.add(jMenuMotCle);

        jMenuTheme.setText("Thème");

        jMenuItemAjouterTheme.setText("Ajouter");
        jMenuTheme.add(jMenuItemAjouterTheme);

        jMenuItemRechercherTheme.setText("Rechercher");
        jMenuTheme.add(jMenuItemRechercherTheme);

        jMenuProduit.add(jMenuTheme);

        jMenuBar1.add(jMenuProduit);

        jMenuVente.setText("Vente");

        jMenuItemClient.setText("Client");
        jMenuVente.add(jMenuItemClient);

        jMenuItemRechercherCommande.setText("Commande");
        jMenuItemRechercherCommande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRechercherCommandeActionPerformed(evt);
            }
        });
        jMenuVente.add(jMenuItemRechercherCommande);

        jMenu7.setText("Etat ");

        jMenuItemAjouterEtat.setText("Ajouter");
        jMenu7.add(jMenuItemAjouterEtat);

        jMenuItemRechercherEtat.setText("Rechercher");
        jMenu7.add(jMenuItemRechercherEtat);

        jMenuVente.add(jMenu7);

        jMenuItemCommentaire.setText("Commentaire");
        jMenuVente.add(jMenuItemCommentaire);

        jMenuBar1.add(jMenuVente);

        jMenuGestion.setText("Gestion");

        jMenu2.setText("Rubrique");

        jMenuItemAjouterRubrique.setText("Ajouter");
        jMenuItemAjouterRubrique.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAjouterRubriqueActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemAjouterRubrique);

        jMenuItemRechercherRubrique.setText("Rechercher");
        jMenuItemRechercherRubrique.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRechercherRubriqueActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemRechercherRubrique);

        jMenuGestion.add(jMenu2);

        jMenu8.setText("Site de paiement");

        jMenuItemAjouterSitePaiement.setText("Ajouter");
        jMenu8.add(jMenuItemAjouterSitePaiement);

        jMenuItemRechercherSitePaiement.setText("Rechercher");
        jMenu8.add(jMenuItemRechercherSitePaiement);

        jMenuGestion.add(jMenu8);

        jMenu3.setText("Transporteur");

        jMenuItemAjouterTransporteur.setText("Ajouter");
        jMenuItemAjouterTransporteur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAjouterTransporteurActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItemAjouterTransporteur);

        jMenuItemRechercherTransporteur.setText("Rechercher");
        jMenuItemRechercherTransporteur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRechercherTransporteurActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItemRechercherTransporteur);

        jMenuGestion.add(jMenu3);

        jMenu1.setText("Employe");

        jMenuItemAjouterEmploye.setText("Ajouter");
        jMenu1.add(jMenuItemAjouterEmploye);

        jMenuItemRechercherEmploye.setText("Rechercher");
        jMenu1.add(jMenuItemRechercherEmploye);

        jMenuGestion.add(jMenu1);

        jMenuAcces.setText("Acces");

        jMenuItemAjouterAcces.setText("Ajouter");
        jMenuItemAjouterAcces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAjouterAccesActionPerformed(evt);
            }
        });
        jMenuAcces.add(jMenuItemAjouterAcces);

        jMenuItemRechercherAcces.setText("Rechercher");
        jMenuItemRechercherAcces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRechercherAccesActionPerformed(evt);
            }
        });
        jMenuAcces.add(jMenuItemRechercherAcces);

        jMenuGestion.add(jMenuAcces);

        jMenuBar1.add(jMenuGestion);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );

        setSize(new java.awt.Dimension(1024, 780));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemAjouterTransporteurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAjouterTransporteurActionPerformed
        AjoutTransporteur addTrans = new AjoutTransporteur();
        addTrans.setVisible(true);
        jDesktopPane1.add(addTrans);
        addTrans.toFront();
    }//GEN-LAST:event_jMenuItemAjouterTransporteurActionPerformed

    private void jMenuItemRechercherTransporteurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRechercherTransporteurActionPerformed
        RechercherTransporteur findTrans= new RechercherTransporteur();
        findTrans.setVisible(true);
        jDesktopPane1.add(findTrans);
        findTrans.toFront();
    }//GEN-LAST:event_jMenuItemRechercherTransporteurActionPerformed

    private void jMenuItemAjouterRubriqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAjouterRubriqueActionPerformed
        AjouterRubrique addrub = new AjouterRubrique();
        addrub.setVisible(true);
        jDesktopPane1.add(addrub);
        addrub.toFront();
    }//GEN-LAST:event_jMenuItemAjouterRubriqueActionPerformed

    private void jMenuItemRechercherRubriqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRechercherRubriqueActionPerformed
        RechercherRubrique findrub = new RechercherRubrique();
        findrub.setVisible(true);
        jDesktopPane1.add(findrub);
        findrub.toFront();
    }//GEN-LAST:event_jMenuItemRechercherRubriqueActionPerformed

    private void jMenuItemAjouterAccesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAjouterAccesActionPerformed
        AjouterAcces addAcces = new AjouterAcces();
        addAcces.setVisible(true);
        jDesktopPane1.add(addAcces);
        addAcces.toFront();
    }//GEN-LAST:event_jMenuItemAjouterAccesActionPerformed

    private void jMenuItemRechercherAccesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRechercherAccesActionPerformed
        RechercherAcces findAcces = new RechercherAcces();
        findAcces.setVisible(true);
        jDesktopPane1.add(findAcces);
        findAcces.toFront();
    }//GEN-LAST:event_jMenuItemRechercherAccesActionPerformed

    private void jMenuItemRechercherCommandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRechercherCommandeActionPerformed
        RechercherCommande findcom = new RechercherCommande();
        findcom.setVisible(true);
        jDesktopPane1.add(findcom);
        findcom.toFront();
    }//GEN-LAST:event_jMenuItemRechercherCommandeActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jFrameMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenuAcces;
    private javax.swing.JMenu jMenuAuteur;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuEditeur;
    private javax.swing.JMenu jMenuFormat;
    private javax.swing.JMenu jMenuGestion;
    private javax.swing.JMenuItem jMenuItemAjouterAcces;
    private javax.swing.JMenuItem jMenuItemAjouterAuteur;
    private javax.swing.JMenuItem jMenuItemAjouterEditeur;
    private javax.swing.JMenuItem jMenuItemAjouterEmploye;
    private javax.swing.JMenuItem jMenuItemAjouterEtat;
    private javax.swing.JMenuItem jMenuItemAjouterFormat;
    private javax.swing.JMenuItem jMenuItemAjouterLivre;
    private javax.swing.JMenuItem jMenuItemAjouterMotCle;
    private javax.swing.JMenuItem jMenuItemAjouterRubrique;
    private javax.swing.JMenuItem jMenuItemAjouterSitePaiement;
    private javax.swing.JMenuItem jMenuItemAjouterTheme;
    private javax.swing.JMenuItem jMenuItemAjouterTransporteur;
    private javax.swing.JMenuItem jMenuItemClient;
    private javax.swing.JMenuItem jMenuItemCommentaire;
    private javax.swing.JMenuItem jMenuItemRechercherAcces;
    private javax.swing.JMenuItem jMenuItemRechercherAuteur;
    private javax.swing.JMenuItem jMenuItemRechercherCommande;
    private javax.swing.JMenuItem jMenuItemRechercherEditeur;
    private javax.swing.JMenuItem jMenuItemRechercherEmploye;
    private javax.swing.JMenuItem jMenuItemRechercherEtat;
    private javax.swing.JMenuItem jMenuItemRechercherFormat;
    private javax.swing.JMenuItem jMenuItemRechercherLivre;
    private javax.swing.JMenuItem jMenuItemRechercherMotCle;
    private javax.swing.JMenuItem jMenuItemRechercherRubrique;
    private javax.swing.JMenuItem jMenuItemRechercherSitePaiement;
    private javax.swing.JMenuItem jMenuItemRechercherTheme;
    private javax.swing.JMenuItem jMenuItemRechercherTransporteur;
    private javax.swing.JMenu jMenuLivre;
    private javax.swing.JMenu jMenuMotCle;
    private javax.swing.JMenu jMenuProduit;
    private javax.swing.JMenu jMenuTheme;
    private javax.swing.JMenu jMenuVente;
    // End of variables declaration//GEN-END:variables
}
