package Interface;

import BDD.*;
import Objets.Rubrique;
import Classes.*;
import java.util.Date;
import java.util.Vector;

public class RechercherRubrique extends javax.swing.JInternalFrame {

    //Modele de la jTable
    private RubriqueModel modele = new RubriqueModel();
    
    public RechercherRubrique() {
        Vector v = new Vector();
        Connexion con = new Connexion();
        con.connexion();
        RubriqueBDD rubbdd = new RubriqueBDD();
        
        Date date = new Date();
        //Recupere toutes les rubriques qui ne sont pas encore passées    
        v = rubbdd.rechercheBddDate(ConvertDate.convertDateSQL(date), con);
        
        //Et les ajoute dans la jTable
        for(int j = 0; j < v.size(); j++)
        {
            Rubrique rub = (Rubrique) v.get(j);
           
            modele.addLine(rub);
        }
        
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonAnnuler = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableRubrique = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldRechercheNom = new javax.swing.JTextField();

        setTitle("Rechercher une rubrique");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jButtonAnnuler.setText("Quitter");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonAnnuler);
        jButtonAnnuler.setBounds(340, 320, 130, 30);

        jTableRubrique.setModel(modele);
        jScrollPane1.setViewportView(jTableRubrique);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(0, 60, 810, 230);

        jLabel1.setText("Recherche nom :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(140, 10, 100, 30);

        jTextFieldRechercheNom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldRechercheNomKeyReleased(evt);
            }
        });
        jPanel1.add(jTextFieldRechercheNom);
        jTextFieldRechercheNom.setBounds(270, 10, 310, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 820, 370);

        setBounds(0, 0, 826, 400);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    //Permet l'affichage de toutes les rubriques commencant par le caractere selectionné
    private void jTextFieldRechercheNomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldRechercheNomKeyReleased
        Vector v = new Vector();
        RubriqueBDD rubbdd = new RubriqueBDD();
        Connexion con = new Connexion();
        con.connexion();
        
        modele.removeAll();
        
        v = rubbdd.rechercheBddLike(jTextFieldRechercheNom.getText(), con);
        
        //Insertion jTable
        if(v.size() > 0)
        {
            jTableRubrique.setVisible(true);
            for(int j = 0; j < v.size(); j++)
            {
                Rubrique rub = (Rubrique) v.get(j);

                modele.addLine(rub);
            }
        }
        //Permet de ne rien afficher quand la jTable est vide.
        else
            jTableRubrique.setVisible(false);
        
        con.close();
    }//GEN-LAST:event_jTextFieldRechercheNomKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableRubrique;
    private javax.swing.JTextField jTextFieldRechercheNom;
    // End of variables declaration//GEN-END:variables
}
