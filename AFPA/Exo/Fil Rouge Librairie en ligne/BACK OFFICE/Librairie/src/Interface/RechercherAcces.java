package Interface;

import BDD.AccesBDD;
import BDD.Connexion;
import Objets.Acces;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class RechercherAcces extends javax.swing.JInternalFrame {

    public RechercherAcces() {
        initComponents();
    }

    //Permet l'initialisation de la combobox des Acces
    private DefaultComboBoxModel initModelAcces()
    {
        return new DefaultComboBoxModel(initAcces());
    }
    
    //Recupere les acces enregistré en base
    private Vector initAcces()
    {
        Connexion con = new Connexion();
        con.connexion();
        AccesBDD accesbdd = new AccesBDD();
        
        Vector v = new Vector();
        
        v = accesbdd.getAllAcces(con);
        
        con.close();
        return v;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldDesignationAcces = new javax.swing.JTextField();
        jTextFieldTypeAcces = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jButtonModifier = new javax.swing.JButton();
        jButtonAnnuler = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setTitle("Rechercher Acces");
        getContentPane().setLayout(null);

        jPanel2.setLayout(null);

        jLabel1.setText("Acces :");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(22, 20, 90, 30);

        jLabel2.setText("Type Acces :");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 90, 100, 30);

        jLabel3.setText("Designation Acces : ");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 150, 130, 30);

        jTextFieldDesignationAcces.setEnabled(false);
        jPanel2.add(jTextFieldDesignationAcces);
        jTextFieldDesignationAcces.setBounds(170, 150, 200, 30);

        jTextFieldTypeAcces.setEnabled(false);
        jPanel2.add(jTextFieldTypeAcces);
        jTextFieldTypeAcces.setBounds(170, 90, 200, 30);

        jComboBox1.setModel(initModelAcces());
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(jComboBox1);
        jComboBox1.setBounds(170, 20, 200, 30);

        jButtonModifier.setText("Modifier");
        jButtonModifier.setEnabled(false);
        jButtonModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonModifier);
        jButtonModifier.setBounds(60, 220, 90, 30);

        jButtonAnnuler.setText("Annuler");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonAnnuler);
        jButtonAnnuler.setBounds(223, 220, 90, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 400, 280);

        setBounds(0, 0, 410, 307);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        //Lorsque l'utilisateur a selectionné un acces dans la combobox, les textFields + le bouton modifier
        jTextFieldDesignationAcces.setEnabled(true);
        jTextFieldTypeAcces.setEnabled(true);
        jButtonModifier.setEnabled(true);
        
        Acces acc = (Acces)jComboBox1.getSelectedItem();
        
        jTextFieldDesignationAcces.setText(acc.getDesignationAcces());
        jTextFieldTypeAcces.setText(Integer.toString(acc.getTypeAcces()));
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButtonModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierActionPerformed
        Acces acc = (Acces)jComboBox1.getSelectedItem();
        AccesBDD accbdd = new AccesBDD();
        Connexion con = new Connexion();
        con.connexion();
        
        int result;
        
        //Try catch pour verifier que seulement des chiffres ont été saisie pour le type acces
        try
        {
            acc.setTypeAcces(Integer.parseInt(jTextFieldTypeAcces.getText()));
            acc.setDesignationAcces(jTextFieldDesignationAcces.getText());
        
            result = accbdd.updateAcces(acc, con);

            if(result > 0)
            {
                JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
            }
        }
        catch(NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour le type d'acces", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
        }
      
        con.close();
    }//GEN-LAST:event_jButtonModifierActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JButton jButtonModifier;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextFieldDesignationAcces;
    private javax.swing.JTextField jTextFieldTypeAcces;
    // End of variables declaration//GEN-END:variables
}
