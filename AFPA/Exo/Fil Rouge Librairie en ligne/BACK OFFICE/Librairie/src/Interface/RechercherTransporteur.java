package Interface;

import BDD.Connexion;
import BDD.TransporteurBDD;
import Objets.Transporteur;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class RechercherTransporteur extends javax.swing.JInternalFrame {

    public RechercherTransporteur() {
        initComponents();
    }
    
    //Initialise la comboBox contenant les transporteurs
    private DefaultComboBoxModel initModelTransporteur()
    {
        return new DefaultComboBoxModel(initTransporteur());
    }

    private Vector initTransporteur()
    {
        Connexion con = new Connexion();
        con.connexion();
        TransporteurBDD transbdd = new TransporteurBDD();
        
        Vector v = new Vector();
        
        v = transbdd.getAllTransporteur(con);
        
        con.close();
        return v;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jComboBoxTransporteur = new javax.swing.JComboBox();
        jTextFieldNomTransporteur = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldTelTransporteur = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldAvisTransporteur = new javax.swing.JTextField();
        jButtonModifier = new javax.swing.JButton();
        jButtonQuitter = new javax.swing.JButton();
        jRadioButtonHide = new javax.swing.JRadioButton();
        jRadioButtonNotHide = new javax.swing.JRadioButton();

        setTitle("Recherche Transporteur");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jLabel1.setText("Transporteur :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 10, 110, 30);

        jComboBoxTransporteur.setModel(initModelTransporteur());
        jComboBoxTransporteur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTransporteurActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBoxTransporteur);
        jComboBoxTransporteur.setBounds(140, 20, 400, 20);

        jTextFieldNomTransporteur.setEnabled(false);
        jPanel1.add(jTextFieldNomTransporteur);
        jTextFieldNomTransporteur.setBounds(180, 60, 340, 30);

        jLabel2.setText("Nom Transporteur :");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 60, 150, 20);

        jLabel3.setText("Tel Transporteur :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 120, 110, 20);

        jTextFieldTelTransporteur.setEnabled(false);
        jPanel1.add(jTextFieldTelTransporteur);
        jTextFieldTelTransporteur.setBounds(180, 120, 340, 30);

        jLabel4.setText("Avis Transporteur : ");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 180, 110, 30);

        jTextFieldAvisTransporteur.setEnabled(false);
        jPanel1.add(jTextFieldAvisTransporteur);
        jTextFieldAvisTransporteur.setBounds(180, 180, 340, 30);

        jButtonModifier.setText("Modifier");
        jButtonModifier.setEnabled(false);
        jButtonModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonModifier);
        jButtonModifier.setBounds(20, 260, 130, 30);

        jButtonQuitter.setText("Quitter");
        jButtonQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitterActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonQuitter);
        jButtonQuitter.setBounds(410, 260, 130, 30);

        buttonGroup1.add(jRadioButtonHide);
        jRadioButtonHide.setText("Hide");
        jRadioButtonHide.setEnabled(false);
        jPanel1.add(jRadioButtonHide);
        jRadioButtonHide.setBounds(130, 220, 93, 23);

        buttonGroup1.add(jRadioButtonNotHide);
        jRadioButtonNotHide.setText("NotHide");
        jRadioButtonNotHide.setEnabled(false);
        jPanel1.add(jRadioButtonNotHide);
        jRadioButtonNotHide.setBounds(350, 220, 63, 23);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 560, 320);

        setBounds(0, 0, 575, 344);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitterActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitterActionPerformed

    //Rend disponible les champs une fois la selection d'un transporteur dans la comboBox et l'affiche
    private void jComboBoxTransporteurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTransporteurActionPerformed
        jTextFieldAvisTransporteur.setEnabled(true);
        jTextFieldNomTransporteur.setEnabled(true);
        jTextFieldTelTransporteur.setEnabled(true);
        jButtonModifier.setEnabled(true);
        jRadioButtonHide.setEnabled(true);
        jRadioButtonNotHide.setEnabled(true);

        Transporteur trans = (Transporteur)jComboBoxTransporteur.getSelectedItem();

        jTextFieldNomTransporteur.setText(trans.getNomTransporteur());
        jTextFieldAvisTransporteur.setText(trans.getAvisTransporteur());
        jTextFieldTelTransporteur.setText(trans.getTelTransporteur());
        
        if(trans.getHideTransporteur() == 1)
            jRadioButtonNotHide.setSelected(true);
        else
            jRadioButtonHide.setSelected(true);
    }//GEN-LAST:event_jComboBoxTransporteurActionPerformed

    private void jButtonModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        TransporteurBDD transbdd = new TransporteurBDD();
        
        Transporteur trans = (Transporteur)jComboBoxTransporteur.getSelectedItem();
        
        trans.setAvisTransporteur(jTextFieldAvisTransporteur.getText());
        trans.setNomTransporteur(jTextFieldNomTransporteur.getText());
        trans.setTelTransporteur(jTextFieldTelTransporteur.getText());
        
        if(jRadioButtonHide.isSelected())
            //"Supprimé" --- Non visible
            trans.setHideTransporteur(0);
        else
            //Visible
            trans.setHideTransporteur(1);

        int result = transbdd.updateTransporteur(trans, con);
        
        if(result > 0)
        {
            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
            //Permet la mise a jour de la comboBox en rappelant le init.
            new RechercherTransporteur().setVisible(true);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
        }
        
        con.close();
    }//GEN-LAST:event_jButtonModifierActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonModifier;
    private javax.swing.JButton jButtonQuitter;
    private javax.swing.JComboBox jComboBoxTransporteur;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButtonHide;
    private javax.swing.JRadioButton jRadioButtonNotHide;
    private javax.swing.JTextField jTextFieldAvisTransporteur;
    private javax.swing.JTextField jTextFieldNomTransporteur;
    private javax.swing.JTextField jTextFieldTelTransporteur;
    // End of variables declaration//GEN-END:variables
}
