package Classes;

import BDD.Connexion;
import BDD.RubriqueBDD;
import Objets.Rubrique;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class RubriqueModel extends AbstractTableModel {

    private List<Rubrique> rubriques = new ArrayList<Rubrique>();

    //Titre
    private final String[] entetes = {"Nom Rubrique", "Date Debut", "Date Fin", "Avis", "Taux Promotion"};

    public RubriqueModel() {
        super();
    }

    //Permet l'edition de toutes les cellules
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    //Gere le changement de valeur dans la jTable, modifie dans l'objet et en base.
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        int result;
        Connexion con = new Connexion();
        con.connexion();
        RubriqueBDD rubbdd = new RubriqueBDD();

        if (aValue != null) {
            Rubrique rub = rubriques.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    rub.setNomRubrique((String) aValue);
                    result = rubbdd.updateNomRubrique(rub, con);

                    if (result > 0) {
                        JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 1:
                    rub.setDateDebutRubrique((String) aValue);
                    result = rubbdd.updateDateRubrique(rub, con);

                    if (result > 0) {
                        JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 2:
                    rub.setDateFinRubrique((String) aValue);
                    result = rubbdd.updateDateFinRubrique(rub, con);

                    if (result > 0) {
                        JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 3:
                    rub.setAvisRubrique((String) aValue);
                    result = rubbdd.updateAvisRubrique(rub, con);

                    if (result > 0) {
                        JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 4:
                    rub.setTauxPromotionRubrique(Integer.parseInt(aValue.toString()));
                    result = rubbdd.updateTauxPromotionRubrique(rub, con);

                    if (result > 0) {
                        JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
            }
        }

        con.close();
    }

    //Vide la liste d'objet(donc la jTable)
    public void removeAll() {
        rubriques.clear();
    }

    //Ajout d'une ligne dans la jTable
    public void addLine(Rubrique rub) {
        rubriques.add(rub);

        fireTableRowsInserted(rubriques.size() - 1, rubriques.size() - 1);
    }

    //NB ligne
    public int getRowCount() {
        return rubriques.size();
    }

    //NB Colonne
    public int getColumnCount() {
        return entetes.length;
    }

    //Nom des colonnes
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    //Recupere la valeur a afficher dans la case.
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return rubriques.get(rowIndex).getNomRubrique();
            case 1:
                return rubriques.get(rowIndex).getDateDebutRubrique();
            case 2:
                return rubriques.get(rowIndex).getDateFinRubrique();
            case 3:
                return rubriques.get(rowIndex).getAvisRubrique();
            case 4:
                return rubriques.get(rowIndex).getTauxPromotionRubrique();
            default:
                return null; //Ne devrait jamais arriver
        }
    }
}
