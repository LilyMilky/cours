package Classes;

import java.util.Date;

public class ConvertDate {
    
    //Convertit une date java.util en date java.sql puis String
    public static String convertDateSQL(Date date)
    {
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        String dateSql = sqlDate.toString().substring(8, 10) + "-" + sqlDate.toString().substring(5, 8) + sqlDate.toString().substring(0, 4);
        
        return dateSql;
    }
    
    //Mise en forme de la date reçu par sql
    public static String convertDate(String date)
    {
        date = date.substring(8, 10) + "-" + date.substring(5, 8) + date.substring(0, 4);
        
        return date;
    }
    
    //Mise en forme de la date pour sql
    public static String convertDateAme(String date)
    {
        date = date.substring(6, 10) + "-" + date.substring(3, 6) + date.substring(0, 2);
        
        return date;
    }
    
}
