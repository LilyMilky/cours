package Classes;

import BDD.*;
import Objets.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class ContenuCommandeModel extends AbstractTableModel {

    private List<Contenant> contenants = new ArrayList<Contenant>();
    
    private final String[] entetes = {"Quantité", "Prix Vente", "TVA", "Promotion"};
    
    //Vide la liste d'objet(donc la jTable)
    public void removeAll() {
        contenants.clear();
    }
    //Permet l'edition de toutes les cellules
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    //Ajout d'une ligne dans la jTable
    public void addLine(Contenant cont) {
        contenants.add(cont);

        fireTableRowsInserted(contenants.size() - 1, contenants.size() - 1);
    }
    
    //Gere le changement de valeur dans la jTable, modifie dans l'objet et en base.
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        int result;
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD combdd = new CommandeBDD();

        if (aValue != null) {
            Contenant cont = contenants.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    try
                    {
                        cont.setNbLivre(Integer.parseInt(aValue.toString()));
                        result = combdd.updateNombreLivre(cont, con);

                        if (result > 0) {
                            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                        }
                    }catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour le nombre de livre", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 1:
                    try
                    {
                        cont.setPrixVente(Float.valueOf(aValue.toString().replace(',', '.')));
                        result = combdd.updatePrixVente(cont, con);

                        if (result > 0) {
                            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                        }
                    }catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour le prix de vente", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 2:
                    try
                    {
                        cont.setTVA(Float.valueOf(aValue.toString().replace(',', '.')));
                        result = combdd.updateTVA(cont, con);

                        if (result > 0) {
                            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                        }
                    }catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour la TVA", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                case 3:
                    try
                    {
                        cont.setPromotion(Integer.parseInt(aValue.toString()));
                        result = combdd.updatePromotion(cont, con);

                        if (result > 0) {
                            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
                        }
                    }catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour la promotion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
            }
        }

        con.close();
    }
    //NB ligne
    @Override
    public int getRowCount() {
        return contenants.size();
    }

    //NB Colonne
    @Override
    public int getColumnCount() {
        return entetes.length;
    }

    //Nom des colonnes
    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }
    
    //Recupere la valeur a afficher dans la case.
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return contenants.get(rowIndex).getNbLivre();
            case 1:
                return contenants.get(rowIndex).getPrixVente();
            case 2:
                return contenants.get(rowIndex).getTVA();
            case 3:
                return contenants.get(rowIndex).getPromotion();
            default:
                return null; //Ne devrait jamais arriver
        }
    }
    
}
