package BDD;

import Objets.Acces;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class AccesBDD {
    
    //Permet l'ajout d'un acces en base
    public int addAcces(int numAcces, String nomAcces, Connexion con)
    {
        int result = 0;
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "insert into acces (typeAcces, designationAcces) values ("
                    + numAcces +",'" + nomAcces + "')";

            //System.out.println(query);
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
            
            
        } catch (SQLException ex) {
            //System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
        }
         
         return result;
    }
    
    
    //Recuperation des acces pour la comboBox
    public Vector getAllAcces(Connexion con)
    {
        Vector v = new Vector();
        
        String query = "SELECT * FROM acces";
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                v.add(new Acces(rs.getInt("idAcces"), rs.getInt("typeAcces"), rs.getString("designationAcces")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        return v;
    }
    
    
    //Mise a jour d'un transporteur via l'id
    public int updateAcces(Acces acc, Connexion con)
    {
        int result = 0;
        
        String query = "Update acces set typeAcces = " + acc.getTypeAcces()
                + ", designationAcces = '" + acc.getDesignationAcces()
                + "' where idAcces = " + acc.getIdAcces();
        
        try
        {
            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(query);
            
            stmt.close();
        }
        catch(SQLException ex)
        {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        return result;
    }
    
}
