package Objets;

/**
 *
 * @author Milky
 */
public class Commande {
    
    private String numeroCommande;
    private int idSitePaiement;
    private String nomSitePaiement;
    private int idClient;
    private String identifiantCompte;
    private int idEtat;
    private String nomEtatCommande;
    private int idLivraisonCoordonnees;
    private String adresseLivraison;
    private int idTransporteur;
    private String nomTransporteur;
    private int idFacturationCommande;
    private String adresseFacturation;
    private String dateCommande;
    private String modePaiement;        //Always Carte Credit
    private String dateLivraisonCommande;
    private String ipCommande;
    private String referencePaiement;
    private String referenceColis;

    public Commande() {
    }

    public String getNomSitePaiement() {
        return nomSitePaiement;
    }

    public void setNomSitePaiement(String nomSitePaiement) {
        this.nomSitePaiement = nomSitePaiement;
    }

    public String getIdentifiantCompte() {
        return identifiantCompte;
    }

    public void setIdentifiantCompte(String identifiantCompte) {
        this.identifiantCompte = identifiantCompte;
    }

    public String getNomEtatCommande() {
        return nomEtatCommande;
    }

    public void setNomEtatCommande(String nomEtatCommande) {
        this.nomEtatCommande = nomEtatCommande;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    public String getNomTransporteur() {
        return nomTransporteur;
    }

    public void setNomTransporteur(String nomTransporteur) {
        this.nomTransporteur = nomTransporteur;
    }

    public String getAdresseFacturation() {
        return adresseFacturation;
    }

    public void setAdresseFacturation(String adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }

    public Commande(String numeroCommande, int idSitePaiement, String nomSitePaiement, int idClient, String identifiantCompte, int idEtat, String nomEtatCommande, int idLivraisonCoordonnees, String adresseLivraison, int idTransporteur, String nomTransporteur, int idFacturationCommande, String adresseFacturation, String dateCommande, String modePaiement, String dateLivraisonCommande, String ipCommande, String referencePaiement, String referenceColis) {
        this.numeroCommande = numeroCommande;
        this.idSitePaiement = idSitePaiement;
        this.nomSitePaiement = nomSitePaiement;
        this.idClient = idClient;
        this.identifiantCompte = identifiantCompte;
        this.idEtat = idEtat;
        this.nomEtatCommande = nomEtatCommande;
        this.idLivraisonCoordonnees = idLivraisonCoordonnees;
        this.adresseLivraison = adresseLivraison;
        this.idTransporteur = idTransporteur;
        this.nomTransporteur = nomTransporteur;
        this.idFacturationCommande = idFacturationCommande;
        this.adresseFacturation = adresseFacturation;
        this.dateCommande = dateCommande;
        this.modePaiement = modePaiement;
        this.dateLivraisonCommande = dateLivraisonCommande;
        this.ipCommande = ipCommande;
        this.referencePaiement = referencePaiement;
        this.referenceColis = referenceColis;
    }


    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public int getIdSitePaiement() {
        return idSitePaiement;
    }

    public void setIdSitePaiement(int idSitePaiement) {
        this.idSitePaiement = idSitePaiement;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdEtat() {
        return idEtat;
    }

    public void setIdEtat(int idEtat) {
        this.idEtat = idEtat;
    }

    public int getIdLivraisonCoordonnees() {
        return idLivraisonCoordonnees;
    }

    public void setIdLivraisonCoordonnees(int idLivraisonCoordonnees) {
        this.idLivraisonCoordonnees = idLivraisonCoordonnees;
    }

    public int getIdTransporteur() {
        return idTransporteur;
    }

    public void setIdTransporteur(int idTransporteur) {
        this.idTransporteur = idTransporteur;
    }

    public int getIdFacturationCommande() {
        return idFacturationCommande;
    }

    public void setIdFacturationCommande(int idFacturationCommande) {
        this.idFacturationCommande = idFacturationCommande;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    public String getDateLivraisonCommande() {
        return dateLivraisonCommande;
    }

    public void setDateLivraisonCommande(String dateLivraisonCommande) {
        this.dateLivraisonCommande = dateLivraisonCommande;
    }

    public String getIpCommande() {
        return ipCommande;
    }

    public void setIpCommande(String ipCommande) {
        this.ipCommande = ipCommande;
    }

    public String getReferencePaiement() {
        return referencePaiement;
    }

    public void setReferencePaiement(String referencePaiement) {
        this.referencePaiement = referencePaiement;
    }

    public String getReferenceColis() {
        return referenceColis;
    }

    public void setReferenceColis(String referenceColis) {
        this.referenceColis = referenceColis;
    }
    
    
    
}
