package Objets;

/**
 *
 * @author Milky
 */
public class SitePaiement {
    
    private int idSitePaiement;
    private String nomSitePaiement;
    private String lienSitePaiement;
    private int hideSitePaiement;

    public int getHideSitePaiement() {
        return hideSitePaiement;
    }

    public void setHideSitePaiement(int hideSitePaiement) {
        this.hideSitePaiement = hideSitePaiement;
    }

    public SitePaiement() {
    }

    public SitePaiement(int idSitePaiement, String nomSitePaiement, String lienSitePaiement,int hideSitePaiemnent) {
        this.idSitePaiement = idSitePaiement;
        this.nomSitePaiement = nomSitePaiement;
        this.lienSitePaiement = lienSitePaiement;
        this.hideSitePaiement=hideSitePaiemnent;
    }

    public int getIdSitePaiement() {
        return idSitePaiement;
    }

    public void setIdSitePaiement(int idSitePaiement) {
        this.idSitePaiement = idSitePaiement;
    }

    public String getNomSitePaiement() {
        return nomSitePaiement;
    }

    public void setNomSitePaiement(String nomSitePaiement) {
        this.nomSitePaiement = nomSitePaiement;
    }

    public String getLienSitePaiement() {
        return lienSitePaiement;
    }

    public void setLienSitePaiement(String lienSitePaiement) {
        this.lienSitePaiement = lienSitePaiement;
    }

    @Override
    public String toString() {
        return ""+ nomSitePaiement ;
    }
    
    
    
    
    
    
    
    
}
