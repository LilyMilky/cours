package Objets;

/**
 *
 * @author Milky
 */
public class Auteur {
    
    private int idAuteur;
    private String nomAuteur;
    private String prenomAuteur;
    private String dateNaissanceAuteur;
    private String dateDecesAuteur;
    private String avisAuteur;

    public Auteur() {
    }

    public Auteur(int idAuteur, String nomAuteur, String prenomAuteur, String dateNaissanceAuteur, String dateDecesAuteur, String avisAuteur) {
        this.idAuteur = idAuteur;
        this.nomAuteur = nomAuteur;
        this.prenomAuteur = prenomAuteur;
        this.dateNaissanceAuteur = dateNaissanceAuteur;
        this.dateDecesAuteur = dateDecesAuteur;
        this.avisAuteur = avisAuteur;
    }

    public int getIdAuteur() {
        return idAuteur;
    }

    public void setIdAuteur(int idAuteur) {
        this.idAuteur = idAuteur;
    }

    public String getNomAuteur() {
        return nomAuteur;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }

    public String getPrenomAuteur() {
        return prenomAuteur;
    }

    public void setPrenomAuteur(String prenomAuteur) {
        this.prenomAuteur = prenomAuteur;
    }

    public String getDateNaissanceAuteur() {
        return dateNaissanceAuteur;
    }

    public void setDateNaissanceAuteur(String dateNaissanceAuteur) {
        this.dateNaissanceAuteur = dateNaissanceAuteur;
    }

    public String getDateDecesAuteur() {
        return dateDecesAuteur;
    }

    public void setDateDecesAuteur(String dateDecesAuteur) {
        this.dateDecesAuteur = dateDecesAuteur;
    }

    public String getAvisAuteur() {
        return avisAuteur;
    }

    public void setAvisAuteur(String avisAuteur) {
        this.avisAuteur = avisAuteur;
    }
    
    @Override
    public String toString() {
        return nomAuteur + " " + prenomAuteur ;
    }
    
}
