package Objets;

/**
 *
 * @author Milky
 */
public class Transporteur {
    
    private int idTransporteur;
    private String nomTransporteur;
    private String telTransporteur;
    private String avisTransporteur;
    private int hideTransporteur;

    public Transporteur() {
    }

    public int getHideTransporteur() {
        return hideTransporteur;
    }

    public void setHideTransporteur(int hideTransporteur) {
        this.hideTransporteur = hideTransporteur;
    }

    public Transporteur(int idTransporteur, String nomTransporteur, String telTransporteur, String avisTransporteur, int hideTransporteur) {
        this.idTransporteur = idTransporteur;
        this.nomTransporteur = nomTransporteur;
        this.telTransporteur = telTransporteur;
        this.avisTransporteur = avisTransporteur;
        this.hideTransporteur = hideTransporteur;
    }


    public int getIdTransporteur() {
        return idTransporteur;
    }

    public void setIdTransporteur(int idTransporteur) {
        this.idTransporteur = idTransporteur;
    }

    public String getNomTransporteur() {
        return nomTransporteur;
    }

    public void setNomTransporteur(String nomTransporteur) {
        this.nomTransporteur = nomTransporteur;
    }

    public String getTelTransporteur() {
        return telTransporteur;
    }

    public void setTelTransporteur(String telTransporteur) {
        this.telTransporteur = telTransporteur;
    }

    public String getAvisTransporteur() {
        return avisTransporteur;
    }

    public void setAvisTransporteur(String avisTransporteur) {
        this.avisTransporteur = avisTransporteur;
    }

    @Override
    public String toString() {
        return nomTransporteur;
    }
    
    
    
}
