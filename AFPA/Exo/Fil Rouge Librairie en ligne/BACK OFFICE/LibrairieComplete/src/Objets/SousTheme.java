package Objets;

/**
 *
 * @author Milky
 */
public class SousTheme {
    
    private int idSousTheme;
    private String nomSousTheme;

    public SousTheme() {
    }

    public SousTheme(int idSousTheme, String nomSousTheme) {
        this.idSousTheme = idSousTheme;
        this.nomSousTheme = nomSousTheme;
    }

    public int getIdSousTheme() {
        return idSousTheme;
    }

    public void setIdSousTheme(int idSousTheme) {
        this.idSousTheme = idSousTheme;
    }

    public String getNomSousTheme() {
        return nomSousTheme;
    }

    public void setNomSousTheme(String nomSousTheme) {
        this.nomSousTheme = nomSousTheme;
    }

    @Override
    public String toString() {
        return nomSousTheme;
    }
    
    
    
}
