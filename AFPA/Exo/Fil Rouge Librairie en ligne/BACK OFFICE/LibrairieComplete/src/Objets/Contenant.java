package Objets;

/**
 *
 * @author Milky
 */
public class Contenant {
    
    private String isbnLivre;
    private String numeroCommande;
    private int nbLivre;
    private float prixVente;
    private float TVA;
    private int promotion;

    public Contenant() {
    }

    public Contenant(String isbnLivre, String numeroCommande, int nbLivre, float prixVente, float TVA, int promotion) {
        this.isbnLivre = isbnLivre;
        this.numeroCommande = numeroCommande;
        this.nbLivre = nbLivre;
        this.prixVente = prixVente;
        this.TVA = TVA;
        this.promotion = promotion;
    }

    public String getIsbnLivre() {
        return isbnLivre;
    }

    public void setIsbnLivre(String isbnLivre) {
        this.isbnLivre = isbnLivre;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public int getNbLivre() {
        return nbLivre;
    }

    public void setNbLivre(int nbLivre) {
        this.nbLivre = nbLivre;
    }

    public float getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(float prixVente) {
        this.prixVente = prixVente;
    }

    public float getTVA() {
        return TVA;
    }

    public void setTVA(float TVA) {
        this.TVA = TVA;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }
    
    
    
}
