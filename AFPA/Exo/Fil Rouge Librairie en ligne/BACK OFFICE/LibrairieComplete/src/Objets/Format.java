package Objets;

/**
 *
 * @author Milky
 */
public class Format {
    
    private int idFormat;
    private String nomFormat;

    public Format() {
    }

    public Format(int idFormat, String nomFormat) {
        this.idFormat = idFormat;
        this.nomFormat = nomFormat;
    }

    public int getIdFormat() {
        return idFormat;
    }

    public void setIdFormat(int idFormat) {
        this.idFormat = idFormat;
    }

    public String getNomFormat() {
        return nomFormat;
    }

    public void setNomFormat(String nomFormat) {
        this.nomFormat = nomFormat;
    }

    @Override
    public String toString() {
        return nomFormat;
    }
    
    
    
}
