package Objets;

/**
 *
 * @author Milky
 */
public class Employe {
    
    private int idEmploye;
    private String nomEmploye;
    private String prenomEmploye;
    private String emailEmploye;
    private String telEmploye;
    private int hideEmploye;

    public Employe() {
    }

    public Employe(int idEmploye, String nomEmploye, String prenomEmploye, String emailEmploye, String telEmploye,int hideEmploye) {
        this.idEmploye = idEmploye;
        this.nomEmploye = nomEmploye;
        this.prenomEmploye = prenomEmploye;
        this.emailEmploye = emailEmploye;
        this.telEmploye = telEmploye;
        this.hideEmploye=hideEmploye;
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public int getHideEmploye() {
        return hideEmploye;
    }

    public void setHideEmploye(int hideEmploye) {
        this.hideEmploye = hideEmploye;
    }

    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public String getPrenomEmploye() {
        return prenomEmploye;
    }

    public void setPrenomEmploye(String prenomEmploye) {
        this.prenomEmploye = prenomEmploye;
    }

    public String getEmailEmploye() {
        return emailEmploye;
    }

    public void setEmailEmploye(String emailEmploye) {
        this.emailEmploye = emailEmploye;
    }

    public String getTelEmploye() {
        return telEmploye;
    }

    public void setTelEmploye(String telEmploye) {
        this.telEmploye = telEmploye;
    }
    
    
    
    // Generer la methode toString

    @Override
    public String toString() {
        return nomEmploye +" "+ prenomEmploye ;
    }
    
    
    
    
}
