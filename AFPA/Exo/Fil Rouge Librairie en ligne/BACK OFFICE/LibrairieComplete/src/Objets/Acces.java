package Objets;

/**
 *
 * @author Milky
 */
public class Acces {
    
    private int idAcces;
    private int typeAcces;
    private String designationAcces;

    public Acces() {
    }

    public Acces(int idAcces, int typeAcces, String designationAcces) {
        this.idAcces = idAcces;
        this.typeAcces = typeAcces;
        this.designationAcces = designationAcces;
    }

    public int getIdAcces() {
        return idAcces;
    }

    public void setIdAcces(int idAcces) {
        this.idAcces = idAcces;
    }

    public int getTypeAcces() {
        return typeAcces;
    }

    public void setTypeAcces(int typeAcces) {
        this.typeAcces = typeAcces;
    }

    public String getDesignationAcces() {
        return designationAcces;
    }

    public void setDesignationAcces(String designationAcces) {
        this.designationAcces = designationAcces;
    }

    @Override
    public String toString() {
        return typeAcces + ",  " + designationAcces;
    }
    
    
    
}
