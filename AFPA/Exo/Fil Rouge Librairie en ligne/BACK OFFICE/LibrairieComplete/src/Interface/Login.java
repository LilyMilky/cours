package Interface;

import BDD.*;
import Objets.Compte;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class Login extends javax.swing.JFrame {

    public Login() {
        initComponents();
        setIconImage(new ImageIcon(this.getClass().getResource("/Images/iconeLivre.png")).getImage());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldIdentifiant = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButtonConnexion = new javax.swing.JButton();
        jButtonAnnuler = new javax.swing.JButton();
        jPasswordFieldMotDePasse = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bienvenue à la librairie PYPH");
        getContentPane().setLayout(null);

        jLabel1.setText("IDENTIFIANT :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 30, 110, 30);

        jTextFieldIdentifiant.setText("graven@pyph.com");
        getContentPane().add(jTextFieldIdentifiant);
        jTextFieldIdentifiant.setBounds(130, 30, 250, 30);

        jLabel2.setText("MOT DE PASSE :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(10, 80, 110, 30);

        jButtonConnexion.setText("Connexion");
        jButtonConnexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConnexionActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonConnexion);
        jButtonConnexion.setBounds(50, 170, 110, 25);

        jButtonAnnuler.setText("Annuler");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAnnuler);
        jButtonAnnuler.setBounds(220, 170, 120, 25);

        jPasswordFieldMotDePasse.setText("jPasswordField1");
        jPasswordFieldMotDePasse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPasswordFieldMotDePasseKeyPressed(evt);
            }
        });
        getContentPane().add(jPasswordFieldMotDePasse);
        jPasswordFieldMotDePasse.setBounds(130, 80, 250, 30);

        setSize(new java.awt.Dimension(416, 247));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    private void jButtonConnexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConnexionActionPerformed
        Connexion con = new Connexion();
        Compte compte;
        
        con.connexion();
        String mdp = new String(jPasswordFieldMotDePasse.getPassword());
        compte = con.identification(jTextFieldIdentifiant.getText(), mdp);
        //Si un compte existe, on ouvre le menu principal et on recupere le typeAcces (car chaque acces a des menu differents)
        if(compte.getActifCompte()== 1){
            jFrameMenu.typeAcces = compte.getIdAcces();
            new jFrameMenu().setVisible(true);
            this.setVisible(false);
        //Si le compte est desactivé, on refuse la connexion
        }else if(compte.getActifCompte()== 0){
            JOptionPane.showMessageDialog(null, "Compte désactivé.", "Erreur de login.", JOptionPane.ERROR_MESSAGE);
            new Login().setVisible(true);
        }
        else{
            JOptionPane.showMessageDialog(null, "Veuillez vérifier l'identifiant et/ou le mot de passe.", "Erreur de login.", JOptionPane.ERROR_MESSAGE);
            new Login().setVisible(true);
        }
        con.close();
        
        dispose();
    }//GEN-LAST:event_jButtonConnexionActionPerformed

    private void jPasswordFieldMotDePasseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordFieldMotDePasseKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Connexion con = new Connexion();
            Compte compte;

            con.connexion();
            compte = con.identification(jTextFieldIdentifiant.getText(), jPasswordFieldMotDePasse.getText());
            if (compte.getActifCompte() == 1) {
                jFrameMenu.typeAcces = compte.getIdAcces();
                new jFrameMenu().setVisible(true);
                this.setVisible(false);
            } else if (compte.getActifCompte() == 0) {
                JOptionPane.showMessageDialog(null, "Compte désactivé.", "Erreur de login.", JOptionPane.ERROR_MESSAGE);
                new Login().setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Veuillez vérifier l'identifiant et/ou le mot de passe.", "Erreur de login.", JOptionPane.ERROR_MESSAGE);
                new Login().setVisible(true);
            }
            con.close();

            dispose();
        }
    }//GEN-LAST:event_jPasswordFieldMotDePasseKeyPressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                /*if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }*/
                com.jtattoo.plaf.acryl.AcrylLookAndFeel.setTheme("Default", "Afpa-CDI1511", "PYPH Librairie");
                UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JButton jButtonConnexion;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField jPasswordFieldMotDePasse;
    private javax.swing.JTextField jTextFieldIdentifiant;
    // End of variables declaration//GEN-END:variables
}
