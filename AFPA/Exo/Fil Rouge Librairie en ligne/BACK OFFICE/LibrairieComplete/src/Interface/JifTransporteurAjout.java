package Interface;

import BDD.*;
import static Interface.jFrameMenu.compteur;
import javax.swing.JOptionPane;

public class JifTransporteurAjout extends javax.swing.JInternalFrame {

    public JifTransporteurAjout() {
        initComponents();
        setBounds((compteur*10)%100, ((compteur++)*10)%100, 434 , 212);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAjoutTransporteur = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldNomTransporteur = new javax.swing.JTextField();
        jTextFieldTelephoneTransporteur = new javax.swing.JTextField();
        jTextFieldAvisTransporteur = new javax.swing.JTextField();
        jButtonAjouter = new javax.swing.JButton();
        jButtonAnnuler = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("AjoutTransporteur");
        getContentPane().setLayout(null);

        jPanelAjoutTransporteur.setLayout(null);

        jLabel1.setText("Nom Transporteur : ");
        jPanelAjoutTransporteur.add(jLabel1);
        jLabel1.setBounds(10, 20, 150, 30);

        jLabel2.setText("Telephone Transporteur : ");
        jPanelAjoutTransporteur.add(jLabel2);
        jLabel2.setBounds(10, 60, 160, 30);

        jLabel3.setText("Avis Transporteur : ");
        jPanelAjoutTransporteur.add(jLabel3);
        jLabel3.setBounds(10, 100, 130, 30);
        jPanelAjoutTransporteur.add(jTextFieldNomTransporteur);
        jTextFieldNomTransporteur.setBounds(160, 20, 240, 30);
        jPanelAjoutTransporteur.add(jTextFieldTelephoneTransporteur);
        jTextFieldTelephoneTransporteur.setBounds(160, 60, 240, 30);
        jPanelAjoutTransporteur.add(jTextFieldAvisTransporteur);
        jTextFieldAvisTransporteur.setBounds(160, 100, 240, 30);

        jButtonAjouter.setText("Ajouter");
        jButtonAjouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterActionPerformed(evt);
            }
        });
        jPanelAjoutTransporteur.add(jButtonAjouter);
        jButtonAjouter.setBounds(60, 150, 100, 25);

        jButtonAnnuler.setText("Annuler");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        jPanelAjoutTransporteur.add(jButtonAnnuler);
        jButtonAnnuler.setBounds(239, 150, 110, 25);

        getContentPane().add(jPanelAjoutTransporteur);
        jPanelAjoutTransporteur.setBounds(0, 0, 420, 180);

        setBounds(0, 0, 434, 212);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    private void jButtonAjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterActionPerformed
        int result;
        Connexion con = new Connexion();
        con.connexion();
        
        TransporteurBDD trans = new TransporteurBDD();
        
        //Ajout d'un transporteur en base
        result = trans.addTransporteur(jTextFieldNomTransporteur.getText(), jTextFieldTelephoneTransporteur.getText(), jTextFieldAvisTransporteur.getText(), con);
        
        //Test si l'ajout du transporteur c'est passé sans probleme
        if(result > 0)
        {
            JOptionPane.showMessageDialog(null, "Insertion reussi !", "Insertion reussi !", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de l'insertion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonAjouterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAjouter;
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanelAjoutTransporteur;
    private javax.swing.JTextField jTextFieldAvisTransporteur;
    private javax.swing.JTextField jTextFieldNomTransporteur;
    private javax.swing.JTextField jTextFieldTelephoneTransporteur;
    // End of variables declaration//GEN-END:variables
}
