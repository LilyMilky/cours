package Interface;

import BDD.Connexion;
import BDD.EmployeBDD;
import Objets.Employe;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Hervé
 */
public class JifEmployeRecherche extends javax.swing.JInternalFrame {

    /**
     * Creates new form jifRechercherEmploye
     */
    public JifEmployeRecherche() {
        initComponents();
        jComboBoxEmploye.setModel(initModelEmploye());
    }

    //La méthode qui renvoie le  modele de la comboBox 
    private DefaultComboBoxModel initModelEmploye() {
        return new DefaultComboBoxModel(initEmploye());
    }

    private Vector initEmploye() {
        Vector v = new Vector();
        Connexion con = new Connexion();
        con.connexion();
        // La connexion à la base de donnée
        String requete = "select * from employe where hideEmploye=1 order by nomEmploye";

        try {

            Statement stmt = con.getConnexion().createStatement();
            ResultSet res = stmt.executeQuery(requete);

            while (res.next()) {

                Employe employe = new Employe(res.getInt("idEmploye"),
                        res.getString("nomEmploye"),
                        res.getString("prenomEmploye"),
                        res.getString("emailEmploye"),
                        res.getString("telephoneEmploye"),
                        res.getInt("hideEmploye")
                );

                v.add(employe);
            }

            res.close();
            stmt.close();

        } catch (Exception ex) {
            System.out.println("Attention!!" + ex.getMessage());
        }
        con.close();
        return v;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelRechercherEmploye = new javax.swing.JPanel();
        jLabelListeEmploye = new javax.swing.JLabel();
        jLabelNomEmploye = new javax.swing.JLabel();
        jTextFieldNomEmploye = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldPrenomEmploye = new javax.swing.JTextField();
        jComboBoxEmploye = new javax.swing.JComboBox();
        jLabelEmailEmploye = new javax.swing.JLabel();
        jTextFieldEmailEmploye = new javax.swing.JTextField();
        jLabelTelephoneEmploye = new javax.swing.JLabel();
        jTextFieldTelephoneEmploye = new javax.swing.JTextField();
        jButtonModifierEmploye = new javax.swing.JButton();
        jButtonSupprimerEmploye = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("RECHERCHER EMPLOYE");

        jLabelListeEmploye.setText("Liste des employes:");

        jLabelNomEmploye.setText("Nom:");

        jLabel1.setText("Prenom:");

        jComboBoxEmploye.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxEmploye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxEmployeActionPerformed(evt);
            }
        });

        jLabelEmailEmploye.setText("Email:");

        jLabelTelephoneEmploye.setText("Telephone:");

        jButtonModifierEmploye.setText("Modifier");
        jButtonModifierEmploye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierEmployeActionPerformed(evt);
            }
        });

        jButtonSupprimerEmploye.setText("Supprimer");
        jButtonSupprimerEmploye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSupprimerEmployeActionPerformed(evt);
            }
        });

        jButton1.setText("Annuler");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelRechercherEmployeLayout = new javax.swing.GroupLayout(jPanelRechercherEmploye);
        jPanelRechercherEmploye.setLayout(jPanelRechercherEmployeLayout);
        jPanelRechercherEmployeLayout.setHorizontalGroup(
            jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRechercherEmployeLayout.createSequentialGroup()
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelRechercherEmployeLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jButtonModifierEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonSupprimerEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelRechercherEmployeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelRechercherEmployeLayout.createSequentialGroup()
                                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabelTelephoneEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelEmailEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabelNomEmploye, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldEmailEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldPrenomEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldNomEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldTelephoneEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabelListeEmploye))
                        .addGap(50, 50, 50)
                        .addComponent(jComboBoxEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanelRechercherEmployeLayout.setVerticalGroup(
            jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRechercherEmployeLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelListeEmploye)
                    .addComponent(jComboBoxEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNomEmploye)
                    .addComponent(jTextFieldNomEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldPrenomEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEmailEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEmailEmploye))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTelephoneEmploye)
                    .addComponent(jTextFieldTelephoneEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelRechercherEmployeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButtonSupprimerEmploye)
                    .addComponent(jButtonModifierEmploye))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        jPanelRechercherEmployeLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabelEmailEmploye, jLabelListeEmploye, jLabelNomEmploye, jLabelTelephoneEmploye, jTextFieldEmailEmploye, jTextFieldNomEmploye, jTextFieldPrenomEmploye, jTextFieldTelephoneEmploye});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelRechercherEmploye, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelRechercherEmploye, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    Employe objetEmploye = new Employe(); // récuperer l'identifiant courant 
    private void jComboBoxEmployeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxEmployeActionPerformed
        // Ici le code quand on selectionne un employe dans la combobox 

        if (jComboBoxEmploye.getSelectedItem() != null) {

            objetEmploye = (Employe) jComboBoxEmploye.getSelectedItem();

            jTextFieldNomEmploye.setText(objetEmploye.getNomEmploye());
            jTextFieldPrenomEmploye.setText(objetEmploye.getPrenomEmploye());
            jTextFieldEmailEmploye.setText(objetEmploye.getEmailEmploye());
            jTextFieldTelephoneEmploye.setText(objetEmploye.getTelEmploye());

        }


    }//GEN-LAST:event_jComboBoxEmployeActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Ici le code si je clique sur annnuler
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed


    private void jButtonModifierEmployeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierEmployeActionPerformed
        // TODO add your handling code here:

        // ici le code si je modifie un element 
        if (jComboBoxEmploye.getSelectedItem() != null) {

            int result = 0;
            String nomEmploye = jTextFieldNomEmploye.getText();
            String prenomEmploye = jTextFieldPrenomEmploye.getText();
            String emailEmploye = jTextFieldEmailEmploye.getText();
            String telephoneEmploye = jTextFieldTelephoneEmploye.getText();
            int idEmploye = objetEmploye.getIdEmploye();

            EmployeBDD empBDD = new EmployeBDD();
            result = empBDD.modifierEmploye(idEmploye, nomEmploye, prenomEmploye, emailEmploye, telephoneEmploye);

            if (result > 0) {
                JOptionPane.showMessageDialog(null, "Insertion reussi !", "Insertion reussi !", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Erreur lors de l'insertion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
            }

        }


    }//GEN-LAST:event_jButtonModifierEmployeActionPerformed

    private void jButtonSupprimerEmployeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSupprimerEmployeActionPerformed
        // ici on supprimme les informations en passant le hideEmploye à zero 

        if (jComboBoxEmploye.getSelectedItem() != null) {

            int result = 0;

            int idEmploye = objetEmploye.getIdEmploye();
            int hideEmploye = 0;

            EmployeBDD empBDD = new EmployeBDD();
            result = empBDD.supprimerEmploye(hideEmploye, idEmploye);

            if (result > 0) {
                JOptionPane.showMessageDialog(null, "Insertion reussi !", "Suppression reussi !", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Erreur lors de la suppression ", "Erreur lors de la suppression", JOptionPane.ERROR_MESSAGE);
            }

        }

    }//GEN-LAST:event_jButtonSupprimerEmployeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonModifierEmploye;
    private javax.swing.JButton jButtonSupprimerEmploye;
    private javax.swing.JComboBox jComboBoxEmploye;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelEmailEmploye;
    private javax.swing.JLabel jLabelListeEmploye;
    private javax.swing.JLabel jLabelNomEmploye;
    private javax.swing.JLabel jLabelTelephoneEmploye;
    private javax.swing.JPanel jPanelRechercherEmploye;
    private javax.swing.JTextField jTextFieldEmailEmploye;
    private javax.swing.JTextField jTextFieldNomEmploye;
    private javax.swing.JTextField jTextFieldPrenomEmploye;
    private javax.swing.JTextField jTextFieldTelephoneEmploye;
    // End of variables declaration//GEN-END:variables
}
