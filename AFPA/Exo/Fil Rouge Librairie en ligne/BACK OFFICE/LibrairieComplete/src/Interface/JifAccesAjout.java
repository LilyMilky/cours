package Interface;

import BDD.AccesBDD;
import BDD.Connexion;
import static Interface.jFrameMenu.compteur;
import javax.swing.JOptionPane;

public class JifAccesAjout extends javax.swing.JInternalFrame {

    public JifAccesAjout() {
        initComponents();
        setBounds((compteur*10)%100, ((compteur++)*10)%100, 410 , 228);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButtonAnnuler = new javax.swing.JButton();
        jTextFieldNomAcces = new javax.swing.JTextField();
        jTextFieldNumAcces = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setTitle("Ajouter Acces");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jLabel1.setText("Type Acces :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 30, 100, 30);

        jLabel2.setText("Designation Acces :");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(30, 80, 130, 30);

        jButton1.setText("Ajouter");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(70, 163, 80, 30);

        jButtonAnnuler.setText("Annuler");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonAnnuler);
        jButtonAnnuler.setBounds(220, 163, 80, 30);
        jPanel1.add(jTextFieldNomAcces);
        jTextFieldNomAcces.setBounds(170, 80, 210, 30);
        jPanel1.add(jTextFieldNumAcces);
        jTextFieldNumAcces.setBounds(170, 30, 210, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, -10, 400, 220);

        setBounds(0, 0, 410, 228);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        AccesBDD accesBdd = new AccesBDD();
        int result;
        
        //Try catch pour le parse, permet de verifier que seulement des chiffres ont étés saisie pour le type d'acces
        try
        {
            //Test que le champ ne soit pas vide
            if(!jTextFieldNumAcces.getText().trim().isEmpty())
            {
                //Ajout d'un acces en base
                result = accesBdd.addAcces(Integer.parseInt(jTextFieldNumAcces.getText()), jTextFieldNomAcces.getText(), con);

                if(result > 0)
                {
                    JOptionPane.showMessageDialog(null, "Insertion reussi !", "Insertion reussi !", JOptionPane.INFORMATION_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Erreur lors de l'insertion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                }
            }
            else
                JOptionPane.showMessageDialog(null, "Veuillez saisir un type d'acces", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
        }
        catch(NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour le type d'acces", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
        }
     
        con.close();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldNomAcces;
    private javax.swing.JTextField jTextFieldNumAcces;
    // End of variables declaration//GEN-END:variables
}
