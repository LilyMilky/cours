package Interface;

import BDD.*;
import Classes.ContenuCommandeModel;
import Classes.ConvertDate;
import static Interface.jFrameMenu.compteur;
import Objets.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class JifCommande extends javax.swing.JInternalFrame {

    private int runT = 0;
    private int runE = 0;
    private int runSP = 0;
    private Vector vTrans = new Vector();
    private Vector vEtat = new Vector();
    private Vector vSitePaiement = new Vector();
    
    //Modele pour la jTable
    private ContenuCommandeModel modele = new ContenuCommandeModel();
    
    public JifCommande() {
        
        initComponents();
        setBounds((compteur*10)%100, ((compteur++)*10)%100, 930 , 764);
        jPanelInfoCommande.setVisible(false);
    }

    //Initialise la comboBox des sites de paiement
    private DefaultComboBoxModel initModelSitePaiement()
    {
        return new DefaultComboBoxModel(initSitePaiement());
    }

    private Vector initSitePaiement()
    {
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD sitePaiementbdd = new CommandeBDD();

        vSitePaiement = sitePaiementbdd.getSitePaiement(con);
        
        con.close();
        return vSitePaiement;
    }
    
    
    //Initialise la comboBox des transporteurs
    private DefaultComboBoxModel initModelTransporteur()
    {
        return new DefaultComboBoxModel(initTransporteur());
    }

    private Vector initTransporteur()
    {
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD transbdd = new CommandeBDD();

        vTrans = transbdd.getTransporteurs(con);
        
        con.close();
        return vTrans;
    }
    
    //Initialise la comboBox des etats
    private DefaultComboBoxModel initModelEtat()
    {
        return new DefaultComboBoxModel(initEtat());
    }

    private Vector initEtat()
    {
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD etatbdd = new CommandeBDD();

        vEtat = etatbdd.getEtats(con);
        
        con.close();
        return vEtat;
    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldNumeroCommande = new javax.swing.JTextField();
        jButtonOKNumeroCommande = new javax.swing.JButton();
        jPanelInfoCommande = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxSitePaiement = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxEtatCommande = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldCompteClient = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxTransporteur = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldDateCommande = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldModePaiement = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jDateChooserDateLivraison = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldIpCommande = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldRefPaiement = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldRefColis = new javax.swing.JTextField();
        jButtonModifier = new javax.swing.JButton();
        jButtonAnnuler = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextFieldAdresseLiv = new javax.swing.JTextField();
        jTextFieldAdrFactu = new javax.swing.JTextField();

        setClosable(true);
        setTitle("Rechercher Commande");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jLabel1.setText("Numero Commande : ");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(190, 20, 130, 30);
        jPanel1.add(jTextFieldNumeroCommande);
        jTextFieldNumeroCommande.setBounds(330, 20, 270, 30);

        jButtonOKNumeroCommande.setText("OK");
        jButtonOKNumeroCommande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOKNumeroCommandeActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonOKNumeroCommande);
        jButtonOKNumeroCommande.setBounds(640, 20, 49, 30);

        jPanelInfoCommande.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelInfoCommande.setLayout(null);

        jLabel2.setText("Site de Paiement : ");
        jPanelInfoCommande.add(jLabel2);
        jLabel2.setBounds(20, 220, 100, 30);

        jComboBoxSitePaiement.setModel(initModelSitePaiement());
        jComboBoxSitePaiement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSitePaiementActionPerformed(evt);
            }
        });
        jPanelInfoCommande.add(jComboBoxSitePaiement);
        jComboBoxSitePaiement.setBounds(170, 220, 290, 30);

        jLabel3.setText("Etat Commande : ");
        jPanelInfoCommande.add(jLabel3);
        jLabel3.setBounds(20, 270, 110, 30);

        jComboBoxEtatCommande.setModel(initModelEtat());
        jComboBoxEtatCommande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxEtatCommandeActionPerformed(evt);
            }
        });
        jPanelInfoCommande.add(jComboBoxEtatCommande);
        jComboBoxEtatCommande.setBounds(170, 270, 290, 30);

        jLabel4.setText("Compte Client : ");
        jPanelInfoCommande.add(jLabel4);
        jLabel4.setBounds(20, 20, 110, 30);

        jTextFieldCompteClient.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldCompteClient);
        jTextFieldCompteClient.setBounds(170, 20, 290, 30);

        jLabel5.setText("Adresse Livraison : ");
        jPanelInfoCommande.add(jLabel5);
        jLabel5.setBounds(20, 70, 120, 30);

        jLabel6.setText("Adresse Facturation : ");
        jPanelInfoCommande.add(jLabel6);
        jLabel6.setBounds(20, 120, 130, 30);

        jLabel7.setText("Transporteur : ");
        jPanelInfoCommande.add(jLabel7);
        jLabel7.setBounds(20, 174, 120, 30);

        jComboBoxTransporteur.setModel(initModelTransporteur());
        jComboBoxTransporteur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTransporteurActionPerformed(evt);
            }
        });
        jPanelInfoCommande.add(jComboBoxTransporteur);
        jComboBoxTransporteur.setBounds(170, 170, 290, 30);

        jLabel8.setText("Date de Commande : ");
        jPanelInfoCommande.add(jLabel8);
        jLabel8.setBounds(480, 20, 120, 30);

        jTextFieldDateCommande.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldDateCommande);
        jTextFieldDateCommande.setBounds(610, 20, 300, 30);

        jLabel9.setText("Mode de Paiement : ");
        jPanelInfoCommande.add(jLabel9);
        jLabel9.setBounds(480, 120, 120, 30);

        jTextFieldModePaiement.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldModePaiement);
        jTextFieldModePaiement.setBounds(610, 120, 300, 30);

        jLabel10.setText("Date de Livraison : ");
        jPanelInfoCommande.add(jLabel10);
        jLabel10.setBounds(480, 70, 130, 30);

        jDateChooserDateLivraison.setDateFormatString("dd-MM-yyyy");
        jPanelInfoCommande.add(jDateChooserDateLivraison);
        jDateChooserDateLivraison.setBounds(610, 70, 300, 30);

        jLabel11.setText("IP Commande :");
        jPanelInfoCommande.add(jLabel11);
        jLabel11.setBounds(480, 170, 120, 30);

        jTextFieldIpCommande.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldIpCommande);
        jTextFieldIpCommande.setBounds(610, 170, 300, 30);

        jLabel12.setText("Reference Paiement :");
        jPanelInfoCommande.add(jLabel12);
        jLabel12.setBounds(480, 220, 120, 30);

        jTextFieldRefPaiement.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldRefPaiement);
        jTextFieldRefPaiement.setBounds(610, 220, 300, 30);

        jLabel13.setText("Reference Colis :");
        jPanelInfoCommande.add(jLabel13);
        jLabel13.setBounds(480, 270, 120, 30);

        jTextFieldRefColis.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldRefColis);
        jTextFieldRefColis.setBounds(610, 270, 300, 30);

        jButtonModifier.setText("Modifier");
        jButtonModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierActionPerformed(evt);
            }
        });
        jPanelInfoCommande.add(jButtonModifier);
        jButtonModifier.setBounds(290, 600, 120, 30);

        jButtonAnnuler.setText("Annuler");
        jButtonAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerActionPerformed(evt);
            }
        });
        jPanelInfoCommande.add(jButtonAnnuler);
        jButtonAnnuler.setBounds(510, 600, 120, 30);

        jLabel14.setText("Contenu :");
        jPanelInfoCommande.add(jLabel14);
        jLabel14.setBounds(20, 320, 60, 30);

        jTable1.setModel(modele);
        jScrollPane1.setViewportView(jTable1);

        jPanelInfoCommande.add(jScrollPane1);
        jScrollPane1.setBounds(90, 340, 820, 250);

        jTextFieldAdresseLiv.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldAdresseLiv);
        jTextFieldAdresseLiv.setBounds(170, 70, 290, 30);

        jTextFieldAdrFactu.setEnabled(false);
        jPanelInfoCommande.add(jTextFieldAdrFactu);
        jTextFieldAdrFactu.setBounds(170, 120, 290, 30);

        jPanel1.add(jPanelInfoCommande);
        jPanelInfoCommande.setBounds(-10, 80, 930, 660);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 920, 740);

        setBounds(0, 0, 930, 764);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOKNumeroCommandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOKNumeroCommandeActionPerformed
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Commande com = new Commande();
        Connexion con = new Connexion();
        con.connexion();
        
        CommandeBDD combdd = new CommandeBDD();
        //On vide la jTable avant de la reremplir pour eviter une redondance d'information
        modele.removeAll();
        
        com = combdd.findCommande(jTextFieldNumeroCommande.getText(), con);    
        
        if(com.getDateCommande() != null)
        {
            jPanelInfoCommande.setVisible(true);
                    
            jTextFieldDateCommande.setText(com.getDateCommande());
            jTextFieldIpCommande.setText(com.getIpCommande());
            jTextFieldRefColis.setText(com.getReferenceColis());
            jTextFieldRefPaiement.setText(com.getReferencePaiement());
            jTextFieldModePaiement.setText(com.getModePaiement());
            try
            {
                if(com.getDateLivraisonCommande() != null)
                {
                    String date = ConvertDate.convertDateAme(com.getDateLivraisonCommande());
                    jDateChooserDateLivraison.setDate(sdf.parse(date));
                }
            }
            catch(ParseException ex)
            {
                //Date de livraison null : le textField reste vide
            }
    
            Vector v = new Vector();
            
            //Recuperation des coordonnees de livraison + facturation
            v = combdd.getAdresses(com.getIdLivraisonCoordonnees(), com.getIdFacturationCommande(), con);
            
            //Coordonnees LIVRAISON         
            Iterator<Coordonnees> itc = v.iterator();
            while (itc.hasNext())
            {
                Coordonnees coor = itc.next();
                if(coor.getIdCoordonnees()== com.getIdLivraisonCoordonnees())
                {
                    jTextFieldAdresseLiv.setText(coor.getVoieCoordonnees() + " " + coor.getCodePostalCoordonnees() + " " + coor.getVilleCoordonnees());
                }
                else if(coor.getIdCoordonnees()== com.getIdFacturationCommande())
                {
                    jTextFieldAdrFactu.setText(coor.getVoieCoordonnees() + " " + coor.getCodePostalCoordonnees() + " " + coor.getVilleCoordonnees());
                }
                
                jTextFieldCompteClient.setText(coor.getEmailCoordonnees());
            }
              
                        
            //Mise a jour de l'affichage des comboBox 
            
            //Transporteur            
            Iterator<Transporteur> it = vTrans.iterator();
            while (it.hasNext())
            {
                Transporteur trans = it.next();
                if(trans.getIdTransporteur() == com.getIdTransporteur())
                {
                    jComboBoxTransporteur.setSelectedItem(trans);
                    break;
                }
            }
            
            //Etat            
            Iterator<Etat> itEtat = vEtat.iterator();
            while (itEtat.hasNext())
            {
                Etat etat = itEtat.next();
                if(etat.getIdEtat()== com.getIdEtat())
                {
                    jComboBoxEtatCommande.setSelectedItem(etat);
                    break;
                }
            }
            
            //SitePaiement            
            Iterator<SitePaiement> itSitePaiement = vSitePaiement.iterator();
            while (itSitePaiement.hasNext())
            {
                SitePaiement sitePaiement = itSitePaiement.next();
                if(sitePaiement.getIdSitePaiement()== com.getIdSitePaiement())
                {
                    jComboBoxSitePaiement.setSelectedItem(sitePaiement);
                    break;
                }
            }
            
            //Mise a jour de la jTable
            v = combdd.rechercheBDDContenant(jTextFieldNumeroCommande.getText(), con);

            for(int j = 0; j < v.size(); j++)
            {
                Contenant cont = (Contenant) v.get(j);

                modele.addLine(cont);
            }

        }
        else
        {
            JOptionPane.showMessageDialog(null, "Numero commande introuvable", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonOKNumeroCommandeActionPerformed

    private void jButtonAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonAnnulerActionPerformed

    //Mise a jour du transporteur en base lors de la selection d'un nouveau transporteur dans la comboBox
    private void jComboBoxTransporteurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTransporteurActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD combdd = new CommandeBDD();
        
        int result = combdd.updateidTransporteurCommande(((Transporteur)jComboBoxTransporteur.getSelectedItem()).getIdTransporteur(), jTextFieldNumeroCommande.getText(), con);
        
        if(result > 0 && runT > 0)
        {
            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(runT > 0)
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
        }
        
        runT++;
        con.close();
    }//GEN-LAST:event_jComboBoxTransporteurActionPerformed

    //Mise a jour du site de paiement en base lors de la selection d'un nouveau site de paiement dans la comboBox
    private void jComboBoxSitePaiementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSitePaiementActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD combdd = new CommandeBDD();
        
        int result = combdd.updateidSitePaiementCommande(((SitePaiement)jComboBoxSitePaiement.getSelectedItem()).getIdSitePaiement(), jTextFieldNumeroCommande.getText(), con);
        
        if(result > 0 && runSP > 0)
        {
            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(runSP > 0)
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
        }
        
        runSP++;
        con.close();
    }//GEN-LAST:event_jComboBoxSitePaiementActionPerformed

    //Mise a jour de l'etat du colis en base lors de la selection d'un nouvel etat de colis dans la comboBox
    private void jComboBoxEtatCommandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxEtatCommandeActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD combdd = new CommandeBDD();
        
        int result = combdd.updateidEtatCommande(((Etat)jComboBoxEtatCommande.getSelectedItem()).getIdEtat(), jTextFieldNumeroCommande.getText(), con);
        
        if(result > 0 && runE > 0)
        {
            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(runE > 0)
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
        }
        
        runE++;
        con.close();
    }//GEN-LAST:event_jComboBoxEtatCommandeActionPerformed

    //Mise a jour de la date de livraison en base
    private void jButtonModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        CommandeBDD combdd = new CommandeBDD();
        
        int result = combdd.updateDateLivraisonCommande(jTextFieldNumeroCommande.getText(),ConvertDate.convertDateSQL(jDateChooserDateLivraison.getDate()), con);
        
        if(result > 0)
        {
            JOptionPane.showMessageDialog(null, "Mise a jour reussi !", "Mise a jour reussi !", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Erreur lors de la mise a jour", "Erreur lors de la mise a jour", JOptionPane.ERROR_MESSAGE);
        }
        
        con.close();
    }//GEN-LAST:event_jButtonModifierActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnnuler;
    private javax.swing.JButton jButtonModifier;
    private javax.swing.JButton jButtonOKNumeroCommande;
    private javax.swing.JComboBox jComboBoxEtatCommande;
    private javax.swing.JComboBox jComboBoxSitePaiement;
    private javax.swing.JComboBox jComboBoxTransporteur;
    private com.toedter.calendar.JDateChooser jDateChooserDateLivraison;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelInfoCommande;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextFieldAdrFactu;
    private javax.swing.JTextField jTextFieldAdresseLiv;
    private javax.swing.JTextField jTextFieldCompteClient;
    private javax.swing.JTextField jTextFieldDateCommande;
    private javax.swing.JTextField jTextFieldIpCommande;
    private javax.swing.JTextField jTextFieldModePaiement;
    private javax.swing.JTextField jTextFieldNumeroCommande;
    private javax.swing.JTextField jTextFieldRefColis;
    private javax.swing.JTextField jTextFieldRefPaiement;
    // End of variables declaration//GEN-END:variables
}
