package Interface;

import BDD.*;
import static Interface.jFrameMenu.compteur;
import javax.swing.JOptionPane;

public class JifRubriqueAjout extends javax.swing.JInternalFrame {

    public JifRubriqueAjout() {
        initComponents();
        setBounds((compteur*10)%100, ((compteur++)*10)%100, 410 , 370);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDayChooser1 = new com.toedter.calendar.JDayChooser();
        jPanel1 = new javax.swing.JPanel();
        Annuler = new javax.swing.JButton();
        jButtonAjouter = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldNomRubrique = new javax.swing.JTextField();
        jDateChooserDateDebut = new com.toedter.calendar.JDateChooser();
        jDateChooserDateFin = new com.toedter.calendar.JDateChooser();
        jTextFieldAvisRubrique = new javax.swing.JTextField();
        jTextFieldTauxPromotion = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setTitle("Ajouter une rubrique");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });
        jPanel1.add(Annuler);
        Annuler.setBounds(250, 290, 90, 30);

        jButtonAjouter.setText("Ajouter");
        jButtonAjouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonAjouter);
        jButtonAjouter.setBounds(70, 290, 100, 30);

        jLabel5.setText("Taux de promotion :");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(20, 210, 120, 30);

        jLabel4.setText("Avis Rubrique :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 160, 120, 30);

        jLabel3.setText("Date de Fin : ");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 110, 120, 30);

        jLabel2.setText("Date de Debut: ");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 60, 120, 30);

        jLabel1.setText("Nom rubrique :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 10, 110, 30);
        jPanel1.add(jTextFieldNomRubrique);
        jTextFieldNomRubrique.setBounds(140, 10, 250, 30);

        jDateChooserDateDebut.setDateFormatString("dd-MM-yyyy");
        jPanel1.add(jDateChooserDateDebut);
        jDateChooserDateDebut.setBounds(140, 60, 250, 30);

        jDateChooserDateFin.setDateFormatString("dd-MM-yyyy");
        jPanel1.add(jDateChooserDateFin);
        jDateChooserDateFin.setBounds(140, 110, 250, 30);
        jPanel1.add(jTextFieldAvisRubrique);
        jTextFieldAvisRubrique.setBounds(140, 160, 250, 30);
        jPanel1.add(jTextFieldTauxPromotion);
        jTextFieldTauxPromotion.setBounds(140, 210, 250, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(-10, 0, 410, 340);

        setBounds(0, 0, 410, 370);
    }// </editor-fold>//GEN-END:initComponents

    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_AnnulerActionPerformed

    private void jButtonAjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterActionPerformed
        Connexion con = new Connexion();
        con.connexion();
        RubriqueBDD rubbdd = new RubriqueBDD();
        int prom;
        //Try catch permettant de verifier que seulement des chiffres ont été saisie pour le taux de promotion
        try
        {
            //Test que le champ ne soit pas vide
            if(!jTextFieldTauxPromotion.getText().trim().isEmpty())
            {
                prom = Integer.parseInt(jTextFieldTauxPromotion.getText());

                int result = rubbdd.addRubrique(jTextFieldNomRubrique.getText(), jDateChooserDateDebut.getDate(), jDateChooserDateFin.getDate(), jTextFieldAvisRubrique.getText(), prom, con);

                if(result > 0)
                {
                    JOptionPane.showMessageDialog(null, "Insertion reussi !", "Insertion reussi !", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Erreur lors de l'insertion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);
                }       
            }
            else
                JOptionPane.showMessageDialog(null, "Veuillez saisir un taux de promotion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);        
        }catch(NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(null, "Veuillez saisir seulement des chiffres pour le taux de promotion", "Erreur lors de l'insertion", JOptionPane.ERROR_MESSAGE);        
        }
        
        
        
        con.close();
    }//GEN-LAST:event_jButtonAjouterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Annuler;
    private javax.swing.JButton jButtonAjouter;
    private com.toedter.calendar.JDateChooser jDateChooserDateDebut;
    private com.toedter.calendar.JDateChooser jDateChooserDateFin;
    private com.toedter.calendar.JDayChooser jDayChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldAvisRubrique;
    private javax.swing.JTextField jTextFieldNomRubrique;
    private javax.swing.JTextField jTextFieldTauxPromotion;
    // End of variables declaration//GEN-END:variables
}
