package BDD;

import java.sql.SQLException;
import java.sql.Statement;

public class TvaBDD {

    public void enregistrerTva(Connexion con, String tauxTva, int typeTva) {
        System.out.println(tauxTva);
        float tauxTvaFloat = Float.valueOf(tauxTva);
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "UPDATE tva SET tauxTva=" + tauxTvaFloat + ", dateTva=GETDATE() WHERE typeTva="+ typeTva;
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
}
