package BDD;

import Objets.Auteur;
import Objets.Coordonnees;
import Objets.Editeur;
import Objets.Livre;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class EditeurBDD {

    private Connection connexion = null;

    public EditeurBDD(String sqlProtocol) {
        if (sqlProtocol.equals("SQLServer")) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException ex) {
                System.err.println("Oops: Classe pas trouvé: " + ex.getMessage());
            }

            try {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=Librairie;user=sa;password=sa;";
                connexion = DriverManager.getConnection(connectionUrl);
            } catch (SQLException ex) {
                System.err.println("Oops:SQLConnection:" + ex.getErrorCode() + "/" + ex.getMessage());
            }
        }

//        if(sqlProtocol.equals("MySQL")){
//        if(sqlProtocol.equals("SQLServer")){
//            try {
//                Class.forName("com.mysql.jdbc.Driver");
//            } catch (ClassNotFoundException ex) {
//                System.err.println("ClassNotFound:"+ex.getMessage());
//            }
//
//
//            
//            try {
//                String url = "jdbc:mysql://leox/cdi08";
//                connexion = DriverManager.getConnection(url, "cdi08", "afpa");
//                String url = "jdbc:mysql://localhost/carnetAdresses";
//                connexion = DriverManager.getConnection(url, "root", "admin");
//                System.out.println("Succes");
//            } catch (SQLException ex) {
//                System.err.println("SQL:"+ex.getErrorCode()+"/"+ex.getMessage());
//            }   
//            
//            
//        }
    }

    public Editeur sqlGetEditeur(String EditeurToGet) {
        Editeur subResult = new Editeur();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );
            System.out.println(EditeurToGet);
//            String query = "SELECT * FROM editeur WHERE numeroSiretEditeur='57220675300012'";
            String query = "SELECT * FROM editeur WHERE numeroSiretEditeur=" + "'" + EditeurToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);

            
            while (rs.next()) {
                subResult.setNumeroSiretEditeur(rs.getString("numeroSiretEditeur"));
                subResult.setIdCoordonnees(Integer.parseInt(rs.getString("idCoordonnees")));
                subResult.setNomEditeur(rs.getString("nomEditeur"));
                subResult.setAvisEditeur(rs.getString("avisEditeur"));
            }
            
            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        System.out.println(subResult);
        return subResult;
    }

    public Coordonnees sqlGetCoordonnees(int coordonneesToGet) {
        Coordonnees subResult = new Coordonnees();
        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "SELECT * FROM coordonnees WHERE idCoordonnees=" + "'" + coordonneesToGet + "'" + ";";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                subResult.setIdCivilite(Integer.parseInt(rs.getString("idCivilite")));
                subResult.setIdCoordonnees(Integer.parseInt(rs.getString("idCoordonnees")));
                subResult.setNomCoordonnees(rs.getString("nomCoordonnees"));
                subResult.setPrenomCoordonnees(rs.getString("prenomCoordonnees"));
                subResult.setVoieCoordonnees(rs.getString("voieCoordonnees"));
                subResult.setCodePostalCoordonnees(rs.getString("codePostalCoordonnees"));
                subResult.setVilleCoordonnees(rs.getString("villeCoordonnees"));
                subResult.setComplementCoordonnees(rs.getString("complementCoordonnees"));
                subResult.setTelephoneCoordonnees(rs.getString("telephoneCoordonnees"));
                subResult.setTelephonePortableCoordonnees(rs.getString("telephonePortableCoordonnees"));
                subResult.setEmailCoordonnees(rs.getString("emailCoordonnees"));
                subResult.setTypeCoordonnees(Integer.parseInt(rs.getString("typeCoordonnees")));
            }
            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
        return subResult;
    }

    public void sqlAddCoordonnees(Coordonnees paramCoordonnees, Editeur paramEditeur) {
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "INSERT INTO coordonnees (idCivilite,nomCoordonnees,prenomCoordonnees,voieCoordonnees,codePostalCoordonnees,villeCoordonnees,telephoneCoordonnees,telephonePortableCoordonnees,emailCoordonnees,typeCoordonnees) VALUES ("
                    + "'" + paramCoordonnees.getIdCivilite() + "',"
                    + "'" + paramCoordonnees.getNomCoordonnees() + "',"
                    + "'" + paramCoordonnees.getPrenomCoordonnees() + "',"
                    + "'" + paramCoordonnees.getVoieCoordonnees() + "',"
                    + "'" + paramCoordonnees.getCodePostalCoordonnees() + "',"
                    + "'" + paramCoordonnees.getVilleCoordonnees() + "',"
                    + "'" + paramCoordonnees.getTelephoneCoordonnees()+ "',"
                    + "'" + paramCoordonnees.getTelephonePortableCoordonnees()+ "',"
                    + "'" + paramCoordonnees.getEmailCoordonnees() + "',"
                    + "'" + paramCoordonnees.getTypeCoordonnees() + "'"
                    + ");";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }

        int coordonneesPos = 0;

        try {
            Statement stmt = connexion.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            String query = "select top 1 * from coordonnees order by idCoordonnees desc;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                coordonneesPos = (Integer.parseInt(rs.getString("idCoordonnees")));
            }
            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }

        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "INSERT INTO editeur (numeroSiretEditeur,idCoordonnees,nomEditeur,avisEditeur) VALUES ("
                    + "'" + paramEditeur.getNumeroSiretEditeur() + "',"
                    + "'" + coordonneesPos + "',"
                    + "'" + paramEditeur.getNomEditeur() + "',"
                    + "'" + paramEditeur.getAvisEditeur() + "'"
                    + ");";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateCoordonnees(Coordonnees paramCoordonnees, Editeur paramEditeur) {
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "UPDATE coordonnees SET "
                    + "idCivilite='" + paramCoordonnees.getIdCivilite() + "',"
                    + "nomCoordonnees='" + paramCoordonnees.getNomCoordonnees() + "',"
                    + "prenomCoordonnees='" + paramCoordonnees.getPrenomCoordonnees() + "',"
                    + "voieCoordonnees='" + paramCoordonnees.getVoieCoordonnees() + "',"
                    + "codePostalCoordonnees='" + paramCoordonnees.getCodePostalCoordonnees() + "',"
                    + "villeCoordonnees='" + paramCoordonnees.getVilleCoordonnees() + "',"
                    + "telephoneCoordonnees='" + paramCoordonnees.getTelephoneCoordonnees()+ "',"
                    + "telephonePortableCoordonnees='" + paramCoordonnees.getTelephonePortableCoordonnees()+ "',"
                    + "emailCoordonnees='" + paramCoordonnees.getEmailCoordonnees() + "',"
                    + "typeCoordonnees='" + paramCoordonnees.getTypeCoordonnees() + "'"
                    + " WHERE idCoordonnees='" + paramCoordonnees.getIdCoordonnees() + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }

        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            query = "UPDATE editeur SET "
                    + "numeroSiretEditeur='" + paramEditeur.getNumeroSiretEditeur() + "',"
                    + "idCoordonnees='" + paramCoordonnees.getIdCoordonnees() + "',"
                    + "nomEditeur='" + paramEditeur.getNomEditeur() + "',"
                    + "avisEditeur='" + paramEditeur.getAvisEditeur() + "'"
                    + " WHERE idCoordonnees='" + paramCoordonnees.getIdCoordonnees() + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateAuteur(Auteur paramAuteur) {
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

            System.out.println("idauteur" + paramAuteur.getIdAuteur());

            query = "UPDATE auteur SET "
                    + "nomAuteur='" + paramAuteur.getNomAuteur() + "',"
                    + "prenomAuteur='" + paramAuteur.getPrenomAuteur() + "',"
                    + "dateNaissanceAuteur='" + paramAuteur.getDateNaissanceAuteur() + "',"
                    + "dateDecesAuteur='" + paramAuteur.getDateDecesAuteur() + "',"
                    + "avisAuteur='" + paramAuteur.getAvisAuteur() + "'"
                    + " WHERE idAuteur='" + paramAuteur.getIdAuteur() + "';";

            int result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlUpdateLivre(Livre tempLivre, int auteurChoix, int rubriqueChoix, ArrayList<Integer> selectedMotCle) {
        for (int z : selectedMotCle) {
            System.out.println(z);
        }
        try {
            Statement stmt = connexion.createStatement();
            String query = "";

//            UPDATE livre SET titreLivre='Test2' WHERE isbnLivre='973-2-266-26282-4';
            String isbnStr = tempLivre.getIsbnLivre();
            query = "UPDATE livre SET "
                    + "numeroSiretEditeur='" + tempLivre.getNumeroSiretEditeur() + "',"
                    + "idFormat='" + tempLivre.getIdFormat() + "',"
                    + "titreLivre='" + tempLivre.getTitreLivre() + "',"
                    + "sousTitreLivre='" + tempLivre.getSousTitreLivre() + "',"
                    + "adresseImageLivre='" + tempLivre.getAdresseImageLivre() + "',"
                    + "resumeLivre='" + tempLivre.getResumeLivre() + "',"
                    + "stockLivre='" + tempLivre.getStockLivre() + "',"
                    + "collectionLivre='" + tempLivre.getCollectionLivre() + "',"
                    + "prixHTLivre='" + tempLivre.getPrixHTLivre() + "',"
                    + "avisLivre='" + tempLivre.getAvisLivre() + "'"
                    + " WHERE isbnLivre='" + isbnStr + "';";
//            System.out.println(query);

            int result = stmt.executeUpdate(query);
            stmt.close();

///*
            Statement stmt2del = connexion.createStatement();
            String query2del = "";

            query2del = "DELETE FROM ecriture WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result2del = stmt2del.executeUpdate(query2del);
            stmt2del.close();

            Statement stmt2 = connexion.createStatement();
            String query2 = "";

            query2 = "INSERT INTO ecriture (isbnLivre,idAuteur) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + auteurChoix + "'"
                    + ");";

            int result2 = stmt2.executeUpdate(query2);
            stmt2.close();

            Statement stmt3del = connexion.createStatement();
            String query3del = "";

            query3del = "DELETE FROM participation WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

            int result3del = stmt3del.executeUpdate(query3del);
            stmt3del.close();

            Statement stmt3 = connexion.createStatement();
            String query3 = "";

            query3 = "INSERT INTO participation (isbnLivre,idRubrique) VALUES ("
                    + "'" + tempLivre.getIsbnLivre() + "',"
                    + "'" + rubriqueChoix + "'"
                    + ");";

            int result3 = stmt3.executeUpdate(query3);
            stmt3.close();

            Statement stmt4del = connexion.createStatement();
            String query4del = "";

            for (int a : selectedMotCle) {
                query4del = "DELETE FROM referencement WHERE isbnLivre=" + "'" + tempLivre.getIsbnLivre() + "';";

                int result4del = stmt4del.executeUpdate(query4del);
            }

            stmt4del.close();

            Statement stmt4 = connexion.createStatement();
            String query4 = "";

            for (int a : selectedMotCle) {
                query4 = "INSERT INTO referencement (idMotCle,isbnLivre) VALUES ("
                        + "'" + a + "',"
                        + "'" + tempLivre.getIsbnLivre() + "'"
                        + ");";

                int result4 = stmt4.executeUpdate(query4);
            }

            stmt4.close();
//            */

        } catch (SQLException ex) {
            System.err.println("TOops:SQL:" + ex.getErrorCode() + "/" + ex.getMessage());
        }
    }

    public void sqlClose() {
        try {
            connexion.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:close:" + ex.getMessage());
        }

    }
}
