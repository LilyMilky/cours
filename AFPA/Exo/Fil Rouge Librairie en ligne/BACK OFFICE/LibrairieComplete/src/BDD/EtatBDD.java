package BDD;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hervé
 */
public class EtatBDD {

    // Nous avons besoin de deux classes 
    // L'une pour creer un Etat  et l'autre pour le rechercher et le modifier 
    public void creerEtat(int typeEtat, String nomEtat) {
        Connexion con = new Connexion();
        con.connexion();
        String requete = "insert into Etat(typeEtat,nomEtat) values (?,?)";
        try {
            PreparedStatement stmt = con.getConnexion().prepareStatement(requete);
            stmt.setInt(1, typeEtat);
            stmt.setString(2, nomEtat);
            int res = stmt.executeUpdate();

            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(EtatBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
        con.close();
    }

    // Une fonction  qui me renvoie le  type Etat donc le code associé  l'état de commande   
    public int renvoyerTypeEtat(String nomEtat) {
        Connexion con = new Connexion();
        int etat = 0;
        con.connexion();
        try {

            String requete = "select typeEtat from etat where nomEtat='" + nomEtat + "';";
            Statement stmt = con.getConnexion().createStatement();
            ResultSet res = stmt.executeQuery(requete);

            while (res.next()) {

              // etat = new Etat(res.getInt(1), res.getInt(2), res.getString(3));
                etat = res.getInt("typeEtat");

            }

            res.close();
            stmt.close();
        } catch (Exception ex) {
            System.out.println("Attention" + ex.getMessage());
        }
        con.close();
        return etat;
    }

    // La fonction qui modifie un état de commande 
    public int modifierEtat(int idEtat, String nomEtat,int typeEtat) {
        Connexion con = new Connexion();   
        int result=0;
        con.connexion();
        try {
            
           // String requete = "select idEtat from etat where nomEtat='" + nomEtat + "' and typeEtat = '" + typeEtat + "'; ;";
            
            
            String requete = "Update etat set nomEtat = '" +  nomEtat
                    +  "',  typeEtat = " + typeEtat
                    +  " where idEtat = " + idEtat;

            
            Statement stmt = con.getConnexion().createStatement();
            result = stmt.executeUpdate(requete);

           

        } catch (Exception ex) {

            System.out.println("Attention!!" + ex.getMessage());
        }
        con.close();
        return result;
    }

}
