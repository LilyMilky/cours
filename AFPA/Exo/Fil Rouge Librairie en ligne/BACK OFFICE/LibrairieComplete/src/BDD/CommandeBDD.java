package BDD;

import Classes.ConvertDate;
import Objets.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class CommandeBDD {
    
    //Recupere les donnees de la commande
    public Commande findCommande(String numeroCommande, Connexion con)
    {
        Commande com = new Commande();
        
        String query = "select idSitePaiement, idClient, idEtat, idLivraisonCoordonnees,"
                + "idTransporteur, idFacturationCoordonnees, dateCommande, modePaiementCommande, dateLivraisonCommande," 
                + "ipCommande, referencePaiement,referenceColisCommande" 
                + " from commande where numeroCommande = '" + numeroCommande +"';";
        
        //System.out.println(query);
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                
                com.setNumeroCommande(numeroCommande);
                com.setDateCommande(ConvertDate.convertDate(rs.getString("dateCommande")));
                com.setModePaiement(rs.getString("modePaiementCommande"));
                
                if(rs.getString("dateLivraisonCommande") != null)
                    com.setDateLivraisonCommande(ConvertDate.convertDate(rs.getString("dateLivraisonCommande")));
                
                com.setIpCommande(rs.getString("ipCommande"));
                com.setReferencePaiement(rs.getString("referencePaiement"));
                com.setReferenceColis(rs.getString("referenceColisCommande")); 
                com.setIdClient(rs.getInt("idClient"));
                com.setIdEtat(rs.getInt("idEtat"));
                com.setIdFacturationCommande(rs.getInt("idFacturationCoordonnees"));
                com.setIdLivraisonCoordonnees(rs.getInt("idLivraisonCoordonnees"));
                com.setIdSitePaiement(rs.getInt("idSitePaiement"));
                com.setIdTransporteur(rs.getInt("idTransporteur"));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return com;
        }
        
        return com;
    }
    
    //Recupere les transporteur
    public Vector getTransporteurs(Connexion con)
    {        
        Vector v = new Vector();
        String query = "select * from transporteur";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while(rs.next()) {
                
                v.add(new Transporteur(rs.getInt("idTransporteur"), rs.getString("nomTransporteur"), rs.getString("telephoneTransporteur"), rs.getString("avisTransporteur"), rs.getInt("hideTransporteur")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;
    }
    
    //Recupere les coordonnees de la commande
    public Vector getAdresses(int idCoordonneesLiv, int idCoordonneesFact, Connexion con)
    {        
        Vector v = new Vector();
        String query = "select * from coordonnees where idCoordonnees = " + idCoordonneesLiv + " OR idCoordonnees = " + idCoordonneesFact;
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while(rs.next()) {              
                v.add(new Coordonnees(rs.getInt("idCoordonnees"),rs.getInt("idCivilite"), rs.getString("nomCoordonnees"), rs.getString("prenomCoordonnees"), 
                rs.getString("voieCoordonnees"), rs.getString("codePostalCoordonnees"), rs.getString("villeCoordonnees"),
                rs.getString("complementCoordonnees"), rs.getString("telephoneCoordonnees"), rs.getString("telephonePortableCoordonnees"),
                rs.getString("emailCoordonnees"), rs.getInt("typeCoordonnees")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;
    }
    
    
    //Recupere les Etats
    public Vector getEtats(Connexion con)
    {        
        Vector v = new Vector();
        String query = "select * from etat";
        
        //System.out.println(query);
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while(rs.next()) {
                
                v.add(new Etat(rs.getInt("idEtat"), rs.getInt("typeEtat"), rs.getString("nomEtat")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;
    }
    
    //Recupere les site paiement
    public Vector getSitePaiement(Connexion con)
    {        
        Vector v = new Vector();
        String query = "select * from sitePaiement";
        
        //System.out.println(query);
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while(rs.next()) {
                
                v.add(new SitePaiement(rs.getInt("idSitePaiement"), rs.getString("nomSitePaiement"), rs.getString("lienSitePaiement"), rs.getInt("hideSitePaiement")));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;
    }
    
    public int updateNombreLivre(Contenant cont, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE contenant set nbLivre = " + cont.getNbLivre()+ " where numeroCommande = '" + cont.getNumeroCommande() + "' AND isbnLivre = '" + cont.getIsbnLivre() + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updatePrixVente(Contenant cont, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE contenant set prixVente = " + cont.getPrixVente()+ " where numeroCommande = '" + cont.getNumeroCommande() + "' AND isbnLivre = '" + cont.getIsbnLivre() + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updateTVA(Contenant cont, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE contenant set TVA = " + cont.getTVA()+ " where numeroCommande = '" + cont.getNumeroCommande() + "' AND isbnLivre = '" + cont.getIsbnLivre() + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updatePromotion(Contenant cont, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE contenant set promotion = " + cont.getPromotion()+ " where numeroCommande = '" + cont.getNumeroCommande() + "' AND isbnLivre = '" + cont.getIsbnLivre() + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    
    public Vector rechercheBDDContenant(String numeroCommande, Connexion con)
    {
        Vector v = new Vector();
        
        String query = "SELECT * FROM contenant where numeroCommande = '" + numeroCommande + "';";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                v.add(new Contenant(rs.getString("isbnLivre"), rs.getString("numeroCommande"), rs.getInt("nbLivre"), rs.getFloat("prixVente"), rs.getFloat("TVA"), rs.getInt("promotion")));
            }     
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        
        return v;      
    }
    
    
    public int updateidTransporteurCommande(int idTransporteur, String numeroCommande, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE commande set idTransporteur = " + idTransporteur + " where numeroCommande = '" + numeroCommande + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updateidEtatCommande(int idEtat, String numeroCommande, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE commande set idEtat = " + idEtat + " where numeroCommande = '" + numeroCommande + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updateidSitePaiementCommande(int idSitePaiement, String numeroCommande, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE commande set idSitePaiement = " + idSitePaiement + " where numeroCommande = '" + numeroCommande + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
           // System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
    
    public int updateDateLivraisonCommande(String numeroCommande, String date, Connexion con )
    {
        int result = 0;
        
        String query = "UPDATE commande set dateLivraisonCommande = '" + date + "' where numeroCommande = '" + numeroCommande + "'";
        
        //System.out.println(query);
        
        try {
            Statement stmt = con.getConnexion().createStatement();
            
            result = stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            //System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return result;
        }
        
        return result;
    }
}
