package BDD;

import Objets.Coordonnees;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Hervé
 */
public class CoordonneesBDD {

    public Coordonnees rechercherCoordonnees(String email) {
//        System.out.println("juh " + email);
        Coordonnees objetCoordonnees = new Coordonnees();
        Connexion co = new Connexion();
        co.connexion();
        try {

            String requete;

            requete = "select * from coordonnees where emailCoordonnees  = '" + email + "';";
//            System.out.println(requete);
            Statement stmt = co.getConnexion().createStatement();

            ResultSet res = stmt.executeQuery(requete);

            while (res.next()) {

                objetCoordonnees.setIdCoordonnees(res.getInt("idCoordonnees"));
                objetCoordonnees.setIdCivilite(res.getInt("idCivilite"));
                objetCoordonnees.setNomCoordonnees(res.getString("nomCoordonnees"));
                objetCoordonnees.setPrenomCoordonnees(res.getString("PrenomCoordonnees"));
                objetCoordonnees.setVoieCoordonnees(res.getString("voieCoordonnees"));
                objetCoordonnees.setCodePostalCoordonnees(res.getString("codePostalCoordonnees"));
                objetCoordonnees.setVilleCoordonnees(res.getString("villeCoordonnees"));
                objetCoordonnees.setComplementCoordonnees(res.getString("complementCoordonnees"));
                objetCoordonnees.setTelephoneCoordonnees(res.getString("telephoneCoordonnees"));
                objetCoordonnees.setTelephonePortableCoordonnees(res.getString("telephonePortableCoordonnees"));
                objetCoordonnees.setEmailCoordonnees(res.getString("emailCoordonnees"));
                objetCoordonnees.setTypeCoordonnees(res.getInt("typeCoordonnees"));

            }

        } catch (Exception ex) {

            System.out.println("attention " + ex.getMessage());
        }

        return objetCoordonnees;
    }

    public int modifierCoordonnees(Coordonnees objetCoordonnees) {
        Connexion con = new Connexion();
        int result = 0;

        try {

            String requete = "UPDATE coordonnees SET idCivilite=?, "
                    + "  nomCoordonnees=? "
                    + ", prenomCoordonnees=? "
                    + ",voieCoordonnees=? "
                    + ",codePostalCoordonnees=? "
                    + ", villeCoordonnees=? "
                    + ", complementCoordonnees=? "
                    + ", telephoneCoordonnees=? "
                    + ", telephonePortableCoordonnees=? "
                    + ", emailCoordonnees=? "
                    + ", typeCoordonnees=? "
                    + " WHERE idCoordonnees= ? ;";

            PreparedStatement stmt = con.getConnexion().prepareStatement(requete);
            stmt.setInt(1, objetCoordonnees.getIdCivilite());
            stmt.setString(2, objetCoordonnees.getNomCoordonnees());
            stmt.setString(3, objetCoordonnees.getPrenomCoordonnees());
            stmt.setString(4, objetCoordonnees.getVoieCoordonnees());
            stmt.setString(5, objetCoordonnees.getCodePostalCoordonnees());
            stmt.setString(6, objetCoordonnees.getVilleCoordonnees());
            stmt.setString(7, objetCoordonnees.getComplementCoordonnees());
            stmt.setString(8, objetCoordonnees.getTelephoneCoordonnees());
            stmt.setString(9, objetCoordonnees.getTelephonePortableCoordonnees());
            stmt.setString(10, objetCoordonnees.getEmailCoordonnees());
            stmt.setInt(11, objetCoordonnees.getTypeCoordonnees());
            stmt.setInt(12, objetCoordonnees.getIdCoordonnees());

            result = stmt.executeUpdate();

            stmt.close();

        } catch (Exception ex) {

            System.out.println("oops" + ex.getMessage());
        }

        return result;
    }

}
