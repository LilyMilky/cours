package BDD;

import java.sql.Statement;

/**
 *
 * @author Hervé
 */
public class SitePaiementBDD {

    // La methode  qui ajoute un site 
    public int ajouterSitePaiement(String nomSitePaiement, String lienSitePaiement, int hideSitePaiement) {
        Connexion con = new Connexion();
        int result = 0;
        con.connexion();
        try {
            String requete = "insert into sitePaiement (nomSitePaiement,lienSitePaiement,hideSitePaiement)values('" + nomSitePaiement + "','" + lienSitePaiement + "'," + hideSitePaiement + ")";

            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(requete);

        } catch (Exception ex) {
            System.out.println("oops" + ex.getMessage());
        }
        con.close();
        return result;
    }

   // La methode qui permet de modifier un site de paiement  
    public int modifierSitePaiement(String nomSitePaiement, String lienSitePaiement, int hideSitePaiement, int idSitePaiement) {
        Connexion con = new Connexion();
        int result = 0;
        con.connexion();
        try {

            String requete = "Update sitePaiement set nomSitePaiement = '" + nomSitePaiement
                    + "',  lienSitePaiement = '" + lienSitePaiement
                    + "',  hideSitePaiement=" + hideSitePaiement
                    + " where idSitePaiement = " + idSitePaiement;

            Statement stmt = con.getConnexion().createStatement();

            result = stmt.executeUpdate(requete);

        } catch (Exception ex) {
            System.out.println(" oops" + ex.getMessage());
        }
        con.close();
        return result;
    }

}
