package BDD;

import Objets.MotCle;
import Objets.SousTheme;
import Objets.Theme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

/**
 *
 * @author Philippe Crombez
 */
public class ThemeBDD{
    
    // Methode pour remplir la liste de la frame JInternalFrameThemeAjout
    public Vector initVectorSousTheme(Connexion con){
        
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM sousTheme ORDER BY nomSousTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new SousTheme(rs.getInt("idSousTheme"), rs.getString("nomSousTheme")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Methode pour remplir la liste de la frame JInternalFrameThemeRecherche
    public Vector initVectorTheme(Connexion con){
        
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM theme ORDER BY nomTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new Theme(rs.getInt("idTheme"), rs.getString("nomTheme")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Methode pour enregistrer un nouveau theme dans la BDD
    public void enregistrerTheme(String nomDuTheme){
        Connexion con = new Connexion();
        
        con.connexion();
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO theme(nomTheme) VALUES('" + nomDuTheme + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        con.close();
    }
    
    // Methode pour remplir la table specialisation de la BDD
    public void associerThemeSousTheme(List<String> sousThemeSelectionnes){
        Connexion con = new Connexion();
        con.connexion();
        long idMax = idDerniereEntreeTheme(con);
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM sousTheme ORDER BY nomSousTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                Iterator it = sousThemeSelectionnes.iterator();
                while(it.hasNext()){
                    String compare = it.next().toString();
                    if(compare.equalsIgnoreCase(rs.getString("nomSousTheme"))){
                        long idSousTheme = rs.getLong("idSousTheme");
                        completerSpecialisation(con, idMax, idSousTheme);
                    }
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        con.close();
    }
    
    // Methode pour récupérer l'id du dernier thème entré
    public long idDerniereEntreeTheme(Connexion con){
        
        con.connexion();
        long idMax = 0;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            String query = "SELECT * FROM Theme ORDER BY idTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                idMax = rs.getLong("idTheme");
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        return idMax;
        
    }
    
    
   // Methode pour remplir la table specialisation de la BDD
    public void completerSpecialisation(Connexion con, long idMax, long idSousTheme){
        
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO specialisation(idTheme, idSousTheme) VALUES(" + idMax + ", " + idSousTheme + ")";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
    }
    
    // Methode pour vérifier si le nouveau thème n'existe pas.
    public boolean isExist(String nouveauTheme, Connexion con){
        
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM Theme ORDER BY nomTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if(nouveauTheme.equalsIgnoreCase(rs.getString("nomTheme"))){
                    return true;
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erreur. Veuillez rééssayer", "Erreur BDD", JOptionPane.ERROR_MESSAGE);
            return true;
        }
        return false;
    }
    
    // Methode pour insérer le nouveau sous-thème
    public void enregistrerSousTheme(String nomDuSousTheme){
        Connexion con = new Connexion();
        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "INSERT INTO sousTheme(nomSousTheme) VALUES('" + nomDuSousTheme + "')";
            stmt.executeUpdate(query);
            
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
        }
        con.close();
    }
    
    // Methode pour vérifier l'existance du nouveau sous-thème
    public boolean isExist(Connexion con, String nouveauSousTheme) {

        con.connexion();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM sousTheme ORDER BY nomSousTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if (nouveauSousTheme.equalsIgnoreCase(rs.getString("nomSousTheme"))) {
                    return true;
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        return false;
    }
    
    // Méthode pour modifier un théme
    public void modifierTheme(String ancienTheme, String themeModifie){
        Connexion con = new Connexion();
        con.connexion();
        long index=0;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM theme ORDER BY nomTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if (ancienTheme.equalsIgnoreCase(rs.getString("nomTheme"))) {
                    index = rs.getLong("idTheme");
                }
            }
            query = "UPDATE theme SET nomTheme=? FROM theme WHERE idTheme=?";
            PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
            stmt2.setString(1, themeModifie);
            stmt2.setLong(2, index);
            stmt2.executeUpdate();
            
            stmt2.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            System.out.println("Là ?");
        }
        
    }
    
    // Méthode pour modifier un sous-thème dans la BDD
    public void modifierSousTheme(String ancienSousTheme, String sousThemeModifie){
        Connexion con = new Connexion();
        con.connexion();
        long index=0;
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM sousTheme ORDER BY nomSousTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                if (ancienSousTheme.equalsIgnoreCase(rs.getString("nomSousTheme"))) {
                    index = rs.getLong("idSousTheme");
                }
            }
            query = "UPDATE sousTheme SET nomSousTheme=? FROM sousTheme WHERE idSousTheme=?";
            PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
            stmt2.setString(1, sousThemeModifie);
            stmt2.setLong(2, index);
            stmt2.executeUpdate();
            
            stmt2.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            
        }
        
    }

    // Methode pour retourner un vecteur pour initialiser la liste de theme avec la saisie rapide
    public Vector initVectorTheme(Connexion con, String nomTheme) {
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM theme WHERE nomTheme LIKE '" + nomTheme + "%' ORDER BY nomTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idTheme"), rs.getString("nomTheme")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }
    
    // Methode pour retourner un vecteur pour initialiser la liste de sous-theme avec la saisie rapide
    public Vector initVectorSousTheme(Connexion con, String nomSousTheme) {
        Vector v = new Vector();
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM sousTheme WHERE nomSousTheme LIKE '" + nomSousTheme + "%' ORDER BY nomSousTheme";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                v.add(new MotCle(rs.getInt("idSousTheme"), rs.getString("nomSousTheme")));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }
        return v;
    }

    // Methode qui retourne une liste d'entier (idSousTheme) pour la selection des sous-themes
    public List<Integer> lierSousTheme(Object selectedValue, Connexion con) {
        
        List<Integer> tempSousTheme = new ArrayList();
        
        Theme themeCourant = (Theme)selectedValue;
        
        int idTheme = themeCourant.getIdTheme();
        
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "SELECT * FROM specialisation WHERE idTheme=" + idTheme;
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                tempSousTheme.add(rs.getInt("idSousTheme"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return tempSousTheme;
        }
        
        return tempSousTheme;
    }

    // Methode pour enregistrer dans la table 'specialisation' l'idTheme et les idSousTheme liés
    public void modifierSpecialisation(int idTheme, List<SousTheme> listSousTheme, Connexion con) {
        try { 
            Statement stmt = con.getConnexion().createStatement();
            
            String query = "DELETE FROM specialisation WHERE idTheme=" + idTheme;
            stmt.executeUpdate(query);
            
            for(SousTheme tmp : listSousTheme){
                query = "INSERT INTO specialisation(idTheme, idSousTheme) VALUES(?,?)";
                PreparedStatement stmt2 = con.getConnexion().prepareStatement(query);
                stmt2.setInt(1, idTheme);
                stmt2.setInt(2, tmp.getIdSousTheme());
                stmt2.executeUpdate();
                stmt2.close();        
            }
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            System.out.println("ici ?");
        }
    }

    //Methode qui retourne une liste d'entier correspondant aux sous-thèmes sélectionnés
    public int[] selectionnerSousTheme(List<Integer> idSousTheme, ListModel tmpSousTheme) {
         int[] listSousThemeLie = new int[idSousTheme.size()];
            for(int i=0; i<idSousTheme.size(); i++){
                
                for (int j = 0; j < tmpSousTheme.getSize(); j++) {
                    SousTheme tmp = (SousTheme) tmpSousTheme.getElementAt(j);
                    if(tmp.getIdSousTheme() == idSousTheme.get(i)){
                        listSousThemeLie[i] = j;
                    }
                }
            }
        return listSousThemeLie;
    }
    
}
