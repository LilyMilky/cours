package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "servlet", urlPatterns = {"/servlet"})
public class servlet extends HttpServlet {
   
    
    
    private Cookie getCookie(Cookie[] cookies, String name)
    {
        if(cookies != null)
        {
            for(Cookie c : cookies){
                if(c.getName().equals((name)))
                return c;
            }
        }
        return null;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet servlet</title>");  
            
            Cookie c = getCookie(request.getCookies(), "NBTentative");
            int nbTentative = Integer.parseInt(c.getValue());
            
            if(!request.getParameter("Identifiant").equals("admin") && !request.getParameter("MDP").equals("root"))
            {    
                if(nbTentative < 3)
                {
                    c = new Cookie("NBTentative", Integer.toString(nbTentative + 1));   
                    c.setMaxAge(60*2);
                    response.addCookie(c);
                    out.println("<meta http-equiv='refresh' content='0 URL=formErreur.html'>");
                }
                else
                    out.println("<h1> Trop d'erreur, compte bloqué </h1>");
            }
            
            out.println("</head>");
            out.println("<body>");
            
            if(request.getParameter("Identifiant").equals("admin") && request.getParameter("MDP").equals("root"))
                  out.println("<h1> Bienvenue Admin ! </h1>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
