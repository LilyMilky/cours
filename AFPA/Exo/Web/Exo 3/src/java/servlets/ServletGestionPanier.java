package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "servlet", urlPatterns = {"/servlet"})
public class ServletGestionPanier extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>EXO 3</title>");
            out.println("</head>");
            out.println("<body>");

            //JDBC
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (ClassNotFoundException ex) {
                out.println("Probleme de base de données"
                        + "<!--" //Commentaire
                        + "Oops:Driver:" + ex.getMessage()
                        + " -->");
                return;
            }
            Connection connexion = null;
            try {
                connexion = DriverManager.getConnection(
                        "jdbc:sqlserver://localhost:1433;databaseName=maBase;user=sa;password=sa");
            } catch (SQLException ex) {
                out.println("Probleme de base de données"
                        + "<!--" //Commentaire
                        + "Oops:Connection:" + ex.getErrorCode() + ":" + ex.getMessage()
                        + " -->");
                return;
            }

            String query = "SELECT * FROM Produits ORDER BY ReferenceProduit;";
            try {
                Statement stmt = connexion.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                out.println("<FORM Action='servlet'>");
                out.println("<SELECT NAME = 'Reference'>");    //ComboBox Name = nom parametre

                while (rs.next()) {

                    out.println("<OPTION Value='" + rs.getString("ReferenceProduit") + "'>" //Value = valeur du parametre
                            + rs.getString("ReferenceProduit")
                            + "</OPTION>");
                }

                out.println("</SELECT>");
                out.println("Quantité : <input type='text' name='Quantite' value='' />");
                out.println("<br>");

                out.println("<INPUT TYPE ='SUBMIT' NAME='doIt' Value='Ok'/>");
                out.println("</FORM");

                rs.close();
                stmt.close();
            } catch (SQLException ex) {
                out.println("Probleme de base de données"
                        + "<!--" //Commentaire
                        + "Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage()
                        + " -->");
                return;
            }

            try {
                connexion.close();
            } catch (SQLException ex) {
                out.println("Probleme de base de données"
                        + "<!--" //Commentaire
                        + "Oops:Close:" + ex.getErrorCode() + ":" + ex.getMessage()
                        + " -->");
                return;
            }

            HttpSession session = request.getSession();
            if (request.getParameter("Ordre") == null) {
                if (session.getAttribute("COMPTEUR") == null) {
                    session.setAttribute("COMPTEUR", 1);
                } else {

                    Integer i = (Integer) session.getAttribute("COMPTEUR");
                    session.setAttribute("COMPTEUR", ++i);
                }

                if (request.getParameter("Reference") != null && request.getParameter("Quantite") != null) {
                    session.setAttribute(("Reference" + session.getAttribute("COMPTEUR")), request.getParameter("Reference"));
                    session.setAttribute(("Quantite" + session.getAttribute("COMPTEUR")), request.getParameter("Quantite"));
                }
            }
//            out.println("<form name='Form' action='ServletGestionPanier'>");
//            out.println("Ref : <input type='text' name='Reference' value='' />");
//            out.println("<br>");
//            out.println("Quantité : <input type='text' name='Quantite' value='' />");
//            out.println("<br>");
//            out.println("<input type='submit' value='Ajouter au panier' name='doIt' />");
//            out.println("</form> ");

            out.println("Contenu du panier : ");
            out.println("<br>");
            if((Integer)session.getAttribute("COMPTEUR") == 1)
            {
                out.println("Le panier est vide");
                out.println("<br>");
            }

            //out.println(request.getParameter("Ordre"));
            if (request.getParameter("Ordre") != null) {
                //Plus
                if (request.getParameter("i") != null && request.getParameter("Ordre").equals("1")) {
                    int val = Integer.parseInt(session.getAttribute("Quantite" + request.getParameter("i")).toString());
                    session.setAttribute("Quantite" + request.getParameter("i"), ++val);
                } //Moins
                else if (request.getParameter("i") != null && request.getParameter("Ordre").equals("2")) {
                    int val = Integer.parseInt(session.getAttribute("Quantite" + request.getParameter("i")).toString());
                    session.setAttribute("Quantite" + request.getParameter("i"), --val);
                } //Suppr
                else if (request.getParameter("i") != null && request.getParameter("Ordre").equals("3")) {
                    session.removeAttribute("Reference" + request.getParameter("i"));
                    session.removeAttribute("Quantite" + request.getParameter("i"));
                } //Suppr all
                else if (request.getParameter("Ordre").equals("4")) {
                    out.println("<BR>Le panier est vide");
                    session.invalidate();
                }
            }

            for (int i = 1; i <= Integer.parseInt(session.getAttribute("COMPTEUR").toString()); i++) {
                if (session.getAttribute("Reference" + i) != null) {
                    if (session.getAttribute("Quantite" + i) != null) {
                        out.println(session.getAttribute("Quantite" + i) + "  ");
                        out.println(session.getAttribute("Reference" + i) + "  ");
                        out.println("<a href='servlet?Ordre=1&amp;i=" + i + "'>+</a>" + " ");
                        out.println("<a href='servlet?Ordre=2&amp;i=" + i + "'>-</a>" + " ");
                        out.println("<a href='servlet?Ordre=3&amp;i=" + i + "'>X</a>" + " ");
                        out.println("<br>");
                    }
                }
            }

            if((Integer)session.getAttribute("COMPTEUR") != 1)            
                out.println("<a href='servlet?Ordre=4'>Vider le panier</a>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request ServletGestionPanier request
     * @param response ServletGestionPanier response
     * @throws ServletException if a ServletGestionPanier-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request ServletGestionPanier request
     * @param response ServletGestionPanier response
     * @throws ServletException if a ServletGestionPanier-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the ServletGestionPanier.
     *
     * @return a String containing ServletGestionPanier description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
