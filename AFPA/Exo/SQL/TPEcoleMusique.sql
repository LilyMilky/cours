use ecoleMusique


--Ajout enumeration niveau session
Alter Table session
add constraint enumNiveauSession
Check(niveauSession ='DEBUTANT' or niveauSession = 'MEDIUM' or niveauSession = 'AVANCE')

--Enumeration solfege

Alter Table inscription
add constraint enumsolfege
Check(solfege ='1' or solfege = '2')

--Ajout contrainte inscription un eleve a une session unique.
Alter table inscription
add constraint InscriptionSessionUnique
Unique(idEleve,codeSession)

--Trigger qui g�n�re automatiquement le code session (3lettre nom instrument + 3 lettre niveau + mois + ann�e)

create trigger autoCodeSession 
--Table pour trigger
on session
-- Quel evenement
for insert , update
--Debut script
as
-- Script creation code session
update session set 
codeSession=Upper(Concat(SUBSTRING(nomInstrument,1,3),SUBSTRING(niveauSession,1,3),
SUBSTRING(CAST(dateDebutSession as varchar(11)),6,2),substring(cast(dateDebutSession as varchar(11)),1,4)))

--Test trigger

select * from session

insert into instrument(nomInstrument)
values('Flute')

insert into session(codeSession,nomInstrument,dateDebutSession,dateFinSession,niveauSession)
values('','Flute','2015-12-14','2016-12-14','Debutant')


--Creation Eleve + Session

Insert into eleve(nomEleve,prenomEleve,dateNaissanceEleve)
values('Dupont','Jean','1980-11-19'),
('Vandamn','Jean-Claude','1970-05-10'),
('Line','Krystal','1985-02-23'),
('Bond','James','1975-08-05')

select * from eleve

insert into instrument(nomInstrument)
values('Violon'),
('Violoncelle'),
('Guitare'),
('Piano'),
('Basse'),
('Saxophone')

select * from instrument

insert into session(codeSession,nomInstrument,dateDebutSession,dateFinSession,niveauSession)
values('1','Violon','2015-08-14','2016-02-14','Avance'),
('2','Piano','2015-10-20','2016-04-20','Debutant'),
('3','Guitare','2016-01-01','2016-07-01','Medium'),
('4','Saxophone','2016-03-10','2016-09-10','Avance'),
('','Violoncelle','2015-03-17','2016-09-17','Debutant')

select * from session

--Inscription Eleve a session

insert into inscription(idEleve,codeSession,dateInscription,solfege)
values('1','PIADEB102015','2015-12-14','2'),
('3','FLUDEB122015','2015-12-14','2'),
('4','GUIMED012016','2015-12-14','1'),
('5','SAXAVA032016','2015-12-14','2'),
('5','VIOAVA082015','2015-12-14','2')

select * from inscription

--Afficher les nom et pr�nom de chaque �l�ve, ainsi que les sessions auxquelles 
--il s�est inscrit, ainsi que le niveau et la discipline correspondant � chaque session.

Select nomEleve,prenomEleve,s.codeSession,niveauSession,nomInstrument 
from eleve as e, session as s,inscription as i
where e.idEleve = i.idEleve and i.codeSession = s.codeSession