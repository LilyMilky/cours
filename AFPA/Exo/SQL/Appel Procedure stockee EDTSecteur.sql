use zoo

--Declare Variable pour la procedure
Declare @VARidSecteur bigint,
@VARheureDebut datetime,
@VARheureFin dateTime

--Valeur de test
set @VARidSecteur=2
set @VARheureDebut='15-12-2015 08:00:00'
set @VARheureFin='15-12-2015 18:00:00'


EXEC dbo.AffichageEdtSecteur @VARidSecteur,@VARheureDebut,@VARheureFin