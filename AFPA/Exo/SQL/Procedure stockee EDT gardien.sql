--Procedure stockee permettant d'afficher l'emploi du temps d'un secteur avec les infos

--create procedure [dbo].[affichageEdtGardien]
--On liste les parametres re�u en entr�e et renvoy�
Declare @VARidSecteur bigint,
@VARheureDebut datetime,
@VARheureFin datetime,
@VARavsEmploye varchar(13),
@VARjourDebut dateTime,
@VARjourFin dateTime

--as

set @VARidSecteur=2
set @VARheureDebut='15-12-2015 08:00:00'
set @VARheureFin='15-12-2015 18:00:00'
set @VARavsEmploye = '1111111111111'
set @VARjourDebut ='15-12-2015 08:00:00'
set @VARjourFin ='18-12-2015 18:00:00'


Declare @VARaffichage varchar(500),
@VARAffichageGardien varchar(100),
@VARparcelleEnCours bigint,
@VARSecteur varchar(50)

set @VARaffichage = ''
set @VARAffichageGardien = ''

--Declaration d'un cursor(tableau) pour recuperer les idParcelles d'un secteur
Declare curIdParcelles CURSOR scroll
for
--Recuperation de toutes les parcelles li�s a un secteur
Select idParcelle from parcelle where idSecteur = @VARidSecteur order by numeroParcelle
--Les idParcelles sont maintenant dans le cursor

-- Ouverture du cursor
open curIdParcelles
--On recupere la valeur du 1er indice
Fetch curIdParcelles
--On affecte la valeur en cours a une variable
Into @VARparcelleEnCours

--Recup nom gardien
set @VARAffichageGardien = 'Emploi du temps de: ' + (cast( (select prenomEmploye from employe 
where numeroAVSEmploye = @VARavsEmploye) as varchar(50)))
 + ' ' + (cast( (select nomEmploye from employe 
where numeroAVSEmploye = @VARavsEmploye) as varchar(50)))
--Affichage prenom + nom gardien
print @VARAffichageGardien

--Affichage semaine du xx-xx au xx-xx

-- Affichage Jour: Secteur xxx,		parcelle x-x-x-x-x-x-x
while @VARheureDebut < @VARheureFin
	begin
		set @VARaffichage += cast((select numeroParcelle from parcelle as p, surveillance as s where p.idParcelle = s.idParcelle and s.numeroAVSEmploye = @VARavsEmploye and jourHeureSurveillance = @VARheureDebut) as varchar(10)) + '-'

		set @VARheureDebut = dateadd(hour,1,@VARheureDebut)

		print @VARaffichage

	end

 print 'coucou'
print @VARaffichage

set @VARSecteur = (select fonctionSecteur from secteur where idSecteur = @VARidSecteur)

print cast(Format(@VARheureDebut, 'dddd dd/MM/yyyy') as varchar(50)) + '	Secteur ' + @VarSecteur +',		Parcelles ' + @VARaffichage 

Close curIdParcelles
Deallocate curIdParcelles
