
--Procedure stockee permettant d'afficher l'emploi du temps d'un secteur avec les infos
/*Secteur
  Jour
		  Parcelle       Parcelle      Parcelle    .....
  Heure   gardien		 gardien	   gardien*/

alter procedure [dbo].[AffichageEdtSecteur]
--On liste les parametres re�u en entr�e et renvoy�
@VARidSecteur bigint,
@VARheureDebut datetime,
@VARheureFin dateTime

as

Declare 
@VARnomSecteur varchar(50),
@VARmemoGardien varchar(300),
@VARaffichageParcelle varchar(500),
@VARparcelleEnCours bigint


set @VARaffichageParcelle =''
set @VARmemoGardien = ''

--Declaration d'un cursor(tableau) pour recuperer les idParcelles d'un secteur
Declare curIdParcelles CURSOR scroll
for
--Recuperation de toutes les parcelles li�s a un secteur
Select idParcelle from parcelle where idSecteur = @VARidSecteur order by numeroParcelle
--Les idParcelles sont maintenant dans le cursor

-- Ouverture du cursor
open curIdParcelles
--On recupere la valeur du 1er indice
Fetch curIdParcelles
--On affecte la valeur en cours a une variable
Into @VARparcelleEnCours


set @VARnomSecteur = (Select fonctionSecteur from Secteur where idSecteur = @VARidSecteur)
PRINT ('Secteur: ' + cast(@VARnomSecteur as varchar(50)))

Print Concat(Upper(Substring(Format(@VARheureDebut,'dddd'),1,1)), Substring(Format(@VARheureDebut,'dddd dd/MM/yyyy'),2,15))
--Recuperation de l'id de chaque parcelle
while @@FETCH_STATUS = 0

Begin
	set @VARaffichageParcelle += ('			Parcelle ' + cast((Select numeroParcelle from parcelle where idParcelle = @VARparcelleEnCours) as varchar(30)) + '	')

	Fetch next from curIdParcelles into @VARparcelleEnCours
End
--Affichage des parcelles
print @VARaffichageParcelle



WHILE @VARheureDebut < @VARheureFin

Begin
	--On reinitialise les codesmemos
	set @VARmemoGardien = ''

	--On reprend l'id de la premiere parcelle a chaque changement d'heure
	fetch first from curIdParcelles Into @VARparcelleEnCours

	--On recupere chaque code memo pour les gardiens pour une heure.
	while @@FETCH_STATUS = 0
	begin
		set @VARmemoGardien += cast((select codeMemoEmploye from employe as e,surveillance as s 
		where e.numeroAVSEmploye = s.numeroAVSEmploye and @VARparcelleEnCours = s.idParcelle and s.jourHeureSurveillance = @VARheureDebut) as varchar(3)) + '							'

		Fetch next from curIdParcelles into @VARparcelleEnCours
	end

	--On affiche l'heure + le code memo de chaque gardien pour chaque parcelle
	print (Format(@VarheureDebut, 'hh:mm			')) + @VARmemoGardien
	--On incremente l'heure et on recommence
	set @VARheureDebut = dateadd(hour,1,@VARheureDebut)
END

Close curIdParcelles
Deallocate curIdParcelles