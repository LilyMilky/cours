create trigger ForInsertUpdateeffectuer
on effectuer
for insert, update
as

Declare @idMaintenanceEnCours bigint

declare ListeIdMaintenance cursor FOR
select idMaintenance from effectuer 
open ListeIdMaintenance
Fetch ListeIdMaintenance into @idMaintenanceEnCours


while @@FETCH_STATUS = 0
begin

	select i.matriculeMecanicien from effectuer as e join inserted as i on e.idMaintenance = i.idMaintenance where i.idMaintenance = @idMaintenanceEnCours 

	if @@ROWCOUNT < 2
	begin

		delete from effectuer where idMaintenance = @idMaintenanceEnCours
		print 'inserer au moins 2 meca PLS'

	end

	fetch next from ListeIdMaintenance into @idMaintenanceEnCours

end


Close ListeIdMaintenance
Deallocate ListeIdMaintenance



--drop trigger ForInsertUpdateeffectuer