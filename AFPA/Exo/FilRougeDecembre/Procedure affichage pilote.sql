/*Cr�er une proc�dure qui permet d�afficher pour chaque pilote son nom, son adresse, son num�ro de t�l�phone, son num�ro de brevet de pilote 
et les types d'avion qu'il est habilit� � piloter avec le nombre total de vols qu'il a effectu� sur chacun de ces types. */

create procedure [dbo].[AffichagePilote]

as

Declare @Affichage varchar(1000),
@PiloteEnCours bigint,
@TypeEnCours bigint

Declare pilote CURSOR scroll for
Select idPilote from pilote where idPilote is not null

Declare typeAvion CURSOR scroll for
Select idType from type
open typeAvion
Fetch typeAvion into @TypeEnCours

open pilote
Fetch pilote
Into @PiloteEnCours


while @@FETCH_STATUS = 0
begin
	set @Affichage = ''

	set @Affichage = 'Pilote : ' + (select nomCoordonnees from coordonnees where @PiloteEnCours = idPilote)
	+ ' Adresse: ' + cast((select numeroCoordonnees from coordonnees where @PiloteEnCours = idPilote) as varchar(3))
	+ ' ' + (select voieCoordonnees from coordonnees where @PiloteEnCours = idPilote)
	+ ' ' + (select cpCoordonnees from coordonnees where @PiloteEnCours = idPilote)
	+ ' ' + (select paysCoordonnees from coordonnees where @PiloteEnCours = idPilote)

	
	fetch first from typeAvion Into @TypeEnCours
	while @@Fetch_Status = 0
	begin	
		if Exists(select * from habilitation where @PiloteEnCours = idPilote and @TypeEnCours = idType)
		begin 
			set @Affichage += '  --  Numero de brevet: ' + (select numeroBrevetPilote from habilitation where @PiloteEnCours = idPilote and @TypeEnCours = idType)		
		end
		Fetch next from typeAvion into @TypeEnCours
	end

	fetch first from typeAvion Into @TypeEnCours
	while @@Fetch_Status = 0
	begin	
		if Exists(select * from habilitation where @PiloteEnCours = idPilote and @TypeEnCours = idType)
		begin 
			set @Affichage += '  --  Type d avion: ' + (select nomModeleType from habilitation as h, type as t where @PiloteEnCours = idPilote and  h.idType = t.idType and @TypeEnCours = h.idType)
			+ '  --  Nombre de vol: ' + cast((select nbTotalVol from habilitation as h, type as t where @PiloteEnCours = idPilote and  h.idType = t.idType and @TypeEnCours = h.idType) as varchar(10))			
		end
		Fetch next from typeAvion into @TypeEnCours
	end

	print @Affichage

	Fetch next from pilote into @PiloteEnCours
end

Close pilote
Deallocate pilote
Close typeAvion
deallocate typeAvion

drop procedure AffichagePilote
