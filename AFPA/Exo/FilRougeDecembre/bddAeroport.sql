create database Aeroport

use Aeroport

------------------------------------------------------------

create table proprietaire(idProprietaire bigint identity(1,1) not null,
nomSociete varchar(150),
typeProprietaire  char(1)not null,

constraint pkidProprietaire primary key(idProprietaire))

------------------------------------------------------------------------------

create table pilote(idPilote bigint identity(1,1) not null,
dateEmbauchePilote date not null,
dateFinContratPilote date,

constraint pkidPilote primary key(idPilote))

---------------------------------------------------------------------------------------

create table mecanicien(matriculeMecanicien varchar(50) not null,
dateEmbaucheMecanicien date not null,
dateFinContratMecanicien date,

constraint pkmatriculeMecanicien primary key(matriculeMecanicien))

-----------------------------------------------
create table coordonnees(idCoordonnees bigint identity(1,1) not null,
idPilote bigint,
idProprietaire bigint,
matriculeMecanicien varchar(50),
nomCoordonnees varchar(150) not null,
prenomCoordonnees varchar(150) not null,
civiliteCoordonnees char(1) not null,
numeroCoordonnees smallint not null,
voieCoordonnees varchar(150) not null,
cpCoordonnees varchar(5) not null,
villeCoordonnees varchar(150) not null,
paysCoordonnees varchar(50) not null,
numeroTelephoneCoordonnees varchar(20)not null,

constraint pkidCoordonnees primary key(idCoordonnees),
constraint fkidPiloteCoordonees foreign key(idPilote) references pilote(idPilote),
constraint fkidProprietaireCoordonees foreign key(idProprietaire) references proprietaire(idProprietaire),
constraint fkmatriculeMecanicienCoordonees foreign key(matriculeMecanicien) references mecanicien(matriculeMecanicien))


--------------------------------------------------------------------------------------

create table type(idType bigint identity(1,1) not null,
nomModeleType varchar(50) not null,
nomConstructeurType varchar(100) not null,
puissanceType int not null,
nbPlacesType int not null,

constraint pkidType primary key(idType))

------------------------------------------------------------------------------------

create table habilitation(idType bigint not null,
idPilote bigint not null,
nbTotalVol int not null,
numeroBrevetPilote varchar(13) not null,

constraint pkHabilitation primary key(idType,idPilote),
constraint fkidType foreign key (idType) references type(idType),
constraint fkidPilote foreign key (idPilote) references pilote(idPilote))

--------------------------------------------------------------------------------

create table maitrise(idType bigint not null,
matriculeMecanicien varchar(50) not null

constraint pkmaitrise primary key(idType,matriculeMecanicien),
constraint fkidTypeMaitrise foreign key (idType) references type(idType),
constraint fkmatriculeMecanicien foreign key (matriculeMecanicien) references mecanicien(matriculeMecanicien))

---------------------------------------------------------------------------------------------

create table avion(immatriculationAvion varchar(8) not null,
idProprietaire bigint not null,
idType bigint not null,
dateAchatAvion date not null,

constraint pkimmatriculationAvion primary key(immatriculationAvion),
constraint fkidTypeAvion foreign key (idType) references type(idType),
constraint fkidProprietaire foreign key (idProprietaire) references proprietaire(idProprietaire))

-----------------------------------------------------------------------------------------------

create table maintenance(idMaintenance bigint identity(1,1) not null,
immatriculationAvion varchar(8) not null,
objetMaintenance varchar(1000) not null,
dateDebutMaintenance datetime not null,
dateFinMaintenance datetime,


constraint pkidmaintenance primary key(idMaintenance),
constraint fkimmatriculationAvion foreign key (immatriculationAvion) references avion(immatriculationAvion))

-------------------------------------------------------------------------------------------------

create table effectuer(idMaintenance bigint not null,
matriculeMecanicien varchar(50) not null,

constraint pkeffectuer primary key(idMaintenance,matriculeMecanicien),
constraint fkidMaintenance foreign key (idMaintenance) references maintenance(idMaintenance),
constraint fkmatriculeMecanicienEffectuer foreign key (matriculeMecanicien) references mecanicien(matriculeMecanicien))


--drop database Aeroport


