/*Cr�er une proc�dure stock�e qui permet d�afficher toutes les maintenance effectu�es sur chaque avion, 
ainsi que les noms , pr�noms et matricules des m�caniciens qui ont effectu� chaque maintenance. */

create procedure [dbo].[maintenanceAvion]

as

Declare @Affichage varchar(1000),
@MaintenanceEnCours bigint,
@MecanicienEnCours varchar(50),
@idMaintenancePrec bigint,
@MecaPrec varchar(50)

set @idMaintenancePrec = 0
set @MecaPrec = '0'

Declare maintenance CURSOR for
Select idMaintenance from effectuer

open maintenance
Fetch maintenance Into @MaintenanceEnCours

Declare mecaniciens CURSOR scroll for
Select matriculeMecanicien from effectuer order by matriculeMecanicien
open mecaniciens
Fetch mecaniciens into @MecanicienEnCours 

while @@FETCH_STATUS = 0
begin
	
	if @idMaintenancePrec <> @MaintenanceEnCours
	begin
		set @Affichage = ''

		set @Affichage = 'Avion: ' + (select immatriculationAvion from maintenance where @MaintenanceEnCours = idMaintenance)
		+ ' Raison: ' + (select objetMaintenance from maintenance where @MaintenanceEnCours = idMaintenance)
		+ ' DateDebut: ' + cast((select dateDebutMaintenance from maintenance where @MaintenanceEnCours = idMaintenance) as varchar(20))
		+ ' DateFin: ' + cast((select dateFinMaintenance from maintenance where @MaintenanceEnCours = idMaintenance) as varchar(20))
		+ '  --  Mecaniciens: '

		fetch first from mecaniciens Into @MecanicienEnCours
		while @@FETCH_STATUS = 0
		begin

			if (exists(select * from effectuer where matriculeMecanicien = @MecanicienEnCours and idMaintenance = @MaintenanceEnCours) and @MecaPrec <> @MecanicienEnCours)
			begin
				set @Affichage += (select nomCoordonnees from coordonnees as c, effectuer as e where @MaintenanceEnCours = e.idMaintenance and e.matriculeMecanicien = @MecanicienEnCours and e.matriculeMecanicien = c.matriculeMecanicien )
				+ ' ' + (select prenomCoordonnees from coordonnees as c, effectuer as e where @MaintenanceEnCours = e.idMaintenance and e.matriculeMecanicien = @MecanicienEnCours and e.matriculeMecanicien = c.matriculeMecanicien )
				+ ' ' + @MecanicienEnCours + ' -- ' 
			end
			set @MecaPrec = @MecanicienEnCours
			Fetch next from mecaniciens into @MecanicienEnCours

		end

		print @Affichage
	end

	set @idMaintenancePrec = @MaintenanceEnCours
	Fetch next from maintenance into @MaintenanceEnCours
end

Close maintenance
Deallocate maintenance
Close mecaniciens
Deallocate mecaniciens

--drop procedure [maintenanceAvion]

--select * from effectuer