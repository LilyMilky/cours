use aeroport
GO

INSERT INTO mecanicien(matriculeMecanicien, dateEmbaucheMecanicien)
VALUES 
	('1', '1995-07-01'),
	('2', '1980-01-01'),
	('3', '1991-10-15'),
	('4', '1970-08-20'),
	('5', '1986-12-01'),
	('6', '1975-04-06'),
	('7', '1990-06-01'),
	('8', '1996-12-01')

INSERT INTO pilote(dateEmbauchePilote)
VALUES 
	('1995-07-01'),
	('1980-01-01'),
	('1991-10-15'),
	('1970-08-20'),
	('1986-12-01'),
	('1975-04-06'),
	('1990-06-01'),
	('1996-12-01')

INSERT INTO proprietaire(nomSociete, typeProprietaire)
VALUES
	('RENAULT', 'S'),
	('GOOGLE', 'S'),
	('GEFCO', 'S'),
	('TOTAL', 'S'),
	('CHRISTIAN DIOR', 'S'),
	('AIR CHINA', 'S'),
	('JAL', 'S')

INSERT INTO coordonnees(nomCoordonnees, prenomCoordonnees, civiliteCoordonnees, numeroCoordonnees,
		 voieCoordonnees, cpCoordonnees, villeCoordonnees, paysCoordonnees, numeroTelephoneCoordonnees, 
		 idPilote, idProprietaire, matriculeMecanicien)
VALUES
	('Irise', 'Marthe', 'F', 9,'Rue de la Paix', 75000, 'Paris', 'france', '01.43.25.36.75', NULL,NULL, '1'),
	('TANEN', 'Biff', 'M', 9,'Rue de la Paix', 75000, 'Paris', 'france', '01.43.25.36.75', NULL,NULL, '2'),
	('McFly', 'Marty', 'M', 9,'Rue de la Paix', 75000, 'Paris', 'france', '01.43.25.36.75', 1,NULL, NULL),
	('Doubtfire', 'Iphigenie', 'F', 62,'Rue de joie', 75000, 'Paris', 'france', '01.43.37.53.58',NULL, 1, NULL),
	('Pan', 'Peter', 'M', 463,'Rue du G�n�ral De Gaulle', 75000, 'Paris', 'france', '01.43.34.35.27',NULL, 1,NULL),
	('O''Neil', 'Jack', 'M', 3,'Rue Mar�chal Foch', 75000, 'Paris', 'france', '01.43.62.15.75',NULL, 2,NULL),
	('Carter', 'Samantha', 'F', 96,'Rue Andr� Dessaux', 75000, 'Paris', 'france', '01.43.33.47.80',NULL,NULL, '3'),
	('Gates', 'Bill', 'M', 759,'Rue Marcel Proust', 75000, 'Paris', 'france', '01.43.13.89.68',NULL, 3,NULL),
	('Roxet', 'Roucky', 'M', 64,'Rue de Bourgogne', 75000, 'Paris', 'france', '01.43.83.82.34', 2,NULL,NULL),
	('Dallas', 'Corben', 'M', 7,'Rue de la Republique', 75000, 'Paris', 'france', '01.43.95.83.97',NULL, 4,NULL),
	('Renault', 'Megane', 'F', 168,'Rue des Grands Champs', 75000, 'Paris', 'france', '01.43.57.36.57',NULL,NULL, '4'),
	('Brown', 'Emeth', 'M', 124,'Rue de Patay', 75000, 'Paris', 'france', '01.43.28.30.18', 3,NULL,NULL),
	('Moranne', 'Bob', 'M', 54,'Rue du Quebec', 75000, 'Paris', 'france', '01.43.59.96.96',NULL, 5,NULL),
	('Obiwan', 'Kenobi', 'M', 1,'Rue du louvre', 75000, 'Paris', 'france', '01.43.96.36.63', 4,NULL,NULL),
	('Amidala', 'Padm�', 'F', 23,'Rue du G�n�ral Leclerc', 75000, 'Paris', 'france', '01.43.20.20.27',NULL,NULL, '5'),
	('Potter', 'Harry', 'M', 94,'Rue des petits champs', 75000, 'Paris', 'france', '01.43.94.73.87',NULL,NULL, '6'),
	('Holmes', 'Sherlock', 'M', 16,'Rue Notre Dame de Recouvrance', 75000, 'Paris', 'france', '01.43.21.82.00', 5,NULL,NULL),
	('Christie', 'Agatha', 'F', 282,'Rue du Bourdon Blanc', 75000, 'Paris', 'france', '01.43.35.08.11',NULL, 6,NULL),
	('Wayne', 'Bruce', 'M', 86,'Avenue Alexandre Martin', 75000, 'Paris', 'france', '01.43.38.95.00', 6,NULL,NULL),
	('Pendragon', 'Arthur', 'M', 72,'Rue de la croix morin', 75000, 'Paris', 'france', '01.43.93.52.37', 8, NULL, NULL), 
	('Jackson', 'Daniel', 'M', 14,'Avenue des Champs Elys�e', 75000, 'Paris', 'france', '01.43.23.49.63', 7,NULL,NULL),
	('Oswald', 'Clara', 'F', 55,'Rue Voltaire', 75000, 'Paris', 'france', '01.43.44.32.04', NULL,NULL,'8'),
	('Allen', 'Barry', 'M', 37,'Rue Rousseau', 75000, 'Paris', 'france', '01.43.69.83.15',NULL,NULL, '7')

INSERT INTO type(nomModeleType, nomConstructeurType, puissanceType, nbPlacesType)
VALUES	
	('A320', 'AIRBUS', 14515, 220),
	('B757', 'BOEING', 30940, 279),
	('B747', 'Boeing', 26310, 660),
	(' A340', 'Airbus', 27216, 440),
	('A330', 'Airbus', 32250, 440),
	('B777', 'Boeing', 52163, 550),
	('A321', 'AIRBUS', 14515, 220),
	('B758', 'BOEING', 30940, 279),
	('B748', 'Boeing', 26310, 660),
	('A341', 'Airbus', 27216, 440),
	('A331', 'Airbus', 32250, 440),
	('B778', 'Boeing', 52163, 550)

INSERT INTO avion(immatriculationAvion, dateAchatAvion, idProprietaire, idType)
VALUES	
	('YA-336', '1985-03-23', 1, 1),
	('F-1385', '2008-03-31', 2, 2),
	('OH-864', '1995-10-08', 3, 3),
	('JA321I', '1991-06-15', 4, 3),
	('4U-986', '2012-01-03', 5, 6),
	('VH-656', '2015-01-03', 6, 4),
	('G-4698', '2002-12-10', 7, 5)

INSERT INTO habilitation(idType, idPilote, nbTotalVol, numeroBrevetPilote)
VALUES 
	(6, 1, 50, 'BB 9869055986'),
	(3, 2, 150, 'BB 2040908422'),
	(1, 3, 200, 'BB 7321527654'),
	(4, 4, 175, 'BB 6652667478'),
	(5, 5, 300, 'BB 3606244328'),
	(6, 6, 350, 'BB 9408875137'),
	(3, 7, 325, 'BB 4577902561'),
	(2, 8, 398, 'BB 8799870772'),
	(3, 1, 50, 'BB 9869055986'),
	(4, 2, 150, 'BB 2040908422'),
	(6, 3, 200, 'BB 7321527654'),
	(1, 4, 175, 'BB 6652667480'),
	(2, 5, 300, 'BB 3606244328'),
	(5, 6, 350, 'BB 9408875137'),
	(4, 7, 325, 'BB 3477902561'),
	(1, 8, 398, 'BB 8799870772')

INSERT INTO maintenance(immatriculationAvion, objetMaintenance, dateDebutMaintenance, dateFinMaintenance)
VALUES
	('YA-336', 'R�acteur', '20150101 08:00', '20150101 18:00'),
	('F-1385', 'Cockpit', '20150210 08:00', '20150212 17:00'),
	('OH-864', 'Train d''atterisssage', '20150113 08:00', '20150113 14:00'),
	('JA321I', 'Aile droite', '20150331 08:00', '20150401 18:00'),
	('4U-986', 'Volets', '20150101 08:00:00', '20150101 14:00'),
	('VH-656', 'R�acteur', '20150101 08:00:00', '20150101 14:00'),
	('G-4698', 'R�acteur', '20150101 08:00:00', '20150101 14:00')

INSERT INTO effectuer(idMaintenance, matriculeMecanicien)
VALUES
	(4, 'CarSam7'),	(4, 'TANBif2'),	(5, 'AmiPad15'), (5, 'PotHar16'), (5, 'OswCla22'), (6, 'PotHar16'), (6, 'IriMar1'),
	(6, 'RenMeg11'),(7, 'AmiPad15'),(7, 'CarSam7'),(1, 'IriMar1'),(1, 'CarSam7'),
	(3, 'TANBif2'), (3, 'AmiPad15'), (2, 'TANBif2'), (3, 'RenMeg11')

INSERT INTO maitrise(matriculeMecanicien, idType)
VALUES
	('IriMar1', 1), ('IriMar1', 12),
	('TANBif2', 2), ('TANBif2', 11),
	('CarSam7', 3), ('CarSam7', 10),
	('RenMeg11', 4), ('RenMeg11', 5),
	('AmiPad15', 5), ('AmiPad15', 2),
	('PotHar16', 6), ('PotHar16', 9),
	('OswCla22', 7), ('OswCla22', 10),
	('AllBar23', 8), ('AllBar23', 12)

	