use aeroport

DECLARE
@fetchIdPilote as BIGINT,
@fetchCurrentPilote as BIGINT

SET @fetchIdPilote = 0
SET @fetchCurrentPilote = 0



DECLARE cursorIdPilote CURSOR SCROLL
FOR
select idPilote from coordonnees where idPilote IS NOT NULL
ORDER BY idPilote

OPEN cursorIdPilote FETCH cursorIdPilote INTO @fetchCurrentPilote

--(SELECT DISTINCT COUNT(idPilote) FROM coordonnees)

WHILE (@@FETCH_STATUS = 0)
BEGIN --1

	EXEC affichagePilote @fetchIdPilote,@fetchCurrentPilote


	FETCH NEXT FROM cursorIdPilote INTO @fetchCurrentPilote
	print ('')
	print ('')
	print ('')
END --1 

CLOSE cursorIdPilote
DEALLOCATE cursorIdPilote