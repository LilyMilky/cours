use aeroport



DECLARE
@fetchIdAvion as VARCHAR(8),
@fetchCurrentAvion as VARCHAR(8)

SET @fetchIdAvion = ''
SET @fetchCurrentavion = 0

IF (SELECT DISTINCT COUNT(immatriculationAvion) FROM avion) = 0	print('pas d''avions')

print ('Liste des avions')
print('')

DECLARE cursorIdAvion CURSOR SCROLL
FOR
select immatriculationAvion from avion

OPEN cursorIdAvion FETCH cursorIdAvion INTO @fetchCurrentAvion


WHILE (@@FETCH_STATUS = 0)
BEGIN --1

	EXEC affichageMecano @fetchIdAvion,@fetchCurrentAvion 

	FETCH NEXT FROM cursorIdAvion INTO @fetchCurrentAvion
	print ('')
	print ('')
	print ('')



END --1 

CLOSE cursorIdAvion
DEALLOCATE cursorIdAvion