use aeroport
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE dbo.affichageMecano

@fetchIdAvion as VARCHAR(8),
@fetchCurrentAvion as VARCHAR(8)

AS

	DECLARE @getAvionMatricule VARCHAR(50)

	SET @getAvionMatricule = (select immatriculationAvion from avion where immatriculationAvion=@fetchCurrentAvion)

	print CONCAT('Avion matricul� ',@fetchCurrentAvion)
	print ('	Liste des interventions : ')




		--*** FONCTION NICHE 1
		--***

		DECLARE
		@fetchIdMaintenance as BIGINT,
		@fetchCurrentMaintenance as BIGINT

		SET @fetchIdMaintenance = 0
		SET @fetchCurrentMaintenance = 0


		IF (SELECT COUNT(idMaintenance) FROM maintenance WHERE immatriculationAvion=@fetchCurrentAvion) = 0	print('	pas d''interventions')

		DECLARE cursorIdMaintenance CURSOR SCROLL
		FOR
			select ma.idMaintenance
			from avion as av
			join maintenance as ma
			on av.immatriculationAvion  = ma.immatriculationAvion 
			WHERE av.immatriculationAvion=@fetchCurrentAvion

		OPEN cursorIdMaintenance FETCH cursorIdMaintenance INTO @fetchCurrentMaintenance

		WHILE (@@FETCH_STATUS = 0)
		BEGIN --2 Sous boucle Maintenance
			DECLARE	@getMaintenanceDesc as VARCHAR(50)
			DECLARE @getDateDebut as DATETIME
			DECLARE @getDateFin as DATETIME

			SET @getMaintenanceDesc = (
				select objetMaintenance FROM maintenance
				WHERE idMaintenance=@fetchCurrentMaintenance
			 )
			SET @getDateDebut = (
				select dateDebutMaintenance FROM maintenance
				WHERE idMaintenance=@fetchCurrentMaintenance
			 )
			SET @getDateFin = (
				select dateFinMaintenance FROM maintenance
				WHERE idMaintenance=@fetchCurrentMaintenance
			 )
			
			print(CONCAT('	D�but� le : ',@getDateDebut,' Termin� le : ',@getDateFin))
			print(CONCAT('	Motif : ',@getMaintenanceDesc))
			print('	R�par� par : ')






				--*** FONCTION NICHE 2�me degr�
				--***
				DECLARE
				@fetchIdMecano as BIGINT,
				@fetchCurrentMecano as BIGINT


				SET @fetchIdMecano = 0
				SET @fetchCurrentMecano = 0

				IF (
					select COUNT(mec.matriculeMecanicien)
					from coordonnees as co
					join mecanicien as mec
					on co.matriculeMecanicien = mec.matriculeMecanicien
					join effectuer as ef
					on mec.matriculeMecanicien = ef.matriculeMecanicien
					join maintenance as ma
					on ef.idMaintenance  = ma.idMaintenance 
					join avion as av
					on ma.immatriculationAvion  = av.immatriculationAvion
					WHERE ma.idMaintenance = @fetchCurrentMaintenance AND av.immatriculationAvion=@fetchCurrentAvion
					) = 0 	print('	pas de m�caniciens intervenus..?!')

				DECLARE cursorIdMecano CURSOR SCROLL
				FOR
					select mec.matriculeMecanicien
					from coordonnees as co
					join mecanicien as mec
					on co.matriculeMecanicien = mec.matriculeMecanicien
					join effectuer as ef
					on mec.matriculeMecanicien = ef.matriculeMecanicien
					join maintenance as ma
					on ef.idMaintenance  = ma.idMaintenance 
					join avion as av
					on ma.immatriculationAvion  = av.immatriculationAvion
					WHERE ma.idMaintenance = @fetchCurrentMaintenance AND av.immatriculationAvion=@fetchCurrentAvion

				OPEN cursorIdMecano FETCH cursorIdMecano INTO @fetchCurrentMecano


				WHILE (@@FETCH_STATUS = 0)
				BEGIN --3 Sous boucle mecano
					DECLARE	@getMecanoNom as VARCHAR(50)
					DECLARE	@getMecanoPrenom as VARCHAR(50)

					SET @getMecanoNom = (
						select co.nomCoordonnees
						from coordonnees as co
						join mecanicien as mec
						on co.matriculeMecanicien = mec.matriculeMecanicien
						join effectuer as ef
						on mec.matriculeMecanicien = ef.matriculeMecanicien
						join maintenance as ma
						on ef.idMaintenance  = ma.idMaintenance 
						join avion as av
						on ma.immatriculationAvion  = av.immatriculationAvion
						WHERE ma.idMaintenance = @fetchCurrentMaintenance AND av.immatriculationAvion=@fetchCurrentAvion AND ef.matriculeMecanicien=@fetchCurrentMecano
					 )
					SET @getMecanoPrenom = (
						select co.prenomCoordonnees
						from coordonnees as co
						join mecanicien as mec
						on co.matriculeMecanicien = mec.matriculeMecanicien
						join effectuer as ef
						on mec.matriculeMecanicien = ef.matriculeMecanicien
						join maintenance as ma
						on ef.idMaintenance  = ma.idMaintenance 
						join avion as av
						on ma.immatriculationAvion  = av.immatriculationAvion
						WHERE ma.idMaintenance = @fetchCurrentMaintenance AND av.immatriculationAvion=@fetchCurrentAvion AND ef.matriculeMecanicien=@fetchCurrentMecano
					 )
			

					print(CONCAT('		Matricule : ',@fetchCurrentMecano,'; Nom : ',@getMecanoNom,'; Prenom : ',@getMecanoPrenom))



					FETCH NEXT FROM cursorIdMecano INTO @fetchCurrentMecano



				END --3 Sous boucle mecano

				CLOSE cursorIdMecano
				DEALLOCATE cursorIdMecano


				--***
				--***FIN FONCTION NICHE 2


			print('')

			FETCH NEXT FROM cursorIdMaintenance INTO @fetchCurrentMaintenance
		END --2 Sous boucle Type

		CLOSE cursorIdMaintenance
		DEALLOCATE cursorIdMaintenance

		--***
		--***FIN FONCTION NICHE 1






