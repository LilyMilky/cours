USE aeroport
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE  dbo.affichagePilote
	@fetchIdPilote as BIGINT,
	@fetchCurrentPilote as BIGINT

	AS

	DECLARE	@getPiloteNom as VARCHAR(50)
	DECLARE	@getPilotePrenom as VARCHAR(50)
	DECLARE	@getPiloteAdresse as VARCHAR(50)
	DECLARE	@getPiloteTelephone as VARCHAR(50)

	SET @getPiloteNom = (select nomCoordonnees from coordonnees where idPilote=@fetchCurrentPilote)
	SET @getPilotePrenom = (select prenomCoordonnees from coordonnees where idPilote=@fetchCurrentPilote)
	SET @getPiloteAdresse = CONCAT(
									(select numeroCoordonnees from coordonnees where idPilote=@fetchCurrentPilote),
									' ',(select voieCoordonnees from coordonnees where idPilote=@fetchCurrentPilote),
									' ',(select cpCoordonnees from coordonnees where idPilote=@fetchCurrentPilote),
									' ',(select villeCoordonnees from coordonnees where idPilote=@fetchCurrentPilote),
									' ',(select paysCoordonnees from coordonnees where idPilote=@fetchCurrentPilote)
									)
	SET @getPiloteTelephone = (select numeroTelephoneCoordonnees from coordonnees where idPilote=@fetchCurrentPilote)

	print CONCAT('Pilote num�ro ',@fetchCurrentPilote)
	print CONCAT('Nom : ',@getPiloteNom)
	print CONCAT('Prenom : ',@getPilotePrenom)
	print CONCAT('Adresse : ',@getPiloteAdresse)
	print CONCAT('Num�ro de t�l�phone : ',@getPiloteTelephone)





		--*** FONCTION NICHE 1
		--***

		DECLARE
		@fetchIdType as BIGINT,
		@fetchCurrentType as VARCHAR(50),
		@getBrevet as VARCHAR(50)

		SET @fetchIdType = 0
		SET @fetchCurrentType = ''
		SET @getBrevet = ''

		print('')
		print('Brevet(s) en possession : ')
		IF ((SELECT COUNT(DISTINCT numeroBrevetPilote) FROM habilitation WHERE idPilote=@fetchCurrentPilote) = 0) 	print('pas de brevet')

		DECLARE cursorIdType CURSOR SCROLL
		FOR
		select DISTINCT numeroBrevetPilote from habilitation as ha JOIN coordonnees as co ON ha.idPilote = co.idPilote 
		WHERE co.idPilote = @fetchCurrentPilote

		OPEN cursorIdType FETCH cursorIdType INTO @fetchCurrentType

		WHILE (@@FETCH_STATUS = 0)
		BEGIN --2 Sous boucle Type
			DECLARE	@getBrevetNom as VARCHAR(50)

			SET @getBrevetNom = (
				select DISTINCT numeroBrevetPilote 
				from habilitation as ha JOIN coordonnees as co ON ha.idPilote = co.idPilote 
				WHERE co.idPilote = @fetchCurrentPilote AND numeroBrevetPilote = @fetchCurrentType
			 )
			
			print(@getBrevetNom)


			FETCH NEXT FROM cursorIdType INTO @fetchCurrentType
		END --2 Sous boucle Type

		CLOSE cursorIdType
		DEALLOCATE cursorIdType

		--***
		--***FIN FONCTION NICHE 1

		print('')
		print('Mod�le(s) avec vol(s) effectu�(s) :')

		--*** FONCTION NICHE 2
		--***
		DECLARE
		@fetchIdModeleAvion as BIGINT,
		@fetchCurrentModeleAvion as BIGINT,
		@getModeleAvion as VARCHAR(50),
		@getNombreVol as VARCHAR(50)

		SET @fetchIdModeleAvion = 0
		SET @fetchCurrentModeleAvion = 0
		SET @getModeleAvion = ''

		IF (
			SELECT DISTINCT COUNT(nomModeleType) 		
			FROM type AS ty 
			JOIN habilitation as ha ON ty.idType = ha.idType 
			JOIN coordonnees as co ON ha.idPilote = co.idPilote
			WHERE co.idPilote = @fetchCurrentPilote		
		) = 0 	print('pas d''avion')

		DECLARE cursorIdModeleAvion CURSOR SCROLL
		FOR
		select idType from habilitation as ha JOIN coordonnees as co ON ha.idPilote = co.idPilote 
		WHERE co.idPilote = @fetchCurrentPilote

		OPEN cursorIdModeleAvion FETCH cursorIdModeleAvion INTO @fetchCurrentModeleAvion

		WHILE (@@FETCH_STATUS = 0)
		BEGIN --3 Sous boucle modele Avion
			DECLARE	@getModeleAvionNom as VARCHAR(50)

			SET @getModeleAvionNom = (
				select nomModeleType
				FROM type AS ty 
				JOIN habilitation as ha ON ty.idType = ha.idType 
				JOIN coordonnees as co ON ha.idPilote = co.idPilote
				WHERE co.idPilote = @fetchCurrentPilote AND ty.idType = @fetchCurrentModeleAvion
			 )
			

			SET @getNombreVol = (
				select nbTotalVol from type as ty 
				JOIN habilitation as ha ON ty.idType = ha.idType 
				JOIN coordonnees as co ON ha.idPilote = co.idPilote
				WHERE co.idPilote = @fetchCurrentPilote AND ty.idType = @fetchCurrentModeleAvion

			 )
			
			print(CONCAT(@getModeleAvionNom,' avec ',@getNombreVol,' vols'))



			FETCH NEXT FROM cursorIdModeleAvion INTO @fetchCurrentModeleAvion



		END --3 Sous boucle modele Avion

		CLOSE cursorIdModeleAvion
		DEALLOCATE cursorIdModeleAvion


		--***
		--***FIN FONCTION NICHE 2




