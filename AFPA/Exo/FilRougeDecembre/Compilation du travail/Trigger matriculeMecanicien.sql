--------------TRIGGER CREATION MATRICULE MECANICIEN-----------------------

create trigger ForInsertUpdateCoordonneesMecanicien
--Table pour trigger
on coordonnees
-- Quel evenement
for insert , update
--Debut script
as

declare @Matricule varchar(20)
declare @nbEntree varchar(50)

declare Entree cursor FOR
select i.matriculeMecanicien from coordonnees as c join inserted as i on c.idCoordonnees = i.idCoordonnees

OPEN Entree
FETCH Entree
INTO @nbEntree



Begin Transaction coordonnees
		Declare @monErreur int
		set @monErreur = @@ERROR

alter table coordonnees drop [fkmatriculeMecanicienCoordonees]

while @@FETCH_STATUS = 0
begin
	
	if @nbEntree is not null
	begin

		update mecanicien set

		@Matricule = CONCAT(SUBSTRING(i.nomCoordonnees,1,3), substring(i.prenomCoordonnees,1,3), i.idCoordonnees)
		from coordonnees as c join inserted as i on c.idCoordonnees = i.idCoordonnees where i.matriculeMecanicien = @nbEntree

		update mecanicien set

		matriculeMecanicien = @Matricule from mecanicien as m, coordonnees as c join inserted as i on c.idCoordonnees = i.idCoordonnees 
		where m.matriculeMecanicien = i.matriculeMecanicien and i.matriculeMecanicien = @nbEntree

		update coordonnees set

		matriculeMecanicien = @Matricule from coordonnees as c join inserted as i on c.idCoordonnees = i.idCoordonnees
		where c.matriculeMecanicien = i.matriculeMecanicien and i.matriculeMecanicien = @nbEntree

	end

	FETCH NEXT FROM Entree INTO @nbEntree

end
alter table coordonnees add constraint fkmatriculeMecanicienCoordonees foreign key(matriculeMecanicien) references mecanicien(matriculeMecanicien)

commit transaction coordonnees

CLOSE Entree
DEALLOCATE Entree

