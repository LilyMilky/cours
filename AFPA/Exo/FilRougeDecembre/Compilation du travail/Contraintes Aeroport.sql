------------------------------------Contrainte énumération habilitation : nombre de vol------------------------------------

ALTER TABLE habilitation
ADD CONSTRAINT enumnbTotalVol
CHECK(nbTotalVol > 25)


------------------------------------Contrainte unicité Type : nom du modele------------------------------------

alter table type
add constraint UniciteNomModeleType 
Unique (nomModeleType)

-----------------------------------Contrainte ENUM Coordonnees : civilite ---------------------------------------
alter table coordonnees
add constraint enumCivilite
Check(civiliteCoordonnees ='F' or civiliteCoordonnees = 'M' or civiliteCoordonnees = 'N')

-----------------------------------Contrainte Unicité maintenance : immatriculationAvion + DateDebut -------------------------------
alter table maintenance
add constraint UniciteDateAvion 
Unique (immatriculationAvion,dateDebutMaintenance)

-----------------------------------Contrainte ENUM proprietaire : civilite ---------------------------------------
alter table proprietaire
add constraint enumType
Check(typeProprietaire ='S' or typeProprietaire = 'P')

