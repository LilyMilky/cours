/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeulenombremysterieux;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author cdi209
 */
public class JeuLeNombreMysterieux {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException
    {
        int nbATrouver = (int) (Math.random() * 100);
        int nbSaisie = 0;
        boolean trouve = false;
        int nbTest = 0;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);

        do
        {
            System.out.println("Veuillez saisir un nombre: ");
            nbSaisie = Integer.parseInt(br.readLine());

            if(nbSaisie > nbATrouver)
                System.out.println("Trop grand");
            else if(nbSaisie < nbATrouver)
                System.out.println("Trop petit");
            else
                trouve = true;
            
            nbTest++;
        }while((trouve == false) && (nbTest < 10));
        
        if(trouve == true)
            System.out.println("BRAVO VOUS AVEZ TROUVE EN :" + nbTest + " COUP(S)");
        else
            System.out.println("Vous avez perdu");
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }   
}
