package Interface;

import bdd.BdD;
import individu.*;
import carnet.*;

public class Menu extends javax.swing.JFrame {
    BdD bdd = null;
    CarnetSwing carnet = null;
    Connaissance con = null;
                     
                     
    public Menu() {
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComponents();
        bdd = new BdD();
        
        bdd.connexion();
        
        carnet = new CarnetSwing(bdd);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonAffRepertoire = new javax.swing.JButton();
        jButtonAffGroupe = new javax.swing.JButton();
        jButtonAffContact = new javax.swing.JButton();
        jButtonAjout = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButtonQuitter = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 51));
        getContentPane().setLayout(null);

        jButtonAffRepertoire.setBackground(new java.awt.Color(255, 255, 255));
        jButtonAffRepertoire.setText("Afficher le repertoire");
        getContentPane().add(jButtonAffRepertoire);
        jButtonAffRepertoire.setBounds(0, 42, 400, 23);

        jButtonAffGroupe.setBackground(new java.awt.Color(255, 255, 255));
        jButtonAffGroupe.setText("Afficher un groupe");
        getContentPane().add(jButtonAffGroupe);
        jButtonAffGroupe.setBounds(0, 83, 400, 23);

        jButtonAffContact.setBackground(new java.awt.Color(255, 255, 255));
        jButtonAffContact.setText("Afficher/Modifier/Supprimer Contact");
        jButtonAffContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAffContactActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAffContact);
        jButtonAffContact.setBounds(0, 124, 400, 23);

        jButtonAjout.setBackground(new java.awt.Color(255, 255, 255));
        jButtonAjout.setText("Ajouter un contact");
        jButtonAjout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjoutActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAjout);
        jButtonAjout.setBounds(0, 165, 400, 23);

        jLabel1.setBackground(new java.awt.Color(255, 255, 51));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Bienvenue! Que souhaitez-vous faire?");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 400, 24);

        jButtonQuitter.setText("Quitter");
        jButtonQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitterActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonQuitter);
        jButtonQuitter.setBounds(0, 220, 400, 23);

        setSize(new java.awt.Dimension(416, 288));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void jButtonAjoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjoutActionPerformed
        AjouterContact ajout = new AjouterContact(carnet, bdd);
        ajout.setDefaultCloseOperation(ajout.DISPOSE_ON_CLOSE);
        ajout.setVisible(true);
    }//GEN-LAST:event_jButtonAjoutActionPerformed

    private void jButtonQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitterActionPerformed
        bdd.close();

        dispose();
    }//GEN-LAST:event_jButtonQuitterActionPerformed

    private void jButtonAffContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAffContactActionPerformed
        GestionContact affiche = new GestionContact(carnet, bdd);
        affiche.setDefaultCloseOperation(affiche.DISPOSE_ON_CLOSE);
        affiche.setVisible(true);
    }//GEN-LAST:event_jButtonAffContactActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAffContact;
    private javax.swing.JButton jButtonAffGroupe;
    private javax.swing.JButton jButtonAffRepertoire;
    private javax.swing.JButton jButtonAjout;
    private javax.swing.JButton jButtonQuitter;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
