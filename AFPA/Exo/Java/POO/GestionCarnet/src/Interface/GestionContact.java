package Interface;

import bdd.BdD;
import carnet.CarnetSwing;
import individu.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;

public class GestionContact extends javax.swing.JFrame {

    static CarnetSwing carnet = null;
    static BdD bdd = null;
    
    public GestionContact(CarnetSwing carnet, BdD bdd) {
        GestionContact.carnet = carnet;
        GestionContact.bdd = bdd;
        
        initComponents();
        jComboBoxType.setSelectedItem("");
    }

    private DefaultComboBoxModel initModelTel()
    {
        return new DefaultComboBoxModel(initTel());
    }
    
    private Vector initTel()
    {
        Vector v = new Vector();

        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Oops:Driver:" + ex.getMessage());
            return v;
        }
        Connection connexion = null;
        try {
            connexion = DriverManager.getConnection(
                    "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=carnet;user=sa;password=sa");
        } catch (SQLException ex) {
            System.err.println("Oops:Connection:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        String query = "SELECT * FROM Contact;";
        try {
            Statement stmt = connexion.createStatement();
            ResultSet rs= stmt.executeQuery(query);

            while( rs.next()) {
                if(rs.getString("Type").equals("c"))
                {
                   v.add(new Connaissance(rs.getString("Nom"), rs.getString("Prenom"), rs.getString("Adresse"), rs.getString("CodePostal"),
                           rs.getString("Ville"), rs.getString("Tel"), rs.getString("Email"))); 
                }
                else if(rs.getString("Type").equals("a"))
                {
                   v.add(new Ami(rs.getString("TelMobile"), rs.getString("Nom"), rs.getString("Prenom"), rs.getString("Adresse"), rs.getString("CodePostal"),
                           rs.getString("Ville"), rs.getString("Tel"), rs.getString("Email"))); 
                }
                else
                {
                   v.add(new Famille(rs.getDate("DateNaissance").toString(), rs.getString("TelMobile") ,rs.getString("Nom"), rs.getString("Prenom"), rs.getString("Adresse"), rs.getString("CodePostal"),
                           rs.getString("Ville"), rs.getString("Tel"), rs.getString("Email"))); 
                }
                
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("Oops:SQL:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        try {
            connexion.close();
        } catch (SQLException ex) {
            System.err.println("Oops:Close:" + ex.getErrorCode() + ":" + ex.getMessage());
            return v;
        }

        System.out.println("Done!");
        
        return v;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButtonQuitter = new javax.swing.JButton();
        jTextFieldDateNaissance = new javax.swing.JTextField();
        jTextFieldNom = new javax.swing.JTextField();
        jTextFieldPrenom = new javax.swing.JTextField();
        jTextFieldAdresse = new javax.swing.JTextField();
        jTextFieldCodePostal = new javax.swing.JTextField();
        jTextFieldVille = new javax.swing.JTextField();
        jTextFieldEMail = new javax.swing.JTextField();
        jTextFieldTelMobile = new javax.swing.JTextField();
        jButtonValider = new javax.swing.JButton();
        jComboBoxTel = new javax.swing.JComboBox();
        jButtonModifier = new javax.swing.JButton();
        jButtonSupprimer = new javax.swing.JButton();
        jComboBoxType = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Gestion contact");

        jLabel2.setText("Tel :");

        jLabel3.setText("Nom :");

        jLabel4.setText("Prenom :");

        jLabel5.setText("Adresse :");

        jLabel6.setText("Code Postal :");

        jLabel7.setText("Ville :");

        jLabel8.setText("E-Mail :");

        jLabel9.setText("Type :");

        jLabel10.setText("Tel Mobile :");

        jLabel11.setText("Date Naissance :");

        jButtonQuitter.setText("Quitter");
        jButtonQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitterActionPerformed(evt);
            }
        });

        jTextFieldDateNaissance.setEnabled(false);

        jTextFieldNom.setEnabled(false);

        jTextFieldPrenom.setEnabled(false);

        jTextFieldAdresse.setEnabled(false);

        jTextFieldCodePostal.setEnabled(false);

        jTextFieldVille.setEnabled(false);

        jTextFieldEMail.setEnabled(false);

        jTextFieldTelMobile.setEnabled(false);

        jButtonValider.setText("Afficher");
        jButtonValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonValiderActionPerformed(evt);
            }
        });

        jComboBoxTel.setModel(initModelTel());

        jButtonModifier.setText("Modifier");
        jButtonModifier.setEnabled(false);
        jButtonModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierActionPerformed(evt);
            }
        });

        jButtonSupprimer.setText("Supprimer");
        jButtonSupprimer.setEnabled(false);
        jButtonSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSupprimerActionPerformed(evt);
            }
        });

        jComboBoxType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Connaissance", "Ami", "Famille" }));
        jComboBoxType.setEnabled(false);
        jComboBoxType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTypeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButtonValider, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jButtonModifier, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonSupprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonQuitter, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldDateNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(45, 45, 45)
                        .addComponent(jTextFieldTelMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(74, 74, 74)
                        .addComponent(jTextFieldVille, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(35, 35, 35)
                        .addComponent(jTextFieldCodePostal, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(53, 53, 53)
                        .addComponent(jTextFieldAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(56, 56, 56)
                        .addComponent(jTextFieldPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(71, 71, 71)
                        .addComponent(jTextFieldNom, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(78, 78, 78)
                        .addComponent(jComboBoxTel, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(64, 64, 64)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldEMail)
                            .addComponent(jComboBoxType, 0, 351, Short.MAX_VALUE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addComponent(jComboBoxTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel3))
                    .addComponent(jTextFieldNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel4))
                    .addComponent(jTextFieldPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel5))
                    .addComponent(jTextFieldAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel6))
                    .addComponent(jTextFieldCodePostal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel7))
                    .addComponent(jTextFieldVille, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel8))
                    .addComponent(jTextFieldEMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel10))
                    .addComponent(jTextFieldTelMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel11))
                    .addComponent(jTextFieldDateNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonValider, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonModifier, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSupprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonQuitter, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitterActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitterActionPerformed

    private void jButtonValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonValiderActionPerformed
        Connaissance con;
        
        con = carnet.affPersonne(jComboBoxTel.getSelectedItem().toString());
        
        jTextFieldAdresse.setText(con.getAdresse());
        jTextFieldCodePostal.setText(con.getCodePostal());
        jTextFieldEMail.setText(con.geteMail());
        jTextFieldNom.setText(con.getNom());
        jTextFieldPrenom.setText(con.getPrenom());
        jTextFieldVille.setText(con.getVille());
        
        if(con.getType() == 'c')
        {
            jComboBoxType.setSelectedItem("Connaissance");
            jTextFieldTelMobile.setText("");
            jTextFieldDateNaissance.setText("");
        }
        else if(con.getType() == 'a')
        {
            jComboBoxType.setSelectedItem("Ami");
            jTextFieldTelMobile.setText(((Ami)con).getTelMobile());
            jTextFieldDateNaissance.setText("");
        }
        else
        {
            jComboBoxType.setSelectedItem("Famille");
            jTextFieldTelMobile.setText(((Famille)con).getTelMobile());
            jTextFieldDateNaissance.setText(((Famille)con).getDateNaissance());
        }
        
        jButtonModifier.setEnabled(true);
        jButtonSupprimer.setEnabled(true);
        jTextFieldAdresse.setEnabled(true);
        jTextFieldCodePostal.setEnabled(true);
        jTextFieldEMail.setEnabled(true);
        jTextFieldNom.setEnabled(true);
        jTextFieldPrenom.setEnabled(true);
        jTextFieldVille.setEnabled(true);
        jTextFieldTelMobile.setEnabled(true);
        jTextFieldDateNaissance.setEnabled(true);
        jComboBoxType.setEnabled(true);
    }//GEN-LAST:event_jButtonValiderActionPerformed

    private void jButtonSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSupprimerActionPerformed
        Vector<Connaissance> v = new Vector();
        
        carnet.supprPersonne(jComboBoxTel.getSelectedItem().toString(), bdd);
        
        jComboBoxTel.removeAllItems();
        
        jTextFieldAdresse.setText("");
        jTextFieldCodePostal.setText("");
        jTextFieldEMail.setText("");
        jTextFieldNom.setText("");
        jTextFieldPrenom.setText("");
        jTextFieldVille.setText("");
        jTextFieldTelMobile.setText("");
        jTextFieldDateNaissance.setText("");
        jComboBoxType.setSelectedItem("Connaissance");
        
        v = initTel();
        
        for(Connaissance con : v)
        {
            jComboBoxTel.addItem(con);
        }
        
        jButtonModifier.setEnabled(false);
        jButtonSupprimer.setEnabled(false);
        jTextFieldAdresse.setEnabled(false);
        jTextFieldCodePostal.setEnabled(false);
        jTextFieldEMail.setEnabled(false);
        jTextFieldNom.setEnabled(false);
        jTextFieldPrenom.setEnabled(false);
        jTextFieldVille.setEnabled(false);
        jTextFieldTelMobile.setEnabled(false);
        jTextFieldDateNaissance.setEnabled(false);
        jComboBoxType.setEnabled(false);
    }//GEN-LAST:event_jButtonSupprimerActionPerformed

    private void jButtonModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierActionPerformed
        Connaissance con;
        
        if(jComboBoxType.getSelectedItem().toString().equals("Connaissance"))
        {
            con = new Connaissance();
            jTextFieldTelMobile.setEnabled(false);
            jTextFieldDateNaissance.setEnabled(false);
        }
        else if(jComboBoxType.getSelectedItem().toString().equals("Ami"))
        {
            con = new Ami();
            ((Ami)con).setTelMobile(jTextFieldTelMobile.getText());
            jTextFieldTelMobile.setEnabled(true);
            jTextFieldDateNaissance.setEnabled(false);
        }
        else
        {
            con = new Famille();
            ((Famille)con).setTelMobile(jTextFieldTelMobile.getText());
            ((Famille)con).setDateNaissance(jTextFieldDateNaissance.getText());
            jTextFieldTelMobile.setEnabled(true);
            jTextFieldDateNaissance.setEnabled(true);
        }
        
        con.setAdresse(jTextFieldAdresse.getText());
        con.setCodePostal(jTextFieldCodePostal.getText());
        con.setNom(jTextFieldNom.getText());
        con.setPrenom(jTextFieldPrenom.getText());
        con.setTel(jComboBoxTel.getSelectedItem().toString());
        con.setVille(jTextFieldVille.getText());
        con.seteMail(jTextFieldEMail.getText());
        
        carnet.supprPersonne(jComboBoxTel.getSelectedItem().toString(), bdd);
        try {
            carnet.ajoutPersonne(con, bdd);
        } catch (IOException ex) {}
    }//GEN-LAST:event_jButtonModifierActionPerformed

    private void jComboBoxTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTypeItemStateChanged
        if(jComboBoxType.getSelectedItem().toString().equals("Connaissance"))
        {
            jTextFieldTelMobile.setEnabled(false);
            jTextFieldDateNaissance.setEnabled(false);
        }
        else if(jComboBoxType.getSelectedItem().toString().equals("Ami"))
        {
            jTextFieldTelMobile.setEnabled(true);
            jTextFieldDateNaissance.setEnabled(false);
        }
        else
        {
            jTextFieldTelMobile.setEnabled(true);
            jTextFieldDateNaissance.setEnabled(true);
        }
    }//GEN-LAST:event_jComboBoxTypeItemStateChanged

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GestionContact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GestionContact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GestionContact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GestionContact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GestionContact(carnet, bdd).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonModifier;
    private javax.swing.JButton jButtonQuitter;
    private javax.swing.JButton jButtonSupprimer;
    private javax.swing.JButton jButtonValider;
    private javax.swing.JComboBox jComboBoxTel;
    private javax.swing.JComboBox jComboBoxType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField jTextFieldAdresse;
    private javax.swing.JTextField jTextFieldCodePostal;
    private javax.swing.JTextField jTextFieldDateNaissance;
    private javax.swing.JTextField jTextFieldEMail;
    private javax.swing.JTextField jTextFieldNom;
    private javax.swing.JTextField jTextFieldPrenom;
    private javax.swing.JTextField jTextFieldTelMobile;
    private javax.swing.JTextField jTextFieldVille;
    // End of variables declaration//GEN-END:variables
}
