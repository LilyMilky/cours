package main;

import classes.*;
import bdd.BdD;
import carnet.CarnetConsole;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class AppMain {

    public static void main(String[] args) throws IOException, ParseException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        int choix;
        char continuer;
        String tel;
        BdD bdd = new BdD();
        
        //Ouverture du flux vers la base
        bdd.connexion();
        CarnetConsole carnet = new CarnetConsole(bdd);
        
        do {
            //Affichage du menu d'acceuil + recuperation du choix de l'utilisateur
            
            do {
                Affichage.affichageMenu();
                try {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }

                if (choix < 1 || choix > 6) {
                    System.out.println("Choix Invalide");
                }

            } while (choix < 1 || choix > 6);

            switch (choix) {
                case 1:    //Afficher repertoire --> Affichage pour selectionner forme de tri du repertoire
                    GestionContact.choixTri(carnet,br);
                    break;
                case 2:    //Afficher un groupe
                    carnet.afficherGroupePersonne(GestionContact.choixGroupe(br));
                    break;
                case 3:     //Afficher un contact  --> via un numero de tel (UNIQUE)
                    System.out.println("Veuillez saisir le numero de telephone de ce contact");
                    tel = br.readLine();
                    carnet.affPersonne(tel);
                    break;
                case 4:     //Ajouter un contact
                    carnet.ajoutPersonne(br, bdd);
                    break;
                case 5:     //Modifier un contact
                    System.out.println("Veuillez saisir le numero de telephone du contact a modifier");
                    tel = br.readLine();
                    carnet.modifPersonne(tel, br, bdd);
                    break;
                case 6:     //Supprimer un contact
                    System.out.println("Veuillez saisir le numero de telephone du contact a supprimer");
                    tel = br.readLine();
                    carnet.supprPersonne(tel, bdd);
                    break;
            }

            System.out.println("Souhaitez vous continuer? (Y pour continuer)");
            continuer = br.readLine().charAt(0);
        } while (continuer == 'y' || continuer == 'Y');

        
        bdd.close();
        br.close();
        isr.close();
    }
}
