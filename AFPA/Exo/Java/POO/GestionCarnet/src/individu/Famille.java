package individu;

import bdd.BdD;
import carnet.CarnetConsole;
import carnet.CarnetSwing;
import classes.Affichage;
import classes.GestionContact;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;


public class Famille extends Ami{
    
    private String dateNaissance;

    public Famille() {
        setType('f');
    }

    public Famille(String dateNaissance, String telMobile, String nom, String prenom, String adresse, String codePostal, String ville, String tel, String eMail) {
        super(telMobile, nom, prenom, adresse, codePostal, ville, tel, eMail);
        this.dateNaissance = dateNaissance;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    @Override
    public String toString() {
        return getTel();
    }
    
    //Permet la modification d'un contact avec le type d'objet = FAMILLE
    @Override
    public void modifConnaissance(BufferedReader br, CarnetConsole carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 8);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    }
    
    //------------------------------------------------------------------------------------------------
    
    //Permet la modification d'un connaissance en swing ENLEVER LES SOUT
    @Override
    public void modifConnaissance(BufferedReader br, CarnetSwing carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 10);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    } 
}
