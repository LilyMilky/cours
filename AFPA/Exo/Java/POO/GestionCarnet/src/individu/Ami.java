package individu;

import bdd.BdD;
import carnet.CarnetConsole;
import carnet.CarnetSwing;
import classes.Affichage;
import classes.GestionContact;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

public class Ami extends Connaissance{
    
    private String telMobile;

    public Ami() {
        setType('a');
    }

    public Ami(String telMobile, String nom, String prenom, String adresse, String codePostal, String ville, String tel, String eMail) {
        super(nom, prenom, adresse, codePostal, ville, tel, eMail);
        this.telMobile = telMobile;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }
    
    @Override
    public String toString() {
        return getTel();
    }
    
    //Permet la modification d'un contact avec le type d'objet = AMI
    @Override
    public void modifConnaissance(BufferedReader br, CarnetConsole carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 9);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    }
    
    //------------------------------------------------------------------------------------------------
    
    //Permet la modification d'un connaissance en swing ENLEVER LES SOUT
    @Override
    public void modifConnaissance(BufferedReader br, CarnetSwing carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 9);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    } 
}
