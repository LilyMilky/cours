package individu;

import bdd.BdD;
import carnet.*;
import classes.Affichage;
import classes.GestionContact;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

public class Connaissance {
    
    private String nom;
    private String prenom;
    private String adresse;
    private String codePostal;
    private String ville;
    private String tel;
    private String eMail;
    private char type; //Type objet (connaissance/ami/famille)

    public Connaissance() {
        type = 'c';
    }

    public Connaissance(String nom, String prenom, String adresse, String codePostal, String ville, String tel, String eMail) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.tel = tel;
        this.eMail = eMail;
        type = 'c';
    }

    // <editor-fold defaultstate="collapsed" desc=" Getter/Setter ">
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getAdresse() {
        return adresse;
    }
    
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    public String getCodePostal() {
        return codePostal;
    }
    
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }
    
    public String getVille() {
        return ville;
    }
    
    public void setVille(String ville) {
        this.ville = ville;
    }
    
    public String getTel() {
        return tel;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }
    
    public String geteMail() {
        return eMail;
    }
    
    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }
// </editor-fold>

    @Override
    public String toString() {
        return tel;
    }

    
    
    
    //Permet la modification d'un connaissance
    public void modifConnaissance(BufferedReader br, CarnetConsole carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 8);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    } 
    
    public boolean testConnaissanceOK()
    {
        if(!nom.trim().isEmpty() && !prenom.trim().isEmpty() && !tel.trim().isEmpty())
            return true;
        else     
            return false;
    }
    
    //------------------------------------------------------------------------------------------------
    
    //Permet la modification d'un connaissance en swing ENLEVER LES SOUT
    public void modifConnaissance(BufferedReader br, CarnetSwing carnet, BdD bdd) throws IOException, ParseException{
        
        String conti;
        int choix = 0;
      
        do
        {
            do
            {
                Affichage.affichageMenu(this);

                try 
                {
                    choix = Integer.parseInt(br.readLine());
                } 
                catch (NumberFormatException nfe) {
                    choix = 0;
                    System.out.println("Veuillez saisir un chiffre");
                }
            }while(choix < 1 && choix > 8);

            GestionContact.modifContact(br, this, choix, carnet, bdd);
            
            System.out.println("Voulez-vous modifier autre chose? (N pour arreter): ");
            conti = br.readLine();
            
        } while (!(conti.equalsIgnoreCase("n")));
    } 
}
