package classes;

import individu.Connaissance;
import individu.Ami;
import individu.Famille;

public class Affichage {
    
    //Affiche le menu de modification d'un contact
    public static void affichageMenu(Connaissance con){
        
        System.out.println("Que voulez-vous modifier?");
        System.out.println("1) Nom");
        System.out.println("2) Prenom");
        System.out.println("3) Adresse");
        System.out.println("4) CodePostal");
        System.out.println("5) Ville");
        System.out.println("6) Tel");
        System.out.println("7) Email");
        System.out.println("8) Changement de groupe");
        
        if(con instanceof Ami)
            System.out.println("9) TelMobile");
        if(con instanceof Famille)
            System.out.println("10) Date de Naissance");
    }
    
    //Affiche le menu d'accueil
    public static void affichageMenu(){
        
        System.out.println("Que souhaitez-vous faire?");
        System.out.println("1) Afficher le repertoire");
        System.out.println("2) Afficher un groupe");
        System.out.println("3) Afficher un contact");
        System.out.println("4) Ajouter un contact");
        System.out.println("5) Modifier un contact");
        System.out.println("6) Supprimer un contact");      
    }
    
    //Affiche les choix disponible pour le tri des contacts
    public static void affichageTypeTri(){
        
        System.out.println("Dans quel ordre souhaitez-vous afficher le repertoire?");
        System.out.println("1) Par nom");
        System.out.println("2) Par groupe");
        System.out.println("3) Par code postal");
    }
    
    //Affiche la liste des groupes
    public static void affichageGroupe(){
        
        System.out.println("Selectionnez un groupe");
        System.out.println("C) Connaissances");
        System.out.println("A) Amis");
        System.out.println("F) Familles");
    }
    
}
