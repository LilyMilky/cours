package classes;

import individu.Connaissance;
import java.util.Comparator;

//Permet de trier la liste de contact par nom
public class TriParNom implements Comparator<Connaissance>{
    
    @Override
    public int compare(Connaissance o1, Connaissance o2) {
        String nom01 = o1.getNom();
        String nom02 = o2.getNom();
        
        return nom01.compareToIgnoreCase(nom02);
    }
    
}

