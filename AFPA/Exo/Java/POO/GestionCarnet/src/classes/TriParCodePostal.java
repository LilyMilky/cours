package classes;

import individu.Connaissance;
import java.util.Comparator;

//Permet le tri de la liste de contact par code postal
public class TriParCodePostal implements Comparator<Connaissance>{
    
    @Override
    public int compare(Connaissance o1, Connaissance o2) {
        String codePostal01 = o1.getCodePostal();
        String codePostal02 = o2.getCodePostal();
        
        return codePostal01.compareToIgnoreCase(codePostal02);
    }
    
}
