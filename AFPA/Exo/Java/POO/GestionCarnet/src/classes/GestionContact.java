package classes;

import individu.*;
import bdd.BdD;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import carnet.*;

public class GestionContact {

    //Permet de modifier un contact
    public static void modifContact(BufferedReader br, Connaissance con, int choix, CarnetConsole carnet, BdD bdd) throws IOException, ParseException {
        
        boolean dateOK;
        Connaissance con2 = null;
        char type;
        String ajout;
        String firstTel = con.getTel();

        switch (choix) {
            case 1:     //Changement nom
                System.out.println("Veuillez saisir le nouveau nom: ");
                con.setNom(br.readLine());
                break;
            case 2:     //Changement prenom
                System.out.println("Veuillez saisir le nouveau prenom: ");
                con.setPrenom(br.readLine());
                break;
            case 3:     //Changement adresse
                System.out.println("Veuillez saisir la nouvelle adresse: ");
                con.setAdresse(br.readLine());
                break;
            case 4:     //Changement code postal
                System.out.println("Veuillez saisir le nouveau code postal: ");
                con.setCodePostal(br.readLine());
                break;
            case 5:     //Changement ville
                System.out.println("Veuillez saisir le nouveau ville: ");
                con.setVille(br.readLine());
                break;
            case 6:     //Changement tel
                System.out.println("Veuillez saisir le nouveau numero de telephone: ");
                con.setTel(br.readLine());
                break;
            case 7:     //Changement mail
                System.out.println("Veuillez saisir le nouveau email: ");
                con.seteMail(br.readLine());
                break;
            case 8:     //Changement de groupe
                do
                {
                    Affichage.affichageGroupe();
                    type = Character.toLowerCase(br.readLine().charAt(0));
                }
                while(type != 'c' && type != 'a' && type != 'f');
                if(type == 'c')
                {
                    con2 = new Connaissance();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                }
                if(type == 'a')
                {
                    con2 = new Ami();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                    
                    System.out.println("Voulez vous ajouter un numero de telephone mobile? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                        System.out.println("Veuillez saisir le numero de telephone");
                        ((Ami)con2).setTelMobile(br.readLine());
                    }
                }
                
                if(type == 'f')
                {
                    con2 = new Famille();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                    
                    System.out.println("Voulez vous ajouter un numero de telephone mobile? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                        System.out.println("Veuillez saisir le numero de telephone");
                        ((Famille)con2).setTelMobile(br.readLine());
                    }
                    
                    System.out.println("Voulez vous ajouter une date de naissance? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                        do {
                            System.out.println("Veuillez saisir la date de naissance (dd/MM/yyyy): "); 
                            ((Famille) con).setDateNaissance(br.readLine());
                            dateOK = true;
                        } while (dateOK == false);
                    } 
                }
                    carnet.supprPersonne(con.getTel(),bdd);
                    carnet.ajoutPersonne(con2, bdd);
                break;
            case 9:     //(Ami) changement tel mobile
                System.out.println("Veuillez saisir le nouveau numero de telephone mobile: ");
                ((Ami) con).setTelMobile(br.readLine());
                break;
            case 10:        //(Famille) changement date de naissance
                do {
                    System.out.println("Veuillez saisir la date de naissance (dd/MM/yyyy): "); 
                    ((Famille) con).setDateNaissance(br.readLine());
                    dateOK = true;
                } while (dateOK == false);
                break;
            default:
                System.out.println("Choix Invalide");
        }
        
        //Maj de la base
        bdd.updateContact(con, choix, firstTel);
    }

    //Permet la rentré des données pour l'ajout d'un contact
    public static Connaissance ajoutContact(BufferedReader br) throws IOException{
        
        char type = Character.toLowerCase(choixGroupe(br));
        boolean dateOK;

        //De base, l'ajout d'un contact est au moins une connaissance
        Connaissance con = new Connaissance();

        if (type == 'a')
            con = new Ami();
        else if (type == 'f') 
            con = new Famille();

        System.out.println("Veuillez saisir le nom: ");
        con.setNom(br.readLine());

        System.out.println("Veuillez saisir le prenom: ");
        con.setPrenom(br.readLine());

        System.out.println("Veuillez saisir l adresse: ");
        con.setAdresse(br.readLine());

        System.out.println("Veuillez saisir le code postal: ");
        con.setCodePostal(br.readLine());

        System.out.println("Veuillez saisir le ville: ");
        con.setVille(br.readLine());

        System.out.println("Veuillez saisir le numero de telephone: ");
        con.setTel(br.readLine());

        System.out.println("Veuillez saisir l email: ");
        con.seteMail(br.readLine());

        if (type == 'a' || type == 'f') {
            System.out.println("Veuillez saisir le numero de telephone mobile: ");
            ((Ami) con).setTelMobile(br.readLine());
        }

        if (type == 'f') {
            do {
                System.out.println("Veuillez saisir la date de naissance (dd/MM/yyyy): "); 
                ((Famille) con).setDateNaissance(br.readLine());
                dateOK = true;
            } while (dateOK == false);
        }

        return con;
    }

    //Permet le choix d'un groupe pour l'ajout d'un contact ou l'affichage par groupe
    public static char choixGroupe(BufferedReader br) throws IOException {
        
        char type;

        do 
        {
            Affichage.affichageGroupe();
            type = Character.toLowerCase(br.readLine().charAt(0));

            if (type != 'c' && type != 'a' && type != 'f' )
                System.out.println("Choix Invalide");

        } while (type != 'c'&& type != 'a' && type != 'f');

        return type;
    }

    //Selectionner le type de tri pour l'affichage du repertoire
    public static void choixTri(CarnetConsole carnet, BufferedReader br) throws IOException {
        int choixTri;

        do 
        {
            Affichage.affichageTypeTri();
            //Test si bien saisi d'un nombre
            try{
                choixTri = Integer.parseInt(br.readLine());
            }
            catch(NumberFormatException nfe)
            {
                 choixTri = 0;
                 System.out.println("Veuillez saisir un chiffre");
            }
            
        } while (choixTri < 1 || choixTri > 3);

        switch (choixTri) {
            case 1:     //Tri par nom
                carnet.afficherCarnetParNom();
                break;
            case 2:     //Tri par groupe
                carnet.afficherCarnetParType();
                break;
            case 3:    //Tri par codePostal
                carnet.afficherCarnetParCodePostal();
                break;
        }
    }
    
    
    //-----------------------------------------------------------------------------------------------
    
    
    //Permet de modifier un contact en swing (ENLEVER LES SOUT)
    public static void modifContact(BufferedReader br, Connaissance con, int choix, CarnetSwing carnet, BdD bdd) throws IOException, ParseException {
        
        boolean dateOK;
        Connaissance con2 = null;
        char type;
        String ajout;
        String firstTel = con.getTel();

        switch (choix) {
            case 1:     //Changement nom
                System.out.println("Veuillez saisir le nouveau nom: ");
                con.setNom(br.readLine());
                break;
            case 2:     //Changement prenom
                System.out.println("Veuillez saisir le nouveau prenom: ");
                con.setPrenom(br.readLine());
                break;
            case 3:     //Changement adresse
                System.out.println("Veuillez saisir la nouvelle adresse: ");
                con.setAdresse(br.readLine());
                break;
            case 4:     //Changement code postal
                System.out.println("Veuillez saisir le nouveau code postal: ");
                con.setCodePostal(br.readLine());
                break;
            case 5:     //Changement ville
                System.out.println("Veuillez saisir le nouveau ville: ");
                con.setVille(br.readLine());
                break;
            case 6:     //Changement tel
                System.out.println("Veuillez saisir le nouveau numero de telephone: ");
                con.setTel(br.readLine());
                break;
            case 7:     //Changement mail
                System.out.println("Veuillez saisir le nouveau email: ");
                con.seteMail(br.readLine());
                break;
            case 8:     //Changement de groupe
                do
                {
                    Affichage.affichageGroupe();
                    type = Character.toLowerCase(br.readLine().charAt(0));
                }
                while(type != 'c' && type != 'a' && type != 'f');
                if(type == 'c')
                {
                    con2 = new Connaissance();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                }
                if(type == 'a')
                {
                    con2 = new Ami();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                    
                    System.out.println("Voulez vous ajouter un numero de telephone mobile? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                        System.out.println("Veuillez saisir le numero de telephone");
                        ((Ami)con2).setTelMobile(br.readLine());
                    }
                }
                
                if(type == 'f')
                {
                    con2 = new Famille();
                    con2.setAdresse(con.getAdresse());
                    con2.setCodePostal(con.getCodePostal());
                    con2.setNom(con.getNom());
                    con2.setPrenom(con.getPrenom());
                    con2.setTel(con.getTel());
                    con2.setVille(con.getVille());
                    con2.seteMail(con.geteMail());
                    
                    System.out.println("Voulez vous ajouter un numero de telephone mobile? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                        System.out.println("Veuillez saisir le numero de telephone");
                        ((Famille)con2).setTelMobile(br.readLine());
                    }
                    
                    System.out.println("Voulez vous ajouter une date de naissance? (O pour en ajouter)");
                    ajout = br.readLine();
                    
                    if(ajout.equalsIgnoreCase("o"))
                    {
                            do {
                            System.out.println("Veuillez saisir la date de naissance (dd/MM/yyyy): "); 
                            ((Famille) con).setDateNaissance(br.readLine());
                            dateOK = true;
                        } while (dateOK == false);
                    } 
                }
                    carnet.supprPersonne(con.getTel(),bdd);
                    carnet.ajoutPersonne(con2, bdd);
                break;
            case 9:     //(Ami) changement tel mobile
                System.out.println("Veuillez saisir le nouveau numero de telephone mobile: ");
                ((Ami) con).setTelMobile(br.readLine());
                break;
            case 10:        //(Famille) changement date de naissance
                do {
                    System.out.println("Veuillez saisir la date de naissance (dd/MM/yyyy): ");
                    ((Famille) con).setDateNaissance(br.readLine());
                    dateOK = true;
                } while (dateOK == false);
                break;
            default:
                System.out.println("Choix Invalide");
        }
        
        //Maj de la base
        bdd.updateContact(con, choix, firstTel);
    }
}
