package bdd;

import individu.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class BdD {

    Connection connexion = null;
    
    public void connexion()
    {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Oops: ClassNotFound: " +ex.getMessage());
        }
        
        try {
            String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=carnet;user=sa;password=sa";
            connexion = DriverManager.getConnection(connectionUrl);
        } catch (SQLException ex) {
            System.err.println("Oops: SQLConnexion: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
    }
    
    public void connexionSQL()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Oops: ClassNotFound: " +ex.getMessage());
        }
        
        try {
            String connectionUrl = "jdbc:mysql://leox/cdi09";
            connexion = DriverManager.getConnection(connectionUrl, "cdi09", "afpa");
        } catch (SQLException ex) {
            System.err.println("Oops: SQLConnexion: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
    }
    
    public void close()
    {
        try {
            connexion.close();
        } catch (SQLException ex) {
            System.out.println("Echec fermeture connexion");
        }
    }
    
    
    public void insertionContact(Connaissance con)
    {
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "Insert into Contact values ('" + con.getNom() + "', '" + con.getPrenom()+ "', '" + con.getAdresse()+ "', '"
                    + con.getCodePostal()+ "', '" + con.getVille()+ "', '" + con.getTel()+ "', '" + con.geteMail() + "', '" 
                    + con.getType()+ "'";
            
            if(con.getType() == 'a')
                query +=  ", '" + ((Ami)con).getTelMobile() + "', null";

            else if(con.getType() == 'f')
            {
                
                if(((Famille)con).getTelMobile().trim().isEmpty())
                    query += ", null,";
                else
                    query += ", '" + ((Famille)con).getTelMobile() + "',";
                
                
                query +=  "Convert(datetime,'" + ((Famille)con).getDateNaissance() + "',103)";
            }
            else
                query += ", null, null";
                
            query += ");";
            //System.out.println(query);
            
            int result = stmt.executeUpdate(query);
            //System.out.println("result = " + result);
            
            stmt.close();
            
        } catch (SQLException ex) {
            //System.err.println("Oops: SQL: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
    }
    
    public void deleteContact(String tel)
    {
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "Delete from Contact where Tel = " + tel;

            //System.out.println(query);
            
            int result = stmt.executeUpdate(query);
            //System.out.println("result = " + result);
            
            stmt.close();
            
        } catch (SQLException ex) {
            System.err.println("Oops: SQL: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
    }
    
    public void updateContact(Connaissance con, int choix, String firstTel)
    {
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "Update Contact set ";
            switch(choix)
            {
                case 1:     //Nom
                    query += "Nom = '" + con.getNom() + "' Where Tel = '" + firstTel + "'";
                    break;
                case 2:     //Prenom
                    query += "Prenom = '" + con.getPrenom()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 3:     //Adresse
                    query += "Adresse = '" + con.getAdresse()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 4:     //Code Postal
                    query += "CodePostal = '" + con.getCodePostal()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 5:     //Ville
                    query += "Ville = '" + con.getVille()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 6:     //Tel
                    query += "Tel = '" + con.getTel()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 7:     //Email
                    query += "Email = '" + con.geteMail()+ "' Where Tel = '" + firstTel + "'";
                    break;
                case 8:     //Type
                    query += "Type = '" + con.getType() + "' Where Tel = '" + firstTel + "'";
                    break;
                case 9:     //Tel Mobile
                    query += "TelMobile = '" + ((Ami)con).getTelMobile() + "' Where Tel = '" + firstTel + "'";
                    break;
                case 10:     //Date Naissance
                    query += "DateNaissance = '" + ((Famille)con).getDateNaissance()+ "' Where Tel = '" + firstTel + "'";
                    break;
            }

            //System.out.println(query);
            
            int result = stmt.executeUpdate(query);
            //System.out.println("result = " + result);
            
            stmt.close();
            
        } catch (SQLException ex) {
            System.err.println("Oops: SQL: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
        
        
    }
    
    //Rempli la liste de contact au lancement de l'application
    public ArrayList<Connaissance> firstLecture() 
    {
        ArrayList<Connaissance> personnes = new ArrayList();
        Connaissance personne = null;
        
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "select * from Contact;";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next())
            {
                String type = rs.getString("Type");
                if(type.equals("c"))
                    personne = new Connaissance();
                else if(type.equals("a"))
                {
                    personne = new Ami();
                    ((Ami)personne).setTelMobile(rs.getString("TelMobile"));
                }
                else if(type.equals("f"))
                {
                    personne = new Famille();
                    ((Famille)personne).setDateNaissance(rs.getDate("DateNaissance").toString());
                }
                
                personne.setAdresse(rs.getString("Adresse"));
                personne.setCodePostal(rs.getString("CodePostal"));
                personne.setNom(rs.getString("Nom"));
                personne.setPrenom(rs.getString("Prenom"));
                personne.setTel(rs.getString("Tel"));
                personne.setType((rs.getString("Type").charAt(0)));
                personne.setVille(rs.getString("Ville"));
                personne.seteMail(rs.getString("Email"));
                
                personnes.add(personne);
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
        }
 
        return personnes;
    }
    
}
