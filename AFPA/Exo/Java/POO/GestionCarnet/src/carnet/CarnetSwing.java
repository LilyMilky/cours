package carnet;

import classes.*;
import individu.*;
import bdd.BdD;
import java.util.ArrayList;
import java.util.Collections;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import Interface.*;

public class CarnetSwing {
    
    private  ArrayList<Connaissance> personnes = new ArrayList();
    
    public CarnetSwing(BdD bdd) {
        personnes = bdd.firstLecture();
    }

    //Permet d'ajouter une personne au carnet en saissisant
    public boolean ajoutPersonne(BufferedReader br, BdD bdd) throws IOException {
        
        Connaissance con = classes.GestionContact.ajoutContact(br);
        
        if (con.testConnaissanceOK())
        {
            personnes.add(con);
            bdd.insertionContact(con);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Permet d'ajouter une personne au carnet via un objet
    public boolean ajoutPersonne(Connaissance con, BdD bdd) throws IOException {

        if (con.testConnaissanceOK())
        {
            personnes.add(con);
            bdd.insertionContact(con);
            return true;
        }
        else
            return false;
    }

    //Permet d'afficher une personne en particulier
    public Connaissance affPersonne(String numTel) {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        
        if (con != null)
            return con;
        
        else
            return null;
    }

    //Permet de modifier une personne
    public void modifPersonne(String numTel, BufferedReader br, BdD bdd) throws IOException, ParseException {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        con.modifConnaissance(br, this, bdd);

    }

    //Permet de supprimer une personne
    public void supprPersonne(String numTel, BdD bdd) {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        
        bdd.deleteContact(numTel);
        personnes.remove(con);
    }

    //Permet d'afficher le carnet par nom
    public ArrayList afficherCarnetParNom() {
        
        Collections.sort(personnes, new TriParNom());

        return personnes;
    }

    //Permet d'afficher le carnet par type de personne
    public ArrayList<Connaissance> afficherCarnetParType() {
        ArrayList<Connaissance> cons;
        ArrayList<Connaissance> consTri = new ArrayList();
        
        //System.out.println("Connaissances:");
        cons = afficherGroupePersonne('c');
        for (Connaissance con : cons)
        {
            consTri.add(con);
        }
       // System.out.println("Amis:");
        cons = afficherGroupePersonne('a');
        for (Connaissance con : cons)
        {
            consTri.add(con);
        }
       // System.out.println("Familles:");
        cons = afficherGroupePersonne('f');
        for (Connaissance con : cons)
        {
            consTri.add(con);
        }
        
        return consTri;
    }

    //Permet d'afficher le carnet par code postal
    public ArrayList afficherCarnetParCodePostal() {
        
        Collections.sort(personnes, new TriParCodePostal());
        
        /*for (Connaissance con : personnes) 
        {
            System.out.println(con);
        }*/
        
        return personnes;
    }

    //Permet d'afficher seulement un groupe de personne (Connaissance = C, Ami = A, Famille = F)
    public ArrayList<Connaissance> afficherGroupePersonne(char type) {
        
        ArrayList<Connaissance> cons = new ArrayList();
        
        for (Connaissance con : personnes) 
        {
            if (type == 'c' && con.getType() == 'c') {
                cons.add(con);
            } else if (type == 'a' && con.getType() == 'a') {
                cons.add(con);
            } else if (type == 'f' && con.getType() == 'f') {
                cons.add(con);
            }            
        }
        
        return cons;
    }

    //Permet de trouver une personne dans la liste
    private Connaissance recherchePers(String numTel) {
        
        ErreurRecherche err = new ErreurRecherche();
        
        for (Connaissance con : personnes) 
        {
            if (con.getTel().equals(numTel))
                return con;
        }
        
        err = new ErreurRecherche();
        err.setDefaultCloseOperation(err.DISPOSE_ON_CLOSE);
        err.setVisible(true);
        
        return null;
    }
}

