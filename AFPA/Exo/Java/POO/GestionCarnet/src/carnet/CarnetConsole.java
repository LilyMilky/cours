package carnet;

import classes.*;
import individu.*;
import bdd.BdD;
import java.util.ArrayList;
import java.util.Collections;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

public class CarnetConsole {
    
    private  ArrayList<Connaissance> personnes = new ArrayList();
    
    public CarnetConsole(BdD bdd) {
        personnes = bdd.firstLecture();
    }

    //Permet d'ajouter une personne au carnet en saissisant
    public void ajoutPersonne(BufferedReader br, BdD bdd) throws IOException {
        
        Connaissance con = GestionContact.ajoutContact(br);
        
        if (con.testConnaissanceOK())
        {
            personnes.add(con);
            bdd.insertionContact(con);
        }
        else
            System.out.println("Ajout impossible : Le nom, le prenom et le numero de telephone ne peuvent etre null!");
    }
    
    //Permet d'ajouter une personne au carnet via un objet
    public void ajoutPersonne(Connaissance con, BdD bdd) throws IOException {

        if (con.testConnaissanceOK())
        {
            personnes.add(con);
            bdd.insertionContact(con);
        }
        else
            System.out.println("Ajout impossible : Le nom, le prenom et le numero de telephone ne peuvent etre null!");
    }

    //Permet d'afficher une personne en particulier
    public  void affPersonne(String numTel) {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        
        if (con != null)
            System.out.println(con);
    }

    //Permet de modifier une personne
    public  void modifPersonne(String numTel, BufferedReader br, BdD bdd) throws IOException, ParseException {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        con.modifConnaissance(br, this, bdd);
    }

    //Permet de supprimer une personne
    public  void supprPersonne(String numTel, BdD bdd) {
        
        Connaissance con;
        
        con = recherchePers(numTel);
        
        bdd.deleteContact(numTel);
        personnes.remove(con);
    }

    //Permet d'afficher le carnet par nom
    public  void afficherCarnetParNom() {
        
        Collections.sort(personnes, new TriParNom());
        
        for (Connaissance con : personnes) 
        {
            System.out.println(con);
        }
    }

    //Permet d'afficher le carnet par type de personne
    public  void afficherCarnetParType() {
        
        System.out.println("Connaissances:");
        afficherGroupePersonne('c');
        System.out.println("Amis:");
        afficherGroupePersonne('a');
        System.out.println("Familles:");
        afficherGroupePersonne('f');
    }

    //Permet d'afficher le carnet par code postal
    public  void afficherCarnetParCodePostal() {
        
        Collections.sort(personnes, new TriParCodePostal());
        
        for (Connaissance con : personnes) 
        {
            System.out.println(con);
        }
    }

    //Permet d'afficher seulement un groupe de personne (Connaissance = C, Ami = A, Famille = F)
    public  void afficherGroupePersonne(char type) {
        
        for (Connaissance con : personnes) 
        {
            if (type == 'c' && con.getType() == 'c') {
                System.out.println(con);
            } else if (type == 'a' && con.getType() == 'a') {
                System.out.println(con);
            } else if (type == 'f' && con.getType() == 'f') {
                System.out.println(con);
            }            
        }
    }

    //Permet de trouver une personne dans la liste
    private  Connaissance recherchePers(String numTel) {
        
        for (Connaissance con : personnes) 
        {
            if (con.getTel().equals(numTel))
                return con;
        }
        
        System.out.println("Ce numero n'existe pas");
        return null;
    }
}
