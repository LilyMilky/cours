package POOHeritageTP2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AppMain {
    
    public static void main(String[] args) throws IOException {
        ObjetPostal BoiteALettres[] = new ObjetPostal[4];
        String saisieColis, saisie;
        float prix;
        
         //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        
        for(int i = 0; i < BoiteALettres.length; i++)
        {
            do
            {
                System.out.println("Voulez-vous envoyer une lettre ou un colis?(L/C): ");
                saisieColis = br.readLine();

                //Colis
                if(saisieColis.equalsIgnoreCase("C"))
                {
                    Colis colis = new Colis();
                    
                    System.out.println("Veuillez saisir l'adresse du destinataire: ");
                    colis.setAdresseDest(br.readLine());
                    System.out.println("Veuillez saisir le code postal: ");
                    colis.setCodePostal(br.readLine());
                    System.out.println("Veuillez saisir le nom du destinataire: ");
                    colis.setNomDest(br.readLine());
                    System.out.println("Veuillez saisir la ville de destination: ");
                    colis.setNomVilleDest(br.readLine());
                    
                    System.out.println("Veuillez saisir le poids du colis en gramme: ");
                    colis.setPoids(Integer.parseInt(br.readLine()));
                    
                    do
                    {
                        System.out.println("Recommande? (O/N)");
                        saisie = br.readLine();
                        
                        if(saisie.equalsIgnoreCase("O"))
                            colis.setRecommande(true);
                        else if (saisie.equalsIgnoreCase("N"))
                            colis.setRecommande(false);
                        else
                            System.out.println("Veuillez saisir une valeur valide");
                        
                    }while(!(saisie.equalsIgnoreCase("O")) && !(saisie.equalsIgnoreCase("N")));
                    
                    BoiteALettres[i] = colis;
                }
                //Lettre
                else if(saisieColis.equalsIgnoreCase("L"))
                {
                    Lettre lettre = new Lettre();
                    
                    System.out.println("Veuillez saisir l'adresse du destinataire: ");
                    lettre.setAdresseDest(br.readLine());
                    System.out.println("Veuillez saisir le code postal: ");
                    lettre.setCodePostal(br.readLine());
                    System.out.println("Veuillez saisir le nom du destinataire: ");
                    lettre.setNomDest(br.readLine());
                    System.out.println("Veuillez saisir la ville de destination: ");
                    lettre.setNomVilleDest(br.readLine());
                    
                    //Recommande
                    do
                    {
                        System.out.println("Recommande? (O/N)");
                        saisie = br.readLine();
                        
                        if(saisie.equalsIgnoreCase("O"))
                            lettre.setRecommande(true);
                        else if (saisie.equalsIgnoreCase("N"))
                            lettre.setRecommande(false);
                        else
                            System.out.println("Veuillez saisir une valeur valide");
                        
                    }while(!(saisie.equalsIgnoreCase("O")) && !(saisie.equalsIgnoreCase("N")));
                   
                    //Urgence
                    do
                    {
                        System.out.println("Urgent? (O/N)");
                        saisie = br.readLine();
                        
                        if(saisie.equalsIgnoreCase("O"))
                            lettre.setUrgence(true);
                        else if (saisie.equalsIgnoreCase("N"))
                            lettre.setUrgence(false);
                        else
                            System.out.println("Veuillez saisir une valeur valide");
                        
                    }while(!(saisie.equalsIgnoreCase("O")) && !(saisie.equalsIgnoreCase("N")));
                    
                    BoiteALettres[i] = lettre;
                }
                else
                {
                    System.out.println("Veuillez saisir une valeur valide!");
                }
            }while(!(saisieColis.equalsIgnoreCase("C")) && !(saisieColis.equalsIgnoreCase("L")));
        }
        
        //Affichage
        for(int i = 0; i < BoiteALettres.length; i++)
        {
            System.out.println("*** Objet num: " + i);
            System.out.println(BoiteALettres[i]);
            prix = BoiteALettres[i].calculPrixAffranchissement();
            System.out.println("Son tarif d'affranchissement = " + prix + "€");                    
        }
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
}
