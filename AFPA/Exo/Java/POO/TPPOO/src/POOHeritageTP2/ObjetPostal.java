
package POOHeritageTP2;

public abstract class ObjetPostal {
    private String nomDest;
    private String adresseDest;
    private String codePostal;
    private String nomVilleDest;
    private boolean recommande;

    //Constructeur
    public ObjetPostal() {
    }

    public ObjetPostal(String nomDest, String adresseDest, String codePostal, String nomVilleDest, boolean recommande) {
        this.nomDest = nomDest;
        this.adresseDest = adresseDest;
        this.codePostal = codePostal;
        this.nomVilleDest = nomVilleDest;
        this.recommande = recommande;
    }
    
    
    public String getNomDest() {
     
       return nomDest;
      
    }

    public void setNomDest(String nomDest) {
        this.nomDest = nomDest;
    }

    public String getAdresseDest() {
        return adresseDest;
    }

    public void setAdresseDest(String adresseDest) {
        this.adresseDest = adresseDest;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getNomVilleDest() {
        return nomVilleDest;
    }

    public void setNomVilleDest(String nomVilleDest) {
        this.nomVilleDest = nomVilleDest;
    }

    public boolean isRecommande() {
        return recommande;
    }

    //Getter setter
    public void setRecommande(boolean recommande) {
        this.recommande = recommande;
    }

    @Override
    public String toString() {
        String recommandeString = "NON";
        if(this.recommande == true)
            recommandeString = "OUI";
        
        return "Nom Destinataire: " + nomDest + ", adresse: " + adresseDest
                + ", code postal : " + codePostal + ", nom ville destination: " + nomVilleDest
                + ", Recommande: " + recommandeString;
    }
    
    public abstract float calculPrixAffranchissement();
    
}
