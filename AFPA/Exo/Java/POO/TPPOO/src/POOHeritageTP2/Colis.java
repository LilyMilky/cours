package POOHeritageTP2;

public class Colis extends ObjetPostal{
    
    private float poids; //en g

    //Constructeur
    public Colis() {
    }

    public Colis(float poids, String nomDest, String adresseDest, String codePostal, String nomVilleDest, boolean recommande) {
        super(nomDest, adresseDest, codePostal, nomVilleDest, recommande);
        this.poids = poids;
    }

    //Getter setter
    public float getPoids() {
        return poids;
    }

    public void setPoids(float poids) {
        this.poids = poids;
    }
    
    
    public float calculPrixAffranchissement()
    {
        float prix = 0;
        
        for(int i = 0; i <= poids/100;i++)
        {
            prix += 0.8;
        }
        
        if(isRecommande() == true)
        {
            prix += 3;
        }
        
        return prix;
    }
    
    @Override
    public String toString() {
        
        String recommandeString = "pas recommende";
        if(isRecommande() == true)
            recommandeString = "en recommende";
        
        return "Type = Colis\n" + "Nom: " + getNomDest() + ", Adresse: " + getAdresseDest()
                + " " + getCodePostal() + " " + getNomVilleDest()
                + ", " + recommandeString + "\nPoids: " + (poids/1000) + "Kgs";
    }
}
