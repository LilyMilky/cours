package POOHeritageTP2;

public class Lettre extends ObjetPostal{
    private boolean urgence;

    public Lettre() {
    }

    public Lettre(boolean urgence, String nomDest, String adresseDest, String codePostal, String nomVilleDest, boolean recommande) {
        super(nomDest, adresseDest, codePostal, nomVilleDest, recommande);
        this.urgence = urgence;
    }

    public boolean isUrgence() {
        return urgence;
    }

    public void setUrgence(boolean urgence) {
        this.urgence = urgence;
    }

   
    public float calculPrixAffranchissement()
    {
        float prixAffranchissement = 0.53f;
        
        if(isRecommande() == true)
        {
            prixAffranchissement += 1.5;
        }
        
        if(urgence == true)
        {
            prixAffranchissement += 0.6;
        }
        
        return prixAffranchissement;
    }
    
    @Override
    public String toString() {
        
        String recommandeString = "pas recommande";
        if(isRecommande() == true)
            recommandeString = "en recommande";
        
        String urgenceString = "pas urgent";
        if(urgence == true)
            urgenceString = "en urgence";
        
        return "Type = lettre\n" + "Nom: " + getNomDest() + ", Adresse: " + getAdresseDest()
                + " " + getCodePostal() + " " + getNomVilleDest()
                + ", " + recommandeString + ", " + urgenceString;
    }
}
