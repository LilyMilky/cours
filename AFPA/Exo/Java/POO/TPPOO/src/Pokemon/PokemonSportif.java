package Pokemon;

public class PokemonSportif extends PokemonTerrestre{

    private int freqCardiaque; //pulsation/min

    public PokemonSportif() {
    }

    public PokemonSportif(int freqCardiaque, int nbPatte, float taille, String nom, int poids) {
        super(nbPatte, taille, nom, poids);
        this.freqCardiaque = freqCardiaque;
    }

    
    public int getFreqCardiaque() {
        return freqCardiaque;
    }

    public void setFreqCardiaque(int freqCardiaque) {
        this.freqCardiaque = freqCardiaque;
    }
    
    @Override
    public String toString()
    {
        return "Je suis le pokemon " + getNom() + " mon poids est de "
                + getPoids() + " kg, ma vitesse est de " + getDf().format(calculVitesse()) + 
                " km/h j'ai " + getNbPatte() + " pattes, ma taille est de "
                + getTaille() + "m ma frequence cardiaque est de " + freqCardiaque
                + " pulsations a la minute";
    }

}
