package Pokemon;

public class PokemonCasaniers extends PokemonTerrestre{

    private int nbHeureTV;
 
    public PokemonCasaniers() {
    }

    public PokemonCasaniers(int nbHeureTV, int nbPatte, float taille, String nom, int poids) {
        super(nbPatte, taille, nom, poids);
        this.nbHeureTV = nbHeureTV;
    }

    public int getNbHeureTV() {
        return nbHeureTV;
    }

    public void setNbHeureTV(int nbHeureTV) {
        this.nbHeureTV = nbHeureTV;
    }

    @Override
    public String toString()
    {
        return "Je suis le pokemon " + getNom() + " mon poids est de "
                + getPoids() + " kg, ma vitesse est de " + getDf().format(calculVitesse()) + 
                " km/h j'ai " + getNbPatte() + " pattes, ma taille est de "
                + getTaille() + "m je regarde la tele " + nbHeureTV
                + "h par jour";
    }
}
