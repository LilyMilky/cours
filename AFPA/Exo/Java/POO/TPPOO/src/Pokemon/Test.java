package Pokemon;

public class Test {
    
    public static void main(String[] args) {
        Pokedex colPoke = new Pokedex();
        
        
        PokemonSportif pokeS = new PokemonSportif(120, 2, 0.85f, "Pikachu", 18);

        System.out.println(pokeS);
        colPoke.insertArray(pokeS);
        
        pokeS = new PokemonSportif(80, 4, 0.55f, "Carapuce", 15);

        System.out.println(pokeS);
        colPoke.insertArray(pokeS);
        
        pokeS = new PokemonSportif(100, 2, 1.85f, "Dracolosse", 80);

        System.out.println(pokeS); 
        colPoke.insertArray(pokeS);
        //-------------------
        PokemonCasaniers pokeC = new PokemonCasaniers(8, 2, 0.65f, "Salameche", 12);
        
        System.out.println(pokeC);
        colPoke.insertArray(pokeC);
        //--------------------------
        PokemonDesMers pokeM = new PokemonDesMers(2, "Rondoudou", 45);
        
        System.out.println(pokeM);
        colPoke.insertArray(pokeM);
        //---------------------------
        PokemonDeCroisiere pokeCr = new PokemonDeCroisiere(3, "Bulbizarre", 15);
        
        System.out.println(pokeCr);
        colPoke.insertArray(pokeCr);

        System.out.println("Vitesse moyenne: " + pokeC.getDf().format(colPoke.vitesseMoyenne()) + "km/h");
        System.out.println("Vitesse moyenne Sportif: " + pokeC.getDf().format(colPoke.vitesseMoyenneSportifs()) + "km/h");
    }
}
