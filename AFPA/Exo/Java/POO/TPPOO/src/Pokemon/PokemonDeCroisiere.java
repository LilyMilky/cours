package Pokemon;

public class PokemonDeCroisiere extends PokemonMarin{
    
    public PokemonDeCroisiere() {
    }

    public PokemonDeCroisiere(int nbNageoire, String nom, int poids) {
        super(nbNageoire, nom, poids);
    }
    
    @Override
    public String toString()
    {
        return "Je suis le pokemon " + getNom() + " mon poids est de "
                + getPoids() + " kg, ma vitesse est de " + getDf().format(calculVitesse()) + 
                " km/h j'ai " + getNbNageoire() + " nageoires.";
    }
    
    @Override
    public float calculVitesse()
    {
        return ((float)getPoids()/25 * getNbNageoire()) /2;
    }
}
