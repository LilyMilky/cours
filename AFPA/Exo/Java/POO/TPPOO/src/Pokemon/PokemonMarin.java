package Pokemon;

public abstract class PokemonMarin extends Pokemon{
    
    private int nbNageoire;

    public PokemonMarin() {
    }

    public PokemonMarin(int nbNageoire, String nom, int poids) {
        super(nom, poids);
        this.nbNageoire = nbNageoire;
    }

    public int getNbNageoire() {
        return nbNageoire;
    }

    public void setNbNageoire(int nbNageoire) {
        this.nbNageoire = nbNageoire;
    }
    
    @Override
    public float calculVitesse()
    {
        return ((float)getPoids()/25 * nbNageoire);
    }
}
