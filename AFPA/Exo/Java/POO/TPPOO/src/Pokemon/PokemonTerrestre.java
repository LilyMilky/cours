package Pokemon;

public  class PokemonTerrestre extends Pokemon{
    
    private int nbPatte;
    private float taille; //m

    public PokemonTerrestre() {
    }

    public PokemonTerrestre(int nbPatte, float taille, String nom, int poids) {
        super(nom, poids);
        this.nbPatte = nbPatte;
        this.taille = taille;
    }

    public int getNbPatte() {
        return nbPatte;
    }

    public void setNbPatte(int nbPatte) {
        this.nbPatte = nbPatte;
    }

    public float getTaille() {
        return taille;
    }

    public void setTaille(float taille) {
        this.taille = taille;
    }
    
    public float calculVitesse()
    {
        return(nbPatte * taille * 3);
    }
    
}
