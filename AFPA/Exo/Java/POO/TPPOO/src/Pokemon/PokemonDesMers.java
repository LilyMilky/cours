package Pokemon;

public class PokemonDesMers extends PokemonMarin{

    public PokemonDesMers() {
    }

    public PokemonDesMers(int nbNageoire, String nom, int poids) {
        super(nbNageoire, nom, poids);
    }

    @Override
    public String toString()
    {
        return "Je suis le pokemon " + getNom() + " mon poids est de "
                + getPoids() + " kg, ma vitesse est de " + getDf().format(calculVitesse()) + 
                " km/h j'ai " + getNbNageoire()+ " nageoires.";
    }
}
