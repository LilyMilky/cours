
package Pokemon;

import java.util.ArrayList;

public class Pokedex {
    
    private ArrayList arrayL = new ArrayList();

    public Pokedex() {
    }

    public ArrayList getArrayL() {
        return arrayL;
    }

    public void setArrayL(ArrayList arrayL) {
        this.arrayL = arrayL;
    }
    
    public void insertArray(Pokemon poke)
    {
        arrayL.add(poke);
    }
    
    public float vitesseMoyenne()
    {
        float vitMoy = 0;
        Pokemon pok;
        
        for(int i = 0; i < arrayL.size(); i++)
        {
            pok = (Pokemon)arrayL.get(i);
            vitMoy += pok.calculVitesse();
        }
        
        return (vitMoy/arrayL.size());
    }
    
    public float vitesseMoyenneSportifs()
    {
        float vitMoy = 0;
        int nbPokSportif = 0;
        PokemonSportif pokS;
        
        for(int i = 0; i < arrayL.size(); i++)
        {
            if(arrayL.get(i) instanceof PokemonSportif)
            {
                pokS = (PokemonSportif)arrayL.get(i);
                vitMoy += pokS.calculVitesse();
                nbPokSportif++;
            }
        }

        return vitMoy/nbPokSportif;
    }
    
}
