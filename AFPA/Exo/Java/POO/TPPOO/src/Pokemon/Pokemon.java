
package Pokemon;

import java.text.DecimalFormat;

public abstract class Pokemon {
    private String nom;
    private int poids; //en kg
    private DecimalFormat df = new DecimalFormat("#######0.0"); 

    public Pokemon(String nom, int poids) {
        this.nom = nom;
        this.poids = poids;
    }

    public Pokemon() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public DecimalFormat getDf() {
        return df;
    }
    
    public abstract float calculVitesse();
}
