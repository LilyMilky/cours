
package POOHeritageTP2Ex2;

public class Voiture extends VehiculeMotorise{

    public Voiture() {
    }

    public Voiture(Moteur moteur, String modele) {
        super(moteur, modele);
    }
    
    
    
    public void rouler(int consom)
    {
        System.out.println("Je roule en voiture et consomme: " + consom + " litres");
    }
    
}
