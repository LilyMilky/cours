package POOHeritageTP2Ex2;

public class VehiculeMotorise extends Vehicule{
    
    private Moteur moteur;

    public VehiculeMotorise() {
    }

    public VehiculeMotorise(Moteur moteur, String modele) {
        super(modele);
        this.moteur = moteur;
    }

    public Moteur getMoteur() {
        return moteur;
    }

    public void setMoteur(Moteur moteur) {
        this.moteur = moteur;
    }
    
    public boolean demarrer()
    {
        return moteur.demarrer();
    }
    
    public void arreter()
    {
        moteur.arreter();
    }
    
    public void faireLePlein(int essence)
    {
        moteur.faireLePlein(essence);
    }
}