package POOHeritageTP2Ex2;

public class Moteur {
    private int carburant;

    public Moteur() {
    }

    public Moteur(int carburant) {
        this.carburant = carburant;
    }

    public int getCarburant() {
        return carburant;
    }

    public void setCarburant(int carburant) {
        this.carburant = carburant;
    }
    
    public boolean demarrer()
    {
        boolean demarrage = false;
        
        if(carburant > 0)
        {
            demarrage = true;
            System.out.println("Le moteur est demarré avec " + carburant + "litres dans le reservoir");
            carburant--;
            System.out.println("Je viens de consommer 1 litre pour demarrer");
        }
        else
        {
            System.out.println("La voiture est en panne et ne peut redemarrer.");
        }
        
        return demarrage;
    }
    
    public int utiliser(int consom)
    {
        if (carburant > 0) {
            if(consom < carburant)
            {
                carburant -= consom;
                System.out.println("Le moteur utilise " + consom + "litres -----> Il reste " + carburant + "litres");
            }
            else
            {                
                System.out.println("Le moteur a utilisé le reste de son carburant! Il n'a utiliser que " + carburant + " litres avant de tomber en panne");
                carburant = 0;
                return (consom - carburant);
            }
        }
        return carburant;
    }
    
    public void arreter()
    {
        System.out.println("Le moteur est arrete");
    }
    
    public void faireLePlein(int essence)
    {
        carburant += essence;
        System.out.println("Plein effectue avec " + essence + "litres");
    }
}
