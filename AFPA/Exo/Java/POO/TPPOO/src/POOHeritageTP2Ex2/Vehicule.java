package POOHeritageTP2Ex2;

public abstract class Vehicule {
    private String modele;

    public Vehicule() {
    }

    public Vehicule(String modele) {
        this.modele = modele;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }  
}
