package POOHeritageTP2Ex2;

public class Moto extends VehiculeMotorise{

    public Moto() {
    }

    public Moto(Moteur moteur, String modele) {
        super(moteur, modele);
    }
           
    
    public void rouler(int consom)
    {
        System.out.println("Je roule en moto et consomme: " + consom + " litres");
        
        
    }
    
}
