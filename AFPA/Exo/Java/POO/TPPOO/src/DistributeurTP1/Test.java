package DistributeurTP1;

import java.io.IOException;

public class Test 
{
    public static void main(String[] args) throws IOException
    {
        Compte tabCompte[] = new Compte[2];
        Compte compte = new Compte(1500.5f, "1255");
        Compte compte2 = new Compte(3500.5f, "1256");
        
        tabCompte[0] = compte;
        tabCompte[1] = compte2;
         
         DAB dab = new DAB();
         
         dab.activer(tabCompte);
    }
}
