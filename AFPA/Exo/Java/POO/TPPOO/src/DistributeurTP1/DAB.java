package DistributeurTP1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DAB 
{
    public void activer(Compte compte[]) throws IOException
    {
        int choix ,montant, compteVirement;
        int choixCompte = 0;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        
        do
        {
            System.out.println("0-Selectionner compte courant");
            System.out.println("1-Retrait");
            System.out.println("2-Depot");
            System.out.println("3-Solde");
            System.out.println("4-Quit");
            System.out.println("5-Virement");
            
            choix = Integer.parseInt(br.readLine());

            if(choix >= 0 && choix <= 5)
            {
                System.out.println("Vous avez fait le choix: " + choix);

                switch(choix)
                {
                    //Selection d'un compte
                    case 0: 
                        choixCompte = selectionCompte(compte, br);
                        break;
                        
                    //Retirer
                    case 1:
                        System.out.println("Combien souhaitez vous retirer? ");
                        montant = Integer.parseInt(br.readLine());
                        compte[choixCompte].retirer(montant);                
                        break;
                        
                    //Deposer
                    case 2:
                        System.out.println("Combien souhaitez vous deposer? ");
                        montant = Integer.parseInt(br.readLine());
                        compte[choixCompte].deposer(montant);  
                        break;
                    
                    //Affichage du solde
                    case 3:
                        System.out.println(compte[choixCompte].infos());
                        break;
                    
                    //Quitter
                    case 4:
                        System.out.println("Au revoir");
                        break;
                        
                    //Virement
                    case 5:
                        System.out.println("Veuillez selectionner un compte sur lequel faire le virement");
                        compteVirement = selectionCompte(compte, br);
                        
                        System.out.println("Veuillez selectionner un montant");
                        montant = Integer.parseInt(br.readLine());
                        compte[choixCompte].virement(compte[compteVirement], montant);
                        break;
                        
                    //Choix invalide
                    default:
                        System.out.println("Vous devez entrer un choix valide!");
                }
            }

        }while(choix != 4);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
    //Permet de selectionner un compte via le numero de compte
    public int selectionCompte(Compte compte[], BufferedReader br) throws IOException
    {
        boolean choixOK = false;
        String choixCompte;
        int choix = 0;
        
        for (int i = 0; i < compte.length; i++) 
        {
            System.out.println(compte[i].getNumCompte());
        }

        do {
            System.out.println("Selectionner un numero de compte: ");
            choixCompte = br.readLine();

            for (int i = 0; i < compte.length; i++) {
                if (choixCompte.equals(compte[i].getNumCompte())) {
                    choixOK = true;
                    choix = i;
                    break;
                }
            }
        } while (choixOK == false);
        
        return choix;
    }
}
