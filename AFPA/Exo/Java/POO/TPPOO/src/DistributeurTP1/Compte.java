package DistributeurTP1;

public class Compte 
{
    private float soldeCompte;
    private String numCompte;

    public Compte() {
    }

    public Compte(float soldeCompte, String numCompte) {
        this.soldeCompte = soldeCompte;
        this.numCompte = numCompte;
    }

    public float getSolde() {
        return soldeCompte;
    }

    public void setSoldeCompte(float soldeCompte) {
        this.soldeCompte = soldeCompte;
    }

    public String getNumCompte() {
        return numCompte;
    }

    public void setNumCompte(String numCompte) {
        this.numCompte = numCompte;
    }
    
    //Permet de deposer de l'argent d'un compte
    public void deposer(int s)
    {
        soldeCompte += s;
    }
    
    //Permet de retirer de l'argent d'un compte
    public void retirer(int s)
    {
        if(soldeCompte > s)
            soldeCompte -= s;
        else
            System.out.println("Vous n'avez pas les moyens !");
    }
    
    //Affiche les infos d'un compte
    public String infos()
    {
        return "Compte numero: " + numCompte + ", soldes: " + soldeCompte +" €.";
    }
    
    //Permet de faire un virement d'un compte a l'autre
    public void virement(Compte compte, int virement)
    {
        if(soldeCompte > virement)
        {
            soldeCompte -= virement;
            compte.soldeCompte += virement;
        }
        else
            System.out.println("Vous n'avez pas les moyens !");
    }  
}
