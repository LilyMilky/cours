package exjavaobjetexpositioncanine;
import Objets.Chien;
import Objets.Proprietaire;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExJavaObjetExpositionCanine 
{

    public static void main(String[] args) throws IOException, ParseException 
    {
        /*A une exposition canine sont presentés des chiens de race
        Chaque chien a un proprietaire humain.
        Créer les classes objet ainsi que la classe main
        Qui permet d'afficher pour un chien donné
        -Son prenom
        -Son poids          ET          Le nom + prenom de son proprio
        -Sa race
        -Sa couleur
        -Son age
        */
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        Proprietaire proprio[];
        Chien chien[];
        
        int nbProprio, nbChien, ageChien;
        String nomProprio, prenomProprio, dateNaissance;
        Proprietaire PropriChien = null;
        
        System.out.print("Veuillez saisir le nombre de chien: ");
        nbChien = Integer.parseInt(br.readLine());
        
        System.out.print("Veuillez saisir le nombre de proprietaire: ");
        nbProprio = Integer.parseInt(br.readLine());
        
        proprio = new Proprietaire[nbProprio];
        chien = new Chien[nbChien];

        //Creation des proprios
        for(int i = 0; i < nbProprio; i++)
        {
            System.out.print("Veuillez saisir le nom du proprietaire " + (i+1) + ": ");
            nomProprio = br.readLine();
            System.out.print("Veuillez saisir le prenom du proprietaire " + (i+1) + ": ");
            prenomProprio = br.readLine();
            
            proprio[i] = new Proprietaire(nomProprio, prenomProprio);
        }

        //Creation des chiens
        for(int i = 0; i < nbChien; i++)
        {
            PropriChien = null;
            
            chien[i] = new Chien();
            
            do
            {
                System.out.print("Veuillez saisir le nom du proprietaire du chien " + (i+1) + ": ");
                nomProprio = br.readLine();

                for(int j = 0; j < nbProprio; j++)
                {
                    if(nomProprio.equals(proprio[j].getNomProprio()))
                    {
                        PropriChien = proprio[j];
                        break;
                    }
                }
            }
            while(PropriChien == null);
            
            chien[i].setPropri(PropriChien);
            
            System.out.print("Veuillez saisir le prenom du chien " + (i+1) + ": ");
            chien[i].setPrenomChien(br.readLine());
            System.out.print("Veuillez saisir le poids du chien " + (i+1) + ": ");
            chien[i].setPoidsChien(Float.valueOf(br.readLine()));
            System.out.print("Veuillez saisir la race du chien " + (i+1) + ": ");
            chien[i].setRaceChien(br.readLine());
            System.out.print("Veuillez saisir la couleur du chien " + (i+1) + ": ");
            chien[i].setCouleurChien(br.readLine());
            System.out.print("Veuillez saisir la date de naissance du chien " + (i+1) + ": ");
            dateNaissance = br.readLine();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date d = sdf.parse(dateNaissance);  
            
            chien[i].setDateNaiss(d);
        }
        
        //Affichage
        for(int i = 0; i < nbChien; i ++)   
        {
            System.out.println(chien[i]);
        }
                
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
