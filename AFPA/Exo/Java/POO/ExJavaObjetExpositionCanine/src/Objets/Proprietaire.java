package Objets;

public class Proprietaire 
{
    private String nom;
    private String prenom;
    
    public Proprietaire(){}
    
    public Proprietaire(String nom, String prenom)
    {
        this.nom = nom;
        this.prenom = prenom;
    }
    
    //Getter
    public String getNomProprio()
    {
        return nom;
    }
    
    public String getPrenomProprio()
    {
        return prenom;
    }
    
    //Setter
    public void setPrenomProprio(String nom)
    {
        this.nom = nom;
    }
    
    public void setNomProprio(String prenom)
    {
        this.prenom = prenom;
    }
}
