package Objets;

import java.util.Calendar;
import java.util.Date;

public class Chien 
{
    private String prenomChien;
    private float poids; // En kg
    private String race;
    private String couleur;
    private Proprietaire proprio;
    private Date dateNaiss;
    
    public Chien(){};
    
    public Chien(String prenom, float poids, String race, String couleur, Date dateNaiss, Proprietaire proprio)
    {
        prenomChien = prenom;
        this.poids = poids;
        this.race = race;
        this.couleur = couleur;
        this.dateNaiss = dateNaiss;
        this.proprio = proprio;
    };
    
    //Getter
    public String getPrenomChien()
    {
        return prenomChien;
    }
    
    public float getPoidsChien()
    {
        return poids;
    }
    
    public String getRaceChien()
    {
        return race;
    }
    
    public String getCouleurChien()
    {
        return couleur;
    }
    
    public Date getdateNaissance()
    {
        return dateNaiss;
    }
    
    public Proprietaire getPriprio()
    {
        return proprio;
    }
    
    //Setter
    public void setPrenomChien(String prenom)
    {
        prenomChien = prenom;
    }
    
    public void setPoidsChien(float poids)
    {
        this.poids = poids;
    }
    
    public void setRaceChien(String race)
    {
        this.race = race;
    }
    
    public void setCouleurChien(String couleur)
    {
        this.couleur = couleur;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }
    
    public void setPropri(Proprietaire proprio)
    {
        this.proprio = proprio;
    }
    
    //Calcul de l'age
    public int calculAge()
    {
        Calendar dateActuel = Calendar.getInstance();
        Calendar dateNaissance = Calendar.getInstance();
        dateNaissance.setTime(dateNaiss);
        int yeardiff = dateActuel.get(Calendar.YEAR) - dateNaissance.get(Calendar.YEAR);
        dateActuel.add(Calendar.YEAR,-yeardiff);
        //Si la date de naissance est apres la date actuel, on reduit l'age d'un an.
        if(dateNaissance.after(dateActuel))
        {
          yeardiff = yeardiff - 1;
        }
        return yeardiff;
   }
    
    //Affichage
    
    @Override
    public String toString() 
    {
 
       return "Ce chien s'appelle " + prenomChien
                + ", il pèse " + poids + "kg"
                + ", sa race est " + race
                + ", il est de couleur " + couleur
                + ", il a " + calculAge() + " ans."
                + " Son proprietaire est monsieur: " + proprio.getNomProprio() + " " + proprio.getPrenomProprio(); 
    }
}
