/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Outil;

/**
 *
 * @author cdi209
 */
public class Affichage {
    
    //Affiche les nombres de 1 à 10
    public static void affiche1a10()
    {
        for(int i = 1; i <= 10; i++)
        {
            System.out.println(i);
        }
        
    }
    
    //Affiche la somme des nombres de 1 à 10
    public static void afficheSomme1a10()
    {
        int somme = 0;
        
        for(int i = 1; i <= 10; i++)
        {
            somme += i;
        }
        
        System.out.println("Voici la somme "+ somme);
        
    }
    
    //Affichage d'un tableau
    public static void afficheTableau(int tab[])
    {
        for(int i = 0; i < tab.length; i++)
        {
            System.out.println(tab[i]);
        }     
    }
    
    public static void affichageTrouve(boolean trouve)
    {
        if(trouve == true)
        {
            System.out.println("Valeur trouvee");
        }
        else
        {
            System.out.println("Valeur non trouvee");
        }
    }
    
    public static void affichage(StringBuffer chaine)
    {
        System.out.println("Chaine: " + chaine);
    }
    
    public static void affichage(String chaine)
    {
        System.out.println("Chaine: " + chaine);
    }
    
    public static void affichage(String chaine, int nbLettreRecherche)
    {
        if(nbLettreRecherche > 0)
            System.out.println("Il y a " + nbLettreRecherche + " fois votre lettre");
        else
            System.out.println("Lettre absente");
    }
    
    public static void affichage(int tab[], char Alphabet[])
    {
        for(int i = 0; i < tab.length; i++)
        {
            System.out.println("La lettre '" + Alphabet[i] + "' est presente: " + tab[i] + " fois");
        }
    }
    
    public static void affichage(int tab[])
    {
        System.out.println("Plus Petit: " + tab[0] + " rang: " + tab[1]);
    }
    
    public static void affichage(int tab[], int rangPlusPetit, int rangPlusGrand)
    {
        System.out.println("Plus grand nombre: " + tab[rangPlusGrand] 
                + " et son rang: " + rangPlusGrand);
        System.out.println("Plus petit nombre: " + tab[rangPlusPetit] 
                + " et son rang: " + rangPlusPetit);
    }
    
    public static void affichage(int lecture[], int rangGrand, float moyenne, String nbrInfMoyenne)
    {
        System.out.println("Nombre le plus grand: " + lecture[rangGrand] 
                 + " rang: " + rangGrand);
         System.out.println("Moyenne: " + moyenne);
         System.out.println("Nombre inferieur a la moyenne: " + nbrInfMoyenne);
    }
}
