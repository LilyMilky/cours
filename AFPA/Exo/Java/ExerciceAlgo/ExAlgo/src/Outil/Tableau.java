/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Outil;

import java.io.BufferedReader;
import java.io.IOException;

/**
 *
 * @author cdi209
 */
public class Tableau 
{
    public static int[] remplissageTableau(int tab[], BufferedReader br) throws IOException 
    {
        //Remplissage du tableau
        for(int i = 0; i < tab.length; i++)
        {
            System.out.println("Veuillez saisir un nombre: ");
            tab[i] = Integer.parseInt(br.readLine());
        }
        
        return tab;
    }
    
    //Inversion des données d'un tableau (int)
    public static int[] inversionTableau(int tab[])
    {
        int temp;
        
        for(int i = 0; i <= tab.length / 2; i++)
        {               
            temp = tab[i];
            tab[i] = tab[tab.length - i - 1];
            tab[tab.length - i - 1] = temp;
        }
        
        return tab;
    }
    
    //Inversion des données d'un tableau (string)
    public static String[] inversionTableau(String tab[])
    {
        String temp;
        
        for(int i = 0; i <= tab.length / 2; i++)
        {               
            temp = tab[i];
            tab[i] = tab[tab.length - i - 1];
            tab[tab.length - i - 1] = temp;
        }
        
        return tab;
    }
    
    //Trie tableau par ordre croissant
    public static int[] trieTableau(int tab[])
    {
        int temp;
        for(int i = 0; i < tab.length; i++)
        {
            for(int j = i + 1; j < tab.length; j++)
            {
                if(tab[j] < tab[i])
                {
                    temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                }
            }
        }
        
        return tab;
    }
    
    //Fusion de 2 tableau trié par ordre croissant
    
    public static int[] fusionTableauCroissant(int tab[], int tab2[], int tabFinal[])
    {
        int indiceLecture = 0, indiceLecture2 = 0, indiceTabFinal = 0;
        
        while((indiceLecture < tab.length) && (indiceLecture2 < tab2.length))
        {
            if(tab[indiceLecture] < tab2[indiceLecture2])
            {
                tabFinal[indiceTabFinal] = tab[indiceLecture];
                indiceLecture++;
                indiceTabFinal++;
            }
            else
            {
                tabFinal[indiceTabFinal] = tab2[indiceLecture2];
                indiceLecture2++;
                indiceTabFinal++;
            }
        }
        
        //Si l'un des tableaux arrive à la fin, on copie la fin du 2eme tableau à la suite
        while(indiceLecture < tab.length)
        {
            tabFinal[indiceTabFinal] = tab[indiceLecture];
            indiceLecture++;
            indiceTabFinal++;
        }
        
        while(indiceLecture2 < tab2.length)
        {
            tabFinal[indiceTabFinal] = tab2[indiceLecture2];
            indiceLecture2++;
            indiceTabFinal++;
        }
        
        return tabFinal;
    }
    
    public static boolean rechercheDichotomique(boolean trouve, int lecture[], int valeurRecherche)
    {
        int indiceDebut = 0, indiceFin = lecture.length, indiceMilieu;
        
        while((trouve == false) && ((indiceFin - indiceDebut) >= 1))
        {
            indiceMilieu = (indiceDebut + indiceFin) / 2;
            
            if(lecture[indiceMilieu] == valeurRecherche)
            {
                trouve = true;
            }
            else if(lecture[indiceMilieu] > valeurRecherche)
            {
                indiceFin = indiceMilieu;
            }
            else
            {
                indiceDebut = indiceMilieu;
            }
        }
        
        return trouve;
    }
    
    public static boolean rechercheSequentiel(int lecture[], boolean trouve, int valeurRecherche)
    {
        for(int i = 0; i < lecture.length; i++)
        {
            if(lecture[i] == valeurRecherche)
            {
                trouve = true;
            }
        }
        return trouve;
    }
    
    //Initialisation tableau à 0
    public static int[] initialisationTableau(int tab[])
    {
        for(int i = 0; i < tab.length; i ++)
        {
            tab[i] = 0;
        }
        
        return tab;
    }
    
    //Trouve nombre plus grand et plus petit
    public static int[] nombreMinMax(int[] lecture, int[] rang)
    {
        for(int i = 1; i < lecture.length; i++)
        {
            if(lecture[i] > lecture[rang[1]])
            {
                rang[1] = i;
            }
            
            if(lecture[i] < lecture[rang[0]])
            {
                rang[0] = i;
            }
        }
        
        return rang;
    }
    
    //Liste nombre inferieur a la moyenne
    public static String listeNbrInfMoyenne(int[] lecture, float moyenne)
    {
        String nbrInfMoyenne = "";
        for(int i = 0; i < lecture.length; i++)
        {
            if(lecture[i] < moyenne)
            {
                nbrInfMoyenne += lecture[i] + "  ";
            }
        }
        
        return nbrInfMoyenne;
    }
    
    //Trouve le nombre le plus grand + somme des nombres
    public static int[] nbPLusGrandSommeNb(int[] tab, int[] lecture)
    {
        tab[1] = lecture[0];
        
        for(int i = 1; i < lecture.length; i++)
        {
            if(lecture[i] > lecture[tab[0]])
            {
                tab[0] = i;
            }
            
            tab[1] += lecture[i];
        }
        
        return tab;
    }
}
