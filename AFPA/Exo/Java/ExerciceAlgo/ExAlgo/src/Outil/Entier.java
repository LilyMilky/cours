package Outil;

import java.io.BufferedReader;
import java.io.IOException;

public class Entier 
{
    //Recupere un nombre saisie par l'utilisateur
    public static int recuperationNombre(BufferedReader br) throws IOException
    {
        int n;
        
        System.out.println("Veuillez saisir le premier nombre: ");
        n = Integer.parseInt(br.readLine());
        
        return n;
    }
    
    //Somme des nombres pair de n à m
    
    public static int sommePair(int n, int m)
    {
        int somme = 0;
         //Test si n est pair
        if(((n/2)*2) != n)
        {
            n++;
        }
        
        for(int i = n;i <= m; i = i + 2)
        {
            somme += i;
        }
        
        return somme;
    }
    
    //Lis 10 nombres et trouve le plus grand
    public static int nombrePlusGrand(BufferedReader br) throws IOException
    {
        int plusGrand = 0, lecture;
        
        for(int i = 1; i <= 10; i++)
        {
            System.out.println("Veuillez saisir un nombre: ");
            lecture = Integer.parseInt(br.readLine());
            
            if (i == 1)
            {
                plusGrand = lecture;
            }
            
            if(lecture > plusGrand)
            {
                plusGrand = lecture;
            }
        }
        
        return plusGrand;
    }
    
    //Lis 10 nombres et trouve le plus petit
    public static int[] nombrePlusPetit(BufferedReader br, int[] tab) throws IOException
    {
        int lecture;
        for(int i = 1; i <= 10; i++)
        {
            System.out.println("Veuillez saisir un nombre: ");
            lecture = Integer.parseInt(br.readLine());
            
            if (i == 1)
            {
                tab[0] = lecture;
                
            }
            
            if(lecture < tab[0])
            {
                tab[0] = lecture;
                tab[1] = i;
            }
        }
        
        return tab;
    }
    
    //Lire + calcul de la somme 
    public static int calculMoyenne(BufferedReader br) throws IOException
    {
        int lecture, somme = 0, nbLecture = 0;
        String Fin;
        
        do
        {
            System.out.println("Veuillez saisir un nombre: ");
            lecture = Integer.parseInt(br.readLine());
            
            somme += lecture;
            nbLecture++;

            System.out.println("Pour arreter, tapez N: ");
            Fin = br.readLine();
                    
        }while(!"N".equals(Fin));
        
        return somme/nbLecture;
    }
}
