package Outil;

import java.io.BufferedReader;
import java.io.IOException;

public class ChaineCaractere 
{
    //Remplissage d'une chaine par l'utilisateur
    public static String remplissageChaine(String chaine, BufferedReader br) throws IOException
    {
        System.out.println("Veuillez saisir une chaine de caractere: ");
        chaine = br.readLine();
        
        return chaine;
    }
    
    //Remplacement des caractere d'une chaine par des *
    public static String remplacementChaine (String chaine)
    {
        for(int i = 0; i < chaine.length(); i++)
        {
            chaine = chaine.substring(0,i) + '*' + chaine.substring(i+1);
        }
        
        return chaine;
    }
    
    //Remplacement une lettre choisi par une autre
    public static String remplacementChaine(String chaine, char lettreARemplacer,char lettreRemplacante)
    {
        for(int i = 0; i < chaine.length(); i++)
        {
            if(chaine.charAt(i) == lettreARemplacer)
                chaine = chaine.substring(0,i) + lettreRemplacante + chaine.substring(i+1);
        }
        
        return chaine;
    }
   
    //Comptage d'une lettre dans une chaine
    public static int comptageLettreChaine(String chaine, char lettreRecherche)
    {
        int nbLettreRecherche = 0;
        
        for(int i = 0; i < chaine.length(); i++)
        {
            
            if(chaine.charAt(i) == lettreRecherche)
            {
                nbLettreRecherche ++;
            }
        
        }        
        return nbLettreRecherche;
    }
    
    
    
    //Comptage du nombre d'occurence de chaque lettre dans une chaine
    public static int[] comptageLettreChaine(String chaine, int nbLettre[], char Alphabet[])
    {
        for(int i = 0; i < chaine.length(); i++)
        {
            for(int j = 0; j < nbLettre.length; j++)
            {
                //On compare avec le tableau contenant les lettres
                if(chaine.toLowerCase().charAt(i) == Alphabet[j])
                {
                    nbLettre[j]++;
                    break;
                }
            }
        }     
        return nbLettre;
    }
    
    //Suppression des double (ou +) espace
    
    public static String suppressionEspace(String chaine,boolean modif)
    {
        char lastChar = chaine.charAt(0);
        
        for(int i = 0; i < chaine.length(); i++)
        {
            //On supprime les espaces tant qu'il y en a plus de 2
            do
            {
                modif = false;
                if(chaine.charAt(i) == lastChar && chaine.charAt(i) == ' ')
                {
                    chaine = chaine.substring(0,i) + chaine.substring(i+1);
                    modif = true;
                }
            }
            while(modif == true);
            
            lastChar = chaine.charAt(i);
        }
        
        return chaine;
    }
    
    //Comptage du nombre de mot
    public static int comptageMot(String chaine)
    {
        int nbMot = 0;
        
        for(int i = 0; i < chaine.length(); i++)
        {
            if(chaine.charAt(i) == ' ')
            {
                nbMot ++;
            }
        }
        
        return nbMot;
    }
    
    //Decoupage d'une chaine en mot
    public static String[] decoupageChaine(String chaine, String[] chaineDecoupe)
    {
        String mot = "";
        int indiceChaineDecoupe = 0;
        
        for(int i = 0; i< chaine.length(); i++)
        {
            mot += chaine.charAt(i);
            
            if(chaine.charAt(i) == ' ')
            {
                chaineDecoupe[indiceChaineDecoupe] = mot;
                indiceChaineDecoupe ++;
                mot = "";
            }
        }
        //Ajout du dernier mot dans la chaine
        chaineDecoupe[indiceChaineDecoupe] = (mot + ' ');
        
        return chaineDecoupe;
    }
    
    public static String tableauDansString(String[] chaineDecoupe, String chaineFinale)
    {
        for(int i = 0; i < chaineDecoupe.length; i++)
        {
            if (chaineDecoupe[i] != null)
                chaineFinale += chaineDecoupe[i];
        }
        
        return chaineFinale;
    }
}
