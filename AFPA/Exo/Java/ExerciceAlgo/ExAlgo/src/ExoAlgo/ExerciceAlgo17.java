package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo17 {
    
    public static void main(String[] args)  throws IOException
    {
        //17. Remplacer toutes les occurences d'une lettre
        //par une autre dans une chaine de caractere
        
        String chaine = "";
        char lettreARemplacer, lettreRemplacante;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        //Recuperation de la lettre à remplacer
        System.out.println("Veuillez saisir la lettre à remplacer: ");
        lettreARemplacer = (char) br.read();
        
        br.readLine();
        
        //Recuperation de la lettre remplacante
        System.out.println("Veuillez saisir la lettre à remplacante: ");
        lettreRemplacante = (char) br.read();
        
        //remplace la lettre choisi par une autre
        chaine = remplacementChaine(chaine, lettreARemplacer, lettreRemplacante);
        
        //Affichage
        affichage(chaine);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
