package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

public class ExerciceAlgo13 {

    public static void main(String[] args) throws IOException
    {
        //13. Dire si une valeur existe dans un tableau trié (séquentiel, dichotomique).
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);      
        
        int lecture [] = new int [10];
        int valeurRecherche;
        boolean trouve = false;    
        
        //Remplissage du tableau 
        lecture = remplissageTableau(lecture, br);
        
        System.out.println("Veuillez saisir la valeur recherchee: ");
        valeurRecherche = Integer.parseInt(br.readLine());
        
        //Trie du tableau 
        lecture = trieTableau(lecture);
        
        //Recherche SEQUENTIEL
        //trouve = rechercheSequentiel(lecture, trouve, valeurRecherche);
        
        //Recherche DICHOTOMIQUE
        trouve = rechercheDichotomique(trouve, lecture, valeurRecherche);
        
        affichageTrouve(trouve);
               
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
