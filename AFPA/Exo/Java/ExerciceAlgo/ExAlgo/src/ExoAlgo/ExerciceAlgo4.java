package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Affichage.*;
import static Outil.Entier.*;

public class ExerciceAlgo4 {

    public static void main(String[] args) throws IOException

    {
        // 4)Afficher la somme des nombres pairs de n à m.
        
        int n, m, somme = 0;
        
         //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Recuperation des nombres
        n = recuperationNombre(br);
        m = recuperationNombre(br);
        
        //Calcul somme des nombres pairs
        somme = sommePair(n, m);
        
        System.out.println(somme);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
