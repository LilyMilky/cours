package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Affichage.*;

public class ExerciceAlgo3 {
    
    public static void main(String[] args)  throws IOException
    {
        int n, m, temp;
        
         //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        System.out.println("Veuillez saisir le premier nombre: ");
        n = Integer.parseInt(br.readLine());
        System.out.println("Veuillez saisir le deuxieme nombre: ");
        m = Integer.parseInt(br.readLine());
        
        
        // VERSION 1 : Afficher un message d'erreur et arrêter le programme.
        /* 
        if(n > m)
        {
            System.out.println("Erreur n est superieur a m");
            System.exit(0);
        }
        else
        {
            for(int i = n;i <= m;i++)
            {
                System.out.println(i);
            }
        }
        -----------------------------------------------------------------*/
        
        //Version 2 : Permuter n et m, puis continuer.
        
        /*if(n > m)
        {
            temp = n;
            n = m;
            m = temp;                   
        }
        
        for(int i = n;i <= m;i++)
        {
            System.out.println(i);
        }*/
        
        //Version 3 : Afficher de n à m (de manière décroissante).
        
        if(n > m)
        {
           for(int i = n;i >= m;i--)
           {
               System.out.println(i);
           } 
        }
        
        
        else
        {
            for(int i = n;i <= m;i++)
            {
                System.out.println(i);
            }
        }
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
