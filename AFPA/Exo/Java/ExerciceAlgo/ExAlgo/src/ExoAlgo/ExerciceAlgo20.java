package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;
import static Outil.ChaineCaractere.*;

public class ExerciceAlgo20 {

    public static void main(String[] args) throws IOException
    {
        // 20. Compter le nombre d'occurrences de chaque lettre dans une chaine.
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        String chaine = "";
        char [] Alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        int nbLettre[] = new int[26];
        
        //Initialisation Tableau nbLettre
        nbLettre = initialisationTableau(nbLettre);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        
        //Comptage du nombre d'occurence de chaque lettre dans une chaine
        nbLettre = comptageLettreChaine(chaine, nbLettre, Alphabet);
        
        //Affichage
        affichage(nbLettre, Alphabet);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
