package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

public class ExerciceAlgo9 {

    public static void main(String[] args) throws IOException
    {
        //9. Dans un tableau de 10 entiers, afficher :
        //- le plus grand et sa position,
        //- la moyenne des entiers,
        //- les nombres inférieurs à la moyenne
        
        float moyenne;
        String nbrInfMoyenne;
        //Creation de tableau vide de 10 case
        int lecture [] = new int [10];
        //Tableau [0] = rangPlus grand, [1] = somme
        int tab[] = {0,0};
        
         //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage du tableau
        lecture = remplissageTableau(lecture, br);
        
        //Calcul somme + recuperation nbr plus grand
        tab = nbPLusGrandSommeNb(tab, lecture);
        
        moyenne = (float)tab[1]/lecture.length;
        
        //Nombre inferieur a la moyenne
        nbrInfMoyenne = listeNbrInfMoyenne(lecture, moyenne);
        
        affichage(lecture, tab[0], moyenne, nbrInfMoyenne);
         
         //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
