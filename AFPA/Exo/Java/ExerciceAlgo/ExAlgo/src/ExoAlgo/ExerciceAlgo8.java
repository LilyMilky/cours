package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

public class ExerciceAlgo8 {

    public static void main(String[] args)  throws IOException
    {
        // 8. Dans un tableau de 10 entiers, trouver les rangs du 
        //plus petit et du plus grand élément,
        //et afficher les rangs et leurs valeurs.
        
        //Creation de tableau vide de 10 case
        int lecture [] = new int [10];
        
        //rang[0] = petit, rang[1] = grand
        int rang[] = new int [2];

        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage du tableau
        lecture = remplissageTableau(lecture, br);
        
        //Trouver plus petit + plus grand
        rang = nombreMinMax(lecture, rang);
        
        //Affichage
        affichage(lecture, rang[0], rang[1]);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
