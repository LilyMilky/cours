package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo21 {

    public static void main(String[] args) throws IOException
    {
        //21. Remplacer les doubles-espaces (ou +) dans une chaine de caractere par un espace
        
        String chaine = "";
        boolean modif = false;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        chaine = suppressionEspace(chaine, modif);
        
        //Affichage
        affichage(chaine);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
