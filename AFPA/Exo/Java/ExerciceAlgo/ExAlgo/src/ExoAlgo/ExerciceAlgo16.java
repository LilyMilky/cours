package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo16 {
        
    public static void main(String[] args)  throws IOException
    {
        // 16. Remplacer tous les caractères d'une chaîne par une '*'
        
        String chaine = "";
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        //remplace les caractere par *
        chaine = remplacementChaine(chaine);
        
        //Affichage
        affichage(chaine);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
}
