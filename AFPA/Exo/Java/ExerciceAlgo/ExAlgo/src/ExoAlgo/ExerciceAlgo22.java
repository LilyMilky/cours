package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo22 {

    public static void main(String[] args) throws IOException
    {
        //Decouper une chaine de caractere en mot avec l'espace comme separateur
        //Et les compter
          
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        String chaine = "";
        int nbMot = 0;
        boolean modif = false;
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);      
        
        //Suppression des doubles-espace
        chaine = suppressionEspace(chaine, modif);
        
        //On compte le nombre d'espace
        nbMot = comptageMot(chaine);
        
        //Affichage, on rajoute un pour le dernier mot
        System.out.println("Il y a " + (nbMot + 1) + " mots dans la phrase");
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
