package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Entier.*;
import static Outil.Affichage.*;

public class ExerciceAlgo5 {

    public static void main(String[] args) throws IOException
    {
        // 5)Lire 10 nombres et trouver le plus grand.
        
        int plusGrand = 0;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        plusGrand = nombrePlusGrand(br);
        
        System.out.println("Plus Grand : " + plusGrand);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
