package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Entier.*;
import static Outil.Affichage.*;

public class ExerciceAlgo6 {

    public static void main(String[] args) throws IOException
    {
        // 6)Lire 10 nombres et trouver le plus petit et son rang.
        
        int tab[] = {0,1};
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        tab = nombrePlusPetit(br, tab);
        
        affichage(tab);
        
         //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
