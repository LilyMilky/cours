package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Entier.*;
import static Outil.Affichage.*;

public class ExerciceAlgo7 {

    public static void main(String[] args)  throws IOException
    {
        // 7) Lire n nombres et en calculer la moyenne.
        
        int moyenne;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
             
        moyenne = calculMoyenne(br);
        
        System.out.println("La moyenne: " + moyenne);
        
         //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
