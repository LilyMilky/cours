package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;
import static Outil.Tableau.*;

public class ExerciceAlgo23 {

    public static void main(String[] args) throws IOException
    {
        // 23. Inverser les mots d'une phrase.
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        String chaine = "", temp, chaineFinale = "";
        String chaineDecoupe[];
        boolean modif = false;
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        chaineDecoupe = new String[chaine.length()/2];
        
        //Suppression des doubles-espace
        chaine = suppressionEspace(chaine, modif);
        
        //Decoupage de la phrase en mot
        chaineDecoupe = decoupageChaine(chaine, chaineDecoupe);
            
        //Inversion des mots
        chaineDecoupe = inversionTableau(chaineDecoupe);
        
        //Mise dans la chaine
        chaineFinale = tableauDansString(chaineDecoupe, chaineFinale);
        
        //Affichage
        affichage(chaineFinale);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
