package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

public class ExerciceAlgo11 {

    public static void main(String[] args) throws IOException
    {
        //11. Trier un tableau de 10 éléments par ordre croissant.

        //Creation de tableau vide de 10 case
        int lecture [] = new int [10];
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage du tableau
        lecture = remplissageTableau(lecture, br);
        
        //Trie tableau de 10 éléments
        lecture = trieTableau(lecture);
        
        //Affichage
        afficheTableau(lecture);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
