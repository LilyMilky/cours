package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo19 {

    public static void main(String[] args) throws IOException
    {
        // 19. Afficher la présence d'une lettre dans une chaîne 
        //(si oui, en afficher le nombre(quantité) si non, afficher "absent").
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        String chaine = "";
        int nbLettreRecherche = 0;
        char lettreRecherche;
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        //Recuperation de la lettre à recherche
        System.out.println("Veuillez saisir la lettre à recherche: ");
        lettreRecherche = (char)br.read();
        
        //Comptage de la lettre dans la chaine
        nbLettreRecherche = comptageLettreChaine(chaine, lettreRecherche);
        
        affichage(chaine, nbLettreRecherche);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
        
    }
    
}
