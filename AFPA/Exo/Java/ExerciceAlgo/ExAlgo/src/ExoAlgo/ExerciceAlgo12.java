/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

/**
 *
 * @author cdi209
 */
public class ExerciceAlgo12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  throws IOException
    {
        //12. Fusionner deux tableaux déjà triés par ordre croissant.
        
        
        //Creation de tableau vide de 10 case
        int lecture [] = new int [10];
        int lecture2[] = new int[5];
        int tabFinal[] = new int [lecture.length + lecture2.length];
        
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage du tableau 1
        lecture = remplissageTableau(lecture, br);
        
        //Remplissage du tableau 2
        lecture2 = remplissageTableau(lecture2, br);
        
        //Trie du tableau 1
        lecture = trieTableau(lecture);
        
        //Trie du tableau 2
        lecture2 = trieTableau(lecture2);
        
        //Fusion des tableaux
        tabFinal = fusionTableauCroissant(lecture, lecture2, tabFinal);
        
        //Affichage
        afficheTableau(tabFinal);
        
         //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
