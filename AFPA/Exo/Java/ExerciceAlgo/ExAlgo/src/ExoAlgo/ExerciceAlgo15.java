package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo15 {

    public static void main(String[] args) throws IOException
    {
        //15. Inverser une chaîne de caractères (sans supprimer l'originale).
        
        String chaine = "";
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        //Inversion de la chaine
        StringBuffer chaineInverser = (new StringBuffer(chaine)).reverse();
        
        //Affichage
        affichage(chaineInverser);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
