package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.Tableau.*;
import static Outil.Affichage.*;

public class ExerciceAlgo10 {

    public static void main(String[] args) throws IOException
    {
        //10. Inverser les éléments d'un tableau de 10 éléments 
        //(sans passer par un tableau intermédiaire).

        //Creation de tableau vide de 10 case
        int lecture [] = new int [10];
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage du tableau
        lecture = remplissageTableau(lecture, br);
        
        //Inversion du tableau
        lecture = inversionTableau(lecture);
        
        //Affichage
        afficheTableau(lecture);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
    
}
