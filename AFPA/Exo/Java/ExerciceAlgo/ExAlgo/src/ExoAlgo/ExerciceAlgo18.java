package ExoAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static Outil.ChaineCaractere.*;
import static Outil.Affichage.*;

public class ExerciceAlgo18 {

     public static void main(String[] args)  throws IOException
    {
        //18. Supprimer toutes les occurences d'une lettre dans une chaine
        
        String chaine = "";
        String lettreASupprimer;
        
        //Ouverture d'un flux pour la lecture
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //Cache permettant de gerer la 'vitesse' des differents composants.
        BufferedReader br = new BufferedReader(isr);
        
        //Remplissage de la chaine 
        chaine = remplissageChaine(chaine, br);
        
        //Recuperation de la lettre à supprimer
        System.out.println("Veuillez saisir la lettre à remplacer: ");
        lettreASupprimer = br.readLine();
        
        //Suppression de la lettre dans la chaine
        chaine = chaine.replaceAll(lettreASupprimer, "");
        
        //Affichage
        affichage(chaine);
        
        //Fermeture du buffer(cache) puis du flux de lecture
        br.close();
        isr.close();
    }
}
