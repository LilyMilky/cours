/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

/**
 *
 * @author cdi209
 */
public class FonctionSudoku {
    
    public static boolean absentSurLigne(int k, int grille[][], int i)
    {
        for (int j = 0; j < 9; j++)
        {
            if (grille[i][j] == k)
                return false;
        }
        return true;
    }
    
    public static boolean absentSurLaColonne (int k, int grille[][], int j)

    {
        for (int i=0; i < 9; i++)
        {
            if (grille[i][j] == k)
                return false;
        }
        return true;
    }
    
    public static boolean absentSurBloc (int k, int grille[][], int i, int j)
    {
        //Calcul de l'indice de depart du bloc (ligne et colonne)
        int indiceI = i - (i % 3), indiceJ = j - (j % 3);
        for (i = indiceI; i < indiceI + 3; i++)
        {
            for (j= indiceJ; j < indiceJ + 3; j++)
            {
                if (grille[i][j] == k)
                    return false;
            }
        }
        return true;
    }
    
    
    public static boolean estValide(int grille [][], int position)
    {
        // Si on est à la 81e case (on sort du tableau)
        if (position == 9*9)
            return true;

        // On récupère les coordonnées de la case(ligne/colonne)
        int i = position/9, j = position%9;

        // Si la case n'est pas vide, on passe à la suivante (appel récursif)
        if (grille[i][j] != 0)
            return estValide(grille, position+1);
        
        //k = valeur a inserer
        for(int k = 1; k <=9; k++) 
        {
            //Si le chiffre n'est ni sur la ligne, ni sur la colonne, ni dans le bloc, on l'insert
            if(absentSurLigne(k, grille, i) && absentSurLaColonne(k, grille, j) 
                    && absentSurBloc(k, grille, i, j))
            {
                //On insert
                grille[i][j] = k;
                //On test pour la case suivante, si non valide, on continue pour la valeur precedente
                if(estValide(grille, position + 1) == true)
                    return true;
            }
        }
        grille[i][j] = 0;
        return false;
    }
    
    public static void affichage(int grille[][], boolean valide)
    {
        String Affichage = "| ";
        
        if(valide == true)
        {
            //Affichage
            System.out.println("-------------------------");
            for (int i=0; i<9; i++)
            {
                for (int j=0; j<9; j++)
                {
                    Affichage += grille[i][j];
                    Affichage += ' ';
                    if((j+1)%3 == 0)
                        Affichage += "| ";
                }
                System.out.println(Affichage);
                Affichage = "| ";
                if((i+1)%3 == 0)
                    System.out.println("-------------------------");
            }
        }
        else
            System.out.println("Grille non valide");
    }
}
