/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

import static sudoku.FonctionSudoku.*;

/**
 *
 * @author cdi209
 */
public class Sudoku {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {  
        boolean valide;
        //Creation d'une grille de sudoku
        int grille[][] =
        {
        {0,0,0,5,7,0,0,0,0},
        {0,0,8,0,0,0,0,0,4},
        {0,0,1,0,0,0,6,0,3},
        {0,6,0,3,0,0,0,4,0},
        {0,0,0,1,5,6,0,0,0},
        {0,5,0,0,0,2,0,9,0},
        {4,0,6,0,0,0,1,0,0},
        {8,0,0,0,0,0,7,0,0},
        {0,0,0,0,3,9,0,0,0}};
        
        
        //Appel de la fonction de remplissage avec backtracking
        valide = estValide(grille, 0);
        
        affichage(grille, valide);
        
    }   
}


