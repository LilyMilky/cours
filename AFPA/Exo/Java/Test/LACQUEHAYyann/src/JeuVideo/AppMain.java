package JeuVideo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AppMain {
    public static void main(String[] args) throws IOException {
        
        PNJ PNJ1 = new PNJ();
        PNJ PNJ2 = new PNJ();
        Healer healer = new Healer();
        
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        
        //Gestion dynamique du PNJ1
        System.out.println("Quel est le nom du premier Guerrier?");
        PNJ.instanciationPNJ(PNJ1, br);
        
        //Gestion dynamique du PNJ2
        System.out.println("Quel est le nom du deuxieme Guerrier?");
        PNJ.instanciationPNJ(PNJ2, br);
        
        //Gestion dynamique du healer/joueur
        Healer.instanciationHealer(healer, br);
        
        //Presentation des PNJ/Joueur
        Affichage.affichage(PNJ1, PNJ2, healer);
        
        //On combat jusqu'a la mort de l'un des guerriers
        do
        {
            PNJ1.taper(PNJ2);
            //Si le 2eme pnj est mort, il ne tape pas, et on ne gere pas le heal.
            if(PNJ2.getNbPV() > 0)
            {
               PNJ2.taper(PNJ1);
               
               //Gestion du heal
               Healer.choixHeal(PNJ1, PNJ2, br, healer);
            }
   
        }while(PNJ1.getNbPV() > 0 && PNJ2.getNbPV() > 0);
        
        //Affichage
        Affichage.affichage(PNJ1, PNJ2);
        
        br.close();
        isr.close();
    }
}
