package JeuVideo;

import java.io.BufferedReader;
import java.io.IOException;

public class PNJ {
    private int nbPV;
    private int nbDMG;
    private String name;
    private int maxHP;

    // <editor-fold defaultstate="collapsed" desc=" CONSTRUCTOR ">
    public PNJ(int nbPV, int nbDMG, String name) {
        this.nbPV = nbPV;
        this.nbDMG = nbDMG;
        this.name = name;
        maxHP = nbPV;
    }
    
    public PNJ() {
    }

// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" GETTER/SETTER ">
    public int getNbPV() {
        return nbPV;
    }
    
    public void setNbPV(int nbPV) {
        this.nbPV = nbPV;
    }
    
    public int getNbDMG() {
        return nbDMG;
    }
    
    public void setNbDMG(int nbDMG) {
        this.nbDMG = nbDMG;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }    
    
    public int getMaxHP() {
        return maxHP;
    }
    
    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

// </editor-fold>
    
    //Enleve des points de vue au PNJ
    public void taper(PNJ PNJ2)
    {
        PNJ2.setNbPV(PNJ2.getNbPV() - nbDMG);
        
        if(PNJ2.getNbPV() > 0)
            System.out.println(PNJ2.getName() + " a subit " + nbDMG + " dmg. Il lui reste " + PNJ2.getNbPV() + " HP");
        else
            System.out.println(PNJ2.getName() + " a subit " + nbDMG + " dmg. Il est mort!");
    }
    
    //Affichage
    @Override
    public String toString()
    {
        return "PNJ " + name + " qui possede " + nbPV + " HP. Il inflige " + nbDMG + " par attaque";
    }
    
    //Creation d'un PNJ
    public static void instanciationPNJ(PNJ pnj, BufferedReader br) throws IOException
    {
        pnj.setName(br.readLine());
        System.out.println("Quel est son nombre de HP?");
        pnj.setMaxHP(Integer.parseInt(br.readLine()));
        pnj.setNbPV(pnj.getMaxHP());
        System.out.println("Quel est son nombre de degats?");
        pnj.setNbDMG(Integer.parseInt(br.readLine()));
    }
}
