package JeuVideo;

import java.io.BufferedReader;
import java.io.IOException;

public class Healer {

    private String name;
    private int heal;

    // <editor-fold defaultstate="collapsed" desc=" CONSTRUCTOR ">
    public Healer() {
    }
    
    public Healer(String name, int heal) {
        this.name = name;
        this.heal = heal;
    }

// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" GETTER/SETTER ">
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getHeal() {
        return heal;
    }
    
    public void setHeal(int heal) {
        this.heal = heal;
    }

// </editor-fold>

    //Gestion du Heal sur les PNJ
    public void Heal(PNJ pnj) {
        if (pnj.getNbPV() + heal < pnj.getMaxHP()) {
            pnj.setNbPV(pnj.getNbPV() + heal);
            System.out.println("Le healer heal " + pnj.getName() + " de " + heal + "HP. Il a desormais " + pnj.getNbPV() + " HP.");
        } else {
            pnj.setNbPV(pnj.getMaxHP());
            System.out.println("Le healer heal " + pnj.getName() + ", il est full life.");
        }
    }

    //Affichage
    @Override
    public String toString() {
        return "Healer " + name + " qui heal de " + heal + " HP.";
    }

    //Le joueur choici qui heal puis Heal d'un des PNJ
    public static void choixHeal(PNJ PNJ1, PNJ PNJ2, BufferedReader br, Healer healer) throws IOException {
        
        int choix;
        
        //Choix du PNJ
        do {
            System.out.println("Qui le healer va t'il heal? ");
            System.out.println("1) " + PNJ1.getName() + "?");
            System.out.println("2) " + PNJ2.getName() + "?");
            choix = Integer.parseInt(br.readLine());
        } while (choix != 1 && choix != 2);

        if (choix == 1) {
            healer.Heal(PNJ1);
        } else {
            healer.Heal(PNJ2);
        }
    }
    
    //Creation du Healer
    public static void instanciationHealer(Healer healer, BufferedReader br) throws IOException
    {
        System.out.println("Quel est votre nom?");
        healer.setName(br.readLine());
        System.out.println("Votre puissance de heal?");
        healer.setHeal(Integer.parseInt(br.readLine()));
    }
}
