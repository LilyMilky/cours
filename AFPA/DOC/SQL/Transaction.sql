use zoo

--TRANSACTION: Tout ou rien

Begin Transaction creerSalaire

Declare @monErreur int
set @monErreur = @@ERROR


insert into salaire(numeroAVSEmploye,datesalaire,montantSalaire)
values('1123456739100','2015-01-17','1500'),
('1123456739100','2015-02-17','1500'),
('1123456739100','2015-03-17','1500'),
('1123456739100','2015-04-17','1500'),
('1123456739100','2015-05-17','1500'),
('1123456739100','2015-06-17','1500')

print @monErreur

commit transaction creerSalaire

--select * from employe
--delete from salaire