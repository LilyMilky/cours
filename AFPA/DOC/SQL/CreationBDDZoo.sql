--Cr�ation de la base de donn�es

CREATE DATABASE zoo

-- Indiquer la base de donn�es � utiliser

USE zoo

-- Cr�ation de la table employ�

CREATE TABLE employe (numeroAVSEmploye varchar(13) PRIMARY KEY,civiliteEmploye varchar(1) not null,nomEmploye varchar(50) not null,nomMaritalEmploye varchar(50),prenomEmploye varchar(50) not null,dateNaissanceEmploye date not null,lieuNaissanceEmploye varchar(50)Not NUll,codeMemoEmploye varchar(3) Not NUll)

--Detruire table employe

DROP TABLE employe

-- Supr cl� etrangere

ALTER TABLE employe DROP CONSTRAINT [PK__employe__B05E9DF8CC9B2CAA]

--Rajout cle primaire

Alter table employe
add constraint pkemploye primary key(numeroavsEmploye)


--Creation table Salaire

CREATE Table salaire(idSalaire BIGINT IDENTITY(1,1) NOT NULL, numeroAVSEmploye varchar(13) NOT NULL, datesalaire date NOT NULL, montantSalaire int NOT NULL,

Constraint pkSalaire primary key(idSalaire),

Constraint fkEmployeSalaire FOREIGN KEY(numeroavsEmploye) REFERENCES employe(numeroAVSEmploye)

)

--------------------------------------------------

Create table services(idService BIGINT IDENTITY(1,1) NOT NULL,
nomService varchar(50),

Constraint pkIDService primary key(idService)

)

--------------------------------------------------
CREate table poste(idPoste BIGINT IDENTITY(1,1) NOT NULL,
numeroAVSEmploye varchar(13) NOT NULL,
idService BIGint NOT NULL,
nomPoste varchar(50) NOT NULL,
gradePoste varchar(5) NOT NULL,
tauxOccupationPoste int NOT NULL,
entreePoste date,
sortiePoste date,

Constraint pkidPoste primary key(idPoste),
constraint fknumeroAVSEmploye FOREIGN Key(numeroAVSEmploye) REFERENCES employe(numeroAVSEmploye),
constraint fkIDServicePOSTE FOREIGN KEY(idService) REFERENCES Services(idService)
)

--------------------------------------------


Create table Secteur(idSecteur BIGINT IDENTITY(1,1) Not null,
fonctionSecteur varchar(50) NOT null,

constraint pkidsecteur primary key(idSecteur)
)

-----------------------------------------------

create table concerner(numeroAVSEmploye varchar(13) not null,
idSecteur BIGINT not null,
code char,

constraint pknumeroAVSEmployeConcerner primary KEY(numeroAVSEmploye, idSecteur),
constraint fkAVSEmploye foreign key(numeroAVSEmploye) references employe(numeroAVSEmploye),
constraint fkidSecteurConcerner foreign key(idSecteur) references secteur(idSecteur)
)


--------------------------------------------------------------

create table parcelle(idParcelle BIGINT Identity(1,1) not null,
idSecteur BIGint not null,
numeroParcelle int,

constraint pkidParcelle primary key(idParcelle),
constraint fkidSecteurParcelle foreign key(idSecteur) references Secteur(idSecteur)
)


--------------------------------------------------------

create table surveillance(idSurveillance BIGINT identity(1,1) not null,
numeroAVSEmploye varchar(13) not null,
idParcelle bigint not null,
jourHeureSurveillance datetime,

constraint pkidSurveillance primary key(idSurveillance),
constraint fknumeroAVSEmplyeSurveillance foreign key(numeroAVSEmploye) references employe(numeroAVSEmploye),
constraint fkidParcelleSurveillance foreign key(idParcelle) references parcelle(idParcelle)
)

drop table surveillance
---------------------------------------------------------

create table espece(idEspece bigint identity(1,1) not null,
idParcelle bigint not null,
nomEspece varchar(50) not null,
categorieEspece varchar(50) not null,

constraint pkidEspece primary key(idEspece),
constraint fkidParcelleEspece foreign key(idParcelle) references parcelle(idParcelle)
)

---------------------------------------------------------

create table pensionnaire(idPensionnaire BIGINT identity(1,1) not null,
idEspece bigint not null,
genrePensionnaire char not null,
nomPensionnaire varchar(50) not null,
dateNaissancePensionnaire date,
dateDecesPensionnaire date,
groupeSanguinPensionnaire varchar(10) not null,
captivitePensionnaire  bit,

constraint pkidPensionnaire primary key(idPensionnaire),
constraint fkidEspecePensionnaire foreign key(idEspece) references espece(idEspece)
)

----------------------------------------------------------

create table parente(idGeniteur bigint not null,
idEnfant bigint not null,

constraint pkparente primary key(idGeniteur, idEnfant),
constraint fkGeniteur foreign key(idGeniteur) references pensionnaire(idPensionnaire),
constraint fkEnfant foreign key(idEnfant) references pensionnaire(idPensionnaire)
)

---------------------------------------------------------

create table recenser(idRecensement bigint identity(1,1) not null,
numeroAVSEmploye varchar(13) not null,
idEspece bigint not null,
dateRecensement date not null,
nombreRecensement int not null,

constraint pkidRecensement primary key(idRecensement),
constraint fknumeroAVSEmployeRecenser foreign key(numeroAVSEmploye) references employe(numeroAVSEmploye),
constraint fkidEspeceRecenser foreign key(idEspece) references espece(idEspece)
)

-----------------------------------------------------------

create table suivi(idSuivi bigint identity(1,1) not null,
numeroAVSEmploye varchar(13) not null,
idPensionnaire bigint not null,
dateSuivi date not null,
poidsSuivi float,
tailleSuivi float,

constraint pkidSuivi primary key(idSuivi),
constraint fknumeroAVSEmplyeSuivi foreign key(numeroAVSEmploye) references employe(numeroAVSEmploye),
constraint fkidPensionnaireSuivi foreign key(idPensionnaire) references pensionnaire(idPensionnaire)
)
