--Cr�ation de la base de donn�es

CREATE DATABASE ecoleMusique

-- Indiquer la base de donn�es � utiliser

USE ecoleMusique

-------------------------------------------
create table instrument(nomInstrument varchar(50) not null,

constraint pknomInstrument primary key(nomInstrument)
)

-------------------------------------------

create table session(codeSession varchar(20) not null,
nomInstrument varchar(50) not null,
dateDebutSession date,
dateFinSession date,
niveauSession varchar(5) not null,

constraint pkCodeSession primary key(codeSession),
constraint fknomInstrument foreign key(nomInstrument) references instrument(nomInstrument)
)

------------------------------------------------

create table preference(idPreference bigint IDENTITY(1,1) not null,
jourPreference varchar(10) not null,
creneauPreference varchar(20) not null,
typeCours varchar(20) not null,

constraint pkidPreference primary key(idPreference)
)

--------------------------------------------------------

create table eleve(idEleve bigint IDENTITY(1,1) not null,
nomEleve varchar(50) not null,
prenomEleve varchar(50) not null,
dateNaissanceEleve date not null,

constraint pkidEleve primary key(idEleve)
)

--------------------------------------------

create table inscription(idInscription bigint IDENTITY(1,1) not null,
idEleve bigint not null,
codeSession varchar(20) not null,
dateInscription date not null,
solfege int not null,

constraint pkidInscription primary key(idInscription),
constraint fkidEleve foreign key(idEleve) references eleve(idEleve)
)

------------------------------------------------

create table inscriptionPreference(idPreference bigint not null,
idInscription bigint not null,

constraint pkinscriptionPreference primary key (idPreference, idInscription),
constraint fkidPreference foreign key(idPreference) references preference(idPreference),
constraint fkidInscription foreign key(idInscription) references inscription(idInscription)
)

-----------------------------------------------

create table cours(idCours bigint IDENTITY(1,1) not null,
nomSalle varchar(50) not null,
dateHeureCours date not null,
typeCours varchar(20) not null,

constraint pkidCours primary key(idCours)
)

--------------------------------------------

create table presenceEleve(idInscription bigint not null,
idCours bigint not null,
present bit not null,
dateNotification date,
motifAbsence varchar(255),
MotifValide bit,

constraint pkpresenceEleve primary key(idInscription, idCours),
constraint fkidInscriptionEleve foreign key(idInscription) references inscription(idInscription),
constraint fkidCours foreign key(idCours) references cours(idCours)
)

----------------------------------------------

create table salle(nomSalle varchar(50) not null,
capacite int not null,

constraint pknomSalle primary key(nomSalle)
)

----------------------------------------------

create table professeur(idProfesseur bigint IDENTITY(1,1) not null,
nomProfesseur varchar(50) not null,
prenomProfesseur varchar(50) not null,
numSiren varchar(20) not null,

constraint pkidProfesseur primary key(idProfesseur)
)

-------------------------------------------------

create table presenceProfesseur(idCours bigint not null,
idProfesseur bigint not null,
present bit not null,
dateNotification date,
motifAbsence varchar(255),
motifValide bit,

constraint pkpresenceProfesseur primary key(idCours, idProfesseur),
constraint fkidCourspresenceProf foreign key(idCours) references cours(idCours),
constraint fkidProfesseur foreign key(idProfesseur) references professeur(idProfesseur)
)

-----------------------------------------------

create table maitrise(idProfesseur bigint not null,
nomInstrument varchar(50) not null,

constraint pkmaitrise primary key(idProfesseur,nomInstrument),
constraint fkidProfesseurmaitrise foreign key(idProfesseur) references professeur(idProfesseur),
constraint fknomInstrumentMaitrise foreign key(nomInstrument) references instrument(nomInstrument)
)

-------------------------------------------

create table paiement(idPaiement bigint IDENTITY(1,1) not null,
idInscription bigint not null,
montantPaiement int not null,
datePaiement date not null,
modePaiement varchar(20) not null,
nombrePaiement int not null,

constraint pkidPaiement primary key(idPaiement),
constraint fkidInscriptionPaiement foreign key(idInscription) references inscription(idInscription) 
)