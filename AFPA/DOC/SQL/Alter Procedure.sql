USE [zoo]
GO
/****** Object:  StoredProcedure [dbo].[creationEdtGardien]    Script Date: 16/12/2015 11:08:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Cette procedure permet d'affecter un secteur a surveiller a un gardien en particulier pour une date donnée 
--en respectant les horaires de debut et de fin de service


--On donne un nom parlant a notre procedure
ALTER procedure [dbo].[creationEdtGardien]
--On liste les parametres reçu en entrée et renvoyé
@VARidSecteur bigint,
@VARavsEmploye varchar(13),
@VARheureDebut datetime,
@VARheureFin datetime,
@VARheurePause datetime,
@VARheureEffectue smallInt output

AS 

--Declaration d'une variable pour recuperer la parcelle en cours.
Declare @VarParcelleEnCours bigint

--Declaration d'un cursor(tableau) pour recuperer les idParcelles d'un secteur (pas @)
Declare curIdParcelles CURSOR scroll
for
--Recuperation de toutes les parcelles liés a un secteur
Select idParcelle from parcelle where idSecteur = @VARidSecteur order by numeroParcelle
--Les idParcelles sont maintenant dans le cursor

-- Ouverture du curdor
open curIdParcelles
--On recupere la valeur du 1er indice
Fetch curIdParcelles
--On affecte la valeur en cours a une variable
Into @VARparcelleEnCours


--Boucle
WHILE @VARheureDebut < @VARheureFin

Begin

--On test si l'on est a la fin du tableau.
if @@FETCH_STATUS = -1
	begin 

		fetch first from curIdParcelles Into @VARparcelleEnCours

	end

--Test Heure de pause
if @VARheureDebut = @VarHeurePause
	begin
		
		set @VARheureDebut = dateadd(hour,1,@VARheureDebut)
 	end

select idParcelle,jourHeureSurveillance from surveillance
where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut

while @@RowCount = 1 and @@FETCH_STATUS <> -1
	Begin
	
		--print @VARparcelleEnCours
		Fetch next from curIdParcelles into @VARparcelleEnCours

		select idParcelle,jourHeureSurveillance from surveillance
		where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut

		--print @@RowCount

	End

	select idParcelle,jourHeureSurveillance from surveillance
		where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut
if @@ROWCOUNT = 0
Begin

	--Print('Parcelle + heure non utilisé')

	select numeroAVSEmploye,jourHeureSurveillance from surveillance
	where numeroAVSEmploye=@VARavsEmploye and jourHeureSurveillance = @VARheureDebut

	if @@ROWCount = 0
	begin

	--Insert un enregistrement dans la table
	--Print('Insertion')
	Insert into surveillance(numeroAVSEmploye, idParcelle, jourHeureSurveillance)
	values(@VARavsEmploye,@VarParcelleEnCours,@VARheureDebut)

	End
end
	
	--Incremente heure debut
	set @VARheureDebut = dateadd(hour,1,@VARheureDebut)

	--On recupere la valeur de l'indice suivant dans le cursor
	Fetch next from curIdParcelles into @VARparcelleEnCours
End

Select @VARheureEffectue = Count(idSurveillance) from surveillance 
where numeroAVSEmploye = @VARavsEmploye and left(jourHeureSurveillance,10) = left(@VARheureDebut,10)

--Fermer curseur + suppr memoire
Close curIdParcelles
Deallocate curIdParcelles
