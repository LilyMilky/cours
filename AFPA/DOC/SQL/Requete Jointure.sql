use zoo

Insert INTO services(nomService)
Values ('Administratif'),('Veterinaire'),('Surveillance'),('Comptable')

--Liste employe
/*1123451789100	F	JOLY		Mary	1981-05-01	ZURICH	
1123453789100	N	MRMME		Neutre	1955-03-22	ZURICH	
1123456739100	M	LURON		Joyeux	1959-03-22	ZURICH	
1123456789100	F	IRISEE		Marthe	1955-03-22	ZURICH	
1124456789100	F	WOLF		Ering	1975-03-22	Lille	
2123422789100	M	LIL		    Milky	1990-10-29	Nancy	
2123456789100	M	PEUPLU		Jean	1985-03-22	ZURICH	*/

--Liste Service
/*1	Administratif
2	Veterinaire
3	Surveillance
4	Comptable*/

update services
set nomService = 'Medical' where nomService= 'Veterinaire'

alter table poste
add tauxOccupationPoste smallint default '100' not null

Insert into poste(numeroAVSEmploye,idService,nomPoste,gradePoste,entreePoste,tauxOccupationPoste)
VALUES/*('2123422789100',1,'Directeur','I','2015-12-14',100),*/('1124456789100',3,'Gardien','IIB','2010-10-20',''),
('1123456789100',2,'Veterinaire','III','2000-02-03',''),('1123451789100',1,'Secretaire','II','2012-04-21',50),
('1123453789100',3,'Gardien','IIB','2010-10-10',''), ('1123456739100',3,'Gardien','I','2008-08-08','')

-- Afficher nom prenom + moyenne de leur salaire + Poste et service (Jointure).

Select nomEmploye,prenomEmploye, AVG(montantSalaire) as 'Moyenne Salaire', nomPoste AS 'Poste', nomService as 'Departement'

from services join poste on services.idService = poste.idService

join employe on employe.numeroAVSEmploye = poste.numeroAVSEmploye

join salaire on salaire.numeroAVSEmploye = poste.numeroAVSEmploye

where employe.numeroAVSEmploye = salaire.numeroAVSEmploye
group by nomEmploye, prenomEmploye, nomPoste,nomService
