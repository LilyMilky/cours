use zoo

-- Construire l'algorithme en transact sql permettant d'affecter un secteur a surveiller a un gardien en particulier
-- A une date donnee en respectant les horaires de debut et fin de service.

--Declaration des variables (@) envoy�es par l'interface graphique de la secretaire.
Declare @VARidSecteur bigint,
@VARavsEmploye varchar(13),
@VARheureDebut datetime,
@VARheureFin datetime,
@VARheurePause datetime,
--Declaration d'une variable pour recuperer la parcelle en cours.
@VarParcelleEnCours bigint

--Valeur de test
set @VARidSecteur=2
set @VARavsEmploye='1123451789100'
set @VARheureDebut='15-12-2015 08:00:00'
set @VARheureFin='15-12-2015 18:00:00'
set @VARheurePause ='15-12-2015 12:00:00'


--Declaration d'un cursor(tableau) pour recuperer les idParcelles d'un secteur (pas @)
Declare curIdParcelles CURSOR scroll
for
--Recuperation de toutes les parcelles li�s a un secteur
Select idParcelle from parcelle where idSecteur = @VARidSecteur order by numeroParcelle
--Les idParcelles sont maintenant dans le cursor

-- Ouverture du curdor
open curIdParcelles
--On recupere la valeur du 1er indice
Fetch curIdParcelles
--On affecte la valeur en cours a une variable
Into @VARparcelleEnCours


--Boucle
WHILE @VARheureDebut < @VARheureFin

Begin

--On test si l'on est a la fin du tableau.
if @@FETCH_STATUS = -1
	begin 

		fetch first from curIdParcelles Into @VARparcelleEnCours

	end

--Test Heure de pause
if @VARheureDebut = @VarHeurePause
	begin
		
		set @VARheureDebut = dateadd(hour,1,@VARheureDebut)

	end

select idParcelle,jourHeureSurveillance from surveillance
where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut

while @@RowCount = 1 and @@FETCH_STATUS <> -1
	Begin
		--Afficher un message Print(vchar)
		--print @VARparcelleEnCours
		Fetch next from curIdParcelles into @VARparcelleEnCours

		select idParcelle,jourHeureSurveillance from surveillance
		where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut

		--print @@RowCount

	End

	select idParcelle,jourHeureSurveillance from surveillance
		where idParcelle=@VarParcelleEnCours and jourHeureSurveillance = @VARheureDebut
if @@ROWCOUNT = 0
	Begin

	--Print('Parcelle + heure non utilis�')

	select numeroAVSEmploye,jourHeureSurveillance from surveillance
	where numeroAVSEmploye=@VARavsEmploye and jourHeureSurveillance = @VARheureDebut

	if @@ROWCount = 0
	begin

	--Insert un enregistrement dans la table
	--Print('Insertion')
	Insert into surveillance(numeroAVSEmploye, idParcelle, jourHeureSurveillance)
	values(@VARavsEmploye,@VarParcelleEnCours,@VARheureDebut)

	End
end
	
	--Incremente heure debut
	set @VARheureDebut = dateadd(hour,1,@VARheureDebut)

	--On recupere la valeur de l'indice suivant dans le cursor
	Fetch next from curIdParcelles into @VARparcelleEnCours
End

--Fermer curseur + suppr memoire
Close curIdParcelles
Deallocate curIdParcelles


--Affichage Surveillance

Select nomEmploye as 'Nom',prenomEmploye as 'Prenom',numeroParcelle as 'Numero de Parcelle',
Format(jourHeureSurveillance,'dddd dd MMMM yyyy') as 'Jour',
Format(jourHeureSurveillance, 'hh:mm') as 'Heure' 
from surveillance as s,employe as e,parcelle as p
where s.numeroAVSEmploye = e.numeroAVSEmploye and s.idParcelle = p.idParcelle
order by numeroParcelle, Heure

select * from surveillance
delete from surveillance 
