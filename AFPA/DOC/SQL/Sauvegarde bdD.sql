--Creation d'une table contenant les saves

use zoo

create table sauvegarde (idSauvegarde bigint identity(1,1) not null, dateSauvegarde datetime not null, nomSauvegarde varchar(50)
constraint pkidSauvegarde primary key (idSauvegarde))


--Sauvegarde
Declare @nomFichier varchar(50),
@dateSauvegarde datetime

set @dateSauvegarde = getdate()

set @nomFichier = 'D:\Cours\SQL\SaveBdD\SAVzoo' + cast(format(@dateSauvegarde, 'dd-MM-yyyy') as varchar(30)) + '.bak'

backup database zoo
to disk = @nomFichier
with format



--Insert save dans la base

insert into sauvegarde(dateSauvegarde,nomSauvegarde)
Values(@dateSauvegarde, @nomFichier)


select * from sauvegarde




--Restore BDD
declare @nomFichierSave varchar(150)

select @nomFichierSave = nomSauvegarde from sauvegarde where idSauvegarde = IDENT_CURRENT('sauvegarde')

print @nomFichierSave


restore database zoo
from disk = 'D:\Cours\SQL\SaveBdD\SAVzoo18-12-2015.bak'
with replace