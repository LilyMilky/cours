Use zoo

--Afficher contenu table employe

Select * from employe

--Afficher nom prenom employe

Select nomEmploye,prenomEmploye from employe

-- Alias

Select nomEmploye as 'NOM',prenomEmploye as 'PRENOM' from employe

-- Insert donnees

Insert into employe (numeroAVSEmploye, civiliteEmploye, nomEmploye, nomMaritalEmploye, prenomEmploye, dateNaissanceEmploye, lieuNaissanceEmploye, codeMemoEmploye)
values ('1123456789100','F','Irisee','','MarThe','1955-03-22','ZURICH','mAi'),
('2123456789100','M','peuPLU','','Jean','1985-03-22','ZURICH',''),
('2123422789100','M','Lil','','Milky','1990-10-29','Nancy',''),
('1123451789100','F','Joly','','Mary','1981-05-01','ZURICH',''),
('1124456789100','F','Wolf','','ering','1975-03-22','Lille',''),
('1123453789100','N','MrMMe','','Neutre','1955-03-22','ZURICH',''),
('1123456739100','M','Luron','','Joyeux','1959-03-22','ZURICH','')

Insert into salaire (numeroAVSEmploye,datesalaire,montantSalaire)
values ('2123422789100','2015-12-11','100000'),
('2123422789100','2015-11-11','10000'),
('2123422789100','2015-10-11','2000'),
('2123422789100','2015-09-11','80000'),
('2123422789100','2015-08-11','150000'),
('2123422789100','2015-07-11','50000'),
('2123422789100','2015-06-11','155000'),
('2123422789100','2015-05-11','250000'),
('2123422789100','2015-04-11','200000'),
('2123422789100','2015-03-11','140000'),
('2123422789100','2015-02-11','130000'),
('2123422789100','2015-01-11','110000'),

('1123451789100','2015-12-11','100000'),
('1123451789100','2015-11-11','10000'),
('1123451789100','2015-10-11','2000'),
('1123451789100','2015-09-11','80000'),
('1123451789100','2015-08-11','150000'),
('1123451789100','2015-07-11','50000'),
('1123451789100','2015-06-11','155000'),
('1123451789100','2015-05-11','250000'),
('1123451789100','2015-04-11','200000'),
('1123451789100','2015-03-11','140000'),
('1123451789100','2015-02-11','130000'),
('1123451789100','2015-01-11','110000'),

('1124456789100','2015-12-11','1000'),
('1124456789100','2015-11-11','1000'),
('1124456789100','2015-10-11','200'),
('1124456789100','2015-09-11','8000'),
('1124456789100','2015-08-11','1500'),
('1124456789100','2015-07-11','5000'),
('1124456789100','2015-06-11','1550'),
('1124456789100','2015-05-11','2500'),
('1124456789100','2015-04-11','2000'),
('1124456789100','2015-03-11','1400'),
('1124456789100','2015-02-11','1300'),
('1124456789100','2015-01-11','1100')

Select * from salaire

delete from salaire

-- Ajout contrainte 1 salaire par mois

Alter table salaire
add constraint avsDateSalaireUnique
Unique(numeroAVSEmploye,dateSalaire)

-- Ajout contrainte enum

Alter Table employe
add constraint enumCivilite
Check(civiliteEmploye ='F' or civiliteEmploye = 'M' or civiliteEmploye = 'N')

-- Vider table employe

Delete from employe

-- Suppr occurence en particulier

Delete from employe where numeroAVSEmploye = '1123456789101'

