--APPEL DE PROCEDURE STOCKEE
--delete from surveillance
--DECLARATION DES VARIABLES ENVOYEES A LA PROCEDURE
DECLARE 
--SECTEUR A SURVEILLER
@VarAppIdSecteur BIGINT,
--EMPLOYE AFFECTE AU SECTEUR
@VarAppAvsEmploye VARCHAR(13),
--HEURE PRISE DE POSTE DE L EMPLOYE
@AppHeureDebut DATETIME,
--HEURE FIN DE POSTE DE L'EMPLOYE
@AppHeureFin DATETIME,
--HEURE DE PAUSE DE L EMPLOYE
--@AppVarPause DATETIME,
--NB HEURES EFFECTUEES PAR EMPLOYE
@nbHeuresEffectuees SMALLINT,
--NB HEURE OUVERTURE JOURNEE
@nbHeuresOuvertureJournee SMALLINT,
@nbHeuresNecessaire INT,
@SommeHeuresEffectuees INT
SET @SommeHeuresEffectuees=0

SET @VarAppIdSecteur=1
SET @AppHeureDebut	='20150714 08:00:00'
SET @AppHeureFin ='20150714 18:00:00'
--SET @AppVarPause ='20150714 12:00:00'
SET @nbHeuresEffectuees = 0

SET @nbHeuresOuvertureJournee = 10

--ON COMPTE LE NOMBRE D INSERTIONS NECESSAIRES 
--POUR COUVRIR TOUTES LES HEURES POUR TOUTES LES PARCELLES
SELECT @nbHeuresNecessaire=COUNT(idParcelle)*@nbHeuresOuvertureJournee
FROM Parcelle
WHERE idSecteur=@VarAppIdSecteur

--DECLARATION D UNE VARIABLE DE TYPE CURSOR
--POUR RECUPERER LES AVS DES EMPLOYES
DECLARE cursAvsEmploye CURSOR 
FOR
--RECUPERATION DE TOUS LES AVS EMPLOYES
SELECT avsEmploye FROM employe
--LES avsEmploye sont maintenant dans la variable cursAvsEmploye


		--OUVERTURE DU CURSOR cursAvsEmploye
		OPEN cursAvsEmploye
		--ON RECUPERE LA VALEUR DU PREMIER INDICE
		FETCH cursAvsEmploye
		--ON AFFECTE LA VALEUR EN COURS A UNE VARIABLE
		INTO @VarAppAvsEmploye


		WHILE @@FETCH_STATUS = 0 AND @nbHeuresNecessaire>@SommeHeuresEffectuees

			BEGIN
			EXEC creationEdtGardien @VarAppIdSecteur,@VarAppAvsEmploye ,@AppHeureDebut,
									@AppHeureFin , @nbHeuresNecessaire, @nbHeuresEffectuees OUTPUT

			--select * from surveillance
			PRINT('cet employe va effectuer '+CAST(@nbHeuresEffectuees AS VARCHAR(2)) 
					+' HEURES DANS LA JOURNEE')

			SET @SommeHeuresEffectuees=@SommeHeuresEffectuees+@nbHeuresEffectuees
			--ON RECUPERE LA VALEUR DE L INDICE SUIVANT DANS LE CURSOR
			FETCH NEXT FROM cursAvsEmploye INTO @VarAppAvsEmploye

			END 
	
PRINT ('SOMME HEURES EFFECTUEES : '+CAST(@SommeHeuresEffectuees AS VARCHAR(4)))
PRINT ('SOMME HEURES NECESSAIRES : '+CAST(@nbHeuresNecessaire AS VARCHAR(4)))


CLOSE cursAvsEmploye
DEALLOCATE cursAvsEmploye

--select * from surveillance
--ORDER BY  idParcelle , jourHeureSurveillance



