use zoo

--Declare Variable pour la procedure
Declare @VARidSecteur bigint,
@VARavsEmploye varchar(13),
@VARheureDebut datetime,
@VARheureFin datetime,
@VARheurePause datetime,
@VARnbHeureEffectue smallint

--Recup tous les avsEMploye
Declare cursavsEmploye cursor
for
select e.numeroAVSEmploye from employe as e,poste as p where e.numeroAVSEmploye = p.numeroAVSEmploye and p.nomPoste = 'Gardien'

open cursavsEmploye
fetch cursavsEmploye
into @VARavsEmploye

--Valeur de test
set @VARidSecteur=2
set @VARheureDebut='15-12-2015 08:00:00'
set @VARheureFin='15-12-2015 18:00:00'
set @VARheurePause ='15-12-2015 11:00:00'
set @VARnbHeureEffectue = 0


while @@FETCH_STATUS = 0

Begin

--Appel procedure stock�e

EXEC dbo.creationEdtGardien @VARidSecteur,@VARavsEmploye,@VARheureDebut,@VARheureFin,@VARheurePause, @VARnbHeureEffectue output

print ('Cet employe a effectu� : ' + cast(@VARnbHeureEffectue as varchar(2)) + ' Heures')

set @VARheurePause = dateadd(hour,1,@VARheurePause)

if @VARheurePause > '13:00'
begin

	set @VARheurePause = dateadd(hour,-3,@VARheurePause)

end

Fetch next from cursavsEmploye into @VARavsEmploye

end

Close cursavsEmploye
Deallocate cursavsEmploye


--Affichage Surveillance

Select nomEmploye as 'Nom',prenomEmploye as 'Prenom',numeroParcelle as 'Numero de Parcelle',
Format(jourHeureSurveillance,'dddd dd MMMM yyyy') as 'Jour',
Format(jourHeureSurveillance, 'hh:mm') as 'Heure' 
from surveillance as s,employe as e,parcelle as p
where s.numeroAVSEmploye = e.numeroAVSEmploye and s.idParcelle = p.idParcelle
order by numeroParcelle, Heure

delete from surveillance 
