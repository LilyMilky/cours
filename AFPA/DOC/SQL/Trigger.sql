--use zoo

--Trigger est lie � la table employe. Il permet de generer automatiquement un code memo en concatenant
--la premiere lettre du nom avec les 2 premiere lettre du prenom l'employe et de l'inserer dans le champ code memo
-- + regle casse nom prenom.

create trigger ForInsertUpdateEmploye 
--Table pour trigger
on employe
-- Quel evenement
for insert , update
--Debut script
as
-- Script creation code memo
update employe set 
--Casse
nomEmploye=Upper(em.nomEmploye),
prenomEmploye= Concat(UPPER(SUBSTRING(em.prenomEmploye,1,1)) ,
Lower(Substring(em.prenomEmploye,2,LEN(em.prenomEmploye)))),
--Code memo
codeMemoEmploye =
upper(concat ( left(em.nomEmploye,1), (left(em.prenomEmploye,2))))

--Jointure table enploye + Inserted
from employe as em
join inserted as ins
on em.numeroAVSEmploye = ins.numeroAVSEmploye


--Test trigger

select * from employe

update employe set nomEmploye = 'Salut' where nomEmploye = 'MRMME'

Insert into employe(numeroAVSEmploye,civiliteEmploye,nomEmploye,nomMaritalEmploye,prenomEmploye,dateNaissanceEmploye,lieuNaissanceEmploye,codeMemoEmploye)
values ('1211111112111','F','laO','','laOg','1990-11-20','Toulouse','xx')

--Suppresion trigger

drop trigger ForInsertUpdateEmploye