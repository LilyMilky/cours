use zoo

-- Afficher nom prenom + 12salaires des employes

Select nomEmploye,prenomEmploye,dateSalaire, montantSalaire from employe,salaire where employe.numeroAVSEmploye = salaire.numeroAVSEmploye

-- Afficher nom prenom + moyenne de leur salaire

Select nomEmploye,prenomEmploye, AVG(montantSalaire) as 'Moyenne Salaire' from employe,salaire where employe.numeroAVSEmploye = salaire.numeroAVSEmploye
group by nomEmploye, prenomEmploye

-- Augmenter tous les salaires de 50�

Update salaire set montantSalaire = montantSalaire + 50 from employe, salaire where employe.civiliteEmploye = 'F' and employe.numeroAVSEmploye = salaire.numeroAVSEmploye

--Afficher nom prenom + moyenne de leur salaire + Ecart de leur salaire par rapport a la moyenne
Select nomEmploye,prenomEmploye, AVG(montantSalaire) as 'Moyenne Salaire',
Round(avg(montantSalaire) - (select avg(montantSalaire) from salaire), 2)as 'Ecart Salaire moyenne generale' --Round : Arrondir
from employe,salaire where employe.numeroAVSEmploye = salaire.numeroAVSEmploye and datesalaire like '2015%'
group by nomEmploye, prenomEmploye

--Corriger la casse nom prenom Employe

UPDATE employe
set nomEmploye=Upper(nomEmploye), 

prenomEmploye= Concat(UPPER(SUBSTRING(prenomEmploye,1,1)) ,
Lower(Substring(prenomEmploye,2,LEN(prenomEmploye))))

select * from employe
