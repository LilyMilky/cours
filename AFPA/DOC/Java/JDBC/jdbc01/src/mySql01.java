
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class mySql01 {
    public static void main(String[] args) {
   
        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch(ClassNotFoundException ex){
            System.err.println("Oops: ClassNotFound: " + ex.getMessage());
        }
               
        Connection con = null;  
        
        try{
            String url = "jdbc:mysql://leox/cdi09";
            con = DriverManager.getConnection(url, "cdi09", "afpa");
        }catch(SQLException ex)
        {
            System.err.println("SQL: " + ex.getMessage());
        }
        
        try
        {
            Statement stmt = con.createStatement();
            String query = "Select * from maTable";
            
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next())
            {
                System.out.println(rs.getString("ref"));
                System.out.println(rs.getString("text"));
                System.out.println(rs.getString("solde"));
                System.out.println("-------------");
            }
            
            rs.close();
            stmt.close();
        }catch(SQLException ex)
        {
            System.err.println("SQL: " + ex.getMessage());
        }
        
        
        try
        {
            con.close();
        }catch(SQLException ex)
        {
            System.err.println("SQL Close: " + ex.getMessage());
        }
        
        
        System.out.println("Done");
    }
}
