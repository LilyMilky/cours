public class doException {

    public static void tests(String s) throws mesExceptions
    {
        if(s == null)
            throw new mesExceptions("La chaine ne peut etre null");
        if(s.trim().isEmpty())
            throw new mesExceptions("La chaine ne peut etre vide");
    }
    
    
    public static void main(String[] args) throws mesExceptions {
        
        try{
            tests(null);
        }
        catch(mesExceptions ex)
        {
            System.err.println("Oops: " + ex.getMessage());
            ex.printStackTrace(); //Pour afficher le message d'erreur complet(avec ligne) seulement pour le dev, ne pas utiliser pour utilisateur!!!
        }
        try{
            tests("");
        }
        catch(mesExceptions ex)
        {
            System.err.println("Oops: " + ex.getMessage());
        }
        try{
            tests("        ");
        }
        catch(mesExceptions ex)
        {
            System.err.println("Oops: " + ex.getMessage());
        }
        try{
            tests("Bonjour");
        }
        catch(mesExceptions ex)
        {
            System.err.println("Oops: " + ex.getMessage());
        }
        
        
    }
    
}
