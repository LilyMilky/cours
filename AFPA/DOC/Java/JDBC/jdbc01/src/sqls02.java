import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class sqls02 {

    public static void main(String[] args) {
        Connection connexion = null;
        SQLServerDataSource ds = new SQLServerDataSource();
        ds.setServerName("localhost");
        ds.setUser("sa");
        ds.setPassword("sa");
	ds.setDatabaseName("maBase");
        
        try {
            connexion = ds.getConnection();
        } catch (SQLServerException ex) {
            System.err.println("OOPS : SQLServer: " + ex.getErrorCode()+"/"+ ex.getMessage());
        }
        
        try {
            Statement stmt = connexion.createStatement(
            ResultSet.TYPE_SCROLL_INSENSITIVE //Permet le lecture dans les 2 sens
                    , ResultSet.CONCUR_READ_ONLY
            );
            
            String query = "select * from maTable;";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next())
            {
                System.out.println(rs.getInt("ref"));
                System.out.println(rs.getString("text"));
                System.out.println(rs.getFloat("solde"));
                System.out.println("------");
            }
            
            System.out.println("---------------------------------------");
            
            while(rs.previous())
            {
                System.out.println(rs.getInt("ref"));
                System.out.println(rs.getString("text"));
                System.out.println(rs.getFloat("solde"));
                System.out.println("------");
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println("OOPS : SQL: " + ex.getErrorCode()+"/"+ ex.getMessage());
        }
        
        
        try {
            connexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(sqls02.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Done!");
    }
    
}
