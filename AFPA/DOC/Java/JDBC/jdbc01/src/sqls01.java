import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class sqls01 {
    
    public static void main(String[] args) 
    {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Oops: ClassNotFound: " +ex.getMessage());
        }
        
        Connection connexion = null;
        
        try {
            String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=maBase;user=sa;password=sa";
            connexion = DriverManager.getConnection(connectionUrl);
        } catch (SQLException ex) {
            System.err.println("Oops: SQLConnexion: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
        
        try {
            Statement stmt = connexion.createStatement();
            
            String query = "Insert into maTable values (2,'BBB',2.1);";
            System.out.println(query);
            
            int result = stmt.executeUpdate(query);
            System.out.println("result = " + result);
            
            stmt.close();
            
        } catch (SQLException ex) {
            System.err.println("Oops: SQL: " + ex.getErrorCode() + " / " + ex.getMessage());
        }
        
        try {
            connexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(sqls01.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Done!");
      
    }
    
}
