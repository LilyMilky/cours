package essai;


public class AppliMain {
    public static void main(String[] args) {
        System.out.println("-------------  debut ------------");
        // declaration d'un tableau        
        String mots[] = null;
        System.out.println("A.1) après declaration mots:" + mots);
        // initialisation
        mots = new String[3];
        System.out.println("A.2) après initilisation mots:" + mots);

        for(int i = 0; i < mots.length ; i = i + 1){
            System.out.println("mots["+i+"]=" +mots[i]);
        }
        
        mots[2] = "deux";
        mots[0] = "zero";
        System.out.println("A.3) après remplissage :");
        for(int i = 0; i < mots.length ; i = i + 1){
            System.out.println("mots["+i+"]=" +mots[i]);
        }
    
        int entiers[] = {10,20,30,40};
        
        System.out.println("B.1) après remplissage de entiers :");
        for(int i = 0; i < entiers.length ; i = i + 1){
            System.out.println("entiers["+i+"]=" +entiers[i]);
        }
        System.out.println("B.2) longueur de entiers : " + entiers.length);
        System.out.println("=============   FIN ============");
    }
}
