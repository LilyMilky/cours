package essai;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RemplirTableau {
    public static void main(String[] args) throws IOException {
        System.out.println("----------- debut ---------");
        int n = 0;
        InputStreamReader isr = 
                new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = "";
        
        do {
            System.out.println("saisir une quantité de valeurs superieure ou egale à 1");
            s = br.readLine();
            n = Integer.valueOf(s);
        }while(n <=0);        
        
        int entiers[] = new int[n];
        
        for(int i = 0; i < entiers.length; i = i + 1){
            int j = i + 1;
            System.out.println("saisir l'entier numero "+ j );
            s = br.readLine();
            entiers[i] = Integer.valueOf(s);
        }
        
        System.out.println("--------------\nvos entiers :");
        for(int i = 0; i < entiers.length; i = i + 1){
            System.out.println("entiers["+i+"]="+entiers[i]);
        }
        
        int somme =0;
        for(int i = 0; i < entiers.length; i = i + 1){
            somme = somme + entiers[i];
            // equivalent
            //somme +=  entiers[i];
        }
        float moyenne = ((float)somme)/n;
        System.out.println("moyenne : "+ moyenne);
        br.close();
        isr.close();
        System.out.println("=========== FIN ==========");
    }
}
