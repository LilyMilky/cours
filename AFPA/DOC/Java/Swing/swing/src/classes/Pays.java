package classes;

import java.util.Vector;

public class Pays {

    private String Pays;
    private String A2;
    private String A3;
    private int Number;

    public Pays(String Text, String A2, String A3, int Number) {
        this.Pays = Text;
        this.A2 = A2;
        this.A3 = A3;
        this.Number = Number;
    }

    public String getPays() {
        return Pays;
    }

    public String getA2() {
        return A2;
    }

    public String getA3() {
        return A3;
    }

    public int getNumber() {
        return Number;
    }

    public Vector getVector() {
        Vector vv = new Vector();
        vv.add(this);
        vv.add(this.A2);
        vv.add(this.A3);
        vv.add(this.Number);
        return vv;
    }

    @Override
    public String toString() {
        return this.Pays;
    }
}
