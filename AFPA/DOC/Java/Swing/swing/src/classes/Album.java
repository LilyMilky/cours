package classes;

public class Album {
 
    private String genre;
    private String artiste;
    private String titre;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtiste() {
        return artiste;
    }

    public void setArtiste(String artiste) {
        this.artiste = artiste;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Album(String genre, String artiste, String titre) {
        this.genre = genre;
        this.artiste = artiste;
        this.titre = titre;
    }

    public Album() {
    }

    @Override
    public String toString() {
        return titre;
    }
    
    
}
