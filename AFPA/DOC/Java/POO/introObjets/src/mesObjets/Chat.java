package mesObjets;

/**
 *
 * @author sibylle
 */
public class Chat {

    //PROPRIETES DE LA CLASSE CHAT
    private String nomChat;
    private float poidsChat;
    private String couleurChat;

    // METHODES DE LA CLASSE CHAT
    //CONSTRUCTEUR PAR DEFAUT
    public Chat() {
    }

    //CONSTRUCTEUR COMPLET
    public Chat(String nomChat, float poidsChat, String couleurChat) {
        this.nomChat = nomChat;
        this.poidsChat = poidsChat;
        this.couleurChat = couleurChat;
    }

    // LES SETTERS PERMETTENT D AFFECTER UNE VALEUR A UNE
    //PROPRIETE ET DONC D Y AVOIR ACCES MEME SI ELLE EST PRIVATE
    public void setPoidsChat(float poidsChat) {
        this.poidsChat = poidsChat;
    }

    public void setCouleurChat(String couleurChat) {
        this.couleurChat = couleurChat;
    }

    public void setNomChat(String nomChat) {
        this.nomChat = nomChat;
    }

    // LES GETTERS PERMETTENT DE RECUPERER LA VALEUR D UNE PROPRIETE
    // NOTAMMENT POUR L AFFICHER
    public String getNomChat() {
        return nomChat;
    }

    public float getPoidsChat() {
        return poidsChat;
    }

    public String getCouleurChat() {
        return couleurChat;
    }

    public String suiviPoids() {

        String message = "";

        if (poidsChat > 6) {
            message = "Attention il faut mettre " + nomChat + " au régime";
        }
        else if (poidsChat < 1) {
            message = "Attention il faut mieux nourrir " + nomChat + " il est trop maigre";
        } 
        else {
            message = nomChat + " n'est ni trop maigre ni trop gros";
        }
        return message;
    }

    public static void crier()
    {
        System.out.println("MIAOU MIAOU");
    }
            
            
    @Override
    public String toString() {
     
        
       return "Ce gentil petit chat s'appelle " + nomChat
                + ", il pèse " + poidsChat
                + ", il est de couleur " + couleurChat;
      
    }

}
