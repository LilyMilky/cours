package testMesObjets;

import mesObjets.Chat;

public class testObjets {

    public static void main(String[] args) {

        // CREATION D UN OBJET CHAT EN APPELANT LE CONSTRUCTEUR
        // COMPLET QUI INITIALISE LES PROPRIETES DE L OBJET        
        Chat monPremierChat = new Chat("TOUDOUX", 8.5f, "GRIS");
        Chat monDeuxiemeChat = new Chat("DIABOLO", 0.8f, "NOIR");

        //ON CHANGE LA VALEUR DE LA PROPRIETE "nomChat"
        //EN UTILISANT LE SETTER DE "nomChat"
        monPremierChat.setNomChat("SATANAS");
        monDeuxiemeChat.setNomChat("DIABOLO");

        //ON AFFICHE LA VALEUR DES PROPRIETES EN UTILISANT LEURS GETTERS
        System.out.println("mon premier chat s'appelle : " + monPremierChat.getNomChat()
                + " son poids :" + monPremierChat.getPoidsChat()
                + " kilos, sa couleur : " + monPremierChat.getCouleurChat());

        //ON AFFICHE LA VALEUR DES PROPRIETES EN APPELANT LA METHODE
        // toString() (redéfinie) de la classe Chat
        System.out.println("mon deuxieme chat : " + monDeuxiemeChat);

        //ON VEUT SAVOIR SI NOS CHATS ONT UN POIDS CORRECT
           System.out.println(monPremierChat.suiviPoids());
           System.out.println(monDeuxiemeChat.suiviPoids());
           
        //APPEL D UNE METHODE STATIQUE
        // SE FAIT SANS AVOIR A INSTANCIER UN OBJET DE LA CLASSE CHAT
        Chat.crier();

    }

}
