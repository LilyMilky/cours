package tableauxDynamiques;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import mesClasses.Point2DD;

public class Exemple01 {
    
    public static void main(String[] args) {
        
        //Avant java 1.5
        ArrayList ar01 = new ArrayList();
        System.out.println("a.1) après instanciation, longueur: " + ar01.size());
        
        String s01= "aa";
        ar01.add(s01);
        ar01.add("Zero");
        
        System.out.println("a.2) après remplissage, longueur: " + ar01.size());
        
        Point2DD p01 = new Point2DD();
        System.out.println("a.3) p01: " + p01);
        Point2DD p02 = new Point2DD(1, 0, "p02");
        System.out.println("a.4) p02: " + p02);
        
        ar01.add(p01);
        ar01.add(p02);
        ar01.add(new Point2DD(10,1,"p03"));
        
        System.out.println("a.5) longueur: " + ar01.size());
        System.out.println("a.6) contenu: " + ar01);
        
        ((Point2DD) ar01.get(3)).setNom("X");
        System.out.println("a.7) p02: " + p02);
        
        int k = 25;
        ar01.add(15);
        ar01.add(k);
        
        int somme = (int)ar01.get(5) + (int)ar01.get(6);
        System.out.println("a.8) Somme: " + somme);
        
        // Depuis java 1.5
        
        //Autoboxing (transforme primitif en objet ex: int -> Integer)
        
        //List ne peut contenir que des integer/int
        ArrayList<Integer> entiers = new ArrayList();
        entiers.add(156);
        entiers.add(121);
        entiers.add(0);
        entiers.add(10);
        entiers.add(10);
        entiers.add(-10);
        entiers.add(6);
        //entiers.add("hellow"); //IMPOSSIBLE
        System.out.println("b.1) Liste entier: " + entiers);
        int produit = entiers.get(0) + entiers.get(1);
        System.out.println("b.2) Produit: " + produit);
        
        //Parcours pourri:
        System.out.println("b.3) Parcours pourri: ");
        for(int i = 0; i < entiers.size(); i++)
        {
            System.out.println("Entier à l'adresse: " + i + " = " + entiers.get(i));
        }
        
        //foreach
        //Pour chaque entier val dans la collection entiers
        System.out.println("b.4) Parcours foreach: ");
        System.out.println("Entier = ");
        for(Integer val:entiers)
        {
            System.out.print(val + "  ");
        }
        
        System.out.println("");
        
        //Trier les entiers (Collections contient des methodes static pour gerer les collections)
        Collections.sort(entiers);
        
        System.out.println("b.5) Parcours apres trie: ");
        System.out.println("Entier = ");
        for(Integer val:entiers)
        {
            System.out.print(val + "  ");
        }
        
        System.out.println("");
        
        
        
        List<String> mots = new ArrayList();
        mots.add("MMM");
        mots.add("eee");
        mots.add("ZZZ");
        mots.add("AAA");
        mots.add("EEE");
        mots.add("fff");
        
        Collections.sort(mots);
        
        
        System.out.println("b.6) Parcours tableau mot apres trie: ");
        for(String val:mots)
        {
            System.out.println(val + "  ");
        }
        
        
        //ITERATEUR
        
        List<Integer> nbs = new ArrayList();
        nbs.add(-10);
        nbs.add(26);
        nbs.add(30);
        nbs.add(100);
        nbs.add(-56);
        
        System.out.println("c.1) tous les entiers: " + nbs);
        
        //Pas de remove/add dans un foreach
//        for(Integer v : nbs)
//        {
//            if(v < 0)
//            {
//                nbs.remove(v);
//            }
//        }
        
        //Curseur
        Iterator<Integer> it = nbs.iterator();
        while(it.hasNext())
        {
            int valeur = it.next();
            if(valeur < 0)
            {
                //Supprime la valeur courante
                it.remove();
           }
        }

        
        System.out.println("c.2) Liste entier apres remove: ");
        for(Integer v : nbs)
        {
            System.out.print(v + "  ");
        }
        
        System.out.println("");
        
    }
}
