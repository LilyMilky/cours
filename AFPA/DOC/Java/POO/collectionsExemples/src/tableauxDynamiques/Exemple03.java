package tableauxDynamiques;

import java.util.HashMap;
import mesClasses.Point2DD;

public class Exemple03 {

    public static void main(String[] args) {
        HashMap<String, Integer> mp = new HashMap();
        mp.put("un", 1);
        mp.put("dix", 10);
        System.out.println("mp : "+mp);
        System.out.println("-------------------------");
        HashMap<Point2DD, String> mp02 = new HashMap();
        Point2DD p01 = new Point2DD(0, 0, "A");
        mp02.put(p01, "Ville A");
        System.out.println("a.1) mp02 : "+mp02);
        Point2DD p02 = new Point2DD(0, 0, "H");
        mp02.put(p02, "Ville H");
        System.out.println("a.2) mp02 : "+mp02);
        mp02.put(new Point2DD(5, 7, "E"), "Ville E");
        System.out.println("a.3) mp02 : "+mp02);
        
        String ville = mp02.get(p02);
        System.out.println("ville = "+ville);
        
    }

}
