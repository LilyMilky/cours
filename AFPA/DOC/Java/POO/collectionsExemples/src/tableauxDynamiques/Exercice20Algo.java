package tableauxDynamiques;

import java.util.HashMap;


public class Exercice20Algo {
    public static void main(String[] args) {
        String phrase = "Jeune,          beau et élégant. Talon, le ciel était dégagé, comme une plante, une fleur vivante, épandant ses effluves, en pleine lumière ceux qui le dirigent. Secrètement, elle versait bien des larmes et lui creusèrent une fosse. Savoir est un viatique ; penser est de première force pour lire les faits divers : on ne donne pas dans ces tourbillons de poussière. Abasourdi d'un spectacle gratis régalent le parterre. Inspiré par la sympathie qu'elles avaient été à la guerre : c'est notre arbre généalogique ! Demeure ; il faut le redire, c'était justement la promiscuité où se coudoyaient là tous les matins ; elle avait cru être, l'empoigne par la peau du corps ! Enferré, le taureau, qu'elle puisse, il est là pour montrer que le fait de vous mon éternel ennemi." +
        "Chrétien réconcilié, ne m'a-t-on pas raconté que le jour dût venir de sitôt, car ce qui appelle à l'existence qui sont si déplacées dans ma bouche. Gardons-nous bien de raconter la proposition de cette nouvelle clientèle chinoise, avaient conduit à cette conclusion par mes propres amis.";
        String phrase01 = phrase.replaceAll(" ", "");
        HashMap<Character, Integer> mp = new HashMap();
        for(int i = 0; i < phrase01.length(); i++){
            Character c = phrase01.charAt(i);
            int qte = 1;
            if(mp.containsKey(c)){
                qte = mp.get(c) +1;
            }
            mp.put(c, qte);
        }
        System.out.println("resultat:\n"+mp);
        System.out.println("------------- avec un for -------");
        for( Character cle : mp.keySet()){
            System.out.println(cle+"->"+mp.get(cle)+"x");
        }
    }
}
