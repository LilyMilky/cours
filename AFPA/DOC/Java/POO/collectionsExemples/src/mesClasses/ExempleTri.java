package mesClasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExempleTri {

    public static void main(String[] args) {

        List<Point2DD> points = new ArrayList();
        points.add(new Point2DD(1, 15, "E"));
        points.add(new Point2DD(6, 2, "H"));
        points.add(new Point2DD(-5, -5, "c"));
        points.add(new Point2DD(20, 2, "A"));
        points.add(new Point2DD(15, 3, "B"));
        points.add(new Point2DD(6, 8, "R"));
        points.add(new Point2DD(15, 2, "m"));
        points.add(new Point2DD(6, 8, "a"));
        points.add(new Point2DD(-5, 2, "M"));
        points.add(new Point2DD(20, 1, "A"));

        Collections.sort(points);

        System.out.println("Apres tri par defaut: " + points);

        System.out.println("----------Tri par nom----------");

//      Comparator<Point2DD> tri = new TriParNom();        
//      Collections.sort(points, tri);
        Collections.sort(points, new TriParNom());

        System.out.println("Apres tri par nom: " + points);

        System.out.println("--------Tri par classe interne-----------");
        Collections.sort(points, new TriParY());

        System.out.println("Apres tri par Y" + points);

        //CLASSE ANONYME
        System.out.println("Tri par x+y ");

        Collections.sort(points,
                new Comparator<Point2DD>() {

                    @Override
                    public int compare(Point2DD o1, Point2DD o2) {
                        float somme01 = o1.getX() + o1.getY();
                        float somme02 = o2.getX() + o2.getY();
                        float d = somme01 - somme02;
                        
                        if(d>0)
                            return 1;
                        else if (d<0)
                            return -1;
                        else
                            return 0;
                    }

                }
        );//Fin appel parametre
        
        System.out.println("Apres tri X+Y: " + points);

        //---------------------------------------------------------------------------------------
    }

    //Classe Interne: Toutes variables de la classe externe visible dans la classe interne

    static class TriParY implements Comparator<Point2DD> {

        @Override
        public int compare(Point2DD o1, Point2DD o2) {
            int res = 0;
            if (o1.getY() - o2.getY() < 0) {
                res = -1;
            } else if (o1.getY() - o2.getY() > 0) {
                res = 1;
            }

            return res; //Peut mettre - pour obtenir l'inverse du resultat obtenu.
        }

    }

}
