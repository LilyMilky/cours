package mesClasses;

public class Point2DD implements Comparable<Point2DD>{
    
    private float x;
    private float y;
    private String nom;

    public Point2DD() {
    }

    public Point2DD(float x, float y, String nom) {
        this.x = x;
        this.y = y;
        this.nom = nom;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Override
    public String toString()
    {
        return "Nom: " + nom + " (x: " + x + " y: " + y + ")";
    }

    @Override
    public int compareTo(Point2DD cible) {
        
        int res = 0;
        
        if(this.x - cible.x <0)
            res = -1;
        else if(this.x - cible.x > 0)
            res = 1;
        
        return res;
    }
    
    
}
