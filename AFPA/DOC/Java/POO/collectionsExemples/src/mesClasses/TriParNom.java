package mesClasses;

import java.util.Comparator;

public class TriParNom implements Comparator<Point2DD>{

    @Override
    public int compare(Point2DD o1, Point2DD o2) {
        String nom01 = o1.getNom();
        String nom02 = o2.getNom();
        
        return nom01.compareToIgnoreCase(nom02);
    }
    
}
