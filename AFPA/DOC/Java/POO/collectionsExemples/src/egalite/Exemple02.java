package egalite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import mesClasses.Point2DD;

public class Exemple02 {
    public static void main(String[] args) throws IOException {
        
        /*
        String s01 = "bonjour";
        String s02 = "bonjour";
        String s03 = "hello";
        boolean b01 = s01 == s02; // true
        boolean b02 = s01 == s03; // false
        
        System.out.println("b01 = "+b01);
        System.out.println("b02 = "+b02);
        System.out.println("------------------------------");
        InputStreamReader isr = 
                new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.println("saisir un mot :");
        String mot = br.readLine();
        
        System.out.println("s01 = "+s01);
        System.out.println("mot = "+mot);
        boolean b03 = s01 == mot;
        System.out.println("s01 == mot ? "+b03);
        boolean b04 = mot.equals(s01);
        System.out.println("mot.equals(s01) ? "+b04);
        br.close();
        isr.close();
        */
        
        String str = "hello";
        Point2DD p01 = new Point2DD(0, 0, "R");
        Point2DD p02 = new Point2DD(0, 0, "A");
        Point2DD p03 = new Point2DD(9, 10, "R");
        System.out.println(p01.equals(null));
        System.out.println(p01.equals(str));
        System.out.println(p01.equals(p02));
        System.out.println(p01.equals(p03));
        System.out.println(p01==p02);
    }
}
