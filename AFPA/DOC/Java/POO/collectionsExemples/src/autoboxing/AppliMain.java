package autoboxing;

import mesClasses.Point2DD;

public class AppliMain {
    public static void main(String[] args) {
        Integer i = null;
        i = new Integer(15);
        int j = 15;
        
        j = i;
        
        Point2DD  p = new Point2DD();
        String s = "hello";
        // p = s;
        
        Boolean b = null;
        b = false;
        boolean c;
        c = b;        
        
        System.out.println("---------------------");
        int en[] = {10,11,12,13};
        for(int n : en){
            System.out.println("valeur = "+n);
        }
    }
}
