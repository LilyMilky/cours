
package essai;

import individus.Guerrier;
import individus.Soigneur;
import items.Baton;
import items.Hache;
import java.io.IOException;
import stuffs.Fusil;
import terrains.AreneConsole;


public class AppliMain {
    public static void main(String[] args) throws IOException {
        Hache h01 = new Hache(6, "glace", "superHache");
        Hache h02 = new Hache(5, "feu", "hache feu");
        Guerrier gu01 = new Guerrier(h02, 15, 15, 2, "Conan");
        Guerrier gu02 = new Guerrier(h01, 17, 17, 2, "Hulk");        
//        Baton b01 = new Baton(3, "eau", "superbaton");
//        Soigneur s01 = new Soigneur(b01, 10, 10, 2, "Doc");
        
        Fusil f01 = new Fusil(7);
        Guerrier g03 = new Guerrier(f01, 10, 10, 1, "snipper sadique");
        
        AreneConsole arene =  new AreneConsole(g03, gu01, gu02);
        arene.commencer();
        
    }
}
