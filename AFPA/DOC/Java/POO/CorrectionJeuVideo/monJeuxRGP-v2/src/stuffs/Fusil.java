package stuffs;

import individus.Personnage;
import items.ArmeDeGuerre;

public class Fusil implements ArmeDeGuerre{
    
    private int degat;
    
    public Fusil() {
    }

    public Fusil(int degat) {
        this.degat = degat;
    }

    
    
    
    @Override
    public String attaquer(Personnage auteur, Personnage cible) {
        int qte = cible.getVie() - degat;
        cible.setVie(qte);
        return cible.getNom() + "subit "+ degat +" points";
        
    }
    
}
