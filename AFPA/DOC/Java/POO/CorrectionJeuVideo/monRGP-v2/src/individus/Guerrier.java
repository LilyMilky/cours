package individus;

import items.ArmeDeGuerre;

public class Guerrier extends Personnage{
    private ArmeDeGuerre arme;

    public Guerrier() {
    }

    public Guerrier(ArmeDeGuerre arme, int hpMax, int vie, int force, String nom) {
        super(hpMax, vie, force, nom);
        this.arme = arme;
    }

    public ArmeDeGuerre getArme() {
        return arme;
    }

    public void setArme(ArmeDeGuerre arme) {
        this.arme = arme;
    }
  
    @Override
    public String agir(Personnage cible) {
        if(arme != null) {
            return arme.attaquer(this, cible);
        }else {
            return agirDefaut(cible);
        }
    }
}
