package individus;

import items.ArmeDeSoin;

public class Soigneur extends Personnage{
    private ArmeDeSoin arme;

    public Soigneur() {
    }

    public Soigneur(ArmeDeSoin arme, int hpMax, int vie, int force, String nom) {
        super(hpMax, vie, force, nom);
        this.arme = arme;
    }

    public ArmeDeSoin getArme() {
        return arme;
    }

    public void setArme(ArmeDeSoin arme) {
        this.arme = arme;
    }

    @Override
    public String agir(Personnage cible) {
        if(arme !=null){
            return arme.soigner(this, cible);
        }else {
            return "je peux pas soigner!";
        }
    }
   
}
