package individus;

public abstract class Personnage {
    private int hpMax;
    private int vie;
    private int force;
    private String nom;

    public Personnage() {
    }

    public Personnage(int hpMax, int vie, int force, String nom) {
        this.hpMax = hpMax;
        setVie(vie);
        this.force = force;
        this.nom = nom;
    }

    public int getHpMax() {
        return hpMax;
    }

    public void setHpMax(int hpMax) {
        this.hpMax = hpMax;
    }

    public int getVie() {
        return vie;
    }

    public final void setVie(int vie) {
        if(vie < 0){
            vie = 0;
        }
        if(vie > hpMax){
            vie  = hpMax;
        }
        this.vie = vie;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String agirDefaut(Personnage cible){   
        if(vie > 0){
            int qte = cible.getVie() - force;
            cible.setVie(qte);
            // cible.setVie(cible.getHpMax() - force);
            return nom + "inflige "+force +" points de degat à "+cible.getNom();
        }else {
            return "Vous ne pouvez pas effectuer cette action.";
        }         
    }
    
    public abstract String agir(Personnage cible);
}
