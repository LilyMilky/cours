package terrains;

import individus.Personnage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AreneConsole {
    private Personnage pers[];

    public AreneConsole() {
        pers = new Personnage[3];
    }

    public AreneConsole(Personnage soigneur, Personnage guerrier01, Personnage guerrier02) {
        this();
        pers[0] = soigneur;
        pers[1] = guerrier01;
        pers[2] = guerrier02;
    }

    public Personnage[] getPers() {
        return pers;
    }

    public void setPers(Personnage[] pers) {
        this.pers = pers;
    }
    
    public void commencer() throws IOException{
        boolean continuer = true;
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String tmp;
        int choix;
        System.out.println("==========  DEBUT ==========");
        do {
            System.out.println(pers[1].agir(pers[2]));
            System.out.println(pers[2].agir(pers[1]));
            System.out.println("Choisir un personnage");
            System.out.println("\t1: "+pers[1].getNom());
            System.out.println("\t2: "+pers[2].getNom());
            tmp = br.readLine();
            choix = Integer.valueOf(tmp);
            System.out.println("-------------- resultat du tour -------------");
            System.out.println(pers[1].getNom()+" : " +pers[1].getVie());
            System.out.println(pers[2].getNom()+" : " +pers[2].getVie());
            System.out.println(pers[0].agir(pers[choix]));

            if((pers[1].getVie() <= 0) || (pers[2].getVie() <= 0)){
                continuer = false;
            }
        }while(continuer);
    
        if(pers[1].getVie() > 0){
            System.out.println(pers[1].getNom() +" a gagné!");
        }else {
            System.out.println(pers[2].getNom() +" a gagné!");
        }
        
        br.close();
        isr.close();
        System.out.println("=============  FIN ==============");
    }
    
    
}
