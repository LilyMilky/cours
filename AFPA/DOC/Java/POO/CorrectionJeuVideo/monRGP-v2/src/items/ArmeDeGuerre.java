package items;

import individus.Personnage;

public interface ArmeDeGuerre {
    public String attaquer(Personnage auteur, Personnage cible);
}
