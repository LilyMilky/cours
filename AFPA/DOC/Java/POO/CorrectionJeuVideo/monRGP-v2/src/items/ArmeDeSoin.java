package items;

import individus.Personnage;

public interface ArmeDeSoin {
   public String soigner(Personnage auteur, Personnage cible); 
}
