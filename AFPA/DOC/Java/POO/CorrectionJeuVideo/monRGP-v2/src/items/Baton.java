package items;

import individus.Personnage;


public class Baton extends Arme implements ArmeDeSoin{

    public Baton() {
    }

    public Baton(int puissance, String element, String nom) {
        super(puissance, element, nom);
    }
    
    

    @Override
    public String agir(Personnage cible) {
        if(cible.getVie() > 0){
            int qte  = cible.getVie()+ getPuissance();
            cible.setVie(qte);
            return cible.getNom()+" recupere "+ getPuissance() +" points de vie";
        }else {
            return cible.getNom() +" est incurable.";
        }
    }

    @Override
    public String soigner(Personnage auteur, Personnage cible) {
        return agir(auteur, cible);
    }
    
}
