package items;

import individus.Personnage;


public class Hache extends Arme implements ArmeDeGuerre{

    public Hache() {
    }

    public Hache(int puissance, String element, String nom) {
        super(puissance, element, nom);
    }
    
    
    
    @Override
    public String agir(Personnage cible) {
        int qte = cible.getVie() - getPuissance();
        cible.setVie(qte);
        return cible.getNom() + " subit " + getPuissance()
                +" point de degat("+getElement()+")";
    }

    @Override
    public String attaquer(Personnage auteur, Personnage cible) {
        return agir(auteur, cible);
    }
   
}
