package items;

import individus.Personnage;

public abstract class Arme {
    private int puissance;
    private String element;
    private String nom;

    public Arme() {
    }

    public Arme(int puissance, String element, String nom) {
        this.puissance = puissance;
        this.element = element;
        this.nom = nom;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String agir(Personnage auteur, Personnage cible){
        String s = "";
        if(auteur.getVie() > 0){
            s = auteur.getNom()+" agit sur "+cible.getNom()+".";
            s += " "+agir(cible);
            return s;
        }else {
            return auteur.getNom() + " ne peut pas faire cette action.";
        }
    }
    
    public abstract String agir(Personnage cible);
    
}
