package exemple;


public class Rectangle extends Figure2DD{
    private float longueur;
    private float largeur;

    public Rectangle() {
    }

    public Rectangle(float longueur, float largeur) {
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public float getLongueur() {
        return longueur;
    }

    public void setLongueur(float longueur) {
        this.longueur = longueur;
    }

    public float getLargeur() {
        return largeur;
    }

    public void setLargeur(float largeur) {
        this.largeur = largeur;
    }

    @Override
    public float calculerSurface() {
        return longueur * largeur;
    }
  
}
