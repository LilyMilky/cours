package exemple;

public class Pyramide {
    private Figure2DD base;
    private float hauteur;

    public Pyramide() {
    }

    public Pyramide(Figure2DD base, float hauteur) {
        this.base = base;
        this.hauteur = hauteur;
    }

    public Figure2DD getBase() {
        return base;
    }

    public void setBase(Figure2DD base) {
        this.base = base;
    }

    public float getHauteur() {
        return hauteur;
    }

    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }
    
    public float calculerVolume(){
        return hauteur * base.calculerSurface() / 3;
    }
    
}
