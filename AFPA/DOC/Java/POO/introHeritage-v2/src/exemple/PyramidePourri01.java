
package exemple;

public class PyramidePourri01 {
    private Carre base;
    private float hauteur;

    public PyramidePourri01() {
    }

    public PyramidePourri01(Carre base, float hauteur) {
        this.base = base;
        this.hauteur = hauteur;
    }

    public Carre getBase() {
        return base;
    }

    public void setBase(Carre base) {
        this.base = base;
    }

    public float getHauteur() {
        return hauteur;
    }

    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }
    
    public float calculerVolume(){
        return hauteur * base.calculerSurface() / 3;
    }
    
    
}
