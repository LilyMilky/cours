
package exemple;


public class PyramidePourri02 {
    private Rectangle base;
    private float hauteur;

    public PyramidePourri02() {
    }

    public PyramidePourri02(Rectangle base, float hauteur) {
        this.base = base;
        this.hauteur = hauteur;
    }

    public Rectangle getBase() {
        return base;
    }

    public void setBase(Rectangle base) {
        this.base = base;
    }

    

    public float getHauteur() {
        return hauteur;
    }

    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }
    
    public float calculerVolume(){
        return hauteur * base.calculerSurface() / 3;
    }
}
