package exemple;

// abstract : on interdit à la classe d'avoir des instances
public abstract class Figure2DD {
    private String nom;

    public Figure2DD() {
    }

    public Figure2DD(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
        
    // methode abstraite = methode sans corps
    // les classes filles 'concrete' doivent definir les methodes abstraites
    // les reference de type Figure2DD 'voient' / accéder la methode
    public abstract float calculerSurface();
    
    // à savoir : 
    // 1) une classe contenant une methode abstraite est forcément une classe abstraite
    // 2) une classe peut être abstraite sans contenir de methode abstraite
}
