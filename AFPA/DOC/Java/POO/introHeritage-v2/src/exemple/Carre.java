package exemple;


public class Carre extends Figure2DD{
    private float c;

    public Carre() {
    }

    public Carre(float c, String nom) {
        super(nom);
        this.c = c;
    }

    public float getC() {
        return c;
    }

    public void setC(float c) {
        this.c = c;
    }
    
    
   
    @Override
    public float calculerSurface(){
        return c * c;
    }
}
