package exemple;


public class Cercle extends Figure2DD{
    private float rayon;

    public Cercle() {
    }

    public Cercle(float rayon, String nom) {
        super(nom);
        this.rayon = rayon;
    }

    @Override
    public float calculerSurface() {
     return 3.14f * rayon * rayon;    
    }
    
    
    
}
