
package exemple;


public class AppMain {
    public static void main(String[] args) {
        Carre cr01 = new Carre(4, "carre01");
        float aire01 = cr01.calculerSurface();
        System.out.println("a.1) aire de cr01 = "+aire01);
        
        Figure2DD f01 = cr01;
        System.out.println("a.2) aire de f01 = "+f01.calculerSurface());
        
        System.out.println("------------ pyramide 01 ----------");
        PyramidePourri01 pyr01 = new PyramidePourri01(cr01, 12);
        //float v01 = pyr01.calculerVolume();
        System.out.println("b.1) volume de pyr01 = "+pyr01.calculerVolume());
        
        System.out.println("--------  vraie pyramide ---------");
        Rectangle rg01 = new Rectangle(5, 4);
        Pyramide pr01 = new Pyramide(rg01, 18);
        System.out.println("c.1) volume de pr01 = "+pr01.calculerVolume());
        
        pr01.setBase(cr01);
        pr01.setHauteur(12);
        
        System.out.println("c.2) volume de pr01 = "+pr01.calculerVolume());
        
        System.out.println(" ----------- avec un cercle --------");
        Cercle cl01 = new Cercle(1, "cercle");
        pr01.setBase(cl01);
        pr01.setHauteur(3);
        System.out.println("c.3) volume de pr01 = "+pr01.calculerVolume());
    }
}
