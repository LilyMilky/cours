package heritage;

public class Dauphin extends Animal {
     private float longueur;
    
    // il y a un appel au constructeur de la classe mere dans le contructeur par defaut de la classe fille
    public Dauphin() {
        super();
        
    }

    public Dauphin(float longueur, String nom, int poids, int age) {
        super(nom, poids, age);
        this.longueur = longueur;
    }

    public float getLongueur() {
        return longueur;
    }

    public void setLongueur(float longueur) {
        this.longueur = longueur;
    }
    
     public String seDeplacer(){
         return "je me deplace en nageant!";
     }
}
