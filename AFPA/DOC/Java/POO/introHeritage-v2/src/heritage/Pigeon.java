package heritage;


public class Pigeon extends Animal {
    private float envergure;
    private float vitesseMax;

    public Pigeon() {
        
    }

    public Pigeon(float envergure, String nom, int poids, int age) {
        super(nom, poids, age);
        this.envergure = envergure;
    }

    public float getEnvergure() {
        return envergure;
    }

    public void setEnvergure(float envergure) {
        this.envergure = envergure;
    }

    public float getVitesseMax() {
        return vitesseMax;
    }

    public void setVitesseMax(float vitesseMax) {
        this.vitesseMax = vitesseMax;
    }
    
    
    
    public String seDeplacer(){
        return "je peux atteindre "+ vitesseMax + "km/h en volant!";
    }
    
}
