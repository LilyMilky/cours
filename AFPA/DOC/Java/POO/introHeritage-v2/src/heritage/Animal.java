package heritage;

public class Animal {
    private String nom;
    private int poids; // en g
    private int age; // attention c'est à faire avec une date

    public Animal(String nom, int poids, int age) {
        this.nom = nom;
        this.poids = poids;
        this.age = age;
    }

    public Animal() {
        
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
   
    
    public String seDeplacer(){
        return "je me deplace!";
    }
    
}
