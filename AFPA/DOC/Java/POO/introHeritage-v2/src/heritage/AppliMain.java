package heritage;

public class AppliMain {
    public static void main(String[] args) {
        System.out.println("----------  debut -----------");
//      Animal anim = new Animal();
        System.out.println("a.1) instanciation d'un Dauphin");
        Dauphin d01 = new Dauphin();
        String s = d01.seDeplacer();
        System.out.println("a.2) s : " + s);
        System.out.println("a.3) nom d01 = " + d01.getNom());
        
        System.out.println("b.1) instanciation avec le 2eme constructeur");
        Dauphin d02 = new Dauphin(2.5f, "Flipper", 100000, 4);
        System.out.println("b.2) nom d02 = " + d02.getNom());
        System.out.println("b.2) age de d02 = " + d02.getAge());
        
        System.out.println("c.1 utilisation des types Mere, Fille");
        Animal anim02 = new Animal();
//        d02 = (Dauphin) anim02;
        
        anim02 = new Dauphin();
        Dauphin d03 = (Dauphin) anim02;
        String s15 = anim02.seDeplacer();
        System.out.println("c.2) anim02.seDeplacer() = "+s15);
        
        anim02 = new Pigeon(60, "Titi", 1, 6);
        // anim02 est une reference de type Animal : elle ne 'voit' que les prorpiétés et les methodes publiques de la classe Animal
        // - si Animal herite d'une classe mere, anim02 verra aussi les propriétés et les méhodes publiques héritées
        // on ne peut pas  faire anim02.setVitesseMax(30);
        // donc :
        Pigeon p01 = (Pigeon)anim02;
        p01.setVitesseMax(30);
        
        s15 = anim02.seDeplacer();
        
        System.out.println("c.2) anim02.seDeplacer() = "+s15);
        
        System.out.println("------------- fin -----------");
    }
}
