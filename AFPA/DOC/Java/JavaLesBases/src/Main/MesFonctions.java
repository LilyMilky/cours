package Main;

import java.io.IOException;
import static outils.EntreesSorties.*;


public class MesFonctions {

    public static void main(String[] args) throws IOException {

          System.out.println("ENTREZ a");
        //variable pour récupérer la saisie
        String saisie =lireAuClavier();
        int chiffre1;
        chiffre1 = Integer.valueOf(saisie);

        System.out.println("ENTREZ B");
        saisie =lireAuClavier();
        float chiffre2;
        chiffre2 = (float) Integer.valueOf(saisie);

        //APPELER LA FONCTION calculer et afficher son retour    
        String result=(calculer(chiffre1,chiffre2," - "));
        System.out.println("RESULTAT "+result);
        
        fermerFlux();
  }   
    
    //une fonction en JAVA a une signature qui est UNIQUE 
    //au sein d'une même classe
    
    //LA SIGNATURE D'UNE FONCTION EST COMPOSEE DE
    // SON NOM
    // DU NOMBRE DE SES PARAMETRES
    // DU TYPE DE SES PARAMETRES
    // DE L ORDRE DE SES PARAMETRES
    
    
    public static float calculer(int x, int y) {
        
        System.out.println("JE DIVISE");
        float resultat;
        resultat= (float) x/y;       
        return resultat;
    }
    
    public static float calculer(float x, int y)
    {
        System.out.println("JE MULTIPLIE ");
        float resultat;
        resultat=x*y;
        return resultat;
    }

    public static float calculer(int x, float y)
    {
        System.out.println("J'ADDITIONNE ");
        float resultat;
        resultat=x+y;
        return resultat;
    }
   
    public static String calculer(int x, float y,String separateur)
    {
        System.out.println("JE CONCATENE ");
        return x+separateur+y;
    }

    //LA FONCTION "calculer" EST UNE FONCTION SURCHARGEE
    
    
    
}

    
    
    


    
    
    
    
   