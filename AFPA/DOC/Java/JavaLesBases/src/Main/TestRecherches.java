package Main;

import static outils.Recherches.recherche;

/**
 *
 * @author sibylle
 */
public class TestRecherches {

    public static void main(String[] args) {

        String maChaine = "ceci est un exemple de chaine de caractères";

        //ON VEUT SAVOIR SI UN CARACTERE EST PRESENT DANS LA CHAINE
   
        char monCaractere = 'a';
        int position = recherche(maChaine, monCaractere);

        if (position != -1) {
            System.out.println("Le caractère " + monCaractere
                    + " est présent dans la chaîne");

        } else {
            System.out.println("Le caractère " + monCaractere
                    + " ne se trouve pas dans la chaîne");

        }
        
         //ON VEUT SAVOIR SI UN CARACTERE EST PRESENT DANS LA CHAINE
         //ET COMBIEN DE FOIS ELLE FIGURE DANS CETTE CHAINE
        int nbFois=0;
        nbFois=recherche(maChaine, monCaractere,nbFois);
        
        if (nbFois >0){
  
          
        int leTableau[]={12,17,20,40,66,102};
        int laValeur=22;
        boolean existe=recherche(leTableau,laValeur);
        
        if (existe) {
            
                System.out.println("La valeur "+laValeur
                    + " est présente dans le tableau)");

        } else {
            System.out.println("La valeur "+laValeur
                    + " n'est pas présente dans le tableau)");
        }      
            
        }
        
        
    }

}
