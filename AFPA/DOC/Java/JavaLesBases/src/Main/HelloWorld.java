package Main;

//declaration d'une classe PRINCIPALE NON OBJET HelloWorld

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HelloWorld {

    public static void main(String[] args) throws IOException {

      //TYPES DE VARIABLES DE BASE
        // char (caractère)
        // int(entier 4octets)
        // long (entier long 8octets)
        // short (petit entier (2octets)
        // float (réel 4 octets)
        // double (réel long 8octets)
        // boolean (booléen)
       //TYPE DE VARIABLES OBJETS 
        //String (Chaine de caractères)
        //Integer (entier)
        //Déclaration d'une variable de type chaine de caractère
        String message;
        //Affectation d'une valeur à la variable
        message = "Hello World";
 
        System.out.println(message);
        
     //ON VEUT RECUPERER UN MESSAGE TAPE AU CLAVIER
     // par un utilisateur et pouvoir l'afficher
        
        //ON DECLARE UNE VARIABLE DE TYPE"InputStreamReader"
        //qui permet de lire une entrée clavier
        InputStreamReader monEntree;
        monEntree = new InputStreamReader(System.in);
        
          //ON DECLARE UNE VARIABLE DE TYPE"BufferedReader"
        //qui permet STOCKER la valeur entrée au clavier
        BufferedReader monBuffer;
        monBuffer = new BufferedReader(monEntree);
        
        //on DECLARE UNE VARIABLE DE TYPE CHAINE DE CARACTERE
        String maSaisie;
       String maSaisie2;
        //On demande à l'utilisateur d'entrer un mot
        //System.out.println("Entrez votre prenom");
        System.out.println("Entrez un premier chiffre");
        
        //On affecte la valeur saisie au clavier
        //à la variable maSaisie
         maSaisie=monBuffer.readLine();
         Integer chiffre1= Integer.parseInt(maSaisie);
        
        //System.out.println("BONJOUR "+maSaisie);
        System.out.println("PREMIER CHIFFRE : "+maSaisie);
      
          //System.out.println("Entrez votre prenom");
        System.out.println("Entrez un SECOND chiffre");
        
        //On affecte la valeur saisie au clavier
        //à la variable maSaisie
        maSaisie2=monBuffer.readLine();
        Integer chiffre2= Integer.parseInt(maSaisie2);
 
          //System.out.println("BONJOUR "+maSaisie);
        System.out.println("SECOND CHIFFRE : "+maSaisie2);
 
        System.out.println(chiffre1+"DIVISE PAR "+chiffre2+" EGAL "+chiffre1/chiffre2);
        

        // ON REFERME LE BUFFER READER et l'inputStreamReader
        monBuffer.close();
        monEntree.close();
        
        
    }

}
