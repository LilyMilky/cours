package Main;

public class MesBoucles {

    public static void main(String[] args) {

        //AFFICHER LES NOMBRES DE 1 A 10
        // AVEC UNE BOUCLE FOR
        for (int i = 1; i <= 10; i++) {

            System.out.print("BOUCLE FOR : " + i + " - ");
        }

        System.out.println("----------------------------------");
        System.out.println();

        //AFFICHER LES NOMBRES DE 1 A 10
        // AVEC UNE BOUCLE WHILE
        int i;
        i = 1;
        while (i <= 10) {

            System.out.print("BOUCLE WHILE : " + i + " - ");
            i++;
        }

        System.out.println("----------------------------------");
        System.out.println();

        //AFFICHER LES NOMBRES DE 1 A 10
        // AVEC UNE BOUCLE DO WHILE (L'instruction s'exécute au moins 1 fois)
        i = 1;
        do {
            System.out.print("BOUCLE DO WHILE : " + i + " - ");
            i++;
        } while (i <= 10);
        

    }

}
