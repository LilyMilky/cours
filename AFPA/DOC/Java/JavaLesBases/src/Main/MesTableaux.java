package Main;

import java.io.IOException;
import static outils.EntreesSorties.afficherTableau;
import static outils.EntreesSorties.lireAuClavier;
import static outils.EntreesSorties.remplirTableau;

/**
 *
 * @author sibylle
 */
public class MesTableaux {

    public static void main(String[] args) throws IOException {
        int n = 0;

        //ON DEMANDE A L UTILISATEUR D ENTRER LA TAILLE DU TABLEAU
        do {
            System.out.println("ENTREZ la taille du tableau");
        //variable pour récupérer la saisie
            //ON UTILISE LA FONCTION lireAUClavier de outils    
            String saisie = lireAuClavier();
            int chiffre1;
            n = Integer.valueOf(saisie);
        } while (n < 0);

        int monTableau[] = new int[n];
        //ON APPELLE LA FONCTION DE OUTILS POUR REMPLIR LE TABLEAU
        monTableau = remplirTableau(monTableau);

        //ON APPELLE LA FONCTION DE OUTILS POUR AFFICHER LE TABLEAU
        afficherTableau(monTableau);

    }

}
