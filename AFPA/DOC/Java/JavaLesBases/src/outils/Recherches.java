/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

/**
 *
 * @author sibylle
 */
public class Recherches {

    //CETTE FONCTION PREND EN PARAMETRE UNE CHAINE DE CARACTERES ET UN CARACTERE
    //ELLE RETOURNE UN INT qui a la valeur -1 si le caractère n'a pas été trouvé dans la chaien
    //                         une valeur supérieure à 0 si le caractère a été trouvé
    public static int recherche(String laChaine, char leCaractere) {

        int positionCaractere = -1;

        for (int i = 0; i < laChaine.length(); i++) {

            if (laChaine.charAt(i) == leCaractere) {
                positionCaractere = i + 1;
            }
        }
        return positionCaractere;
    }

    public static int recherche(String laChaine, char leCaractere, int nbFois) {

        for (int i = 0; i < laChaine.length(); i++) {

            if (laChaine.charAt(i) == leCaractere) {

                nbFois++;
            }
        }
        return nbFois;
    }

    // CETTE FONCTION PREND UN ARGUMENT UN TABLEAU D ENTIERS ET UN ENTIER
    // ELLE RETOURNE UN BOOLEAN QUI EST A TRUE SI L'ENTIER FIGURE DANS LE TABLEAU
    public static boolean recherche(int leTableau[], int unEntier) {
        boolean trouve=false;

        for (int i = 0; i < leTableau.length; i++) {

            if (leTableau[i] == unEntier) {

                trouve = true;

            }
        }
        return trouve;

    }

}
