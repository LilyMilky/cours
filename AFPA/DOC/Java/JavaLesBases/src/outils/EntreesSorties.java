package outils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author sibylle
 */
public class EntreesSorties {

    static InputStreamReader isr = new InputStreamReader(System.in);

    static BufferedReader br = new BufferedReader(isr);

    static String saisie;

    public static String lireAuClavier() throws IOException {
        saisie = br.readLine();

        return saisie;

    }

    public static void fermerFlux() throws IOException {
        br.close();
        isr.close();
    }

    public static int[] remplirTableau(int tab[]) throws IOException {
        System.out.println("Vous devez entrer " + tab.length + " chiffres");
        for (int i = 0; i < tab.length; i++) {
            String saisie = lireAuClavier();
            tab[i] = Integer.valueOf(saisie);
        }
        return tab;
    }

    public static void afficherTableau(int tab[]) {

        for (int i = 0; i < tab.length; i++) {
            System.out.println("indice " + i + " valeur : " + tab[i]);
        }

    }
}
